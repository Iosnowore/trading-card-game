﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TCGData.Services;

namespace TCGData
{
    public static class Commands
    {
        /*
         * Command Bytes
         */

        ///
        /// -- Start of CHAT COMMANDS (these need to be integers therefore we can set them in commands.sdb
        /// 

        // SerializationException results in '0'

        // Staff
        public const byte BROADCAST_MESSAGE = 1;
        public const byte KICK_PLAYER = 2;
        public const byte MUTE_PLAYER = 3;
        public const byte SET_CARD_QUANTITY = 4;

        // Client
        public const byte CLEAR_CHAT = 10;
        public const byte HELP = 11;
        public const byte GUILD_OPEN_PAGE = 12;

        // Friend & Ignore List
        public const byte FRIEND_ADD = 13;
        public const byte FRIEND_REMOVE = 14;
        public const byte IGNORE_PLAYER = 15;
        public const byte UNIGNORE_PLAYER = 16;

        // Guild
        public const byte GUILD_CHAT = 17;
        public const byte GUILD_INVITE = 18;
        public const byte GUILD_KICK = 19;
        public const byte GUILD_LEAVE = 20;


        // Player Communication
        public const byte ME = 21;
        public const byte SAYTO = 22;
        public const byte WHISPER = 23;
        public const byte PING_PLAYER = 24;

        // Game
        public const byte JOIN_GAME = 25;
        public const byte OBSERVE_GAME = 26;
        public const byte RESUME = 27;

        // Misc
        public const byte TIME = 28;
        public const byte TRADE_PLAYER = 29;
        public const byte POSTEDTRADE = 30;

        //public const byte SHUTDOWN_SERVER = 19;

        ///
        /// -- End of CHAT COMMANDS
        ///

        // Guild Command Bytes
        public const byte GUILD_ACCEPT_INVITE = 31;
        public const byte GUILD_CREATE = 32;
        public const byte GUILD_PROMOTE = 33;
        public const byte GUILD_DEMOTE = 34;
        public const byte SAVE_GUILD_MESSAGE = 35;

        // ServerSystem Command Bytes
        public const byte SERVER_CHAT = 40;
        public const byte SERVER_COMMAND = 41;
        public const byte UPDATE_LOBBY_USERNAMES = 42;
        public const byte UPDATE_MATCH_USERNAMES = 43;
        public const byte UPDATE_MATCHES = 44;
        public const byte LOGIN_VALIDATION = 45;
        public const byte CRASH_PING = 46;

        // ClientSystem Command Bytes
        public const byte CHATROOM_JOIN = 50;
        public const byte CHATROOM_LEAVE = 51;
        public const byte LOBBY_CHAT = 52;
        public const byte MATCH_CHAT = 53;
        public const byte CLIENT_DISCONNECT = 54;
        public const byte LOBBY_JOIN = 55;
        public const byte LOBBY_LEAVE = 56;
        public const byte START_SCENARIO = 57;
        public const byte SAVE_ACCOUNTINFO = 58;

        // Match Commands
        public const byte MATCH_CREATE = 60;
        public const byte MATCH_JOIN = 61;
        public const byte MATCH_LEAVE = 62;
        public const byte MATCH_READY = 63;

        // Request Command Bytes
        public const byte REQUEST_COLLECTION = 70;
        public const byte REQUEST_ALLGUILDINFO = 71;
        public const byte REQUEST_GUILDINFO = 72;
        public const byte REQUEST_MATCH = 73;
        public const byte REQUEST_USERINFO = 74;
        public const byte REQUEST_FRIENDSIGNORELIST = 75;
        public const byte REQUEST_DECK_PACKETS = 76;
        public const byte REQUEST_POSTEDTRADES = 77;
        public const byte REQUEST_SCENARIOS = 78;
        public const byte REQUEST_LOBBYINFO = 79;

        // DeckBuilder Commands
        public const byte SAVE_AVATAR = 80;
        public const byte CREATE_AVATAR = 81;
        public const byte OPEN_PACK = 82;
        public const byte CARDWANT_DECREASE = 83;
        public const byte CARDWANT_INCREASE = 84;
        public const byte DECK_SAVE = 85;
        public const byte DECK_OPEN = 86;
        public const byte DECK_DELETE = 87;

        // Trade Command Bytes
        public const byte TRADE_CANCEL = 90;
        public const byte TRADE_ADD = 91;
        public const byte TRADE_REMOVE = 92;
        public const byte TRADE_ACCEPT = 93;
        public const byte TRADE_CLEAR = 94;
        public const byte TRADE_CONFIRM = 95;
        public const byte TRADE_CONFIRM_CANCEL = 96;
        public const byte TRADE_CONFIRM_ACCEPTED = 97;
        public const byte POSTEDTRADE_ACCEPT = 98;
        public const byte POSTEDTRADE_CREATE = 99;
        public const byte POSTEDTRADE_CANCEL = 100;

        // Collection Manager
        public const byte REDEEM_LOOTCARD = 101;
        public const byte REDEEM_PACK = 102;
        public const byte REDEEM_PROMOPACK = 103;

        // Combat
        public const byte LOAD_GAME = 110;
        public const byte LEAVE_GAME = 111;
        public const byte PLACE_CARD = 112;
        public const byte REDRAW_HAND = 113;
        public const byte UPDATE_AVATAR_HEALTH = 114;
        public const byte UPDATE_AVATAR_POWER = 115;
        public const byte DRAW_PHASE = 116;
        public const byte QUEST_PHASE = 117;
        public const byte READY_PHASE = 118;
        public const byte MAIN_PHASE = 119;
        public const byte UPDATE_CARD_STATS = 120;
        public const byte START_RAID = 121;
        public const byte QUEST_PHASE_COMBAT = 122;
        public const byte QUEST_COMPLETE = 123;
        public const byte QUEST_UPDATE_COMPLETED_LEVELS = 124;
        public const byte UPDATE_PLAYED_ABILITIES = 125;
        public const byte RAID_EXERT_CARD = 126;
        public const byte RAID_EXERT_AVATAR = 127;
        public const byte RAID_END_TURN = 128;
        public const byte RAID_ATTACKER_WON = 129;
        public const byte RAID_DEFENDER_WON = 130;
        public const byte RAID_DRAW = 131;
        public const byte RAID_TAKE_DAMAGE = 132;
        public const byte RAID_SHOW_OUTCOME = 133;
        public const byte RAID_DESTROY_CARD = 134;
        public const byte RAID_DAMAGE_CARD = 135;
        public const byte RAID_DAMAGE_AVATAR = 136;
        public const byte RAID_END = 137;
        public const byte GAME_END = 138;
        public const byte QUEST_ABILITY = 139;

        /*
         * Command Strings
         */

        public static byte CommandStringToByte(string Command)
        {
            string Value = DatatableReader.GetValue("commands", "command", Command, "index");

            if (string.IsNullOrEmpty(Value))
            {
                // Command doesn't exist, return a byte that doesn't exist.
                return byte.MaxValue;
            }

            return byte.Parse(Value);
        }
    }
}
