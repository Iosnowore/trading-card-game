﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.IO;

namespace TCGData.Services
{
    public static class DatatableReader
    {
        private const string DATATABLE_PATH = "datatables/";

        public static string GetValue(string Table, string ColumnHave, string ValueHave, string ColumnNeeded)
        {
            string TableFile = DATATABLE_PATH + Table + ".sdb";

            // We are reading a server datatable (or overriding client one)
            if (File.Exists(TableFile))
            {
                using (StreamReader Reader = new StreamReader(TableFile))
                {
                    string Value = GetValue(Reader, ColumnHave, ValueHave, ColumnNeeded);

                    if (!string.IsNullOrEmpty(Value))
                        return Value;
                }
            }
            else
            {
                byte[] TableByte = (byte[])Properties.Resources.ResourceManager.GetObject(Table);

                MemoryStream MemoryStream = new MemoryStream(TableByte);
                using (StreamReader Reader = new StreamReader(MemoryStream))
                {
                    string Value = GetValue(Reader, ColumnHave, ValueHave, ColumnNeeded);

                    if (!string.IsNullOrEmpty(Value))
                        return Value;
                }
            }


            return null;
        }

        private static string GetValue(StreamReader Reader, string ColumnHave, string ValueHave, string ColumnNeeded)
        {
            string Line;
            string[] LineData;

            Line = Reader.ReadLine();
            LineData = Line.Split('\t');

            int HaveIndex = -1;
            int NeedIndex = -1;

            for (int i = 0; i < LineData.Length; i++)
            {
                if (LineData[i].Equals(ColumnHave))
                {
                    HaveIndex = i;
                }
                else if (LineData[i].Equals(ColumnNeeded))
                {
                    NeedIndex = i;
                }
            }

            if (HaveIndex != -1 && NeedIndex != -1)
            {
                while ((Line = Reader.ReadLine()) != null)
                {
                    LineData = Line.Split('\t');

                    string[] Values = LineData[HaveIndex].Split(',');

                    foreach (string Value in Values)
                    {
                        if (Value.Equals(ValueHave))
                        {
                            return LineData[NeedIndex];
                        }
                    }
                }
            }
            return null;
        }

        public static string[] GetValues(string Table, string ColumnHave, string ValueHave, string ColumnNeeded)
        {
            // TODO: Implement
            return null;
        }

        public static string[] GetValues(string Table, string Column)
        {
            string TableFile = DATATABLE_PATH + Table + ".sdb";

            // We are reading a server datatable (or overriding client one)
            if (File.Exists(TableFile))
            {
                using (StreamReader Reader = new StreamReader(TableFile))
                    return GetValues(Reader, Column);
            }
            else
            {
                byte[] TableByte = (byte[])Properties.Resources.ResourceManager.GetObject(Table);

                MemoryStream MemoryStream = new MemoryStream(TableByte);
                using (StreamReader Reader = new StreamReader(MemoryStream))
                    return GetValues(Reader, Column);
            }
        }

        private static string[] GetValues(StreamReader Reader, string Column)
        {
            List<string> Values = new List<string>();

            string Line;
            string[] LineData;

            Line = Reader.ReadLine();
            LineData = Line.Split('\t');

            int IndexNeeded = -1;

            for (int i = 0; i < LineData.Length; i++)
            {
                if (LineData[i].Equals(Column))
                {
                    IndexNeeded = i;
                    break;
                }
            }

            if (IndexNeeded != -1)
            {
                while ((Line = Reader.ReadLine()) != null)
                {
                    LineData = Line.Split('\t');

                    Values.Add(LineData.Length > IndexNeeded ? LineData[IndexNeeded] : string.Empty);
                }
            }

            return Values.ToArray();
        }

        public static string GetPackValue(string PackId, int Column)
        {
            MemoryStream MemoryStream = new MemoryStream(Properties.Resources.packs);
            using (StreamReader Reader = new StreamReader(MemoryStream))
            {
                string Line;
                string[] LineData;

                Reader.ReadLine(); // Get past title row

                while ((Line = Reader.ReadLine()) != null)
                {
                    LineData = Line.Split('\t');

                    if (LineData[0].Equals(PackId))
                        return LineData[Column];
                }
                return null;
            }
        }

        public static void GetCardData()
        {
            MemoryStream MemoryStream = new MemoryStream(Properties.Resources.cards);
            using (StreamReader Reader = new StreamReader(MemoryStream))
            {
                string Line;
                string[] CardData;

                Reader.ReadLine(); // Get past title row

                // Store the datatypes of each column
                string[] DataTypes = Reader.ReadLine().Split('\t');

                while ((Line = Reader.ReadLine()) != null)
                {
                    CardData = Line.Split('\t');

                    for (int i = 0; i < DataTypes.Length; i++)
                        if (DataTypes[i].Equals("i") && string.IsNullOrEmpty(CardData[i]))
                            CardData[i] = "0";
                    Cards.AddCardToCardData(new CardData(int.Parse(CardData[0]), CardData[1], CardData[2], CardData[3], CardData[4], CardData[5], CardData[6], CardData[7], CardData[8], CardData[9], CardData[10], int.Parse(CardData[11]), int.Parse(CardData[12]), int.Parse(CardData[13]), int.Parse(CardData[14]), int.Parse(CardData[15])));
                }
            }
        }

        public static void GetLootCardData()
        {
            MemoryStream MemoryStream = new MemoryStream(Properties.Resources.loot);
            using (StreamReader Reader = new StreamReader(MemoryStream))
            {
                const string LootCardTitle = "Star Wars Galaxies Loot";

                string Line;
                string[] CardData;

                Reader.ReadLine(); // Get past title row

                while ((Line = Reader.ReadLine()) != null)
                {
                    CardData = Line.Split('\t');

                    int Id = int.Parse(CardData[0]);
                    string Title = Id > 0 ? LootCardTitle : null;

                    Cards.AddCardToLootCardData(new CardData(Id, CardData[1], CardData[2], null, CardData[3], Title, null, null, null, "loot", CardData[4], 0, 0, 0, 0, 0));
                }
            }
        }

        public static void GetPackData()
        {
            MemoryStream MemoryStream = new MemoryStream(Properties.Resources.packs);
            using (StreamReader Reader = new StreamReader(MemoryStream))
            {
                string Line;
                string[] CardData;

                Reader.ReadLine(); // Get past title row

                while ((Line = Reader.ReadLine()) != null)
                {
                    CardData = Line.Split('\t');
                    Cards.AddPackToPackData(new PackData(int.Parse(CardData[0]), CardData[1], CardData[2]));
                }
            }
        }

        public static void GetPromoPackData()
        {
            MemoryStream MemoryStream = new MemoryStream(Properties.Resources.promopacks);
            using (StreamReader Reader = new StreamReader(MemoryStream))
            {
                string Line;
                string[] CardData;

                Reader.ReadLine(); // Get past title row

                while ((Line = Reader.ReadLine()) != null)
                {
                    CardData = Line.Split('\t');
                    Cards.AddPromoPackToPromoPackData(new PackData(int.Parse(CardData[0]), CardData[1], CardData[2]));
                }
            }
        }
    }
}
