﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData.Packets;
using TCGData.Services;

namespace TCGData
{
    public static class Cards
    {
        private static readonly List<CardData> CardData = new List<CardData>();
        private static readonly List<CardData> LootCardData = new List<CardData>();
        private static readonly List<PackData> PackData = new List<PackData>();
        private static readonly List<PackData> PromoPackData = new List<PackData>();

        public static void LoadGameCards()
        {
            DatatableReader.GetCardData();
            DatatableReader.GetLootCardData();
            DatatableReader.GetPackData();
            DatatableReader.GetPromoPackData();
        }

        public static void AddCardToCardData(CardData Data)
        {
            CardData.Add(Data);
        }

        public static void AddCardToLootCardData(CardData Data)
        {
            LootCardData.Add(Data);
        }

        public static void AddPackToPackData(PackData Pack)
        {
            PackData.Add(Pack);
        }

        public static void AddPromoPackToPromoPackData(PackData PromoPack)
        {
            PromoPackData.Add(PromoPack);
        }

        public static CardData GetCardData(int cardId)
        {
            for (int i = 0; i < CardData.Count; i++)
                if (CardData[i].GetId() == cardId)
                    return CardData[i];
            return null;
        }

        public static CardData GetLootCardData(int cardId)
        {
            for (int i = 0; i < LootCardData.Count; i++)
                if (LootCardData[i].GetId() == cardId)
                    return LootCardData[i];
            return null;
        }

        public static PackData GetPackData(int PackId)
        {
            for (int i = 0; i < PackData.Count; i++)
                if (PackData[i].GetId() == PackId)
                    return PackData[i];
            return null;
        }

        public static PackData GetPromoPackData(int PromoPackId)
        {
            for (int i = 0; i < PromoPackData.Count; i++)
                if (PromoPackData[i].GetId() == PromoPackId)
                    return PromoPackData[i];
            return null;
        }

        public static bool IsCard(string Title)
        {
            foreach (CardData Data in CardData)
                if (Data.GetTitle().Equals(Title))
                    return true;
            return false;
        }

        public static bool IsLootCard(string Title)
        {
            foreach (CardData Data in LootCardData)
                if (Data.GetTitle().Equals(Title))
                    return true;
            return false;
        }

        public static int GetNumberOfCards()
        {
            return CardData.Count;
        }

        public static int GetNumberOfLootCards()
        {
            return LootCardData.Count;
        }

        public static int[] GetCardIdsGivenRarity(int[] CardIds, char RaritySymbol)
        {
            List<int> FilteredCardIds = new List<int>();

            foreach (int CardId in CardIds)
            {
                CardData CardData = GetCardData(CardId);
                if (CardData.GetCollectorInfo()[1] == RaritySymbol)
                    FilteredCardIds.Add(CardId);
            }
            return FilteredCardIds.ToArray();
        }

        public static int[] GetCardIdsGivenArchetype(int[] CardIds, string Archetype)
        {
            List<int> FilteredCardIds = new List<int>();

            foreach (int CardId in CardIds)
            {
                CardData CardData = GetCardData(CardId);
                if (CardData.GetArchetype().Equals(Archetype))
                    FilteredCardIds.Add(CardId);
            }
            return FilteredCardIds.ToArray();
        }

        public static int[] GetCardIdsFromSet(int Set)
        {
            int[] CardIds = GetCardIds();

            List<int> TypeCardIds = new List<int>();

            CardData CardData;

            for (int i = 0; i < CardIds.Length; i++)
            {
                CardData = GetCardData(CardIds[i]);
                int ThisSet = int.Parse(CardData.GetCollectorInfo()[0].ToString());

                if (ThisSet == Set)
                    TypeCardIds.Add(CardIds[i]);
            }

            return TypeCardIds.ToArray();
        }

        public static int GetTotalNumberOfCards(SortedDictionary<int, int> Cards)
        {
            int Total = 0;

            foreach (int CardId in Cards.Keys)
                Total += Cards[CardId];
            return Total;
        }

        public static int[] GetLootCardIdsFromSpecificSet(int Set, bool IncludePromotional)
        {
            int[] CardIds = GetLootCardIds();

            List<int> TypeCardIds = new List<int>();

            CardData CardData;

            for (int i = 0; i < CardIds.Length; i++)
            {
                CardData = GetLootCardData(CardIds[i]);

                string CollectorInfo = CardData.GetCollectorInfo();

                if (string.IsNullOrEmpty(CollectorInfo))
                    continue;

                int ThisSet = int.Parse(CollectorInfo[0].ToString());
                if (ThisSet == Set && (IncludePromotional || !CardData.GetCollectorInfo().Contains("P")))
                {
                    TypeCardIds.Add(CardIds[i]);
                }
            }
            return TypeCardIds.ToArray();
        }

        public static int[] GetCardIds()
        {
            int[] ids = new int[CardData.Count];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = CardData[i].GetId();
            return ids;
        }

        public static int[] GetLootCardIds()
        {
            int[] ids = new int[LootCardData.Count];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = LootCardData[i].GetId();
            return ids;
        }

        public static int[] GetPackIds()
        {
            int[] ids = new int[PackData.Count];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = PackData[i].GetId();
            return ids;
        }
        public static int[] GetPromoPackIds()
        {
            int[] ids = new int[PromoPackData.Count];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = PromoPackData[i].GetId();
            return ids;
        }


        public static int[] GetCardIdsThatContainsKeyword(string Keyword, bool OnlyOwnedCards, int[] CardIds, bool[] SearchOptions)
        {
            List<int> KeywordIds = new List<int>();
            Keyword = Keyword.ToLower();

            if (OnlyOwnedCards)
            {
                CardData Data;

                for (int i = 0; i < CardIds.Length; i++)
                {
                    Data = GetCardData(CardIds[i]);

                    if (string.IsNullOrEmpty(Keyword) || SearchOptions[0] && Data.GetTitle().ToLower().Contains(Keyword) || SearchOptions[2] && Data.GetDescription().ToLower().Contains(Keyword) || SearchOptions[4] && Data.GetLore().ToLower().Contains(Keyword))
                        KeywordIds.Add(Data.GetId());
                }
            }
            else
            {
                for (int i = 0; i < CardData.Count; i++)
                    if (string.IsNullOrEmpty(Keyword) || SearchOptions[0] && CardData[i].GetTitle().ToLower().Contains(Keyword) || SearchOptions[2] && CardData[i].GetDescription().ToLower().Contains(Keyword) || SearchOptions[4] && CardData[i].GetLore().ToLower().Contains(Keyword))
                        KeywordIds.Add(CardData[i].GetId());
            }
            return KeywordIds.ToArray();
        }

        public static int[] GetLootCardIdsThatContainsKeyword(string Keyword, bool OnlyOwnedCards, CollectionPacket OwnedCollection, bool Redeemed)
        {
            List<int> KeywordIds = new List<int>();

            if (OnlyOwnedCards)
            {
                CardData Data;
                SortedDictionary<int, int> OwnedCards = Redeemed ? OwnedCollection.GetRedeemed() : OwnedCollection.GetRedeemables();

                foreach (int CardId in OwnedCards.Keys)
                {
                    Data = GetLootCardData(CardId);

                    if (Data.GetTitle().ToLower().Contains(Keyword.ToLower()) || Data.GetDescription().ToLower().Contains(Keyword.ToLower()))
                        KeywordIds.Add(Data.GetId());
                }
            }
            else
            {
                for (int i = 0; i < LootCardData.Count; i++)
                    if (LootCardData[i].GetTitle().ToLower().Contains(Keyword.ToLower()) || LootCardData[i].GetDescription().ToLower().Contains(Keyword.ToLower()))
                        KeywordIds.Add(LootCardData[i].GetId());
            }
            return KeywordIds.ToArray();
        }
    }
}
