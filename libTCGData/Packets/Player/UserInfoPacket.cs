﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData.Packets.Player
{
    [Serializable]
    public class UserInfoPacket
    {
        private readonly string Games;

        private readonly int Avatar;
        private readonly string Guild;
        private readonly string Username;

        private readonly string Biography;
        private readonly string HomePage;
        private readonly string InstantMessenger;

        public UserInfoPacket(string Username, string Games, int Avatar, string Biography, string Guild, string HomePage, string InstantMessenger)
        {
            this.Username = Username;
            this.Games = Games;
            this.Avatar = Avatar;
            this.Biography = Biography;
            this.Guild = Guild;
            this.HomePage = HomePage;
            this.InstantMessenger = InstantMessenger;
        }

        public string GetGames()
        {
            return Games;
        }

        public int GetAvatar()
        {
            return Avatar;
        }

        public string GetBiography()
        {
            return Biography;
        }

        public string GetGuild()
        {
            return Guild;
        }

        public string GetUsername()
        {
            return Username;
        }

        public string GetHomePage()
        {
            return HomePage;
        }

        public string GetInstantMessenger()
        {
            return InstantMessenger;
        }
    }
}
