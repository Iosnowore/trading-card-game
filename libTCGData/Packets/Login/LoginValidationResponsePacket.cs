﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using static TCGData.LoginPacketResponses;

namespace TCGData.Packets
{
    [Serializable]
    public class LoginValidationResponsePacket
    {
        private readonly Responses Response;

        private readonly int UserId;

        private readonly string Username;

        private readonly string GuildName;
        private readonly string GuildMessage;

        private readonly SortedDictionary<string, int> LeaderboardInfo;

        public LoginValidationResponsePacket(Responses Response, int UserId, string Username, SortedDictionary<string, int> LeaderboardInfo, string GuildName, string GuildMessage)
        {
            this.Response = Response;
            this.UserId = UserId;
            this.Username = Username;
            this.LeaderboardInfo = LeaderboardInfo;
            this.GuildName = GuildName;
            this.GuildMessage = GuildMessage;
        }

        public Responses GetResponse()
        {
            return Response;
        }

        public int GetUserId()
        {
            return UserId;
        }

        public string GetUsername()
        {
            return Username;
        }

        public SortedDictionary<string, int> GetLeaderboardInfo()
        {
            return LeaderboardInfo;
        }

        public string GetGuildName()
        {
            return GuildName;
        }

        public string GetGuildMessage()
        {
            return GuildMessage;
        }
    }
}
