﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData.Packets.Guild
{
    [Serializable]
    public class GuildInfoPacket
    {
        private readonly string Name;
        private readonly int CreationDate;
        private readonly string Leader;
        private readonly string Message;

        private readonly string[] Officers;
        private readonly string[] Members;

        private readonly int[] TotalRatings;
        private readonly int[] AverageRatings;

        private readonly int Rank;

        public GuildInfoPacket(string Name, int CreationDate, string Leader, string Message, string[] Officers, string[] Members, int[] TotalRatings, int[] AverageRatings, int Rank)
        {
            this.Name = Name;
            this.CreationDate = CreationDate;
            this.Leader = Leader;
            this.Message = Message;

            this.Officers = Officers;
            this.Members = Members;

            this.TotalRatings = TotalRatings;
            this.AverageRatings = AverageRatings;

            this.Rank = Rank;
        }

        public string GetName()
        {
            return Name;
        }

        public int GetCreationDate()
        {
            return CreationDate;
        }

        public string GetLeader()
        {
            return Leader;
        }

        public string GetMessage()
        {
            return Message;
        }

        public string[] GetOfficers()
        {
            return Officers;
        }

        public string[] GetMembers()
        {
            return Members;
        }

        public int[] GetTotalRatings()
        {
            return TotalRatings;
        }

        public int[] GetAverageRatings()
        {
            return AverageRatings;
        }

        public int GetRank()
        {
            return Rank;
        }
    }
}
