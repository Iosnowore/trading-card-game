﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace TCGData.Packets.Trade
{
    [Serializable]
    public class ConfirmTradePacket
    {
        private readonly SortedDictionary<int, int> YouGetCards;
        private readonly SortedDictionary<int, int> TheyGetCards;

        public ConfirmTradePacket(SortedDictionary<int, int> YouGetCards, SortedDictionary<int, int> TheyGetCards)
        {
            this.YouGetCards = YouGetCards;
            this.TheyGetCards = TheyGetCards;
        }

        public SortedDictionary<int, int> GetYouGetCards()
        {
            return YouGetCards;
        }

        public SortedDictionary<int, int> GetTheyGetCards()
        {
            return TheyGetCards;
        }
    }
}
