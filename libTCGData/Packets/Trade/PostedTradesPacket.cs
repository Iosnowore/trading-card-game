﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData.Packets.Trade
{
    [Serializable]
    public class PostedTradesPacket
    {
        private readonly PostedTradePacket[] PostedTrades;

        public PostedTradesPacket(PostedTradePacket[] PostedTrades)
        {
            this.PostedTrades = PostedTrades;
        }

        public PostedTradePacket GetPostedTrade(int Index)
        {
            return PostedTrades[Index];
        }

        public int GetNumberOfPostedTrades()
        {
            return PostedTrades.Length;
        }
    }
}
