﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData.Packets
{
    [Serializable]
    public class MatchPacket
    {
        private readonly int MatchId;

        private readonly bool AllowObservers;
        private readonly bool FriendsOnly;
        private readonly bool LightVsDark;

        private readonly int MaxPlayers;

        private readonly string GuildOption;
        private readonly string Structure;
        private readonly string Password;
        private readonly string PlayFormat;
        private readonly string TimeLimit;
        private readonly string Title;
        private readonly string Type;

        private readonly string[] Players;

        public MatchPacket(int MatchId, bool AllowObservers, bool FriendsOnly, bool LightVsDark, int MaxPlayers, string GuildOption, string Password, string PlayFormat, string Structure, string TimeLimit, string Title, string Type, string[] Players)
        {
            this.MatchId = MatchId;

            this.AllowObservers = AllowObservers;
            this.FriendsOnly = FriendsOnly;
            this.LightVsDark = LightVsDark;

            this.MaxPlayers = MaxPlayers;

            this.GuildOption = GuildOption;
            this.Password = Password;
            this.PlayFormat = PlayFormat;
            this.Structure = Structure;
            this.TimeLimit = TimeLimit;
            this.Title = Title;
            this.Type = Type;

            this.Players = Players;
        }

        public int GetMatchId()
        {
            return MatchId;
        }

        public bool GetAllowObservers()
        {
            return AllowObservers;
        }

        public bool GetFriendsOnly()
        {
            return FriendsOnly;
        }

        public bool GetLightVsDark()
        {
            return LightVsDark;
        }

        public int GetMaxPlayers()
        {
            return MaxPlayers;
        }

        public string GetGuildOption()
        {
            return GuildOption;
        }

        public string GetPassword()
        {
            return Password;
        }

        public string GetPlayFormat()
        {
            return PlayFormat;
        }

        public string GetStructure()
        {
            return Structure;
        }

        public string GetTimeLimit()
        {
            return TimeLimit;
        }

        public string GetTitle()
        {
            return Title;
        }

        public new string GetType()
        {
            return Type;
        }

        public string[] GetPlayers()
        {
            return Players;
        }
    }
}
