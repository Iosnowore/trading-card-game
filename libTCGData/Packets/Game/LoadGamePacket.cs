﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData.Packets.Game
{
    [Serializable]
    public class LoadGamePacket
    {
        private readonly int AvatarId;
        private readonly int EnemyAvatarId;

        private readonly string Username;
        private readonly string EnemyUsername;

        private readonly int[] HandCardIds;

        private readonly int QuestId;
        private readonly int EnemyQuestId;

        private readonly int DeckQty;
        private readonly int EnemyDeckQty;

        private readonly int HandQty;
        private readonly int EnemyHandQty;

        private readonly int ScenarioId;

        public LoadGamePacket(int AvatarId, int EnemyAvatarId, string Username, string EnemyUsername, int[] HandCardIds, int QuestId, int EnemyQuestId, int DeckQty, int EnemyDeckQty, int HandQty, int EnemyHandQty, int ScenarioId)
        {
            this.AvatarId = AvatarId;
            this.EnemyAvatarId = EnemyAvatarId;

            this.Username = Username;
            this.EnemyUsername = EnemyUsername;

            this.HandCardIds = HandCardIds;

            this.QuestId = QuestId;
            this.EnemyQuestId = EnemyQuestId;

            this.DeckQty = DeckQty;
            this.EnemyDeckQty = EnemyDeckQty;

            this.HandQty = HandQty;
            this.EnemyHandQty = EnemyHandQty;

            this.ScenarioId = ScenarioId;
        }

        public int GetAvatarId()
        {
            return AvatarId;
        }

        public int GetEnemyAvatarId()
        {
            return EnemyAvatarId;
        }

        public string GetUsername()
        {
            return Username;
        }

        public string GetEnemyUsername()
        {
            return EnemyUsername;
        }

        public int[] GetHand()
        {
            return HandCardIds;
        }

        public int GetQuestId()
        {
            return QuestId;
        }

        public int GetEnemyQuestId()
        {
            return EnemyQuestId;
        }

        public int GetDeckQty()
        {
            return DeckQty;
        }

        public int GetEnemyDeckQty()
        {
            return EnemyDeckQty;
        }

        public int GetHandQty()
        {
            return HandQty;
        }

        public int GetEnemyHandQty()
        {
            return EnemyHandQty;
        }

        public int GetScenarioId()
        {
            return ScenarioId;
        }
    }
}
