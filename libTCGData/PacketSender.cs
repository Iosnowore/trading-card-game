﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TCGData
{
    [Serializable]
    public class PacketSender
    {
        private readonly object Packet;
        private readonly int SenderId;
        private readonly byte Command;

        public PacketSender(int SenderId, byte Command, object Packet)
        {
            this.SenderId = SenderId;
            this.Command = Command;
            this.Packet = Packet;
        }

        public PacketSender(byte[] packetBytes)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(packetBytes))
                {
                    ms.Position = 0;
                    PacketSender p = (PacketSender)new BinaryFormatter().Deserialize(ms);
                    SenderId = p.SenderId;
                    Command = p.Command;
                    Packet = p.Packet;
                }
            }
            catch (SerializationException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public byte[] ToBytes()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public object GetPacket()
        {
            return Packet;
        }

        public byte GetCommand()
        {
            return Command;
        }

        public int GetSenderId()
        {
            return SenderId;
        }
    }
}
