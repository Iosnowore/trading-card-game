﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData
{
    [Serializable]
    public class CardData
    {
        private readonly int Id, Cost, Attack, Defense, Bonus, HealthOrLevel;
        private readonly string Title, Description, Lore, Image, Type, Traits, Restriction, Keyword, Archetype, CollectorInfo;

        public CardData(int Id, string Title, string Description, string Lore, string Image, string Type, string Traits, string Restriction, string Keyword, string Archetype, string CollectorInfo, int Cost, int Attack, int Defense, int Bonus, int HealthOrLevel)
        {
            this.Id = Id;
            this.Title = Title;
            this.Description = Description;
            this.Lore = Lore;
            this.Image = Image;
            this.Type = Type;
            this.Traits = Traits;
            this.Restriction = Restriction;
            this.Keyword = Keyword;
            this.Archetype = Archetype;
            this.CollectorInfo = CollectorInfo;
            this.Cost = Cost;
            this.Attack = Attack;
            this.Defense = Defense;
            this.Bonus = Bonus;
            this.HealthOrLevel = HealthOrLevel;
        }

        public int GetId()
        {
            return Id;
        }

        public string GetTitle()
        {
            return Title;
        }

        public string GetDescription()
        {
            return Description;
        }

        public string GetLore()
        {
            return Lore;
        }

        public string GetImage()
        {
            return Image;
        }

        public new string GetType()
        {
            return Type;
        }

        public string GetTraits()
        {
            return Traits;
        }

        public string GetRestriction()
        {
            return Restriction;
        }

        public string GetKeyword()
        {
            return Keyword;
        }

        public int GetCost()
        {
            return Cost;
        }

        public string GetArchetype()
        {
            return Archetype;
        }

        public string GetCollectorInfo()
        {
            return CollectorInfo;
        }

        public int GetAttack()
        {
            return Attack;
        }

        public int GetDefense()
        {
            return Defense;
        }

        public int GetBonus()
        {
            return Bonus;
        }

        public int GetHealthOrLevel()
        {
            return HealthOrLevel;
        }
    }
}
