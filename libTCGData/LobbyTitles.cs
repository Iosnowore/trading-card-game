﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace TCGData
{
    public static class LobbyTitles
    {
        public const string CASUAL_GAMES = "Casual Games";
        public const string MOS_EISLEY = "Mos Eisley";
        public const string MOS_ESPA = "Mos Espa";
        public const string TOSCHE_STATION = "Tosche Station";
        public const string TOURNAMENT = "Tournament";
        public const string TRADE = "Trade";

        public static readonly string[] DEFAULT_LOBBY_TITLES = {
            CASUAL_GAMES,
            MOS_EISLEY,
            MOS_ESPA,
            TOSCHE_STATION,
            TOURNAMENT,
            TRADE
        };
    }
}
