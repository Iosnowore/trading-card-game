﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace TCGData
{
    [Serializable]
    public class PlaymatCard
    {
        private readonly int CardId;

        private int Attack;
        private int Damage;
        private int Defense;
        private int Health;

        private bool Exerted;

        public PlaymatCard(int CardId, int Attack, int Damage, int Defense, int Health)
        {
            this.CardId = CardId;

            this.Attack = Attack;
            this.Damage = Damage;
            this.Defense = Defense;
            this.Health = Health;
        }

        public int GetCardId()
        {
            return CardId;
        }

        public void SetAttack(int Attack)
        {
            this.Attack = Attack;
        }

        public int GetAttack()
        {
            return Attack;
        }

        public void SetDamage(int Damage)
        {
            this.Damage = Damage;
        }

        public int GetDamage()
        {
            return Damage;
        }

        public void SetDefense(int Defense)
        {
            this.Defense = Defense;
        }

        public int GetDefense()
        {
            return Defense;
        }

        public void SetHealth(int Health)
        {
            this.Health = Health;
        }

        public int GetHealth()
        {
            return Health;
        }

        public void SetExerted(bool Exerted)
        {
            this.Exerted = Exerted;
        }

        public bool GetExerted()
        {
            return Exerted;
        }
    }
}
