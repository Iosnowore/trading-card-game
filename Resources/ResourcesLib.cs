﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources.Res;
using Resources.Res.Avatars;
using Resources.Res.Buttons;
using Resources.Res.Graphicsitems;
using Resources.Res.Icons;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Resources
{
    public static class ResourcesLib
    {
        public static readonly ImageList PLAYER_LIST = new ImageList();
        public static readonly ImageList BUTTON_IMAGES = new ImageList();
        public static readonly ImageList ICONS_IMAGES = new ImageList();

        public static void LoadImageList()
        {
            PLAYER_LIST.ColorDepth = ColorDepth.Depth32Bit;
            BUTTON_IMAGES.ColorDepth = ColorDepth.Depth32Bit;
            ICONS_IMAGES.ColorDepth = ColorDepth.Depth32Bit;

            // Load playerlist buttons
            PLAYER_LIST.Images.Add(string.Empty, new Bitmap(16, 16, PixelFormat.Format32bppRgb));
            PLAYER_LIST.Images.Add("bronze", GetImageFromPlayerlist("bronze"));
            PLAYER_LIST.Images.Add("gold", GetImageFromPlayerlist("gold"));
            PLAYER_LIST.Images.Add("silver", GetImageFromPlayerlist("silver"));
            PLAYER_LIST.Images.Add("staff", GetImageFromPlayerlist("staff"));
            PLAYER_LIST.Images.Add("ingame_bronze", GetImageFromPlayerlist("ingame_bronze"));
            PLAYER_LIST.Images.Add("ingame_gold", GetImageFromPlayerlist("ingame_gold"));
            PLAYER_LIST.Images.Add("ingame_silver", GetImageFromPlayerlist("ingame_silver"));
            PLAYER_LIST.Images.Add("ingame_staff", GetImageFromPlayerlist("ingame_staff"));
            PLAYER_LIST.Images.Add("deckbuilder", GetImageFromPlayerlist("deckbuilder"));

            // Load button images
            BUTTON_IMAGES.Images.Add(string.Empty, new Bitmap(16, 16, PixelFormat.Format32bppRgb));
            BUTTON_IMAGES.Images.Add("dropdown_checkbox_off", GetImageFromButtons("dropdown_checkbox_off"));
            BUTTON_IMAGES.Images.Add("dropdown_checkbox_on", GetImageFromButtons("dropdown_checkbox_on"));

            // Load icons
            ICONS_IMAGES.Images.Add(string.Empty, new Bitmap(16, 16, PixelFormat.Format32bppRgb));
            ICONS_IMAGES.Images.Add("lock", GetImageFromIcons("lock"));
            ICONS_IMAGES.Images.Add("no", GetImageFromIcons("no"));
            ICONS_IMAGES.Images.Add("search", GetImageFromIcons("search"));
            ICONS_IMAGES.Images.Add("yes", GetImageFromIcons("yes"));
        }

        public static Image GetImageFromAvatars(string image)
        {
            return (Image)Avatars.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromBackgrounds(string image)
        {
            return (Image)Backgrounds.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromBinder(string image)
        {
            return (Image)Binder.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromBorders(string image)
        {
            return (Image)Borders.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromButtons(string image)
        {
            return (Image)Buttons.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromCursors(string image)
        {
            return (Image)Res.Cursors.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromDialogs(string image)
        {
            return (Image)Dialogs.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromDraft(string image)
        {
            return (Image)Draft.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromFrames(string image)
        {
            return (Image)Frames.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromGamescreen(string image)
        {
            return (Image)Gamescreen.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromGuild(string image)
        {
            return (Image)Guild.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromIcons(string image)
        {
            return (Image)Icons.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromMapScreen(string image)
        {
            return (Image)Map_Screen.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromMatch(string image)
        {
            return (Image)Match.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromMedallions(string image)
        {
            return (Image)Medallions.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromNavigation(string image)
        {
            return (Image)Navigation.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromPictures(string image)
        {
            return (Image)Pictures.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromPlayerlist(string image)
        {
            return (Image)Playerlist.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromPreferences(string image)
        {
            return (Image)Preferences.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromScrollbar(string image)
        {
            return (Image)Scrollbar.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromStats(string image)
        {
            return (Image)Stats.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromTutorialImages(string image)
        {
            return (Image)Tutorial_Images.ResourceManager.GetObject(image);
        }

        public static Image GetImageFromTutorialNav(string image)
        {
            return (Image)Tutorial_Nav.ResourceManager.GetObject(image);
        }

        public static byte[] GetCustomFont()
        {
            return Res.Font.vera;
        }
    }
}
