﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace String.Res {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Tutorial {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Tutorial() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("String.Res.Tutorial", typeof(Tutorial).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Learn about trading card games, the playmat, your
        ///avatar, playing a card, and how to win the game..
        /// </summary>
        internal static string description {
            get {
                return ResourceManager.GetString("description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn the following things about setting up a game.
        ///
        ///Learn how to start a game
        ///
        ///Learn about the playmat
        ///
        ///Learn about redrawing your hand
        ///
        ///Learn about playing cards
        ///
        ///Estimated time for this tutorial: 4 minutes..
        /// </summary>
        internal static string description1 {
            get {
                return ResourceManager.GetString("description1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, we&apos;ll walk you through a couple of guided turns and you&apos;ll
        ///learn about turn sequence and strategy.
        ///
        ///Estimated time for this tutorial: 5 minutes..
        /// </summary>
        internal static string description10 {
            get {
                return ResourceManager.GetString("description10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll play an actual guided game against a computer controlled
        ///opponent. Both decks are provided, along with helpful context sensitive tips
        ///while you play.
        ///
        ///Estimated time for this tutorial: 15 minutes..
        /// </summary>
        internal static string description11 {
            get {
                return ResourceManager.GetString("description11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn about all the parts of your avatar card.
        ///
        ///Learn about avatar cards
        ///
        ///Learn about archetypes
        ///
        ///Learn how to win the game by combat
        ///
        ///Estimated time for this tutorial: 2 minutes..
        /// </summary>
        internal static string description2 {
            get {
                return ResourceManager.GetString("description2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn how to complete quests and gain power.
        ///
        ///Learn about quest cards
        ///
        ///Learn about levels
        ///
        ///Learn about rewards
        ///
        ///Learn about gaining power
        ///
        ///Learn how to win the game by questing
        ///
        ///Estimated time for this tutorial: 2 minutes..
        /// </summary>
        internal static string description3 {
            get {
                return ResourceManager.GetString("description3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn how units are played. You&apos;ll also learn about keywords.
        ///
        ///Learn about unit cards
        ///
        ///Learn about keywaords
        ///
        ///Estimated time for this tutorial: 2 minutes..
        /// </summary>
        internal static string description4 {
            get {
                return ResourceManager.GetString("description4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn how to apply abilities and create level tokens.
        ///
        ///Learn about ability cards
        ///
        ///Learn how to apply an ability to a quest
        ///
        ///Learn about level tokens
        ///
        ///Estimated time for this tutorial: 2 minutes..
        /// </summary>
        internal static string description5 {
            get {
                return ResourceManager.GetString("description5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn about items, traits, restrictions, and activated actions.
        ///
        ///Learn about item cards
        ///
        ///Learn about traits
        ///
        ///Learn about restrictions
        ///
        ///Learn about activated actions
        ///
        ///Estimated time for this tutorial: 3 minutes..
        /// </summary>
        internal static string description6 {
            get {
                return ResourceManager.GetString("description6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn about tactic cards, conditions for playing them,
        ///and the basics of combat.
        ///
        ///Learn about tactic cards
        ///
        ///Learn about conditions
        ///
        ///Learn about combat
        ///
        ///Estimated time for this tutorial: 4 minutes..
        /// </summary>
        internal static string description7 {
            get {
                return ResourceManager.GetString("description7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn how to raid with units.
        ///
        ///Learn how to raid with units
        ///
        ///Learn how to raid against an avatar
        ///
        ///Learn how to resolve combat
        ///
        ///Learn how to take damage
        ///
        ///Estimated time for this tutorial: 3 minutes..
        /// </summary>
        internal static string description8 {
            get {
                return ResourceManager.GetString("description8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In this tutorial, you&apos;ll learn how to use your avatar to attack both units and avatars.
        ///
        ///Learn how to attack units
        ///
        ///Learn how to attack an avatar
        ///
        ///Estimated time for this tutorial: 3 minutes..
        /// </summary>
        internal static string description9 {
            get {
                return ResourceManager.GetString("description9", resourceCulture);
            }
        }
    }
}
