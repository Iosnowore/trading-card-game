﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace String.Res {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Help {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Help() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("String.Res.Help", typeof(Help).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string CasualLobby {
            get {
                return ResourceManager.GetString("CasualLobby", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to the Collection Manager, this is where
        ///all your play cards, loot cards, and packs are.
        ///
        ///Click the Jump To dropdown menu to select what
        ///view you want to see. To flip through pages, click
        ///on the next and previous arrows on the right and left
        ///side..
        /// </summary>
        internal static string Collection {
            get {
                return ResourceManager.GetString("Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string Deckbuilder {
            get {
                return ResourceManager.GetString("Deckbuilder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string ExcessiveAttack {
            get {
                return ResourceManager.GetString("ExcessiveAttack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to , welcome to the Star Wars Galaxies Trading Card Game.
        ///
        ///TUTORIALS/SCENARIOS should be your first stop to learn about the game. You&apos;ll also find single-
        ///player Scenarios to play games against AI opponents and earn reward cards! Skirmish lets you test
        ///your decks against four AI opponents with starter decks.
        ///
        ///DECK BUILDER is a powerful tool that helps you create and customize your decks. You&apos;ll find the Deck
        ///Building Helper here and you can also make a Custom Avatar!
        ///
        ///CASUAL GAMES leads to several gam [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MainScreen {
            get {
                return ResourceManager.GetString("MainScreen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string MultiplayerGames {
            get {
                return ResourceManager.GetString("MultiplayerGames", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string QuestFilled {
            get {
                return ResourceManager.GetString("QuestFilled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string Trade {
            get {
                return ResourceManager.GetString("Trade", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to the Trade Lobby, where residents
        ///of the Galaxy can get together and trade
        ///assets.
        ///
        ///To trade with another player, right-click on
        ///his or her username at the top right and select
        ///&quot;Trade.&quot; That person then has the option to
        ///accept or reject your trade offer..
        /// </summary>
        internal static string TradeLobby {
            get {
                return ResourceManager.GetString("TradeLobby", resourceCulture);
            }
        }
    }
}
