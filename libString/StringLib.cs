﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


using String.Res;

namespace String
{
    public static class StringLib
    {
        public static string GetString(string title)
        {
            string[] index = title.Split(':');

            switch (index[0])
            {
                case "help":
                    return Help.ResourceManager.GetString(index[1]);
                case "map":
                    return Map.ResourceManager.GetString(index[1]);
                case "tutorial":
                    return Tutorial.ResourceManager.GetString(index[1]);
                case "avatar_creator":
                    return AvatarCreator.ResourceManager.GetString(index[1]);
                case "guild":
                    return Guild.ResourceManager.GetString(index[1]);
                case "commands":
                    return Commands.ResourceManager.GetString(index[1]);
                case "scenarios":
                    return Scenarios.ResourceManager.GetString(index[1]);
                default:
                    return null;
            }
        }
    }
}
