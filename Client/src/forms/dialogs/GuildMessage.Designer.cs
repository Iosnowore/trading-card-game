﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class GuildMessage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MessageTextBox = new System.Windows.Forms.RichTextBox();
            this.LastUpdatedLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.BackColor = System.Drawing.Color.Black;
            this.MessageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageTextBox.ForeColor = System.Drawing.Color.White;
            this.MessageTextBox.Location = new System.Drawing.Point(9, 9);
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.ReadOnly = true;
            this.MessageTextBox.Size = new System.Drawing.Size(382, 85);
            this.MessageTextBox.TabIndex = 66;
            this.MessageTextBox.Text = "";
            // 
            // LastUpdatedLabel
            // 
            this.LastUpdatedLabel.AutoSize = true;
            this.LastUpdatedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastUpdatedLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.LastUpdatedLabel.Location = new System.Drawing.Point(133, 100);
            this.LastUpdatedLabel.Name = "LastUpdatedLabel";
            this.LastUpdatedLabel.Size = new System.Drawing.Size(135, 16);
            this.LastUpdatedLabel.TabIndex = 67;
            this.LastUpdatedLabel.Text = "Last Updated: N/A";
            // 
            // GuildMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.LastUpdatedLabel);
            this.Controls.Add(this.MessageTextBox);
            this.DoubleBuffered = true;
            this.Name = "GuildMessage";
            this.Size = new System.Drawing.Size(400, 122);
            this.Load += new System.EventHandler(this.GuildMessage_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GuildMessage_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GuildMessage_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox MessageTextBox;
        private System.Windows.Forms.Label LastUpdatedLabel;
    }
}
