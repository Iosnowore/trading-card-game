﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class Delivery : UserControl
    {
        public Delivery(SortedDictionary<int, int> Cards, int TransactionId, bool IsPack)
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(CloseButton);
            TransactionListView.Items.Add("Transaction ID #" + TransactionId);
            TransactionIdLabel.Text = "Transaction ID: #" + TransactionId;

            int[] CardIds = new int[Cards.Count];
            Cards.Keys.CopyTo(CardIds, 0);

            int TotalQuantity = 0;
            Card Card;

            for (int i = 0; i < Cards.Count; i++)
            {
                if (CardIds[i] < 10000)
                {
                    if (IsPack)
                        Card = new Card(TCGData.Cards.GetPackData(CardIds[i]), Card.SIZE.MEDIUM);
                    else
                        Card = new Card(TCGData.Cards.GetCardData(CardIds[i]), Card.SIZE.MEDIUM);
                    CardsFlowLayoutPanel.Controls.Add(Card);
                }
                else
                {
                    Card = new Card(TCGData.Cards.GetLootCardData(CardIds[i] - 10000), Card.SIZE.MEDIUM);
                    CardsFlowLayoutPanel.Controls.Add(Card);
                }
                Cards.TryGetValue(CardIds[i], out int Quantity);
                Card.SetQuantityLabel(Quantity);
                TotalQuantity += Quantity;
            }
            NumberOfItemsLabel.Text = "Number of items: " + TotalQuantity;
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            Parent.Dispose();
        }
    }
}
