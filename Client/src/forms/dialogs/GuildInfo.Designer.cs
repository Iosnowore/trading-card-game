﻿namespace TradingCardGame.forms.dialogs
{
    partial class GuildInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GuildInfo));
            this.panel1 = new System.Windows.Forms.Panel();
            this.settingsLabel = new System.Windows.Forms.Label();
            this.membersLabel = new System.Windows.Forms.Label();
            this.overviewLabel = new System.Windows.Forms.Label();
            this.settingsButton = new System.Windows.Forms.PictureBox();
            this.membersButton = new System.Windows.Forms.PictureBox();
            this.overviewButton = new System.Windows.Forms.PictureBox();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.settingsLabel);
            this.panel1.Controls.Add(this.membersLabel);
            this.panel1.Controls.Add(this.overviewLabel);
            this.panel1.Controls.Add(this.settingsButton);
            this.panel1.Controls.Add(this.membersButton);
            this.panel1.Controls.Add(this.overviewButton);
            this.panel1.Location = new System.Drawing.Point(21, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 475);
            this.panel1.TabIndex = 64;
            // 
            // settingsLabel
            // 
            this.settingsLabel.AutoSize = true;
            this.settingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.settingsLabel.Location = new System.Drawing.Point(46, 272);
            this.settingsLabel.Name = "settingsLabel";
            this.settingsLabel.Size = new System.Drawing.Size(56, 16);
            this.settingsLabel.TabIndex = 5;
            this.settingsLabel.Text = "Settings";
            // 
            // membersLabel
            // 
            this.membersLabel.AutoSize = true;
            this.membersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.membersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.membersLabel.Location = new System.Drawing.Point(42, 176);
            this.membersLabel.Name = "membersLabel";
            this.membersLabel.Size = new System.Drawing.Size(65, 16);
            this.membersLabel.TabIndex = 4;
            this.membersLabel.Text = "Members";
            // 
            // overviewLabel
            // 
            this.overviewLabel.AutoSize = true;
            this.overviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overviewLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.overviewLabel.Location = new System.Drawing.Point(42, 80);
            this.overviewLabel.Name = "overviewLabel";
            this.overviewLabel.Size = new System.Drawing.Size(64, 16);
            this.overviewLabel.TabIndex = 3;
            this.overviewLabel.Text = "Overview";
            // 
            // settingsButton
            // 
            this.settingsButton.Location = new System.Drawing.Point(31, 195);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(87, 74);
            this.settingsButton.TabIndex = 2;
            this.settingsButton.TabStop = false;
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // membersButton
            // 
            this.membersButton.Location = new System.Drawing.Point(31, 99);
            this.membersButton.Name = "membersButton";
            this.membersButton.Size = new System.Drawing.Size(87, 74);
            this.membersButton.TabIndex = 1;
            this.membersButton.TabStop = false;
            this.membersButton.Click += new System.EventHandler(this.membersButton_Click);
            // 
            // overviewButton
            // 
            this.overviewButton.Location = new System.Drawing.Point(31, 3);
            this.overviewButton.Name = "overviewButton";
            this.overviewButton.Size = new System.Drawing.Size(87, 74);
            this.overviewButton.TabIndex = 0;
            this.overviewButton.TabStop = false;
            this.overviewButton.Click += new System.EventHandler(this.overviewButton_Click);
            // 
            // MainPanel
            // 
            this.MainPanel.Location = new System.Drawing.Point(177, 38);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(503, 475);
            this.MainPanel.TabIndex = 70;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "bronze");
            this.imageList.Images.SetKeyName(1, "yes");
            // 
            // GuildInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "GuildInfo";
            this.Size = new System.Drawing.Size(700, 550);
            this.Load += new System.EventHandler(this.GuildInfo_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GuildInfo_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membersButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox overviewButton;
        private System.Windows.Forms.PictureBox membersButton;
        private System.Windows.Forms.PictureBox settingsButton;
        private System.Windows.Forms.Label overviewLabel;
        private System.Windows.Forms.Label membersLabel;
        private System.Windows.Forms.Label settingsLabel;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.ImageList imageList;
    }
}