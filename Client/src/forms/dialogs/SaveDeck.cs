﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class SaveDeck : UserControl
    {
        private readonly SortedDictionary<int, int> CardIds;

        public SaveDeck(SortedDictionary<int, int> CardIds)
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(DeleteButton);
            ButtonAnimation.FormatButton(CancelButton);
            ButtonAnimation.FormatButton(SaveButton);
            this.CardIds = CardIds;
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            Parent.Dispose();
        }

        private void SaveButton_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(NewOnlineDeckTextBox.Text))
            {
                SoundService.PlaySound("button");
                PacketService.SendPacket(Commands.DECK_SAVE, new DeckPacket(NewOnlineDeckTextBox.Text, CardIds));
                Parent.Dispose();
            }
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            if (OnlineDecksListView.SelectedItems.Count == 0)
                return;

            SoundService.PlaySound("button");
            PacketService.SendPacket(Commands.DECK_DELETE, new CommandPacket(new string[] { OnlineDecksListView.SelectedItems[0].Text }));
            OnlineDecksListView.Items.Remove(OnlineDecksListView.SelectedItems[0]);
        }

        private void SaveDeck_Load(object sender, System.EventArgs e)
        {
            DeckPacket[] Decks = PacketService.GetDecksPacket().GetDeckPackets();

            for (int i = 0; i < Decks.Length; i++)
                OnlineDecksListView.Items.Add(Decks[i].GetDeckName());
        }

        private void OnlineDecksListView_ItemActivate(object sender, System.EventArgs e)
        {
            NewOnlineDeckTextBox.Text = OnlineDecksListView.SelectedItems[0].Text;
        }
    }
}
