﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class OpenDeck
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.OnlineDecksListView = new System.Windows.Forms.ListView();
            this.OnlineColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OpenButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.HideStartersCheckBox = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.CardsInDeckLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.Black;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(340, 12);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(213, 24);
            this.TitleLabel.TabIndex = 20;
            this.TitleLabel.Text = "Select a deck to open";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Location = new System.Drawing.Point(340, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Online Decks:";
            // 
            // OnlineDecksListView
            // 
            this.OnlineDecksListView.BackColor = System.Drawing.Color.Black;
            this.OnlineDecksListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.OnlineColumnHeader});
            this.OnlineDecksListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnlineDecksListView.ForeColor = System.Drawing.Color.White;
            this.OnlineDecksListView.FullRowSelect = true;
            this.OnlineDecksListView.HideSelection = false;
            this.OnlineDecksListView.Location = new System.Drawing.Point(340, 68);
            this.OnlineDecksListView.Name = "OnlineDecksListView";
            this.OnlineDecksListView.Size = new System.Drawing.Size(350, 362);
            this.OnlineDecksListView.TabIndex = 21;
            this.OnlineDecksListView.UseCompatibleStateImageBehavior = false;
            this.OnlineDecksListView.View = System.Windows.Forms.View.Details;
            this.OnlineDecksListView.SelectedIndexChanged += new System.EventHandler(this.OnlineDecksListView_SelectedIndexChanged);
            // 
            // OnlineColumnHeader
            // 
            this.OnlineColumnHeader.Text = "Deck Name";
            this.OnlineColumnHeader.Width = 300;
            // 
            // OpenButton
            // 
            this.OpenButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OpenButton.FlatAppearance.BorderSize = 0;
            this.OpenButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.OpenButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.OpenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenButton.Location = new System.Drawing.Point(595, 469);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(95, 29);
            this.OpenButton.TabIndex = 25;
            this.OpenButton.Text = "Open";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton.FlatAppearance.BorderSize = 0;
            this.CancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Location = new System.Drawing.Point(494, 469);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(95, 29);
            this.CancelButton.TabIndex = 26;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeleteButton.FlatAppearance.BorderSize = 0;
            this.DeleteButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeleteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteButton.Location = new System.Drawing.Point(393, 469);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(95, 29);
            this.DeleteButton.TabIndex = 27;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // HideStartersCheckBox
            // 
            this.HideStartersCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HideStartersCheckBox.FlatAppearance.BorderSize = 0;
            this.HideStartersCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.HideStartersCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.HideStartersCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HideStartersCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideStartersCheckBox.Location = new System.Drawing.Point(340, 436);
            this.HideStartersCheckBox.Name = "HideStartersCheckBox";
            this.HideStartersCheckBox.Size = new System.Drawing.Size(23, 20);
            this.HideStartersCheckBox.TabIndex = 28;
            this.HideStartersCheckBox.UseVisualStyleBackColor = true;
            this.HideStartersCheckBox.Click += new System.EventHandler(this.HideStartersCheckBox_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label11.Location = new System.Drawing.Point(369, 439);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 16);
            this.label11.TabIndex = 29;
            this.label11.Text = "Hide Starters";
            // 
            // CardsInDeckLabel
            // 
            this.CardsInDeckLabel.AutoSize = true;
            this.CardsInDeckLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardsInDeckLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.CardsInDeckLabel.Location = new System.Drawing.Point(48, 414);
            this.CardsInDeckLabel.Name = "CardsInDeckLabel";
            this.CardsInDeckLabel.Size = new System.Drawing.Size(146, 16);
            this.CardsInDeckLabel.TabIndex = 30;
            this.CardsInDeckLabel.Text = "Total Cards In Deck: 50";
            this.CardsInDeckLabel.Visible = false;
            // 
            // OpenDeck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.CardsInDeckLabel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.HideStartersCheckBox);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OpenButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OnlineDecksListView);
            this.Controls.Add(this.TitleLabel);
            this.DoubleBuffered = true;
            this.Name = "OpenDeck";
            this.Size = new System.Drawing.Size(700, 550);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView OnlineDecksListView;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.ColumnHeader OnlineColumnHeader;
        private System.Windows.Forms.Button HideStartersCheckBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label CardsInDeckLabel;
    }
}
