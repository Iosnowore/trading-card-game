﻿namespace TradingCardGame.forms.dialogs
{
    partial class UserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sprintTournamentsPanel = new System.Windows.Forms.Panel();
            this.BronzeMedallionPictureBox = new System.Windows.Forms.PictureBox();
            this.silverMedallionPictureBox = new System.Windows.Forms.PictureBox();
            this.GoldMedallionPictureBox = new System.Windows.Forms.PictureBox();
            this.friendsListTitleLabel = new System.Windows.Forms.Label();
            this.joinDateLabel = new System.Windows.Forms.Label();
            this.guildLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.winsLabel = new System.Windows.Forms.Label();
            this.lossesLabel = new System.Windows.Forms.Label();
            this.playerInformationPanel = new System.Windows.Forms.Panel();
            this.playerBiographyTextBox = new System.Windows.Forms.RichTextBox();
            this.playerBiographyTitleLabel = new System.Windows.Forms.Label();
            this.userInformationPanel = new System.Windows.Forms.Panel();
            this.DateOfBirthLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.avatarPictureBox = new System.Windows.Forms.PictureBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.sprintTournamentsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BronzeMedallionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.silverMedallionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoldMedallionPictureBox)).BeginInit();
            this.playerInformationPanel.SuspendLayout();
            this.userInformationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // sprintTournamentsPanel
            // 
            this.sprintTournamentsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.sprintTournamentsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sprintTournamentsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sprintTournamentsPanel.Controls.Add(this.BronzeMedallionPictureBox);
            this.sprintTournamentsPanel.Controls.Add(this.silverMedallionPictureBox);
            this.sprintTournamentsPanel.Controls.Add(this.GoldMedallionPictureBox);
            this.sprintTournamentsPanel.Location = new System.Drawing.Point(240, 256);
            this.sprintTournamentsPanel.Name = "sprintTournamentsPanel";
            this.sprintTournamentsPanel.Size = new System.Drawing.Size(285, 85);
            this.sprintTournamentsPanel.TabIndex = 23;
            // 
            // BronzeMedallionPictureBox
            // 
            this.BronzeMedallionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BronzeMedallionPictureBox.Location = new System.Drawing.Point(191, 0);
            this.BronzeMedallionPictureBox.Name = "BronzeMedallionPictureBox";
            this.BronzeMedallionPictureBox.Size = new System.Drawing.Size(85, 85);
            this.BronzeMedallionPictureBox.TabIndex = 10;
            this.BronzeMedallionPictureBox.TabStop = false;
            // 
            // silverMedallionPictureBox
            // 
            this.silverMedallionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.silverMedallionPictureBox.Location = new System.Drawing.Point(100, 0);
            this.silverMedallionPictureBox.Name = "silverMedallionPictureBox";
            this.silverMedallionPictureBox.Size = new System.Drawing.Size(85, 85);
            this.silverMedallionPictureBox.TabIndex = 9;
            this.silverMedallionPictureBox.TabStop = false;
            // 
            // GoldMedallionPictureBox
            // 
            this.GoldMedallionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.GoldMedallionPictureBox.Location = new System.Drawing.Point(9, 0);
            this.GoldMedallionPictureBox.Name = "GoldMedallionPictureBox";
            this.GoldMedallionPictureBox.Size = new System.Drawing.Size(85, 85);
            this.GoldMedallionPictureBox.TabIndex = 8;
            this.GoldMedallionPictureBox.TabStop = false;
            // 
            // friendsListTitleLabel
            // 
            this.friendsListTitleLabel.AutoSize = true;
            this.friendsListTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.friendsListTitleLabel.ForeColor = System.Drawing.Color.White;
            this.friendsListTitleLabel.Location = new System.Drawing.Point(236, 233);
            this.friendsListTitleLabel.Name = "friendsListTitleLabel";
            this.friendsListTitleLabel.Size = new System.Drawing.Size(167, 20);
            this.friendsListTitleLabel.TabIndex = 7;
            this.friendsListTitleLabel.Text = "Sprint Tournaments";
            // 
            // joinDateLabel
            // 
            this.joinDateLabel.AutoSize = true;
            this.joinDateLabel.BackColor = System.Drawing.Color.Transparent;
            this.joinDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinDateLabel.ForeColor = System.Drawing.Color.White;
            this.joinDateLabel.Location = new System.Drawing.Point(3, 150);
            this.joinDateLabel.Name = "joinDateLabel";
            this.joinDateLabel.Size = new System.Drawing.Size(132, 18);
            this.joinDateLabel.TabIndex = 21;
            this.joinDateLabel.Text = "Joined: 12/18/2016";
            // 
            // guildLabel
            // 
            this.guildLabel.AutoSize = true;
            this.guildLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildLabel.ForeColor = System.Drawing.Color.White;
            this.guildLabel.Location = new System.Drawing.Point(76, 108);
            this.guildLabel.Name = "guildLabel";
            this.guildLabel.Size = new System.Drawing.Size(67, 20);
            this.guildLabel.TabIndex = 8;
            this.guildLabel.Text = "<TEST>";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(68, 88);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(83, 20);
            this.usernameLabel.TabIndex = 1;
            this.usernameLabel.Text = "Username";
            // 
            // winsLabel
            // 
            this.winsLabel.AutoSize = true;
            this.winsLabel.BackColor = System.Drawing.Color.Transparent;
            this.winsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winsLabel.ForeColor = System.Drawing.Color.White;
            this.winsLabel.Location = new System.Drawing.Point(4, 360);
            this.winsLabel.Name = "winsLabel";
            this.winsLabel.Size = new System.Drawing.Size(58, 16);
            this.winsLabel.TabIndex = 8;
            this.winsLabel.Text = "Wins: 10";
            // 
            // lossesLabel
            // 
            this.lossesLabel.AutoSize = true;
            this.lossesLabel.BackColor = System.Drawing.Color.Transparent;
            this.lossesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lossesLabel.ForeColor = System.Drawing.Color.White;
            this.lossesLabel.Location = new System.Drawing.Point(4, 376);
            this.lossesLabel.Name = "lossesLabel";
            this.lossesLabel.Size = new System.Drawing.Size(72, 16);
            this.lossesLabel.TabIndex = 9;
            this.lossesLabel.Text = "Losses: 10";
            // 
            // playerInformationPanel
            // 
            this.playerInformationPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.playerInformationPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.playerInformationPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playerInformationPanel.Controls.Add(this.playerBiographyTextBox);
            this.playerInformationPanel.Location = new System.Drawing.Point(240, 34);
            this.playerInformationPanel.Name = "playerInformationPanel";
            this.playerInformationPanel.Size = new System.Drawing.Size(285, 192);
            this.playerInformationPanel.TabIndex = 19;
            // 
            // playerBiographyTextBox
            // 
            this.playerBiographyTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.playerBiographyTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.playerBiographyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerBiographyTextBox.ForeColor = System.Drawing.Color.White;
            this.playerBiographyTextBox.Location = new System.Drawing.Point(6, 6);
            this.playerBiographyTextBox.Name = "playerBiographyTextBox";
            this.playerBiographyTextBox.ReadOnly = true;
            this.playerBiographyTextBox.Size = new System.Drawing.Size(273, 180);
            this.playerBiographyTextBox.TabIndex = 10;
            this.playerBiographyTextBox.Text = "";
            this.playerBiographyTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.playerBiographyTextBox_KeyDown);
            // 
            // playerBiographyTitleLabel
            // 
            this.playerBiographyTitleLabel.AutoSize = true;
            this.playerBiographyTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerBiographyTitleLabel.ForeColor = System.Drawing.Color.White;
            this.playerBiographyTitleLabel.Location = new System.Drawing.Point(240, 11);
            this.playerBiographyTitleLabel.Name = "playerBiographyTitleLabel";
            this.playerBiographyTitleLabel.Size = new System.Drawing.Size(156, 20);
            this.playerBiographyTitleLabel.TabIndex = 7;
            this.playerBiographyTitleLabel.Text = "Personal Message";
            // 
            // userInformationPanel
            // 
            this.userInformationPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.userInformationPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userInformationPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userInformationPanel.Controls.Add(this.DateOfBirthLabel);
            this.userInformationPanel.Controls.Add(this.lossesLabel);
            this.userInformationPanel.Controls.Add(this.winsLabel);
            this.userInformationPanel.Controls.Add(this.label2);
            this.userInformationPanel.Controls.Add(this.label5);
            this.userInformationPanel.Controls.Add(this.label4);
            this.userInformationPanel.Controls.Add(this.joinDateLabel);
            this.userInformationPanel.Controls.Add(this.label3);
            this.userInformationPanel.Controls.Add(this.avatarPictureBox);
            this.userInformationPanel.Controls.Add(this.guildLabel);
            this.userInformationPanel.Controls.Add(this.usernameLabel);
            this.userInformationPanel.Location = new System.Drawing.Point(16, 11);
            this.userInformationPanel.Name = "userInformationPanel";
            this.userInformationPanel.Size = new System.Drawing.Size(218, 397);
            this.userInformationPanel.TabIndex = 18;
            // 
            // DateOfBirthLabel
            // 
            this.DateOfBirthLabel.AutoSize = true;
            this.DateOfBirthLabel.BackColor = System.Drawing.Color.Transparent;
            this.DateOfBirthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateOfBirthLabel.ForeColor = System.Drawing.Color.White;
            this.DateOfBirthLabel.Location = new System.Drawing.Point(3, 172);
            this.DateOfBirthLabel.Name = "DateOfBirthLabel";
            this.DateOfBirthLabel.Size = new System.Drawing.Size(189, 18);
            this.DateOfBirthLabel.TabIndex = 23;
            this.DateOfBirthLabel.Text = "DOB: Hidden (29 years old)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 340);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Win / Loss Record";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(4, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 16);
            this.label5.TabIndex = 22;
            this.label5.Text = "Regular: 1000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(4, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ranked: 1000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 283);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ratings";
            // 
            // avatarPictureBox
            // 
            this.avatarPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.avatarPictureBox.Location = new System.Drawing.Point(67, 0);
            this.avatarPictureBox.Name = "avatarPictureBox";
            this.avatarPictureBox.Padding = new System.Windows.Forms.Padding(5);
            this.avatarPictureBox.Size = new System.Drawing.Size(85, 85);
            this.avatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatarPictureBox.TabIndex = 0;
            this.avatarPictureBox.TabStop = false;
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.Black;
            this.CloseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CloseButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CloseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CloseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.ForeColor = System.Drawing.Color.Black;
            this.CloseButton.Location = new System.Drawing.Point(469, 430);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 52;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // UserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.sprintTournamentsPanel);
            this.Controls.Add(this.playerInformationPanel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.friendsListTitleLabel);
            this.Controls.Add(this.playerBiographyTitleLabel);
            this.Controls.Add(this.userInformationPanel);
            this.DoubleBuffered = true;
            this.Name = "UserInfo";
            this.Size = new System.Drawing.Size(547, 456);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UserInfo_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UserInfo_MouseMove);
            this.sprintTournamentsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BronzeMedallionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.silverMedallionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoldMedallionPictureBox)).EndInit();
            this.playerInformationPanel.ResumeLayout(false);
            this.userInformationPanel.ResumeLayout(false);
            this.userInformationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.avatarPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel sprintTournamentsPanel;
        private System.Windows.Forms.Label friendsListTitleLabel;
        private System.Windows.Forms.Label winsLabel;
        private System.Windows.Forms.Label lossesLabel;
        private System.Windows.Forms.Panel playerInformationPanel;
        private System.Windows.Forms.RichTextBox playerBiographyTextBox;
        private System.Windows.Forms.Label playerBiographyTitleLabel;
        private System.Windows.Forms.Panel userInformationPanel;
        private System.Windows.Forms.Label guildLabel;
        private System.Windows.Forms.PictureBox avatarPictureBox;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.PictureBox GoldMedallionPictureBox;
        private System.Windows.Forms.PictureBox BronzeMedallionPictureBox;
        private System.Windows.Forms.PictureBox silverMedallionPictureBox;
        private System.Windows.Forms.Label joinDateLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label DateOfBirthLabel;
        private System.Windows.Forms.Button CloseButton;
    }
}