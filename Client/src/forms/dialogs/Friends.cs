﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData.Packets.Player;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.forms.dialogs
{
    public partial class Friends : UserControl
    {
        private Point p;

        public Friends()
        {
            InitializeComponent();
            ButtonAnimation.FormatButton(removeFriendButton);
            ButtonAnimation.FormatButton(removeIgnoredButton);
            ButtonAnimation.FormatButton(IgnoreUserButton);
        }

        private void Friends_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void Friends_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }

        private void Friends_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();

            FriendsIgnoreListPacket Packet = PacketService.GetFriendsIgnoreListPacket();

            foreach (string Friend in Packet.GetFriends())
                friendsListBox.Items.Add(Friend);

            foreach (string Ignore in Packet.GetIgnores())
                ignoreListBox.Items.Add(Ignore);
        }

        private void removeFriendButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            PlayerService.RemoveFriendCommand(friendsListBox.SelectedItems[0].Text);
        }

        private void removeIgnoredButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            PlayerService.UnignorePlayerCommand(ignoreListBox.SelectedItems[0].Text);
        }

        private void IgnoreUserButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            PlayerService.IgnorePlayerCommand(IgnoreUserTextBox.Text);
        }
    }
}
