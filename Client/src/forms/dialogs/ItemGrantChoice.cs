﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class ItemGrantChoice : UserControl
    {
        private readonly RedemptionConfirmation RedemptionConfirmation;

        private Card PreviewCard;

        public ItemGrantChoice(int CardId, RedemptionConfirmation RedemptionConfirmation, CollectionManager.LoadType LoadType)
        {
            InitializeComponent();

            this.RedemptionConfirmation = RedemptionConfirmation;

            CardData CardData = null;
            PackData PackData = null;

            if (LoadType == CollectionManager.LoadType.RedeemableItems)
                CardData = TCGData.Cards.GetLootCardData(CardId);
            else if (LoadType == CollectionManager.LoadType.PromoPacks)
                PackData = TCGData.Cards.GetPromoPackData(CardId);

            // Add the title
            TitleLabel.Text = PackData == null ? CardData.GetTitle() : PackData.GetTitle();
            TitleLabel.Location = new Point((Width - TitleLabel.Width) / 2, TitleLabel.Location.Y);

            // Add the card
            PreviewCard = PackData == null ? new Card(CardData, Card.SIZE.MEDIUM) : new Card(PackData, Card.SIZE.MEDIUM);
            PreviewCard.Location = new Point(20, (Height - PreviewCard.Height) / 2);
            Controls.Add(PreviewCard);

            // Add the UI to the buttons
            ButtonAnimation.FormatButton(CloseButton);
            ButtonAnimation.FormatButton(ChooseButton);

            // Disable the ChooseButton until the player selects a valid option
            ButtonAnimation.DisableButton(ChooseButton);

            // Populate listbox
            PopulateListBox(LoadType);
        }

        private void PopulateListBox(CollectionManager.LoadType LoadType)
        {
            if (LoadType == CollectionManager.LoadType.RedeemableItems)
            {
                for (int i = 1; i < 9; i++)
                {
                    string PackIdString = i + "10";
                    int PackId = int.Parse(PackIdString);
                    string PackTitle = TCGData.Cards.GetPackData(PackId).GetTitle();
                    ListBox.Items.Add(PackIdString + "\t" + PackTitle);
                }
            }
            else
            {
                for (int i = 1; i < 5; i++)
                {
                    string PackIdString = "11" + i;
                    int PackId = int.Parse(PackIdString);
                    string PackTitle = TCGData.Cards.GetPackData(PackId).GetTitle();
                    ListBox.Items.Add(PackIdString + "\t" + PackTitle);
                }
            }
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            Parent.Dispose();
        }

        private void ChooseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            string SelectedRow = ListBox.SelectedItem.ToString();
            string PackIdString = SelectedRow.Split('\t')[0];
            int PackId = int.Parse(PackIdString);

            RedemptionConfirmation.SetSelectedId(PackId);

            Parent.Dispose();
        }

        private void ListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (PreviewCard != null && !PreviewCard.IsDisposed)
            {
                PreviewCard.Dispose();
                PreviewCard = null;
            }

            SoundService.PlaySound("button");

            string SelectedRow = ListBox.SelectedItem.ToString();
            string PackIdString = SelectedRow.Split('\t')[0];
            int PackId = int.Parse(PackIdString);

            PackData PackData = TCGData.Cards.GetPackData(PackId);
            PreviewCard = new Card(PackData, Card.SIZE.MEDIUM);
            PreviewCard.Location = new Point(20, (Height - PreviewCard.Height) / 2);
            Controls.Add(PreviewCard);

            if (!ChooseButton.Enabled)
                ButtonAnimation.EnableButton(ChooseButton);
        }
    }
}
