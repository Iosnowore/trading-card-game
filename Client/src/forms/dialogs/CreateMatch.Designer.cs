﻿namespace TradingCardGame.forms.dialogs
{
    partial class CreateMatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelMatchCreationButton = new System.Windows.Forms.Button();
            this.ResetMatchCreationButton = new System.Windows.Forms.Button();
            this.CreateMatchButton = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.guildOnlyComboBox = new System.Windows.Forms.ComboBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.timeLimitComboBox = new System.Windows.Forms.ComboBox();
            this.deckSelectionTextBox = new System.Windows.Forms.TextBox();
            this.SelectDeckButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.matchStructureComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.playFormatComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numberOfPlayersListBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.matchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.matchTitleTextBox = new System.Windows.Forms.TextBox();
            this.ValidateButton = new System.Windows.Forms.Button();
            this.AllowObserversCheckBox = new System.Windows.Forms.CheckBox();
            this.FriendsOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.LightVsDarkCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // CancelMatchCreationButton
            // 
            this.CancelMatchCreationButton.BackColor = System.Drawing.Color.Black;
            this.CancelMatchCreationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelMatchCreationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CancelMatchCreationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelMatchCreationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelMatchCreationButton.ForeColor = System.Drawing.Color.Black;
            this.CancelMatchCreationButton.Location = new System.Drawing.Point(314, 383);
            this.CancelMatchCreationButton.Name = "CancelMatchCreationButton";
            this.CancelMatchCreationButton.Size = new System.Drawing.Size(95, 29);
            this.CancelMatchCreationButton.TabIndex = 75;
            this.CancelMatchCreationButton.Text = "Cancel";
            this.CancelMatchCreationButton.UseVisualStyleBackColor = false;
            this.CancelMatchCreationButton.Click += new System.EventHandler(this.CancelMatchCreationButton_Click);
            // 
            // ResetMatchCreationButton
            // 
            this.ResetMatchCreationButton.BackColor = System.Drawing.Color.Black;
            this.ResetMatchCreationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ResetMatchCreationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ResetMatchCreationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetMatchCreationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetMatchCreationButton.ForeColor = System.Drawing.Color.Black;
            this.ResetMatchCreationButton.Location = new System.Drawing.Point(415, 383);
            this.ResetMatchCreationButton.Name = "ResetMatchCreationButton";
            this.ResetMatchCreationButton.Size = new System.Drawing.Size(95, 29);
            this.ResetMatchCreationButton.TabIndex = 74;
            this.ResetMatchCreationButton.Text = "Reset";
            this.ResetMatchCreationButton.UseVisualStyleBackColor = false;
            this.ResetMatchCreationButton.Click += new System.EventHandler(this.ResetMatchCreationButton_Click);
            // 
            // CreateMatchButton
            // 
            this.CreateMatchButton.BackColor = System.Drawing.Color.Black;
            this.CreateMatchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CreateMatchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CreateMatchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateMatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateMatchButton.ForeColor = System.Drawing.Color.Black;
            this.CreateMatchButton.Location = new System.Drawing.Point(516, 383);
            this.CreateMatchButton.Name = "CreateMatchButton";
            this.CreateMatchButton.Size = new System.Drawing.Size(95, 29);
            this.CreateMatchButton.TabIndex = 73;
            this.CreateMatchButton.Text = "Create";
            this.CreateMatchButton.UseVisualStyleBackColor = false;
            this.CreateMatchButton.Click += new System.EventHandler(this.CreateMatchButton_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label14.Location = new System.Drawing.Point(134, 345);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 15);
            this.label14.TabIndex = 72;
            this.label14.Text = "Guild Only:";
            // 
            // guildOnlyComboBox
            // 
            this.guildOnlyComboBox.BackColor = System.Drawing.Color.Black;
            this.guildOnlyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guildOnlyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildOnlyComboBox.ForeColor = System.Drawing.Color.White;
            this.guildOnlyComboBox.FormattingEnabled = true;
            this.guildOnlyComboBox.Items.AddRange(new object[] {
            "No Guild Restriction",
            "Guild Officers Only",
            "All Guild Members"});
            this.guildOnlyComboBox.Location = new System.Drawing.Point(208, 342);
            this.guildOnlyComboBox.Name = "guildOnlyComboBox";
            this.guildOnlyComboBox.Size = new System.Drawing.Size(262, 23);
            this.guildOnlyComboBox.TabIndex = 71;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.Color.Black;
            this.passwordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.passwordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.passwordTextBox.Location = new System.Drawing.Point(208, 314);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.ReadOnly = true;
            this.passwordTextBox.Size = new System.Drawing.Size(262, 22);
            this.passwordTextBox.TabIndex = 70;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label13.Location = new System.Drawing.Point(137, 314);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 15);
            this.label13.TabIndex = 69;
            this.label13.Text = "Password:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label12.Location = new System.Drawing.Point(134, 285);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 15);
            this.label12.TabIndex = 68;
            this.label12.Text = "Time Limit:";
            // 
            // timeLimitComboBox
            // 
            this.timeLimitComboBox.BackColor = System.Drawing.Color.Black;
            this.timeLimitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timeLimitComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLimitComboBox.ForeColor = System.Drawing.Color.White;
            this.timeLimitComboBox.FormattingEnabled = true;
            this.timeLimitComboBox.Items.AddRange(new object[] {
            "No Time Limit",
            "90m Chess Clock"});
            this.timeLimitComboBox.Location = new System.Drawing.Point(208, 282);
            this.timeLimitComboBox.Name = "timeLimitComboBox";
            this.timeLimitComboBox.Size = new System.Drawing.Size(262, 23);
            this.timeLimitComboBox.TabIndex = 67;
            // 
            // deckSelectionTextBox
            // 
            this.deckSelectionTextBox.BackColor = System.Drawing.Color.Black;
            this.deckSelectionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.deckSelectionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deckSelectionTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.deckSelectionTextBox.Location = new System.Drawing.Point(90, 250);
            this.deckSelectionTextBox.Name = "deckSelectionTextBox";
            this.deckSelectionTextBox.ReadOnly = true;
            this.deckSelectionTextBox.Size = new System.Drawing.Size(354, 22);
            this.deckSelectionTextBox.TabIndex = 61;
            // 
            // SelectDeckButton
            // 
            this.SelectDeckButton.BackColor = System.Drawing.Color.Black;
            this.SelectDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.SelectDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectDeckButton.ForeColor = System.Drawing.Color.Black;
            this.SelectDeckButton.Location = new System.Drawing.Point(523, 250);
            this.SelectDeckButton.Name = "SelectDeckButton";
            this.SelectDeckButton.Size = new System.Drawing.Size(95, 29);
            this.SelectDeckButton.TabIndex = 65;
            this.SelectDeckButton.Text = "Select Deck";
            this.SelectDeckButton.UseVisualStyleBackColor = false;
            this.SelectDeckButton.Click += new System.EventHandler(this.SelectDeckButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label8.Location = new System.Drawing.Point(87, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 15);
            this.label8.TabIndex = 60;
            this.label8.Text = "Deck Selection:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label6.Location = new System.Drawing.Point(352, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "Match Structure:";
            // 
            // matchStructureComboBox
            // 
            this.matchStructureComboBox.BackColor = System.Drawing.Color.Black;
            this.matchStructureComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchStructureComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchStructureComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchStructureComboBox.FormattingEnabled = true;
            this.matchStructureComboBox.Items.AddRange(new object[] {
            "Single Game",
            "Best of Three Games",
            "Best of Five Games"});
            this.matchStructureComboBox.Location = new System.Drawing.Point(355, 196);
            this.matchStructureComboBox.Name = "matchStructureComboBox";
            this.matchStructureComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchStructureComboBox.TabIndex = 58;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label7.Location = new System.Drawing.Point(88, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 57;
            this.label7.Text = "Play Format:";
            // 
            // playFormatComboBox
            // 
            this.playFormatComboBox.BackColor = System.Drawing.Color.Black;
            this.playFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.playFormatComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playFormatComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.playFormatComboBox.FormattingEnabled = true;
            this.playFormatComboBox.Items.AddRange(new object[] {
            "Standard",
            "Raid"});
            this.playFormatComboBox.Location = new System.Drawing.Point(90, 196);
            this.playFormatComboBox.Name = "playFormatComboBox";
            this.playFormatComboBox.Size = new System.Drawing.Size(259, 24);
            this.playFormatComboBox.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label5.Location = new System.Drawing.Point(352, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 15);
            this.label5.TabIndex = 55;
            this.label5.Text = "Number of Players:";
            // 
            // numberOfPlayersListBox
            // 
            this.numberOfPlayersListBox.BackColor = System.Drawing.Color.Black;
            this.numberOfPlayersListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberOfPlayersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfPlayersListBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfPlayersListBox.FormattingEnabled = true;
            this.numberOfPlayersListBox.Items.AddRange(new object[] {
            "2 Players",
            "4 Players"});
            this.numberOfPlayersListBox.Location = new System.Drawing.Point(355, 146);
            this.numberOfPlayersListBox.Name = "numberOfPlayersListBox";
            this.numberOfPlayersListBox.Size = new System.Drawing.Size(259, 24);
            this.numberOfPlayersListBox.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(88, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 53;
            this.label4.Text = "Match Type:";
            // 
            // matchTypeComboBox
            // 
            this.matchTypeComboBox.BackColor = System.Drawing.Color.Black;
            this.matchTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.matchTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTypeComboBox.ForeColor = System.Drawing.Color.White;
            this.matchTypeComboBox.FormattingEnabled = true;
            this.matchTypeComboBox.Items.AddRange(new object[] {
            "Constructed"});
            this.matchTypeComboBox.Location = new System.Drawing.Point(90, 146);
            this.matchTypeComboBox.Name = "matchTypeComboBox";
            this.matchTypeComboBox.Size = new System.Drawing.Size(259, 24);
            this.matchTypeComboBox.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(88, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 49;
            this.label2.Text = "Match Title:";
            // 
            // matchTitleTextBox
            // 
            this.matchTitleTextBox.BackColor = System.Drawing.Color.Black;
            this.matchTitleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.matchTitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTitleTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.matchTitleTextBox.Location = new System.Drawing.Point(91, 94);
            this.matchTitleTextBox.Name = "matchTitleTextBox";
            this.matchTitleTextBox.Size = new System.Drawing.Size(523, 22);
            this.matchTitleTextBox.TabIndex = 48;
            // 
            // ValidateButton
            // 
            this.ValidateButton.BackColor = System.Drawing.Color.Black;
            this.ValidateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ValidateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ValidateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ValidateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValidateButton.ForeColor = System.Drawing.Color.Black;
            this.ValidateButton.Location = new System.Drawing.Point(442, 247);
            this.ValidateButton.Name = "ValidateButton";
            this.ValidateButton.Size = new System.Drawing.Size(91, 29);
            this.ValidateButton.TabIndex = 79;
            this.ValidateButton.UseVisualStyleBackColor = false;
            // 
            // AllowObserversCheckBox
            // 
            this.AllowObserversCheckBox.AutoSize = true;
            this.AllowObserversCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllowObserversCheckBox.Location = new System.Drawing.Point(493, 285);
            this.AllowObserversCheckBox.Name = "AllowObserversCheckBox";
            this.AllowObserversCheckBox.Size = new System.Drawing.Size(116, 19);
            this.AllowObserversCheckBox.TabIndex = 80;
            this.AllowObserversCheckBox.Text = "Allow Observers:";
            this.AllowObserversCheckBox.UseVisualStyleBackColor = true;
            // 
            // FriendsOnlyCheckBox
            // 
            this.FriendsOnlyCheckBox.AutoSize = true;
            this.FriendsOnlyCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FriendsOnlyCheckBox.Location = new System.Drawing.Point(493, 310);
            this.FriendsOnlyCheckBox.Name = "FriendsOnlyCheckBox";
            this.FriendsOnlyCheckBox.Size = new System.Drawing.Size(97, 19);
            this.FriendsOnlyCheckBox.TabIndex = 81;
            this.FriendsOnlyCheckBox.Text = "Friends Only:";
            this.FriendsOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // LightVsDarkCheckBox
            // 
            this.LightVsDarkCheckBox.AutoSize = true;
            this.LightVsDarkCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LightVsDarkCheckBox.Location = new System.Drawing.Point(493, 335);
            this.LightVsDarkCheckBox.Name = "LightVsDarkCheckBox";
            this.LightVsDarkCheckBox.Size = new System.Drawing.Size(102, 19);
            this.LightVsDarkCheckBox.TabIndex = 82;
            this.LightVsDarkCheckBox.Text = "Light vs. Dark:";
            this.LightVsDarkCheckBox.UseVisualStyleBackColor = true;
            // 
            // CreateMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.LightVsDarkCheckBox);
            this.Controls.Add(this.FriendsOnlyCheckBox);
            this.Controls.Add(this.AllowObserversCheckBox);
            this.Controls.Add(this.SelectDeckButton);
            this.Controls.Add(this.deckSelectionTextBox);
            this.Controls.Add(this.ValidateButton);
            this.Controls.Add(this.guildOnlyComboBox);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.timeLimitComboBox);
            this.Controls.Add(this.matchStructureComboBox);
            this.Controls.Add(this.playFormatComboBox);
            this.Controls.Add(this.numberOfPlayersListBox);
            this.Controls.Add(this.matchTypeComboBox);
            this.Controls.Add(this.matchTitleTextBox);
            this.Controls.Add(this.CancelMatchCreationButton);
            this.Controls.Add(this.ResetMatchCreationButton);
            this.Controls.Add(this.CreateMatchButton);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.DoubleBuffered = true;
            this.Name = "CreateMatch";
            this.Size = new System.Drawing.Size(700, 550);
            this.Load += new System.EventHandler(this.CreateMatch_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CancelMatchCreationButton;
        private System.Windows.Forms.Button ResetMatchCreationButton;
        private System.Windows.Forms.Button CreateMatchButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox guildOnlyComboBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox timeLimitComboBox;
        private System.Windows.Forms.TextBox deckSelectionTextBox;
        private System.Windows.Forms.Button SelectDeckButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox matchStructureComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox playFormatComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox numberOfPlayersListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox matchTypeComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox matchTitleTextBox;
        private System.Windows.Forms.Button ValidateButton;
        private System.Windows.Forms.CheckBox AllowObserversCheckBox;
        private System.Windows.Forms.CheckBox FriendsOnlyCheckBox;
        private System.Windows.Forms.CheckBox LightVsDarkCheckBox;
    }
}