﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Deck;

namespace TradingCardGame.forms.dialogs
{
    public partial class JoinMatch : UserControl
    {
        private Point mouseDownPoint;
        private readonly int MatchId;

        public JoinMatch(int MatchId)
        {
            InitializeComponent();
            BackgroundImage = ResourcesLib.GetImageFromBorders("navframe");
            exitButton.Image = ResourcesLib.GetImageFromButtons("closebutton");
            deckStatusPictureBox.Image = ResourcesLib.GetImageFromIcons("no");
            deckStatusPictureBox.BackgroundImage = ResourcesLib.GetImageFromButtons("button_validate");

            this.MatchId = MatchId;

            ButtonAnimation.FormatButton(selectDeckButton);
            ButtonAnimation.FormatButton(joinButton);
        }

        private void joinButton_Click(object sender, EventArgs e)
        {
            //string password = DatabaseService.GetValue("matches", "password", "title", matchTitleLabel.Text.Replace("'", "''"));
            string password = null;

            if (!string.IsNullOrEmpty(password))
            {
                if (!joinMatchPasswordTextBox.Text.Equals(password))
                {
                    new SystemMessage("The password you entered is incorrect.", "Incorrect Password").Show();
                    return;
                }
            }

            if (string.IsNullOrEmpty(deckSelectionTextBox.Text))
            {
                new SystemMessage("Please select a deck to use in battle.", "No deck selected").Show();
                return;
            }

            PacketService.SendPacket(Commands.MATCH_JOIN, new CommandPacket(new string[] { MatchId.ToString() }));
            Dispose();
        }

        private void JoinMatch_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DeckService.GetSelectedDeckName()))
            {
                deckStatusPictureBox.Image = ResourcesLib.GetImageFromIcons("yes");
                deckSelectionTextBox.Text = DeckService.GetSelectedDeckName();
            }

            MatchPacket MatchPacket = PacketService.GetMatchPacket(MatchId.ToString());
            joinMatchFormatLabel.Text = "Format: " + MatchPacket.GetType();

            if (string.IsNullOrEmpty(MatchPacket.GetPassword()))
            {
                passwordLabel.Visible = false;
                joinMatchPasswordTextBox.Visible = false;
            }
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void exitJoinMatchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void selectDeckButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Controls.Add(new CreateMatch());
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.Image = ResourcesLib.GetImageFromButtons("closebutton_over");
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.Image = ResourcesLib.GetImageFromButtons("closebutton_down");
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            exitButton.Image = ResourcesLib.GetImageFromButtons("closebutton");
        }

        private void JoinMatch_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(Location.X + (e.X - mouseDownPoint.X), Location.Y + (e.Y - mouseDownPoint.Y));
        }

        private void JoinMatch_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }
    }
}
