﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs.preferences;
using TradingCardGame.forms.widgets;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.forms.dialogs
{
    public partial class Preferences : UserControl
    {
        private int currentPage;
        private UserControl currentControl;

        public Preferences()
        {
            InitializeComponent();

            accountInfoButton.Image = ResourcesLib.GetImageFromPreferences("account_icon");
            avatarButton.Image = ResourcesLib.GetImageFromPreferences("avatar_icon");
            soundButton.Image = ResourcesLib.GetImageFromPreferences("display_icon");
            userInterfaceButton.Image = ResourcesLib.GetImageFromPreferences("ui_icon");

            ButtonAnimation.FormatButton(resetButton);
            ButtonAnimation.FormatButton(cancelButton);
            ButtonAnimation.FormatButton(acceptButton);

            // Disable button
            ButtonAnimation.DisableButton(acceptButton);

            LoadNewPage(new AccountInfo(this), 1);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void LoadNewPage(UserControl control, int page)
        {
            if (currentControl != null)
            {
                if (currentControl is DisplaySound)
                {
                    MusicService.SetVolume(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume")));
                    SoundService.SetVolume(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "SoundVolume")));
                }
                currentControl.Dispose();
            }
            currentPage = page;
            currentControl = control;
            preferencesFillPanel.Controls.Add(control);
        }

        public void ChangedSetting(bool Loading)
        {
            if (!acceptButton.Enabled && !Loading)
            {
                ButtonAnimation.EnableButton(acceptButton);
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            CloseDialog();
        }

        private void CloseDialog()
        {
            MusicService.SetVolume(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume")));
            SoundService.SetVolume(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "SoundVolume")));
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            ButtonAnimation.DisableButton(acceptButton);

            switch (currentPage)
            {
                case 1:
                    ((AccountInfo)currentControl).SaveAccountInfo();
                    break;
                case 2:
                    ((Avatar)currentControl).SaveAvatar();
                    break;
                case 3:
                    bool Enabled = ((DisplaySound)currentControl).SoundIsEnabled();

                    SettingsService.SetValueInSettingsXml("Audio", "Sound", Enabled.ToString());
                    SettingsService.SetValueInSettingsXml("Audio", "SoundVolume", SoundService.GetVolume().ToString());
                    SettingsService.SetValueInSettingsXml("Audio", "MusicVolume", MusicService.GetVolume().ToString());

                    if (!Enabled)
                        MusicService.StopMusic();
                    break;
                case 4:
                    // TODO: Finish this up
                    UserInterface UserInterface = ((UserInterface)currentControl);
                    SettingsService.SetValueInSettingsXml("General", "LobbyMessages", UserInterface.GetLobbyMessages());
                    SettingsService.SetValueInSettingsXml("General", "RequestAttention", UserInterface.GetRequestAttention());
                    SettingsService.SetValueInSettingsXml("General", "LeftNavigatorPulse", UserInterface.GetLeftNavigatorPulse());
                    SettingsService.SetValueInSettingsXml("General", "MultiPassRendering", UserInterface.GetMultiPassRendering());

                    SettingsService.SetValueInSettingsXml("GamePlaymat", "HandDisplayMode", UserInterface.GetHandDisplayMode());
                    SettingsService.SetValueInSettingsXml("GamePlaymat", "FoilEffects", UserInterface.GetFoilEffects());
                    SettingsService.SetValueInSettingsXml("GamePlaymat", "PassWhenOutOfActions", UserInterface.GetPass());
                    SettingsService.SetValueInSettingsXml("GamePlaymat", "CardWarnings", UserInterface.GetCardWarnings());
                    SettingsService.SetValueInSettingsXml("GamePlaymat", "AutoSelectFirstAction", UserInterface.GetCardWarnings());
                    SettingsService.SetValueInSettingsXml("GamePlaymat", "BackgroundImage", UserInterface.GetBackgroundImage());

                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "MainScreen", UserInterface.GetHideMainScreen());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "Collection", UserInterface.GetHideCollection());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "DeckBuilder", UserInterface.GetHideDeckbuilder());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "ExcessiveAttack", UserInterface.GetHideExcessiveAttack());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "MultiplayerGames", UserInterface.GetHideMultiplayerGames());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "Trade", UserInterface.GetHideTrade());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "TradeLobby", UserInterface.GetHideTradeLobby());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "CasualLobby", UserInterface.GetHideCasualLobby());
                    SettingsService.SetValueInSettingsXml("HidePlayerTips", "QuestFilled", UserInterface.GetHideQuestFilled());

                    SettingsService.SetValueInSettingsXml("ShowOnlyCardsIOwn", "DeckBuilder", UserInterface.GetDeckBuilder());
                    SettingsService.SetValueInSettingsXml("ShowOnlyCardsIOwn", "CollectionManager", UserInterface.GetCollectionManager());

                    SettingsService.SetValueInSettingsXml("CardSize", "DeckBuilderCollection", UserInterface.GetDeckBuilderCollection());
                    SettingsService.SetValueInSettingsXml("CardSize", "DeckBuilderDeck", UserInterface.GetDeckBuilderDeck());
                    SettingsService.SetValueInSettingsXml("CardSize", "Playmat", UserInterface.GetPlaymat());

                    Navigator Navigator = ((TCG)Parent.Parent.Parent).GetNavigator();
                    bool Pulse = UserInterface.GetLeftNavigatorPulse();
                    Navigator.TogglePulse(Pulse);
                    break;
            }
            SoundService.PlaySound("button");
            ((TCG)Parent.Parent.Parent).AddSystemMessage("You have successfully saved your settings.", "Saved Settings");
        }

        private void AccountInfoButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TitleLabel.Text = "Account Info";
            LoadNewPage(new AccountInfo(this), 1);
        }

        private void AvatarButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TitleLabel.Text = "Avatar";
            LoadNewPage(new Avatar(this), 2);
        }

        private void SoundButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TitleLabel.Text = "Sound";
            LoadNewPage(new DisplaySound(this), 3);
        }

        private void UserInterfaceButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TitleLabel.Text = "User Interface Options";
            LoadNewPage(new UserInterface(this), 4);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            // TODO: implement
            SoundService.PlaySound("button");
        }
    }
}
