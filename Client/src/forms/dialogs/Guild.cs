﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Guild;
using TradingCardGame.forms.dialogs;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.forms
{
    public partial class Guild : UserControl
    {
        private readonly bool userIsInAGuild = false;

        public Guild()
        {
            InitializeComponent();

            RequestGuildsPacket AllGuildInfo = PacketService.GetRequestGuildsPacket();

            // TODO: Implement a better way in determining if user is in a guild
            for (int i = 0; i < AllGuildInfo.GetNumberOfGuilds(); i++)
            {
                GuildInfoPacket Guild = AllGuildInfo.GetGuild(i);

                if (Guild.GetLeader().Equals(PlayerService.GetUsername()))
                {
                    userIsInAGuild = true;
                    break;
                }

                foreach (string Officer in Guild.GetOfficers())
                {
                    if (Officer.Equals(Guild.GetOfficers()))
                    {
                        userIsInAGuild = true;
                        break;
                    }
                }

                foreach (string Member in Guild.GetMembers())
                {
                    if (Member.Equals(Guild.GetMembers()))
                    {
                        userIsInAGuild = true;
                        break;
                    }
                }
            }

            pictureBox1.Image = ResourcesLib.GetImageFromGuild("guild_" + (userIsInAGuild ? "active" : "inactive"));

            ButtonAnimation.FormatButton(closeButton);
            ButtonAnimation.FormatButton(createGuildButton);
            ButtonAnimation.FormatButton(leaveGuildButton);

            if (userIsInAGuild)
                ButtonAnimation.DisableButton(createGuildButton);
            else
                ButtonAnimation.DisableButton(leaveGuildButton);

            creatingAGuildDescriptionLabel.Text = StringLib.GetString("guild:creatingAGuildDescriptionText");
            joiningAGuildDescriptionLabel.Text = StringLib.GetString("guild:joiningAGuildDescriptionText");

            if (AllGuildInfo.GetNumberOfGuilds() == 0)
                return;

            ListViewItem guild;

            for (int i = 0; i < AllGuildInfo.GetNumberOfGuilds(); i++)
            {
                guild = new ListViewItem("1");
                guild.SubItems.Add(AllGuildInfo.GetGuild(i).GetName());
                guild.SubItems.Add(AllGuildInfo.GetGuild(i).GetMembers().Length.ToString());
                guild.SubItems.Add(AllGuildInfo.GetGuild(i).GetLeader());
                guildsListViewer.Items.Add(guild);
            }

            numberOfGuildsLabel.Text = guildsListViewer.Items.Count + " guilds.";
            numberOfFriendsGuildsLabel.Text = "0 guilds with friends.";
        }

        private void GuildPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            new Map().Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private void createGuildButton_Click(object sender, EventArgs e)
        {
            if (userIsInAGuild || TCG.GetDialogStatus())
                return;

            SoundService.PlaySound("button");

            TCG.SetDialogStatus(true);
            Controls.Add(new Window(new CreateGuild(), "Create Guild"));
        }

        private void leaveGuildButton_Click(object sender, EventArgs e)
        {
            if (!userIsInAGuild)
                return;

            SoundService.PlaySound("button");

            DialogResult dr = MessageBox.Show("Are you sure you want to leave your guild?", "", MessageBoxButtons.YesNoCancel);

            if (dr == DialogResult.Yes)
                PacketService.SendPacket(Commands.GUILD_LEAVE, null);
            Parent.Dispose();
        }

        private void Guild_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }
    }
}
