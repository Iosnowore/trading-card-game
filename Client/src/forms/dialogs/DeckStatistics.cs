﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Collections.Generic;
using System.Windows.Forms;
using TCGData;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class DeckStatistics : UserControl
    {
        private readonly CardData[] CardData;

        public DeckStatistics(CardData[] CardData)
        {
            InitializeComponent();

            this.CardData = CardData;

            RatioChart.BackgroundImage = ResourcesLib.GetImageFromBackgrounds("card_bg");
            RatioChart.BackgroundImageLayout = ImageLayout.Stretch;

            CostChart.BackgroundImage = ResourcesLib.GetImageFromBackgrounds("card_bg");
            CostChart.BackgroundImageLayout = ImageLayout.Stretch;

            // Using lists, this allows us to obtain: (1)Qty total of each(&total), (2)Cost of each(&total)
            List<int> AbilitiesCost = new List<int>();
            List<int> ItemsCost = new List<int>();
            List<int> TacticsCost = new List<int>();
            List<int> UnitsCost = new List<int>();

            foreach (CardData Card in CardData)
            {
                switch (Card.GetType())
                {
                    case "Ability":
                        AbilitiesCost.Add(Card.GetCost());
                        break;
                    case "Item":
                        ItemsCost.Add(Card.GetCost());
                        break;
                    case "Tactic":
                        TacticsCost.Add(Card.GetCost());
                        break;
                    case "Unit":
                        UnitsCost.Add(Card.GetCost());
                        break;
                    default:
                        System.Console.WriteLine("Statistics Error: " + Card.GetType());
                        break;
                }
            }

            // Graph 1 (Pie Chart)
            int TotalQty = AbilitiesCost.Count + ItemsCost.Count + TacticsCost.Count + UnitsCost.Count;
            RatioChart.Series["Series1"].Points.AddXY("Abilities - " + DeckBuilder.GetStringValue(TotalQty, AbilitiesCost.Count), AbilitiesCost.Count);
            RatioChart.Series["Series1"].Points.AddXY("Items - " + DeckBuilder.GetStringValue(TotalQty, ItemsCost.Count), ItemsCost.Count);
            RatioChart.Series["Series1"].Points.AddXY("Tactics - " + DeckBuilder.GetStringValue(TotalQty, TacticsCost.Count), TacticsCost.Count);
            RatioChart.Series["Series1"].Points.AddXY("Units - " + DeckBuilder.GetStringValue(TotalQty, UnitsCost.Count), UnitsCost.Count);
            RatioChart.Series["Series1"]["PieLabelStyle"] = "Disabled";

            // Graph 2 (Stacked Bar Chart)
            int TotalAbilityCost = 0;
            int TotalItemCost = 0;
            int TotalTacticCost = 0;
            int TotalUnitCost = 0;

            for (int i = 0; i < AbilitiesCost.Count; i++)
                TotalAbilityCost += AbilitiesCost[i];

            for (int i = 0; i < ItemsCost.Count; i++)
                TotalItemCost += ItemsCost[i];

            for (int i = 0; i < TacticsCost.Count; i++)
                TotalTacticCost += TacticsCost[i];

            for (int i = 0; i < UnitsCost.Count; i++)
                TotalUnitCost += UnitsCost[i];

            int TotalCost = TotalAbilityCost + TotalItemCost + TotalTacticCost + TotalUnitCost;

            // Cost range is [0, 10]
            for (int i = 0; i < 10; i++)
            {
                int TotalQtyForCurrentCost = 0;

                // Ability Cost
                int QtyForCurrentCost = 0;
                foreach (int Cost in AbilitiesCost)
                    if (Cost == i)
                        QtyForCurrentCost++;
                TotalQtyForCurrentCost += QtyForCurrentCost;
                int x = CostChart.Series["Abilities"].Points.AddXY(i, QtyForCurrentCost);

                // Item Cost
                QtyForCurrentCost = 0;
                foreach (int Cost in ItemsCost)
                    if (Cost == i)
                        QtyForCurrentCost++;
                TotalQtyForCurrentCost += QtyForCurrentCost;
                CostChart.Series["Items"].Points.AddXY(i, QtyForCurrentCost);

                // Tactic Cost
                QtyForCurrentCost = 0;
                foreach (int Cost in TacticsCost)
                    if (Cost == i)
                        QtyForCurrentCost++;
                TotalQtyForCurrentCost += QtyForCurrentCost;
                CostChart.Series["Tactics"].Points.AddXY(i, QtyForCurrentCost);

                QtyForCurrentCost = 0;
                foreach (int Cost in UnitsCost)
                    if (Cost == i)
                        QtyForCurrentCost++;
                CostChart.Series["Units"].Points.AddXY(i, QtyForCurrentCost);

                TotalQtyForCurrentCost += QtyForCurrentCost;

                // Check if anything was placed
                if (TotalQtyForCurrentCost > 0)
                {
                    CostChart.Series["Abilities"].Points[x].Label = TotalQtyForCurrentCost.ToString();
                }
            }

            // Set the legend info
            CostChart.Series["Abilities"].LegendText = "Abilities - " + DeckBuilder.GetStringValue(TotalCost, TotalAbilityCost);
            CostChart.Series["Items"].LegendText = "Items - " + DeckBuilder.GetStringValue(TotalCost, TotalItemCost);
            CostChart.Series["Tactics"].LegendText = "Tactics - " + DeckBuilder.GetStringValue(TotalCost, TotalTacticCost);
            CostChart.Series["Units"].LegendText = "Units - " + DeckBuilder.GetStringValue(TotalCost, TotalUnitCost);
        }
    }
}
