﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Deck;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class OpenDeck : UserControl
    {
        private readonly DeckBuilder DeckBuilder;
        private readonly CreateMatch CreateMatch;
        private readonly Scenarios Scenarios;

        private readonly string[] RequiredArchetypes;

        private DeckPacket[] DeckPackets;

        private Card AvatarCard;

        private bool HideStarters;

        public OpenDeck(DeckBuilder DeckBuilder)
        {
            InitializeComponent();

            this.DeckBuilder = DeckBuilder;

            Initialize();
        }
        public OpenDeck(Scenarios Scenarios, string[] RequiredArchetypes)
        {
            InitializeComponent();

            this.Scenarios = Scenarios;
            this.RequiredArchetypes = RequiredArchetypes;

            Initialize();
        }


        public OpenDeck(CreateMatch CreateMatch)
        {
            InitializeComponent();

            this.CreateMatch = CreateMatch;

            Initialize();
        }

        private void Initialize()
        {
            ButtonAnimation.FormatButton(OpenButton);
            ButtonAnimation.FormatButton(CancelButton);
            ButtonAnimation.FormatButton(DeleteButton);
            ButtonAnimation.ToggleCheckBox(HideStartersCheckBox, false);

            // Disable until player chooses a valid deck
            ButtonAnimation.DisableButton(OpenButton);

            // Set the validation ImageList
            OnlineDecksListView.SmallImageList = ResourcesLib.ICONS_IMAGES;

            DeckPackets = PacketService.GetDecksPacket().GetDeckPackets();

            for (int i = 0; i < DeckPackets.Length; i++)
            {
                bool DeckIsGood = DeckService.DeckIsGoodForScenario(DeckPackets[i].GetCardIds(), RequiredArchetypes);
                OnlineDecksListView.Items.Add(DeckPackets[i].GetDeckName(), DeckIsGood ? "yes" : "no");
            }

            OnlineColumnHeader.Width = -2;

            if (OnlineDecksListView.Items.Count > 0)
                OnlineDecksListView.Items[0].Selected = true;
        }

        private void OpenButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (OnlineDecksListView.SelectedItems.Count > 0)
            {
                if (DeckBuilder != null)
                    DeckBuilder.OpenDeck(DeckPackets[OnlineDecksListView.SelectedItems[0].Index].GetCardIds());
                else if (CreateMatch != null)
                    CreateMatch.ChooseDeck(OnlineDecksListView.SelectedItems[0].Text);
                else if (Scenarios != null)
                    Scenarios.ChooseDeck(DeckPackets[OnlineDecksListView.SelectedItems[0].Index]);
                Parent.Dispose();
            }
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            Parent.Dispose();
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            if (OnlineDecksListView.SelectedItems.Count == 0)
                return;

            SoundService.PlaySound("button");
            PacketService.SendPacket(Commands.DECK_DELETE, new CommandPacket(new string[] { OnlineDecksListView.SelectedItems[0].Text }));
            OnlineDecksListView.Items.Remove(OnlineDecksListView.SelectedItems[0]);
        }

        private void OnlineDecksListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (OnlineDecksListView.SelectedItems.Count > 0)
            {
                // Show the avatar of the selected pack
                DeckPacket Packet = DeckPackets[OnlineDecksListView.SelectedItems[0].Index];

                // Go through each card to find the avatar
                foreach (int CardId in Packet.GetCardIds().Keys)
                {
                    CardData CardData = TCGData.Cards.GetCardData(CardId);

                    // We found it, create the preview card
                    if (CardData.GetType().Equals("Avatar"))
                    {
                        if (AvatarCard == null)
                        {
                            AvatarCard = new Card(CardData, Card.SIZE.LARGE);
                            AvatarCard.Location = new Point(TitleLabel.Location.X - AvatarCard.Width - 10, TitleLabel.Location.Y);
                            Controls.Add(AvatarCard);
                        }
                        else
                        {
                            AvatarCard.ChangeCard(CardData);
                            AvatarCard.Visible = true;
                        }
                        CardsInDeckLabel.Text = "Total Cards In Deck: " + TCGData.Cards.GetTotalNumberOfCards(Packet.GetCardIds());
                        CardsInDeckLabel.Location = new Point(AvatarCard.Location.X, AvatarCard.Location.Y + AvatarCard.Height + 10);
                        CardsInDeckLabel.Visible = true;
                        break;
                    }
                }

                if (OnlineDecksListView.SelectedItems[0].ImageKey.Equals("yes"))
                    ButtonAnimation.EnableButton(OpenButton);
                else
                    ButtonAnimation.DisableButton(OpenButton);
            }
            else
            {
                if (AvatarCard != null)
                {
                    AvatarCard.Dispose();
                    AvatarCard = null;

                    CardsInDeckLabel.Visible = false;

                    ButtonAnimation.DisableButton(OpenButton);
                }
            }
        }

        private void HideStartersCheckBox_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            HideStarters = !HideStarters;


            ButtonAnimation.ToggleCheckBox(HideStartersCheckBox, HideStarters);
        }
    }
}
