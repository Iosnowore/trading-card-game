﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Trade;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Screens;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class PostedTrades : UserControl
    {
        private readonly PostedTradesPacket PostedTradesPacket;

        private Window DetailWindow;

        public PostedTrades()
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(CloseButton);
            ButtonAnimation.FormatButton(FilterButton);
            ButtonAnimation.FormatButton(CreateButton);

            PostedTradesPacket = PacketService.GetPostedTradesPacket();

            PostedTradePacket PostedTrade;
            string[] Row;

            // Prepare the main posted trade row
            for (int i = 0; i < PostedTradesPacket.GetNumberOfPostedTrades(); i++)
            {
                // Prepare the posted trade row for each card

                PostedTrade = PostedTradesPacket.GetPostedTrade(i);

                int[] OfferedCardIds = new int[PostedTrade.GetOffered().Count];
                PostedTrade.GetOffered().Keys.CopyTo(OfferedCardIds, 0);

                int[] WantCardIds = new int[PostedTrade.GetWant().Count];
                PostedTrade.GetWant().Keys.CopyTo(WantCardIds, 0);

                int MaxNumberOfRows = OfferedCardIds.Length > WantCardIds.Length ? OfferedCardIds.Length : WantCardIds.Length;

                for (int j = 0; j < MaxNumberOfRows; j++)
                {
                    string OfferedCard = string.Empty;

                    if (OfferedCardIds.Length > j)
                    {
                        PostedTrade.GetOffered().TryGetValue(OfferedCardIds[j], out int OfferedCardQuantity);
                        OfferedCard = FormatCardString(OfferedCardIds[j], OfferedCardQuantity);
                    }

                    string WantCard = string.Empty;

                    if (WantCardIds.Length > j)
                    {
                        PostedTrade.GetWant().TryGetValue(WantCardIds[j], out int WantCardQuantity);
                        WantCard = FormatCardString(WantCardIds[j], WantCardQuantity);
                    }

                    if (j == 0)
                    {
                        Row = new string[] { PostedTrade.GetId().ToString(), PostedTrade.GetUsername(), OfferedCard, WantCard, PostedTrade.GetDatePosted() };
                    }
                    else
                    {
                        Row = new string[] { string.Empty, string.Empty, OfferedCard, WantCard, PostedTrade.GetDatePosted() };
                    }
                    ListViewItem Item = new ListViewItem(Row);
                    // Set the tag to be the real index of the posted trade
                    Item.Tag = i;

                    // Set the background of the trade
                    if (i % 2 == 1)
                        Item.BackColor = Color.DimGray;

                    ListView.Items.Add(Item);
                }
            }

            PostedTradesLabel.Text = PostedTradesPacket.GetNumberOfPostedTrades() + " posted trades.";
        }

        private string FormatCardString(int CardId, int CardQuantity)
        {
            CardData CardData = TCGData.Cards.GetCardData(CardId);

            string FormattedString = CardQuantity + " x " + CardData.GetCollectorInfo() + " " + CardData.GetTitle();

            return FormattedString;
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            Parent.Dispose();
        }

        private void FilterButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void CreateButton_Click(object sender, System.EventArgs e)
        {
            Parent.Parent.Controls.Add(new Window(new Trade(null, null, PacketService.GetCollectionPacket()), "Trade Creation"));
            SoundService.PlaySound("button");
        }

        private void ListView_ItemActivate(object sender, System.EventArgs e)
        {
            PostedTradePacket PostedTrade = PostedTradesPacket.GetPostedTrade((int)ListView.SelectedItems[0].Tag);

            if (DetailWindow != null && !DetailWindow.IsDisposed)
            {
                DetailWindow.Dispose();
            }

            DetailWindow = new Window(new PostedTradeDetail(PostedTrade.GetOffered(), PostedTrade.GetWant(), PostedTrade.GetId(), PostedTrade.GetUsername()), "Posted Trade #" + ListView.SelectedItems[0].Text);

            Parent.Parent.Controls.Add(DetailWindow);
        }
    }
}
