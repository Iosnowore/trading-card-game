﻿namespace TradingCardGame.forms.dialogs
{
    partial class LobbyChooser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.joinButton = new System.Windows.Forms.Button();
            this.mosEisleyButton = new System.Windows.Forms.Panel();
            this.MosEisleyInfoLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.toscheStationButton = new System.Windows.Forms.Panel();
            this.ToscheStationInfoLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.mosEspaButton = new System.Windows.Forms.Panel();
            this.MosEspaInfoLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.CasualGamesButton = new System.Windows.Forms.Panel();
            this.casualInfoLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mosEisleyButton.SuspendLayout();
            this.toscheStationButton.SuspendLayout();
            this.mosEspaButton.SuspendLayout();
            this.CasualGamesButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Transparent;
            this.cancelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(470, 378);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(111, 29);
            this.cancelButton.TabIndex = 56;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // joinButton
            // 
            this.joinButton.BackColor = System.Drawing.Color.Transparent;
            this.joinButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.joinButton.FlatAppearance.BorderSize = 0;
            this.joinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.joinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.joinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.joinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinButton.ForeColor = System.Drawing.Color.Black;
            this.joinButton.Location = new System.Drawing.Point(587, 378);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(111, 29);
            this.joinButton.TabIndex = 55;
            this.joinButton.Text = "Join";
            this.joinButton.UseVisualStyleBackColor = false;
            this.joinButton.Click += new System.EventHandler(this.JoinButton_Click);
            // 
            // mosEisleyButton
            // 
            this.mosEisleyButton.BackColor = System.Drawing.Color.Black;
            this.mosEisleyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mosEisleyButton.Controls.Add(this.MosEisleyInfoLabel);
            this.mosEisleyButton.Controls.Add(this.label15);
            this.mosEisleyButton.Location = new System.Drawing.Point(366, 65);
            this.mosEisleyButton.Name = "mosEisleyButton";
            this.mosEisleyButton.Size = new System.Drawing.Size(332, 150);
            this.mosEisleyButton.TabIndex = 51;
            // 
            // MosEisleyInfoLabel
            // 
            this.MosEisleyInfoLabel.AutoSize = true;
            this.MosEisleyInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.MosEisleyInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MosEisleyInfoLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.MosEisleyInfoLabel.Location = new System.Drawing.Point(151, 41);
            this.MosEisleyInfoLabel.Name = "MosEisleyInfoLabel";
            this.MosEisleyInfoLabel.Size = new System.Drawing.Size(106, 48);
            this.MosEisleyInfoLabel.TabIndex = 1;
            this.MosEisleyInfoLabel.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.SkyBlue;
            this.label15.Location = new System.Drawing.Point(6, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mos Eisley";
            // 
            // toscheStationButton
            // 
            this.toscheStationButton.BackColor = System.Drawing.Color.Black;
            this.toscheStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toscheStationButton.Controls.Add(this.ToscheStationInfoLabel);
            this.toscheStationButton.Controls.Add(this.label13);
            this.toscheStationButton.Location = new System.Drawing.Point(366, 222);
            this.toscheStationButton.Name = "toscheStationButton";
            this.toscheStationButton.Size = new System.Drawing.Size(332, 150);
            this.toscheStationButton.TabIndex = 50;
            // 
            // ToscheStationInfoLabel
            // 
            this.ToscheStationInfoLabel.AutoSize = true;
            this.ToscheStationInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.ToscheStationInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToscheStationInfoLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.ToscheStationInfoLabel.Location = new System.Drawing.Point(151, 41);
            this.ToscheStationInfoLabel.Name = "ToscheStationInfoLabel";
            this.ToscheStationInfoLabel.Size = new System.Drawing.Size(106, 48);
            this.ToscheStationInfoLabel.TabIndex = 1;
            this.ToscheStationInfoLabel.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.SkyBlue;
            this.label13.Location = new System.Drawing.Point(6, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(130, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tosche Station";
            // 
            // mosEspaButton
            // 
            this.mosEspaButton.BackColor = System.Drawing.Color.Black;
            this.mosEspaButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mosEspaButton.Controls.Add(this.MosEspaInfoLabel);
            this.mosEspaButton.Controls.Add(this.label17);
            this.mosEspaButton.Location = new System.Drawing.Point(28, 222);
            this.mosEspaButton.Name = "mosEspaButton";
            this.mosEspaButton.Size = new System.Drawing.Size(332, 150);
            this.mosEspaButton.TabIndex = 49;
            // 
            // MosEspaInfoLabel
            // 
            this.MosEspaInfoLabel.AutoSize = true;
            this.MosEspaInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.MosEspaInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MosEspaInfoLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.MosEspaInfoLabel.Location = new System.Drawing.Point(151, 41);
            this.MosEspaInfoLabel.Name = "MosEspaInfoLabel";
            this.MosEspaInfoLabel.Size = new System.Drawing.Size(106, 48);
            this.MosEspaInfoLabel.TabIndex = 1;
            this.MosEspaInfoLabel.Text = "Users: 0 / 8\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.SkyBlue;
            this.label17.Location = new System.Drawing.Point(6, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Mos Espa";
            // 
            // CasualGamesButton
            // 
            this.CasualGamesButton.BackColor = System.Drawing.Color.Black;
            this.CasualGamesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CasualGamesButton.Controls.Add(this.casualInfoLabel);
            this.CasualGamesButton.Controls.Add(this.label1);
            this.CasualGamesButton.Location = new System.Drawing.Point(28, 65);
            this.CasualGamesButton.Name = "CasualGamesButton";
            this.CasualGamesButton.Size = new System.Drawing.Size(332, 150);
            this.CasualGamesButton.TabIndex = 48;
            this.CasualGamesButton.Click += new System.EventHandler(this.CasualGamesButton_Click);
            // 
            // casualInfoLabel
            // 
            this.casualInfoLabel.AutoSize = true;
            this.casualInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.casualInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.casualInfoLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.casualInfoLabel.Location = new System.Drawing.Point(151, 41);
            this.casualInfoLabel.Name = "casualInfoLabel";
            this.casualInfoLabel.Size = new System.Drawing.Size(106, 48);
            this.casualInfoLabel.TabIndex = 1;
            this.casualInfoLabel.Text = "Users: 0 / 6\r\nTotal Matches: 0\r\nWaiting: 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Casual Games";
            // 
            // LobbyChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.mosEisleyButton);
            this.Controls.Add(this.toscheStationButton);
            this.Controls.Add(this.mosEspaButton);
            this.Controls.Add(this.CasualGamesButton);
            this.DoubleBuffered = true;
            this.Name = "LobbyChooser";
            this.Size = new System.Drawing.Size(727, 473);
            this.mosEisleyButton.ResumeLayout(false);
            this.mosEisleyButton.PerformLayout();
            this.toscheStationButton.ResumeLayout(false);
            this.toscheStationButton.PerformLayout();
            this.mosEspaButton.ResumeLayout(false);
            this.mosEspaButton.PerformLayout();
            this.CasualGamesButton.ResumeLayout(false);
            this.CasualGamesButton.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.Panel mosEisleyButton;
        private System.Windows.Forms.Label MosEisleyInfoLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel toscheStationButton;
        private System.Windows.Forms.Label ToscheStationInfoLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel mosEspaButton;
        private System.Windows.Forms.Label MosEspaInfoLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel CasualGamesButton;
        private System.Windows.Forms.Label casualInfoLabel;
        private System.Windows.Forms.Label label1;
    }
}
