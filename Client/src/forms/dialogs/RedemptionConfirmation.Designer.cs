﻿
namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class RedemptionConfirmation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.ChoiceAllowedLabel = new System.Windows.Forms.Label();
            this.ChooseButton = new System.Windows.Forms.Button();
            this.TermsButton = new System.Windows.Forms.Button();
            this.RedeemButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(350, 0);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(150, 31);
            this.TitleLabel.TabIndex = 0;
            this.TitleLabel.Text = "Title Label";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DescriptionLabel.Location = new System.Drawing.Point(438, 107);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.DescriptionLabel.TabIndex = 1;
            this.DescriptionLabel.Text = "Description";
            // 
            // ChoiceAllowedLabel
            // 
            this.ChoiceAllowedLabel.AutoSize = true;
            this.ChoiceAllowedLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.ChoiceAllowedLabel.Location = new System.Drawing.Point(438, 132);
            this.ChoiceAllowedLabel.Name = "ChoiceAllowedLabel";
            this.ChoiceAllowedLabel.Size = new System.Drawing.Size(307, 13);
            this.ChoiceAllowedLabel.TabIndex = 2;
            this.ChoiceAllowedLabel.Text = "This item requires that you choose the item that you will receive.";
            // 
            // ChooseButton
            // 
            this.ChooseButton.Location = new System.Drawing.Point(670, 148);
            this.ChooseButton.Name = "ChooseButton";
            this.ChooseButton.Size = new System.Drawing.Size(75, 23);
            this.ChooseButton.TabIndex = 3;
            this.ChooseButton.Text = "Choose";
            this.ChooseButton.UseVisualStyleBackColor = true;
            this.ChooseButton.Click += new System.EventHandler(this.ChooseButton_Click);
            // 
            // TermsButton
            // 
            this.TermsButton.Location = new System.Drawing.Point(690, 545);
            this.TermsButton.Name = "TermsButton";
            this.TermsButton.Size = new System.Drawing.Size(131, 23);
            this.TermsButton.TabIndex = 4;
            this.TermsButton.Text = "Terms / Conditions";
            this.TermsButton.UseVisualStyleBackColor = true;
            // 
            // RedeemButton
            // 
            this.RedeemButton.Location = new System.Drawing.Point(746, 574);
            this.RedeemButton.Name = "RedeemButton";
            this.RedeemButton.Size = new System.Drawing.Size(75, 23);
            this.RedeemButton.TabIndex = 5;
            this.RedeemButton.Text = "Redeem";
            this.RedeemButton.UseVisualStyleBackColor = true;
            this.RedeemButton.Click += new System.EventHandler(this.RedeemButton_Click);
            // 
            // RedemptionConfirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RedeemButton);
            this.Controls.Add(this.TermsButton);
            this.Controls.Add(this.ChooseButton);
            this.Controls.Add(this.ChoiceAllowedLabel);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.TitleLabel);
            this.Name = "RedemptionConfirmation";
            this.Size = new System.Drawing.Size(850, 642);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Label ChoiceAllowedLabel;
        private System.Windows.Forms.Button ChooseButton;
        private System.Windows.Forms.Button TermsButton;
        private System.Windows.Forms.Button RedeemButton;
    }
}
