﻿namespace TradingCardGame.forms.dialogs.preferences
{
    partial class DisplaySound
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.soundButtonOff = new System.Windows.Forms.RadioButton();
            this.soundButtonOn = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.MusicTrackBar = new System.Windows.Forms.TrackBar();
            this.SoundTrackBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MusicTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoundTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.soundButtonOff);
            this.panel2.Controls.Add(this.soundButtonOn);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.MusicTrackBar);
            this.panel2.Controls.Add(this.SoundTrackBar);
            this.panel2.Location = new System.Drawing.Point(44, 79);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(275, 180);
            this.panel2.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(10, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Music Volume";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Location = new System.Drawing.Point(10, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Sound Volume";
            // 
            // soundButtonOff
            // 
            this.soundButtonOff.AutoSize = true;
            this.soundButtonOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soundButtonOff.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.soundButtonOff.Location = new System.Drawing.Point(159, 14);
            this.soundButtonOff.Name = "soundButtonOff";
            this.soundButtonOff.Size = new System.Drawing.Size(42, 20);
            this.soundButtonOff.TabIndex = 6;
            this.soundButtonOff.TabStop = true;
            this.soundButtonOff.Text = "Off";
            this.soundButtonOff.UseVisualStyleBackColor = true;
            // 
            // soundButtonOn
            // 
            this.soundButtonOn.AutoSize = true;
            this.soundButtonOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soundButtonOn.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.soundButtonOn.Location = new System.Drawing.Point(75, 14);
            this.soundButtonOn.Name = "soundButtonOn";
            this.soundButtonOn.Size = new System.Drawing.Size(43, 20);
            this.soundButtonOn.TabIndex = 5;
            this.soundButtonOn.TabStop = true;
            this.soundButtonOn.Text = "On";
            this.soundButtonOn.UseVisualStyleBackColor = true;
            this.soundButtonOn.CheckedChanged += new System.EventHandler(this.SoundButtonOn_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(10, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sound";
            // 
            // MusicTrackBar
            // 
            this.MusicTrackBar.LargeChange = 1;
            this.MusicTrackBar.Location = new System.Drawing.Point(3, 126);
            this.MusicTrackBar.Name = "MusicTrackBar";
            this.MusicTrackBar.Size = new System.Drawing.Size(265, 45);
            this.MusicTrackBar.TabIndex = 1;
            this.MusicTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.MusicTrackBar.ValueChanged += new System.EventHandler(this.MusicTrackBar_ValueChanged);
            // 
            // SoundTrackBar
            // 
            this.SoundTrackBar.LargeChange = 1;
            this.SoundTrackBar.Location = new System.Drawing.Point(3, 62);
            this.SoundTrackBar.Name = "SoundTrackBar";
            this.SoundTrackBar.Size = new System.Drawing.Size(265, 45);
            this.SoundTrackBar.TabIndex = 0;
            this.SoundTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.SoundTrackBar.ValueChanged += new System.EventHandler(this.SoundTrackBar_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(56, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Audio Settings";
            // 
            // DisplaySound
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Name = "DisplaySound";
            this.Size = new System.Drawing.Size(767, 436);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MusicTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoundTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar SoundTrackBar;
        private System.Windows.Forms.TrackBar MusicTrackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton soundButtonOn;
        private System.Windows.Forms.RadioButton soundButtonOff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
    }
}
