﻿namespace TradingCardGame.forms.dialogs.preferences
{
    partial class Avatar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CurrentAvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FocusedPictureBox = new System.Windows.Forms.PictureBox();
            this.RightPictureBox = new System.Windows.Forms.PictureBox();
            this.LeftPictureBox = new System.Windows.Forms.PictureBox();
            this.LeftButton = new System.Windows.Forms.Button();
            this.RightButton = new System.Windows.Forms.Button();
            this.CustomCheckBox = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DefaultCheckBox = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentAvatarPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusedPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CurrentAvatarPictureBox
            // 
            this.CurrentAvatarPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CurrentAvatarPictureBox.Location = new System.Drawing.Point(616, 48);
            this.CurrentAvatarPictureBox.Name = "CurrentAvatarPictureBox";
            this.CurrentAvatarPictureBox.Size = new System.Drawing.Size(85, 85);
            this.CurrentAvatarPictureBox.TabIndex = 0;
            this.CurrentAvatarPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(573, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Current Selection:";
            // 
            // FocusedPictureBox
            // 
            this.FocusedPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FocusedPictureBox.Location = new System.Drawing.Point(270, 114);
            this.FocusedPictureBox.Name = "FocusedPictureBox";
            this.FocusedPictureBox.Size = new System.Drawing.Size(85, 85);
            this.FocusedPictureBox.TabIndex = 2;
            this.FocusedPictureBox.TabStop = false;
            // 
            // RightPictureBox
            // 
            this.RightPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightPictureBox.Location = new System.Drawing.Point(365, 125);
            this.RightPictureBox.Name = "RightPictureBox";
            this.RightPictureBox.Size = new System.Drawing.Size(70, 70);
            this.RightPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RightPictureBox.TabIndex = 3;
            this.RightPictureBox.TabStop = false;
            // 
            // LeftPictureBox
            // 
            this.LeftPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftPictureBox.Location = new System.Drawing.Point(190, 125);
            this.LeftPictureBox.Name = "LeftPictureBox";
            this.LeftPictureBox.Size = new System.Drawing.Size(70, 70);
            this.LeftPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LeftPictureBox.TabIndex = 4;
            this.LeftPictureBox.TabStop = false;
            // 
            // LeftButton
            // 
            this.LeftButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.LeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftButton.Location = new System.Drawing.Point(59, 294);
            this.LeftButton.Name = "LeftButton";
            this.LeftButton.Size = new System.Drawing.Size(80, 28);
            this.LeftButton.TabIndex = 5;
            this.LeftButton.Text = "<<";
            this.LeftButton.UseVisualStyleBackColor = true;
            this.LeftButton.Click += new System.EventHandler(this.LeftButton_Click);
            // 
            // RightButton
            // 
            this.RightButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightButton.Location = new System.Drawing.Point(145, 294);
            this.RightButton.Name = "RightButton";
            this.RightButton.Size = new System.Drawing.Size(80, 28);
            this.RightButton.TabIndex = 6;
            this.RightButton.Text = ">>";
            this.RightButton.UseVisualStyleBackColor = true;
            this.RightButton.Click += new System.EventHandler(this.RightButton_Click);
            // 
            // CustomCheckBox
            // 
            this.CustomCheckBox.FlatAppearance.BorderSize = 0;
            this.CustomCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CustomCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CustomCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheckBox.Location = new System.Drawing.Point(576, 214);
            this.CustomCheckBox.Name = "CustomCheckBox";
            this.CustomCheckBox.Size = new System.Drawing.Size(23, 20);
            this.CustomCheckBox.TabIndex = 7;
            this.CustomCheckBox.UseVisualStyleBackColor = true;
            this.CustomCheckBox.Click += new System.EventHandler(this.CustomCheckBox_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(573, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Include:";
            // 
            // DefaultCheckBox
            // 
            this.DefaultCheckBox.FlatAppearance.BorderSize = 0;
            this.DefaultCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DefaultCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DefaultCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DefaultCheckBox.Location = new System.Drawing.Point(576, 240);
            this.DefaultCheckBox.Name = "DefaultCheckBox";
            this.DefaultCheckBox.Size = new System.Drawing.Size(23, 20);
            this.DefaultCheckBox.TabIndex = 9;
            this.DefaultCheckBox.UseVisualStyleBackColor = true;
            this.DefaultCheckBox.Click += new System.EventHandler(this.DefaultCheckBox_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(605, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Custom / Unique (0)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(605, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Default (30)";
            // 
            // Avatar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DefaultCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CustomCheckBox);
            this.Controls.Add(this.RightButton);
            this.Controls.Add(this.LeftButton);
            this.Controls.Add(this.LeftPictureBox);
            this.Controls.Add(this.RightPictureBox);
            this.Controls.Add(this.FocusedPictureBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CurrentAvatarPictureBox);
            this.Name = "Avatar";
            this.Size = new System.Drawing.Size(767, 436);
            ((System.ComponentModel.ISupportInitialize)(this.CurrentAvatarPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FocusedPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox CurrentAvatarPictureBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox FocusedPictureBox;
        private System.Windows.Forms.PictureBox RightPictureBox;
        private System.Windows.Forms.PictureBox LeftPictureBox;
        private System.Windows.Forms.Button LeftButton;
        private System.Windows.Forms.Button RightButton;
        private System.Windows.Forms.Button CustomCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button DefaultCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
