﻿namespace TradingCardGame.forms.dialogs.preferences
{
    partial class AccountInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.HomePageButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.HomePageTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.InstantMessengerButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.InstantMessengerTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PersonalMessageTextBox = new System.Windows.Forms.RichTextBox();
            this.PersonalMessageButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.HomePageButton);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.HomePageTextBox);
            this.panel2.Location = new System.Drawing.Point(6, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(754, 50);
            this.panel2.TabIndex = 1;
            // 
            // HomePageButton
            // 
            this.HomePageButton.BackColor = System.Drawing.Color.Transparent;
            this.HomePageButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomePageButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.HomePageButton.FlatAppearance.BorderSize = 0;
            this.HomePageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.HomePageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.HomePageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HomePageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomePageButton.ForeColor = System.Drawing.Color.Black;
            this.HomePageButton.Location = new System.Drawing.Point(636, 8);
            this.HomePageButton.Name = "HomePageButton";
            this.HomePageButton.Size = new System.Drawing.Size(111, 30);
            this.HomePageButton.TabIndex = 49;
            this.HomePageButton.Text = "Nobody";
            this.HomePageButton.UseVisualStyleBackColor = false;
            this.HomePageButton.Click += new System.EventHandler(this.HomePageButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(581, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Visible to";
            // 
            // HomePageTextBox
            // 
            this.HomePageTextBox.BackColor = System.Drawing.Color.Black;
            this.HomePageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HomePageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomePageTextBox.ForeColor = System.Drawing.Color.White;
            this.HomePageTextBox.Location = new System.Drawing.Point(3, 16);
            this.HomePageTextBox.MaxLength = 30;
            this.HomePageTextBox.Name = "HomePageTextBox";
            this.HomePageTextBox.Size = new System.Drawing.Size(572, 22);
            this.HomePageTextBox.TabIndex = 0;
            this.HomePageTextBox.TextChanged += new System.EventHandler(this.HomePageTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(19, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Home Page";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.InstantMessengerButton);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.InstantMessengerTextBox);
            this.panel3.Location = new System.Drawing.Point(6, 118);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(754, 50);
            this.panel3.TabIndex = 2;
            // 
            // InstantMessengerButton
            // 
            this.InstantMessengerButton.BackColor = System.Drawing.Color.Transparent;
            this.InstantMessengerButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.InstantMessengerButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.InstantMessengerButton.FlatAppearance.BorderSize = 0;
            this.InstantMessengerButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.InstantMessengerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.InstantMessengerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InstantMessengerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstantMessengerButton.ForeColor = System.Drawing.Color.Black;
            this.InstantMessengerButton.Location = new System.Drawing.Point(636, 8);
            this.InstantMessengerButton.Name = "InstantMessengerButton";
            this.InstantMessengerButton.Size = new System.Drawing.Size(111, 30);
            this.InstantMessengerButton.TabIndex = 49;
            this.InstantMessengerButton.Text = "Nobody";
            this.InstantMessengerButton.UseVisualStyleBackColor = false;
            this.InstantMessengerButton.Click += new System.EventHandler(this.InstantMessengerButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(581, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Visible to";
            // 
            // InstantMessengerTextBox
            // 
            this.InstantMessengerTextBox.BackColor = System.Drawing.Color.Black;
            this.InstantMessengerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InstantMessengerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstantMessengerTextBox.ForeColor = System.Drawing.Color.White;
            this.InstantMessengerTextBox.Location = new System.Drawing.Point(3, 16);
            this.InstantMessengerTextBox.MaxLength = 20;
            this.InstantMessengerTextBox.Name = "InstantMessengerTextBox";
            this.InstantMessengerTextBox.Size = new System.Drawing.Size(572, 22);
            this.InstantMessengerTextBox.TabIndex = 0;
            this.InstantMessengerTextBox.TextChanged += new System.EventHandler(this.InstantMessengerTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.SkyBlue;
            this.label5.Location = new System.Drawing.Point(19, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Instant Messenger";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.PersonalMessageTextBox);
            this.panel4.Controls.Add(this.PersonalMessageButton);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(6, 196);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(754, 190);
            this.panel4.TabIndex = 4;
            // 
            // PersonalMessageTextBox
            // 
            this.PersonalMessageTextBox.BackColor = System.Drawing.Color.Black;
            this.PersonalMessageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PersonalMessageTextBox.ForeColor = System.Drawing.Color.White;
            this.PersonalMessageTextBox.Location = new System.Drawing.Point(3, 15);
            this.PersonalMessageTextBox.Name = "PersonalMessageTextBox";
            this.PersonalMessageTextBox.Size = new System.Drawing.Size(744, 132);
            this.PersonalMessageTextBox.TabIndex = 50;
            this.PersonalMessageTextBox.Text = "";
            this.PersonalMessageTextBox.TextChanged += new System.EventHandler(this.PersonalMessageTextBox_TextChanged);
            // 
            // PersonalMessageButton
            // 
            this.PersonalMessageButton.BackColor = System.Drawing.Color.Transparent;
            this.PersonalMessageButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PersonalMessageButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PersonalMessageButton.FlatAppearance.BorderSize = 0;
            this.PersonalMessageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PersonalMessageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PersonalMessageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PersonalMessageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PersonalMessageButton.ForeColor = System.Drawing.Color.Black;
            this.PersonalMessageButton.Location = new System.Drawing.Point(636, 153);
            this.PersonalMessageButton.Name = "PersonalMessageButton";
            this.PersonalMessageButton.Size = new System.Drawing.Size(111, 30);
            this.PersonalMessageButton.TabIndex = 49;
            this.PersonalMessageButton.Text = "Nobody";
            this.PersonalMessageButton.UseVisualStyleBackColor = false;
            this.PersonalMessageButton.Click += new System.EventHandler(this.PersonalMessageButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.SkyBlue;
            this.label6.Location = new System.Drawing.Point(581, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Visible to";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(19, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Personal Message";
            // 
            // AccountInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Name = "AccountInfo";
            this.Size = new System.Drawing.Size(767, 436);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox HomePageTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button HomePageButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button InstantMessengerButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox InstantMessengerTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button PersonalMessageButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox PersonalMessageTextBox;
    }
}
