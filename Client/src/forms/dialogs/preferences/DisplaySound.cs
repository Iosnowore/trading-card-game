﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.forms.dialogs.preferences
{
    public partial class DisplaySound : UserControl
    {
        private readonly Preferences Preferences;

        private readonly bool Loading = true;

        public DisplaySound(Preferences Preferences)
        {
            InitializeComponent();

            this.Preferences = Preferences;

            soundButtonOff.Checked = !SettingsService.GetBoolFromSettingsXml("Audio", "Sound");
            soundButtonOn.Checked = !soundButtonOff.Checked;

            if (soundButtonOff.Checked)
            {
                MusicTrackBar.Enabled = false;
                SoundTrackBar.Enabled = false;
            }
            else
            {
                SoundTrackBar.Value = (int)(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "SoundVolume")) * 10);
                MusicTrackBar.Value = (int)(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume")) * 10);
            }
            Loading = false;
        }

        public bool SoundIsEnabled()
        {
            return soundButtonOn.Checked;
        }

        private void SoundButtonOn_CheckedChanged(object sender, System.EventArgs e)
        {
            SoundTrackBar.Enabled = soundButtonOn.Checked;
            SoundTrackBar.Value = soundButtonOn.Checked ? 1 : 0;

            MusicTrackBar.Enabled = soundButtonOn.Checked;
            MusicTrackBar.Value = soundButtonOn.Checked ? 1 : 0;

            Preferences.ChangedSetting(Loading);
        }

        private void MusicTrackBar_ValueChanged(object sender, System.EventArgs e)
        {
            if (!Loading)
            {
                // If music wasn't playing upon game load, it will never because volume is only set if provider is set
                if (!MusicService.MusicIsPlaying())
                    MusicService.TransitionMusic("music_lobby_", 2);
                MusicService.SetVolume((float)(MusicTrackBar.Value / 10.0));
                Preferences.ChangedSetting(Loading);
            }
        }

        private void SoundTrackBar_ValueChanged(object sender, System.EventArgs e)
        {
            if (!Loading)
            {
                SoundService.SetVolume((float)(SoundTrackBar.Value / 10.0));
                Preferences.ChangedSetting(Loading);
            }
        }
    }
}
