﻿namespace TradingCardGame.forms.dialogs.preferences
{
    partial class UserInterface
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.MultiPassRenderingOff = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.MultiPassRenderingOn = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.LeftNavigatorPulseOff = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.LeftNavigatorPulseOn = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.RequestAttentionOff = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.RequestAttentionOn = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.LobbyMessagesOff = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.LobbyMessagesOn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.AutoSelectOff = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.AutoSelectOn = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.CardWarningsOff = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.CardWarningsOn = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.PassOff = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.PassOn = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.FoilEffectsOff = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.FoilEffectsStandalone = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.FoilEffectsOn = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.HandDisplayModeFullCard = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.HandDisplayModeFisheye = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.HandDisplayModePopup = new System.Windows.Forms.Button();
            this.BackgroundImageDropdown = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.CollectionManagerFalse = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.CollectionManagerTrue = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.DeckBuilderFalse = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.DeckBuilderTrue = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.QuestFilledButton = new System.Windows.Forms.Button();
            this.CasualLobbyButton = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.TradeLobbyButton = new System.Windows.Forms.Button();
            this.TradeButton = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.MultiplayerGamesButton = new System.Windows.Forms.Button();
            this.ExcessiveAttackButton = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.DeckbuilderButton = new System.Windows.Forms.Button();
            this.CollectionButton = new System.Windows.Forms.Button();
            this.label58 = new System.Windows.Forms.Label();
            this.MainScreenButton = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.DeckLarge = new System.Windows.Forms.Button();
            this.label52 = new System.Windows.Forms.Label();
            this.DeckMedium = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.DeckSmall = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.PlaymatLarge = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.PlaymatMedium = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.PlaymatSmall = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.CollectionLarge = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.CollectionMedium = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.CollectionSmall = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(69, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "General";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.MultiPassRenderingOff);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.MultiPassRenderingOn);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.LeftNavigatorPulseOff);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.LeftNavigatorPulseOn);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.RequestAttentionOff);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.RequestAttentionOn);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.LobbyMessagesOff);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.LobbyMessagesOn);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(62, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 165);
            this.panel2.TabIndex = 4;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label27.Location = new System.Drawing.Point(225, 130);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(24, 16);
            this.label27.TabIndex = 66;
            this.label27.Text = "Off";
            // 
            // MultiPassRenderingOff
            // 
            this.MultiPassRenderingOff.BackColor = System.Drawing.Color.Transparent;
            this.MultiPassRenderingOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MultiPassRenderingOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOff.FlatAppearance.BorderSize = 0;
            this.MultiPassRenderingOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiPassRenderingOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MultiPassRenderingOff.ForeColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOff.Location = new System.Drawing.Point(200, 127);
            this.MultiPassRenderingOff.Name = "MultiPassRenderingOff";
            this.MultiPassRenderingOff.Size = new System.Drawing.Size(22, 22);
            this.MultiPassRenderingOff.TabIndex = 65;
            this.MultiPassRenderingOff.UseVisualStyleBackColor = false;
            this.MultiPassRenderingOff.Click += new System.EventHandler(this.MultiPassRenderingOff_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label28.Location = new System.Drawing.Point(165, 130);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 16);
            this.label28.TabIndex = 64;
            this.label28.Text = "On";
            // 
            // MultiPassRenderingOn
            // 
            this.MultiPassRenderingOn.BackColor = System.Drawing.Color.Transparent;
            this.MultiPassRenderingOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MultiPassRenderingOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOn.FlatAppearance.BorderSize = 0;
            this.MultiPassRenderingOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiPassRenderingOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MultiPassRenderingOn.ForeColor = System.Drawing.Color.Black;
            this.MultiPassRenderingOn.Location = new System.Drawing.Point(140, 127);
            this.MultiPassRenderingOn.Name = "MultiPassRenderingOn";
            this.MultiPassRenderingOn.Size = new System.Drawing.Size(22, 22);
            this.MultiPassRenderingOn.TabIndex = 63;
            this.MultiPassRenderingOn.UseVisualStyleBackColor = false;
            this.MultiPassRenderingOn.Click += new System.EventHandler(this.MultiPassRenderingOn_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label25.Location = new System.Drawing.Point(225, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(24, 16);
            this.label25.TabIndex = 62;
            this.label25.Text = "Off";
            // 
            // LeftNavigatorPulseOff
            // 
            this.LeftNavigatorPulseOff.BackColor = System.Drawing.Color.Transparent;
            this.LeftNavigatorPulseOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftNavigatorPulseOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOff.FlatAppearance.BorderSize = 0;
            this.LeftNavigatorPulseOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftNavigatorPulseOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftNavigatorPulseOff.ForeColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOff.Location = new System.Drawing.Point(200, 93);
            this.LeftNavigatorPulseOff.Name = "LeftNavigatorPulseOff";
            this.LeftNavigatorPulseOff.Size = new System.Drawing.Size(22, 22);
            this.LeftNavigatorPulseOff.TabIndex = 61;
            this.LeftNavigatorPulseOff.UseVisualStyleBackColor = false;
            this.LeftNavigatorPulseOff.Click += new System.EventHandler(this.LeftNavigatorPulseOff_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label26.Location = new System.Drawing.Point(165, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(25, 16);
            this.label26.TabIndex = 60;
            this.label26.Text = "On";
            // 
            // LeftNavigatorPulseOn
            // 
            this.LeftNavigatorPulseOn.BackColor = System.Drawing.Color.Transparent;
            this.LeftNavigatorPulseOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftNavigatorPulseOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOn.FlatAppearance.BorderSize = 0;
            this.LeftNavigatorPulseOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftNavigatorPulseOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftNavigatorPulseOn.ForeColor = System.Drawing.Color.Black;
            this.LeftNavigatorPulseOn.Location = new System.Drawing.Point(140, 93);
            this.LeftNavigatorPulseOn.Name = "LeftNavigatorPulseOn";
            this.LeftNavigatorPulseOn.Size = new System.Drawing.Size(22, 22);
            this.LeftNavigatorPulseOn.TabIndex = 59;
            this.LeftNavigatorPulseOn.UseVisualStyleBackColor = false;
            this.LeftNavigatorPulseOn.Click += new System.EventHandler(this.LeftNavigatorPulseOn_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label23.Location = new System.Drawing.Point(225, 60);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 16);
            this.label23.TabIndex = 58;
            this.label23.Text = "Off";
            // 
            // RequestAttentionOff
            // 
            this.RequestAttentionOff.BackColor = System.Drawing.Color.Transparent;
            this.RequestAttentionOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RequestAttentionOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RequestAttentionOff.FlatAppearance.BorderSize = 0;
            this.RequestAttentionOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RequestAttentionOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RequestAttentionOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RequestAttentionOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequestAttentionOff.ForeColor = System.Drawing.Color.Black;
            this.RequestAttentionOff.Location = new System.Drawing.Point(200, 57);
            this.RequestAttentionOff.Name = "RequestAttentionOff";
            this.RequestAttentionOff.Size = new System.Drawing.Size(22, 22);
            this.RequestAttentionOff.TabIndex = 57;
            this.RequestAttentionOff.UseVisualStyleBackColor = false;
            this.RequestAttentionOff.Click += new System.EventHandler(this.RequestAttentionOff_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label24.Location = new System.Drawing.Point(165, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(25, 16);
            this.label24.TabIndex = 56;
            this.label24.Text = "On";
            // 
            // RequestAttentionOn
            // 
            this.RequestAttentionOn.BackColor = System.Drawing.Color.Transparent;
            this.RequestAttentionOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RequestAttentionOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RequestAttentionOn.FlatAppearance.BorderSize = 0;
            this.RequestAttentionOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RequestAttentionOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RequestAttentionOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RequestAttentionOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequestAttentionOn.ForeColor = System.Drawing.Color.Black;
            this.RequestAttentionOn.Location = new System.Drawing.Point(140, 57);
            this.RequestAttentionOn.Name = "RequestAttentionOn";
            this.RequestAttentionOn.Size = new System.Drawing.Size(22, 22);
            this.RequestAttentionOn.TabIndex = 55;
            this.RequestAttentionOn.UseVisualStyleBackColor = false;
            this.RequestAttentionOn.Click += new System.EventHandler(this.RequestAttentionOn_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label22.Location = new System.Drawing.Point(225, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 16);
            this.label22.TabIndex = 54;
            this.label22.Text = "Off";
            // 
            // LobbyMessagesOff
            // 
            this.LobbyMessagesOff.BackColor = System.Drawing.Color.Transparent;
            this.LobbyMessagesOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LobbyMessagesOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LobbyMessagesOff.FlatAppearance.BorderSize = 0;
            this.LobbyMessagesOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.LobbyMessagesOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.LobbyMessagesOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LobbyMessagesOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LobbyMessagesOff.ForeColor = System.Drawing.Color.Black;
            this.LobbyMessagesOff.Location = new System.Drawing.Point(200, 26);
            this.LobbyMessagesOff.Name = "LobbyMessagesOff";
            this.LobbyMessagesOff.Size = new System.Drawing.Size(22, 22);
            this.LobbyMessagesOff.TabIndex = 53;
            this.LobbyMessagesOff.UseVisualStyleBackColor = false;
            this.LobbyMessagesOff.Click += new System.EventHandler(this.LobbyMessagesOff_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label18.Location = new System.Drawing.Point(165, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 16);
            this.label18.TabIndex = 52;
            this.label18.Text = "On";
            // 
            // LobbyMessagesOn
            // 
            this.LobbyMessagesOn.BackColor = System.Drawing.Color.Transparent;
            this.LobbyMessagesOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LobbyMessagesOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LobbyMessagesOn.FlatAppearance.BorderSize = 0;
            this.LobbyMessagesOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.LobbyMessagesOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.LobbyMessagesOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LobbyMessagesOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LobbyMessagesOn.ForeColor = System.Drawing.Color.Black;
            this.LobbyMessagesOn.Location = new System.Drawing.Point(140, 26);
            this.LobbyMessagesOn.Name = "LobbyMessagesOn";
            this.LobbyMessagesOn.Size = new System.Drawing.Size(22, 22);
            this.LobbyMessagesOn.TabIndex = 51;
            this.LobbyMessagesOn.UseVisualStyleBackColor = false;
            this.LobbyMessagesOn.Click += new System.EventHandler(this.LobbyMessagesOn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label5.Location = new System.Drawing.Point(5, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Multi-Pass Rendering:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(5, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Left Navigator Pulse:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Location = new System.Drawing.Point(5, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Request Attention:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(5, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Lobby Messages:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label6.Location = new System.Drawing.Point(12, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Game / Playmat";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label39);
            this.panel1.Controls.Add(this.AutoSelectOff);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.AutoSelectOn);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.CardWarningsOff);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.CardWarningsOn);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.PassOff);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.PassOn);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.FoilEffectsOff);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.FoilEffectsStandalone);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.FoilEffectsOn);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.HandDisplayModeFullCard);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.HandDisplayModeFisheye);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.HandDisplayModePopup);
            this.panel1.Controls.Add(this.BackgroundImageDropdown);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(3, 180);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(390, 210);
            this.panel1.TabIndex = 6;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label39.Location = new System.Drawing.Point(215, 140);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(24, 16);
            this.label39.TabIndex = 78;
            this.label39.Text = "Off";
            // 
            // AutoSelectOff
            // 
            this.AutoSelectOff.BackColor = System.Drawing.Color.Transparent;
            this.AutoSelectOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AutoSelectOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AutoSelectOff.FlatAppearance.BorderSize = 0;
            this.AutoSelectOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.AutoSelectOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.AutoSelectOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AutoSelectOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoSelectOff.ForeColor = System.Drawing.Color.Black;
            this.AutoSelectOff.Location = new System.Drawing.Point(190, 140);
            this.AutoSelectOff.Name = "AutoSelectOff";
            this.AutoSelectOff.Size = new System.Drawing.Size(22, 22);
            this.AutoSelectOff.TabIndex = 77;
            this.AutoSelectOff.UseVisualStyleBackColor = false;
            this.AutoSelectOff.Click += new System.EventHandler(this.AutoSelectOff_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label40.Location = new System.Drawing.Point(145, 140);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(25, 16);
            this.label40.TabIndex = 76;
            this.label40.Text = "On";
            // 
            // AutoSelectOn
            // 
            this.AutoSelectOn.BackColor = System.Drawing.Color.Transparent;
            this.AutoSelectOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AutoSelectOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AutoSelectOn.FlatAppearance.BorderSize = 0;
            this.AutoSelectOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.AutoSelectOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.AutoSelectOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AutoSelectOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoSelectOn.ForeColor = System.Drawing.Color.Black;
            this.AutoSelectOn.Location = new System.Drawing.Point(120, 140);
            this.AutoSelectOn.Name = "AutoSelectOn";
            this.AutoSelectOn.Size = new System.Drawing.Size(22, 22);
            this.AutoSelectOn.TabIndex = 75;
            this.AutoSelectOn.UseVisualStyleBackColor = false;
            this.AutoSelectOn.Click += new System.EventHandler(this.AutoSelectOn_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label37.Location = new System.Drawing.Point(215, 115);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(24, 16);
            this.label37.TabIndex = 74;
            this.label37.Text = "Off";
            // 
            // CardWarningsOff
            // 
            this.CardWarningsOff.BackColor = System.Drawing.Color.Transparent;
            this.CardWarningsOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CardWarningsOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CardWarningsOff.FlatAppearance.BorderSize = 0;
            this.CardWarningsOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CardWarningsOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CardWarningsOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CardWarningsOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardWarningsOff.ForeColor = System.Drawing.Color.Black;
            this.CardWarningsOff.Location = new System.Drawing.Point(190, 115);
            this.CardWarningsOff.Name = "CardWarningsOff";
            this.CardWarningsOff.Size = new System.Drawing.Size(22, 22);
            this.CardWarningsOff.TabIndex = 73;
            this.CardWarningsOff.UseVisualStyleBackColor = false;
            this.CardWarningsOff.Click += new System.EventHandler(this.CardWarningsOff_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label38.Location = new System.Drawing.Point(145, 115);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(25, 16);
            this.label38.TabIndex = 72;
            this.label38.Text = "On";
            // 
            // CardWarningsOn
            // 
            this.CardWarningsOn.BackColor = System.Drawing.Color.Transparent;
            this.CardWarningsOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CardWarningsOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CardWarningsOn.FlatAppearance.BorderSize = 0;
            this.CardWarningsOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CardWarningsOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CardWarningsOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CardWarningsOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardWarningsOn.ForeColor = System.Drawing.Color.Black;
            this.CardWarningsOn.Location = new System.Drawing.Point(120, 115);
            this.CardWarningsOn.Name = "CardWarningsOn";
            this.CardWarningsOn.Size = new System.Drawing.Size(22, 22);
            this.CardWarningsOn.TabIndex = 71;
            this.CardWarningsOn.UseVisualStyleBackColor = false;
            this.CardWarningsOn.Click += new System.EventHandler(this.CardWarningsOn_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label35.Location = new System.Drawing.Point(215, 75);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(24, 16);
            this.label35.TabIndex = 70;
            this.label35.Text = "Off";
            // 
            // PassOff
            // 
            this.PassOff.BackColor = System.Drawing.Color.Transparent;
            this.PassOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PassOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PassOff.FlatAppearance.BorderSize = 0;
            this.PassOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PassOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PassOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PassOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassOff.ForeColor = System.Drawing.Color.Black;
            this.PassOff.Location = new System.Drawing.Point(190, 75);
            this.PassOff.Name = "PassOff";
            this.PassOff.Size = new System.Drawing.Size(22, 22);
            this.PassOff.TabIndex = 69;
            this.PassOff.UseVisualStyleBackColor = false;
            this.PassOff.Click += new System.EventHandler(this.PassOff_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label36.Location = new System.Drawing.Point(145, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(25, 16);
            this.label36.TabIndex = 68;
            this.label36.Text = "On";
            // 
            // PassOn
            // 
            this.PassOn.BackColor = System.Drawing.Color.Transparent;
            this.PassOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PassOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PassOn.FlatAppearance.BorderSize = 0;
            this.PassOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PassOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PassOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PassOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassOn.ForeColor = System.Drawing.Color.Black;
            this.PassOn.Location = new System.Drawing.Point(120, 75);
            this.PassOn.Name = "PassOn";
            this.PassOn.Size = new System.Drawing.Size(22, 22);
            this.PassOn.TabIndex = 67;
            this.PassOn.UseVisualStyleBackColor = false;
            this.PassOn.Click += new System.EventHandler(this.PassOn_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label32.Location = new System.Drawing.Point(315, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 16);
            this.label32.TabIndex = 66;
            this.label32.Text = "Off";
            // 
            // FoilEffectsOff
            // 
            this.FoilEffectsOff.BackColor = System.Drawing.Color.Transparent;
            this.FoilEffectsOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FoilEffectsOff.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FoilEffectsOff.FlatAppearance.BorderSize = 0;
            this.FoilEffectsOff.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.FoilEffectsOff.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.FoilEffectsOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FoilEffectsOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FoilEffectsOff.ForeColor = System.Drawing.Color.Black;
            this.FoilEffectsOff.Location = new System.Drawing.Point(290, 50);
            this.FoilEffectsOff.Name = "FoilEffectsOff";
            this.FoilEffectsOff.Size = new System.Drawing.Size(22, 22);
            this.FoilEffectsOff.TabIndex = 65;
            this.FoilEffectsOff.UseVisualStyleBackColor = false;
            this.FoilEffectsOff.Click += new System.EventHandler(this.FoilEffectsOff_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label33.Location = new System.Drawing.Point(215, 50);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 16);
            this.label33.TabIndex = 64;
            this.label33.Text = "Standalone";
            // 
            // FoilEffectsStandalone
            // 
            this.FoilEffectsStandalone.BackColor = System.Drawing.Color.Transparent;
            this.FoilEffectsStandalone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FoilEffectsStandalone.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FoilEffectsStandalone.FlatAppearance.BorderSize = 0;
            this.FoilEffectsStandalone.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.FoilEffectsStandalone.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.FoilEffectsStandalone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FoilEffectsStandalone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FoilEffectsStandalone.ForeColor = System.Drawing.Color.Black;
            this.FoilEffectsStandalone.Location = new System.Drawing.Point(190, 50);
            this.FoilEffectsStandalone.Name = "FoilEffectsStandalone";
            this.FoilEffectsStandalone.Size = new System.Drawing.Size(22, 22);
            this.FoilEffectsStandalone.TabIndex = 63;
            this.FoilEffectsStandalone.UseVisualStyleBackColor = false;
            this.FoilEffectsStandalone.Click += new System.EventHandler(this.FoilEffectsStandalone_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label34.Location = new System.Drawing.Point(145, 50);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(25, 16);
            this.label34.TabIndex = 62;
            this.label34.Text = "On";
            // 
            // FoilEffectsOn
            // 
            this.FoilEffectsOn.BackColor = System.Drawing.Color.Transparent;
            this.FoilEffectsOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FoilEffectsOn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FoilEffectsOn.FlatAppearance.BorderSize = 0;
            this.FoilEffectsOn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.FoilEffectsOn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.FoilEffectsOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FoilEffectsOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FoilEffectsOn.ForeColor = System.Drawing.Color.Black;
            this.FoilEffectsOn.Location = new System.Drawing.Point(120, 50);
            this.FoilEffectsOn.Name = "FoilEffectsOn";
            this.FoilEffectsOn.Size = new System.Drawing.Size(22, 22);
            this.FoilEffectsOn.TabIndex = 61;
            this.FoilEffectsOn.UseVisualStyleBackColor = false;
            this.FoilEffectsOn.Click += new System.EventHandler(this.FoilEffectsOn_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label31.Location = new System.Drawing.Point(315, 10);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(61, 16);
            this.label31.TabIndex = 60;
            this.label31.Text = "Full Card";
            // 
            // HandDisplayModeFullCard
            // 
            this.HandDisplayModeFullCard.BackColor = System.Drawing.Color.Transparent;
            this.HandDisplayModeFullCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HandDisplayModeFullCard.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.HandDisplayModeFullCard.FlatAppearance.BorderSize = 0;
            this.HandDisplayModeFullCard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.HandDisplayModeFullCard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.HandDisplayModeFullCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HandDisplayModeFullCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HandDisplayModeFullCard.ForeColor = System.Drawing.Color.Black;
            this.HandDisplayModeFullCard.Location = new System.Drawing.Point(290, 10);
            this.HandDisplayModeFullCard.Name = "HandDisplayModeFullCard";
            this.HandDisplayModeFullCard.Size = new System.Drawing.Size(22, 22);
            this.HandDisplayModeFullCard.TabIndex = 59;
            this.HandDisplayModeFullCard.UseVisualStyleBackColor = false;
            this.HandDisplayModeFullCard.Click += new System.EventHandler(this.HandDisplayModeFullCard_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label29.Location = new System.Drawing.Point(215, 10);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 16);
            this.label29.TabIndex = 58;
            this.label29.Text = "Fish-eye";
            // 
            // HandDisplayModeFisheye
            // 
            this.HandDisplayModeFisheye.BackColor = System.Drawing.Color.Transparent;
            this.HandDisplayModeFisheye.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HandDisplayModeFisheye.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.HandDisplayModeFisheye.FlatAppearance.BorderSize = 0;
            this.HandDisplayModeFisheye.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.HandDisplayModeFisheye.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.HandDisplayModeFisheye.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HandDisplayModeFisheye.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HandDisplayModeFisheye.ForeColor = System.Drawing.Color.Black;
            this.HandDisplayModeFisheye.Location = new System.Drawing.Point(190, 10);
            this.HandDisplayModeFisheye.Name = "HandDisplayModeFisheye";
            this.HandDisplayModeFisheye.Size = new System.Drawing.Size(22, 22);
            this.HandDisplayModeFisheye.TabIndex = 57;
            this.HandDisplayModeFisheye.UseVisualStyleBackColor = false;
            this.HandDisplayModeFisheye.Click += new System.EventHandler(this.HandDisplayModeFisheye_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label30.Location = new System.Drawing.Point(145, 10);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(48, 16);
            this.label30.TabIndex = 56;
            this.label30.Text = "Popup";
            // 
            // HandDisplayModePopup
            // 
            this.HandDisplayModePopup.BackColor = System.Drawing.Color.Transparent;
            this.HandDisplayModePopup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HandDisplayModePopup.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.HandDisplayModePopup.FlatAppearance.BorderSize = 0;
            this.HandDisplayModePopup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.HandDisplayModePopup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.HandDisplayModePopup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HandDisplayModePopup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HandDisplayModePopup.ForeColor = System.Drawing.Color.Black;
            this.HandDisplayModePopup.Location = new System.Drawing.Point(120, 10);
            this.HandDisplayModePopup.Name = "HandDisplayModePopup";
            this.HandDisplayModePopup.Size = new System.Drawing.Size(22, 22);
            this.HandDisplayModePopup.TabIndex = 55;
            this.HandDisplayModePopup.UseVisualStyleBackColor = false;
            this.HandDisplayModePopup.Click += new System.EventHandler(this.HandDisplayModePopup_Click);
            // 
            // BackgroundImageDropdown
            // 
            this.BackgroundImageDropdown.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageDropdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageDropdown.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BackgroundImageDropdown.FlatAppearance.BorderSize = 0;
            this.BackgroundImageDropdown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.BackgroundImageDropdown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.BackgroundImageDropdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackgroundImageDropdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackgroundImageDropdown.ForeColor = System.Drawing.Color.Black;
            this.BackgroundImageDropdown.Location = new System.Drawing.Point(138, 171);
            this.BackgroundImageDropdown.Name = "BackgroundImageDropdown";
            this.BackgroundImageDropdown.Size = new System.Drawing.Size(208, 30);
            this.BackgroundImageDropdown.TabIndex = 50;
            this.BackgroundImageDropdown.UseVisualStyleBackColor = false;
            this.BackgroundImageDropdown.Click += new System.EventHandler(this.BackgroundImageDropdown_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label12.Location = new System.Drawing.Point(5, 180);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 16);
            this.label12.TabIndex = 21;
            this.label12.Text = "Background Image:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label11.Location = new System.Drawing.Point(5, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 32);
            this.label11.TabIndex = 18;
            this.label11.Text = "Auto-Select First\r\nAction:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label7.Location = new System.Drawing.Point(5, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Card Warnings:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label8.Location = new System.Drawing.Point(5, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 32);
            this.label8.TabIndex = 10;
            this.label8.Text = "Pass When Out\r\nOf Actions:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label9.Location = new System.Drawing.Point(5, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 7;
            this.label9.Text = "Foil Effects:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label10.Location = new System.Drawing.Point(5, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 32);
            this.label10.TabIndex = 4;
            this.label10.Text = "Hand Display\r\nMode:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label13.Location = new System.Drawing.Point(417, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Show Only Cards I Own";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label43);
            this.panel3.Controls.Add(this.CollectionManagerFalse);
            this.panel3.Controls.Add(this.label44);
            this.panel3.Controls.Add(this.CollectionManagerTrue);
            this.panel3.Controls.Add(this.label41);
            this.panel3.Controls.Add(this.DeckBuilderFalse);
            this.panel3.Controls.Add(this.label42);
            this.panel3.Controls.Add(this.DeckBuilderTrue);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(409, 159);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(260, 76);
            this.panel3.TabIndex = 8;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label43.Location = new System.Drawing.Point(215, 40);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(42, 16);
            this.label43.TabIndex = 86;
            this.label43.Text = "False";
            // 
            // CollectionManagerFalse
            // 
            this.CollectionManagerFalse.BackColor = System.Drawing.Color.Transparent;
            this.CollectionManagerFalse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionManagerFalse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionManagerFalse.FlatAppearance.BorderSize = 0;
            this.CollectionManagerFalse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionManagerFalse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionManagerFalse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionManagerFalse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionManagerFalse.ForeColor = System.Drawing.Color.Black;
            this.CollectionManagerFalse.Location = new System.Drawing.Point(190, 37);
            this.CollectionManagerFalse.Name = "CollectionManagerFalse";
            this.CollectionManagerFalse.Size = new System.Drawing.Size(22, 22);
            this.CollectionManagerFalse.TabIndex = 85;
            this.CollectionManagerFalse.UseVisualStyleBackColor = false;
            this.CollectionManagerFalse.Click += new System.EventHandler(this.CollectionManagerFalse_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label44.Location = new System.Drawing.Point(155, 40);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 16);
            this.label44.TabIndex = 84;
            this.label44.Text = "True";
            // 
            // CollectionManagerTrue
            // 
            this.CollectionManagerTrue.BackColor = System.Drawing.Color.Transparent;
            this.CollectionManagerTrue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionManagerTrue.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionManagerTrue.FlatAppearance.BorderSize = 0;
            this.CollectionManagerTrue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionManagerTrue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionManagerTrue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionManagerTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionManagerTrue.ForeColor = System.Drawing.Color.Black;
            this.CollectionManagerTrue.Location = new System.Drawing.Point(130, 37);
            this.CollectionManagerTrue.Name = "CollectionManagerTrue";
            this.CollectionManagerTrue.Size = new System.Drawing.Size(22, 22);
            this.CollectionManagerTrue.TabIndex = 83;
            this.CollectionManagerTrue.UseVisualStyleBackColor = false;
            this.CollectionManagerTrue.Click += new System.EventHandler(this.CollectionManagerTrue_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label41.Location = new System.Drawing.Point(215, 13);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(42, 16);
            this.label41.TabIndex = 82;
            this.label41.Text = "False";
            // 
            // DeckBuilderFalse
            // 
            this.DeckBuilderFalse.BackColor = System.Drawing.Color.Transparent;
            this.DeckBuilderFalse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckBuilderFalse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckBuilderFalse.FlatAppearance.BorderSize = 0;
            this.DeckBuilderFalse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckBuilderFalse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckBuilderFalse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckBuilderFalse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckBuilderFalse.ForeColor = System.Drawing.Color.Black;
            this.DeckBuilderFalse.Location = new System.Drawing.Point(190, 10);
            this.DeckBuilderFalse.Name = "DeckBuilderFalse";
            this.DeckBuilderFalse.Size = new System.Drawing.Size(22, 22);
            this.DeckBuilderFalse.TabIndex = 81;
            this.DeckBuilderFalse.UseVisualStyleBackColor = false;
            this.DeckBuilderFalse.Click += new System.EventHandler(this.DeckBuilderFalse_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label42.Location = new System.Drawing.Point(155, 13);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(36, 16);
            this.label42.TabIndex = 80;
            this.label42.Text = "True";
            // 
            // DeckBuilderTrue
            // 
            this.DeckBuilderTrue.BackColor = System.Drawing.Color.Transparent;
            this.DeckBuilderTrue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckBuilderTrue.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckBuilderTrue.FlatAppearance.BorderSize = 0;
            this.DeckBuilderTrue.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckBuilderTrue.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckBuilderTrue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckBuilderTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckBuilderTrue.ForeColor = System.Drawing.Color.Black;
            this.DeckBuilderTrue.Location = new System.Drawing.Point(130, 10);
            this.DeckBuilderTrue.Name = "DeckBuilderTrue";
            this.DeckBuilderTrue.Size = new System.Drawing.Size(22, 22);
            this.DeckBuilderTrue.TabIndex = 79;
            this.DeckBuilderTrue.UseVisualStyleBackColor = false;
            this.DeckBuilderTrue.Click += new System.EventHandler(this.DeckBuilderTrue_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label16.Location = new System.Drawing.Point(5, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 16);
            this.label16.TabIndex = 7;
            this.label16.Text = "Collection Manager:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label17.Location = new System.Drawing.Point(5, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 16);
            this.label17.TabIndex = 4;
            this.label17.Text = "Deck Builder:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label14.Location = new System.Drawing.Point(419, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Hide Player Tips";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label62);
            this.panel4.Controls.Add(this.QuestFilledButton);
            this.panel4.Controls.Add(this.CasualLobbyButton);
            this.panel4.Controls.Add(this.label61);
            this.panel4.Controls.Add(this.TradeLobbyButton);
            this.panel4.Controls.Add(this.TradeButton);
            this.panel4.Controls.Add(this.label60);
            this.panel4.Controls.Add(this.MultiplayerGamesButton);
            this.panel4.Controls.Add(this.ExcessiveAttackButton);
            this.panel4.Controls.Add(this.label59);
            this.panel4.Controls.Add(this.DeckbuilderButton);
            this.panel4.Controls.Add(this.CollectionButton);
            this.panel4.Controls.Add(this.label58);
            this.panel4.Controls.Add(this.MainScreenButton);
            this.panel4.Controls.Add(this.label57);
            this.panel4.Controls.Add(this.label56);
            this.panel4.Controls.Add(this.label54);
            this.panel4.Controls.Add(this.label55);
            this.panel4.Location = new System.Drawing.Point(409, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(260, 144);
            this.panel4.TabIndex = 10;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label62.Location = new System.Drawing.Point(170, 85);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(79, 16);
            this.label62.TabIndex = 61;
            this.label62.Text = "Quest Filled";
            // 
            // QuestFilledButton
            // 
            this.QuestFilledButton.BackColor = System.Drawing.Color.Transparent;
            this.QuestFilledButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.QuestFilledButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.QuestFilledButton.FlatAppearance.BorderSize = 0;
            this.QuestFilledButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.QuestFilledButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.QuestFilledButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QuestFilledButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestFilledButton.ForeColor = System.Drawing.Color.Black;
            this.QuestFilledButton.Location = new System.Drawing.Point(145, 85);
            this.QuestFilledButton.Name = "QuestFilledButton";
            this.QuestFilledButton.Size = new System.Drawing.Size(23, 20);
            this.QuestFilledButton.TabIndex = 76;
            this.QuestFilledButton.UseVisualStyleBackColor = false;
            this.QuestFilledButton.Click += new System.EventHandler(this.QuestFilledButton_Click);
            // 
            // CasualLobbyButton
            // 
            this.CasualLobbyButton.BackColor = System.Drawing.Color.Transparent;
            this.CasualLobbyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CasualLobbyButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CasualLobbyButton.FlatAppearance.BorderSize = 0;
            this.CasualLobbyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CasualLobbyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CasualLobbyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CasualLobbyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CasualLobbyButton.ForeColor = System.Drawing.Color.Black;
            this.CasualLobbyButton.Location = new System.Drawing.Point(145, 60);
            this.CasualLobbyButton.Name = "CasualLobbyButton";
            this.CasualLobbyButton.Size = new System.Drawing.Size(23, 20);
            this.CasualLobbyButton.TabIndex = 75;
            this.CasualLobbyButton.UseVisualStyleBackColor = false;
            this.CasualLobbyButton.Click += new System.EventHandler(this.CasualLobbyButton_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label61.Location = new System.Drawing.Point(170, 60);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(91, 16);
            this.label61.TabIndex = 60;
            this.label61.Text = "Casual Lobby";
            // 
            // TradeLobbyButton
            // 
            this.TradeLobbyButton.BackColor = System.Drawing.Color.Transparent;
            this.TradeLobbyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TradeLobbyButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.TradeLobbyButton.FlatAppearance.BorderSize = 0;
            this.TradeLobbyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.TradeLobbyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.TradeLobbyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TradeLobbyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TradeLobbyButton.ForeColor = System.Drawing.Color.Black;
            this.TradeLobbyButton.Location = new System.Drawing.Point(145, 35);
            this.TradeLobbyButton.Name = "TradeLobbyButton";
            this.TradeLobbyButton.Size = new System.Drawing.Size(23, 20);
            this.TradeLobbyButton.TabIndex = 74;
            this.TradeLobbyButton.UseVisualStyleBackColor = false;
            this.TradeLobbyButton.Click += new System.EventHandler(this.TradeLobbyButton_Click);
            // 
            // TradeButton
            // 
            this.TradeButton.BackColor = System.Drawing.Color.Transparent;
            this.TradeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TradeButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.TradeButton.FlatAppearance.BorderSize = 0;
            this.TradeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.TradeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.TradeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TradeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TradeButton.ForeColor = System.Drawing.Color.Black;
            this.TradeButton.Location = new System.Drawing.Point(145, 10);
            this.TradeButton.Name = "TradeButton";
            this.TradeButton.Size = new System.Drawing.Size(23, 20);
            this.TradeButton.TabIndex = 73;
            this.TradeButton.UseVisualStyleBackColor = false;
            this.TradeButton.Click += new System.EventHandler(this.TradeButton_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label60.Location = new System.Drawing.Point(170, 35);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(86, 16);
            this.label60.TabIndex = 59;
            this.label60.Text = "Trade Lobby";
            // 
            // MultiplayerGamesButton
            // 
            this.MultiplayerGamesButton.BackColor = System.Drawing.Color.Transparent;
            this.MultiplayerGamesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MultiplayerGamesButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MultiplayerGamesButton.FlatAppearance.BorderSize = 0;
            this.MultiplayerGamesButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.MultiplayerGamesButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.MultiplayerGamesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiplayerGamesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MultiplayerGamesButton.ForeColor = System.Drawing.Color.Black;
            this.MultiplayerGamesButton.Location = new System.Drawing.Point(5, 110);
            this.MultiplayerGamesButton.Name = "MultiplayerGamesButton";
            this.MultiplayerGamesButton.Size = new System.Drawing.Size(23, 20);
            this.MultiplayerGamesButton.TabIndex = 72;
            this.MultiplayerGamesButton.UseVisualStyleBackColor = false;
            this.MultiplayerGamesButton.Click += new System.EventHandler(this.MultiplayerGamesButton_Click);
            // 
            // ExcessiveAttackButton
            // 
            this.ExcessiveAttackButton.BackColor = System.Drawing.Color.Transparent;
            this.ExcessiveAttackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExcessiveAttackButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ExcessiveAttackButton.FlatAppearance.BorderSize = 0;
            this.ExcessiveAttackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ExcessiveAttackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.ExcessiveAttackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExcessiveAttackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExcessiveAttackButton.ForeColor = System.Drawing.Color.Black;
            this.ExcessiveAttackButton.Location = new System.Drawing.Point(5, 85);
            this.ExcessiveAttackButton.Name = "ExcessiveAttackButton";
            this.ExcessiveAttackButton.Size = new System.Drawing.Size(23, 20);
            this.ExcessiveAttackButton.TabIndex = 71;
            this.ExcessiveAttackButton.UseVisualStyleBackColor = false;
            this.ExcessiveAttackButton.Click += new System.EventHandler(this.ExcessiveAttackButton_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label59.Location = new System.Drawing.Point(170, 10);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(45, 16);
            this.label59.TabIndex = 58;
            this.label59.Text = "Trade";
            // 
            // DeckbuilderButton
            // 
            this.DeckbuilderButton.BackColor = System.Drawing.Color.Transparent;
            this.DeckbuilderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckbuilderButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckbuilderButton.FlatAppearance.BorderSize = 0;
            this.DeckbuilderButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckbuilderButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckbuilderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckbuilderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckbuilderButton.ForeColor = System.Drawing.Color.Black;
            this.DeckbuilderButton.Location = new System.Drawing.Point(5, 60);
            this.DeckbuilderButton.Name = "DeckbuilderButton";
            this.DeckbuilderButton.Size = new System.Drawing.Size(23, 20);
            this.DeckbuilderButton.TabIndex = 70;
            this.DeckbuilderButton.UseVisualStyleBackColor = false;
            this.DeckbuilderButton.Click += new System.EventHandler(this.DeckbuilderButton_Click);
            // 
            // CollectionButton
            // 
            this.CollectionButton.BackColor = System.Drawing.Color.Transparent;
            this.CollectionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionButton.FlatAppearance.BorderSize = 0;
            this.CollectionButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionButton.ForeColor = System.Drawing.Color.Black;
            this.CollectionButton.Location = new System.Drawing.Point(5, 35);
            this.CollectionButton.Name = "CollectionButton";
            this.CollectionButton.Size = new System.Drawing.Size(23, 20);
            this.CollectionButton.TabIndex = 69;
            this.CollectionButton.UseVisualStyleBackColor = false;
            this.CollectionButton.Click += new System.EventHandler(this.CollectionButton_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label58.Location = new System.Drawing.Point(30, 110);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(120, 16);
            this.label58.TabIndex = 57;
            this.label58.Text = "Multiplayer Games";
            // 
            // MainScreenButton
            // 
            this.MainScreenButton.BackColor = System.Drawing.Color.Transparent;
            this.MainScreenButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MainScreenButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.MainScreenButton.FlatAppearance.BorderSize = 0;
            this.MainScreenButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.MainScreenButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.MainScreenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainScreenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainScreenButton.ForeColor = System.Drawing.Color.Black;
            this.MainScreenButton.Location = new System.Drawing.Point(5, 10);
            this.MainScreenButton.Name = "MainScreenButton";
            this.MainScreenButton.Size = new System.Drawing.Size(23, 20);
            this.MainScreenButton.TabIndex = 68;
            this.MainScreenButton.UseVisualStyleBackColor = false;
            this.MainScreenButton.Click += new System.EventHandler(this.MainScreenButton_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label57.Location = new System.Drawing.Point(30, 85);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(110, 16);
            this.label57.TabIndex = 56;
            this.label57.Text = "Excessive Attack";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label56.Location = new System.Drawing.Point(30, 60);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(81, 16);
            this.label56.TabIndex = 55;
            this.label56.Text = "Deckbuilder";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label54.Location = new System.Drawing.Point(30, 10);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(83, 16);
            this.label54.TabIndex = 53;
            this.label54.Text = "Main Screen";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label55.Location = new System.Drawing.Point(30, 35);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(67, 16);
            this.label55.TabIndex = 54;
            this.label55.Text = "Collection";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label15.Location = new System.Drawing.Point(419, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Card Size";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.DeckLarge);
            this.panel5.Controls.Add(this.label52);
            this.panel5.Controls.Add(this.DeckMedium);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.DeckSmall);
            this.panel5.Controls.Add(this.label48);
            this.panel5.Controls.Add(this.PlaymatLarge);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.PlaymatMedium);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.PlaymatSmall);
            this.panel5.Controls.Add(this.label45);
            this.panel5.Controls.Add(this.CollectionLarge);
            this.panel5.Controls.Add(this.label46);
            this.panel5.Controls.Add(this.CollectionMedium);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.CollectionSmall);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Location = new System.Drawing.Point(409, 242);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(350, 148);
            this.panel5.TabIndex = 12;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label51.Location = new System.Drawing.Point(305, 70);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(43, 16);
            this.label51.TabIndex = 84;
            this.label51.Text = "Large";
            // 
            // DeckLarge
            // 
            this.DeckLarge.BackColor = System.Drawing.Color.Transparent;
            this.DeckLarge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckLarge.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckLarge.FlatAppearance.BorderSize = 0;
            this.DeckLarge.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckLarge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckLarge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckLarge.ForeColor = System.Drawing.Color.Black;
            this.DeckLarge.Location = new System.Drawing.Point(280, 69);
            this.DeckLarge.Name = "DeckLarge";
            this.DeckLarge.Size = new System.Drawing.Size(22, 22);
            this.DeckLarge.TabIndex = 83;
            this.DeckLarge.UseVisualStyleBackColor = false;
            this.DeckLarge.Click += new System.EventHandler(this.DeckLarge_Click);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label52.Location = new System.Drawing.Point(225, 70);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(56, 16);
            this.label52.TabIndex = 82;
            this.label52.Text = "Medium";
            // 
            // DeckMedium
            // 
            this.DeckMedium.BackColor = System.Drawing.Color.Transparent;
            this.DeckMedium.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckMedium.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckMedium.FlatAppearance.BorderSize = 0;
            this.DeckMedium.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckMedium.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckMedium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckMedium.ForeColor = System.Drawing.Color.Black;
            this.DeckMedium.Location = new System.Drawing.Point(200, 69);
            this.DeckMedium.Name = "DeckMedium";
            this.DeckMedium.Size = new System.Drawing.Size(22, 22);
            this.DeckMedium.TabIndex = 81;
            this.DeckMedium.UseVisualStyleBackColor = false;
            this.DeckMedium.Click += new System.EventHandler(this.DeckMedium_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label53.Location = new System.Drawing.Point(155, 70);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(42, 16);
            this.label53.TabIndex = 80;
            this.label53.Text = "Small";
            // 
            // DeckSmall
            // 
            this.DeckSmall.BackColor = System.Drawing.Color.Transparent;
            this.DeckSmall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeckSmall.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DeckSmall.FlatAppearance.BorderSize = 0;
            this.DeckSmall.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DeckSmall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DeckSmall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeckSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckSmall.ForeColor = System.Drawing.Color.Black;
            this.DeckSmall.Location = new System.Drawing.Point(130, 70);
            this.DeckSmall.Name = "DeckSmall";
            this.DeckSmall.Size = new System.Drawing.Size(22, 22);
            this.DeckSmall.TabIndex = 79;
            this.DeckSmall.UseVisualStyleBackColor = false;
            this.DeckSmall.Click += new System.EventHandler(this.DeckSmall_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label48.Location = new System.Drawing.Point(305, 110);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(43, 16);
            this.label48.TabIndex = 78;
            this.label48.Text = "Large";
            // 
            // PlaymatLarge
            // 
            this.PlaymatLarge.BackColor = System.Drawing.Color.Transparent;
            this.PlaymatLarge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlaymatLarge.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PlaymatLarge.FlatAppearance.BorderSize = 0;
            this.PlaymatLarge.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PlaymatLarge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PlaymatLarge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlaymatLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaymatLarge.ForeColor = System.Drawing.Color.Black;
            this.PlaymatLarge.Location = new System.Drawing.Point(280, 110);
            this.PlaymatLarge.Name = "PlaymatLarge";
            this.PlaymatLarge.Size = new System.Drawing.Size(22, 22);
            this.PlaymatLarge.TabIndex = 77;
            this.PlaymatLarge.UseVisualStyleBackColor = false;
            this.PlaymatLarge.Click += new System.EventHandler(this.PlaymatLarge_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label49.Location = new System.Drawing.Point(225, 110);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(56, 16);
            this.label49.TabIndex = 76;
            this.label49.Text = "Medium";
            // 
            // PlaymatMedium
            // 
            this.PlaymatMedium.BackColor = System.Drawing.Color.Transparent;
            this.PlaymatMedium.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlaymatMedium.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PlaymatMedium.FlatAppearance.BorderSize = 0;
            this.PlaymatMedium.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PlaymatMedium.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PlaymatMedium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlaymatMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaymatMedium.ForeColor = System.Drawing.Color.Black;
            this.PlaymatMedium.Location = new System.Drawing.Point(200, 110);
            this.PlaymatMedium.Name = "PlaymatMedium";
            this.PlaymatMedium.Size = new System.Drawing.Size(22, 22);
            this.PlaymatMedium.TabIndex = 75;
            this.PlaymatMedium.UseVisualStyleBackColor = false;
            this.PlaymatMedium.Click += new System.EventHandler(this.PlaymatMedium_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label50.Location = new System.Drawing.Point(155, 113);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(42, 16);
            this.label50.TabIndex = 74;
            this.label50.Text = "Small";
            // 
            // PlaymatSmall
            // 
            this.PlaymatSmall.BackColor = System.Drawing.Color.Transparent;
            this.PlaymatSmall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlaymatSmall.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.PlaymatSmall.FlatAppearance.BorderSize = 0;
            this.PlaymatSmall.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.PlaymatSmall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.PlaymatSmall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlaymatSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaymatSmall.ForeColor = System.Drawing.Color.Black;
            this.PlaymatSmall.Location = new System.Drawing.Point(130, 110);
            this.PlaymatSmall.Name = "PlaymatSmall";
            this.PlaymatSmall.Size = new System.Drawing.Size(22, 22);
            this.PlaymatSmall.TabIndex = 73;
            this.PlaymatSmall.UseVisualStyleBackColor = false;
            this.PlaymatSmall.Click += new System.EventHandler(this.PlaymatSmall_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label45.Location = new System.Drawing.Point(305, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(43, 16);
            this.label45.TabIndex = 72;
            this.label45.Text = "Large";
            // 
            // CollectionLarge
            // 
            this.CollectionLarge.BackColor = System.Drawing.Color.Transparent;
            this.CollectionLarge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionLarge.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionLarge.FlatAppearance.BorderSize = 0;
            this.CollectionLarge.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionLarge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionLarge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionLarge.ForeColor = System.Drawing.Color.Black;
            this.CollectionLarge.Location = new System.Drawing.Point(280, 20);
            this.CollectionLarge.Name = "CollectionLarge";
            this.CollectionLarge.Size = new System.Drawing.Size(22, 22);
            this.CollectionLarge.TabIndex = 71;
            this.CollectionLarge.UseVisualStyleBackColor = false;
            this.CollectionLarge.Click += new System.EventHandler(this.CollectionLarge_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label46.Location = new System.Drawing.Point(225, 20);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(56, 16);
            this.label46.TabIndex = 70;
            this.label46.Text = "Medium";
            // 
            // CollectionMedium
            // 
            this.CollectionMedium.BackColor = System.Drawing.Color.Transparent;
            this.CollectionMedium.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionMedium.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionMedium.FlatAppearance.BorderSize = 0;
            this.CollectionMedium.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionMedium.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionMedium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionMedium.ForeColor = System.Drawing.Color.Black;
            this.CollectionMedium.Location = new System.Drawing.Point(200, 20);
            this.CollectionMedium.Name = "CollectionMedium";
            this.CollectionMedium.Size = new System.Drawing.Size(22, 22);
            this.CollectionMedium.TabIndex = 69;
            this.CollectionMedium.UseVisualStyleBackColor = false;
            this.CollectionMedium.Click += new System.EventHandler(this.CollectionMedium_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label47.Location = new System.Drawing.Point(155, 20);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 16);
            this.label47.TabIndex = 68;
            this.label47.Text = "Small";
            // 
            // CollectionSmall
            // 
            this.CollectionSmall.BackColor = System.Drawing.Color.Transparent;
            this.CollectionSmall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CollectionSmall.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CollectionSmall.FlatAppearance.BorderSize = 0;
            this.CollectionSmall.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CollectionSmall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CollectionSmall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CollectionSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectionSmall.ForeColor = System.Drawing.Color.Black;
            this.CollectionSmall.Location = new System.Drawing.Point(130, 20);
            this.CollectionSmall.Name = "CollectionSmall";
            this.CollectionSmall.Size = new System.Drawing.Size(22, 22);
            this.CollectionSmall.TabIndex = 67;
            this.CollectionSmall.UseVisualStyleBackColor = false;
            this.CollectionSmall.Click += new System.EventHandler(this.CollectionSmall_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label19.Location = new System.Drawing.Point(5, 110);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 16);
            this.label19.TabIndex = 10;
            this.label19.Text = "Playmat:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label20.Location = new System.Drawing.Point(5, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 16);
            this.label20.TabIndex = 7;
            this.label20.Text = "Deck Builder Deck:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label21.Location = new System.Drawing.Point(5, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 32);
            this.label21.TabIndex = 4;
            this.label21.Text = "Deck Builder\r\nCollection:";
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Name = "UserInterface";
            this.Size = new System.Drawing.Size(767, 436);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button BackgroundImageDropdown;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button LobbyMessagesOn;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button LobbyMessagesOff;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button RequestAttentionOff;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button RequestAttentionOn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button LeftNavigatorPulseOff;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button LeftNavigatorPulseOn;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button MultiPassRenderingOff;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button MultiPassRenderingOn;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button HandDisplayModeFisheye;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button HandDisplayModePopup;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button HandDisplayModeFullCard;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button FoilEffectsOff;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button FoilEffectsStandalone;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button FoilEffectsOn;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button AutoSelectOff;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button AutoSelectOn;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button CardWarningsOff;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button CardWarningsOn;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button PassOff;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button PassOn;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button DeckBuilderFalse;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button DeckBuilderTrue;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button CollectionManagerFalse;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button CollectionManagerTrue;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button CollectionLarge;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button CollectionMedium;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button CollectionSmall;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button PlaymatLarge;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button PlaymatMedium;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button PlaymatSmall;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button DeckLarge;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button DeckMedium;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button DeckSmall;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button MultiplayerGamesButton;
        private System.Windows.Forms.Button ExcessiveAttackButton;
        private System.Windows.Forms.Button DeckbuilderButton;
        private System.Windows.Forms.Button CollectionButton;
        private System.Windows.Forms.Button MainScreenButton;
        private System.Windows.Forms.Button TradeButton;
        private System.Windows.Forms.Button TradeLobbyButton;
        private System.Windows.Forms.Button QuestFilledButton;
        private System.Windows.Forms.Button CasualLobbyButton;
    }
}
