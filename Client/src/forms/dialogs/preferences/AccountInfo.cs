﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Player;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.forms.dialogs.preferences
{
    public partial class AccountInfo : UserControl
    {
        private readonly Preferences Preferences;
        private readonly bool Loading = true;

        public AccountInfo(Preferences Preferences)
        {
            InitializeComponent();

            this.Preferences = Preferences;
            Dock = DockStyle.Fill;

            ButtonAnimation.FormatButton(HomePageButton);
            ButtonAnimation.FormatButton(InstantMessengerButton);
            ButtonAnimation.FormatButton(PersonalMessageButton);

            UserInfoPacket UserInfoPacket = PacketService.GetUserInfoPacket(PlayerService.GetUsername());
            HomePageTextBox.Text = UserInfoPacket.GetHomePage();
            InstantMessengerTextBox.Text = UserInfoPacket.GetInstantMessenger();
            PersonalMessageTextBox.Text = UserInfoPacket.GetBiography();
            Loading = false;
        }

        public void SaveAccountInfo()
        {
            PacketService.SendPacket(Commands.SAVE_ACCOUNTINFO, new CommandPacket(new string[] { HomePageTextBox.Text, InstantMessengerTextBox.Text, PersonalMessageTextBox.Text }));
        }

        private void HomePageButton_Click(object sender, System.EventArgs e)
        {
            // TODO: Implement
            SoundService.PlaySound("button");
        }

        private void InstantMessengerButton_Click(object sender, System.EventArgs e)
        {
            // TODO: Implement
            SoundService.PlaySound("button");
        }

        private void PersonalMessageButton_Click(object sender, System.EventArgs e)
        {
            // TODO: Implement
            SoundService.PlaySound("button");
        }

        private void HomePageTextBox_TextChanged(object sender, System.EventArgs e)
        {
            Preferences.ChangedSetting(Loading);
        }

        private void InstantMessengerTextBox_TextChanged(object sender, System.EventArgs e)
        {
            Preferences.ChangedSetting(Loading);
        }

        private void PersonalMessageTextBox_TextChanged(object sender, System.EventArgs e)
        {
            Preferences.ChangedSetting(Loading);
        }
    }
}
