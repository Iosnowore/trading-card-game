﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.forms.dialogs.preferences
{
    public partial class Avatar : UserControl
    {
        private int AvatarIndex;

        private bool CustomChecked;
        private bool DefaultChecked = true;
        private readonly bool Loading = true;

        private readonly Preferences Preferences;

        public Avatar(Preferences Preferences)
        {
            InitializeComponent();

            this.Preferences = Preferences;

            CurrentAvatarPictureBox.Image = ResourcesLib.GetImageFromFrames("bronze");
            FocusedPictureBox.Image = ResourcesLib.GetImageFromFrames("bronze");
            LeftPictureBox.Image = ResourcesLib.GetImageFromFrames("bronze");
            RightPictureBox.Image = ResourcesLib.GetImageFromFrames("bronze");

            AvatarIndex = PacketService.GetUserInfoPacket(PlayerService.GetUsername()).GetAvatar();

            LoadAvatarsForIndex();

            ButtonAnimation.FormatButton(LeftButton);
            ButtonAnimation.FormatButton(RightButton);
            ButtonAnimation.ToggleCheckBox(CustomCheckBox, false);
            ButtonAnimation.ToggleCheckBox(DefaultCheckBox, true);
            Loading = false;
        }

        public void SaveAvatar()
        {
            PacketService.SendPacket(Commands.SAVE_AVATAR, new CommandPacket(new string[] { AvatarIndex.ToString() }));
        }

        private void LoadAvatarsForIndex()
        {
            CurrentAvatarPictureBox.BackgroundImage = ResourcesLib.GetImageFromPictures("avatar_" + FormatSingleIndex(AvatarIndex));
            RightPictureBox.BackgroundImage = ResourcesLib.GetImageFromPictures("avatar_" + (AvatarIndex == 30 ? "01" : FormatSingleIndex(AvatarIndex + 1)));
            LeftPictureBox.BackgroundImage = ResourcesLib.GetImageFromPictures("avatar_" + (AvatarIndex == 1 ? "30" : FormatSingleIndex(AvatarIndex - 1)));
            FocusedPictureBox.BackgroundImage = CurrentAvatarPictureBox.BackgroundImage;
        }

        private string FormatSingleIndex(int Index)
        {
            return (Index < 10 ? "0" : string.Empty) + Index;
        }

        private void LeftButton_Click(object sender, System.EventArgs e)
        {
            if (AvatarIndex == 1)
                AvatarIndex = 30;
            else
                AvatarIndex--;
            LoadAvatarsForIndex();
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void RightButton_Click(object sender, System.EventArgs e)
        {
            if (AvatarIndex == 30)
                AvatarIndex = 1;
            else
                AvatarIndex++;
            LoadAvatarsForIndex();
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void CustomCheckBox_Click(object sender, System.EventArgs e)
        {
            CustomChecked = !CustomChecked;

            ButtonAnimation.ToggleCheckBox(CustomCheckBox, CustomChecked);

            SoundService.PlaySound("button");
        }

        private void DefaultCheckBox_Click(object sender, System.EventArgs e)
        {
            DefaultChecked = !DefaultChecked;

            ButtonAnimation.ToggleCheckBox(DefaultCheckBox, DefaultChecked);

            SoundService.PlaySound("button");
        }
    }
}
