﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Components;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.forms.dialogs.preferences
{
    public partial class UserInterface : UserControl
    {
        private bool LobbyMessages = SettingsService.GetBoolFromSettingsXml("General", "LobbyMessages");
        private bool RequestAttention = SettingsService.GetBoolFromSettingsXml("General", "RequestAttention");
        private bool LeftNavigatorPulse = SettingsService.GetBoolFromSettingsXml("General", "LeftNavigatorPulse");
        private bool MultiPassRendering = SettingsService.GetBoolFromSettingsXml("General", "MultiPassRendering");

        private int BackgroundSetting = SettingsService.GetIntFromSettingsXml("GamePlaymat", "BackgroundImage");
        private string HandDisplayMode = SettingsService.GetStringFromSettingsXml("GamePlaymat", "HandDisplayMode");
        private string FoilEffects = SettingsService.GetStringFromSettingsXml("GamePlaymat", "FoilEffects");
        private bool Pass = SettingsService.GetBoolFromSettingsXml("GamePlaymat", "PassWhenOutOfActions");
        private bool CardWarnings = SettingsService.GetBoolFromSettingsXml("GamePlaymat", "CardWarnings");
        private bool AutoSelect = SettingsService.GetBoolFromSettingsXml("GamePlaymat", "AutoSelectFirstAction");

        private bool HideMainScreen = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "MainScreen");
        private bool HideCollection = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "Collection");
        private bool HideDeckbuilder = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "DeckBuilder");
        private bool HideExcessiveAttack = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "ExcessiveAttack");
        private bool HideMultiplayerGames = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "MultiplayerGames");
        private bool HideTrade = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "Trade");
        private bool HideTradeLobby = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "TradeLobby");
        private bool HideCasualLobby = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "CasualLobby");
        private bool HideQuestFilled = SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "QuestFilled");

        private bool DeckBuilder = SettingsService.GetBoolFromSettingsXml("ShowOnlyCardsIOwn", "DeckBuilder");
        private bool CollectionManager = SettingsService.GetBoolFromSettingsXml("ShowOnlyCardsIOwn", "CollectionManager");

        private string BuilderCollection = SettingsService.GetStringFromSettingsXml("CardSize", "DeckBuilderCollection");
        private string BuilderDeck = SettingsService.GetStringFromSettingsXml("CardSize", "DeckBuilderDeck");
        private string Playmat = SettingsService.GetStringFromSettingsXml("CardSize", "Playmat");

        private readonly string[] BackgroundImages = { "Stars (Default)", "Space", "Mos Eisley", "Nebula", "Korriban", "Exar Kun Temple", "Coruscant", "Rebel Fleet", "Talus", "Empire Battle", "Imperial Fleet", "AT-ST", "Hoth", "Talus", "Darth Vader on Mustafar" };

        private readonly Preferences Preferences;

        private readonly bool Loading = true;

        public UserInterface(Preferences Preferences)
        {
            InitializeComponent();

            this.Preferences = Preferences;

            LobbyMessagesOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (LobbyMessages ? "on" : "off"));
            LobbyMessagesOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!LobbyMessages ? "on" : "off"));

            RequestAttentionOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (RequestAttention ? "on" : "off"));
            RequestAttentionOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!RequestAttention ? "on" : "off"));

            LeftNavigatorPulseOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (LeftNavigatorPulse ? "on" : "off"));
            LeftNavigatorPulseOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!LeftNavigatorPulse ? "on" : "off"));

            MultiPassRenderingOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (MultiPassRendering ? "on" : "off"));
            MultiPassRenderingOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!MultiPassRendering ? "on" : "off"));

            HandDisplayModePopup.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Popup") ? "on" : "off"));
            HandDisplayModeFisheye.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Fish-eye") ? "on" : "off"));
            HandDisplayModeFullCard.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Full Card") ? "on" : "off"));

            FoilEffectsOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("On") ? "on" : "off"));
            FoilEffectsStandalone.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("Standalone") ? "on" : "off"));
            FoilEffectsOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("Off") ? "on" : "off"));

            PassOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Pass ? "on" : "off"));
            PassOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!Pass ? "on" : "off"));

            CardWarningsOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (CardWarnings ? "on" : "off"));
            CardWarningsOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!CardWarnings ? "on" : "off"));

            AutoSelectOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (AutoSelect ? "on" : "off"));
            AutoSelectOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!AutoSelect ? "on" : "off"));

            ButtonAnimation.FormatButton(BackgroundImageDropdown);
            BackgroundImageDropdown.Text = BackgroundImages[BackgroundSetting];

            MainScreenButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideMainScreen ? "on" : "off"));
            CollectionButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideCollection ? "on" : "off"));
            DeckbuilderButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideDeckbuilder ? "on" : "off"));
            ExcessiveAttackButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideExcessiveAttack ? "on" : "off"));
            MultiplayerGamesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideMultiplayerGames ? "on" : "off"));
            TradeButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideTrade ? "on" : "off"));
            TradeLobbyButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideTradeLobby ? "on" : "off"));
            CasualLobbyButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideCasualLobby ? "on" : "off"));
            QuestFilledButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideQuestFilled ? "on" : "off"));

            DeckBuilderTrue.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (DeckBuilder ? "on" : "off"));
            DeckBuilderFalse.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!DeckBuilder ? "on" : "off"));

            CollectionManagerTrue.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (CollectionManager ? "on" : "off"));
            CollectionManagerFalse.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!CollectionManager ? "on" : "off"));

            CollectionSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Small") ? "on" : "off"));
            CollectionMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Medium") ? "on" : "off"));
            CollectionLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Large") ? "on" : "off"));

            DeckSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Small") ? "on" : "off"));
            DeckMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Medium") ? "on" : "off"));
            DeckLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Large") ? "on" : "off"));

            PlaymatSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Small") ? "on" : "off"));
            PlaymatMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Medium") ? "on" : "off"));
            PlaymatLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Large") ? "on" : "off"));
            Loading = false;
        }

        private void LobbyMessagesChange()
        {
            LobbyMessages = !LobbyMessages;
            LobbyMessagesOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (LobbyMessages ? "on" : "off"));
            LobbyMessagesOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!LobbyMessages ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void LobbyMessagesOn_Click(object sender, System.EventArgs e)
        {
            LobbyMessagesChange();
        }

        private void LobbyMessagesOff_Click(object sender, System.EventArgs e)
        {
            LobbyMessagesChange();
        }

        public bool GetLobbyMessages()
        {
            return LobbyMessages;
        }

        private void RequestAttentionChange()
        {
            RequestAttention = !RequestAttention;
            RequestAttentionOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (RequestAttention ? "on" : "off"));
            RequestAttentionOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!RequestAttention ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void RequestAttentionOn_Click(object sender, System.EventArgs e)
        {
            RequestAttentionChange();
        }

        private void RequestAttentionOff_Click(object sender, System.EventArgs e)
        {
            RequestAttentionChange();
        }

        public bool GetRequestAttention()
        {
            return RequestAttention;
        }

        private void LeftNavigatorPulseChange()
        {
            LeftNavigatorPulse = !LeftNavigatorPulse;
            LeftNavigatorPulseOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (LeftNavigatorPulse ? "on" : "off"));
            LeftNavigatorPulseOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!LeftNavigatorPulse ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void LeftNavigatorPulseOn_Click(object sender, System.EventArgs e)
        {
            LeftNavigatorPulseChange();
        }

        private void LeftNavigatorPulseOff_Click(object sender, System.EventArgs e)
        {
            LeftNavigatorPulseChange();
        }

        public bool GetLeftNavigatorPulse()
        {
            return LeftNavigatorPulse;
        }

        private void MultiPassRenderingChange()
        {
            MultiPassRendering = !MultiPassRendering;
            MultiPassRenderingOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (MultiPassRendering ? "on" : "off"));
            MultiPassRenderingOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!MultiPassRendering ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void MultiPassRenderingOn_Click(object sender, System.EventArgs e)
        {
            MultiPassRenderingChange();
        }

        private void MultiPassRenderingOff_Click(object sender, System.EventArgs e)
        {
            MultiPassRenderingChange();
        }

        public bool GetMultiPassRendering()
        {
            return MultiPassRendering;
        }

        private void HandDisplayChange(string HandDisplayMode)
        {
            this.HandDisplayMode = HandDisplayMode;
            HandDisplayModePopup.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Popup") ? "on" : "off"));
            HandDisplayModeFisheye.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Fish-eye") ? "on" : "off"));
            HandDisplayModeFullCard.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (HandDisplayMode.Equals("Full Card") ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void HandDisplayModePopup_Click(object sender, System.EventArgs e)
        {
            HandDisplayChange("Popup");
        }

        private void HandDisplayModeFisheye_Click(object sender, System.EventArgs e)
        {
            HandDisplayChange("Fish-eye");
        }

        private void HandDisplayModeFullCard_Click(object sender, System.EventArgs e)
        {
            HandDisplayChange("Full Card");
        }

        public string GetHandDisplayMode()
        {
            return HandDisplayMode;
        }

        private void FoilEffectsChange(string FoilEffects)
        {
            this.FoilEffects = FoilEffects;
            FoilEffectsOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("On") ? "on" : "off"));
            FoilEffectsStandalone.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("Standalone") ? "on" : "off"));
            FoilEffectsOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (FoilEffects.Equals("Off") ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void FoilEffectsOn_Click(object sender, System.EventArgs e)
        {
            FoilEffectsChange("On");
        }

        private void FoilEffectsStandalone_Click(object sender, System.EventArgs e)
        {
            FoilEffectsChange("Standalone");
        }

        private void FoilEffectsOff_Click(object sender, System.EventArgs e)
        {
            FoilEffectsChange("Off");
        }

        public string GetFoilEffects()
        {
            return FoilEffects;
        }

        private void PassChange()
        {
            Pass = !Pass;
            PassOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Pass ? "on" : "off"));
            PassOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!Pass ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void PassOn_Click(object sender, System.EventArgs e)
        {
            PassChange();
        }

        private void PassOff_Click(object sender, System.EventArgs e)
        {
            PassChange();
        }

        public bool GetPass()
        {
            return Pass;
        }

        private void CardWarningsChange()
        {
            CardWarnings = !CardWarnings;
            CardWarningsOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (CardWarnings ? "on" : "off"));
            CardWarningsOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!CardWarnings ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void CardWarningsOn_Click(object sender, System.EventArgs e)
        {
            CardWarningsChange();
        }

        private void CardWarningsOff_Click(object sender, System.EventArgs e)
        {
            CardWarningsChange();
        }

        public bool GetCardWarnings()
        {
            return CardWarnings;
        }

        private void AutoSelectChange()
        {
            AutoSelect = !AutoSelect;
            AutoSelectOn.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (AutoSelect ? "on" : "off"));
            AutoSelectOff.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!AutoSelect ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void AutoSelectOn_Click(object sender, System.EventArgs e)
        {
            AutoSelectChange();
        }

        private void AutoSelectOff_Click(object sender, System.EventArgs e)
        {
            AutoSelectChange();
        }

        public void DropDownSelected(int Index)
        {
            BackgroundSetting = Index;
            BackgroundImageDropdown.Text = BackgroundImages[Index];
            Preferences.ChangedSetting(Loading);
        }

        private void BackgroundImageDropdown_Click(object sender, System.EventArgs e)
        {
            DropDownMenu Menu = new DropDownMenu(BackgroundImages);
            Menu.Location = new Point(panel1.Location.X + BackgroundImageDropdown.Location.X + BackgroundImageDropdown.Width, panel1.Location.Y + BackgroundImageDropdown.Location.Y + BackgroundImageDropdown.Height - Height + 50);
            Controls.Add(Menu);

            SoundService.PlaySound("button");
        }

        public string GetBackgroundImage()
        {
            return BackgroundSetting.ToString();
        }

        private void DeckBuilderChange()
        {
            DeckBuilder = !DeckBuilder;
            DeckBuilderTrue.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (DeckBuilder ? "on" : "off"));
            DeckBuilderFalse.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!DeckBuilder ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void DeckBuilderTrue_Click(object sender, System.EventArgs e)
        {
            DeckBuilderChange();
        }

        private void DeckBuilderFalse_Click(object sender, System.EventArgs e)
        {
            DeckBuilderChange();
        }

        public bool GetDeckBuilder()
        {
            return DeckBuilder;
        }

        private void CollectionManagerChange()
        {
            CollectionManager = !CollectionManager;
            CollectionManagerTrue.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (CollectionManager ? "on" : "off"));
            CollectionManagerFalse.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (!CollectionManager ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void CollectionManagerTrue_Click(object sender, System.EventArgs e)
        {
            CollectionManagerChange();
        }

        private void CollectionManagerFalse_Click(object sender, System.EventArgs e)
        {
            CollectionManagerChange();
        }

        public bool GetCollectionManager()
        {
            return CollectionManager;
        }

        private void DeckBuilderCollectionChange(string BuilderCollection)
        {
            this.BuilderCollection = BuilderCollection;
            CollectionSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Small") ? "on" : "off"));
            CollectionMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Medium") ? "on" : "off"));
            CollectionLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderCollection.Equals("Large") ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void CollectionSmall_Click(object sender, System.EventArgs e)
        {
            DeckBuilderCollectionChange("Small");
        }

        private void CollectionMedium_Click(object sender, System.EventArgs e)
        {
            DeckBuilderCollectionChange("Medium");
        }

        private void CollectionLarge_Click(object sender, System.EventArgs e)
        {
            DeckBuilderCollectionChange("Large");
        }

        public string GetDeckBuilderCollection()
        {
            return BuilderCollection;
        }

        private void DeckBuilderDeckChange(string BuilderDeck)
        {
            this.BuilderDeck = BuilderDeck;
            DeckSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Small") ? "on" : "off"));
            DeckMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Medium") ? "on" : "off"));
            DeckLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (BuilderDeck.Equals("Large") ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void DeckSmall_Click(object sender, System.EventArgs e)
        {
            DeckBuilderDeckChange("Small");
        }

        private void DeckMedium_Click(object sender, System.EventArgs e)
        {
            DeckBuilderDeckChange("Medium");
        }

        private void DeckLarge_Click(object sender, System.EventArgs e)
        {
            DeckBuilderDeckChange("Large");
        }

        public string GetDeckBuilderDeck()
        {
            return BuilderDeck;
        }

        private void PlaymatChange(string Playmat)
        {
            this.Playmat = Playmat;
            PlaymatSmall.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Small") ? "on" : "off"));
            PlaymatMedium.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Medium") ? "on" : "off"));
            PlaymatLarge.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_" + (Playmat.Equals("Large") ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        private void PlaymatSmall_Click(object sender, System.EventArgs e)
        {
            PlaymatChange("Small");
        }

        private void PlaymatMedium_Click(object sender, System.EventArgs e)
        {
            PlaymatChange("Medium");
        }

        private void PlaymatLarge_Click(object sender, System.EventArgs e)
        {
            PlaymatChange("Large");
        }

        public string GetPlaymat()
        {
            return Playmat;
        }

        private void MainScreenButton_Click(object sender, System.EventArgs e)
        {
            HideMainScreen = !HideMainScreen;
            MainScreenButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideMainScreen ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideMainScreen()
        {
            return HideMainScreen;
        }

        private void CollectionButton_Click(object sender, System.EventArgs e)
        {
            HideCollection = !HideCollection;
            CollectionButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideCollection ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideCollection()
        {
            return HideCollection;
        }

        private void DeckbuilderButton_Click(object sender, System.EventArgs e)
        {
            HideDeckbuilder = !HideDeckbuilder;
            DeckbuilderButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideDeckbuilder ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideDeckbuilder()
        {
            return HideDeckbuilder;
        }

        private void ExcessiveAttackButton_Click(object sender, System.EventArgs e)
        {
            HideExcessiveAttack = !HideExcessiveAttack;
            ExcessiveAttackButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideExcessiveAttack ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideExcessiveAttack()
        {
            return HideExcessiveAttack;
        }

        private void MultiplayerGamesButton_Click(object sender, System.EventArgs e)
        {
            HideMultiplayerGames = !HideMultiplayerGames;
            MultiplayerGamesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideMultiplayerGames ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideMultiplayerGames()
        {
            return HideMultiplayerGames;
        }

        private void TradeButton_Click(object sender, System.EventArgs e)
        {
            HideTrade = !HideTrade;
            TradeButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideTrade ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideTrade()
        {
            return HideTrade;
        }

        private void TradeLobbyButton_Click(object sender, System.EventArgs e)
        {
            HideTradeLobby = !HideTradeLobby;
            TradeLobbyButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideTradeLobby ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideTradeLobby()
        {
            return HideTradeLobby;
        }

        private void CasualLobbyButton_Click(object sender, System.EventArgs e)
        {
            HideCasualLobby = !HideCasualLobby;
            CasualLobbyButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideCasualLobby ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideCasualLobby()
        {
            return HideCasualLobby;
        }

        private void QuestFilledButton_Click(object sender, System.EventArgs e)
        {
            HideQuestFilled = !HideQuestFilled;
            QuestFilledButton.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (HideQuestFilled ? "on" : "off"));
            Preferences.ChangedSetting(Loading);
            SoundService.PlaySound("button");
        }

        public bool GetHideQuestFilled()
        {
            return HideQuestFilled;
        }
    }
}
