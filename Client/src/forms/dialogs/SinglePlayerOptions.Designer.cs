﻿namespace TradingCardGame.forms.dialogs
{
    partial class SinglePlayerOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SkirmishButton = new System.Windows.Forms.Panel();
            this.skirmishDescriptionLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SkirmishButtonImage = new System.Windows.Forms.PictureBox();
            this.ScenariosButton = new System.Windows.Forms.Panel();
            this.scenariosDescriptionLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ScenariosButtonImage = new System.Windows.Forms.PictureBox();
            this.TutorialsButton = new System.Windows.Forms.Panel();
            this.tutorialDescriptionLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TutorialsButtonImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.SkirmishButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SkirmishButtonImage)).BeginInit();
            this.ScenariosButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScenariosButtonImage)).BeginInit();
            this.TutorialsButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TutorialsButtonImage)).BeginInit();
            this.SuspendLayout();
            // 
            // SelectButton
            // 
            this.SelectButton.BackColor = System.Drawing.Color.Transparent;
            this.SelectButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SelectButton.FlatAppearance.BorderSize = 0;
            this.SelectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SelectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SelectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectButton.ForeColor = System.Drawing.Color.Black;
            this.SelectButton.Location = new System.Drawing.Point(369, 433);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(119, 29);
            this.SelectButton.TabIndex = 51;
            this.SelectButton.Text = "Select";
            this.SelectButton.UseVisualStyleBackColor = false;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.SkirmishButton);
            this.panel1.Controls.Add(this.ScenariosButton);
            this.panel1.Controls.Add(this.TutorialsButton);
            this.panel1.Location = new System.Drawing.Point(13, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(475, 350);
            this.panel1.TabIndex = 52;
            // 
            // SkirmishButton
            // 
            this.SkirmishButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.SkirmishButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SkirmishButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SkirmishButton.Controls.Add(this.skirmishDescriptionLabel);
            this.SkirmishButton.Controls.Add(this.label12);
            this.SkirmishButton.Controls.Add(this.SkirmishButtonImage);
            this.SkirmishButton.Location = new System.Drawing.Point(23, 234);
            this.SkirmishButton.Name = "SkirmishButton";
            this.SkirmishButton.Size = new System.Drawing.Size(425, 105);
            this.SkirmishButton.TabIndex = 18;
            this.SkirmishButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SkirmishButton_MouseClick);
            this.SkirmishButton.MouseEnter += new System.EventHandler(this.SkirmishButton_MouseEnter);
            this.SkirmishButton.MouseLeave += new System.EventHandler(this.SkirmishButton_MouseLeave);
            // 
            // skirmishDescriptionLabel
            // 
            this.skirmishDescriptionLabel.AutoSize = true;
            this.skirmishDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skirmishDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.skirmishDescriptionLabel.Name = "skirmishDescriptionLabel";
            this.skirmishDescriptionLabel.Size = new System.Drawing.Size(240, 30);
            this.skirmishDescriptionLabel.TabIndex = 2;
            this.skirmishDescriptionLabel.Text = "Test your newest deck designs against four\r\ndifferent computer-controlled opponen" +
    "ts.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(140, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 24);
            this.label12.TabIndex = 1;
            this.label12.Text = "Skirmish";
            // 
            // SkirmishButtonImage
            // 
            this.SkirmishButtonImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SkirmishButtonImage.Location = new System.Drawing.Point(1, 2);
            this.SkirmishButtonImage.Name = "SkirmishButtonImage";
            this.SkirmishButtonImage.Size = new System.Drawing.Size(133, 100);
            this.SkirmishButtonImage.TabIndex = 0;
            this.SkirmishButtonImage.TabStop = false;
            // 
            // ScenariosButton
            // 
            this.ScenariosButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.ScenariosButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ScenariosButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ScenariosButton.Controls.Add(this.scenariosDescriptionLabel);
            this.ScenariosButton.Controls.Add(this.label10);
            this.ScenariosButton.Controls.Add(this.ScenariosButtonImage);
            this.ScenariosButton.Location = new System.Drawing.Point(23, 121);
            this.ScenariosButton.Name = "ScenariosButton";
            this.ScenariosButton.Size = new System.Drawing.Size(425, 105);
            this.ScenariosButton.TabIndex = 17;
            this.ScenariosButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ScenariosButton_MouseClick);
            this.ScenariosButton.MouseEnter += new System.EventHandler(this.ScenariosButton_MouseEnter);
            this.ScenariosButton.MouseLeave += new System.EventHandler(this.ScenariosButton_MouseLeave);
            // 
            // scenariosDescriptionLabel
            // 
            this.scenariosDescriptionLabel.AutoSize = true;
            this.scenariosDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scenariosDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.scenariosDescriptionLabel.Name = "scenariosDescriptionLabel";
            this.scenariosDescriptionLabel.Size = new System.Drawing.Size(273, 45);
            this.scenariosDescriptionLabel.TabIndex = 2;
            this.scenariosDescriptionLabel.Text = "Uncover ancient relics for both the Rebel Alliance\r\nand the Empire as you battle " +
    "through a series of\r\ncomputer-controlled opponents.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(140, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 24);
            this.label10.TabIndex = 1;
            this.label10.Text = "Scenarios";
            // 
            // ScenariosButtonImage
            // 
            this.ScenariosButtonImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ScenariosButtonImage.Location = new System.Drawing.Point(1, 2);
            this.ScenariosButtonImage.Name = "ScenariosButtonImage";
            this.ScenariosButtonImage.Size = new System.Drawing.Size(133, 100);
            this.ScenariosButtonImage.TabIndex = 0;
            this.ScenariosButtonImage.TabStop = false;
            // 
            // TutorialsButton
            // 
            this.TutorialsButton.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.TutorialsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TutorialsButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TutorialsButton.Controls.Add(this.tutorialDescriptionLabel);
            this.TutorialsButton.Controls.Add(this.label8);
            this.TutorialsButton.Controls.Add(this.TutorialsButtonImage);
            this.TutorialsButton.Location = new System.Drawing.Point(23, 8);
            this.TutorialsButton.Name = "TutorialsButton";
            this.TutorialsButton.Size = new System.Drawing.Size(425, 105);
            this.TutorialsButton.TabIndex = 16;
            this.TutorialsButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TutorialsButton_MouseClick);
            this.TutorialsButton.MouseEnter += new System.EventHandler(this.TutorialsButton_MouseEnter);
            this.TutorialsButton.MouseLeave += new System.EventHandler(this.TutorialsButton_MouseLeave);
            // 
            // tutorialDescriptionLabel
            // 
            this.tutorialDescriptionLabel.AutoSize = true;
            this.tutorialDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tutorialDescriptionLabel.Location = new System.Drawing.Point(141, 47);
            this.tutorialDescriptionLabel.Name = "tutorialDescriptionLabel";
            this.tutorialDescriptionLabel.Size = new System.Drawing.Size(280, 30);
            this.tutorialDescriptionLabel.TabIndex = 2;
            this.tutorialDescriptionLabel.Text = "Learn about trading card games, the playmat, your\r\navatar, playing a card, and ho" +
    "w to win the game.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(140, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Tutorials";
            // 
            // TutorialsButtonImage
            // 
            this.TutorialsButtonImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.TutorialsButtonImage.Location = new System.Drawing.Point(1, 2);
            this.TutorialsButtonImage.Name = "TutorialsButtonImage";
            this.TutorialsButtonImage.Size = new System.Drawing.Size(133, 100);
            this.TutorialsButtonImage.TabIndex = 0;
            this.TutorialsButtonImage.TabStop = false;
            // 
            // SinglePlayerOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.SelectButton);
            this.DoubleBuffered = true;
            this.Name = "SinglePlayerOptions";
            this.Size = new System.Drawing.Size(500, 500);
            this.panel1.ResumeLayout(false);
            this.SkirmishButton.ResumeLayout(false);
            this.SkirmishButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SkirmishButtonImage)).EndInit();
            this.ScenariosButton.ResumeLayout(false);
            this.ScenariosButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScenariosButtonImage)).EndInit();
            this.TutorialsButton.ResumeLayout(false);
            this.TutorialsButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TutorialsButtonImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel SkirmishButton;
        private System.Windows.Forms.Label skirmishDescriptionLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox SkirmishButtonImage;
        private System.Windows.Forms.Panel ScenariosButton;
        private System.Windows.Forms.Label scenariosDescriptionLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox ScenariosButtonImage;
        private System.Windows.Forms.Panel TutorialsButton;
        private System.Windows.Forms.Label tutorialDescriptionLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox TutorialsButtonImage;
    }
}
