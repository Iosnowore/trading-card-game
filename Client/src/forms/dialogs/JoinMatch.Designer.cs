﻿namespace TradingCardGame.forms.dialogs
{
    partial class JoinMatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.joinButton = new System.Windows.Forms.Button();
            this.joinMatchPasswordTextBox = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.deckSelectionTextBox = new System.Windows.Forms.TextBox();
            this.selectDeckButton = new System.Windows.Forms.Button();
            this.deckStatusPictureBox = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.joinMatchFormatLabel = new System.Windows.Forms.Label();
            this.matchTitleLabel = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.deckStatusPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.SuspendLayout();
            // 
            // joinButton
            // 
            this.joinButton.BackColor = System.Drawing.Color.Black;
            this.joinButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.joinButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.joinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.joinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.joinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.joinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinButton.ForeColor = System.Drawing.Color.Black;
            this.joinButton.Location = new System.Drawing.Point(450, 264);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(85, 29);
            this.joinButton.TabIndex = 51;
            this.joinButton.Text = "Join";
            this.joinButton.UseVisualStyleBackColor = false;
            this.joinButton.Click += new System.EventHandler(this.joinButton_Click);
            // 
            // joinMatchPasswordTextBox
            // 
            this.joinMatchPasswordTextBox.BackColor = System.Drawing.Color.Black;
            this.joinMatchPasswordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.joinMatchPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinMatchPasswordTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.joinMatchPasswordTextBox.Location = new System.Drawing.Point(80, 152);
            this.joinMatchPasswordTextBox.Name = "joinMatchPasswordTextBox";
            this.joinMatchPasswordTextBox.Size = new System.Drawing.Size(376, 22);
            this.joinMatchPasswordTextBox.TabIndex = 50;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(133)))), ((int)(((byte)(172)))));
            this.passwordLabel.Location = new System.Drawing.Point(10, 154);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(64, 15);
            this.passwordLabel.TabIndex = 49;
            this.passwordLabel.Text = "Password:";
            // 
            // deckSelectionTextBox
            // 
            this.deckSelectionTextBox.BackColor = System.Drawing.Color.Black;
            this.deckSelectionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.deckSelectionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deckSelectionTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.deckSelectionTextBox.Location = new System.Drawing.Point(13, 120);
            this.deckSelectionTextBox.Name = "deckSelectionTextBox";
            this.deckSelectionTextBox.ReadOnly = true;
            this.deckSelectionTextBox.Size = new System.Drawing.Size(200, 22);
            this.deckSelectionTextBox.TabIndex = 46;
            // 
            // selectDeckButton
            // 
            this.selectDeckButton.BackColor = System.Drawing.Color.Black;
            this.selectDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.selectDeckButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.selectDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.selectDeckButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.selectDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectDeckButton.ForeColor = System.Drawing.Color.Black;
            this.selectDeckButton.Location = new System.Drawing.Point(334, 114);
            this.selectDeckButton.Name = "selectDeckButton";
            this.selectDeckButton.Size = new System.Drawing.Size(125, 30);
            this.selectDeckButton.TabIndex = 47;
            this.selectDeckButton.Text = "Select Deck";
            this.selectDeckButton.UseVisualStyleBackColor = false;
            this.selectDeckButton.Click += new System.EventHandler(this.selectDeckButton_Click);
            // 
            // deckStatusPictureBox
            // 
            this.deckStatusPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.deckStatusPictureBox.Location = new System.Drawing.Point(228, 116);
            this.deckStatusPictureBox.Name = "deckStatusPictureBox";
            this.deckStatusPictureBox.Size = new System.Drawing.Size(91, 29);
            this.deckStatusPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.deckStatusPictureBox.TabIndex = 48;
            this.deckStatusPictureBox.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(133)))), ((int)(((byte)(172)))));
            this.label19.Location = new System.Drawing.Point(10, 102);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 15);
            this.label19.TabIndex = 45;
            this.label19.Text = "Deck Selection:";
            // 
            // joinMatchFormatLabel
            // 
            this.joinMatchFormatLabel.AutoSize = true;
            this.joinMatchFormatLabel.BackColor = System.Drawing.Color.Black;
            this.joinMatchFormatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinMatchFormatLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(133)))), ((int)(((byte)(172)))));
            this.joinMatchFormatLabel.Location = new System.Drawing.Point(10, 76);
            this.joinMatchFormatLabel.Name = "joinMatchFormatLabel";
            this.joinMatchFormatLabel.Size = new System.Drawing.Size(111, 16);
            this.joinMatchFormatLabel.TabIndex = 44;
            this.joinMatchFormatLabel.Text = "Format: Standard";
            // 
            // matchTitleLabel
            // 
            this.matchTitleLabel.AutoSize = true;
            this.matchTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchTitleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(133)))), ((int)(((byte)(172)))));
            this.matchTitleLabel.Location = new System.Drawing.Point(12, 25);
            this.matchTitleLabel.Name = "matchTitleLabel";
            this.matchTitleLabel.Size = new System.Drawing.Size(159, 20);
            this.matchTitleLabel.TabIndex = 43;
            this.matchTitleLabel.Text = "Iosnowore\'s Match";
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Black;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Location = new System.Drawing.Point(496, 8);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(42, 37);
            this.exitButton.TabIndex = 42;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitJoinMatchButton_Click);
            this.exitButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseDown);
            this.exitButton.MouseEnter += new System.EventHandler(this.exitButton_MouseEnter);
            this.exitButton.MouseLeave += new System.EventHandler(this.exitButton_MouseLeave);
            // 
            // JoinMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.joinMatchPasswordTextBox);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.deckSelectionTextBox);
            this.Controls.Add(this.selectDeckButton);
            this.Controls.Add(this.deckStatusPictureBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.joinMatchFormatLabel);
            this.Controls.Add(this.matchTitleLabel);
            this.Controls.Add(this.exitButton);
            this.DoubleBuffered = true;
            this.Name = "JoinMatch";
            this.Size = new System.Drawing.Size(550, 300);
            this.Load += new System.EventHandler(this.JoinMatch_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.JoinMatch_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.JoinMatch_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.deckStatusPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button joinButton;
        private System.Windows.Forms.TextBox joinMatchPasswordTextBox;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox deckSelectionTextBox;
        private System.Windows.Forms.Button selectDeckButton;
        private System.Windows.Forms.PictureBox deckStatusPictureBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label joinMatchFormatLabel;
        private System.Windows.Forms.Label matchTitleLabel;
        private System.Windows.Forms.PictureBox exitButton;
    }
}