﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Src.Services.Deck;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class Validation : UserControl
    {
        private readonly SortedDictionary<int, int> DeckCards;

        public Validation(SortedDictionary<int, int> DeckCards)
        {
            InitializeComponent();

            TreeView.ImageList = ResourcesLib.ICONS_IMAGES;
            TopListView.SmallImageList = ResourcesLib.ICONS_IMAGES;
            //TreeView.Nodes[0].SelectedImageIndex = 12;

            this.DeckCards = DeckCards;
        }

        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (TreeView.SelectedNode != null)
            {
                TopListView.Items.Clear();

                switch (TreeView.SelectedNode.Text)
                {
                    case "Standard":
                        StandardCheck();
                        break;
                }
            }
        }

        private void StandardCheck()
        {
            int[] CardIds = new int[DeckCards.Count];
            DeckCards.Keys.CopyTo(CardIds, 0);

            // Check Avatar
            TopListView.Items.Add("Avatar", DeckService.CheckAvatarValidation(CardIds) ? "yes" : "no");

            // Check Quests
            TopListView.Items.Add("Quests", DeckService.CheckQuestsValidation(CardIds) ? "yes" : "no");
            TopListView.Items[1].BackColor = Color.DimGray;

            // Check Deck
            TopListView.Items.Add("Deck", DeckService.CheckDeckValidation(CardIds, DeckCards) ? "yes" : "no");

            // Check Ownership
            TopListView.Items.Add("Ownership", DeckService.CheckOwnershipValidation(CardIds) ? "yes" : "no");
            TopListView.Items[3].BackColor = Color.DimGray;
        }
    }
}
