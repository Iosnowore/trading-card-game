﻿
namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class Scenarios
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScenariosTreeView = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ScenarioTitleLabel = new System.Windows.Forms.Label();
            this.ScenarioDescriptionLabel = new System.Windows.Forms.Label();
            this.DeckNameTextBox = new System.Windows.Forms.TextBox();
            this.SelectDeckButton = new System.Windows.Forms.Button();
            this.EasyRadioButton = new System.Windows.Forms.RadioButton();
            this.MediumRadioButton = new System.Windows.Forms.RadioButton();
            this.HardRadioButton = new System.Windows.Forms.RadioButton();
            this.BeginButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ValidateButton = new System.Windows.Forms.Button();
            this.DifficultyPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ScenariosTreeView
            // 
            this.ScenariosTreeView.BackColor = System.Drawing.Color.Black;
            this.ScenariosTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScenariosTreeView.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.ScenariosTreeView.FullRowSelect = true;
            this.ScenariosTreeView.HideSelection = false;
            this.ScenariosTreeView.Location = new System.Drawing.Point(3, 3);
            this.ScenariosTreeView.Name = "ScenariosTreeView";
            this.ScenariosTreeView.Size = new System.Drawing.Size(388, 432);
            this.ScenariosTreeView.TabIndex = 0;
            this.ScenariosTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ScenariosTreeView_AfterSelect);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.ScenarioTitleLabel);
            this.panel1.Location = new System.Drawing.Point(397, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 53);
            this.panel1.TabIndex = 1;
            // 
            // ScenarioTitleLabel
            // 
            this.ScenarioTitleLabel.AutoSize = true;
            this.ScenarioTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScenarioTitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.ScenarioTitleLabel.Location = new System.Drawing.Point(3, 14);
            this.ScenarioTitleLabel.Name = "ScenarioTitleLabel";
            this.ScenarioTitleLabel.Size = new System.Drawing.Size(119, 20);
            this.ScenarioTitleLabel.TabIndex = 0;
            this.ScenarioTitleLabel.Text = "Scenario Title";
            // 
            // ScenarioDescriptionLabel
            // 
            this.ScenarioDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScenarioDescriptionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.ScenarioDescriptionLabel.Location = new System.Drawing.Point(394, 59);
            this.ScenarioDescriptionLabel.Name = "ScenarioDescriptionLabel";
            this.ScenarioDescriptionLabel.Size = new System.Drawing.Size(100, 23);
            this.ScenarioDescriptionLabel.TabIndex = 2;
            this.ScenarioDescriptionLabel.Text = "Scenario Description";
            // 
            // DeckNameTextBox
            // 
            this.DeckNameTextBox.BackColor = System.Drawing.Color.Black;
            this.DeckNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeckNameTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DeckNameTextBox.Location = new System.Drawing.Point(397, 264);
            this.DeckNameTextBox.Name = "DeckNameTextBox";
            this.DeckNameTextBox.Size = new System.Drawing.Size(384, 22);
            this.DeckNameTextBox.TabIndex = 3;
            // 
            // SelectDeckButton
            // 
            this.SelectDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectDeckButton.Location = new System.Drawing.Point(670, 290);
            this.SelectDeckButton.Name = "SelectDeckButton";
            this.SelectDeckButton.Size = new System.Drawing.Size(111, 29);
            this.SelectDeckButton.TabIndex = 4;
            this.SelectDeckButton.Text = "Select Deck";
            this.SelectDeckButton.UseVisualStyleBackColor = true;
            this.SelectDeckButton.Click += new System.EventHandler(this.SelectDeckButton_Click);
            // 
            // EasyRadioButton
            // 
            this.EasyRadioButton.AutoSize = true;
            this.EasyRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EasyRadioButton.Location = new System.Drawing.Point(495, 356);
            this.EasyRadioButton.Name = "EasyRadioButton";
            this.EasyRadioButton.Size = new System.Drawing.Size(51, 19);
            this.EasyRadioButton.TabIndex = 6;
            this.EasyRadioButton.TabStop = true;
            this.EasyRadioButton.Text = "Easy";
            this.EasyRadioButton.UseVisualStyleBackColor = true;
            // 
            // MediumRadioButton
            // 
            this.MediumRadioButton.AutoSize = true;
            this.MediumRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MediumRadioButton.Location = new System.Drawing.Point(592, 356);
            this.MediumRadioButton.Name = "MediumRadioButton";
            this.MediumRadioButton.Size = new System.Drawing.Size(71, 19);
            this.MediumRadioButton.TabIndex = 7;
            this.MediumRadioButton.TabStop = true;
            this.MediumRadioButton.Text = "Medium";
            this.MediumRadioButton.UseVisualStyleBackColor = true;
            // 
            // HardRadioButton
            // 
            this.HardRadioButton.AutoSize = true;
            this.HardRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HardRadioButton.Location = new System.Drawing.Point(704, 356);
            this.HardRadioButton.Name = "HardRadioButton";
            this.HardRadioButton.Size = new System.Drawing.Size(52, 19);
            this.HardRadioButton.TabIndex = 8;
            this.HardRadioButton.TabStop = true;
            this.HardRadioButton.Text = "Hard";
            this.HardRadioButton.UseVisualStyleBackColor = true;
            // 
            // BeginButton
            // 
            this.BeginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BeginButton.Location = new System.Drawing.Point(696, 404);
            this.BeginButton.Name = "BeginButton";
            this.BeginButton.Size = new System.Drawing.Size(85, 31);
            this.BeginButton.TabIndex = 9;
            this.BeginButton.Text = "Begin";
            this.BeginButton.UseVisualStyleBackColor = true;
            this.BeginButton.Click += new System.EventHandler(this.BeginButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(402, 358);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Difficulty:";
            // 
            // ValidateButton
            // 
            this.ValidateButton.BackColor = System.Drawing.Color.Black;
            this.ValidateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ValidateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ValidateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ValidateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValidateButton.ForeColor = System.Drawing.Color.Black;
            this.ValidateButton.Location = new System.Drawing.Point(573, 290);
            this.ValidateButton.Name = "ValidateButton";
            this.ValidateButton.Size = new System.Drawing.Size(91, 29);
            this.ValidateButton.TabIndex = 80;
            this.ValidateButton.UseVisualStyleBackColor = false;
            // 
            // DifficultyPanel
            // 
            this.DifficultyPanel.Location = new System.Drawing.Point(397, 174);
            this.DifficultyPanel.Name = "DifficultyPanel";
            this.DifficultyPanel.Size = new System.Drawing.Size(60, 84);
            this.DifficultyPanel.TabIndex = 82;
            this.DifficultyPanel.Visible = false;
            // 
            // Scenarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DifficultyPanel);
            this.Controls.Add(this.ValidateButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BeginButton);
            this.Controls.Add(this.HardRadioButton);
            this.Controls.Add(this.MediumRadioButton);
            this.Controls.Add(this.EasyRadioButton);
            this.Controls.Add(this.SelectDeckButton);
            this.Controls.Add(this.DeckNameTextBox);
            this.Controls.Add(this.ScenarioDescriptionLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ScenariosTreeView);
            this.Name = "Scenarios";
            this.Size = new System.Drawing.Size(800, 450);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView ScenariosTreeView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ScenarioTitleLabel;
        private System.Windows.Forms.Label ScenarioDescriptionLabel;
        private System.Windows.Forms.TextBox DeckNameTextBox;
        private System.Windows.Forms.Button SelectDeckButton;
        private System.Windows.Forms.RadioButton EasyRadioButton;
        private System.Windows.Forms.RadioButton MediumRadioButton;
        private System.Windows.Forms.RadioButton HardRadioButton;
        private System.Windows.Forms.Button BeginButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ValidateButton;
        private System.Windows.Forms.Panel DifficultyPanel;
    }
}
