﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class RedemptionDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.SubtitleLabel = new System.Windows.Forms.Label();
            this.DescriptionTitleLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.TermsButton = new System.Windows.Forms.Button();
            this.RedeemButton = new System.Windows.Forms.Button();
            this.TransmittingLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(254, 11);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(93, 42);
            this.TitleLabel.TabIndex = 0;
            this.TitleLabel.Text = "Title";
            // 
            // SubtitleLabel
            // 
            this.SubtitleLabel.AutoSize = true;
            this.SubtitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubtitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.SubtitleLabel.Location = new System.Drawing.Point(235, 53);
            this.SubtitleLabel.Name = "SubtitleLabel";
            this.SubtitleLabel.Size = new System.Drawing.Size(131, 37);
            this.SubtitleLabel.TabIndex = 1;
            this.SubtitleLabel.Text = "Subtitle";
            // 
            // DescriptionTitleLabel
            // 
            this.DescriptionTitleLabel.AutoSize = true;
            this.DescriptionTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionTitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DescriptionTitleLabel.Location = new System.Drawing.Point(310, 131);
            this.DescriptionTitleLabel.Name = "DescriptionTitleLabel";
            this.DescriptionTitleLabel.Size = new System.Drawing.Size(105, 20);
            this.DescriptionTitleLabel.TabIndex = 2;
            this.DescriptionTitleLabel.Text = "Description:";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DescriptionLabel.Location = new System.Drawing.Point(310, 151);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(276, 68);
            this.DescriptionLabel.TabIndex = 3;
            this.DescriptionLabel.Text = "Description";
            // 
            // TermsButton
            // 
            this.TermsButton.BackColor = System.Drawing.Color.Transparent;
            this.TermsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TermsButton.FlatAppearance.BorderSize = 0;
            this.TermsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.TermsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.TermsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TermsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TermsButton.Location = new System.Drawing.Point(439, 426);
            this.TermsButton.Name = "TermsButton";
            this.TermsButton.Size = new System.Drawing.Size(149, 30);
            this.TermsButton.TabIndex = 4;
            this.TermsButton.Text = "Terms / Conditions";
            this.TermsButton.UseVisualStyleBackColor = false;
            this.TermsButton.Click += new System.EventHandler(this.TermsButton_Click);
            // 
            // RedeemButton
            // 
            this.RedeemButton.BackColor = System.Drawing.Color.Transparent;
            this.RedeemButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RedeemButton.FlatAppearance.BorderSize = 0;
            this.RedeemButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RedeemButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RedeemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RedeemButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RedeemButton.Location = new System.Drawing.Point(488, 462);
            this.RedeemButton.Name = "RedeemButton";
            this.RedeemButton.Size = new System.Drawing.Size(100, 30);
            this.RedeemButton.TabIndex = 5;
            this.RedeemButton.Text = "Redeem";
            this.RedeemButton.UseVisualStyleBackColor = false;
            this.RedeemButton.Click += new System.EventHandler(this.RedeemButton_Click);
            // 
            // TransmittingLabel
            // 
            this.TransmittingLabel.AutoSize = true;
            this.TransmittingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmittingLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TransmittingLabel.Location = new System.Drawing.Point(13, 476);
            this.TransmittingLabel.Name = "TransmittingLabel";
            this.TransmittingLabel.Size = new System.Drawing.Size(76, 16);
            this.TransmittingLabel.TabIndex = 6;
            this.TransmittingLabel.Text = "Description";
            this.TransmittingLabel.Visible = false;
            // 
            // RedemptionDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.TransmittingLabel);
            this.Controls.Add(this.RedeemButton);
            this.Controls.Add(this.TermsButton);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.DescriptionTitleLabel);
            this.Controls.Add(this.SubtitleLabel);
            this.Controls.Add(this.TitleLabel);
            this.DoubleBuffered = true;
            this.Name = "RedemptionDetail";
            this.Size = new System.Drawing.Size(600, 500);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label SubtitleLabel;
        private System.Windows.Forms.Label DescriptionTitleLabel;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Button TermsButton;
        private System.Windows.Forms.Button RedeemButton;
        private System.Windows.Forms.Label TransmittingLabel;
    }
}
