﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class TestDraw : UserControl
    {
        private readonly Card[] Cards = new Card[6];
        private readonly int[] CardIds;
        private readonly List<int> NotChosenCardDataIndexes = new List<int>();

        public TestDraw(int[] CardIds)
        {
            InitializeComponent();

            this.CardIds = CardIds;
            ButtonAnimation.FormatButton(ShuffleAndRedrawButton);
            ButtonAnimation.FormatButton(DrawButton);

            foreach (int Id in CardIds)
                NotChosenCardDataIndexes.Add(Id);
            Draw();
        }

        private void DrawButton_Click(object sender, EventArgs e)
        {
            Draw();
        }

        private void Draw()
        {
            SoundService.PlaySound("button");

            Random Random = new Random();

            for (int i = 0; i < Cards.Length; i++)
            {
                if (NotChosenCardDataIndexes.Count > 0)
                {
                    int Index = Random.Next(0, NotChosenCardDataIndexes.Count);
                    Cards[i] = new Card(TCGData.Cards.GetCardData(NotChosenCardDataIndexes[Index]), Card.SIZE.MEDIUM);
                    Cards[i].Visible = true;
                    if (i != 0)
                        Cards[i].Location = new Point(Cards[i - 1].Location.X + (Cards[i - 1].Width / 2), Cards[i - 1].Location.Y);
                    Controls.Add(Cards[i]);

                    NotChosenCardDataIndexes.Remove(NotChosenCardDataIndexes[Index]);
                }
                else
                {
                    Cards[i] = null;
                    Cards[i].Visible = false;
                }
            }
        }

        private void ShuffleAndRedrawButton_Click(object sender, EventArgs e)
        {
            NotChosenCardDataIndexes.Clear();

            foreach (int CardId in CardIds)
                NotChosenCardDataIndexes.Add(CardId);

            Draw();
        }
    }
}
