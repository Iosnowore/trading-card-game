﻿namespace TradingCardGame.forms
{
    partial class Guild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.guildsListViewer = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.numberOfFriendsGuildsLabel = new System.Windows.Forms.Label();
            this.numberOfGuildsLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.joiningAGuildDescriptionLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.creatingAGuildDescriptionLabel = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.createGuildButton = new System.Windows.Forms.Button();
            this.leaveGuildButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.Black;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.ForeColor = System.Drawing.Color.SkyBlue;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(259, 409);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(470, 140);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Rank";
            this.columnHeader5.Width = 75;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Name";
            this.columnHeader6.Width = 186;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "# Members";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 80;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Leader";
            this.columnHeader8.Width = 125;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Friends\' Guilds";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "All guilds";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(75, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // guildsListViewer
            // 
            this.guildsListViewer.BackColor = System.Drawing.Color.Black;
            this.guildsListViewer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.guildsListViewer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildsListViewer.ForeColor = System.Drawing.Color.SkyBlue;
            this.guildsListViewer.FullRowSelect = true;
            this.guildsListViewer.Location = new System.Drawing.Point(259, 80);
            this.guildsListViewer.MultiSelect = false;
            this.guildsListViewer.Name = "guildsListViewer";
            this.guildsListViewer.Size = new System.Drawing.Size(472, 279);
            this.guildsListViewer.TabIndex = 4;
            this.guildsListViewer.UseCompatibleStateImageBehavior = false;
            this.guildsListViewer.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Rank";
            this.columnHeader1.Width = 75;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 188;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "# Members";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Leader";
            this.columnHeader4.Width = 125;
            // 
            // numberOfFriendsGuildsLabel
            // 
            this.numberOfFriendsGuildsLabel.AutoSize = true;
            this.numberOfFriendsGuildsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfFriendsGuildsLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.numberOfFriendsGuildsLabel.Location = new System.Drawing.Point(601, 572);
            this.numberOfFriendsGuildsLabel.Name = "numberOfFriendsGuildsLabel";
            this.numberOfFriendsGuildsLabel.Size = new System.Drawing.Size(125, 16);
            this.numberOfFriendsGuildsLabel.TabIndex = 11;
            this.numberOfFriendsGuildsLabel.Text = "0 guilds with friends.";
            // 
            // numberOfGuildsLabel
            // 
            this.numberOfGuildsLabel.AutoSize = true;
            this.numberOfGuildsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfGuildsLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.numberOfGuildsLabel.Location = new System.Drawing.Point(601, 556);
            this.numberOfGuildsLabel.Name = "numberOfGuildsLabel";
            this.numberOfGuildsLabel.Size = new System.Drawing.Size(57, 16);
            this.numberOfGuildsLabel.TabIndex = 10;
            this.numberOfGuildsLabel.Text = "0 guilds.";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(259, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 35);
            this.panel1.TabIndex = 53;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(257, 368);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(472, 35);
            this.panel3.TabIndex = 54;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.joiningAGuildDescriptionLabel);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.creatingAGuildDescriptionLabel);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(250, 546);
            this.panel4.TabIndex = 55;
            // 
            // joiningAGuildDescriptionLabel
            // 
            this.joiningAGuildDescriptionLabel.AutoSize = true;
            this.joiningAGuildDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joiningAGuildDescriptionLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.joiningAGuildDescriptionLabel.Location = new System.Drawing.Point(0, 346);
            this.joiningAGuildDescriptionLabel.Name = "joiningAGuildDescriptionLabel";
            this.joiningAGuildDescriptionLabel.Size = new System.Drawing.Size(176, 16);
            this.joiningAGuildDescriptionLabel.TabIndex = 57;
            this.joiningAGuildDescriptionLabel.Text = "Joining a Guild Description...";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label7);
            this.panel6.Location = new System.Drawing.Point(0, 308);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(250, 35);
            this.panel6.TabIndex = 56;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 25);
            this.label7.TabIndex = 5;
            this.label7.Text = "Joining a Guild";
            // 
            // creatingAGuildDescriptionLabel
            // 
            this.creatingAGuildDescriptionLabel.AutoSize = true;
            this.creatingAGuildDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creatingAGuildDescriptionLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.creatingAGuildDescriptionLabel.Location = new System.Drawing.Point(3, 145);
            this.creatingAGuildDescriptionLabel.Name = "creatingAGuildDescriptionLabel";
            this.creatingAGuildDescriptionLabel.Size = new System.Drawing.Size(183, 16);
            this.creatingAGuildDescriptionLabel.TabIndex = 55;
            this.creatingAGuildDescriptionLabel.Text = "Creating a Guild Description...";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(0, 107);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(250, 35);
            this.panel5.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Creating a Guild";
            // 
            // createGuildButton
            // 
            this.createGuildButton.BackColor = System.Drawing.Color.Black;
            this.createGuildButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createGuildButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.createGuildButton.FlatAppearance.BorderSize = 0;
            this.createGuildButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.createGuildButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.createGuildButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createGuildButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createGuildButton.ForeColor = System.Drawing.Color.Black;
            this.createGuildButton.Location = new System.Drawing.Point(259, 6);
            this.createGuildButton.Name = "createGuildButton";
            this.createGuildButton.Size = new System.Drawing.Size(100, 29);
            this.createGuildButton.TabIndex = 56;
            this.createGuildButton.Text = "Create";
            this.createGuildButton.UseVisualStyleBackColor = false;
            this.createGuildButton.Click += new System.EventHandler(this.createGuildButton_Click);
            // 
            // leaveGuildButton
            // 
            this.leaveGuildButton.BackColor = System.Drawing.Color.Black;
            this.leaveGuildButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.leaveGuildButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.leaveGuildButton.FlatAppearance.BorderSize = 0;
            this.leaveGuildButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.leaveGuildButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.leaveGuildButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.leaveGuildButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaveGuildButton.ForeColor = System.Drawing.Color.Black;
            this.leaveGuildButton.Location = new System.Drawing.Point(365, 6);
            this.leaveGuildButton.Name = "leaveGuildButton";
            this.leaveGuildButton.Size = new System.Drawing.Size(125, 29);
            this.leaveGuildButton.TabIndex = 57;
            this.leaveGuildButton.Text = "Leave Guild";
            this.leaveGuildButton.UseVisualStyleBackColor = false;
            this.leaveGuildButton.Click += new System.EventHandler(this.leaveGuildButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Black;
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Black;
            this.closeButton.Location = new System.Drawing.Point(631, 596);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(100, 29);
            this.closeButton.TabIndex = 58;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // Guild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.leaveGuildButton);
            this.Controls.Add(this.createGuildButton);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.numberOfFriendsGuildsLabel);
            this.Controls.Add(this.numberOfGuildsLabel);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.guildsListViewer);
            this.DoubleBuffered = true;
            this.Name = "Guild";
            this.Size = new System.Drawing.Size(750, 625);
            this.Load += new System.EventHandler(this.Guild_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView guildsListViewer;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label numberOfFriendsGuildsLabel;
        private System.Windows.Forms.Label numberOfGuildsLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button createGuildButton;
        private System.Windows.Forms.Button leaveGuildButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label creatingAGuildDescriptionLabel;
        private System.Windows.Forms.Label joiningAGuildDescriptionLabel;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
    }
}