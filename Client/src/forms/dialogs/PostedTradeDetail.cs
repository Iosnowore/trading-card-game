﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class PostedTradeDetail : UserControl
    {
        private readonly int Id;

        private readonly int[] OfferedIds;
        private readonly int[] WantIds;

        private readonly Card Card;

        public PostedTradeDetail(SortedDictionary<int, int> Offered, SortedDictionary<int, int> Want, int Id, string OffererUsername)
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(AcceptButton);
            ButtonAnimation.FormatButton(CancelButton);

            if (!PlayerService.GetUsername().Equals(OffererUsername))
            {
                CancelButton.Visible = false;
            }

            this.Id = Id;

            OfferedIds = new int[Offered.Count];
            int[] OfferedQuantities = new int[Offered.Count];

            Offered.Keys.CopyTo(OfferedIds, 0);
            Offered.Values.CopyTo(OfferedQuantities, 0);

            for (int i = 0; i < Offered.Count; i++)
                OfferedListView.Items.Add(new ListViewItem(new string[] { TCGData.Cards.GetCardData(OfferedIds[i]).GetTitle(), OfferedQuantities[i].ToString(), "No" }));

            WantIds = new int[Want.Count];
            int[] WantQuantities = new int[Want.Count];

            Want.Keys.CopyTo(WantIds, 0);
            Want.Values.CopyTo(WantQuantities, 0);

            for (int i = 0; i < Want.Count; i++)
                WantListView.Items.Add(new ListViewItem(new string[] { TCGData.Cards.GetCardData(WantIds[i]).GetTitle(), WantQuantities[i].ToString(), "No" }));

            Card = new Card(TCGData.Cards.GetCardData(0), Card.SIZE.MEDIUM);
            Card.Location = new Point(10, 30);
            Card.Visible = false;
            Controls.Add(Card);
        }

        private void OfferedListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (OfferedListView.SelectedItems.Count > 0)
            {
                Card.ChangeCard(TCGData.Cards.GetCardData(OfferedIds[OfferedListView.SelectedItems[0].Index]));
                Card.Visible = true;
            }
            else
            {
                Card.Visible = false;
            }
        }

        private void WantListView_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (WantListView.SelectedItems.Count > 0)
            {
                Card.ChangeCard(TCGData.Cards.GetCardData(WantIds[WantListView.SelectedItems[0].Index]));
                Card.Visible = true;
            }
            else
            {
                Card.Visible = false;
            }
        }

        private void AcceptButton_Click(object sender, System.EventArgs e)
        {
            PacketService.SendPacket(Commands.POSTEDTRADE_ACCEPT, new CommandPacket(new string[] { Id.ToString() }));
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            PacketService.SendPacket(Commands.POSTEDTRADE_CANCEL, new CommandPacket(new string[] { Id.ToString() }));
            SoundService.PlaySound("button");
            Parent.Dispose();
        }
    }
}
