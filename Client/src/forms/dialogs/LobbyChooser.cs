﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Lobby;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.forms.dialogs
{
    public partial class LobbyChooser : UserControl
    {
        private Panel lastClickedCasual;

        public LobbyChooser()
        {
            InitializeComponent();

            // Format buttons
            ButtonAnimation.FormatButton(joinButton);
            ButtonAnimation.FormatButton(cancelButton);

            // Disable button
            ButtonAnimation.DisableButton(joinButton);

            CasualGamesButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual");
            mosEisleyButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual");
            mosEspaButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual");
            toscheStationButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual");

            UpdateAllLobbyInfoLabels();
        }

        private string GetFormattedInfoLabel(string LobbyInfo)
        {
            string[] LobbyInfoSplit = LobbyInfo.Split(':');
            string InfoString = "Users: " + LobbyInfoSplit[0] + " / " + LobbyInfoSplit[1] + "\nTotal Matches: " + LobbyInfoSplit[2] + "\nWaiting: " + LobbyInfoSplit[3];
            return InfoString;
        }

        private void UpdateAllLobbyInfoLabels()
        {
            LobbyInfoPacket LobbyInfo = PacketService.GetLobbyInfoPacket();

            casualInfoLabel.Text = GetFormattedInfoLabel(LobbyInfo.GetLobbyInfo(0));
            MosEisleyInfoLabel.Text = GetFormattedInfoLabel(LobbyInfo.GetLobbyInfo(1));
            MosEspaInfoLabel.Text = GetFormattedInfoLabel(LobbyInfo.GetLobbyInfo(2));
            ToscheStationInfoLabel.Text = GetFormattedInfoLabel(LobbyInfo.GetLobbyInfo(3));
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void HelpButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void JoinButton_Click(object sender, System.EventArgs e)
        {
            if (lastClickedCasual == null)
                return;

            SoundService.PlaySound("button");

            if (lastClickedCasual == CasualGamesButton)
            {
                TCG TCG = (TCG)Parent.Parent.Parent;

                // Leave lobby if already in it
                if (TCG.GetChildWindow() is Lobby Lobby)
                {
                    PacketService.SendPacket(Commands.LOBBY_LEAVE, new CommandPacket(new string[] { Lobby.GetLobbyTitle() }));
                    Lobby.SetSentLeaveLobbyPacket();
                }
                TCG.SetChildWindow(new Lobby(LobbyTitles.CASUAL_GAMES));
            }
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private void CasualGamesButton_Click(object sender, System.EventArgs e)
        {
            LobbyClicked(CasualGamesButton);
        }

        private void LobbyClicked(Panel lobbyPanel)
        {
            if (lastClickedCasual != null)
                lastClickedCasual.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual");

            ButtonAnimation.EnableButton(joinButton);

            SoundService.PlaySound("button");
            CasualGamesButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_casual_over");
            lastClickedCasual = lobbyPanel;
        }
    }
}
