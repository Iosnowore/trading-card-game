﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData.Packets.Guild;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs.Guild;

namespace TradingCardGame.forms.dialogs
{
    public partial class GuildInfo : UserControl
    {
        private Point mouseDownPoint;
        private UserControl CurrentControl;
        private GuildInfoPacket GuildInfoPacket;

        public GuildInfo(GuildInfoPacket GuildInfoPacket)
        {
            InitializeComponent();
            overviewButton.Image = ResourcesLib.GetImageFromGuild("guild_overview");
            membersButton.Image = ResourcesLib.GetImageFromGuild("guild_members");
            settingsButton.Image = ResourcesLib.GetImageFromGuild("guild_settings");
            this.GuildInfoPacket = GuildInfoPacket;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void overviewButton_Click(object sender, EventArgs e)
        {
            ChangeUserControl(new Overview(GuildInfoPacket));
        }

        private void membersButton_Click(object sender, EventArgs e)
        {
            ChangeUserControl(new Members(GuildInfoPacket.GetLeader(), GuildInfoPacket.GetOfficers(), GuildInfoPacket.GetMembers()));
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            GuildInfoPacket = PacketService.GetGuildInfoPacket();
            ChangeUserControl(new Settings(GuildInfoPacket.GetMessage()));
        }

        private void GuildInfo_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseDownPoint.IsEmpty)
                Location = new Point(Location.X + (e.X - mouseDownPoint.X), Location.Y + (e.Y - mouseDownPoint.Y));
        }

        private void GuildInfo_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownPoint = new Point(e.X, e.Y);
        }

        private void GuildInfo_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownPoint = Point.Empty;
        }

        private void GuildInfo_Load(object sender, EventArgs e)
        {
            CurrentControl = new Overview(GuildInfoPacket);
            MainPanel.Controls.Add(CurrentControl);
        }

        private void ChangeUserControl(UserControl NewControl)
        {
            CurrentControl.Dispose();
            CurrentControl = NewControl;
            MainPanel.Controls.Add(CurrentControl);
            SoundService.PlaySound("button");
        }
    }
}
