﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Windows.Forms;
using TCGData.Packets.Guild;

namespace TradingCardGame.Src.Forms.Dialogs.Guild
{
    public partial class Overview : UserControl
    {
        public Overview(GuildInfoPacket GuildInfoPacket)
        {
            InitializeComponent();

            leaderLabel.Text = GuildInfoPacket.GetLeader();
            DateTimeOffset Time = DateTimeOffset.FromUnixTimeSeconds(GuildInfoPacket.GetCreationDate());
            formedLabel.Text = Time.ToString("M/d/yyyy");
            rankLabel.Text = GuildInfoPacket.GetRank().ToString();
            numberOfMembersLabel.Text = (GuildInfoPacket.GetMembers().Length + GuildInfoPacket.GetOfficers().Length + 1).ToString();

            guildOfficersListBox.SmallImageList = ResourcesLib.PLAYER_LIST;

            guildOfficersListBox.Items.Add(leaderLabel.Text, "bronze");

            foreach (string Officer in GuildInfoPacket.GetOfficers())
                guildOfficersListBox.Items.Add(Officer, "bronze");

            // Total Ratings
            int[] TotalRatings = GuildInfoPacket.GetTotalRatings();
            totalRatingsListBox.Items.Clear();
            totalRatingsListBox.Items.Add("Overall\t\t" + TotalRatings[0]);
            totalRatingsListBox.Items.Add("Limited\t\t" + TotalRatings[1]);
            totalRatingsListBox.Items.Add("Constructed\t" + TotalRatings[2]);

            // Average Ratings
            int[] AverageRatings = GuildInfoPacket.GetAverageRatings();
            averageRatingsListBox.Items.Clear();
            averageRatingsListBox.Items.Add("Overall\t\t" + AverageRatings[0]);
            averageRatingsListBox.Items.Add("Limited\t\t" + AverageRatings[1]);
            averageRatingsListBox.Items.Add("Constructed\t" + AverageRatings[2]);
        }
    }
}
