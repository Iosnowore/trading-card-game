﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs.Guild
{
    public partial class Settings : UserControl
    {
        private bool CheckBoxEnabled = false;

        public Settings(string GuildMessage)
        {
            InitializeComponent();

            CheckBox.Image = ResourcesLib.GetImageFromButtons("checkbox_off");
            ButtonAnimation.FormatButton(InviteButton);
            ButtonAnimation.FormatButton(SubmitButton);
            MessageTextBox.Text = GuildMessage;
        }

        private void InviteButton_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(InviteTextBox.Text))
                PacketService.SendPacket(Commands.GUILD_INVITE, new CommandPacket(new string[] { InviteTextBox.Text }));
            InviteTextBox.Text = string.Empty;
            SoundService.PlaySound("button");
        }

        private void SubmitButton_Click(object sender, System.EventArgs e)
        {
            PacketService.SendPacket(Commands.SAVE_GUILD_MESSAGE, new CommandPacket(new string[] { MessageTextBox.Text, CheckBoxEnabled ? "1" : "0" }));
            SoundService.PlaySound("button");
        }

        private void CheckBox_Click(object sender, System.EventArgs e)
        {
            CheckBoxEnabled = !CheckBoxEnabled;
            CheckBox.Image = ResourcesLib.GetImageFromButtons("checkbox_" + (CheckBoxEnabled ? "on" : "off"));
            SoundService.PlaySound("button");
        }
    }
}
