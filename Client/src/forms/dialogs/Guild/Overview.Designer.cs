﻿namespace TradingCardGame.Src.Forms.Dialogs.Guild
{
    partial class Overview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.averageRatingsListBox = new System.Windows.Forms.ListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.totalRatingsListBox = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.guildOfficersListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.numberOfMembersLabel = new System.Windows.Forms.Label();
            this.leaderLabel = new System.Windows.Forms.Label();
            this.rankLabel = new System.Windows.Forms.Label();
            this.formedLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // averageRatingsListBox
            // 
            this.averageRatingsListBox.BackColor = System.Drawing.Color.Black;
            this.averageRatingsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.averageRatingsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageRatingsListBox.ForeColor = System.Drawing.Color.White;
            this.averageRatingsListBox.FormattingEnabled = true;
            this.averageRatingsListBox.ItemHeight = 16;
            this.averageRatingsListBox.Items.AddRange(new object[] {
            "Overall\t\t1500",
            "Limited\t\t1500",
            "Constructed\t1500"});
            this.averageRatingsListBox.Location = new System.Drawing.Point(7, 394);
            this.averageRatingsListBox.Name = "averageRatingsListBox";
            this.averageRatingsListBox.Size = new System.Drawing.Size(492, 48);
            this.averageRatingsListBox.TabIndex = 97;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(7, 353);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(492, 35);
            this.panel5.TabIndex = 96;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.SkyBlue;
            this.label12.Location = new System.Drawing.Point(6, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 20);
            this.label12.TabIndex = 74;
            this.label12.Text = "Average Ratings";
            // 
            // totalRatingsListBox
            // 
            this.totalRatingsListBox.BackColor = System.Drawing.Color.Black;
            this.totalRatingsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.totalRatingsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRatingsListBox.ForeColor = System.Drawing.Color.White;
            this.totalRatingsListBox.FormattingEnabled = true;
            this.totalRatingsListBox.ItemHeight = 16;
            this.totalRatingsListBox.Items.AddRange(new object[] {
            "Overall\t\t1500",
            "Limited\t\t1500",
            "Constructed\t1500"});
            this.totalRatingsListBox.Location = new System.Drawing.Point(7, 299);
            this.totalRatingsListBox.Name = "totalRatingsListBox";
            this.totalRatingsListBox.Size = new System.Drawing.Size(492, 48);
            this.totalRatingsListBox.TabIndex = 95;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label11);
            this.panel4.Location = new System.Drawing.Point(7, 258);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(492, 35);
            this.panel4.TabIndex = 94;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.SkyBlue;
            this.label11.Location = new System.Drawing.Point(6, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 20);
            this.label11.TabIndex = 74;
            this.label11.Text = "Total Ratings";
            // 
            // guildOfficersListBox
            // 
            this.guildOfficersListBox.BackColor = System.Drawing.Color.Black;
            this.guildOfficersListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.guildOfficersListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guildOfficersListBox.ForeColor = System.Drawing.Color.White;
            this.guildOfficersListBox.FullRowSelect = true;
            this.guildOfficersListBox.Location = new System.Drawing.Point(7, 152);
            this.guildOfficersListBox.MultiSelect = false;
            this.guildOfficersListBox.Name = "guildOfficersListBox";
            this.guildOfficersListBox.Size = new System.Drawing.Size(492, 100);
            this.guildOfficersListBox.TabIndex = 93;
            this.guildOfficersListBox.UseCompatibleStateImageBehavior = false;
            this.guildOfficersListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 288;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(7, 111);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(492, 35);
            this.panel3.TabIndex = 92;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SkyBlue;
            this.label10.Location = new System.Drawing.Point(6, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 20);
            this.label10.TabIndex = 74;
            this.label10.Text = "Officers";
            // 
            // numberOfMembersLabel
            // 
            this.numberOfMembersLabel.AutoSize = true;
            this.numberOfMembersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfMembersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.numberOfMembersLabel.Location = new System.Drawing.Point(152, 57);
            this.numberOfMembersLabel.Name = "numberOfMembersLabel";
            this.numberOfMembersLabel.Size = new System.Drawing.Size(15, 16);
            this.numberOfMembersLabel.TabIndex = 91;
            this.numberOfMembersLabel.Text = "1";
            // 
            // leaderLabel
            // 
            this.leaderLabel.AutoSize = true;
            this.leaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.leaderLabel.Location = new System.Drawing.Point(58, 41);
            this.leaderLabel.Name = "leaderLabel";
            this.leaderLabel.Size = new System.Drawing.Size(54, 16);
            this.leaderLabel.TabIndex = 90;
            this.leaderLabel.Text = "Shocho";
            // 
            // rankLabel
            // 
            this.rankLabel.AutoSize = true;
            this.rankLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rankLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.rankLabel.Location = new System.Drawing.Point(50, 25);
            this.rankLabel.Name = "rankLabel";
            this.rankLabel.Size = new System.Drawing.Size(15, 16);
            this.rankLabel.TabIndex = 89;
            this.rankLabel.Text = "4";
            // 
            // formedLabel
            // 
            this.formedLabel.AutoSize = true;
            this.formedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formedLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.formedLabel.Location = new System.Drawing.Point(66, 9);
            this.formedLabel.Name = "formedLabel";
            this.formedLabel.Size = new System.Drawing.Size(51, 16);
            this.formedLabel.TabIndex = 88;
            this.formedLabel.Text = "7/25/08";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.SkyBlue;
            this.label8.Location = new System.Drawing.Point(4, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(151, 16);
            this.label8.TabIndex = 87;
            this.label8.Text = "Number of Members:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SkyBlue;
            this.label7.Location = new System.Drawing.Point(4, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 86;
            this.label7.Text = "Leader:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SkyBlue;
            this.label6.Location = new System.Drawing.Point(4, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.TabIndex = 85;
            this.label6.Text = "Rank:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SkyBlue;
            this.label5.Location = new System.Drawing.Point(4, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 16);
            this.label5.TabIndex = 84;
            this.label5.Text = "Formed:";
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.averageRatingsListBox);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.totalRatingsListBox);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.guildOfficersListBox);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.numberOfMembersLabel);
            this.Controls.Add(this.leaderLabel);
            this.Controls.Add(this.rankLabel);
            this.Controls.Add(this.formedLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Name = "Overview";
            this.Size = new System.Drawing.Size(503, 475);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox averageRatingsListBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox totalRatingsListBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListView guildOfficersListBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label numberOfMembersLabel;
        private System.Windows.Forms.Label leaderLabel;
        private System.Windows.Forms.Label rankLabel;
        private System.Windows.Forms.Label formedLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}
