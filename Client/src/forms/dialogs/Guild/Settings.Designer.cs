﻿namespace TradingCardGame.Src.Forms.Dialogs.Guild
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.leaderLabel = new System.Windows.Forms.Label();
            this.InviteButton = new System.Windows.Forms.Button();
            this.InviteTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.MessageTextBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CheckBox = new System.Windows.Forms.PictureBox();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.SkyBlue;
            this.label10.Location = new System.Drawing.Point(6, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(152, 20);
            this.label10.TabIndex = 74;
            this.label10.Text = "Invite New Members";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(5, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(492, 35);
            this.panel3.TabIndex = 95;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 74;
            this.label1.Text = "Guild Message";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(5, 126);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(492, 35);
            this.panel1.TabIndex = 96;
            // 
            // leaderLabel
            // 
            this.leaderLabel.AutoSize = true;
            this.leaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.leaderLabel.Location = new System.Drawing.Point(3, 41);
            this.leaderLabel.Name = "leaderLabel";
            this.leaderLabel.Size = new System.Drawing.Size(497, 48);
            this.leaderLabel.TabIndex = 97;
            this.leaderLabel.Text = resources.GetString("leaderLabel.Text");
            // 
            // InviteButton
            // 
            this.InviteButton.BackColor = System.Drawing.Color.Black;
            this.InviteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.InviteButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.InviteButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.InviteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.InviteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InviteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InviteButton.ForeColor = System.Drawing.Color.Black;
            this.InviteButton.Location = new System.Drawing.Point(392, 91);
            this.InviteButton.Name = "InviteButton";
            this.InviteButton.Size = new System.Drawing.Size(105, 29);
            this.InviteButton.TabIndex = 99;
            this.InviteButton.Text = "Invite";
            this.InviteButton.UseVisualStyleBackColor = false;
            this.InviteButton.Click += new System.EventHandler(this.InviteButton_Click);
            // 
            // InviteTextBox
            // 
            this.InviteTextBox.BackColor = System.Drawing.Color.Black;
            this.InviteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InviteTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InviteTextBox.ForeColor = System.Drawing.Color.White;
            this.InviteTextBox.Location = new System.Drawing.Point(3, 98);
            this.InviteTextBox.Name = "InviteTextBox";
            this.InviteTextBox.Size = new System.Drawing.Size(383, 22);
            this.InviteTextBox.TabIndex = 98;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(3, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(494, 32);
            this.label2.TabIndex = 100;
            this.label2.Text = "This message in the text box below will be presented after logging in. they can a" +
    "lso\r\nview this message in the My Guild interface.";
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.BackColor = System.Drawing.Color.Black;
            this.MessageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MessageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageTextBox.ForeColor = System.Drawing.Color.SkyBlue;
            this.MessageTextBox.Location = new System.Drawing.Point(5, 210);
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.Size = new System.Drawing.Size(492, 177);
            this.MessageTextBox.TabIndex = 101;
            this.MessageTextBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(35, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(336, 16);
            this.label3.TabIndex = 102;
            this.label3.Text = "Send to all logged in members of the guild immediately.";
            // 
            // CheckBox
            // 
            this.CheckBox.BackColor = System.Drawing.Color.Transparent;
            this.CheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CheckBox.Location = new System.Drawing.Point(6, 393);
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.Size = new System.Drawing.Size(23, 20);
            this.CheckBox.TabIndex = 103;
            this.CheckBox.TabStop = false;
            this.CheckBox.Click += new System.EventHandler(this.CheckBox_Click);
            // 
            // SubmitButton
            // 
            this.SubmitButton.BackColor = System.Drawing.Color.Black;
            this.SubmitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SubmitButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SubmitButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.SubmitButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.SubmitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitButton.ForeColor = System.Drawing.Color.Black;
            this.SubmitButton.Location = new System.Drawing.Point(412, 417);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(85, 29);
            this.SubmitButton.TabIndex = 104;
            this.SubmitButton.Text = "Submit";
            this.SubmitButton.UseVisualStyleBackColor = false;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.SubmitButton);
            this.Controls.Add(this.CheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.MessageTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.InviteButton);
            this.Controls.Add(this.InviteTextBox);
            this.Controls.Add(this.leaderLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(503, 475);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label leaderLabel;
        private System.Windows.Forms.Button InviteButton;
        private System.Windows.Forms.TextBox InviteTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox MessageTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox CheckBox;
        private System.Windows.Forms.Button SubmitButton;
    }
}
