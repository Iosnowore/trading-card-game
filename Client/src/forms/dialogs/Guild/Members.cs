﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TradingCardGame.Components;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.Src.Forms.Dialogs.Guild
{
    public partial class Members : UserControl
    {
        private RightClickOptions RightClickOptions = null;

        public Members(string LeaderUsername, string[] OfficerUsernames, string[] MemberUsernames)
        {
            InitializeComponent();

            MembersListBox.SmallImageList = ResourcesLib.PLAYER_LIST;

            MembersListBox.Items.Add(LeaderUsername, "bronze");

            foreach (string Officer in OfficerUsernames)
                MembersListBox.Items.Add(Officer, "bronze");

            foreach (string Member in MemberUsernames)
                MembersListBox.Items.Add(Member, "bronze");
        }

        private void MembersListBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (RightClickOptions != null)
                RightClickOptions.Dispose();

            if (e.Button == MouseButtons.Right && MembersListBox.SelectedItems != null && !MembersListBox.SelectedItems[0].Text.Equals(PlayerService.GetUsername()))
            {
                RightClickOptions = new RightClickOptions(MembersListBox.SelectedItems[0].Text);
                RightClickOptions.Location = e.Location;
                MembersListBox.Controls.Add(RightClickOptions);
            }
        }
    }
}
