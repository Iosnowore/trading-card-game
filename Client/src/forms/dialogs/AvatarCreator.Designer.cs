﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class AvatarCreator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.NextButton = new System.Windows.Forms.Button();
            this.BackButton = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.RadioButton1 = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.RadioButton2 = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.RadioButton3 = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.RadioButton4 = new System.Windows.Forms.Button();
            this.AvatarNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.BackColor = System.Drawing.Color.Transparent;
            this.QuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.QuestionLabel.Location = new System.Drawing.Point(267, 0);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(130, 20);
            this.QuestionLabel.TabIndex = 14;
            this.QuestionLabel.Text = "Question Label";
            // 
            // NextButton
            // 
            this.NextButton.BackColor = System.Drawing.Color.Transparent;
            this.NextButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NextButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.NextButton.FlatAppearance.BorderSize = 0;
            this.NextButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.NextButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.NextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NextButton.ForeColor = System.Drawing.Color.Black;
            this.NextButton.Location = new System.Drawing.Point(607, 428);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(90, 29);
            this.NextButton.TabIndex = 51;
            this.NextButton.Text = "Next";
            this.NextButton.UseVisualStyleBackColor = false;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // BackButton
            // 
            this.BackButton.BackColor = System.Drawing.Color.Transparent;
            this.BackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BackButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BackButton.FlatAppearance.BorderSize = 0;
            this.BackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.BackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackButton.ForeColor = System.Drawing.Color.Black;
            this.BackButton.Location = new System.Drawing.Point(511, 428);
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size(90, 29);
            this.BackButton.TabIndex = 52;
            this.BackButton.Text = "Back";
            this.BackButton.UseVisualStyleBackColor = false;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.Label1.Location = new System.Drawing.Point(299, 32);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(23, 15);
            this.Label1.TabIndex = 54;
            this.Label1.Text = "On";
            // 
            // RadioButton1
            // 
            this.RadioButton1.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RadioButton1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RadioButton1.FlatAppearance.BorderSize = 0;
            this.RadioButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RadioButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton1.ForeColor = System.Drawing.Color.Black;
            this.RadioButton1.Location = new System.Drawing.Point(274, 29);
            this.RadioButton1.Name = "RadioButton1";
            this.RadioButton1.Size = new System.Drawing.Size(22, 22);
            this.RadioButton1.TabIndex = 53;
            this.RadioButton1.UseVisualStyleBackColor = false;
            this.RadioButton1.Click += new System.EventHandler(this.RadioButton1_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.Label2.Location = new System.Drawing.Point(299, 60);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 15);
            this.Label2.TabIndex = 56;
            this.Label2.Text = "On";
            // 
            // RadioButton2
            // 
            this.RadioButton2.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RadioButton2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RadioButton2.FlatAppearance.BorderSize = 0;
            this.RadioButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RadioButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton2.ForeColor = System.Drawing.Color.Black;
            this.RadioButton2.Location = new System.Drawing.Point(274, 57);
            this.RadioButton2.Name = "RadioButton2";
            this.RadioButton2.Size = new System.Drawing.Size(22, 22);
            this.RadioButton2.TabIndex = 55;
            this.RadioButton2.UseVisualStyleBackColor = false;
            this.RadioButton2.Click += new System.EventHandler(this.RadioButton2_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.Label3.Location = new System.Drawing.Point(299, 88);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(23, 15);
            this.Label3.TabIndex = 58;
            this.Label3.Text = "On";
            // 
            // RadioButton3
            // 
            this.RadioButton3.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RadioButton3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RadioButton3.FlatAppearance.BorderSize = 0;
            this.RadioButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RadioButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton3.ForeColor = System.Drawing.Color.Black;
            this.RadioButton3.Location = new System.Drawing.Point(274, 85);
            this.RadioButton3.Name = "RadioButton3";
            this.RadioButton3.Size = new System.Drawing.Size(22, 22);
            this.RadioButton3.TabIndex = 57;
            this.RadioButton3.UseVisualStyleBackColor = false;
            this.RadioButton3.Click += new System.EventHandler(this.RadioButton3_Click);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.Label4.Location = new System.Drawing.Point(299, 116);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(23, 15);
            this.Label4.TabIndex = 60;
            this.Label4.Text = "On";
            // 
            // RadioButton4
            // 
            this.RadioButton4.BackColor = System.Drawing.Color.Transparent;
            this.RadioButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RadioButton4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RadioButton4.FlatAppearance.BorderSize = 0;
            this.RadioButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RadioButton4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RadioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadioButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton4.ForeColor = System.Drawing.Color.Black;
            this.RadioButton4.Location = new System.Drawing.Point(274, 113);
            this.RadioButton4.Name = "RadioButton4";
            this.RadioButton4.Size = new System.Drawing.Size(22, 22);
            this.RadioButton4.TabIndex = 59;
            this.RadioButton4.UseVisualStyleBackColor = false;
            this.RadioButton4.Click += new System.EventHandler(this.RadioButton4_Click);
            // 
            // AvatarNameTextBox
            // 
            this.AvatarNameTextBox.BackColor = System.Drawing.Color.Black;
            this.AvatarNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvatarNameTextBox.ForeColor = System.Drawing.SystemColors.Control;
            this.AvatarNameTextBox.Location = new System.Drawing.Point(302, 25);
            this.AvatarNameTextBox.MaxLength = 30;
            this.AvatarNameTextBox.Name = "AvatarNameTextBox";
            this.AvatarNameTextBox.Size = new System.Drawing.Size(378, 22);
            this.AvatarNameTextBox.TabIndex = 61;
            this.AvatarNameTextBox.Visible = false;
            this.AvatarNameTextBox.TextChanged += new System.EventHandler(this.AvatarNameTextBox_TextChanged);
            // 
            // AvatarCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.AvatarNameTextBox);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.RadioButton4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.RadioButton3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.RadioButton2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.RadioButton1);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.NextButton);
            this.Controls.Add(this.QuestionLabel);
            this.DoubleBuffered = true;
            this.Name = "AvatarCreator";
            this.Size = new System.Drawing.Size(700, 460);
            this.Load += new System.EventHandler(this.AvatarCreator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label QuestionLabel;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Button RadioButton1;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.Button RadioButton2;
        private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.Button RadioButton3;
        private System.Windows.Forms.Label Label4;
        private System.Windows.Forms.Button RadioButton4;
        private System.Windows.Forms.TextBox AvatarNameTextBox;
    }
}
