﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class EndOfGameDialog : UserControl
    {
        public EndOfGameDialog(bool Won, bool AvatarDefeated)
        {
            InitializeComponent();

            BackgroundImage = ResourcesLib.GetImageFromBorders("navframe_darker");

            if (Won)
            {
                if (AvatarDefeated)
                    Label.Text = "You have annihilated your opponent's avatar! Success! You have won the game!";
                else
                    Label.Text = "You have completed all required questions! Success! You have won the game!";
                ImageBox.Image = ResourcesLib.GetImageFromDialogs("victory");
            }
            else
            {
                if (AvatarDefeated)
                    Label.Text = "Your avatar has been defeated! You have been eliminated from the game!";
                else
                    Label.Text = "Your enemy completed all required quests! You have been eliminated from the game!";
                ImageBox.Image = ResourcesLib.GetImageFromDialogs("defeat");
            }

            ButtonAnimation.FormatButton(OKButton);
        }

        private void OKButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            Dispose();
        }
    }
}
