﻿namespace TradingCardGame.forms.dialogs
{
    partial class Tutorials
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.centeringPanel = new System.Windows.Forms.Panel();
            this.tutorialAboutTextLabel = new System.Windows.Forms.Label();
            this.tutorialTitlePictureBox = new System.Windows.Forms.PictureBox();
            this.centeringPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tutorialTitlePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // centeringPanel
            // 
            this.centeringPanel.BackColor = System.Drawing.Color.Transparent;
            this.centeringPanel.Controls.Add(this.tutorialAboutTextLabel);
            this.centeringPanel.Location = new System.Drawing.Point(405, 100);
            this.centeringPanel.Name = "centeringPanel";
            this.centeringPanel.Size = new System.Drawing.Size(518, 293);
            this.centeringPanel.TabIndex = 15;
            // 
            // tutorialAboutTextLabel
            // 
            this.tutorialAboutTextLabel.AutoSize = true;
            this.tutorialAboutTextLabel.BackColor = System.Drawing.Color.Transparent;
            this.tutorialAboutTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tutorialAboutTextLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(133)))), ((int)(((byte)(172)))));
            this.tutorialAboutTextLabel.Location = new System.Drawing.Point(51, 0);
            this.tutorialAboutTextLabel.Name = "tutorialAboutTextLabel";
            this.tutorialAboutTextLabel.Size = new System.Drawing.Size(0, 16);
            this.tutorialAboutTextLabel.TabIndex = 14;
            this.tutorialAboutTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tutorialTitlePictureBox
            // 
            this.tutorialTitlePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.tutorialTitlePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tutorialTitlePictureBox.Location = new System.Drawing.Point(405, 61);
            this.tutorialTitlePictureBox.Name = "tutorialTitlePictureBox";
            this.tutorialTitlePictureBox.Size = new System.Drawing.Size(518, 33);
            this.tutorialTitlePictureBox.TabIndex = 13;
            this.tutorialTitlePictureBox.TabStop = false;
            // 
            // Tutorials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.centeringPanel);
            this.Controls.Add(this.tutorialTitlePictureBox);
            this.DoubleBuffered = true;
            this.Name = "Tutorials";
            this.Size = new System.Drawing.Size(1002, 688);
            this.centeringPanel.ResumeLayout(false);
            this.centeringPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tutorialTitlePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label tutorialAboutTextLabel;
        private System.Windows.Forms.PictureBox tutorialTitlePictureBox;
        private System.Windows.Forms.Panel centeringPanel;
    }
}
