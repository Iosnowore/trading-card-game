﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.guild;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.forms.dialogs
{
    public partial class CreateGuild : UserControl
    {
        private Point p;

        public CreateGuild()
        {
            InitializeComponent();
            ButtonAnimation.FormatButton(createButton);
        }

        private void CreateGuild_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (!string.IsNullOrWhiteSpace(guildNameTextBox.Text))
            {
                TCG.SetDialogStatus(true);
                GuildService.createGuild(guildNameTextBox.Text);
                Parent.Parent.Parent.Parent.Controls.Add(new Window(new GuildInfo(PacketService.GetGuildInfoPacket()), guildNameTextBox.Text));
                Parent.Parent.Parent.Dispose();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            TCG.SetDialogStatus(false);
            SoundService.PlaySound("button");
            Dispose();
        }

        private void CreateGuild_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void CreateGuild_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }
    }
}
