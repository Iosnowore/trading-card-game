﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Trade;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Forms.Screens;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class TradeConfirm : UserControl
    {
        private readonly ConfirmTradePacket Packet;

        private readonly Trade TradeWindow;

        private readonly bool PostedTrade;
        private bool Confirmed;

        public TradeConfirm(ConfirmTradePacket Packet, Trade TradeWindow, bool PostedTrade)
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(CancelButton);
            ButtonAnimation.FormatButton(ConfirmButton);

            this.Packet = Packet;
            this.TradeWindow = TradeWindow;
            this.PostedTrade = PostedTrade;

            Card Card;

            foreach (int CardId in Packet.GetYouGetCards().Keys)
            {
                Card = new Card(TCGData.Cards.GetCardData(CardId), Card.SIZE.SMALL);
                Card.SetQuantityLabel(Packet.GetYouGetCards()[CardId]);
                YouGetCardPanel.Controls.Add(Card);
            }

            foreach (int CardId in Packet.GetTheyGetCards().Keys)
            {
                Card = new Card(TCGData.Cards.GetCardData(CardId), Card.SIZE.SMALL);
                Card.SetQuantityLabel(Packet.GetTheyGetCards()[CardId]);
                TheyGetCardPanel.Controls.Add(Card);
            }
        }

        private void ConfirmButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (PostedTrade)
            {
                PacketService.SendPacket(Commands.POSTEDTRADE_CREATE, new PostedTradePacket(-1, null, null, Packet.GetTheyGetCards(), Packet.GetYouGetCards()));
                TradeWindow.Parent.Dispose();
                Parent.Dispose();
            }
            else
            {
                Confirmed = !Confirmed;
                PacketService.SendPacket(Commands.TRADE_CONFIRM, null);
            }
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            TradeWindow.CancelConfirm();
            PacketService.SendPacket(Commands.TRADE_CONFIRM_CANCEL, null);
            SoundService.PlaySound("button");
            Parent.Dispose();
        }
    }
}
