﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class RedemptionConfirmation : UserControl
    {
        private readonly int CardId;
        private int SelectedPackId;

        private ItemGrantChoice ItemGrantChoice;

        private readonly CollectionManager.LoadType LoadType;

        public RedemptionConfirmation(int CardId, CollectionManager.LoadType LoadType)
        {
            InitializeComponent();

            this.CardId = CardId;
            this.LoadType = LoadType;

            // Get the CardData
            CardData CardData = null;
            PackData PackData = null;

            if (LoadType == CollectionManager.LoadType.RedeemableItems)
                CardData = TCGData.Cards.GetLootCardData(CardId);
            else if (LoadType == CollectionManager.LoadType.PromoPacks)
                PackData = TCGData.Cards.GetPromoPackData(CardId);

            // Create the Card object and add it to the control
            Card Card = null;

            if (CardData != null)
                Card = new Card(CardData, Card.SIZE.LARGE);
            else if (PackData != null)
                Card = new Card(PackData, Card.SIZE.LARGE);

            Card.Location = new Point(10, (Height - Card.Height) / 2);
            Controls.Add(Card);

            // Set the title and center it
            TitleLabel.Text = CardData != null ? CardData.GetTitle() : PackData.GetTitle();
            TitleLabel.Location = new Point((Width - TitleLabel.Width) / 2, TitleLabel.Location.Y);

            // Set the description
            if (CardData != null)
            {
                DescriptionLabel.Text = "Description:\n" + CardData.GetDescription();
                DescriptionLabel.Location = new Point(Card.Location.X + Card.Width + 10, Card.Location.Y + 20);
            }

            ChoiceAllowedLabel.Location = new Point(DescriptionLabel.Location.X, DescriptionLabel.Location.Y + DescriptionLabel.Height + 10);

            // Set the UI for the buttons
            ButtonAnimation.FormatButton(ChooseButton);
            ButtonAnimation.FormatButton(TermsButton);
            ButtonAnimation.FormatButton(RedeemButton);

            // Disable the redeem button until player chooses a valid option
            ButtonAnimation.DisableButton(RedeemButton);
        }

        private void ChooseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (ItemGrantChoice != null && !ItemGrantChoice.IsDisposed)
                return;

            ItemGrantChoice = new ItemGrantChoice(CardId, this, LoadType);
            Parent.Parent.Controls.Add(new Window(ItemGrantChoice, "Redemption Choice"));
        }

        private void RedeemButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (LoadType == CollectionManager.LoadType.RedeemableItems)
                PacketService.SendPacket(Commands.REDEEM_PACK, new CommandPacket(new object[] { SelectedPackId }));
            else if (LoadType == CollectionManager.LoadType.PromoPacks)
                PacketService.SendPacket(Commands.REDEEM_PROMOPACK, new CommandPacket(new object[] { SelectedPackId }));

            Parent.Dispose();
        }

        public void SetSelectedId(int SelectedPackId)
        {
            this.SelectedPackId = SelectedPackId;

            ButtonAnimation.EnableButton(RedeemButton);
        }
    }
}
