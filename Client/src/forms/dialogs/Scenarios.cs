﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TCGData.Packets.Scenario;
using TCGData.Services;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Deck;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class Scenarios : UserControl
    {
        private readonly ScenariosPacket CompletedScenarios;

        private bool Loading;

        private readonly Panel EasyPanel;
        private readonly Panel MediumPanel;
        private readonly Panel HardPanel;

        private DeckPacket ChosenDeck;

        public Scenarios()
        {
            Loading = true;
            InitializeComponent();

            BackColor = Color.Black;
            ScenariosTreeView.ImageList = ResourcesLib.ICONS_IMAGES;

            // Get the list of completed scenarios from the server
            CompletedScenarios = PacketService.GetScenariosPacket();

            // Format button appearances
            ButtonAnimation.FormatButton(SelectDeckButton);
            ButtonAnimation.FormatButton(BeginButton);
            ButtonAnimation.DisableButton(BeginButton);

            // Format RadioButton appearances
            ButtonAnimation.FormatRadioButton(EasyRadioButton);
            ButtonAnimation.FormatRadioButton(MediumRadioButton);
            ButtonAnimation.FormatRadioButton(HardRadioButton);

            // Toggle easy radio button
            ButtonAnimation.ToggleRadioButton(EasyRadioButton, true);

            // Set the background images for the difficulty panels
            EasyPanel = new Panel();
            EasyPanel.BackgroundImage = ResourcesLib.GetImageFromIcons("scenario_complete_easy");
            EasyPanel.BackColor = Color.Transparent;
            EasyPanel.Visible = false;
            DifficultyPanel.Controls.Add(EasyPanel);

            MediumPanel = new Panel();
            MediumPanel.BackgroundImage = ResourcesLib.GetImageFromIcons("scenario_complete_medium");
            MediumPanel.BackColor = Color.Transparent;
            MediumPanel.Visible = false;
            DifficultyPanel.Controls.Add(MediumPanel);

            HardPanel = new Panel();
            HardPanel.BackgroundImage = ResourcesLib.GetImageFromIcons("scenario_complete_hard");
            HardPanel.BackColor = Color.Transparent;
            HardPanel.Visible = false;
            DifficultyPanel.Controls.Add(HardPanel);

            // Set DeckValidatorPictureBox images
            ValidateButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_validate");
            ValidateButton.Image = ResourcesLib.GetImageFromIcons("no");

            // Load scenarios
            string[] CampaignCol = DatatableReader.GetValues("scenarios", "campaign");
            string[] SideCol = DatatableReader.GetValues("scenarios", "side");
            string[] TextCol = DatatableReader.GetValues("scenarios", "text");

            TreeNode LastCampaignNode = null;
            TreeNode LastSideCampaignNode = null;

            int ScenarioNumber = 1;
            int ScenarioId = 0;

            string[] ScenarioDetails;

            for (int i = 0; i < CampaignCol.Length; i++)
            {
                if (!string.IsNullOrEmpty(CampaignCol[i]))
                {
                    TreeNode CurrentNode = new TreeNode();
                    CurrentNode.Text = CampaignCol[i] + " Campaign";

                    if (i % 2 == 1)
                        CurrentNode.BackColor = Color.DimGray;

                    CurrentNode.SelectedImageKey = CurrentNode.ImageKey;
                    ScenariosTreeView.Nodes.Add(CurrentNode);

                    LastCampaignNode = CurrentNode;
                }
                else if (!string.IsNullOrEmpty(SideCol[i]))
                {
                    TreeNode CurrentNode = new TreeNode();
                    CurrentNode.Text = SideCol[i] + " Side Campaign: " + TextCol[i];
                    //CurrentNode.ImageKey = "yes";

                    if (i % 2 == 1)
                        CurrentNode.BackColor = Color.DarkGray;

                    CurrentNode.SelectedImageKey = CurrentNode.ImageKey;
                    LastCampaignNode.Nodes.Add(CurrentNode);
                    LastSideCampaignNode = CurrentNode;
                }
                else if (!string.IsNullOrEmpty(TextCol[i]))
                {
                    TreeNode CurrentNode = new TreeNode();
                    CurrentNode.Text = "Scenario " + ScenarioNumber + ": " + StringLib.GetString("scenarios:title_" + ScenarioId);
                    CurrentNode.Tag = ScenarioId++;

                    ScenarioDetails = GetCompletedScenarioDetails(i);

                    if (ScenarioDetails == null)
                    {
                        CurrentNode.ImageIndex = -1;

                        if (ScenarioNumber == 1)
                            CurrentNode.ImageIndex = -1;
                        else
                            CurrentNode.ImageKey = "lock";
                    }
                    else
                        CurrentNode.ImageKey = "yes";

                    if (i % 2 == 1)
                        CurrentNode.BackColor = Color.DarkGray;

                    if (++ScenarioNumber == 6)
                        ScenarioNumber = 1;

                    CurrentNode.SelectedImageKey = CurrentNode.ImageKey;
                    LastSideCampaignNode.Nodes.Add(CurrentNode);
                }
            }

            ScenariosTreeView.ExpandAll();
            ScenariosTreeView.SelectedNode = ScenariosTreeView.Nodes[0];
        }

        private string[] GetCompletedScenarioDetails(int ScenarioIndex)
        {
            // Element strings will be as such format: CAMPAIGN:SIDE:DIFFICULTY
            string[] Scenarios = CompletedScenarios.GetScenarios();

            string[] ScenarioDetails;

            foreach (string Scenario in Scenarios)
            {
                ScenarioDetails = Scenario.Split(':');

                if (int.Parse(ScenarioDetails[0]) == ScenarioIndex)
                    return ScenarioDetails;
            }
            return null;
        }

        private void ScenariosTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ScenarioTitleLabel.Text = e.Node.Text;

            string ScenarioTitle = ScenarioTitleLabel.Text;

            if (ScenariosTreeView.SelectedNode.Tag != null)
            {
                ScenarioDescriptionLabel.Text = StringLib.GetString("scenarios:description_" + ScenariosTreeView.SelectedNode.Tag);
            }
            else
            {
                if (ScenarioTitle.Contains(":"))
                    ScenarioTitle = ScenarioTitle.Split(':')[1].Substring(1);
                ScenarioTitle = ScenarioTitle.ToLower().Replace(" ", "_");

                ScenarioDescriptionLabel.Text = StringLib.GetString("scenarios:" + ScenarioTitle);
            }

            ScenarioDescriptionLabel.Size = new Size(Width - ScenarioDescriptionLabel.Location.X, 300);

            // If we have a previous chosen deck, make sure it is valid for new selection
            if (!string.IsNullOrEmpty(DeckNameTextBox.Text))
            {
                string[] RequiredArchetypes = GetRequiredArchetypes();
                bool DeckIsGood = DeckService.DeckIsGoodForScenario(ChosenDeck.GetCardIds(), RequiredArchetypes);

                if (!DeckIsGood)
                {
                    DeckNameTextBox.Text = string.Empty;
                    ValidateButton.Image = ResourcesLib.GetImageFromIcons("no");
                }     
            }

            if (ScenariosTreeView.SelectedNode.Tag == null || string.IsNullOrEmpty(DeckNameTextBox.Text))
                ButtonAnimation.DisableButton(BeginButton);
            else
                ButtonAnimation.EnableButton(BeginButton);

            if (!Loading)
                SoundService.PlaySound("button");

            if (e.Node.Tag != null)
                LoadDifficultyCompletion("imperial");
            else
                DifficultyPanel.Visible = false;

            Loading = false;
        }

        private void LoadDifficultyCompletion(string Faction)
        {
            DifficultyPanel.BackgroundImage = ResourcesLib.GetImageFromIcons("faction_icon_" + Faction);
            DifficultyPanel.Visible = true;

            // TODO: Check difficulty.
            EasyPanel.Visible = true;
        }

        private void SelectDeckButton_Click(object sender, System.EventArgs e)
        {
            string[] RequiredArchetypes = GetRequiredArchetypes();

            Parent.Parent.Controls.Add(new Window(new OpenDeck(this, RequiredArchetypes), "Select Deck"));

            SoundService.PlaySound("button");
        }

        private string[] GetRequiredArchetypes()
        {
            if (ScenariosTreeView.SelectedNode.Parent != null)
            {
                if (ScenariosTreeView.SelectedNode.Parent.Text.StartsWith("Dark Side"))
                    return new string[] { "imperial", "sith" };
                if (ScenariosTreeView.SelectedNode.Parent.Text.StartsWith("Light Side"))
                    return new string[] { "rebel", "jedi" };
            }
            return null;
        }

        public void ChooseDeck(DeckPacket ChosenDeck)
        {
            // Update the DeckAvatarId
            this.ChosenDeck = ChosenDeck;

            // Let server know of our deck selection choice
            PacketService.SendPacket(Commands.DECK_OPEN, new CommandPacket(new object[] { ChosenDeck.GetDeckName() }));

            // Update UI
            DeckNameTextBox.Text = ChosenDeck.GetDeckName();
            ValidateButton.Image = ResourcesLib.GetImageFromIcons("yes");

            if (ScenariosTreeView.SelectedNode.Tag != null)
                ButtonAnimation.EnableButton(BeginButton);
        }

        private void BeginButton_Click(object sender, System.EventArgs e)
        {
            CommandPacket CommandPacket = new CommandPacket(new object[] { ScenariosTreeView.SelectedNode.Tag, DeckNameTextBox.Text });
            PacketService.SendPacket(Commands.START_SCENARIO, CommandPacket);
        }
    }
}
