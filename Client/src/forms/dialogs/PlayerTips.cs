﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.forms.dialogs
{
    public partial class PlayerTips : UserControl
    {
        public enum Types
        {
            MainScreen,
            Collection,
            DeckBuilder,
            ExcessiveAttack,
            MuliplayerGames,
            Trade,
            TradeLobby,
            CasualLobby,
            QuestFilled
        }

        private bool hideTipsChecked;
        private readonly Types Type;

        public PlayerTips(Types Type)
        {
            InitializeComponent();
            ButtonAnimation.FormatButton(closeButton);
            pictureBox1.BackgroundImage = ResourcesLib.GetImageFromDialogs("avatar");
            newbieTipCheckBox.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");

            helpTextLabel.Text = string.Empty;
            this.Type = Type;

            if (Type == Types.MainScreen)
                helpTextLabel.Text = PlayerService.GetUsername();

            helpTextLabel.Text += StringLib.GetString("help:" + Type);
        }

        private void newbieTipCheckBox_Click(object sender, EventArgs e)
        {
            newbieTipCheckBox.Image = hideTipsChecked ? ResourcesLib.GetImageFromButtons("checkbox_off") : ResourcesLib.GetImageFromButtons("checkbox_on");
            hideTipsChecked = !hideTipsChecked;
            SettingsService.SetValueInSettingsXml("HidePlayerTips", Type.ToString(), hideTipsChecked);
            SoundService.PlaySound("button");
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Dispose();
        }
    }
}
