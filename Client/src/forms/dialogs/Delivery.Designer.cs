﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class Delivery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransactionListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CloseButton = new System.Windows.Forms.Button();
            this.TransactionIdLabel = new System.Windows.Forms.Label();
            this.NumberOfItemsLabel = new System.Windows.Forms.Label();
            this.CardsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // TransactionListView
            // 
            this.TransactionListView.BackColor = System.Drawing.Color.Black;
            this.TransactionListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TransactionListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.TransactionListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransactionListView.ForeColor = System.Drawing.Color.White;
            this.TransactionListView.FullRowSelect = true;
            this.TransactionListView.Location = new System.Drawing.Point(9, 9);
            this.TransactionListView.MultiSelect = false;
            this.TransactionListView.Name = "TransactionListView";
            this.TransactionListView.Size = new System.Drawing.Size(200, 482);
            this.TransactionListView.TabIndex = 3;
            this.TransactionListView.UseCompatibleStateImageBehavior = false;
            this.TransactionListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Transaction";
            this.columnHeader1.Width = 196;
            // 
            // CloseButton
            // 
            this.CloseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CloseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Location = new System.Drawing.Point(655, 468);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(90, 29);
            this.CloseButton.TabIndex = 6;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // TransactionIdLabel
            // 
            this.TransactionIdLabel.AutoSize = true;
            this.TransactionIdLabel.BackColor = System.Drawing.Color.Black;
            this.TransactionIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransactionIdLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TransactionIdLabel.Location = new System.Drawing.Point(212, 9);
            this.TransactionIdLabel.Name = "TransactionIdLabel";
            this.TransactionIdLabel.Size = new System.Drawing.Size(162, 15);
            this.TransactionIdLabel.TabIndex = 7;
            this.TransactionIdLabel.Text = "Transaction ID: #123456789";
            // 
            // NumberOfItemsLabel
            // 
            this.NumberOfItemsLabel.AutoSize = true;
            this.NumberOfItemsLabel.BackColor = System.Drawing.Color.Black;
            this.NumberOfItemsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumberOfItemsLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.NumberOfItemsLabel.Location = new System.Drawing.Point(212, 27);
            this.NumberOfItemsLabel.Name = "NumberOfItemsLabel";
            this.NumberOfItemsLabel.Size = new System.Drawing.Size(118, 15);
            this.NumberOfItemsLabel.TabIndex = 8;
            this.NumberOfItemsLabel.Text = "Number of items: 15";
            // 
            // CardsFlowLayoutPanel
            // 
            this.CardsFlowLayoutPanel.AutoScroll = true;
            this.CardsFlowLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CardsFlowLayoutPanel.Location = new System.Drawing.Point(215, 110);
            this.CardsFlowLayoutPanel.Name = "CardsFlowLayoutPanel";
            this.CardsFlowLayoutPanel.Size = new System.Drawing.Size(524, 343);
            this.CardsFlowLayoutPanel.TabIndex = 10;
            // 
            // Delivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.CardsFlowLayoutPanel);
            this.Controls.Add(this.NumberOfItemsLabel);
            this.Controls.Add(this.TransactionIdLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.TransactionListView);
            this.DoubleBuffered = true;
            this.Name = "Delivery";
            this.Size = new System.Drawing.Size(750, 500);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListView TransactionListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label TransactionIdLabel;
        private System.Windows.Forms.Label NumberOfItemsLabel;
        private System.Windows.Forms.FlowLayoutPanel CardsFlowLayoutPanel;
    }
}
