﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Chat;

namespace TradingCardGame.forms
{
    public partial class LaunchMatch : UserControl
    {
        private readonly MatchPacket MatchPacket;

        private readonly string MyUsername;

        private bool Player1Ready;
        private bool Player2Ready;

        public LaunchMatch(MatchPacket MatchPacket)
        {
            InitializeComponent();

            Console.WriteLine("Loading LaunchMatch dialog");

            this.MatchPacket = MatchPacket;

            PacketService.SetMatchLobby(this);

            // Set images
            avatarPictureBox1.Image = ResourcesLib.GetImageFromDraft("draft_avatar_seat");
            avatarPictureBox2.Image = ResourcesLib.GetImageFromDraft("draft_avatar_seat");
            allowObserversCheckBox.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");
            friendsOnlyCheckBox.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");
            lightVsDarkCheckBox.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");
            readyToGoCheckBox1.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");
            readyToGoCheckBox2.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_off");

            // Unload MatchPacket
            matchTypeComboBox.Text = MatchPacket.GetType();
            numberOfPlayersListBox.Text = MatchPacket.GetMaxPlayers().ToString() + " Players";
            playFormatComboBox.Text = MatchPacket.GetPlayFormat();
            matchStructureComboBox.Text = MatchPacket.GetStructure();
            timeLimitComboBox.Text = MatchPacket.GetTimeLimit();
            guildOnlyComboBox.Text = MatchPacket.GetGuildOption();
            allowObserversCheckBox.BackgroundImage = MatchPacket.GetAllowObservers() ? ResourcesLib.GetImageFromButtons("checkbox_on_disabled") : ResourcesLib.GetImageFromButtons("checkbox_off_disabled");
            friendsOnlyCheckBox.BackgroundImage = MatchPacket.GetFriendsOnly() ? ResourcesLib.GetImageFromButtons("checkbox_on_disabled") : ResourcesLib.GetImageFromButtons("checkbox_off_disabled");
            lightVsDarkCheckBox.BackgroundImage = MatchPacket.GetLightVsDark() ? ResourcesLib.GetImageFromButtons("checkbox_on_disabled") : ResourcesLib.GetImageFromButtons("checkbox_off_disabled");


            string[] Usernames = new string[MatchPacket.GetPlayers().Length];
            int[] Avatars = new int[Usernames.Length];

            for (int i = 0; i < Usernames.Length; i++)
            {
                string[] UserDetails = MatchPacket.GetPlayers()[i].Split(':');
                Usernames[i] = UserDetails[0];
                Avatars[i] = int.Parse(UserDetails[1]);
            }

            // Add user joined the match in the chat
            MyUsername = Usernames[Usernames.Length - 1];
            chatBox.Items.Add(MyUsername + " joined the match.");

            // Load username listbox
            UpdateMatchUsernames(Usernames);

            // Load user widgets
            ReloadUsers(Usernames, Avatars);
        }

        private delegate void _ReloadUsers(string[] Usernames, int[] Avatars);
        public void ReloadUsers(string[] Usernames, int[] Avatars)
        {
            if (GetUserPanelToUpdate(0).InvokeRequired)
                GetUserPanelToUpdate(0).Invoke(new _ReloadUsers(ReloadUsers), Usernames, Avatars);
            else
                GetUserPanelToUpdate(0).Visible = false;

            if (GetUserPanelToUpdate(1).InvokeRequired)
                GetUserPanelToUpdate(1).Invoke(new _ReloadUsers(ReloadUsers), Usernames, Avatars);
            else
                GetUserPanelToUpdate(1).Visible = false;

            for (int i = 0; i < Usernames.Length; i++)
            {
                if (GetPictureBoxToUpdate(i).InvokeRequired)
                    GetPictureBoxToUpdate(i).Invoke(new _ReloadUsers(ReloadUsers), Usernames, Avatars);
                else
                    GetPictureBoxToUpdate(i).BackgroundImage = ResourcesLib.GetImageFromPictures("avatar_" + (Avatars[i] < 10 ? "0" + Avatars[i] : Avatars[i].ToString()));

                if (GetLabelToUpdate(i).InvokeRequired)
                    GetLabelToUpdate(i).Invoke(new _ReloadUsers(ReloadUsers), Usernames, Avatars);
                else
                    GetLabelToUpdate(i).Text = Usernames[i];

                if (GetUserPanelToUpdate(i).InvokeRequired)
                    GetUserPanelToUpdate(i).Invoke(new _ReloadUsers(ReloadUsers), Usernames, Avatars);
                else
                    GetUserPanelToUpdate(i).Visible = true;
            }
        }

        private PictureBox GetPictureBoxToUpdate(int Index)
        {
            switch (Index)
            {
                case 0:
                    return avatarPictureBox1;
                case 1:
                    return avatarPictureBox2;
            }
            return null;
        }

        private Label GetLabelToUpdate(int Index)
        {
            switch (Index)
            {
                case 0:
                    return usernameLabel1;
                case 1:
                    return usernameLabel2;
            }
            return null;
        }

        private Panel GetUserPanelToUpdate(int Index)
        {
            switch (Index)
            {
                case 0:
                    return userPanel1;
                case 1:
                    return userPanel2;
            }
            return null;
        }

        public void LeaveMatch()
        {
            CommandPacket CommandPacket = new CommandPacket(new object[] { MatchPacket.GetTitle() });
            PacketService.SendPacket(Commands.MATCH_LEAVE, CommandPacket);
        }

        private void InputChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChatService.ChatBoxKeyEnter(InputChatBox, chatBox, e);
            SoundService.KeyPressSound(e.KeyCode);
        }

        public ListBox GetChatBox()
        {
            return chatBox;
        }

        private delegate void MatchDelegate(string[] Usernames);

        public void UpdateMatchUsernames(string[] Usernames)
        {
            LaunchMatch match = PacketService.GetMatchLobby();

            // Update playersNameListBox
            if (playersNameListBox.InvokeRequired)
                playersNameListBox.Invoke(new MatchDelegate(UpdateMatchUsernames), Usernames);
            else
            {
                playersNameListBox.Items.Clear();

                foreach (string Username in Usernames)
                    playersNameListBox.Items.Add(Username, "bronze");
            }
        }

        private void ReadyToGoCheckBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (MyUsername.Equals(usernameLabel1.Text))
            {
                // Tell the server you're ready
                PacketService.SendPacket(Commands.MATCH_READY, null);
            }
        }

        private void ReadyToGoCheckBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (MyUsername.Equals(usernameLabel2.Text))
            {
                // Tell the server you're ready
                PacketService.SendPacket(Commands.MATCH_READY, null);
            }
        }

        public void ToggleReady(string Username)
        {
            if (Username.Equals(usernameLabel1.Text))
            {
                Player1Ready = !Player1Ready;

                readyToGoCheckBox2.BackgroundImage = Player1Ready ? ResourcesLib.GetImageFromButtons("checkbox_on") : ResourcesLib.GetImageFromButtons("checkbox_off");
                userPanel2.BackColor = Player1Ready ? Color.BlanchedAlmond : Color.SkyBlue;
            }
            else if (Username.Equals(usernameLabel2.Text))
            {
                Player2Ready = !Player2Ready;

                readyToGoCheckBox2.BackgroundImage = Player2Ready ? ResourcesLib.GetImageFromButtons("checkbox_on") : ResourcesLib.GetImageFromButtons("checkbox_off");
                userPanel2.BackColor = Player2Ready ? Color.BlanchedAlmond : Color.SkyBlue;
            }
        }
    }
}
