﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.forms.dialogs
{
    public partial class CreateMatch : UserControl
    {
        public CreateMatch()
        {
            InitializeComponent();
            ValidateButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_validate");
            ValidateButton.Image = ResourcesLib.GetImageFromIcons("no");

            // Format checkboxes
            ButtonAnimation.FormatCheckBox(AllowObserversCheckBox);
            ButtonAnimation.FormatCheckBox(FriendsOnlyCheckBox);
            ButtonAnimation.FormatCheckBox(LightVsDarkCheckBox);

            // format buttons
            ButtonAnimation.FormatButton(SelectDeckButton);
            ButtonAnimation.FormatButton(CreateMatchButton);
            ButtonAnimation.FormatButton(ResetMatchCreationButton);
            ButtonAnimation.FormatButton(CancelMatchCreationButton);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void CreateMatch_Load(object sender, EventArgs e)
        {
            SetDefaultCreateMatchValues();
        }

        private void SelectDeckButton_Click(object sender, EventArgs e)
        {
            Parent.Parent.Controls.Add(new Window(new OpenDeck(this), "Select Deck"));

            SoundService.PlaySound("button");
        }

        public void ChooseDeck(string DeckName)
        {
            deckSelectionTextBox.Text = DeckName;
            ValidateButton.Image = ResourcesLib.GetImageFromIcons("yes");

            // Let server know of our deck selection choice
            PacketService.SendPacket(Commands.DECK_OPEN, new CommandPacket(new object[] { DeckName }));
        }

        private void CreateMatchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            TCG TCG = (TCG)Parent.Parent.Parent;

            if (string.IsNullOrWhiteSpace(matchTitleTextBox.Text) || matchTypeComboBox.SelectedIndex == -1 ||
                numberOfPlayersListBox.SelectedIndex == -1 || playFormatComboBox.SelectedIndex == -1 ||
                matchStructureComboBox.SelectedIndex == -1 || timeLimitComboBox.SelectedIndex == -1 ||
                guildOnlyComboBox.SelectedIndex == -1)
            {
                TCG.AddSystemMessage("Please fill out all match information.", "Missing Information");
                return;
            }

            if (string.IsNullOrEmpty(deckSelectionTextBox.Text))
            {
                TCG.AddSystemMessage("Please select a deck to use.", "Missing Deck");
                return;
            }

            PacketService.SendPacket(Commands.MATCH_CREATE, new MatchPacket(-1, AllowObserversCheckBox.Checked, FriendsOnlyCheckBox.Checked, LightVsDarkCheckBox.Checked, int.Parse(numberOfPlayersListBox.Text.Substring(0, 1)), guildOnlyComboBox.Text, passwordTextBox.Text, playFormatComboBox.Text, matchStructureComboBox.Text, timeLimitComboBox.Text, matchTitleTextBox.Text, matchTypeComboBox.Text, null));
            Parent.Dispose();
        }

        private void SetDefaultCreateMatchValues()
        {
            matchTitleTextBox.Text = PlayerService.GetUsername() + "'s Match";
            matchTypeComboBox.SelectedIndex = 0;
            numberOfPlayersListBox.SelectedIndex = 0;
            playFormatComboBox.SelectedIndex = 0;
            matchStructureComboBox.SelectedIndex = 0;
            timeLimitComboBox.SelectedIndex = 0;
            guildOnlyComboBox.SelectedIndex = 0;

            deckSelectionTextBox.Text = string.Empty;
            ValidateButton.Image = ResourcesLib.GetImageFromIcons("no");
        }

        private void ResetMatchCreationButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            SetDefaultCreateMatchValues();
        }

        private void CancelMatchCreationButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Dispose();
        }
    }
}
