﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.forms.dialogs
{
    public partial class SystemMessage : UserControl
    {
        private Point p;
        private static bool active;

        private delegate void DealWithSystemMessageDelegate();

        public SystemMessage(string message, string title)
        {
            InitializeComponent();
            titleLabel.Text = title;
            bodyLabel.Text = message;

            ButtonAnimation.FormatButton(closeButton);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public new void Show()
        {
            if (active && !titleLabel.Text.Equals("SERVER BROADCAST MESSAGE"))
                return;

            TCG TCG = TCGCore.GetTCG();

            if (TCG.GetChildWindow().Parent.InvokeRequired)
                TCG.GetChildWindow().Parent.Invoke(new DealWithSystemMessageDelegate(Show));
            else
            {
                active = true;
                TCG.GetChildWindow().Parent.Controls.Add(this);
                Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
                BringToFront();
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            active = false;
            SoundService.PlaySound("button");
            TCGCore.GetTCG().RemoveSystemMessage(this);
            Dispose();
        }

        private void SystemMessage_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void SystemMessage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }

        public string GetTitle()
        {
            return titleLabel.Text;
        }
    }
}
