﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData.Packets.Player;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.forms.dialogs
{
    public partial class UserInfo : UserControl
    {
        private Point p;

        public UserInfo(UserInfoPacket UserInfoPacket)
        {
            InitializeComponent();
            avatarPictureBox.Image = ResourcesLib.GetImageFromFrames("bronze");
            ButtonAnimation.FormatButton(CloseButton);
            loadProfile(UserInfoPacket);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void loadProfile(UserInfoPacket UserInfoPacket)
        {
            GoldMedallionPictureBox.Image = ResourcesLib.GetImageFromMedallions("bm_1");
            silverMedallionPictureBox.Image = ResourcesLib.GetImageFromMedallions("sm_1");
            BronzeMedallionPictureBox.Image = ResourcesLib.GetImageFromMedallions("gm_1");

            string[] Games = UserInfoPacket.GetGames().Split('-');
            winsLabel.Text = "Wins: " + Games[0];
            lossesLabel.Text = "Losses: " + Games[1];

            avatarPictureBox.BackgroundImage = ResourcesLib.GetImageFromPictures("avatar_" + (UserInfoPacket.GetAvatar() < 10 ? "0" : string.Empty) + UserInfoPacket.GetAvatar());
            usernameLabel.Text = UserInfoPacket.GetUsername();
            usernameLabel.Left = (userInformationPanel.Width - usernameLabel.Width) / 2;

            guildLabel.Text = string.IsNullOrEmpty(UserInfoPacket.GetGuild()) ? string.Empty : "<" + UserInfoPacket.GetGuild() + ">";
            guildLabel.Left = (userInformationPanel.Width - guildLabel.Width) / 2;

            //string regdate = DatabaseService.GetValue("mybb_users", "regdate", "username", username);
            //joinDateLabel.Text = "Joined: " + (string.IsNullOrEmpty(regdate) ? "Unknown" : unixToDateTime(Convert.ToInt64(regdate)).Date.ToString("MMMM yyyy"));
            joinDateLabel.Text = "Joined: Unknown";

            //string birthDate = DatabaseService.GetValue("mybb_users", "birthday", "username", username).Replace("-", "/");
            //DateTime dt = DateTime.ParseExact(birthDate, "d/M/yyyy", CultureInfo.InvariantCulture);
            //DateOfBirthLabel.Text = "DOB: " + dt.ToString("MM/dd/yyyy") + " (" + Math.Round(((DateTime.Now - dt).TotalDays / 365), 0).ToString() + " years old)";
            DateOfBirthLabel.Text = "Age: Unknown";

            playerBiographyTextBox.Text = UserInfoPacket.GetBiography();
        }

        public DateTime unixToDateTime(long unixTime)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTime).ToLocalTime();
        }

        private void playerBiographyTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            TCG.SetDialogStatus(false);
            Parent.Dispose();
        }

        private void UserInfo_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void UserInfo_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }
    }
}
