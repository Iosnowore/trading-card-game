﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class Validation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Standard");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Basic Formats", new System.Windows.Forms.TreeNode[] {
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Standard Draft");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Draft Formats", new System.Windows.Forms.TreeNode[] {
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Highlander");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("No Rares");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Special Formats", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Validation));
            this.TreeView = new System.Windows.Forms.TreeView();
            this.BottomListView = new System.Windows.Forms.ListView();
            this.MiddleListView = new System.Windows.Forms.ListView();
            this.TopListView = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.MainHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TreeView
            // 
            this.TreeView.BackColor = System.Drawing.Color.Black;
            this.TreeView.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TreeView.Location = new System.Drawing.Point(3, 3);
            this.TreeView.Name = "TreeView";
            treeNode15.Name = "Standard";
            treeNode15.Text = "Standard";
            treeNode16.Name = "BasicFormats";
            treeNode16.Text = "Basic Formats";
            treeNode17.Name = "StandardDraft";
            treeNode17.Text = "Standard Draft";
            treeNode18.Name = "DraftFormats";
            treeNode18.Text = "Draft Formats";
            treeNode19.Name = "Highlander";
            treeNode19.Text = "Highlander";
            treeNode20.Name = "NoRares";
            treeNode20.Text = "No Rares";
            treeNode21.Name = "SpecialFormats";
            treeNode21.Text = "Special Formats";
            this.TreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode16,
            treeNode18,
            treeNode21});
            this.TreeView.Size = new System.Drawing.Size(300, 512);
            this.TreeView.TabIndex = 0;
            this.TreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView_AfterSelect);
            // 
            // BottomListView
            // 
            this.BottomListView.BackColor = System.Drawing.Color.Black;
            this.BottomListView.HideSelection = false;
            this.BottomListView.Location = new System.Drawing.Point(309, 390);
            this.BottomListView.Name = "BottomListView";
            this.BottomListView.Size = new System.Drawing.Size(488, 125);
            this.BottomListView.TabIndex = 1;
            this.BottomListView.UseCompatibleStateImageBehavior = false;
            // 
            // MiddleListView
            // 
            this.MiddleListView.BackColor = System.Drawing.Color.Black;
            this.MiddleListView.HideSelection = false;
            this.MiddleListView.Location = new System.Drawing.Point(309, 259);
            this.MiddleListView.Name = "MiddleListView";
            this.MiddleListView.Size = new System.Drawing.Size(488, 125);
            this.MiddleListView.TabIndex = 2;
            this.MiddleListView.UseCompatibleStateImageBehavior = false;
            // 
            // TopListView
            // 
            this.TopListView.BackColor = System.Drawing.Color.Black;
            this.TopListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.MainHeader});
            this.TopListView.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TopListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.TopListView.HideSelection = false;
            this.TopListView.Location = new System.Drawing.Point(309, 128);
            this.TopListView.Name = "TopListView";
            this.TopListView.Size = new System.Drawing.Size(488, 125);
            this.TopListView.TabIndex = 3;
            this.TopListView.UseCompatibleStateImageBehavior = false;
            this.TopListView.View = System.Windows.Forms.View.Details;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.TitleLabel);
            this.panel1.Location = new System.Drawing.Point(309, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(488, 50);
            this.panel1.TabIndex = 4;
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(9, 12);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(85, 24);
            this.TitleLabel.TabIndex = 0;
            this.TitleLabel.Text = "Standard";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DescriptionLabel.Location = new System.Drawing.Point(306, 56);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(475, 60);
            this.DescriptionLabel.TabIndex = 5;
            this.DescriptionLabel.Text = resources.GetString("DescriptionLabel.Text");
            // 
            // MainHeader
            // 
            this.MainHeader.Text = "Header";
            this.MainHeader.Width = 484;
            // 
            // Validation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TopListView);
            this.Controls.Add(this.MiddleListView);
            this.Controls.Add(this.BottomListView);
            this.Controls.Add(this.TreeView);
            this.DoubleBuffered = true;
            this.Name = "Validation";
            this.Size = new System.Drawing.Size(800, 530);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView TreeView;
        private System.Windows.Forms.ListView BottomListView;
        private System.Windows.Forms.ListView MiddleListView;
        private System.Windows.Forms.ListView TopListView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.ColumnHeader MainHeader;
    }
}
