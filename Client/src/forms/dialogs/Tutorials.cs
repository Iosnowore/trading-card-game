﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;

namespace TradingCardGame.forms.dialogs
{
    public partial class Tutorials : UserControl
    {
        private PictureBox lastClicked;

        private static readonly Point[] Points = {
            new Point(120, 50),
            new Point(120, 140),
            new Point(170, 230),
            new Point(233, 310),
            new Point(307, 390),
            new Point(390, 450),
            new Point(470, 510),
            new Point(560, 550),
            new Point(640, 570),
            new Point(730, 580),
            new Point(810, 550)
        };

        public Tutorials()
        {
            InitializeComponent();
            BackgroundImage = ResourcesLib.GetImageFromTutorialNav("tutorial_background");

            for (int i = 0; i < Points.Length; i++)
            {
                PictureBox TutorialButton = new PictureBox
                {
                    Location = Points[i],
                    Image = ResourcesLib.GetImageFromTutorialNav("tutorial_" + (i + 1)),
                    Size = new Size(70, 70)
                };
                TutorialButton.MouseEnter += TutorialButton_MouseEnter;
                TutorialButton.MouseClick += TutorialButton_MouseClick;
                Controls.Add(TutorialButton);
            }
        }

        private void TutorialButton_MouseClick(object sender, MouseEventArgs e)
        {
            PictureBox SelectedPictureBox = (PictureBox)sender;
            int TutorialIndex = GetPictureBoxIndex(SelectedPictureBox);
            PacketService.SendPacket(Commands.LOAD_GAME, new CommandPacket(new string[] { "tutorial:" + TutorialIndex }));
        }

        private void TutorialButton_MouseEnter(object sender, System.EventArgs e)
        {

            PictureBox SelectedPictureBox = (PictureBox)sender;
            int PictureIndex;

            if (lastClicked != null)
            {
                PictureIndex = GetPictureBoxIndex(lastClicked);
                lastClicked.Image = ResourcesLib.GetImageFromTutorialNav("tutorial_" + PictureIndex);
            }
            PictureIndex = GetPictureBoxIndex(SelectedPictureBox);
            tutorialTitlePictureBox.Image = ResourcesLib.GetImageFromTutorialNav("_" + PictureIndex + GetImageTitle(PictureIndex) + "_text");
            SelectedPictureBox.Image = ResourcesLib.GetImageFromTutorialNav("tutorial_" + PictureIndex + "_hover");
            tutorialAboutTextLabel.Text = StringLib.GetString("tutorial:description" + PictureIndex);
            tutorialAboutTextLabel.Location = new Point((centeringPanel.Width - tutorialAboutTextLabel.Width) / 2, 0);


            lastClicked = SelectedPictureBox;
        }

        private static int GetPictureBoxIndex(PictureBox PictureBox)
        {
            for (int i = 0; i < Points.Length; i++)
            {
                if (PictureBox.Location == Points[i])
                {
                    return i + 1;
                }
            }
            return -1;
        }

        private static string GetImageTitle(int i)
        {
            switch (i)
            {
                case 1:
                    return "getting_started";
                case 2:
                    return "avatar";
                case 3:
                    return "quests";
                case 4:
                    return "units";
                case 5:
                    return "abilities";
                case 6:
                    return "items";
                case 7:
                    return "tactics_combat";
                case 8:
                    return "unit_combat";
                case 9:
                    return "avatar_combat";
                case 10:
                    return "summary";
                case 11:
                    return "final_trial";
                default:
                    return null;
            }
        }
    }
}
