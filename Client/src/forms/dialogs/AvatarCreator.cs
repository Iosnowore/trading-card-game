﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class AvatarCreator : UserControl
    {
        private int PageIndex = 0;

        private readonly Button[] SelectedButtons = new Button[4];
        private Card Card;

        public AvatarCreator()
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(BackButton);
            ButtonAnimation.FormatButton(NextButton);

            // Disable back button
            ButtonAnimation.DisableButton(BackButton);

            RadioButton1.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_on");
            RadioButton2.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            RadioButton3.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            RadioButton4.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            SelectedButtons[0] = RadioButton1;
            SelectedButtons[1] = RadioButton1;
            SelectedButtons[2] = RadioButton1;
            SelectedButtons[3] = RadioButton1;

            SetPageInfo();
        }

        private void SetPageInfo()
        {
            if (string.IsNullOrEmpty(AvatarNameTextBox.Text))
                AvatarNameTextBox.Text = "Avatar";

            switch (PageIndex)
            {
                case 0:
                    QuestionLabel.Text = StringLib.GetString("avatar_creator:species");
                    Label1.Text = StringLib.GetString("avatar_creator:human");
                    Label2.Text = StringLib.GetString("avatar_creator:twilek");
                    Label3.Text = StringLib.GetString("avatar_creator:zabrak");
                    Label4.Text = StringLib.GetString("avatar_creator:sullustan");
                    SetVisibility(true);
                    break;
                case 1:
                    QuestionLabel.Text = StringLib.GetString("avatar_creator:gender");
                    Label1.Text = StringLib.GetString("avatar_creator:male");
                    Label2.Text = StringLib.GetString("avatar_creator:female");

                    RadioButton3.Visible = false;
                    RadioButton4.Visible = false;
                    Label3.Visible = false;
                    Label4.Visible = false;
                    break;
                case 2:
                    QuestionLabel.Text = StringLib.GetString("avatar_creator:career");
                    Label1.Text = StringLib.GetString("avatar_creator:rebel");
                    Label2.Text = StringLib.GetString("avatar_creator:sith");
                    Label3.Text = StringLib.GetString("avatar_creator:jedi");
                    Label4.Text = StringLib.GetString("avatar_creator:imperial");
                    SetVisibility(true);
                    break;
                case 3:
                    QuestionLabel.Text = StringLib.GetString("avatar_creator:handle");
                    Label1.Text = StringLib.GetString("avatar_creator:dedicated");
                    Label2.Text = StringLib.GetString("avatar_creator:strong");
                    Label3.Text = StringLib.GetString("avatar_creator:arrogant");

                    Label4.Visible = false;
                    break;
            }
            ResetButtons();
            SelectedButtons[PageIndex].BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_on");
        }

        private void AvatarCreator_Load(object sender, System.EventArgs e)
        {
            UpdateAvatarCard();
        }

        private void RadioButton1_Click(object sender, System.EventArgs e)
        {
            SelectionChanged(RadioButton1);
        }

        private void RadioButton2_Click(object sender, System.EventArgs e)
        {
            SelectionChanged(RadioButton2);
        }

        private void RadioButton3_Click(object sender, System.EventArgs e)
        {
            SelectionChanged(RadioButton3);
        }

        private void RadioButton4_Click(object sender, System.EventArgs e)
        {
            SelectionChanged(RadioButton4);
        }

        private void ResetButtons()
        {
            RadioButton1.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            RadioButton2.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            RadioButton3.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            RadioButton4.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_off");
            NextButton.Text = "Next";
        }

        private void UpdateAvatarCard()
        {
            Card = new Card(GetCardData(), Card.SIZE.MEDIUM);
            Card.Location = new Point(10, (Height - Card.Height) / 2);
            Controls.Add(Card);
        }

        private CardData GetCardData()
        {
            string Image = "";
            string Archetype = "";

            if (SelectedButtons[0] == RadioButton1)
            {
                Image = "human_";
            }
            else if (SelectedButtons[0] == RadioButton2)
            {
                Image = "twilek_";
            }
            else if (SelectedButtons[0] == RadioButton3)
            {
                Image = "zabrak_";
            }
            else if (SelectedButtons[0] == RadioButton4)
            {
                Image = "sullustan_";
            }

            if (SelectedButtons[1] == RadioButton1)
            {
                Image += "male";
            }
            else if (SelectedButtons[1] == RadioButton2)
            {
                Image += "female";
            }

            if (SelectedButtons[2] == RadioButton1)
            {
                Archetype = "rebel";
            }
            else if (SelectedButtons[2] == RadioButton2)
            {
                Archetype = "sith";
            }
            else if (SelectedButtons[2] == RadioButton3)
            {
                Archetype = "jedi";
            }
            else if (SelectedButtons[2] == RadioButton4)
            {
                Archetype = "imperial";
            }

            string Species = Image.Split('_')[0];
            return new CardData(0, AvatarNameTextBox.Text, string.Empty, string.Empty, "full_" + Image, "Avatar", Species[0].ToString().ToUpper() + Species.Substring(1, Species.Length - 1), string.Empty, null, Archetype, null, 0, 1, 1, 1, 8);
        }

        private void SelectionChanged(Button Button)
        {
            ResetButtons();
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("radiobutton_on");
            SoundService.PlaySound("button");
            SelectedButtons[PageIndex] = Button;
            UpdateAvatarCard();
        }

        private void BackButton_Click(object sender, System.EventArgs e)
        {
            if (PageIndex == 0)
                return;

            SoundService.PlaySound("button");
            PageIndex--;
            SetPageInfo();

            if (PageIndex == 0)
                ButtonAnimation.DisableButton(BackButton);
        }

        private void NextButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            PageIndex++;

            if (PageIndex == 1)
                ButtonAnimation.EnableButton(BackButton);

            if (PageIndex == 4)
            {
                SetVisibility(false);
                QuestionLabel.Text = StringLib.GetString("avatar_creator:name");
                NextButton.Text = "Create";
                return;
            }
            else if (PageIndex == 5)
            {
                if (string.IsNullOrWhiteSpace(AvatarNameTextBox.Text))
                {
                    ((TCG)Parent.Parent).AddSystemMessage("Please enter a valid name.", "Invalid Avatar Name");
                    return;
                }

                PacketService.SendPacket(Commands.CREATE_AVATAR, GetCardData());
                ((TCG)Parent.Parent).AddSystemMessage("You have created a new avatar.", "Avatar Created");
                Parent.Dispose();
                return;
            }
            SetPageInfo();
        }

        private void SetVisibility(bool Visible)
        {
            RadioButton1.Visible = Visible;
            RadioButton2.Visible = Visible;
            RadioButton3.Visible = Visible;
            RadioButton4.Visible = Visible;
            Label1.Visible = Visible;
            Label2.Visible = Visible;
            Label3.Visible = Visible;
            Label4.Visible = Visible;
            AvatarNameTextBox.Visible = !Visible;
        }

        private void AvatarNameTextBox_TextChanged(object sender, System.EventArgs e)
        {
            UpdateAvatarCard();
        }
    }
}
