﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class RedemptionDetail : UserControl
    {
        private readonly Card Card;
        private readonly int CardQuantity;

        public RedemptionDetail(Card Card, int CardQuantity)
        {
            InitializeComponent();

            ButtonAnimation.FormatButton(TermsButton);
            ButtonAnimation.FormatButton(RedeemButton);

            this.Card = Card;
            this.CardQuantity = CardQuantity;

            Card NewCard = new Card(Card.GetCardData(), Card.SIZE.MEDIUM);
            NewCard.Location = new Point(20, 110);
            TitleLabel.Text = NewCard.GetCardData().GetTitle();
            SubtitleLabel.Text = NewCard.GetCardData().GetType();
            TitleLabel.Location = new Point((Width - TitleLabel.Width) / 2, TitleLabel.Location.Y);
            SubtitleLabel.Location = new Point((Width - SubtitleLabel.Width) / 2, SubtitleLabel.Location.Y);

            Controls.Add(NewCard);
            DescriptionLabel.Text = NewCard.GetCardData().GetDescription();
        }

        private void TermsButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void RedeemButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            PacketService.SendPacket(Commands.REDEEM_LOOTCARD, new CommandPacket(new object[] { Card.GetCardData().GetId(), CardQuantity }));

            Parent.Dispose();
        }

        public Card GetCard()
        {
            return Card;
        }
    }
}
