﻿namespace TradingCardGame.forms.dialogs
{
    partial class Preferences
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.userInterfaceButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.soundButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.avatarButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.accountInfoButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.preferencesFillPanel = new System.Windows.Forms.Panel();
            this.TitlePanel = new System.Windows.Forms.Panel();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.TitlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.userInterfaceButton);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.soundButton);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.avatarButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.accountInfoButton);
            this.panel1.Location = new System.Drawing.Point(25, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(125, 436);
            this.panel1.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label4.Location = new System.Drawing.Point(3, 335);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "User Interface";
            // 
            // userInterfaceButton
            // 
            this.userInterfaceButton.FlatAppearance.BorderSize = 0;
            this.userInterfaceButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.userInterfaceButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.userInterfaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userInterfaceButton.Location = new System.Drawing.Point(3, 267);
            this.userInterfaceButton.Name = "userInterfaceButton";
            this.userInterfaceButton.Size = new System.Drawing.Size(87, 74);
            this.userInterfaceButton.TabIndex = 6;
            this.userInterfaceButton.UseVisualStyleBackColor = true;
            this.userInterfaceButton.Click += new System.EventHandler(this.UserInterfaceButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label3.Location = new System.Drawing.Point(25, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sound";
            // 
            // soundButton
            // 
            this.soundButton.FlatAppearance.BorderSize = 0;
            this.soundButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.soundButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.soundButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.soundButton.Location = new System.Drawing.Point(3, 179);
            this.soundButton.Name = "soundButton";
            this.soundButton.Size = new System.Drawing.Size(87, 74);
            this.soundButton.TabIndex = 4;
            this.soundButton.UseVisualStyleBackColor = true;
            this.soundButton.Click += new System.EventHandler(this.SoundButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label2.Location = new System.Drawing.Point(25, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Avatar";
            // 
            // avatarButton
            // 
            this.avatarButton.FlatAppearance.BorderSize = 0;
            this.avatarButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.avatarButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.avatarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.avatarButton.Location = new System.Drawing.Point(3, 91);
            this.avatarButton.Name = "avatarButton";
            this.avatarButton.Size = new System.Drawing.Size(87, 74);
            this.avatarButton.TabIndex = 2;
            this.avatarButton.UseVisualStyleBackColor = true;
            this.avatarButton.Click += new System.EventHandler(this.AvatarButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label1.Location = new System.Drawing.Point(9, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Account Info";
            // 
            // accountInfoButton
            // 
            this.accountInfoButton.FlatAppearance.BorderSize = 0;
            this.accountInfoButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.accountInfoButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.accountInfoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.accountInfoButton.Location = new System.Drawing.Point(3, 3);
            this.accountInfoButton.Name = "accountInfoButton";
            this.accountInfoButton.Size = new System.Drawing.Size(87, 74);
            this.accountInfoButton.TabIndex = 0;
            this.accountInfoButton.UseVisualStyleBackColor = true;
            this.accountInfoButton.Click += new System.EventHandler(this.AccountInfoButton_Click);
            // 
            // acceptButton
            // 
            this.acceptButton.BackColor = System.Drawing.Color.Black;
            this.acceptButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.acceptButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.acceptButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.acceptButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acceptButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acceptButton.ForeColor = System.Drawing.Color.Black;
            this.acceptButton.Location = new System.Drawing.Point(813, 506);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(110, 35);
            this.acceptButton.TabIndex = 55;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = false;
            this.acceptButton.Click += new System.EventHandler(this.AcceptButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.Color.Black;
            this.resetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.resetButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.resetButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.resetButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.ForeColor = System.Drawing.Color.Black;
            this.resetButton.Location = new System.Drawing.Point(697, 506);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(110, 35);
            this.resetButton.TabIndex = 56;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Black;
            this.cancelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancelButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(581, 506);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(110, 35);
            this.cancelButton.TabIndex = 57;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // preferencesFillPanel
            // 
            this.preferencesFillPanel.Location = new System.Drawing.Point(156, 100);
            this.preferencesFillPanel.Name = "preferencesFillPanel";
            this.preferencesFillPanel.Size = new System.Drawing.Size(767, 400);
            this.preferencesFillPanel.TabIndex = 58;
            // 
            // TitlePanel
            // 
            this.TitlePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TitlePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TitlePanel.Controls.Add(this.TitleLabel);
            this.TitlePanel.Location = new System.Drawing.Point(156, 64);
            this.TitlePanel.Name = "TitlePanel";
            this.TitlePanel.Size = new System.Drawing.Size(768, 30);
            this.TitlePanel.TabIndex = 59;
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(1, 3);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(112, 20);
            this.TitleLabel.TabIndex = 0;
            this.TitleLabel.Text = "Account Info";
            // 
            // Preferences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.TitlePanel);
            this.Controls.Add(this.preferencesFillPanel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Preferences";
            this.Size = new System.Drawing.Size(950, 560);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.TitlePanel.ResumeLayout(false);
            this.TitlePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button accountInfoButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button avatarButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button soundButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button userInterfaceButton;
        private System.Windows.Forms.Panel preferencesFillPanel;
        private System.Windows.Forms.Panel TitlePanel;
        private System.Windows.Forms.Label TitleLabel;
    }
}
