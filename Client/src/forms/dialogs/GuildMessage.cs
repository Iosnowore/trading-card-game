﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class GuildMessage : UserControl
    {
        private Point p;

        public GuildMessage(string Message)
        {
            InitializeComponent();
            MessageTextBox.Text = Message;
        }

        private void GuildMessage_Load(object sender, System.EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void GuildMessage_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - p.X + Location.X, e.Y - p.Y + Location.Y);
        }

        private void GuildMessage_MouseMove(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }
    }
}
