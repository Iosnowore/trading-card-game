﻿namespace TradingCardGame.forms.dialogs
{
    partial class Friends
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ignoreListBox = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.removeIgnoredButton = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.IgnoreUserButton = new System.Windows.Forms.Button();
            this.IgnoreUserTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.removeFriendButton = new System.Windows.Forms.Button();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendsListBox = new System.Windows.Forms.ListView();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ignoreListBox
            // 
            this.ignoreListBox.BackColor = System.Drawing.Color.Black;
            this.ignoreListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.ignoreListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ignoreListBox.ForeColor = System.Drawing.Color.White;
            this.ignoreListBox.FullRowSelect = true;
            this.ignoreListBox.Location = new System.Drawing.Point(283, 42);
            this.ignoreListBox.MultiSelect = false;
            this.ignoreListBox.Name = "ignoreListBox";
            this.ignoreListBox.Size = new System.Drawing.Size(256, 188);
            this.ignoreListBox.TabIndex = 59;
            this.ignoreListBox.UseCompatibleStateImageBehavior = false;
            this.ignoreListBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Usernames";
            this.columnHeader2.Width = 252;
            // 
            // removeIgnoredButton
            // 
            this.removeIgnoredButton.BackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeIgnoredButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatAppearance.BorderSize = 0;
            this.removeIgnoredButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeIgnoredButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeIgnoredButton.ForeColor = System.Drawing.Color.Black;
            this.removeIgnoredButton.Location = new System.Drawing.Point(362, 236);
            this.removeIgnoredButton.Name = "removeIgnoredButton";
            this.removeIgnoredButton.Size = new System.Drawing.Size(100, 29);
            this.removeIgnoredButton.TabIndex = 57;
            this.removeIgnoredButton.Text = "Remove";
            this.removeIgnoredButton.UseVisualStyleBackColor = false;
            this.removeIgnoredButton.Click += new System.EventHandler(this.removeIgnoredButton_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(283, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(256, 35);
            this.panel4.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(203)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(84, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ignore List";
            // 
            // IgnoreUserButton
            // 
            this.IgnoreUserButton.BackColor = System.Drawing.Color.Black;
            this.IgnoreUserButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IgnoreUserButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.IgnoreUserButton.FlatAppearance.BorderSize = 0;
            this.IgnoreUserButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.IgnoreUserButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.IgnoreUserButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IgnoreUserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IgnoreUserButton.ForeColor = System.Drawing.Color.Black;
            this.IgnoreUserButton.Location = new System.Drawing.Point(426, 274);
            this.IgnoreUserButton.Name = "IgnoreUserButton";
            this.IgnoreUserButton.Size = new System.Drawing.Size(113, 29);
            this.IgnoreUserButton.TabIndex = 60;
            this.IgnoreUserButton.Text = "Ignore User";
            this.IgnoreUserButton.UseVisualStyleBackColor = false;
            this.IgnoreUserButton.Click += new System.EventHandler(this.IgnoreUserButton_Click);
            // 
            // IgnoreUserTextBox
            // 
            this.IgnoreUserTextBox.BackColor = System.Drawing.Color.Black;
            this.IgnoreUserTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IgnoreUserTextBox.ForeColor = System.Drawing.Color.White;
            this.IgnoreUserTextBox.Location = new System.Drawing.Point(283, 277);
            this.IgnoreUserTextBox.Name = "IgnoreUserTextBox";
            this.IgnoreUserTextBox.Size = new System.Drawing.Size(137, 22);
            this.IgnoreUserTextBox.TabIndex = 61;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(203)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(81, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Friends List";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 35);
            this.panel1.TabIndex = 0;
            // 
            // removeFriendButton
            // 
            this.removeFriendButton.BackColor = System.Drawing.Color.Black;
            this.removeFriendButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeFriendButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatAppearance.BorderSize = 0;
            this.removeFriendButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.removeFriendButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeFriendButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeFriendButton.ForeColor = System.Drawing.Color.Black;
            this.removeFriendButton.Location = new System.Drawing.Point(75, 236);
            this.removeFriendButton.Name = "removeFriendButton";
            this.removeFriendButton.Size = new System.Drawing.Size(100, 29);
            this.removeFriendButton.TabIndex = 57;
            this.removeFriendButton.Text = "Remove";
            this.removeFriendButton.UseVisualStyleBackColor = false;
            this.removeFriendButton.Click += new System.EventHandler(this.removeFriendButton_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Usernames";
            this.columnHeader1.Width = 252;
            // 
            // friendsListBox
            // 
            this.friendsListBox.BackColor = System.Drawing.Color.Black;
            this.friendsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.friendsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.friendsListBox.ForeColor = System.Drawing.Color.White;
            this.friendsListBox.FullRowSelect = true;
            this.friendsListBox.Location = new System.Drawing.Point(1, 42);
            this.friendsListBox.MultiSelect = false;
            this.friendsListBox.Name = "friendsListBox";
            this.friendsListBox.Size = new System.Drawing.Size(256, 188);
            this.friendsListBox.TabIndex = 59;
            this.friendsListBox.UseCompatibleStateImageBehavior = false;
            this.friendsListBox.View = System.Windows.Forms.View.Details;
            // 
            // Friends
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.ignoreListBox);
            this.Controls.Add(this.friendsListBox);
            this.Controls.Add(this.removeIgnoredButton);
            this.Controls.Add(this.IgnoreUserTextBox);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.removeFriendButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.IgnoreUserButton);
            this.DoubleBuffered = true;
            this.Name = "Friends";
            this.Size = new System.Drawing.Size(556, 310);
            this.Load += new System.EventHandler(this.Friends_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Friends_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Friends_MouseMove);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView ignoreListBox;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button removeIgnoredButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button IgnoreUserButton;
        private System.Windows.Forms.TextBox IgnoreUserTextBox;
        private System.Windows.Forms.ListView friendsListBox;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button removeFriendButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}