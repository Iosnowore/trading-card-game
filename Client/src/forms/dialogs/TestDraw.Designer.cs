﻿namespace TradingCardGame.Src.Forms.Dialogs
{
    partial class TestDraw
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DrawButton = new System.Windows.Forms.Button();
            this.ShuffleAndRedrawButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DrawButton
            // 
            this.DrawButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DrawButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DrawButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DrawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DrawButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DrawButton.Location = new System.Drawing.Point(846, 322);
            this.DrawButton.Name = "DrawButton";
            this.DrawButton.Size = new System.Drawing.Size(75, 30);
            this.DrawButton.TabIndex = 0;
            this.DrawButton.Text = "Draw";
            this.DrawButton.UseVisualStyleBackColor = true;
            this.DrawButton.Click += new System.EventHandler(this.DrawButton_Click);
            // 
            // ShuffleAndRedrawButton
            // 
            this.ShuffleAndRedrawButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ShuffleAndRedrawButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ShuffleAndRedrawButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.ShuffleAndRedrawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShuffleAndRedrawButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShuffleAndRedrawButton.Location = new System.Drawing.Point(690, 322);
            this.ShuffleAndRedrawButton.Name = "ShuffleAndRedrawButton";
            this.ShuffleAndRedrawButton.Size = new System.Drawing.Size(150, 30);
            this.ShuffleAndRedrawButton.TabIndex = 1;
            this.ShuffleAndRedrawButton.Text = "Shuffle & Redraw";
            this.ShuffleAndRedrawButton.UseVisualStyleBackColor = true;
            this.ShuffleAndRedrawButton.Click += new System.EventHandler(this.ShuffleAndRedrawButton_Click);
            // 
            // TestDraw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.ShuffleAndRedrawButton);
            this.Controls.Add(this.DrawButton);
            this.DoubleBuffered = true;
            this.Name = "TestDraw";
            this.Size = new System.Drawing.Size(940, 355);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DrawButton;
        private System.Windows.Forms.Button ShuffleAndRedrawButton;
    }
}
