﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.forms.dialogs
{
    public partial class SinglePlayerOptions : UserControl
    {
        private Panel selectedButton;

        public SinglePlayerOptions()
        {
            InitializeComponent();
            ButtonAnimation.FormatButton(SelectButton);
            ButtonAnimation.DisableButton(SelectButton);

            TutorialsButtonImage.Image = ResourcesLib.GetImageFromDialogs("scenario_thumb_tutorials");
            ScenariosButtonImage.Image = ResourcesLib.GetImageFromDialogs("scenario_thumb_scenarios");
            SkirmishButtonImage.Image = ResourcesLib.GetImageFromDialogs("scenario_thumb_skirmish");
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            if (selectedButton == null)
                return;

            if (selectedButton == TutorialsButton)
                Parent.Parent.Controls.Add(new Window(new Tutorials(), "Tutorials"));
            else if (selectedButton == ScenariosButton)
                Parent.Parent.Controls.Add(new Window(new Scenarios(), "Scenarios"));
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Parent.Dispose();
        }

        private new void Click(Panel selectedButton)
        {
            if (this.selectedButton == selectedButton)
                return;

            Panel[] buttons = { TutorialsButton, ScenariosButton, SkirmishButton };

            foreach (Panel button in buttons)
            {
                if (button == selectedButton)
                    button.BorderStyle = BorderStyle.Fixed3D;
                else
                {
                    button.BorderStyle = BorderStyle.FixedSingle;
                    button.BackColor = Color.BlanchedAlmond;
                }
                    
            }
            ButtonAnimation.EnableButton(SelectButton);
            this.selectedButton = selectedButton;
            SoundService.PlaySound("button");
        }

        private void TutorialsButton_MouseClick(object sender, MouseEventArgs e)
        {
            Click(TutorialsButton);
        }

        private void ScenariosButton_MouseClick(object sender, MouseEventArgs e)
        {
            Click(ScenariosButton);
        }

        private void SkirmishButton_MouseClick(object sender, MouseEventArgs e)
        {
            Click(SkirmishButton);
        }

        private new void MouseEnter(Panel SelectedButton)
        {
            SelectedButton.BackColor = Color.SkyBlue;
        }

        private new void MouseLeave(Panel SelectedButton)
        {
            if (!SelectedButton.ClientRectangle.Contains(SelectedButton.PointToClient(MousePosition)) && SelectedButton.BorderStyle == BorderStyle.FixedSingle)
                SelectedButton.BackColor = Color.BlanchedAlmond;
        }

        private void TutorialsButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(TutorialsButton);
        }

        private void TutorialsButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(TutorialsButton);
        }

        private void ScenariosButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(ScenariosButton);
        }

        private void ScenariosButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(ScenariosButton);
        }

        private void SkirmishButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(SkirmishButton);
        }

        private void SkirmishButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(SkirmishButton);
        }
    }
}
