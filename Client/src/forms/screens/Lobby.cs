﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TCGData.Packets.Player;
using TradingCardGame.Components;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.guild;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Chat;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Player;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.Forms.Screens
{
    public partial class Lobby : UserControl
    {
        private readonly List<MatchPanel> matchPanels = new List<MatchPanel>();
        private readonly List<string> SentMessages = new List<string>();

        private readonly string lobbyTitle;

        private bool SentLeaveLobbyPacket;

        private int MessageLookbackIndex = -1;

        private readonly Button PostedTradesButton;

        public Lobby(string lobbyTitle)
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            this.lobbyTitle = lobbyTitle;

            BackgroundImage = ResourcesLib.GetImageFromBackgrounds("game_bg_01");
            panel1.BackgroundImage = ResourcesLib.GetImageFromBorders("navframe_header");
            playNowButton.BackgroundImage = ResourcesLib.GetImageFromDialogs("practice_button");

            playersNameListBox.SmallImageList = ResourcesLib.PLAYER_LIST;

            ButtonAnimation.FormatButton(CreateButton);
            ButtonAnimation.FormatButton(QuickJoinButton);

            if (lobbyTitle.Equals(LobbyTitles.CASUAL_GAMES))
            {
                if (!SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "CasualLobby"))
                    Controls.Add(new Window(new PlayerTips(PlayerTips.Types.CasualLobby), "Player Tips"));
            }
            else if (lobbyTitle.Equals(LobbyTitles.TRADE))
            {
                if (!SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "TradeLobby"))
                    Controls.Add(new Window(new PlayerTips(PlayerTips.Types.TradeLobby), "Player Tips"));

                activeGamesPanel.Height = 200;
                activeGamesPanel.BorderStyle = BorderStyle.None;

                chatBox.Height = 450;
                chatBox.Location = new Point(chatBox.Location.X, activeGamesPanel.Location.Y + activeGamesPanel.Height + 5);

                PostedTradesButton = new Button();
                PostedTradesButton.Click += PostedTradesButton_Click;
                PostedTradesButton.Font = new Font(FontFamily.GenericSansSerif, 9.75f, FontStyle.Bold);
                PostedTradesButton.Size = new Size(140, 30);
                PostedTradesButton.BackgroundImageLayout = ImageLayout.Stretch;
                PostedTradesButton.FlatStyle = FlatStyle.Flat;
                PostedTradesButton.FlatAppearance.MouseDownBackColor = Color.FromArgb(15, 32, 42);
                PostedTradesButton.FlatAppearance.MouseOverBackColor = Color.FromArgb(15, 32, 42);
                PostedTradesButton.FlatAppearance.BorderSize = 0;
                PostedTradesButton.Text = "Posted Trades";
                PostedTradesButton.Location = new Point(10, 10);
                PostedTradesButton.BackColor = Color.FromArgb(15, 32, 42);
                ButtonAnimation.FormatButton(PostedTradesButton);
                activeGamesPanel.Controls.Add(PostedTradesButton);

                Label PostedTradesLabel = new Label
                {
                    Text = "Post your own persistent trades or browse trades posted by others.",
                    ForeColor = Color.LightSkyBlue,
                    Width = 400
                };
                PostedTradesLabel.Location = new Point(PostedTradesButton.Location.X + PostedTradesButton.Width, PostedTradesButton.Location.Y + (PostedTradesButton.Height - PostedTradesLabel.Height) / 2);
                activeGamesPanel.Controls.Add(PostedTradesLabel);
            }

            PacketService.SetLobby(this);
            PacketService.SendPacket(Commands.LOBBY_JOIN, new CommandPacket(new string[] { lobbyTitle }));

            HandleDestroyed += Lobby_HandleDestroyed;
            BringToFront();

            DiscordPresenceService.UpdateDetails("In Lobby");
            DiscordPresenceService.UpdatePartyState(lobbyTitle);
            DiscordPresenceService.UpdateJoinSecret(lobbyTitle);
        }

        private void PostedTradesButton_Click(object sender, EventArgs e)
        {
            Controls.Add(new Window(new PostedTrades(), "Posted Trades"));
            SoundService.PlaySound("button");
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void InputChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SentMessages.Add(inputChatBox.Text);
                ChatService.ChatBoxKeyEnter(inputChatBox, chatBox, e);
                MessageLookbackIndex = SentMessages.Count;
            }
            else if (e.KeyCode == Keys.Up)
            {
                if (MessageLookbackIndex - 1 > -1)
                {
                    MessageLookbackIndex--;
                    inputChatBox.Text = SentMessages[MessageLookbackIndex];
                    inputChatBox.SelectionStart = inputChatBox.Text.Length;

                    // Prevent caret from going to the left
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (MessageLookbackIndex + 1 < SentMessages.Count)
                {
                    MessageLookbackIndex++;
                    inputChatBox.Text = SentMessages[MessageLookbackIndex];
                    inputChatBox.SelectionStart = inputChatBox.Text.Length;
                }
                else
                {
                    inputChatBox.Clear();
                }
            }
            SoundService.KeyPressSound(e.KeyCode);
        }

        private delegate void OpenUserInfoDelegate(UserInfoPacket UserInfoPacket);

        public void OpenUserInfo(UserInfoPacket UserInfoPacket)
        {
            if (InvokeRequired)
                Invoke(new OpenUserInfoDelegate(OpenUserInfo), UserInfoPacket);
            else
                Controls.Add(new Window(new UserInfo(UserInfoPacket), UserInfoPacket.GetUsername()));
        }

        private delegate void MatchPanelDelegate(MatchPacket MatchPacket);

        private void AddMatchPanel(MatchPacket MatchPacket)
        {
            if (activeGamesPanel.InvokeRequired)
                activeGamesPanel.Invoke(new MatchPanelDelegate(AddMatchPanel), MatchPacket);
            else
            {
                Console.WriteLine("GOING TO CREATE MatchPanel");
                MatchPanel panel = new MatchPanel(MatchPacket)
                {
                    Location = new Point(50, 50)
                };
                activeGamesPanel.Controls.Add(panel);
                matchPanels.Add(panel);
            }
        }

        private delegate void RemoveMatchPanelDelegate(MatchPanel panel);

        private void RemoveMatchPanel(MatchPanel panel)
        {
            if (panel.InvokeRequired)
                panel.Invoke(new RemoveMatchPanelDelegate(RemoveMatchPanel), panel);
            else
                panel.Dispose();
        }

        public void UpdateMatches(MatchPacket[] MatchPackets)
        {
            if (matchPanels.Count > 0)
            {
                foreach (MatchPanel panel in matchPanels)
                    RemoveMatchPanel(panel);
                matchPanels.Clear();
            }

            if (MatchPackets.Length > 0)
            {
                foreach (MatchPacket MatchPacket in MatchPackets)
                    AddMatchPanel(MatchPacket);
            }
        }

        private void Lobby_HandleDestroyed(object sender, EventArgs e)
        {
            if (!SentLeaveLobbyPacket)
                PacketService.SendPacket(Commands.LOBBY_LEAVE, new CommandPacket(new string[] { lobbyTitle }));
        }

        private void PlayNowButton_MouseEnter(object sender, EventArgs e)
        {
            playNowButton.Image = ResourcesLib.GetImageFromDialogs("practice_button_over");
        }

        private void PlayNowButton_MouseDown(object sender, MouseEventArgs e)
        {
            playNowButton.Image = ResourcesLib.GetImageFromDialogs("practice_button_down");
        }

        private void PlayNowButton_MouseLeave(object sender, EventArgs e)
        {
            playNowButton.Image = ResourcesLib.GetImageFromDialogs("practice_button");
        }

        private void PlayNowButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void ChangeCheckBox(PictureBox checkBox, bool checkedBool)
        {
            checkBox.Image = checkedBool ? ResourcesLib.GetImageFromButtons("checkbox_off") : ResourcesLib.GetImageFromButtons("checkbox_on");
            SoundService.PlaySound("button");
        }

        private void CreateMatchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            Controls.Add(new Window(new CreateMatch(), "Create Match"));
        }

        private void PlayersNameListBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && !TCG.GetDialogStatus() && playersNameListBox.SelectedItems.Count > 0)
            {
                // Dropdown menu
                string[] DropDownOptions = { "Challenge", "Join hosted game", "Observe match", "Trade", "Add to friends list", "Ignore", "Invite to guild", "Kick from guild", "Get info" };

                DropDownMenu Menu = new DropDownMenu(DropDownOptions);
                Menu.Location = new Point(playersNameListBox.Location.X + e.X, playersNameListBox.Location.Y + e.Y);
                Controls.Add(Menu);
            }
        }

        private void ChatBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            chatBox.SelectedIndex = -1;
        }

        public Panel GetActiveGamesPanel()
        {
            return activeGamesPanel;
        }

        public ListBox GetChatBox()
        {
            return chatBox;
        }

        private void ChatBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            string GuildName = GuildService.GetGuildName();
            string Text = chatBox.Items[e.Index].ToString();

            if (!string.IsNullOrEmpty(GuildName) && Text.IndexOf(GuildName) == 0)
            {
                e.Graphics.DrawString(Text, new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular), new SolidBrush(Color.FromArgb(90, 255, 87)), e.Bounds);
            }
            else
            {
                e.Graphics.DrawString(Text, new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular), new SolidBrush(Color.White), e.Bounds);
            }
        }

        private delegate void _UpdateLobbyUsernames(string[] Usernames);
        public void UpdateLobbyUsernames(string[] Usernames)
        {
            DiscordPresenceService.SetParty(lobbyTitle, 6, Usernames.Length);

            // Update usersLabel text
            if (usersLabel.InvokeRequired)
                usersLabel.Invoke(new _UpdateLobbyUsernames(UpdateLobbyUsernames), new object[] { Usernames });
            else
                usersLabel.Text = "Users: " + Usernames.Length + " / 6";

            // Update playersNameListBox
            if (playersNameListBox.InvokeRequired)
                playersNameListBox.Invoke(new _UpdateLobbyUsernames(UpdateLobbyUsernames), new object[] { Usernames });
            else
            {
                playersNameListBox.Items.Clear();

                foreach (string Username in Usernames)
                {
                    string icon = Username.StartsWith("Admin-") || Username.StartsWith("CSR-") || Username.StartsWith("QA-") ? "staff" : "bronze";
                    playersNameListBox.Items.Add(Username, icon);
                }
            }
        }

        public string GetLobbyTitle()
        {
            return lobbyTitle;
        }

        public void SetSentLeaveLobbyPacket()
        {
            SentLeaveLobbyPacket = true;
        }

        public void DropDownSelected(int Index)
        {
            string Username = playersNameListBox.SelectedItems[0].Text;

            switch (Index)
            {
                case 0:
                    // Challenge
                    break;
                case 1:
                    // Join hosted game
                    break;
                case 2:
                    // Observe match
                    break;
                case 3:
                    // Trade
                    PacketService.SendPacket(Commands.TRADE_PLAYER, new CommandPacket(new string[] { Username }));
                    break;
                case 4:
                    // Add to friends list
                    PlayerService.AddFriendCommand(Username);
                    break;
                case 5:
                    // Ignore
                    PlayerService.IgnorePlayerCommand(Username);
                    break;
                case 6:
                    // Invite to guild
                    GuildService.GuildInviteCommand(Username, chatBox);
                    break;
                case 7:
                    // Kick from guild
                    GuildService.GuildKick(Username);
                    break;
                case 8:
                    // Get info
                    TCG.SetDialogStatus(true);
                    OpenUserInfo(PacketService.GetUserInfoPacket(Username));
                    break;
            }
        }
    }
}
