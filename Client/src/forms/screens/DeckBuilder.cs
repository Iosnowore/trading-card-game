﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TradingCardGame.Components;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Forms.Widgets;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Collection;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame
{
    public partial class DeckBuilder : UserControl
    {
        private readonly FilterPanel FilterPanel = new FilterPanel();

        private Button LastClickedButton;
        private readonly bool[] SearchOptions = new bool[] { true, true, true, true, true };

        private readonly CollectionPacket MyCollection;

        private DropDownMenu DropDownMenu;

        private Card PreviewCard;

        private readonly ViewOption CollectionViewOption;
        private readonly ViewOption DeckViewOption;

        private readonly CardTableView CollectionTableView;
        private readonly CardTableView DeckTableView;

        private const int CARDS_PER_PAGE = 48;

        private readonly bool ShowOnlyMyCards;
        private readonly Card.SIZE CollectionSize;
        private readonly Card.SIZE BuilderSize;

        private SortedDictionary<int, int> DeckCardIds;

        private int CurrentPage;
        private readonly int MaxPage;

        public DeckBuilder()
        {
            InitializeComponent();

            MyCollection = PacketService.GetCollectionPacket();

            BackgroundImage = ResourcesLib.GetImageFromBackgrounds("dialog_bg");
            exitButton.Image = ResourcesLib.GetImageFromButtons("helpbutton");
            HelperButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_db_wizard");
            TopCardPanel.BackgroundImage = ResourcesLib.GetImageFromBinder("collection_manager_background");
            BottomCardPanel.BackgroundImage = ResourcesLib.GetImageFromBinder("collection_manager_background");
            AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar");
            QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest");
            DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck");
            AbilitiesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
            TacticsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
            ItemsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
            UnitsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");

            ButtonAnimation.FormatButton(FilterButton);
            ButtonAnimation.FormatButton(SearchButton);
            ButtonAnimation.FormatButton(showValidQuestsButton);
            ButtonAnimation.FormatButton(removeAllFiltersButton);
            ButtonAnimation.FormatButton(standardButton);

            validDeckPictureBox.BackgroundImage = ResourcesLib.GetImageFromButtons("button_validate");
            validDeckPictureBox.Image = ResourcesLib.GetImageFromIcons("no");

            ButtonAnimation.FormatButton(NewDeckButton);
            ButtonAnimation.FormatButton(OpenDeckButton);
            ButtonAnimation.FormatButton(SaveDeckButton);
            ButtonAnimation.FormatButton(ToolsButton);
            //MenuPanel.BackgroundImage = ResourcesLib.GetImageFromBinder("paper_texture");

            PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
            NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");

            ShowOnlyMyCards = SettingsService.GetBoolFromSettingsXml("ShowOnlyCardsIOwn", "DeckBuilder");
            CollectionSize = ConvertToSize(SettingsService.GetStringFromSettingsXml("CardSize", "DeckBuilderCollection"));
            BuilderSize = ConvertToSize(SettingsService.GetStringFromSettingsXml("CardSize", "DeckBuilderDeck"));

            FilterPanel.Location = new Point(FilterButton.Location.X, FilterButton.Location.Y + FilterButton.Height);
            Controls.Add(FilterPanel);
            FilterPanel.Visible = false;

            CardQuantityLabel.Text = CollectionService.GetNumberOfOwnedCards(MyCollection) + " cards.";

            int[] CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedCardIds(MyCollection) : TCGData.Cards.GetCardIds();

            MaxPage = CardIds.Length / CARDS_PER_PAGE;

            DiscordPresenceService.UpdateDetails("Building Card Deck");
            DiscordPresenceService.UpdatePartyState(null);

            // Add Table View options for both collection container and deck container
            CollectionViewOption = new ViewOption(this);
            CollectionViewOption.Location = new Point(SearchButton.Location.X - CollectionViewOption.Width, CollectionViewOption.Location.Y);
            Controls.Add(CollectionViewOption);

            DeckViewOption = new ViewOption(this);
            DeckViewOption.Location = new Point(MenuPanel.Width - DeckViewOption.Width, (MenuPanel.Height - DeckViewOption.Height) / 2);
            MenuPanel.Controls.Add(DeckViewOption);

            CollectionTableView = new CardTableView();
            CollectionTableView.Size = new Size(970, 200);
            CollectionTableView.Location = new Point(27, 73);
            Controls.Add(CollectionTableView);

            DeckTableView = new CardTableView();
            DeckTableView.Size = new Size(600, 409);
            DeckTableView.Location = new Point(187, 59);
            BottomPanel.Controls.Add(DeckTableView);

            DeckCardIds = new SortedDictionary<int, int>();
        }

        private Card.SIZE ConvertToSize(string Size)
        {
            switch (Size)
            {
                case "Small":
                    return Card.SIZE.SMALL;
                case "Medium":
                    return Card.SIZE.MEDIUM;
                default:
                    return Card.SIZE.LARGE;
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void DeckBuilder_Load(object sender, EventArgs e)
        {
            if (!SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "DeckBuilder"))
                Controls.Add(new Window(new PlayerTips(PlayerTips.Types.DeckBuilder), "Player Tips"));

            LoadCards();

            // Play the DeckBuilder music
            MusicService.TransitionMusic("music_deckbuilder", -1);
        }

        private void CoverPanel_MouseLeave(object sender, EventArgs e)
        {
            if (PreviewCard != null && !((Card)sender).ClientRectangle.Contains(PointToClient(MousePosition)))
                PreviewCard.Dispose();
        }

        private void CoverPanel_MouseHover(object sender, EventArgs e)
        {
            if (PreviewCard == null || PreviewCard.IsDisposed)
                SetPreviewCard((Card)sender);
        }

        private void SetPreviewCard(Card card)
        {
            if (card.GetCardData() != null)
                PreviewCard = new Card(card.GetCardData(), Card.SIZE.LARGE);

            if (card.IsShowingLore())
                PreviewCard.ShowLore();

            if (card.Location.X < (card.Parent.Width - 50) / 2)
                PreviewCard.Location = new Point(card.Parent.Location.X + card.Location.X + card.Width + 10, card.Parent.Location.Y + 10);
            else
                PreviewCard.Location = new Point(card.Parent.Location.X + card.Location.X - PreviewCard.Width - 10, card.Parent.Location.Y + 10);
            Controls.Add(PreviewCard);
        }

        public void ChangeCardLocation(Card Card)
        {
            if (Card.Parent == TopCardPanel)
            {
                TransferCardToPanel(Card, 1, TopCardPanel, BottomCardPanel);
            }
            else
            {
                TransferCardToPanel(Card, 1, BottomCardPanel, TopCardPanel);
            }
            UpdateDeckStatistics();
        }

        private void TransferCardToPanel(Card Card, int Quantity, FlowLayoutPanel From, FlowLayoutPanel To)
        {
            Card DestCard = GetCardFromPanel(Card.GetCardData().GetId(), To);
            if (DestCard != null && Card.GetCardData().GetId() > -1)
            {
                DestCard.SetQuantityLabel(DestCard.GetQuantity() + Quantity);

                // Reflect that we added a card to our deck
                if (To == BottomCardPanel)
                    DeckCardIds[DestCard.GetId()] = DestCard.GetQuantity();
            }
            else
            {
                if (Card.GetCardData().GetId() > -1)
                {
                    // Check if we are showing filtered cards
                    string Type = Card.GetCardData().GetType();
                    if (LastClickedButton == null || LastClickedButton == FilterButton ||
                        Type.Equals("Avatar") && LastClickedButton == AvatarButton ||
                        Type.Equals("Quest") && LastClickedButton == QuestsButton ||
                        Type.Equals("Ability") && (LastClickedButton == AbilitiesButton || LastClickedButton == DrawDeckButton) ||
                        Type.Equals("Tactic") && (LastClickedButton == TacticsButton || LastClickedButton == DrawDeckButton) ||
                        Type.Equals("Item") && (LastClickedButton == ItemsButton || LastClickedButton == DrawDeckButton) ||
                        Type.Equals("Unit") && (LastClickedButton == UnitsButton || LastClickedButton == DrawDeckButton))
                    {
                        Card NewCard = new Card(Card.GetCardData().GetId() > -1 ? TCGData.Cards.GetCardData(Card.GetCardData().GetId()) : CollectionService.GetAvatarData(Card.GetCardData().GetTitle(), MyCollection), To == BottomPanel ? BuilderSize : CollectionSize);
                        NewCard.SetQuantityLabel(Quantity);
                        NewCard.MouseHover += CoverPanel_MouseHover;
                        NewCard.MouseLeave += CoverPanel_MouseLeave;
                        To.Controls.Add(NewCard);
                    }
                    // Reflect that we added a new card object to our deck
                    if (To == BottomCardPanel && !DeckCardIds.ContainsKey(Card.GetId()))
                        DeckCardIds.Add(Card.GetId(), Quantity);
                }
            }

            if (Card.GetQuantity() - Quantity == 0 || Card.GetId() == -1)
            {
                // Reflect that we removed a card object from our deck
                if (From == BottomCardPanel)
                    DeckCardIds.Remove(Card.GetId());
                From.Controls.Remove(Card);
            }
            else if (Card.GetQuantity() > 0)
            {
                Card.SetQuantityLabel(Card.GetQuantity() - Quantity);

                // Reflect that we removed a card from our deck
                if (From == BottomCardPanel)
                    DeckCardIds[Card.GetId()] = Card.GetQuantity();
            }

            // Update ListViews if showing
            if (DeckTableView.Visible)
            {
                ReloadDeckTableViewItems();
            }

            if (CollectionTableView.Visible)
            {
                LoadCards();
            }
        }

        private Card GetCardFromPanel(int CardId, FlowLayoutPanel Panel)
        {
            Card Card;

            for (int i = 0; i < Panel.Controls.Count; i++)
            {
                Card = (Card)Panel.Controls[i];
                if (Card.GetId() == CardId)
                    return Card;
            }
            return null;
        }

        private Card GetCardInPanel(int CardId, FlowLayoutPanel Panel)
        {
            Card Card;

            for (int i = 0; i < Panel.Controls.Count; i++)
            {
                Card = ((Card)Panel.Controls[i]);

                if (Card.GetId() == CardId)
                {
                    return Card;
                }
            }
            return null;
        }

        private void TraitFilterButtonClick(string Trait, Button Button)
        {
            if (LastClickedButton == Button)
                return;

            ResetFilterButtons();

            LastClickedButton = Button;

            SoundService.PlaySound("button");

            if (DeckCardIds.Count > 0)
            {
                BottomCardPanel.Controls.Clear();

                Card Card;

                foreach (int Id in DeckCardIds.Keys)
                {
                    Card = new Card(TCGData.Cards.GetCardData(Id), BuilderSize);
                    Card.SetQuantityLabel(DeckCardIds[Id]);

                    if (Card.GetCardData().GetType().Equals(Trait))
                        BottomCardPanel.Controls.Add(Card);
                }
            }
        }

        private void ResetFilterButtons()
        {
            if (LastClickedButton != null)
                if (LastClickedButton == AvatarButton)
                    AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar");
                else if (LastClickedButton == QuestsButton)
                    QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest");
                else if (LastClickedButton == DrawDeckButton)
                    DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck");
                else
                    LastClickedButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
        }

        private void FilterButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            LastClickedButton = FilterButton;
            FilterPanel.Visible = !FilterPanel.Visible;
        }

        private void AvatarButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Avatar", AvatarButton);
        }

        private void AvatarButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != AvatarButton)
                AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar_down");
        }

        private void AvatarButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != AvatarButton)
                AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar_over");
        }

        private void AvatarButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != AvatarButton)
                AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar");
        }

        private void AvatarButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != AvatarButton)
                AvatarButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_avatar_over");
        }

        private void QuestsButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Quest", QuestsButton);
        }

        private void QuestsButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != QuestsButton)
                QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest_down");
        }

        private void QuestsButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != QuestsButton)
                QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest_over");
        }

        private void QuestsButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != QuestsButton)
                QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest");
        }

        private void QuestsButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != QuestsButton)
                QuestsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_quest_over");
        }

        private void DrawDeckButton_Click(object sender, EventArgs e)
        {
            if (LastClickedButton == DrawDeckButton)
                return;

            ResetFilterButtons();
            LastClickedButton = DrawDeckButton;
            SoundService.PlaySound("button");

            if (DeckCardIds.Count > 0)
            {
                BottomCardPanel.Controls.Clear();

                CardData CardData;
                Card Card;

                foreach (int Id in DeckCardIds.Keys)
                {
                    CardData = TCGData.Cards.GetCardData(Id);

                    // Don't show avatars and quests in draw deck
                    if (!CardData.GetType().Equals("Avatar") && !CardData.GetType().Equals("Quest"))
                    {
                        Card = new Card(CardData, BuilderSize);
                        Card.SetQuantityLabel(DeckCardIds[Id]);
                        BottomCardPanel.Controls.Add(Card);
                    }
                }
            }
        }

        private void DrawDeckButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != DrawDeckButton)
                DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck_down");
        }

        private void DrawDeckButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != DrawDeckButton)
                DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck_over");
        }

        private void DrawDeckButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != DrawDeckButton)
                DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck");
        }

        private void DrawDeckButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != DrawDeckButton)
                DrawDeckButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_drawdeck_over");
        }

        private void AbilitiesButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Ability", AbilitiesButton);
        }

        private void AbilitiesButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != AbilitiesButton)
                AbilitiesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_down");
        }

        private void AbilitiesButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != AbilitiesButton)
                AbilitiesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void AbilitiesButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != AbilitiesButton)
                AbilitiesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
        }

        private void AbilitiesButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != AbilitiesButton)
                AbilitiesButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void TacticsButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Tactic", TacticsButton);
        }

        private void TacticsButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != TacticsButton)
                TacticsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_down");
        }

        private void TacticsButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != TacticsButton)
                TacticsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void TacticsButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != TacticsButton)
                TacticsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
        }

        private void TacticsButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != TacticsButton)
                TacticsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void ItemsButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Item", ItemsButton);
        }

        private void ItemsButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != ItemsButton)
                ItemsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_down");
        }

        private void ItemsButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != ItemsButton)
                ItemsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void ItemsButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != ItemsButton)
                ItemsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
        }

        private void ItemsButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != ItemsButton)
                ItemsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void UnitsButton_Click(object sender, EventArgs e)
        {
            TraitFilterButtonClick("Unit", UnitsButton);
        }

        private void UnitsButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != UnitsButton)
                UnitsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_down");
        }

        private void UnitsButton_MouseEnter(object sender, EventArgs e)
        {
            if (LastClickedButton != UnitsButton)
                UnitsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void UnitsButton_MouseLeave(object sender, EventArgs e)
        {
            if (LastClickedButton != UnitsButton)
                UnitsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic");
        }

        private void UnitsButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (LastClickedButton != UnitsButton)
                UnitsButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_generic_over");
        }

        private void ShowValidQuestsButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void RemoveAllFiltersButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            ResetFilterButtons();
            LastClickedButton = null;

            BottomCardPanel.Controls.Clear();

            if (DeckCardIds.Count > 0)
            {
                CardData CardData;
                Card Card;

                foreach (int Id in DeckCardIds.Keys)
                {
                    CardData = TCGData.Cards.GetCardData(Id);
                    Card = new Card(CardData, BuilderSize);
                    Card.SetQuantityLabel(DeckCardIds[Id]);

                    BottomCardPanel.Controls.Add(Card);
                }
            }
        }

        private void StandardButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (DropDownMenu == null || DropDownMenu.IsDisposed)
            {
                LastClickedButton = SearchButton;
                DropDownMenu = new DropDownMenu(new string[] { "Title", "Keywords", "Game Text", "Artist", "Lore" }, SearchOptions)
                {
                    Location = new Point(SearchButton.Location.X, SearchButton.Location.Y + SearchButton.Height)
                };
                Controls.Add(DropDownMenu);
                return;
            }
            DropDownMenu.Dispose();
        }

        private void HelperButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void HelperButton_MouseDown(object sender, MouseEventArgs e)
        {
            HelperButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_db_wizard_down");
        }

        private void HelperButton_MouseEnter(object sender, EventArgs e)
        {
            HelperButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_db_wizard_over");
        }

        private void HelperButton_MouseLeave(object sender, EventArgs e)
        {
            HelperButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_db_wizard");
        }

        private void HelperButton_MouseUp(object sender, MouseEventArgs e)
        {
            HelperButton.BackgroundImage = ResourcesLib.GetImageFromButtons("button_db_wizard_over");
        }

        private void NewDeckButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            DeckCardIds.Clear();
            BottomCardPanel.Controls.Clear();

            LoadCards();
            UpdateDeckStatistics();
        }

        private void OpenDeckButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Controls.Add(new Window(new OpenDeck(this), "Open Deck"));
        }

        private void OpenDeckButton_MouseLeave(object sender, EventArgs e)
        {
            ButtonAnimation.FormatButton(OpenDeckButton);
        }

        private void SaveDeckButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            Controls.Add(new Window(new SaveDeck(DeckCardIds), "Save Deck"));
        }

        private void ToolsButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (DropDownMenu == null || DropDownMenu.IsDisposed)
            {
                LastClickedButton = ToolsButton;
                DropDownMenu = new DropDownMenu(new string[] { "Validate", "Statistics", "Test Draw", "Create Avatar", "Deck Building Helper" })
                {
                    Location = new Point(BottomPanel.Location.X + ToolsButton.Location.X, BottomPanel.Location.Y + ToolsButton.Location.Y + ToolsButton.Height)
                };
                Controls.Add(DropDownMenu);
                return;
            }
            DropDownMenu.Dispose();
        }

        public void DropDownSelected(int Index)
        {
            if (LastClickedButton == ToolsButton)
            {
                switch (Index)
                {
                    case 0:
                        if (DeckCardIds.Count > 0)
                            Controls.Add(new Window(new Validation(DeckCardIds), "Validation"));
                        break;
                    case 1:
                        CardData[] CardData = new CardData[BottomCardPanel.Controls.Count];

                        for (int i = 0; i < BottomCardPanel.Controls.Count; i++)
                            CardData[i] = ((Card)BottomCardPanel.Controls[i]).GetCardData();
                        Controls.Add(new Window(new DeckStatistics(CardData), "Deck Statistics"));
                        break;
                    case 2:
                        if (BottomCardPanel.Controls.Count < 6)
                            return;

                        List<int> TotalCardIds = new List<int>();

                        foreach (int CardId in DeckCardIds.Keys)
                        {
                            int quantity = DeckCardIds[CardId];

                            for (int i = 0; i < quantity; i++)
                                TotalCardIds.Add(CardId);
                        }
                        Controls.Add(new Window(new TestDraw(TotalCardIds.ToArray()), "Test Draw"));
                        break;
                    case 3:
                        Controls.Add(new Window(new AvatarCreator(), "Avatar Creator"));
                        break;
                    case 4:
                        break;
                }
            }
            else if (LastClickedButton == SearchButton)
            {
                DropDownMenu.ChangeCheck(Index);
                SearchOptions[Index] = !SearchOptions[Index];
                LoadCards();
            }
            else if (LastClickedButton == FilterButton)
            {
                FilterPanel.DropDownSelected(Index);
            }
        }

        public void LoadCards()
        {
            TopCardPanel.Controls.Clear();

            int[] CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedCardIds(MyCollection) : TCGData.Cards.GetCardIds();

            // Filter the cards
            int[] FilteredCards = FilterPanel.FilterCardIds(CardIds);

            // Check keywords
            FilteredCards = TCGData.Cards.GetCardIdsThatContainsKeyword(SearchTextBox.Text, ShowOnlyMyCards, FilteredCards, SearchOptions);

            // Adding cards to the panel puts them at the top, not the bottom.
            // Therefore to keep cards in alphebetical order, we must reverse the order of the array.
            Array.Reverse(FilteredCards);

            CardData CardData;

            if (CollectionViewOption.IsGraphicView())
            {
                int CardsShown = CurrentPage * CARDS_PER_PAGE;
                int CardsLeftToShow = FilteredCards.Length - CardsShown;
                int NumberOfCardsToShow = CardsLeftToShow >= CARDS_PER_PAGE ? CARDS_PER_PAGE : CardsLeftToShow;

                Card Card;

                for (int i = 0; i < NumberOfCardsToShow; i++)
                {
                    CardData = TCGData.Cards.GetCardData(FilteredCards[i + CardsShown]);
                    Card = new Card(CardData, CollectionSize);
                    Card.MouseHover += CoverPanel_MouseHover;
                    Card.MouseLeave += CoverPanel_MouseLeave;

                    int Quantity = CollectionService.GetNumberOfSpecficCard(FilteredCards[i + CardsShown], MyCollection);
                    Card DeckCard = GetCardInPanel(FilteredCards[i + CardsShown], BottomCardPanel);

                    if (DeckCard != null)
                    {
                        Quantity -= DeckCard.GetQuantity();
                    }

                    Card.SetQuantityLabel(Quantity);

                    if (Quantity != 0)
                        TopCardPanel.Controls.Add(Card);
                }
            }
            else
            {
                // Clear CollectionTableView items before adding new ones
                CollectionTableView.Items.Clear();

                // Add all cards (master table) to list view
                foreach (int CardId in FilteredCards)
                {
                    int Quantity = CollectionService.GetNumberOfSpecficCard(CardId, MyCollection);
                    Card DeckCard = GetCardInPanel(CardId, BottomCardPanel);

                    if (DeckCard != null)
                    {
                        Quantity -= DeckCard.GetQuantity();
                    }

                    if (Quantity != 0)
                    {
                        // Add it to the list
                        CardData = TCGData.Cards.GetCardData(CardId);
                        string[] Row = { CardData.GetTitle(), CardData.GetTraits(), CardData.GetCost().ToString(), CardData.GetArchetype(), CardData.GetCollectorInfo(), CardData.GetAttack().ToString(), CardData.GetDefense().ToString(), CardData.GetBonus().ToString(), CardData.GetHealthOrLevel().ToString() };
                        ListViewItem ListViewItem = new ListViewItem(Row);
                        CollectionTableView.Items.Add(ListViewItem);
                    }
                }

                // Resize each column to fit
                foreach (ColumnHeader Header in CollectionTableView.Columns)
                    Header.Width = -2;
            }
        }

        public void OpenDeck(SortedDictionary<int, int> DeckCardIds)
        {
            this.DeckCardIds = DeckCardIds;

            BottomCardPanel.Controls.Clear();

            LoadCards();

            Card Card;

            foreach (int Id in DeckCardIds.Keys)
            {
                Card = GetCardFromPanel(Id, TopCardPanel);

                if (Card == null)
                {
                    // Card is not on the top panel, we must create it.
                    CardData CardData = TCGData.Cards.GetCardData(Id);
                    Card = new Card(CardData, BuilderSize);
                }

                TransferCardToPanel(Card, DeckCardIds[Id], TopCardPanel, BottomCardPanel);
            }

            UpdateDeckStatistics();
        }

        private void UpdateDeckStatistics()
        {
            int Avatars = 0, Quests = 0, Abilities = 0, Tactics = 0, Items = 0, Units = 0;

            int Quantity;
            CardData CardData;

            foreach (int CardId in DeckCardIds.Keys)
            {
                Quantity = DeckCardIds[CardId];
                CardData = TCGData.Cards.GetCardData(CardId);

                switch (CardData.GetType())
                {
                    case "Avatar":
                        Avatars += Quantity;
                        break;
                    case "Quest":
                        Quests += Quantity;
                        break;
                    case "Ability":
                        Abilities += Quantity;
                        break;
                    case "Tactic":
                        Tactics += Quantity;
                        break;
                    case "Item":
                        Items += Quantity;
                        break;
                    case "Unit":
                        Units += Quantity;
                        break;
                }
            }

            AvatarButton.Text = "Avatar (" + Avatars + ")";
            QuestsButton.Text = "Quests (" + Quests + ")";

            int DrawDeckTotal = Abilities + Tactics + Items + Units;

            DrawDeckButton.Text = "Draw Deck (" + DrawDeckTotal + ")";
            AbilitiesButton.Text = "Abilities (" + Abilities + ") " + GetStringValue(DrawDeckTotal, Abilities);
            TacticsButton.Text = "Tactics (" + Tactics + ") " + GetStringValue(DrawDeckTotal, Tactics);
            ItemsButton.Text = "Items (" + Items + ") " + GetStringValue(DrawDeckTotal, Items);
            UnitsButton.Text = "Units (" + Units + ") " + GetStringValue(DrawDeckTotal, Units);
        }

        public static string GetStringValue(int Total, int Cards)
        {
            string Value = "0";

            if (Total > 0)
            {
                string X = (((float)Cards / Total) * 100).ToString();
                Value = X.Substring(0, X.Contains(".") ? X.IndexOf('.') : X.Length);
            }

            return Value + "%";
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            LoadCards();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Image BackgroundImageMain = ResourcesLib.GetImageFromBorders("navframe_header");
            base.OnPaintBackground(e);
            Rectangle rc = new Rectangle(0, 0, Width, Height);
            e.Graphics.DrawImage(BackgroundImageMain, rc);
        }

        private void NextPageButton_Click(object sender, EventArgs e)
        {
            if (CurrentPage < MaxPage)
            {
                CurrentPage++;
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
                SoundService.PlaySound("page_flip");
                LoadCards();

                if (CurrentPage == MaxPage)
                    NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_disabled");
            }
        }

        private void NextPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (CurrentPage < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_down");
        }

        private void NextPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (CurrentPage < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void NextPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (CurrentPage < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
        }

        private void NextPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (CurrentPage < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void PreviousPageButton_Click(object sender, EventArgs e)
        {
            if (CurrentPage > 0)
            {
                if (--CurrentPage == 0)
                    PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
                SoundService.PlaySound("page_flip");
                LoadCards();
            }
        }

        private void PreviousPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (CurrentPage > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_down");
        }

        private void PreviousPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (CurrentPage > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void PreviousPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (CurrentPage > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
        }

        private void PreviousPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (CurrentPage > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        public void UpdateView()
        {
            if (CollectionViewOption.GetNeedsUpdating())
            {
                LoadCards();

                CollectionTableView.Visible = !CollectionTableView.Visible;
                TopCardPanel.Visible = !TopCardPanel.Visible;
                NextPageButton.Visible = !NextPageButton.Visible;
                PreviousPageButton.Visible = !PreviousPageButton.Visible;
            }

            if (DeckViewOption.GetNeedsUpdating())
            {
                if (!DeckViewOption.IsGraphicView())
                {
                    ReloadDeckTableViewItems();
                }
                DeckTableView.Visible = !DeckTableView.Visible;
                BottomCardPanel.Visible = !BottomCardPanel.Visible;
            }
        }

        private void ReloadDeckTableViewItems()
        {
            // Clear CollectionTableView items before adding new ones
            DeckTableView.Items.Clear();

            // Add all cards (master table) to list view
            foreach (object Card in BottomCardPanel.Controls)
            {
                Card CardObject = (Card)Card;
                CardData CardData = CardObject.GetCardData();
                string[] Row = { CardData.GetTitle(), CardData.GetType(), CardData.GetCost().ToString(), CardData.GetArchetype(), CardData.GetCollectorInfo(), CardData.GetAttack().ToString(), CardData.GetDefense().ToString(), CardData.GetBonus().ToString(), CardData.GetHealthOrLevel().ToString() };
                ListViewItem ListViewItem = new ListViewItem(Row);
                DeckTableView.Items.Add(ListViewItem);
            }

            // Resize each column to fit
            foreach (ColumnHeader Header in DeckTableView.Columns)
                Header.Width = -2;
        }
    }
}
