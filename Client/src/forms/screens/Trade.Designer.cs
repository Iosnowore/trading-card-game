﻿namespace TradingCardGame.Src.Forms.Screens
{
    partial class Trade
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.TheyGetPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.YouGetPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ExitButton = new System.Windows.Forms.Button();
            this.AcceptButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.YouGetAcceptButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.RightPanel = new System.Windows.Forms.Panel();
            this.TheirPreviousPageButton = new System.Windows.Forms.Button();
            this.PreviousPageButton = new System.Windows.Forms.Button();
            this.NextPageButton = new System.Windows.Forms.Button();
            this.TheirNextPageButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.GuildCheckBox = new System.Windows.Forms.Button();
            this.PlayerTextBox = new System.Windows.Forms.TextBox();
            this.JumpToButton = new System.Windows.Forms.Button();
            this.filterButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.PlayerCheckBox = new System.Windows.Forms.Button();
            this.CollectionPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.TheirCollectionPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label61 = new System.Windows.Forms.Label();
            this.InputChatBox = new System.Windows.Forms.TextBox();
            this.ChatBox = new System.Windows.Forms.ListBox();
            this.TableviewTable = new System.Windows.Forms.PictureBox();
            this.PlayersListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TableviewGraphic = new System.Windows.Forms.PictureBox();
            this.LeftPanel.SuspendLayout();
            this.RightPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableviewTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableviewGraphic)).BeginInit();
            this.SuspendLayout();
            // 
            // LeftPanel
            // 
            this.LeftPanel.Controls.Add(this.TheyGetPanel);
            this.LeftPanel.Controls.Add(this.YouGetPanel);
            this.LeftPanel.Controls.Add(this.ExitButton);
            this.LeftPanel.Controls.Add(this.AcceptButton);
            this.LeftPanel.Controls.Add(this.ClearButton);
            this.LeftPanel.Controls.Add(this.label3);
            this.LeftPanel.Controls.Add(this.YouGetAcceptButton);
            this.LeftPanel.Controls.Add(this.label2);
            this.LeftPanel.Location = new System.Drawing.Point(3, 3);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(215, 575);
            this.LeftPanel.TabIndex = 0;
            // 
            // TheyGetPanel
            // 
            this.TheyGetPanel.AutoScroll = true;
            this.TheyGetPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TheyGetPanel.Location = new System.Drawing.Point(5, 307);
            this.TheyGetPanel.Name = "TheyGetPanel";
            this.TheyGetPanel.Size = new System.Drawing.Size(205, 200);
            this.TheyGetPanel.TabIndex = 65;
            // 
            // YouGetPanel
            // 
            this.YouGetPanel.AutoScroll = true;
            this.YouGetPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.YouGetPanel.Location = new System.Drawing.Point(5, 47);
            this.YouGetPanel.Name = "YouGetPanel";
            this.YouGetPanel.Size = new System.Drawing.Size(205, 200);
            this.YouGetPanel.TabIndex = 64;
            // 
            // ExitButton
            // 
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.FlatAppearance.BorderSize = 0;
            this.ExitButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitButton.Location = new System.Drawing.Point(65, 545);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(85, 27);
            this.ExitButton.TabIndex = 8;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // AcceptButton
            // 
            this.AcceptButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AcceptButton.FlatAppearance.BorderSize = 0;
            this.AcceptButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AcceptButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AcceptButton.Location = new System.Drawing.Point(125, 512);
            this.AcceptButton.Name = "AcceptButton";
            this.AcceptButton.Size = new System.Drawing.Size(85, 27);
            this.AcceptButton.TabIndex = 7;
            this.AcceptButton.Text = "Accept";
            this.AcceptButton.UseVisualStyleBackColor = true;
            this.AcceptButton.Click += new System.EventHandler(this.AcceptButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClearButton.FlatAppearance.BorderSize = 0;
            this.ClearButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearButton.Location = new System.Drawing.Point(7, 512);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(85, 27);
            this.ClearButton.TabIndex = 6;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SkyBlue;
            this.label3.Location = new System.Drawing.Point(5, 284);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "They get:";
            // 
            // YouGetAcceptButton
            // 
            this.YouGetAcceptButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.YouGetAcceptButton.FlatAppearance.BorderSize = 0;
            this.YouGetAcceptButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.YouGetAcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.YouGetAcceptButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YouGetAcceptButton.Location = new System.Drawing.Point(9, 253);
            this.YouGetAcceptButton.Name = "YouGetAcceptButton";
            this.YouGetAcceptButton.Size = new System.Drawing.Size(201, 27);
            this.YouGetAcceptButton.TabIndex = 3;
            this.YouGetAcceptButton.Text = "Pending";
            this.YouGetAcceptButton.UseVisualStyleBackColor = true;
            this.YouGetAcceptButton.Click += new System.EventHandler(this.YouGetAcceptButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(5, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "You get:";
            // 
            // RightPanel
            // 
            this.RightPanel.Controls.Add(this.TheirPreviousPageButton);
            this.RightPanel.Controls.Add(this.PreviousPageButton);
            this.RightPanel.Controls.Add(this.NextPageButton);
            this.RightPanel.Controls.Add(this.TheirNextPageButton);
            this.RightPanel.Controls.Add(this.SearchButton);
            this.RightPanel.Controls.Add(this.textBox1);
            this.RightPanel.Controls.Add(this.label4);
            this.RightPanel.Controls.Add(this.SearchTextBox);
            this.RightPanel.Controls.Add(this.GuildCheckBox);
            this.RightPanel.Controls.Add(this.PlayerTextBox);
            this.RightPanel.Controls.Add(this.JumpToButton);
            this.RightPanel.Controls.Add(this.filterButton);
            this.RightPanel.Controls.Add(this.label1);
            this.RightPanel.Controls.Add(this.PlayerCheckBox);
            this.RightPanel.Controls.Add(this.CollectionPanel);
            this.RightPanel.Controls.Add(this.TheirCollectionPanel);
            this.RightPanel.Controls.Add(this.label61);
            this.RightPanel.Controls.Add(this.InputChatBox);
            this.RightPanel.Controls.Add(this.ChatBox);
            this.RightPanel.Controls.Add(this.TableviewTable);
            this.RightPanel.Controls.Add(this.PlayersListView);
            this.RightPanel.Controls.Add(this.TableviewGraphic);
            this.RightPanel.Location = new System.Drawing.Point(224, 3);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(673, 575);
            this.RightPanel.TabIndex = 1;
            // 
            // TheirPreviousPageButton
            // 
            this.TheirPreviousPageButton.FlatAppearance.BorderSize = 0;
            this.TheirPreviousPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TheirPreviousPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TheirPreviousPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TheirPreviousPageButton.Location = new System.Drawing.Point(638, 135);
            this.TheirPreviousPageButton.Name = "TheirPreviousPageButton";
            this.TheirPreviousPageButton.Size = new System.Drawing.Size(25, 51);
            this.TheirPreviousPageButton.TabIndex = 81;
            this.TheirPreviousPageButton.UseVisualStyleBackColor = true;
            this.TheirPreviousPageButton.Click += new System.EventHandler(this.TheirPreviousPageButton_Click);
            this.TheirPreviousPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TheirPreviousPageButton_MouseDown);
            this.TheirPreviousPageButton.MouseEnter += new System.EventHandler(this.TheirPreviousPageButton_MouseEnter);
            this.TheirPreviousPageButton.MouseLeave += new System.EventHandler(this.TheirPreviousPageButton_MouseLeave);
            this.TheirPreviousPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TheirPreviousPageButton_MouseUp);
            // 
            // PreviousPageButton
            // 
            this.PreviousPageButton.FlatAppearance.BorderSize = 0;
            this.PreviousPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviousPageButton.Location = new System.Drawing.Point(638, 285);
            this.PreviousPageButton.Name = "PreviousPageButton";
            this.PreviousPageButton.Size = new System.Drawing.Size(25, 51);
            this.PreviousPageButton.TabIndex = 80;
            this.PreviousPageButton.UseVisualStyleBackColor = true;
            this.PreviousPageButton.Click += new System.EventHandler(this.PreviousPageButton_Click);
            this.PreviousPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseDown);
            this.PreviousPageButton.MouseEnter += new System.EventHandler(this.PreviousPageButton_MouseEnter);
            this.PreviousPageButton.MouseLeave += new System.EventHandler(this.PreviousPageButton_MouseLeave);
            this.PreviousPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseUp);
            // 
            // NextPageButton
            // 
            this.NextPageButton.FlatAppearance.BorderSize = 0;
            this.NextPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextPageButton.Location = new System.Drawing.Point(638, 228);
            this.NextPageButton.Name = "NextPageButton";
            this.NextPageButton.Size = new System.Drawing.Size(25, 51);
            this.NextPageButton.TabIndex = 79;
            this.NextPageButton.UseVisualStyleBackColor = true;
            this.NextPageButton.Click += new System.EventHandler(this.NextPageButton_Click);
            this.NextPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseDown);
            this.NextPageButton.MouseEnter += new System.EventHandler(this.NextPageButton_MouseEnter);
            this.NextPageButton.MouseLeave += new System.EventHandler(this.NextPageButton_MouseLeave);
            this.NextPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseUp);
            // 
            // TheirNextPageButton
            // 
            this.TheirNextPageButton.FlatAppearance.BorderSize = 0;
            this.TheirNextPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TheirNextPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TheirNextPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TheirNextPageButton.Location = new System.Drawing.Point(638, 78);
            this.TheirNextPageButton.Name = "TheirNextPageButton";
            this.TheirNextPageButton.Size = new System.Drawing.Size(25, 51);
            this.TheirNextPageButton.TabIndex = 78;
            this.TheirNextPageButton.UseVisualStyleBackColor = true;
            this.TheirNextPageButton.Click += new System.EventHandler(this.TheirNextPageButton_Click);
            this.TheirNextPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TheirNextPageButton_MouseDown);
            this.TheirNextPageButton.MouseEnter += new System.EventHandler(this.TheirNextPageButton_MouseEnter);
            this.TheirNextPageButton.MouseLeave += new System.EventHandler(this.TheirNextPageButton_MouseLeave);
            this.TheirNextPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TheirNextPageButton_MouseUp);
            // 
            // SearchButton
            // 
            this.SearchButton.BackColor = System.Drawing.Color.Transparent;
            this.SearchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SearchButton.FlatAppearance.BorderSize = 0;
            this.SearchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.ForeColor = System.Drawing.Color.Black;
            this.SearchButton.Location = new System.Drawing.Point(386, 26);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(111, 29);
            this.SearchButton.TabIndex = 77;
            this.SearchButton.Text = "Search ▼";
            this.SearchButton.UseVisualStyleBackColor = false;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.textBox1.Location = new System.Drawing.Point(543, 393);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(107, 22);
            this.textBox1.TabIndex = 69;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.SkyBlue;
            this.label4.Location = new System.Drawing.Point(367, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 16);
            this.label4.TabIndex = 68;
            this.label4.Text = "Only offer this trade to guild:";
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.BackColor = System.Drawing.Color.Black;
            this.SearchTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTextBox.ForeColor = System.Drawing.SystemColors.Control;
            this.SearchTextBox.Location = new System.Drawing.Point(503, 28);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(160, 22);
            this.SearchTextBox.TabIndex = 76;
            // 
            // GuildCheckBox
            // 
            this.GuildCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GuildCheckBox.Location = new System.Drawing.Point(338, 395);
            this.GuildCheckBox.Name = "GuildCheckBox";
            this.GuildCheckBox.Size = new System.Drawing.Size(23, 20);
            this.GuildCheckBox.TabIndex = 67;
            this.GuildCheckBox.UseVisualStyleBackColor = true;
            this.GuildCheckBox.Click += new System.EventHandler(this.GuildCheckBox_Click);
            // 
            // PlayerTextBox
            // 
            this.PlayerTextBox.BackColor = System.Drawing.Color.Black;
            this.PlayerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PlayerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerTextBox.ForeColor = System.Drawing.SystemColors.Control;
            this.PlayerTextBox.Location = new System.Drawing.Point(225, 393);
            this.PlayerTextBox.Name = "PlayerTextBox";
            this.PlayerTextBox.Size = new System.Drawing.Size(107, 22);
            this.PlayerTextBox.TabIndex = 66;
            // 
            // JumpToButton
            // 
            this.JumpToButton.BackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.JumpToButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.JumpToButton.FlatAppearance.BorderSize = 0;
            this.JumpToButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JumpToButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JumpToButton.ForeColor = System.Drawing.Color.Black;
            this.JumpToButton.Location = new System.Drawing.Point(106, 26);
            this.JumpToButton.Name = "JumpToButton";
            this.JumpToButton.Size = new System.Drawing.Size(130, 29);
            this.JumpToButton.TabIndex = 75;
            this.JumpToButton.Text = "Jump to... ▼";
            this.JumpToButton.UseVisualStyleBackColor = false;
            this.JumpToButton.Click += new System.EventHandler(this.JumpToButton_Click);
            // 
            // filterButton
            // 
            this.filterButton.BackColor = System.Drawing.Color.Transparent;
            this.filterButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.filterButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.filterButton.FlatAppearance.BorderSize = 0;
            this.filterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterButton.ForeColor = System.Drawing.Color.Black;
            this.filterButton.Location = new System.Drawing.Point(10, 26);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(90, 29);
            this.filterButton.TabIndex = 74;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = false;
            this.filterButton.Click += new System.EventHandler(this.FilterButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(40, 397);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 16);
            this.label1.TabIndex = 65;
            this.label1.Text = "Only offer this trade to player:";
            // 
            // PlayerCheckBox
            // 
            this.PlayerCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayerCheckBox.Location = new System.Drawing.Point(10, 395);
            this.PlayerCheckBox.Name = "PlayerCheckBox";
            this.PlayerCheckBox.Size = new System.Drawing.Size(23, 20);
            this.PlayerCheckBox.TabIndex = 64;
            this.PlayerCheckBox.UseVisualStyleBackColor = true;
            this.PlayerCheckBox.Click += new System.EventHandler(this.PlayerCheckBox_Click);
            // 
            // CollectionPanel
            // 
            this.CollectionPanel.AutoScroll = true;
            this.CollectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CollectionPanel.Location = new System.Drawing.Point(10, 228);
            this.CollectionPanel.Name = "CollectionPanel";
            this.CollectionPanel.Size = new System.Drawing.Size(622, 144);
            this.CollectionPanel.TabIndex = 63;
            // 
            // TheirCollectionPanel
            // 
            this.TheirCollectionPanel.AutoScroll = true;
            this.TheirCollectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TheirCollectionPanel.Location = new System.Drawing.Point(10, 78);
            this.TheirCollectionPanel.Name = "TheirCollectionPanel";
            this.TheirCollectionPanel.Size = new System.Drawing.Size(622, 144);
            this.TheirCollectionPanel.TabIndex = 62;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.ForeColor = System.Drawing.Color.SkyBlue;
            this.label61.Location = new System.Drawing.Point(316, 12);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(30, 13);
            this.label61.TabIndex = 72;
            this.label61.Text = "View";
            // 
            // InputChatBox
            // 
            this.InputChatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.InputChatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InputChatBox.ForeColor = System.Drawing.Color.White;
            this.InputChatBox.Location = new System.Drawing.Point(10, 550);
            this.InputChatBox.Name = "InputChatBox";
            this.InputChatBox.Size = new System.Drawing.Size(447, 22);
            this.InputChatBox.TabIndex = 7;
            this.InputChatBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputChatBox_KeyDown);
            // 
            // ChatBox
            // 
            this.ChatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.ChatBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.ChatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChatBox.ForeColor = System.Drawing.Color.White;
            this.ChatBox.FormattingEnabled = true;
            this.ChatBox.HorizontalScrollbar = true;
            this.ChatBox.ItemHeight = 16;
            this.ChatBox.Location = new System.Drawing.Point(10, 444);
            this.ChatBox.Name = "ChatBox";
            this.ChatBox.Size = new System.Drawing.Size(447, 100);
            this.ChatBox.TabIndex = 6;
            // 
            // TableviewTable
            // 
            this.TableviewTable.BackColor = System.Drawing.Color.Transparent;
            this.TableviewTable.Location = new System.Drawing.Point(308, 31);
            this.TableviewTable.Name = "TableviewTable";
            this.TableviewTable.Size = new System.Drawing.Size(24, 24);
            this.TableviewTable.TabIndex = 71;
            this.TableviewTable.TabStop = false;
            // 
            // PlayersListView
            // 
            this.PlayersListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.PlayersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.PlayersListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayersListView.ForeColor = System.Drawing.Color.White;
            this.PlayersListView.FullRowSelect = true;
            this.PlayersListView.HideSelection = false;
            this.PlayersListView.Location = new System.Drawing.Point(463, 444);
            this.PlayersListView.MultiSelect = false;
            this.PlayersListView.Name = "PlayersListView";
            this.PlayersListView.Size = new System.Drawing.Size(200, 128);
            this.PlayersListView.TabIndex = 3;
            this.PlayersListView.UseCompatibleStateImageBehavior = false;
            this.PlayersListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 196;
            // 
            // TableviewGraphic
            // 
            this.TableviewGraphic.BackColor = System.Drawing.Color.Transparent;
            this.TableviewGraphic.Location = new System.Drawing.Point(333, 31);
            this.TableviewGraphic.Name = "TableviewGraphic";
            this.TableviewGraphic.Size = new System.Drawing.Size(24, 24);
            this.TableviewGraphic.TabIndex = 70;
            this.TableviewGraphic.TabStop = false;
            // 
            // Trade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.RightPanel);
            this.Controls.Add(this.LeftPanel);
            this.DoubleBuffered = true;
            this.Name = "Trade";
            this.Size = new System.Drawing.Size(900, 581);
            this.LeftPanel.ResumeLayout(false);
            this.LeftPanel.PerformLayout();
            this.RightPanel.ResumeLayout(false);
            this.RightPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TableviewTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TableviewGraphic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel RightPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button YouGetAcceptButton;
        private System.Windows.Forms.Button AcceptButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ExitButton;
        public System.Windows.Forms.ListView PlayersListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TextBox InputChatBox;
        public System.Windows.Forms.ListBox ChatBox;
        private System.Windows.Forms.FlowLayoutPanel TheirCollectionPanel;
        private System.Windows.Forms.FlowLayoutPanel CollectionPanel;
        private System.Windows.Forms.FlowLayoutPanel YouGetPanel;
        private System.Windows.Forms.FlowLayoutPanel TheyGetPanel;
        private System.Windows.Forms.Button PlayerCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PlayerTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button GuildCheckBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button JumpToButton;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.PictureBox TableviewTable;
        private System.Windows.Forms.PictureBox TableviewGraphic;
        private System.Windows.Forms.Button TheirNextPageButton;
        private System.Windows.Forms.Button NextPageButton;
        private System.Windows.Forms.Button PreviousPageButton;
        private System.Windows.Forms.Button TheirPreviousPageButton;
    }
}
