﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Cards;
using Effects;
using Resources;
using String;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Components.Game;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.Src.Forms.Screens
{
    public partial class Game : UserControl
    {
        private readonly PlayerPanel PlayerPanel;
        private readonly PlayerPanel EnemyPanel;

        private Card Quest1;
        private Card Quest2;

        private Panel LastPlacedExpPanel;

        private readonly Chat Chat;

        private Card SelectedCard;
        private Card PreviewingCard;
        private Card EnlargedCard;
        private Card TacticCard;

        private readonly Timer HoverTimer;
        private readonly Timer CardHintAnimationTimer;
        private readonly Timer UnitMovementTimer;

        private bool PreviewOn;

        private readonly Card[] Hand;

        private readonly List<Card> CardsToDecollapse = new List<Card>();

        private readonly SortedDictionary<int, Card> CardsPlacedQuestOne = new SortedDictionary<int, Card>();
        private readonly SortedDictionary<int, Card> CardsPlacedQuestTwo = new SortedDictionary<int, Card>();

        private readonly SortedDictionary<int, Card> EnemyCardsPlacedQuestOne = new SortedDictionary<int, Card>();
        private readonly SortedDictionary<int, Card> EnemyCardsPlacedQuestTwo = new SortedDictionary<int, Card>();

        private readonly List<Card> CardHints = new List<Card>();

        private Mainbase Mainbase;

        private readonly Panel ExpBase;

        private readonly Mainbase EnemyMainBase;
        private readonly PhaseMeter PhaseMeter;

        private readonly Button LeftRaidButton;
        private readonly Button RightRaidButton;

        private readonly PictureBox EnemyQuest1LevelsCompleted;
        private readonly PictureBox PlayerQuest1LevelsCompleted;

        private readonly PictureBox EnemyQuest2LevelsCompleted;
        private readonly PictureBox PlayerQuest2LevelsCompleted;

        private readonly int ScenarioId;

        private int AttemptedQuestId;

        private const int HAND_CARD_LOC_Y = 730;
        private const int UNIT_MOVEMENT = 5;
        private const int UNIT_OFFDISTANCE = 25;

        private bool SentLeaveGamePacket;

        private bool CanChooseQuest;
        private bool AlreadyRedrawPrompted;

        private int OldQuestId = -1;
        private int NewQuestId = -1;

        private int MyCompletedMissions;
        private int EnemyCompltedMissions;

        private bool RaidActive;
        private int ActiveRaidQuestId = -1;

        private bool CanExertAvatar;
        private bool ExertedAvatar;

        private BattleAnimationPlayer AttackAnimationPlayer;
        private BattleAnimationPlayer DefenseAnimationPlayer;

        private EffectPlayer PendingQuestPlayer;

        private ScenarioDialog Dialog;

        public const int EXERT_ATTACK = 0;
        public const int EXERT_DAMAGE = 1;
        public const int EXERT_DEFENSE = 2;

        private int ExertType;

        private bool Won;

        public Game(int AvatarId, int EnemyAvatarId, string Username, string EnemyUsername, int[] HandCardIds, int QuestId, int EnemyQuestId, int CardsInHand, int EnemyCardsInHand, int CardsInDeck, int EnemyCardsInDeck, int ScenarioId)
        {
            InitializeComponent();
            HandleDestroyed += Game_HandleDestroyed;

            // Save the ScenarioId for future dialogs
            this.ScenarioId = ScenarioId;

            // Update Discord info
            DiscordPresenceService.UpdateDetails("Playing a Match");
            DiscordPresenceService.SetParty(string.Empty, 2, 2);
            DiscordPresenceService.UpdatePartyState(Username + " vs " + EnemyUsername);

            // Setup the timer objects 
            CardHintAnimationTimer = new Timer();
            CardHintAnimationTimer.Interval = 500;
            CardHintAnimationTimer.Tick += CardHintAnimationTimer_Tick;

            HoverTimer = new Timer();
            HoverTimer.Tick += Timer_Tick;
            HoverTimer.Interval = 15;

            UnitMovementTimer = new Timer();
            UnitMovementTimer.Interval = 30;
            UnitMovementTimer.Tick += UnitMovementTimer_Tick;

            Hand = new Card[CardsInHand];

            int Background = SettingsService.GetIntFromSettingsXml("GamePlaymat", "BackgroundImage") + 1;
            string BackgroundString = "game_bg_";

            if (Background < 10)
                BackgroundString += "0";
            BackgroundString += Background;

            BackgroundImage = ResourcesLib.GetImageFromBackgrounds(BackgroundString);

            CardData EnemyAvatarCardData = TCGData.Cards.GetCardData(EnemyAvatarId);
            CardData AvatarCardData = TCGData.Cards.GetCardData(AvatarId);

            int Health = EnemyAvatarCardData.GetHealthOrLevel();
            int Attack = EnemyAvatarCardData.GetAttack();
            int Defense = EnemyAvatarCardData.GetDefense();
            int BonusDmg = EnemyAvatarCardData.GetBonus();
            string Archetype = EnemyAvatarCardData.GetArchetype();

            // Imperial art is marked as 'empire' for some reason
            if (Archetype.Equals("imperial"))
                Archetype = "empire";

            char SeriesNumber = EnemyAvatarCardData.GetCollectorInfo()[0];
            string AvatarThumbnailImage = "thumb_set" + SeriesNumber + "_" + EnemyAvatarCardData.GetTitle().ToLower().Replace(" ", "_");

            EnemyPanel = new PlayerPanel(EnemyUsername, EnemyAvatarId, Archetype, AvatarThumbnailImage, Health, Attack, Defense, BonusDmg, EnemyCardsInHand, EnemyCardsInDeck, "top")
            {
                Location = new Point(0, 0)
            };
            Controls.Add(EnemyPanel);

            Health = AvatarCardData.GetHealthOrLevel();
            Attack = AvatarCardData.GetAttack();
            Defense = AvatarCardData.GetDefense();
            BonusDmg = AvatarCardData.GetBonus();
            Archetype = AvatarCardData.GetArchetype();

            // Imperial art is marked as 'empire' for some reason
            if (Archetype.Equals("imperial"))
                Archetype = "empire";

            SeriesNumber = AvatarCardData.GetCollectorInfo()[0];
            AvatarThumbnailImage = "thumb_set" + SeriesNumber + "_" + AvatarCardData.GetTitle().ToLower().Replace(" ", "_");

            PlayerPanel = new PlayerPanel(Username, AvatarId, Archetype, AvatarThumbnailImage, Health, Attack, Defense, BonusDmg, CardsInHand, CardsInDeck, "bottom");
            PlayerPanel.Location = new Point(0, Height - PlayerPanel.Height - 40);
            PlayerPanel.MouseMove += MoveSelectedCard;
            Controls.Add(PlayerPanel);

            PhaseMeter = new PhaseMeter();
            PhaseMeter.Location = new Point(PlayerPanel.Location.X + 20, PlayerPanel.Location.Y - PhaseMeter.Height / 5);
            Controls.Add(PhaseMeter);
            PhaseMeter.BringToFront();

            Chat = new Chat();
            Chat.Location = new Point(Width - Chat.Width, PlayerPanel.Location.Y - Chat.Height / 3);
            Controls.Add(Chat);
            Chat.BringToFront();

            ExpBase = new Panel
            {
                BackColor = Color.Transparent,
                Size = new Size(175, 203)
            };
            ExpBase.Location = new Point((Width - ExpBase.Width) / 2, (Height - ExpBase.Height) / 2);
            ExpBase.BackgroundImage = ResourcesLib.GetImageFromGamescreen("exp_base");
            ExpBase.MouseMove += MoveSelectedCard;
            Controls.Add(ExpBase);

            // Top Divider
            PictureBox ExpBaseTileTop = new PictureBox
            {
                BackColor = Color.Transparent,
                Size = new Size(175, Height / 4)
            };
            ExpBaseTileTop.Location = new Point((Width - ExpBaseTileTop.Width) / 2, EnemyPanel.Location.Y + 130);
            ExpBaseTileTop.BackgroundImage = ResourcesLib.GetImageFromGamescreen("exp_base_tile");
            ExpBaseTileTop.MouseMove += MoveSelectedCard;
            Controls.Add(ExpBaseTileTop);
            ExpBaseTileTop.BringToFront();

            // Bottom Divider
            PictureBox ExpBaseTileBottom = new PictureBox
            {
                BackColor = Color.Transparent,
                Size = new Size(175, Height / 5 + 10)
            };
            ExpBaseTileBottom.Location = new Point((Width - ExpBaseTileBottom.Width) / 2, ExpBase.Location.Y + 160);
            ExpBaseTileBottom.BackgroundImage = ResourcesLib.GetImageFromGamescreen("exp_base_tile");
            ExpBaseTileBottom.MouseMove += MoveSelectedCard;
            Controls.Add(ExpBaseTileBottom);
            ExpBaseTileBottom.BringToFront();

            // Left Divider
            PictureBox ExpBaseTileRotLeft = new PictureBox
            {
                BackColor = Color.Transparent,
                Size = new Size(Width / 3 + 110, 22)
            };
            ExpBaseTileRotLeft.Location = new Point(0, (Height - ExpBaseTileRotLeft.Height) / 2);
            ExpBaseTileRotLeft.BackgroundImage = ResourcesLib.GetImageFromGamescreen("exp_midline");
            ExpBaseTileRotLeft.MouseMove += MoveSelectedCard;
            Controls.Add(ExpBaseTileRotLeft);
            ExpBaseTileRotLeft.BringToFront();

            // Right Divider
            PictureBox ExpBaseTileRotRight = new PictureBox
            {
                BackColor = Color.Transparent,
                Size = new Size(Width / 2, 22)
            };
            ExpBaseTileRotRight.Location = new Point(ExpBase.Location.X + 147, (Height - ExpBaseTileRotRight.Height) / 2);
            ExpBaseTileRotRight.BackgroundImage = ResourcesLib.GetImageFromGamescreen("exp_midline");
            ExpBaseTileRotRight.MouseMove += MoveSelectedCard;
            Controls.Add(ExpBaseTileRotRight);
            ExpBaseTileRotRight.BringToFront();

            Quest1 = new Card(TCGData.Cards.GetCardData(QuestId), Card.SIZE.PLAYMAT, true);
            Quest1.Location = new Point(Quest1.Width / 4, (Height - Quest1.Height) / 2);
            Controls.Add(Quest1);
            Quest1.BringToFront();

            Quest2 = new Card(TCGData.Cards.GetCardData(EnemyQuestId), Card.SIZE.PLAYMAT, true);
            Quest2.Location = new Point(Width - Quest2.Width - 10, (Height - Quest2.Height) / 2);
            Controls.Add(Quest2);
            Quest2.BringToFront();

            EnemyMainBase = new Mainbase(Mainbase.PHASE.OPPONENT_WAIT);
            EnemyMainBase.SetText("Please wait while " + EnemyPanel.GetUsername() + " completes the action.");
            EnemyMainBase.Location = new Point((Width - EnemyMainBase.Width) / 2, EnemyPanel.Location.Y + EnemyMainBase.Height);
            EnemyMainBase.SetEnabled(false);
            Controls.Add(EnemyMainBase);
            EnemyMainBase.BringToFront();

            LeftRaidButton = new Button
            {
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent,
                Size = new Size(50, 50),
                Location = new Point(Quest1.Location.X + Quest1.Width, Quest1.Location.Y + (Quest1.Height / 2) - (50 / 2)),
                BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_disabled"),
                Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_disabled"),
                BackgroundImageLayout = ImageLayout.Stretch
            };
            LeftRaidButton.FlatAppearance.BorderSize = 0;
            LeftRaidButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            LeftRaidButton.FlatAppearance.MouseOverBackColor = Color.Transparent;

            LeftRaidButton.MouseEnter += RaidButton_MouseEnter;
            LeftRaidButton.MouseLeave += RaidButton_MouseLeave;
            LeftRaidButton.MouseDown += RaidButton_MouseDown;
            LeftRaidButton.Click += RaidButton_Click;
            Controls.Add(LeftRaidButton);
            LeftRaidButton.BringToFront();

            RightRaidButton = new Button
            {
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent,
                Size = new Size(50, 50),
                Location = new Point(Quest2.Location.X - 50, Quest2.Location.Y + (Quest2.Height / 2) - (50 / 2)),
                BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_disabled"),
                Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_disabled"),
                BackgroundImageLayout = ImageLayout.Stretch
            };
            RightRaidButton.FlatAppearance.BorderSize = 0;
            RightRaidButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            RightRaidButton.FlatAppearance.MouseOverBackColor = Color.Transparent;

            RightRaidButton.MouseEnter += RaidButton_MouseEnter;
            RightRaidButton.MouseLeave += RaidButton_MouseLeave;
            RightRaidButton.MouseDown += RaidButton_MouseDown;
            RightRaidButton.Click += RaidButton_Click;
            Controls.Add(RightRaidButton);
            RightRaidButton.BringToFront();

            EnemyQuest1LevelsCompleted = new PictureBox
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackgroundImageLayout = ImageLayout.Stretch,
                BackgroundImage = EffectsLib.GetImageFromGamescreen("level_red_00"),
                BackColor = Color.Transparent,
                Size = new Size(30, 30),
                Location = new Point(Quest1.Location.X, Quest1.Location.Y - 30)
            };
            Controls.Add(EnemyQuest1LevelsCompleted);
            EnemyQuest1LevelsCompleted.BringToFront();

            PlayerQuest1LevelsCompleted = new PictureBox
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackgroundImageLayout = ImageLayout.Stretch,
                BackgroundImage = EffectsLib.GetImageFromGamescreen("level_green_00"),
                BackColor = Color.Transparent,
                Size = new Size(30, 30),
                Location = new Point(Quest1.Location.X, Quest1.Location.Y + Quest1.Height)
            };

            Controls.Add(PlayerQuest1LevelsCompleted);
            PlayerQuest1LevelsCompleted.BringToFront();

            EnemyQuest2LevelsCompleted = new PictureBox
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackgroundImageLayout = ImageLayout.Stretch,
                BackgroundImage = EffectsLib.GetImageFromGamescreen("level_red_00"),
                BackColor = Color.Transparent,
                Size = new Size(30, 30),
                Location = new Point(Quest2.Location.X, Quest2.Location.Y - 30)
            };

            Controls.Add(EnemyQuest2LevelsCompleted);
            EnemyQuest2LevelsCompleted.BringToFront();

            PlayerQuest2LevelsCompleted = new PictureBox
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackgroundImageLayout = ImageLayout.Stretch,
                BackgroundImage = EffectsLib.GetImageFromGamescreen("level_green_00"),
                BackColor = Color.Transparent,
                Size = new Size(30, 30),
                Location = new Point(Quest2.Location.X, Quest2.Location.Y + Quest2.Height)
            };

            Controls.Add(PlayerQuest2LevelsCompleted);
            PlayerQuest2LevelsCompleted.BringToFront();

            CardData[] CardData = new CardData[Hand.Length];

            for (int i = 0; i < CardData.Length; i++)
            {
                CardData[i] = TCGData.Cards.GetCardData(HandCardIds[i]);
            }

            OrderHand(CardData);

            string Output = "Beginning turn 1.";
            Chat.AddText(Output);

            Output = "You draw " + CardData[0].GetTitle() + ", " + CardData[1].GetTitle() + ", " + CardData[2].GetTitle() + ", " + CardData[3].GetTitle() + ", " + CardData[4].GetTitle() + " and " + CardData[5].GetTitle();
            Chat.AddText(Output);

            Output = EnemyUsername + " draws 6 cards.";
            Chat.AddText(Output);

            // Show dialog
            string Text = "Scenario 1: " + StringLib.GetString("scenarios:title_" + ScenarioId) + "\n\n" + StringLib.GetString("scenarios:dialog_start_" + ScenarioId);
            Dialog = new ScenarioDialog(Text, null);
            Dialog.Location = new Point((Width - Dialog.Width) / 2, (Height - Dialog.Height) / 2);
            Controls.Add(Dialog);
            Dialog.BringToFront();
        }

        private void Game_HandleDestroyed(object sender, System.EventArgs e)
        {
            if (!SentLeaveGamePacket)
                PacketService.SendPacket(Commands.LEAVE_GAME, null);
        }

        private void RaidButton_Click(object sender, System.EventArgs e)
        {
            if (ActiveRaidQuestId != -1 || PhaseMeter.GetPhase() != Mainbase.PHASE.MAIN || Mainbase == null)
                return;

            Button RaidButton = (Button)sender;

            int QuestId = RaidButton == LeftRaidButton ? Quest1.GetId() : Quest2.GetId();

            if (!RaidActive && (QuestId == Quest1.GetId() && CardsPlacedQuestOne.Count > 0 || QuestId == Quest2.GetId() && CardsPlacedQuestTwo.Count > 0))
            {
                // Send packet
                PacketService.SendPacket(Commands.START_RAID, new CommandPacket(new object[] { QuestId }));

                // Update raid button animation (disabled)
                RaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_disabled");
                RaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_disabled");

                // Remove our MainBase
                Mainbase.Dispose();
            }
        }

        private void RaidButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (ActiveRaidQuestId != -1 || PhaseMeter.GetPhase() != Mainbase.PHASE.MAIN || Mainbase == null)
                return;

            Button RaidButton = (Button)sender;
            bool LeftSide = RaidButton == LeftRaidButton;

            if (LeftSide && CardsPlacedQuestOne.Count > 0 || !LeftSide && CardsPlacedQuestTwo.Count > 0)
            {
                RaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_down");
                RaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_down");
            }
        }

        private void RaidButton_MouseLeave(object sender, System.EventArgs e)
        {
            if (ActiveRaidQuestId != -1 || PhaseMeter.GetPhase() != Mainbase.PHASE.MAIN || Mainbase == null)
                return;

            Button RaidButton = (Button)sender;
            bool LeftSide = RaidButton == LeftRaidButton;

            if (LeftSide && CardsPlacedQuestOne.Count > 0 || !LeftSide && CardsPlacedQuestTwo.Count > 0)
            {
                RaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base");
                RaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid");
            }
        }

        private void RaidButton_MouseEnter(object sender, System.EventArgs e)
        {
            if (ActiveRaidQuestId != -1 || PhaseMeter.GetPhase() != Mainbase.PHASE.MAIN || Mainbase == null)
                return;

            Button RaidButton = (Button)sender;
            bool LeftSide = RaidButton == LeftRaidButton;

            if (LeftSide && CardsPlacedQuestOne.Count > 0 || !LeftSide && CardsPlacedQuestTwo.Count > 0)
            {
                RaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_over");
                RaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_over");
            }
        }

        public delegate void _HandlePrompRedraw();
        public void HandlePromptRedraw()
        {
            if (!AlreadyRedrawPrompted)
            {
                AlreadyRedrawPrompted = true;
                CreateMainBase(Mainbase.PHASE.REDRAW, "Would you like to deal 1 damage to your avatar to redraw your hand?");
            }
            else
            {
                EnemyMainBase.SetEnabled(true);
            }
        }

        public delegate void _HandleRedraw(int[] CardIds);
        public void HandleRedraw(int[] CardIds)
        {
            List<CardData> NewHand = new List<CardData>();

            // Convert the CardIds into actual CardData objects
            foreach (int CardId in CardIds)
                if (CardId != -1)
                    NewHand.Add(TCGData.Cards.GetCardData(CardId));

            //Use the CardData objects to update the hand
            OrderHand(NewHand.ToArray());
        }

        public delegate void _HandleAvatarHealthChange(string PlayerUsername, int NewHealth);
        public void HandleAvatarHealthChange(string PlayerUsername, int NewHealth)
        {
            if (PlayerUsername.Equals(PlayerPanel.GetUsername()))
            {
                // Update our avatar's health
                PlayerPanel.UpdateAvatarHealth(NewHealth);
            }
            else if (PlayerUsername.Equals(EnemyPanel.GetUsername()))
            {
                // Update the enemy avatar's health
                EnemyPanel.UpdateAvatarHealth(NewHealth);
            }
        }

        public delegate void _HandleAvatarPowerChange(string PlayerUsername, int NewPower);
        public void HandleAvatarPowerChange(string PlayerUsername, int NewPower)
        {
            if (PlayerUsername.Equals(PlayerPanel.GetUsername()))
            {
                // Update our avatar's health
                PlayerPanel.UpdateAvatarPower(NewPower);
            }
            else if (PlayerUsername.Equals(EnemyPanel.GetUsername()))
            {
                // Update the enemy avatar's health
                EnemyPanel.UpdateAvatarPower(NewPower);
            }
        }

        public delegate void _HandleUpdateCardStats(string Username, int HandQty, int DeckQty, int DiscardedQty);
        public void HandleUpdateCardsStats(string Username, int HandQty, int DeckQty, int DiscardedQty)
        {
            if (Username.Equals(PlayerPanel.GetUsername()))
            {
                PlayerPanel.UpdateCardStats(HandQty, DeckQty, DiscardedQty);
            }
            else if (Username.Equals(EnemyPanel.GetUsername()))
            {
                EnemyPanel.UpdateCardStats(HandQty, DeckQty, DiscardedQty);
            }
        }

        public void ShowEnemyMainbase(bool Show)
        {
            EnemyMainBase.SetEnabled(Show);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void CreateMainBase(Mainbase.PHASE Phase, string Text)
        {
            if (Mainbase != null && !Mainbase.IsDisposed)
                Mainbase.Dispose();

            Mainbase = new Mainbase(Phase);
            Mainbase.Location = new Point((Width - Mainbase.Width) / 2, PlayerPanel.Location.Y + 20);
            Mainbase.SetText(Text);
            Controls.Add(Mainbase);
            Mainbase.BringToFront();

            // We don't want the Mainbase overtop the dialog
            if (Dialog != null && !Dialog.IsDisposed)
                Dialog.BringToFront();

            // Bring hand cards on top
            BringHandToFront();
        }

        public delegate void _HandleDrawPhase(int[] HandCardIds);
        public void HandleDrawPhase(int[] HandCardIds)
        {
            // Update PhaseMeter
            PhaseMeter.ChangePhase(Mainbase.PHASE.DRAW);

            if (HandCardIds != null)
            {
                // Hide the waiting for enemy turn base
                EnemyMainBase.SetEnabled(false);

                // Add the Draw Phase
                CreateMainBase(Mainbase.PHASE.DRAW, "Draw Phase");

                // Update Hand
                CardData[] HandCardData = new CardData[HandCardIds.Length];

                for (int i = 0; i < HandCardIds.Length; i++)
                    HandCardData[i] = TCGData.Cards.GetCardData(HandCardIds[i]);

                OrderHand(HandCardData);
            }
            else
            {
                EnemyMainBase.SetEnabled(true);
            }
        }

        public delegate void _HandleQuestAbility();
        public void HandleQuestAbility()
        {
            // Add ability cards to hint
            CardHints.AddRange(PlayerPanel.GetAbilityCards());

            // Start the hint
            CardHintAnimationTimer.Start();
        }

        public delegate void _HandleQuestPhase(bool MyTurn, bool CanChooseQuest);
        public void HandleQuestPhase(bool MyTurn, bool CanChooseQuest)
        {
            // Update PhaseMeter
            PhaseMeter.ChangePhase(Mainbase.PHASE.QUEST);

            if (MyTurn)
            {
                CreateMainBase(Mainbase.PHASE.QUEST, "Quest Phase");

                if (CanChooseQuest)
                {
                    this.CanChooseQuest = CanChooseQuest;

                    // Reset the AttemptedQuestId
                    AttemptedQuestId = -1;

                    // Update the cursor
                    SetCursor("cursor_target");

                    // Add quests to hints
                    CardHints.Add(Quest1);
                    CardHints.Add(Quest2);

                    // Start the hint animations for the quests
                    CardHintAnimationTimer.Start();
                }
            }
        }

        public delegate void _HandleReadyPhase(bool MyTurn);
        public void HandleReadyPhase(bool MyTurn)
        {
            // Update PhaseMeter
            PhaseMeter.ChangePhase(Mainbase.PHASE.READY);

            if (MyTurn)
            {
                CreateMainBase(Mainbase.PHASE.READY, "Ready Phase");

                // Ready avatar
                PlayerPanel.ReadyAvatar();
                ExertedAvatar = false;

                // Ready cards
                foreach (Card Card in CardsPlacedQuestOne.Values)
                    Card.Ready();
                foreach (Card Card in CardsPlacedQuestTwo.Values)
                    Card.Ready();
            }
            else
            {
                // Ready avatar
                EnemyPanel.ReadyAvatar();

                // Ready cards
                foreach (Card Card in EnemyCardsPlacedQuestOne.Values)
                    Card.Ready();
                foreach (Card Card in EnemyCardsPlacedQuestTwo.Values)
                    Card.Ready();
            }
        }

        public delegate void _HandleUpdatePlayedAbilities(PlaymatCard[] Abilities, string Username);
        public void HandleUpdatePlayedAbilities(PlaymatCard[] Abilities, string Username)
        {
            if (PlayerPanel.GetUsername().Equals(Username))
            {
                // Remove all existing ability cards
                PlayerPanel.RemoveAllAbilityCards();

                // Populate ability card panels with latest card ids from server
                foreach (PlaymatCard Ability in Abilities)
                {
                    PlayerPanel.PlaceAbility(Ability);
                }
            }
            else if (EnemyPanel.GetUsername().Equals(Username))
            {
                // Remove all existing ability cards
                EnemyPanel.RemoveAllAbilityCards();

                // Populate ability card panels with latest card ids from server
                foreach (PlaymatCard Ability in Abilities)
                {
                    EnemyPanel.PlaceAbility(Ability);
                }
            }
        }

        private void PlayExertAnimationOnAvatar(string PlayerUsername)
        {
            if (PlayerPanel.GetUsername().Equals(PlayerUsername))
            {
                PlayerPanel.PlayExertEffectOnAvatar(ExertType);
            }
            else if (EnemyPanel.GetUsername().Equals(PlayerUsername))
            {
                EnemyPanel.PlayExertEffectOnAvatar(ExertType);
            }
        }

        private void SetExertableUnitHints(Card[] PlaymatCards)
        {
            if (PlaymatCards == null)
                System.Console.WriteLine("PlaymatCards==null");

            // Check if we have units to help defend
            if (PlaymatCards.Length > 0)
            {
                for (int i = 0; i < PlaymatCards.Length; i++)
                {
                    // Get the playmat card object
                    PlaymatCard PlaymatCard = PlaymatCards[i].GetPlaymatCard();

                    // Check if exerted
                    if (PlaymatCard.GetExerted())
                        continue;

                    if (ExertType == EXERT_DEFENSE && PlaymatCard.GetDefense() > 0 || ExertType == EXERT_ATTACK && PlaymatCard.GetAttack() > 0 || ExertType == EXERT_DAMAGE && PlaymatCard.GetDamage() > 0)
                    {
                        CardHints.Add(PlaymatCards[i]);

                        // Set event mouse hover handlers for mouse cursor
                        PlaymatCards[i].MouseEnter += CardExertable_MouseEnter;
                        PlaymatCards[i].MouseLeave += CardExertable_MouseLeave;
                    }
                }
            }
            else
            {
                // We have no unit cards, use the avatar if we haven't yet
                if (!ExertedAvatar)
                    CanExertAvatar = true;

                System.Console.WriteLine("Starting to gather valid cards to exert");

                // Use any active ability cards
                List<Card> Abilities = PlayerPanel.GetAbilityCards();

                System.Console.WriteLine("Created abilities array");

                foreach (Card Ability in Abilities)
                {
                    PlaymatCard PlaymatCard = Ability.GetPlaymatCard();

                    // Make sure the item card is not already exerted
                    if (PlaymatCard.GetExerted())
                        continue;

                    if (ExertType == EXERT_DEFENSE && PlaymatCard.GetDefense() > 0 || ExertType == EXERT_ATTACK && PlaymatCard.GetAttack() > 0 || ExertType == EXERT_DAMAGE && PlaymatCard.GetDamage() > 0)
                    {
                        CardHints.Add(Ability);

                        // Set event mouse hover handlers for mouse cursor
                        Ability.MouseEnter += CardExertable_MouseEnter;
                        Ability.MouseLeave += CardExertable_MouseLeave;
                    }
                }

                System.Console.WriteLine("Got abilities");

                // Use any active item cards
                List<Card> Items = PlayerPanel.GetItems();

                foreach (Card Item in Items)
                {
                    PlaymatCard PlaymatCard = Item.GetPlaymatCard();

                    // Make sure the item card is not already exerted
                    if (PlaymatCard.GetExerted())
                        continue;

                    if (ExertType == EXERT_DEFENSE && PlaymatCard.GetDefense() > 0 || ExertType == EXERT_ATTACK && PlaymatCard.GetAttack() > 0 || ExertType == EXERT_DAMAGE && PlaymatCard.GetDamage() > 0)
                    {
                        CardHints.Add(Item);

                        // Set event mouse hover handlers for mouse cursor
                        Item.MouseEnter += CardExertable_MouseEnter;
                        Item.MouseLeave += CardExertable_MouseLeave;
                    }   
                }

                System.Console.WriteLine("Got items");
            }

            // Use any available tactics
            for (int i = 0; i < Hand.Length; i++)
            {
                if (Hand[i] == null)
                    continue;

                CardData CardData = Hand[i].GetCardData();

                if (CardData.GetType().Equals("Tactic") && PlayerPanel.GetAvailablePower() >= CardData.GetCost())
                    CardHints.Add(Hand[i]);
            }
            System.Console.WriteLine("Got tactics");

            // Start the hint
            CardHintAnimationTimer.Start();
            System.Console.WriteLine("Started animation timer");
        }

        public void DialogClosed()
        {
            // The text dialog is closed, therefore show the EndOfGameDialog
            bool AvatarDefeated = PlayerPanel.GetAvatarHealth() == 0 || EnemyPanel.GetAvatarHealth() == 0;
            EndOfGameDialog EndOfGameDialog = new EndOfGameDialog(Won, AvatarDefeated);
            Controls.Add(EndOfGameDialog);
            EndOfGameDialog.Location = new Point((Width - EndOfGameDialog.Width) / 2, (Height - EndOfGameDialog.Height) / 2);
            EndOfGameDialog.BringToFront();
        }

        public delegate void _HandleEndGame(string WinnerUsername);
        public void HandleEndGame(string WinnerUsername)
        {
            Won = PlayerPanel.GetUsername().Equals(WinnerUsername);

            string DialogText;

            if (Won)
            {
                DialogText = StringLib.GetString("scenarios:dialog_win_" + ScenarioId);
                DialogText += "\n\nYou have defeated " + EnemyPanel.GetUsername() + "!";
                MusicService.PlayMusic("music_victory");
            }
            else
            {
                DialogText = StringLib.GetString("scenarios:dialog_lose_" + ScenarioId);
                DialogText += "\n\nYou have been defeated by " + EnemyPanel.GetUsername() + "!";
                MusicService.PlayMusic("music_defeat");
            }

            Dialog = new ScenarioDialog(DialogText, this);
            Dialog.Location = new Point((Width - Dialog.Width) / 2, (Height - Dialog.Height) / 2);
            Controls.Add(Dialog);
            Dialog.BringToFront();

            string Text = "The game has ended. Click \"Next\" to go to the next scenario, or \"Menu\" to return to the selection screen.";
            CreateMainBase(Mainbase.PHASE.GAME_ENDED, Text);
        }

        public delegate void _HandleEndRaid();
        public void HandleEndRaid()
        {
            System.Console.WriteLine("HandleEndRaid()");

            // Deactive raid
            RaidActive = false;

            // Set cursor back to default
            Cursor = Cursors.Default;

            // Stop any animations
            StopCardHintAnimation();

            // Move units back to their original position
            UnitMovementTimer.Start();

            // Get rid of the raid effects
            AttackAnimationPlayer.Dispose();
            AttackAnimationPlayer = null;

            DefenseAnimationPlayer.Dispose();
            DefenseAnimationPlayer = null;
        }

        public delegate void _HandleRequestTakeDamage(int Damage);
        public void HandleRequestTakeDamage(int Damage)
        {
            // Hide the enemy dialog
            EnemyMainBase.SetEnabled(false);

            // Show the player dialog
            CreateMainBase(Mainbase.PHASE.RAID_TAKE_DAMAGE, "Select who will take damage. (Damage left: " + Damage + ")");

            // Set the damage cursor
            SetCursor("cursor_target");

            // Show hints
            CardHintAnimationTimer.Start();
        }

        public delegate void _HandleDamageCard(int CardKey);
        public void HandleDamageCard(int CardKey)
        {
            Card CardDamaged = GetCardFromKey(CardKey);
            CardDamaged.DamageCard();
        }

        public delegate void _HandleDestroyCard(int CardKey);
        public void HandleDestroyCard(int CardKey)
        {
            Card CardDestroyed = GetCardFromKey(CardKey);
            CardDestroyed.DestroyCard();

            // Remove card using key
            if (ActiveRaidQuestId == Quest1.GetId())
            {
                if (CardsPlacedQuestOne.ContainsKey(CardKey))
                    CardsPlacedQuestOne.Remove(CardKey);
                else if (EnemyCardsPlacedQuestOne.ContainsKey(CardKey))
                    EnemyCardsPlacedQuestOne.Remove(CardKey);
            }
            else if (ActiveRaidQuestId == Quest2.GetId())
            {
                if (CardsPlacedQuestTwo.ContainsKey(CardKey))
                    CardsPlacedQuestTwo.Remove(CardKey);
                else if (EnemyCardsPlacedQuestTwo.ContainsKey(CardKey))
                    EnemyCardsPlacedQuestTwo.Remove(CardKey);
            }
        }

        public delegate void _HandleShowRaidOutcome();
        public void HandleShowRaidOutcome()
        {
            int Attack = AttackAnimationPlayer.GetValue();
            int Defense = DefenseAnimationPlayer.GetValue();

            string EffectName = "battle_result_";

            if (Attack > Defense)
            {
                EffectName += "attack_";
                SoundService.PlaySound("attack_success");
            }
            else if (Attack < Defense)
            {
                EffectName += "defense_";
                SoundService.PlaySound("attack_fail");
            }
            else
            {
                EffectName += "draw_";
                SoundService.PlaySound("attack_tied");
            }

            EffectPlayer EffectPlayer = new EffectPlayer(EffectName, 69, 2, new Size(450, 450), false, this, true);
            EffectPlayer.Location = new Point((Width - EffectPlayer.Width) / 2, (Height - EffectPlayer.Height) / 2);
            Controls.Add(EffectPlayer);
            EffectPlayer.BringToFront();
            EffectPlayer.StartAnimation(true);
        }

        public delegate void _HandleRequestExertCard(int ExertType);
        public void HandleRequestExertCard(int ExertType)
        {
            // Update ExertType
            this.ExertType = ExertType;

            // Hide the wait for enemy dialog
            EnemyMainBase.SetEnabled(false);

            // Show the regular player dialog
            CreateMainBase(Mainbase.PHASE.RAID, "You have actions you could use. Select one of them, or 'Pass' to continue.");

            // Reset PlaceableCardIds
            CardHints.Clear();

            // Show int for available cards to exert
            if (ActiveRaidQuestId == Quest1.GetId())
            {
                Card[] Cards = new Card[CardsPlacedQuestOne.Count];
                CardsPlacedQuestOne.Values.CopyTo(Cards, 0);
                SetExertableUnitHints(Cards);
            }
            else if (ActiveRaidQuestId == Quest2.GetId())
            {
                Card[] Cards = new Card[CardsPlacedQuestTwo.Count];
                CardsPlacedQuestTwo.Values.CopyTo(Cards, 0);
                SetExertableUnitHints(Cards);
            }

            // Start hint animation
            if (CardHints.Count > 0)
            {
                CardHintAnimationTimer.Start();
            }
        }

        public delegate void _HandleExertAvatar(string PlayerUsername, int ExertType);
        public void HandleExertAvatar(string PlayerUsername, int ExertType)
        {
            // Update ExertType
            this.ExertType = ExertType;

            PlayExertAnimationOnAvatar(PlayerUsername);

            // Get the avatar id
            int ExertedAvatarId = -1;

            if (PlayerUsername.Equals(PlayerPanel.GetUsername()))
            {
                ExertedAvatarId = PlayerPanel.GetAvatarId();
            }
            else if (PlayerUsername.Equals(EnemyPanel.GetUsername()))
            {
                ExertedAvatarId = EnemyPanel.GetAvatarId();
            }

            // Get value amount from exerted card
            CardData CardData = TCGData.Cards.GetCardData(ExertedAvatarId);

            System.Console.WriteLine("Getting avatar info");

            // Update value number for the BattleAnimationPlayer
            switch (ExertType)
            {
                case EXERT_ATTACK:
                    int Attack = AttackAnimationPlayer.GetValue() + CardData.GetAttack();
                    AttackAnimationPlayer.SetValue(Attack);
                    break;
                case EXERT_DEFENSE:
                    int Defense = DefenseAnimationPlayer.GetValue() + CardData.GetDefense();
                    DefenseAnimationPlayer.SetValue(Defense);
                    break;
                case EXERT_DAMAGE:
                    int Bonus = CardData.GetBonus();
                    // TODO: Update BonusDmg animation
                    break;
            }
        }

        private Card GetCardFromKey(int CardKey)
        {
            System.Console.WriteLine("GetCardFromKey(): Looking for CardKey: " + CardKey);

            if (ActiveRaidQuestId == Quest1.GetId())
            {
                if (CardsPlacedQuestOne.ContainsKey(CardKey))
                    return CardsPlacedQuestOne[CardKey];
                if (EnemyCardsPlacedQuestOne.ContainsKey(CardKey))
                    return EnemyCardsPlacedQuestOne[CardKey];
            }
            else if (ActiveRaidQuestId == Quest2.GetId())
            {
                if (CardsPlacedQuestTwo.ContainsKey(CardKey))
                    return CardsPlacedQuestTwo[CardKey];
                if (EnemyCardsPlacedQuestTwo.ContainsKey(CardKey))
                    return EnemyCardsPlacedQuestTwo[CardKey];
            }
            return null;
        }

        public delegate void _HandleExertCard(string PlayerUsername, int ExertedCardKey, int ExertType, string CardType);
        public void HandleExertCard(string PlayerUsername, int ExertedCardKey, int ExertType, string CardType)
        {
            // Update ExertType
            this.ExertType = ExertType;

            // Get the card we are exerting using the supplied key
            Card ExertingCard = null;

            switch (CardType)
            {
                case "Ability":
                    if (PlayerUsername.Equals(PlayerPanel.GetUsername()))
                        ExertingCard = PlayerPanel.GetUnexertedAbilityCard(ExertedCardKey);
                    else
                        ExertingCard = EnemyPanel.GetUnexertedAbilityCard(ExertedCardKey);
                    break;
                case "Item":
                    if (PlayerUsername.Equals(PlayerPanel.GetUsername()))
                        ExertingCard = PlayerPanel.GetUnexertedItemCard(ExertedCardKey);
                    else
                        ExertingCard = EnemyPanel.GetUnexertedItemCard(ExertedCardKey);
                    break;
                case "Unit":
                    ExertingCard = GetCardFromKey(ExertedCardKey);
                    break;
            }

            // Play the animation for the exerted card (before we update the new ExertType)
            ExertingCard.PlayExertAnimation(ExertType);

            // Get value amount from exerted card (using PlaymatCard object)
            PlaymatCard PlaymatCard = ExertingCard.GetPlaymatCard();

            // Set the card as exerted
            PlaymatCard.SetExerted(true);

            // Update value number for the BattleAnimationPlayer
            switch (ExertType)
            {
                case EXERT_ATTACK:
                    int Attack = AttackAnimationPlayer.GetValue();
                    Attack += PlaymatCard.GetAttack();
                    AttackAnimationPlayer.SetValue(Attack);
                    break;
                case EXERT_DAMAGE:
                    int Damage = AttackAnimationPlayer.GetDamage();
                    Damage += PlaymatCard.GetDamage();
                    AttackAnimationPlayer.AddDamage(Damage);
                    break;
                case EXERT_DEFENSE:
                    int Defense = DefenseAnimationPlayer.GetValue();
                    Defense += PlaymatCard.GetDefense();
                    DefenseAnimationPlayer.SetValue(Defense);
                    break;
            }
        }

        private void CardExertable_MouseLeave(object sender, System.EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void CardExertable_MouseEnter(object sender, System.EventArgs e)
        {
            Card Card = (Card)sender;

            if (CardHints.Contains(Card))
            {
                switch (ExertType)
                {
                    case EXERT_ATTACK:
                        SetCursor("cursor_attack");
                        break;
                    case EXERT_DAMAGE:
                        SetCursor("cursor_damage");
                        break;
                    case EXERT_DEFENSE:
                        SetCursor("cursor_defense");
                        break;
                }
            }
        }

        public delegate void _HandleStartRaid(string AttackingUsername, int QuestId, int AttackValue, int DefenseValue);
        public void HandleStartRaid(string AttackingUsername, int QuestId, int AttackValue, int DefenseValue)
        {
            bool Attacking = PlayerPanel.GetUsername().Equals(AttackingUsername);

            ActiveRaidQuestId = QuestId;

            // Set RaidActive to true if not already
            RaidActive = true;

            // Deactivate raid buttons
            LeftRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_disabled");
            LeftRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_disabled");

            RightRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base_disabled");
            RightRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid_disabled");

            SoundService.PlaySound("lightsaber_sound");

            // Animation
            AttackAnimationPlayer = new BattleAnimationPlayer(Attacking ? "bottom" : "top", "attack", AttackValue, this);
            DefenseAnimationPlayer = new BattleAnimationPlayer(Attacking ? "top" : "bottom", "defense", DefenseValue, this);

            Controls.Add(AttackAnimationPlayer);
            Controls.Add(DefenseAnimationPlayer);

            AttackAnimationPlayer.BringToFront();
            DefenseAnimationPlayer.BringToFront();

            SoundService.PlaySound("exert");

            AttackAnimationPlayer.RunAnimation();
            DefenseAnimationPlayer.RunAnimation();

            // Bring cards Forward
            UnitMovementTimer.Start();

            if (Attacking)
            {
                // Wait for opponent to choose their defense option
                EnemyMainBase.SetEnabled(true);
            }
            else
            {
                if (PhaseMeter.GetPhase() == Mainbase.PHASE.QUEST)
                {
                    // Other player chose a quest

                    // Setup pending ability card on quest
                    Card Card = Quest1.GetId() == QuestId ? Quest1 : Quest2;
                    PendingQuestPlayer = new EffectPlayer("active_quest_", 44, 4, Card.Size, false, null, false);
                    Card.Controls.Add(PendingQuestPlayer);
                    PendingQuestPlayer.StartAnimation(false);
                }

                // Choose defense option
            }
        }

        private void UnitMovementTimer_Tick(object sender, System.EventArgs e)
        {
            bool FinishedMovement = true;

            if (RaidActive)
            {
                // Bring units forward

                if (ActiveRaidQuestId == Quest1.GetId())
                {
                    foreach (Card Card in EnemyCardsPlacedQuestOne.Values)
                        if (Card.Location.Y < (Height / 2) - Card.Height)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y + UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }


                    foreach (Card Card in CardsPlacedQuestOne.Values)
                        if (Card.Location.Y > Height / 2)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y - UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }
                }
                else if (ActiveRaidQuestId == Quest2.GetId())
                {
                    foreach (Card Card in EnemyCardsPlacedQuestTwo.Values)
                        if (Card.Location.Y < (Height / 2) - Card.Height)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y + UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }


                    foreach (Card Card in CardsPlacedQuestTwo.Values)
                        if (Card.Location.Y > Height / 2)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y - UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }
                }
            }
            else
            {
                // Bring units back

                if (ActiveRaidQuestId == Quest1.GetId())
                {
                    foreach (Card Card in EnemyCardsPlacedQuestOne.Values)
                        if (Card.Location.Y > (Height / 2) - Card.Height - UNIT_OFFDISTANCE)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y - UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }

                    foreach (Card Card in CardsPlacedQuestOne.Values)
                        if (Card.Location.Y < (Height / 2 + UNIT_OFFDISTANCE))
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y + UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }
                }
                else if (ActiveRaidQuestId == Quest2.GetId())
                {
                    foreach (Card Card in EnemyCardsPlacedQuestTwo.Values)
                        if (Card.Location.Y > (Height / 2) - Card.Height - UNIT_OFFDISTANCE)
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y - UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }

                    foreach (Card Card in CardsPlacedQuestTwo.Values)
                        if (Card.Location.Y < (Height / 2 + UNIT_OFFDISTANCE))
                        {
                            Card.Location = new Point(Card.Location.X, Card.Location.Y + UNIT_MOVEMENT);
                            FinishedMovement = false;
                        }
                }
            }

            // If we're done moving cards, stop the timer
            if (FinishedMovement)
            {
                UnitMovementTimer.Stop();
            }
        }

        private void SetupCardHintAnimation()
        {
            System.Console.WriteLine("SetupCardHintAnimation()");
            System.Console.WriteLine("AvailablePower: " + PlayerPanel.GetAvailablePower());

            if (RaidActive)
            {
                // Show int for available cards to exert
                if (ActiveRaidQuestId == Quest1.GetId())
                {
                    Card[] Cards = new Card[CardsPlacedQuestOne.Count];
                    CardsPlacedQuestOne.Values.CopyTo(Cards, 0);
                    SetExertableUnitHints(Cards);
                }
                else if (ActiveRaidQuestId == Quest2.GetId())
                {
                    Card[] Cards = new Card[CardsPlacedQuestTwo.Count];
                    CardsPlacedQuestTwo.Values.CopyTo(Cards, 0);
                    SetExertableUnitHints(Cards);
                }
            }
            else
            {
                CardData CardData;
                for (int i = 0; i < Hand.Length; i++)
                {
                    if (Hand[i] == null)
                        continue;

                    CardData = Hand[i].GetCardData();

                    // If we don't have enough power, ignore the card
                    if (CardData.GetCost() > PlayerPanel.GetAvailablePower())
                        continue;

                    // Since we are not in a raid, we cannot play Tactic cards
                    if (!CardData.GetType().Equals("Tactic"))
                        CardHints.Add(Hand[i]);
                }

                if (CardHints.Count > 0)
                    CardHintAnimationTimer.Start();
            }
        }

        public delegate void _HandleMainPhase(string Username);
        public void HandleMainPhase(string Username)
        {
            // Update PhaseMeter
            PhaseMeter.ChangePhase(Mainbase.PHASE.MAIN);

            // Check if it's our turn
            if (!PlayerPanel.GetUsername().Equals(Username))
            {
                // Remove the Mainbase
                if (Mainbase != null)
                {
                    Mainbase.Dispose();
                    Mainbase = null;
                }

                // Show that we are waiting for the other player
                EnemyMainBase.SetEnabled(true);
                return;
            }

            // Set hint animations on hand cards
            SetupCardHintAnimation();

            // Setup raid buttons UI
            if (Mainbase.GetPhase() != Mainbase.PHASE.MAIN || Mainbase.GetPhase() != Mainbase.PHASE.RAID)
            {
                CreateMainBase(Mainbase.PHASE.MAIN, "Main Phase: Play cards and take actions.");

                // Reset RaidButton UI
                if (ActiveRaidQuestId == -1)
                {
                    if (CardsPlacedQuestOne.Count > 0)
                    {
                        LeftRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base");
                        LeftRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid");
                    }

                    if (CardsPlacedQuestTwo.Count > 0)
                    {
                        RightRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base");
                        RightRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid");
                    }
                }
                // Reset ActiveRaidQuestId if we are not just coming back from a raid
                if (!UnitMovementTimer.Enabled)
                    ActiveRaidQuestId = -1;
            }
        }

        public delegate void _HandleQuestUpdateCompletedLevels(int QuestId, int CompletedLevels, string Username);
        public void HandleQuestUpdateCompletedLevels(int QuestId, int CompletedLevels, string Username)
        {
            if (PlayerPanel.GetUsername().Equals(Username))
            {
                // Update the player side
                if (Quest1.GetId() == QuestId)
                    PlayerQuest1LevelsCompleted.Image = CompletedLevels > 0 ? CardsLib.GetImageFromCards("level_pebble_0" + CompletedLevels) : null;
                else if (Quest2.GetId() == QuestId)
                    PlayerQuest2LevelsCompleted.Image = CompletedLevels > 0 ? CardsLib.GetImageFromCards("level_pebble_0" + CompletedLevels) : null;
            }
            else if (EnemyPanel.GetUsername().Equals(Username))
            {
                // Update the enemy side
                if (Quest1.GetId() == QuestId)
                    EnemyQuest1LevelsCompleted.Image = CompletedLevels > 0 ? CardsLib.GetImageFromCards("level_pebble_0" + CompletedLevels) : null;
                else if (Quest2.GetId() == QuestId)
                    EnemyQuest2LevelsCompleted.Image = CompletedLevels > 0 ? CardsLib.GetImageFromCards("level_pebble_0" + CompletedLevels) : null;
            }
        }

        private void UpdateCompletedMissionsCounter(string PlayerUsername)
        {
            string ImageString = "exp_counter_";

            if (PlayerPanel.GetUsername().Equals(PlayerUsername))
            {
                MyCompletedMissions++;
                ImageString += "bottom_" + MyCompletedMissions;
            }
            else if (EnemyPanel.GetUsername().Equals(PlayerUsername))
            {
                EnemyCompltedMissions++;
                ImageString += "top_" + EnemyCompltedMissions;
            }

            Panel Panel = new Panel();
            Panel.Dock = DockStyle.Fill;
            //Panel.BackgroundImageLayout = ImageLayout.Stretch;
            Panel.BackgroundImage = ResourcesLib.GetImageFromGamescreen(ImageString);

            if (LastPlacedExpPanel != null)
                LastPlacedExpPanel.Controls.Add(Panel);
            else
                ExpBase.Controls.Add(Panel);
            LastPlacedExpPanel = Panel;
            LastPlacedExpPanel.BringToFront();
        }

        public delegate void _HandleQuestCompleted(int QuestId, int NewQuestId, string PlayerUsername, int AvatarMaxPower);
        public void HandleQuestCompleted(int QuestId, int NewQuestId, string PlayerUsername, int AvatarMaxPower)
        {
            // Update the variables
            OldQuestId = QuestId;
            this.NewQuestId = NewQuestId;

            // Update the completed levels
            CardData CardData = TCGData.Cards.GetCardData(QuestId);
            HandleQuestUpdateCompletedLevels(QuestId, CardData.GetHealthOrLevel(), PlayerUsername);

            // Update the middle counter
            UpdateCompletedMissionsCounter(PlayerUsername);

            // Show the effect
            EffectPlayer EffectPlayer = new EffectPlayer("quest_", 119, 3, new Size(200, 200), true, this, true);

            if (Quest1.GetId() == QuestId)
            {
                EffectPlayer.Location = new Point((Quest1.Width - EffectPlayer.Width) / 2, (Quest1.Height - EffectPlayer.Height) / 2);
                Quest1.Controls.Add(EffectPlayer);
            }
            else if (Quest2.GetId() == QuestId)
            {
                EffectPlayer.Location = new Point((Quest1.Width - EffectPlayer.Width) / 2, (Quest1.Height - EffectPlayer.Height) / 2);
                Quest2.Controls.Add(EffectPlayer);
            }

            EffectPlayer.StartAnimation(true);

            SoundService.PlaySound("quest_complete_move");

            // Update MaxPower for both avatars
            HandleAvatarPowerChange(PlayerPanel.GetUsername(), AvatarMaxPower);
            HandleAvatarPowerChange(EnemyPanel.GetUsername(), AvatarMaxPower);
        }

        public void HandleEffectCompleted()
        {
            // Reset the completed levels for both players
            HandleQuestUpdateCompletedLevels(OldQuestId, 0, PlayerPanel.GetUsername());
            HandleQuestUpdateCompletedLevels(OldQuestId, 0, EnemyPanel.GetUsername());

            // Delete the quest object and show the new one

            if (Quest1.GetId() == OldQuestId)
            {
                Quest1.Dispose();

                Quest1 = new Card(TCGData.Cards.GetCardData(NewQuestId), Card.SIZE.PLAYMAT, true);
                Quest1.Location = new Point(Quest1.Width / 4, (Height - Quest1.Height) / 2);
                Controls.Add(Quest1);
                Quest1.BringToFront();
            }
            else if (Quest2.GetId() == OldQuestId)
            {
                Quest2.Dispose();

                Quest2 = new Card(TCGData.Cards.GetCardData(NewQuestId), Card.SIZE.PLAYMAT, true);
                Quest2.Location = new Point(Width - Quest2.Width - 10, (Height - Quest2.Height) / 2);
                Controls.Add(Quest2);
                Quest2.BringToFront();
            }

            // reset values
            NewQuestId = -1;
            OldQuestId = -1;
        }

        private void CardHintAnimationTimer_Tick(object sender, System.EventArgs e)
        {
            // Go through each card and apply the animation
            foreach (Card Card in CardHints)
                if (Card.BorderStyle == BorderStyle.None)
                    Card.BorderStyle = BorderStyle.Fixed3D;
                else
                    Card.BorderStyle = BorderStyle.None;

            // Check the avatar too
            if (CanExertAvatar)
                PlayerPanel.AlternateBorder();
        }

        public void StopCardHintAnimation()
        {
            CardHintAnimationTimer.Stop();

            foreach (Card Card in CardHints)
                Card.BorderStyle = BorderStyle.None;

            CardHints.Clear();

            // Stop avatar hint animation if playing
            CanExertAvatar = false;
            PlayerPanel.RemoveAvatarHintAnimation();
        }

        private int GetSpaceBetweenHandCards(int HandQty)
        {
            return Width / HandQty - 10;
        }

        private void OrderHand()
        {
            List<CardData> NewHand = new List<CardData>();

            for (int i = 0; i < Hand.Length; i++)
                if (Hand[i] != null)
                    NewHand.Add(Hand[i].GetCardData());
            OrderHand(NewHand.ToArray());
        }

        private void OrderHand(CardData[] NewHand)
        {
            int SpaceBetweenX = GetSpaceBetweenHandCards(NewHand.Length);

            // Go through each Hand element
            for (int i = 0; i < Hand.Length; i++)
            {
                // Reset the element
                if (Hand[i] != null)
                {
                    Hand[i].Dispose();
                    Hand[i] = null;
                }

                // Is there a valid card to place?
                if (i < NewHand.Length)
                {
                    Hand[i] = new Card(NewHand[i], Card.SIZE.MEDIUM, true)
                    {
                        Location = new Point(i * SpaceBetweenX, HAND_CARD_LOC_Y)
                    };
                    Controls.Add(Hand[i]);

                    Hand[i].MouseEnter += Card_MouseEnter;
                    Hand[i].MouseLeave += Card_MouseLeave;
                    Hand[i].MouseDoubleClick += Game_MouseDoubleClick;
                }
            }

            BringHandToFront();
        }

        private Point GetLocationOfNewPlaycard(int QuestId, int NumberOfCards, int Index)
        {
            int SpaceBetweenPlayCards = (Width / 2 - Quest1.Width * 2 - 10) / NumberOfCards;

            if (Quest1.GetId() == QuestId)
                return new Point(Quest1.Location.X + Quest1.Width + SpaceBetweenPlayCards * Index, Height / 2 + UNIT_OFFDISTANCE);
            return new Point(Quest2.Location.X - Quest2.Width - SpaceBetweenPlayCards * Index, Height / 2 + UNIT_OFFDISTANCE);
        }

        private Point GetEnemyLocationOfNewPlaycard(int QuestId, int NumberOfCards, int Index)
        {
            int SpaceBetweenPlayCards = (Width / 2 - Quest1.Width * 2 - 10) / NumberOfCards;

            if (Quest1.GetId() == QuestId)
                return new Point(Quest1.Location.X + Quest1.Width + SpaceBetweenPlayCards * Index, Height / 2 - 145 - UNIT_OFFDISTANCE);
            return new Point(Quest2.Location.X - Quest2.Width - SpaceBetweenPlayCards * Index, Height / 2 - 145 - UNIT_OFFDISTANCE);
        }

        public string GetEnemyUsername()
        {
            return EnemyPanel.GetUsername();
        }

        private void Game_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Card Card = (Card)sender;

            // Make sure we can use this card
            if (SelectedCard != null || !CardHints.Contains(Card))
                return;

            // Remove the card from hand
            for (int i = 0; i < Hand.Length; i++)
                if (Hand[i].GetId() == Card.GetId())
                {
                    Hand[i].Dispose();
                    Hand[i] = null;
                    break;
                }

            // Set the cursor back to default
            Cursor = Cursors.Default;

            // Remove any energy cost preview
            PlayerPanel.UnpreviewAvatarPower();

            // Get the correct card size
            Card.SIZE CardSize = Card.GetCardData().GetType().Equals("Unit") ? Card.SIZE.PLAYMAT : Card.SIZE.TOOLBAR;

            // Set the SelectedCard object
            SelectedCard = new Card(Card.GetCardData(), CardSize);
            SelectedCard.Location = new Point(e.X - (SelectedCard.Width / 2), e.Y - (SelectedCard.Height / 2));
            SelectedCard.MouseMove += MoveSelectedCard;
            Controls.Add(SelectedCard);

            // Stop the card hints until the card is placed
            StopCardHintAnimation();
        }

        private void Card_MouseLeave(object sender, System.EventArgs e)
        {
            Card Card = (Card)sender;

            if (!Card.ClientRectangle.Contains(Card.PointToClient(MousePosition)))
            {
                if (CardHints.Contains(PreviewingCard))
                    PlayerPanel.UnpreviewAvatarPower();

                CardsToDecollapse.Add(Card);
                PreviewOn = false;

                if (!HoverTimer.Enabled)
                {
                    HoverTimer.Start();
                }

                // Set the cursor back to default
                Cursor = Cursors.Default;
            }
        }

        private void Card_MouseEnter(object sender, System.EventArgs e)
        {
            // Player must choose a quest to use, don't show cards
            if (CanChooseQuest)
                return;

            PreviewingCard = ((Card)sender);

            if (CardHints.Contains(PreviewingCard) && !PlayerPanel.IsPreviewingAvatarPower())
            {
                int CardCost = PreviewingCard.GetCardData().GetCost();
                PlayerPanel.PreviewAvatarPower(CardCost);
            }

            if (PreviewingCard.Location.Y == Height - 315)
                return;

            PreviewOn = true;
            CardsToDecollapse.Remove(PreviewingCard);

            if (!HoverTimer.Enabled)
            {
                HoverTimer.Start();
            }

            // Set the cursor depending on if the card is placeable or not
            if (CardHints.Contains(PreviewingCard))
                SetCursor("cursor_ability");
            else
                Cursor = Cursors.Default;
        }

        public void SetCursor(string CursorImageString)
        {
            // Set ability cursor
            Image CursorImage = ResourcesLib.GetImageFromCursors(CursorImageString);
            Bitmap CursorBitmap = (Bitmap)CursorImage;
            Cursor = new Cursor(CursorBitmap.GetHicon());
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            List<Card> CardsToRemove = new List<Card>();

            int MovementAmount = 9;
            int CardHeight = 315;

            foreach (Card Card in CardsToDecollapse)
            {
                if (Card.Location.Y >= HAND_CARD_LOC_Y)
                {
                    CardsToRemove.Add(Card);
                    Card.Location = new Point(Card.Location.X, HAND_CARD_LOC_Y);
                }
                else
                {
                    Card.Location = new Point(Card.Location.X, Card.Location.Y + MovementAmount);
                }
            }

            foreach (Card Card in CardsToRemove)
                CardsToDecollapse.Remove(Card);

            if (PreviewOn)
            {
                if (PreviewingCard.Location.Y < (Height - CardHeight))
                {
                    PreviewingCard.Location = new Point(PreviewingCard.Location.X, Height - CardHeight);
                    PreviewOn = false;
                }
                else
                {
                    PreviewingCard.Location = new Point(PreviewingCard.Location.X, PreviewingCard.Location.Y - MovementAmount);
                }
            }

            if (CardsToDecollapse.Count == 0 && !PreviewOn)
                HoverTimer.Enabled = false;
        }

        private void Game_MouseMove(object sender, MouseEventArgs e)
        {
            if (SelectedCard != null)
            {
                SelectedCard.Location = new Point(e.X - (SelectedCard.Width / 2), e.Y - (SelectedCard.Height / 2));
            }
        }

        private void MoveSelectedCard(object sender, MouseEventArgs e)
        {
            if (SelectedCard != null)
            {
                Control Control = (Control)sender;

                SelectedCard.Location = new Point(Control.Location.X + e.Location.X - (SelectedCard.Width / 2), Control.Location.Y + e.Location.Y - (SelectedCard.Height / 2));
            }
        }

        private void PlaceCardAndOrganize(Card Card, int Key, SortedDictionary<int, Card> CardDictionary, int QuestId)
        {
            if (CardDictionary.ContainsKey(Key))
            {
                System.Console.WriteLine("Attempted to add card (" + Card.GetTitle() + " with duplicate key (" + Key + ")");
                return;
            }

            // Add the new card
            CardDictionary.Add(Key, Card);
            Controls.Add(Card);

            int[] Keys = new int[CardDictionary.Count];
            CardDictionary.Keys.CopyTo(Keys, 0);

            // re-organize the cards
            for (int i = 0; i < Keys.Length; i++)
            {
                int ThisKey = Keys[i];

                if (CardDictionary == CardsPlacedQuestOne || CardDictionary == CardsPlacedQuestTwo)
                    CardDictionary[ThisKey].Location = GetLocationOfNewPlaycard(QuestId, CardDictionary.Count, i);
                else if (CardDictionary == EnemyCardsPlacedQuestOne || CardDictionary == EnemyCardsPlacedQuestTwo)
                    CardDictionary[ThisKey].Location = GetEnemyLocationOfNewPlaycard(QuestId, CardDictionary.Count, i);
            }
        }

        public delegate void _PlacePlaymatCard(string Username, int QuestId, PlaymatCard Card, int Key);
        public void PlacePlaymatCard(string Username, int QuestId, PlaymatCard Card, int Key)
        {
            CardData CardData = TCGData.Cards.GetCardData(Card.GetCardId());

            if (PlayerPanel.GetUsername().Equals(Username))
            {
                SoundService.PlaySound("play_unit");

                // Create the card object
                Card TempCard = new Card(CardData, Card, Components.Card.SIZE.PLAYMAT);

                // Create hover events for the playmat card
                TempCard.MouseEnter += TempCard_MouseEnter;
                TempCard.MouseLeave += TempCard_MouseLeave;

                // Setup raid buttons
                if (QuestId == Quest1.GetId())
                {
                    PlaceCardAndOrganize(TempCard, Key, CardsPlacedQuestOne, QuestId);

                    if (ActiveRaidQuestId != -1)
                    {
                        LeftRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base");
                        LeftRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid");
                    }

                    // Bring raid button forward
                    LeftRaidButton.BringToFront();

                    // Bring phase meter forward
                    PhaseMeter.BringToFront();
                }
                else if (QuestId == Quest2.GetId())
                {
                    PlaceCardAndOrganize(TempCard, Key, CardsPlacedQuestTwo, QuestId);

                    if (ActiveRaidQuestId != -1)
                    {
                        RightRaidButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("radial_button_base");
                        RightRaidButton.Image = ResourcesLib.GetImageFromGamescreen("radial_buttons_raid");
                    }

                    // Bring raid button forward
                    RightRaidButton.BringToFront();

                    // Bring chat forward
                    Chat.BringToFront();
                }

                // Re-organize hand card placement
                OrderHand();

                // Now that the card is placed and power is updated, show card hints again
                SetupCardHintAnimation();

                // Delete the SelectedCard object
                if (SelectedCard != null)
                {
                    SelectedCard.Dispose();
                    SelectedCard = null;
                }
            }
            else if (EnemyPanel.GetUsername().Equals(Username))
            {
                SoundService.PlaySound("play_unit");

                // Create the card object
                Card TempCard = new Card(CardData, Card, Components.Card.SIZE.PLAYMAT);

                if (QuestId == Quest1.GetId())
                {
                    PlaceCardAndOrganize(TempCard, Key, EnemyCardsPlacedQuestOne, QuestId);

                    // Bring raid button forward
                    LeftRaidButton.BringToFront();
                }
                else if (QuestId == Quest2.GetId())
                {
                    PlaceCardAndOrganize(TempCard, Key, EnemyCardsPlacedQuestTwo, QuestId);

                    // Bring raid button forward
                    RightRaidButton.BringToFront();
                }
            }
        }

        private void TempCard_MouseLeave(object sender, System.EventArgs e)
        {
            Card Card = (Card)sender;

            // Make sure the mouse actually left before continuing
            if (Card.ClientRectangle.Contains(Card.PointToClient(MousePosition)))
                return;

            // Reorganize cards
            if (CardsPlacedQuestOne.ContainsValue(Card))
            {
                foreach (Card ThisCard in CardsPlacedQuestOne.Values)
                    ThisCard.BringToFront();
            }
            else if (CardsPlacedQuestTwo.ContainsValue(Card))
            {
                foreach (Card ThisCard in CardsPlacedQuestTwo.Values)
                    ThisCard.BringToFront();
            }

            // Bring phase meter to the front just incase the card is covering it
            PhaseMeter.BringToFront();
        }

        private void TempCard_MouseEnter(object sender, System.EventArgs e)
        {
            Card Card = (Card)sender;

            // Bring the card to the front
            Card.BringToFront();

            // Bring phase meter to the front just incase the card is covering it
            PhaseMeter.BringToFront();
        }

        public delegate void _PlayTactic(string Username, int CardId);
        public void PlayTactic(string Username, int CardId)
        {
            SoundService.PlaySound("play_tactic");

            // Delete any existing TacticCard preview
            if (TacticCard != null)
            {
                TacticCard.Dispose();
                TacticCard = null;
            }

            // Show the tactic card played
            CardData CardData = TCGData.Cards.GetCardData(CardId);
            TacticCard = new Card(CardData, Card.SIZE.LARGE);
            TacticCard.Location = new Point(Width - TacticCard.Width, (Height - TacticCard.Height) / 2);
            Controls.Add(TacticCard);

            // Re-organize hand card placement
            OrderHand();

            // Now that the card is placed and power is updated, show card hints again
            SetupCardHintAnimation();

            // Delete the SelectedCard object
            if (SelectedCard != null)
            {
                SelectedCard.Dispose();
                SelectedCard = null;
            }
        }

        public delegate void _PlaceCard(string Username, PlaymatCard PlaymatCard);
        public void PlaceCard(string Username, PlaymatCard PlaymatCard)
        {
            PlayerPanel Panel = PlayerPanel.GetUsername().Equals(Username) ? PlayerPanel : EnemyPanel;

            CardData CardData = TCGData.Cards.GetCardData(PlaymatCard.GetCardId());

            switch (CardData.GetType())
            {
                case "Ability":
                    SoundService.PlaySound("play_ability");

                    // Place in ability spot
                    Panel.PlaceAbility(PlaymatCard);
                    break;
                case "Item":
                    SoundService.PlaySound("play_item");

                    // Place in item spot
                    Panel.PlaceItem(PlaymatCard);
                    break;
                default:
                    System.Console.WriteLine("Attempted to place invalid CardType: " + CardData.GetType());
                    break;
            }

            // Re-organize hand card placement
            OrderHand();

            // Now that the card is placed and power is updated, show card hints again
            SetupCardHintAnimation();

            // Delete the SelectedCard object
            if (SelectedCard != null)
            {
                SelectedCard.Dispose();
                SelectedCard = null;
            }
        }

        public void CardRightClicked(object sender, MouseEventArgs e)
        {
            Card Card = (Card)sender;

            if (Card == TacticCard)
            {
                // Player is done reading the tactic card
                TacticCard.Dispose();
                TacticCard = null;
            }
            else
            {
                // Player is requesting a preview of the card (enlargement)
                PreviewCard(Card.GetCardData());
            }
        }

        public void PreviewCard(CardData CardData)
        {
            if (EnlargedCard != null)
            {
                EnlargedCard.Dispose();

                if (EnlargedCard.GetCardData() == CardData)
                {
                    EnlargedCard = null;
                    return;
                }
            }
            EnlargedCard = new Card(CardData, Card.SIZE.LARGE);
            EnlargedCard.Location = new Point(PhaseMeter.Location.X, PhaseMeter.Location.Y - EnlargedCard.Size.Height);
            Controls.Add(EnlargedCard);
        }

        public void CardDoubleClicked(object sender, MouseEventArgs e)
        {
            // Player is placing card

            System.Console.WriteLine("CardDoubleClicked()");

            if (CanChooseQuest)
            {
                // Get the selected card object
                Card Card = (Card)sender;

                if (AttemptedQuestId == -1)
                {
                    // Make sure the user selected a valid quest
                    if (!Card.GetCardData().GetType().Equals("Quest"))
                        return;

                    // Play sound
                    SoundService.PlaySound("card_chosen");

                    // Get the id of the selected quest
                    AttemptedQuestId = Card.GetId();

                    // Construct the packet
                    CommandPacket CommandPacket = new CommandPacket(new object[] { AttemptedQuestId });

                    // Send the packet
                    PacketService.SendPacket(Commands.QUEST_PHASE, CommandPacket);

                    // Stop the animation
                    StopCardHintAnimation();

                    // Setup pending ability card on quest
                    PendingQuestPlayer = new EffectPlayer("active_quest_", 44, 4, Card.Size, false, null, false);
                    Card.Controls.Add(PendingQuestPlayer);
                    PendingQuestPlayer.StartAnimation(false);
                }
                else
                {
                    // Remove all borders / animations from ability cards
                    StopCardHintAnimation();

                    // Reset the cursor
                    Cursor = Cursors.Default;

                    // Delete the animation
                    PendingQuestPlayer.Destroy();
                    PendingQuestPlayer = null;

                    // Construct the packet
                    CommandPacket CommandPacket = new CommandPacket(new object[] { Card.GetId() });

                    // Send the packet
                    PacketService.SendPacket(Commands.QUEST_PHASE, CommandPacket);

                    // Reset values
                    CanChooseQuest = false;
                    AttemptedQuestId = -1;
                }
            }
            else if (RaidActive)
            {
                Card Card = (Card)sender;

                // Get the selected card object
                if (Mainbase.GetPhase() == Mainbase.PHASE.RAID_TAKE_DAMAGE)
                {
                    // Tell the server we want to deal damage to this card
                    int CardKey = GetKeyOfCard(Card);
                    CommandPacket CommandPacket = new CommandPacket(new object[] { CardKey });
                    PacketService.SendPacket(Commands.RAID_TAKE_DAMAGE, CommandPacket);
                }
                else
                {
                    switch (Card.GetCardData().GetType())
                    {
                        case "Ability":
                        case "Item":
                        case "Unit":
                            // Check if this card has been exerted already
                            if (Card.GetPlaymatCard().GetExerted())
                                return;

                            // Send the server the card KEY we want to exert along with what we are exerting it for
                            int CardKey = GetKeyOfCard(Card);

                            // Prepare the packet
                            CommandPacket CommandPacket;

                            if (CardKey == -1)
                            {
                                // Must be an Ability or Item card, send the card id instead
                                CommandPacket = new CommandPacket(new object[] { Card.GetId(), ExertType, Card.GetCardData().GetType() });
                            }
                            else
                            {
                                // This must be a Unit card, send the key
                                CommandPacket = new CommandPacket(new object[] { CardKey, ExertType, Card.GetCardData().GetType() });
                            }
                            
                            PacketService.SendPacket(Commands.RAID_EXERT_CARD, CommandPacket);

                            // Remove from hint animation
                            CardHints.Remove(Card);
                            Card.BorderStyle = BorderStyle.None;

                            // Reset cursor
                            Cursor = Cursors.Default;

                            // Change turn animation (mainbase, etc.)
                            EndTurnAnimation();
                            break;
                        case "Tactic":
                            // Play the tactic, stick with the turn
                            CommandPacket = new CommandPacket(new object[] { Card.GetId() });
                            PacketService.SendPacket(Commands.PLACE_CARD, CommandPacket);
                            break;
                    }
                }
            }
            else if (SelectedCard != null)
            {
                // Let server know player wants to place card
                int CardId = SelectedCard.GetCardData().GetId();

                // Construct the card placement packet
                CommandPacket PlacePacket;

                //Only include the QuestId if it's a Unit card
                if (SelectedCard.GetCardData().GetType().Equals("Unit"))
                {
                    // Get which side we placed the card
                    int CursorLocation = SelectedCard.Location.X + (SelectedCard.Width / 2);
                    bool LeftSide = CursorLocation < Width / 2;

                    // Let the server know what quest we are placing the card
                    int QuestId = LeftSide ? Quest1.GetId() : Quest2.GetId();

                    PlacePacket = new CommandPacket(new object[] { CardId, QuestId });
                }
                else
                {
                    PlacePacket = new CommandPacket(new object[] { CardId });
                }
                PacketService.SendPacket(Commands.PLACE_CARD, PlacePacket);
            }
            else
            {
                System.Console.WriteLine("Attempting to place card failed");
            }
        }

        public void DoubleClickAvatar()
        {
            if (RaidActive)
            {
                // Get the selected card object
                if (Mainbase.GetPhase() == Mainbase.PHASE.RAID_TAKE_DAMAGE)
                {
                    // Tell the server we want to deal damage to the avatar
                    PacketService.SendPacket(Commands.RAID_DAMAGE_AVATAR, null);
                    System.Console.WriteLine("Damaging avatar");
                }
                else if (CanExertAvatar)
                {
                    System.Console.WriteLine("Exerting avatar");
                    PacketService.SendPacket(Commands.RAID_EXERT_AVATAR, null);

                    // Remove from hint animation
                    ExertedAvatar = true;
                    CanExertAvatar = false;
                    PlayerPanel.RemoveAvatarHintAnimation();

                    // Reset cursor
                    Cursor = Cursors.Default;

                    // Change turn animation (mainbase, etc.)
                    EndTurnAnimation();
                }
                else
                {
                    System.Console.WriteLine("Double clicked avatar (no affect)");
                }
            }
        }

        private int GetKeyOfCard(Card Card)
        {
            foreach (int Key in CardsPlacedQuestOne.Keys)
                if (CardsPlacedQuestOne[Key] == Card)
                    return Key;

            foreach (int Key in CardsPlacedQuestTwo.Keys)
                if (CardsPlacedQuestTwo[Key] == Card)
                    return Key;

            return -1;
        }

        public void ChangePhase(Mainbase.PHASE Phase)
        {
            PhaseMeter.ChangePhase(Phase);
        }

        public void SetSentLeaveGamePacket()
        {
            SentLeaveGamePacket = true;
        }

        public void ShowGameInfo()
        {
            string Text = StringLib.GetString("scenarios:dialog_start_" + ScenarioId);
            string[] TextSplit = Text.Split('\n');

            // Win information will be the latest sentence of the start dialog text.
            Text = TextSplit[TextSplit.Length - 1];
            TCGCore.GetTCG().AddSystemMessage(Text, "Scenario Information");
        }

        public Card GetSelectedCard()
        {
            return SelectedCard;
        }

        public bool GetCanExertAvatar()
        {
            return CanExertAvatar;
        }

        public bool CanDamageAvatar()
        {
            if (Mainbase != null && Mainbase.GetPhase() == Mainbase.PHASE.RAID_TAKE_DAMAGE)
            {
                if (ActiveRaidQuestId == Quest1.GetId())
                {
                    return CardsPlacedQuestOne.Count > 0;
                }
                else if (ActiveRaidQuestId == Quest2.GetId())
                {
                    return CardsPlacedQuestTwo.Count > 0;
                }
            }
            return false;
        }

        private void EndTurnAnimation()
        {
            // Stop the exert hint animation
            StopCardHintAnimation();

            // Remove our Mainbase
            Mainbase.Dispose();

            // Show that the enemy is now choosing their combat option
            EnemyMainBase.SetEnabled(true);
        }

        private void Game_Load(object sender, System.EventArgs e)
        {
            // Stop any current music
            MusicService.StopMusic();

            // Start playmat music
            MusicService.PlayMusic("music_playmat_", 16);
        }

        private void BringHandToFront()
        {
            for (int i = Hand.Length-1; i >= 0; i--)
            {
                if (Hand[i] != null)
                    Hand[i].BringToFront();
            }

            foreach (Card Card in Hand)
                if (Card != null)
                    Card.BringToFront();
        }
    }
}
