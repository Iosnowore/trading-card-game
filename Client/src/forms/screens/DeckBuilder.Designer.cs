﻿namespace TradingCardGame
{
    partial class DeckBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopCardPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.BottomCardPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.PreviousPageButton = new System.Windows.Forms.Button();
            this.NextPageButton = new System.Windows.Forms.Button();
            this.NewDeckButton = new System.Windows.Forms.Button();
            this.OpenDeckButton = new System.Windows.Forms.Button();
            this.SaveDeckButton = new System.Windows.Forms.Button();
            this.ToolsButton = new System.Windows.Forms.Button();
            this.HelperButton = new System.Windows.Forms.Button();
            this.UnitsButton = new System.Windows.Forms.Button();
            this.validDeckPictureBox = new System.Windows.Forms.PictureBox();
            this.standardButton = new System.Windows.Forms.Button();
            this.ItemsButton = new System.Windows.Forms.Button();
            this.TacticsButton = new System.Windows.Forms.Button();
            this.AbilitiesButton = new System.Windows.Forms.Button();
            this.DrawDeckButton = new System.Windows.Forms.Button();
            this.QuestsButton = new System.Windows.Forms.Button();
            this.AvatarButton = new System.Windows.Forms.Button();
            this.removeAllFiltersButton = new System.Windows.Forms.Button();
            this.showValidQuestsButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.CardQuantityLabel = new System.Windows.Forms.Label();
            this.FilterButton = new System.Windows.Forms.Button();
            this.BottomPanel.SuspendLayout();
            this.MenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.SuspendLayout();
            // 
            // TopCardPanel
            // 
            this.TopCardPanel.AutoScroll = true;
            this.TopCardPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TopCardPanel.Location = new System.Drawing.Point(27, 73);
            this.TopCardPanel.Name = "TopCardPanel";
            this.TopCardPanel.Size = new System.Drawing.Size(970, 200);
            this.TopCardPanel.TabIndex = 71;
            // 
            // BottomPanel
            // 
            this.BottomPanel.BackColor = System.Drawing.Color.Transparent;
            this.BottomPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BottomPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BottomPanel.Controls.Add(this.BottomCardPanel);
            this.BottomPanel.Controls.Add(this.MenuPanel);
            this.BottomPanel.Controls.Add(this.HelperButton);
            this.BottomPanel.Controls.Add(this.UnitsButton);
            this.BottomPanel.Controls.Add(this.validDeckPictureBox);
            this.BottomPanel.Controls.Add(this.standardButton);
            this.BottomPanel.Controls.Add(this.ItemsButton);
            this.BottomPanel.Controls.Add(this.TacticsButton);
            this.BottomPanel.Controls.Add(this.AbilitiesButton);
            this.BottomPanel.Controls.Add(this.DrawDeckButton);
            this.BottomPanel.Controls.Add(this.QuestsButton);
            this.BottomPanel.Controls.Add(this.AvatarButton);
            this.BottomPanel.Controls.Add(this.removeAllFiltersButton);
            this.BottomPanel.Controls.Add(this.showValidQuestsButton);
            this.BottomPanel.Location = new System.Drawing.Point(27, 279);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(970, 475);
            this.BottomPanel.TabIndex = 70;
            // 
            // BottomCardPanel
            // 
            this.BottomCardPanel.AutoScroll = true;
            this.BottomCardPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BottomCardPanel.Location = new System.Drawing.Point(187, 59);
            this.BottomCardPanel.Name = "BottomCardPanel";
            this.BottomCardPanel.Size = new System.Drawing.Size(600, 409);
            this.BottomCardPanel.TabIndex = 62;
            // 
            // MenuPanel
            // 
            this.MenuPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MenuPanel.Controls.Add(this.PreviousPageButton);
            this.MenuPanel.Controls.Add(this.NextPageButton);
            this.MenuPanel.Controls.Add(this.NewDeckButton);
            this.MenuPanel.Controls.Add(this.OpenDeckButton);
            this.MenuPanel.Controls.Add(this.SaveDeckButton);
            this.MenuPanel.Controls.Add(this.ToolsButton);
            this.MenuPanel.Location = new System.Drawing.Point(0, 0);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(963, 50);
            this.MenuPanel.TabIndex = 79;
            // 
            // PreviousPageButton
            // 
            this.PreviousPageButton.FlatAppearance.BorderSize = 0;
            this.PreviousPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviousPageButton.Location = new System.Drawing.Point(450, 0);
            this.PreviousPageButton.Name = "PreviousPageButton";
            this.PreviousPageButton.Size = new System.Drawing.Size(25, 51);
            this.PreviousPageButton.TabIndex = 63;
            this.PreviousPageButton.UseVisualStyleBackColor = true;
            this.PreviousPageButton.Click += new System.EventHandler(this.PreviousPageButton_Click);
            this.PreviousPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseDown);
            this.PreviousPageButton.MouseEnter += new System.EventHandler(this.PreviousPageButton_MouseEnter);
            this.PreviousPageButton.MouseLeave += new System.EventHandler(this.PreviousPageButton_MouseLeave);
            this.PreviousPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseUp);
            // 
            // NextPageButton
            // 
            this.NextPageButton.FlatAppearance.BorderSize = 0;
            this.NextPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextPageButton.Location = new System.Drawing.Point(820, 0);
            this.NextPageButton.Name = "NextPageButton";
            this.NextPageButton.Size = new System.Drawing.Size(25, 51);
            this.NextPageButton.TabIndex = 62;
            this.NextPageButton.UseVisualStyleBackColor = true;
            this.NextPageButton.Click += new System.EventHandler(this.NextPageButton_Click);
            this.NextPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseDown);
            this.NextPageButton.MouseEnter += new System.EventHandler(this.NextPageButton_MouseEnter);
            this.NextPageButton.MouseLeave += new System.EventHandler(this.NextPageButton_MouseLeave);
            this.NextPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseUp);
            // 
            // NewDeckButton
            // 
            this.NewDeckButton.BackColor = System.Drawing.Color.Black;
            this.NewDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NewDeckButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.NewDeckButton.FlatAppearance.BorderSize = 0;
            this.NewDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NewDeckButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NewDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NewDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewDeckButton.ForeColor = System.Drawing.Color.Black;
            this.NewDeckButton.Location = new System.Drawing.Point(3, 11);
            this.NewDeckButton.Name = "NewDeckButton";
            this.NewDeckButton.Size = new System.Drawing.Size(110, 29);
            this.NewDeckButton.TabIndex = 52;
            this.NewDeckButton.Text = "New Deck";
            this.NewDeckButton.UseVisualStyleBackColor = false;
            this.NewDeckButton.Click += new System.EventHandler(this.NewDeckButton_Click);
            // 
            // OpenDeckButton
            // 
            this.OpenDeckButton.BackColor = System.Drawing.Color.Black;
            this.OpenDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OpenDeckButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.OpenDeckButton.FlatAppearance.BorderSize = 0;
            this.OpenDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OpenDeckButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OpenDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenDeckButton.ForeColor = System.Drawing.Color.Black;
            this.OpenDeckButton.Location = new System.Drawing.Point(119, 11);
            this.OpenDeckButton.Name = "OpenDeckButton";
            this.OpenDeckButton.Size = new System.Drawing.Size(110, 29);
            this.OpenDeckButton.TabIndex = 53;
            this.OpenDeckButton.Text = "Open Deck";
            this.OpenDeckButton.UseVisualStyleBackColor = false;
            this.OpenDeckButton.Click += new System.EventHandler(this.OpenDeckButton_Click);
            // 
            // SaveDeckButton
            // 
            this.SaveDeckButton.BackColor = System.Drawing.Color.Black;
            this.SaveDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SaveDeckButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SaveDeckButton.FlatAppearance.BorderSize = 0;
            this.SaveDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SaveDeckButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SaveDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveDeckButton.ForeColor = System.Drawing.Color.Black;
            this.SaveDeckButton.Location = new System.Drawing.Point(235, 11);
            this.SaveDeckButton.Name = "SaveDeckButton";
            this.SaveDeckButton.Size = new System.Drawing.Size(110, 29);
            this.SaveDeckButton.TabIndex = 54;
            this.SaveDeckButton.Text = "Save Deck";
            this.SaveDeckButton.UseVisualStyleBackColor = false;
            this.SaveDeckButton.Click += new System.EventHandler(this.SaveDeckButton_Click);
            // 
            // ToolsButton
            // 
            this.ToolsButton.BackColor = System.Drawing.Color.Black;
            this.ToolsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToolsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ToolsButton.FlatAppearance.BorderSize = 0;
            this.ToolsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToolsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToolsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToolsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolsButton.ForeColor = System.Drawing.Color.Black;
            this.ToolsButton.Location = new System.Drawing.Point(351, 11);
            this.ToolsButton.Name = "ToolsButton";
            this.ToolsButton.Size = new System.Drawing.Size(90, 29);
            this.ToolsButton.TabIndex = 55;
            this.ToolsButton.Text = "Tools▾";
            this.ToolsButton.UseVisualStyleBackColor = false;
            this.ToolsButton.Click += new System.EventHandler(this.ToolsButton_Click);
            // 
            // HelperButton
            // 
            this.HelperButton.BackColor = System.Drawing.Color.Black;
            this.HelperButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HelperButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.HelperButton.FlatAppearance.BorderSize = 0;
            this.HelperButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.HelperButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.HelperButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HelperButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelperButton.ForeColor = System.Drawing.Color.Black;
            this.HelperButton.Location = new System.Drawing.Point(795, 54);
            this.HelperButton.Name = "HelperButton";
            this.HelperButton.Size = new System.Drawing.Size(165, 157);
            this.HelperButton.TabIndex = 77;
            this.HelperButton.UseVisualStyleBackColor = false;
            this.HelperButton.Click += new System.EventHandler(this.HelperButton_Click);
            this.HelperButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HelperButton_MouseDown);
            this.HelperButton.MouseEnter += new System.EventHandler(this.HelperButton_MouseEnter);
            this.HelperButton.MouseLeave += new System.EventHandler(this.HelperButton_MouseLeave);
            this.HelperButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HelperButton_MouseUp);
            // 
            // UnitsButton
            // 
            this.UnitsButton.BackColor = System.Drawing.Color.Black;
            this.UnitsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UnitsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.UnitsButton.FlatAppearance.BorderSize = 0;
            this.UnitsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.UnitsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.UnitsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UnitsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnitsButton.ForeColor = System.Drawing.Color.Black;
            this.UnitsButton.Location = new System.Drawing.Point(32, 253);
            this.UnitsButton.Name = "UnitsButton";
            this.UnitsButton.Size = new System.Drawing.Size(147, 31);
            this.UnitsButton.TabIndex = 76;
            this.UnitsButton.Text = "Units (0) 0%";
            this.UnitsButton.UseVisualStyleBackColor = false;
            this.UnitsButton.Click += new System.EventHandler(this.UnitsButton_Click);
            this.UnitsButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UnitsButton_MouseDown);
            this.UnitsButton.MouseEnter += new System.EventHandler(this.UnitsButton_MouseEnter);
            this.UnitsButton.MouseLeave += new System.EventHandler(this.UnitsButton_MouseLeave);
            this.UnitsButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UnitsButton_MouseUp);
            // 
            // validDeckPictureBox
            // 
            this.validDeckPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.validDeckPictureBox.Location = new System.Drawing.Point(834, 439);
            this.validDeckPictureBox.Name = "validDeckPictureBox";
            this.validDeckPictureBox.Size = new System.Drawing.Size(91, 29);
            this.validDeckPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.validDeckPictureBox.TabIndex = 67;
            this.validDeckPictureBox.TabStop = false;
            // 
            // standardButton
            // 
            this.standardButton.BackColor = System.Drawing.Color.Black;
            this.standardButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.standardButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.standardButton.FlatAppearance.BorderSize = 0;
            this.standardButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.standardButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.standardButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.standardButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.standardButton.ForeColor = System.Drawing.Color.Black;
            this.standardButton.Location = new System.Drawing.Point(811, 404);
            this.standardButton.Name = "standardButton";
            this.standardButton.Size = new System.Drawing.Size(140, 29);
            this.standardButton.TabIndex = 62;
            this.standardButton.Text = "Standard";
            this.standardButton.UseVisualStyleBackColor = false;
            this.standardButton.Click += new System.EventHandler(this.StandardButton_Click);
            // 
            // ItemsButton
            // 
            this.ItemsButton.BackColor = System.Drawing.Color.Black;
            this.ItemsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ItemsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ItemsButton.FlatAppearance.BorderSize = 0;
            this.ItemsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ItemsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ItemsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ItemsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsButton.ForeColor = System.Drawing.Color.Black;
            this.ItemsButton.Location = new System.Drawing.Point(32, 222);
            this.ItemsButton.Name = "ItemsButton";
            this.ItemsButton.Size = new System.Drawing.Size(147, 31);
            this.ItemsButton.TabIndex = 75;
            this.ItemsButton.Text = "Items (0) 0%";
            this.ItemsButton.UseVisualStyleBackColor = false;
            this.ItemsButton.Click += new System.EventHandler(this.ItemsButton_Click);
            this.ItemsButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ItemsButton_MouseDown);
            this.ItemsButton.MouseEnter += new System.EventHandler(this.ItemsButton_MouseEnter);
            this.ItemsButton.MouseLeave += new System.EventHandler(this.ItemsButton_MouseLeave);
            this.ItemsButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ItemsButton_MouseUp);
            // 
            // TacticsButton
            // 
            this.TacticsButton.BackColor = System.Drawing.Color.Black;
            this.TacticsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TacticsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.TacticsButton.FlatAppearance.BorderSize = 0;
            this.TacticsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TacticsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TacticsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TacticsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TacticsButton.ForeColor = System.Drawing.Color.Black;
            this.TacticsButton.Location = new System.Drawing.Point(32, 191);
            this.TacticsButton.Name = "TacticsButton";
            this.TacticsButton.Size = new System.Drawing.Size(147, 31);
            this.TacticsButton.TabIndex = 74;
            this.TacticsButton.Text = "Tactics (0) 0%";
            this.TacticsButton.UseVisualStyleBackColor = false;
            this.TacticsButton.Click += new System.EventHandler(this.TacticsButton_Click);
            this.TacticsButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TacticsButton_MouseDown);
            this.TacticsButton.MouseEnter += new System.EventHandler(this.TacticsButton_MouseEnter);
            this.TacticsButton.MouseLeave += new System.EventHandler(this.TacticsButton_MouseLeave);
            this.TacticsButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TacticsButton_MouseUp);
            // 
            // AbilitiesButton
            // 
            this.AbilitiesButton.BackColor = System.Drawing.Color.Black;
            this.AbilitiesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AbilitiesButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AbilitiesButton.FlatAppearance.BorderSize = 0;
            this.AbilitiesButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AbilitiesButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AbilitiesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AbilitiesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AbilitiesButton.ForeColor = System.Drawing.Color.Black;
            this.AbilitiesButton.Location = new System.Drawing.Point(32, 160);
            this.AbilitiesButton.Name = "AbilitiesButton";
            this.AbilitiesButton.Size = new System.Drawing.Size(147, 31);
            this.AbilitiesButton.TabIndex = 73;
            this.AbilitiesButton.Text = "Abilities (0) 0%";
            this.AbilitiesButton.UseVisualStyleBackColor = false;
            this.AbilitiesButton.Click += new System.EventHandler(this.AbilitiesButton_Click);
            this.AbilitiesButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AbilitiesButton_MouseDown);
            this.AbilitiesButton.MouseEnter += new System.EventHandler(this.AbilitiesButton_MouseEnter);
            this.AbilitiesButton.MouseLeave += new System.EventHandler(this.AbilitiesButton_MouseLeave);
            this.AbilitiesButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AbilitiesButton_MouseUp);
            // 
            // DrawDeckButton
            // 
            this.DrawDeckButton.BackColor = System.Drawing.Color.Black;
            this.DrawDeckButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DrawDeckButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.DrawDeckButton.FlatAppearance.BorderSize = 0;
            this.DrawDeckButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.DrawDeckButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.DrawDeckButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DrawDeckButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DrawDeckButton.ForeColor = System.Drawing.Color.Black;
            this.DrawDeckButton.Location = new System.Drawing.Point(3, 126);
            this.DrawDeckButton.Name = "DrawDeckButton";
            this.DrawDeckButton.Size = new System.Drawing.Size(179, 34);
            this.DrawDeckButton.TabIndex = 72;
            this.DrawDeckButton.Text = "Draw Deck (0)";
            this.DrawDeckButton.UseVisualStyleBackColor = false;
            this.DrawDeckButton.Click += new System.EventHandler(this.DrawDeckButton_Click);
            this.DrawDeckButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DrawDeckButton_MouseDown);
            this.DrawDeckButton.MouseEnter += new System.EventHandler(this.DrawDeckButton_MouseEnter);
            this.DrawDeckButton.MouseLeave += new System.EventHandler(this.DrawDeckButton_MouseLeave);
            this.DrawDeckButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DrawDeckButton_MouseUp);
            // 
            // QuestsButton
            // 
            this.QuestsButton.BackColor = System.Drawing.Color.Black;
            this.QuestsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.QuestsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.QuestsButton.FlatAppearance.BorderSize = 0;
            this.QuestsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.QuestsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.QuestsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QuestsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestsButton.ForeColor = System.Drawing.Color.Black;
            this.QuestsButton.Location = new System.Drawing.Point(3, 92);
            this.QuestsButton.Name = "QuestsButton";
            this.QuestsButton.Size = new System.Drawing.Size(179, 34);
            this.QuestsButton.TabIndex = 71;
            this.QuestsButton.Text = "Quests (0)";
            this.QuestsButton.UseVisualStyleBackColor = false;
            this.QuestsButton.Click += new System.EventHandler(this.QuestsButton_Click);
            this.QuestsButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.QuestsButton_MouseDown);
            this.QuestsButton.MouseEnter += new System.EventHandler(this.QuestsButton_MouseEnter);
            this.QuestsButton.MouseLeave += new System.EventHandler(this.QuestsButton_MouseLeave);
            this.QuestsButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.QuestsButton_MouseUp);
            // 
            // AvatarButton
            // 
            this.AvatarButton.BackColor = System.Drawing.Color.Black;
            this.AvatarButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AvatarButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AvatarButton.FlatAppearance.BorderSize = 0;
            this.AvatarButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AvatarButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AvatarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AvatarButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvatarButton.ForeColor = System.Drawing.Color.Black;
            this.AvatarButton.Location = new System.Drawing.Point(3, 58);
            this.AvatarButton.Name = "AvatarButton";
            this.AvatarButton.Size = new System.Drawing.Size(179, 34);
            this.AvatarButton.TabIndex = 65;
            this.AvatarButton.Text = "Avatar (0)";
            this.AvatarButton.UseVisualStyleBackColor = false;
            this.AvatarButton.Click += new System.EventHandler(this.AvatarButton_Click);
            this.AvatarButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AvatarButton_MouseDown);
            this.AvatarButton.MouseEnter += new System.EventHandler(this.AvatarButton_MouseEnter);
            this.AvatarButton.MouseLeave += new System.EventHandler(this.AvatarButton_MouseLeave);
            this.AvatarButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AvatarButton_MouseUp);
            // 
            // removeAllFiltersButton
            // 
            this.removeAllFiltersButton.BackColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeAllFiltersButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.FlatAppearance.BorderSize = 0;
            this.removeAllFiltersButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.removeAllFiltersButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.removeAllFiltersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeAllFiltersButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeAllFiltersButton.ForeColor = System.Drawing.Color.Black;
            this.removeAllFiltersButton.Location = new System.Drawing.Point(7, 439);
            this.removeAllFiltersButton.Name = "removeAllFiltersButton";
            this.removeAllFiltersButton.Size = new System.Drawing.Size(175, 29);
            this.removeAllFiltersButton.TabIndex = 63;
            this.removeAllFiltersButton.Text = "Remove All Filters";
            this.removeAllFiltersButton.UseVisualStyleBackColor = false;
            this.removeAllFiltersButton.Click += new System.EventHandler(this.RemoveAllFiltersButton_Click);
            // 
            // showValidQuestsButton
            // 
            this.showValidQuestsButton.BackColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.showValidQuestsButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.FlatAppearance.BorderSize = 0;
            this.showValidQuestsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.showValidQuestsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.showValidQuestsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showValidQuestsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showValidQuestsButton.ForeColor = System.Drawing.Color.Black;
            this.showValidQuestsButton.Location = new System.Drawing.Point(7, 404);
            this.showValidQuestsButton.Name = "showValidQuestsButton";
            this.showValidQuestsButton.Size = new System.Drawing.Size(175, 29);
            this.showValidQuestsButton.TabIndex = 62;
            this.showValidQuestsButton.Text = "Show Valid Quests";
            this.showValidQuestsButton.UseVisualStyleBackColor = false;
            this.showValidQuestsButton.Click += new System.EventHandler(this.ShowValidQuestsButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.BackColor = System.Drawing.Color.Transparent;
            this.SearchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SearchButton.FlatAppearance.BorderSize = 0;
            this.SearchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.ForeColor = System.Drawing.Color.Black;
            this.SearchButton.Location = new System.Drawing.Point(694, 22);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(100, 29);
            this.SearchButton.TabIndex = 66;
            this.SearchButton.Text = "Search▾";
            this.SearchButton.UseVisualStyleBackColor = false;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.BackColor = System.Drawing.Color.Black;
            this.SearchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTextBox.ForeColor = System.Drawing.Color.White;
            this.SearchTextBox.Location = new System.Drawing.Point(800, 25);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(150, 22);
            this.SearchTextBox.TabIndex = 65;
            this.SearchTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchTextBox_KeyUp);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.exitButton.Location = new System.Drawing.Point(954, 18);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(42, 37);
            this.exitButton.TabIndex = 64;
            this.exitButton.TabStop = false;
            // 
            // CardQuantityLabel
            // 
            this.CardQuantityLabel.AutoSize = true;
            this.CardQuantityLabel.BackColor = System.Drawing.Color.Transparent;
            this.CardQuantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardQuantityLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.CardQuantityLabel.Location = new System.Drawing.Point(121, 29);
            this.CardQuantityLabel.Name = "CardQuantityLabel";
            this.CardQuantityLabel.Size = new System.Drawing.Size(63, 16);
            this.CardQuantityLabel.TabIndex = 63;
            this.CardQuantityLabel.Text = "0 cards.";
            // 
            // FilterButton
            // 
            this.FilterButton.BackColor = System.Drawing.Color.Transparent;
            this.FilterButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FilterButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FilterButton.FlatAppearance.BorderSize = 0;
            this.FilterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.FilterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.FilterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FilterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilterButton.ForeColor = System.Drawing.Color.Black;
            this.FilterButton.Location = new System.Drawing.Point(27, 22);
            this.FilterButton.Name = "FilterButton";
            this.FilterButton.Size = new System.Drawing.Size(90, 29);
            this.FilterButton.TabIndex = 62;
            this.FilterButton.Text = "Filter";
            this.FilterButton.UseVisualStyleBackColor = false;
            this.FilterButton.Click += new System.EventHandler(this.FilterButton_Click);
            // 
            // DeckBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.TopCardPanel);
            this.Controls.Add(this.BottomPanel);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.CardQuantityLabel);
            this.Controls.Add(this.FilterButton);
            this.DoubleBuffered = true;
            this.Name = "DeckBuilder";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.DeckBuilder_Load);
            this.BottomPanel.ResumeLayout(false);
            this.MenuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.validDeckPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel TopCardPanel;
        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.FlowLayoutPanel BottomCardPanel;
        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Button NewDeckButton;
        private System.Windows.Forms.Button OpenDeckButton;
        private System.Windows.Forms.Button SaveDeckButton;
        private System.Windows.Forms.Button ToolsButton;
        private System.Windows.Forms.Button HelperButton;
        private System.Windows.Forms.Button UnitsButton;
        private System.Windows.Forms.PictureBox validDeckPictureBox;
        private System.Windows.Forms.Button standardButton;
        private System.Windows.Forms.Button ItemsButton;
        private System.Windows.Forms.Button TacticsButton;
        private System.Windows.Forms.Button AbilitiesButton;
        private System.Windows.Forms.Button DrawDeckButton;
        private System.Windows.Forms.Button QuestsButton;
        private System.Windows.Forms.Button AvatarButton;
        private System.Windows.Forms.Button removeAllFiltersButton;
        private System.Windows.Forms.Button showValidQuestsButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.Label CardQuantityLabel;
        private System.Windows.Forms.Button FilterButton;
        private System.Windows.Forms.Button NextPageButton;
        private System.Windows.Forms.Button PreviousPageButton;
    }
}