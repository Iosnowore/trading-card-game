﻿namespace TradingCardGame
{
    partial class Map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Map));
            this.bottomButtonsPanel = new System.Windows.Forms.Panel();
            this.exitButton = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.mainButtonsPanel = new System.Windows.Forms.Panel();
            this.mainInfoPanel = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.leaderboardsListViewer = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.viewMoreButton = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.motdTextBox = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.mainLeftPanel = new System.Windows.Forms.Panel();
            this.gettingStartedLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tcgLogo = new System.Windows.Forms.PictureBox();
            this.bottomButtonsPanel.SuspendLayout();
            this.exitButton.SuspendLayout();
            this.mainInfoPanel.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.mainLeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcgLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomButtonsPanel
            // 
            this.bottomButtonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.bottomButtonsPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bottomButtonsPanel.BackgroundImage")));
            this.bottomButtonsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bottomButtonsPanel.Controls.Add(this.exitButton);
            this.bottomButtonsPanel.Location = new System.Drawing.Point(3, 661);
            this.bottomButtonsPanel.Name = "bottomButtonsPanel";
            this.bottomButtonsPanel.Size = new System.Drawing.Size(1018, 104);
            this.bottomButtonsPanel.TabIndex = 22;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Black;
            this.exitButton.Controls.Add(this.label13);
            this.exitButton.Location = new System.Drawing.Point(800, 8);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(192, 88);
            this.exitButton.TabIndex = 45;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            this.exitButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseDown);
            this.exitButton.MouseEnter += new System.EventHandler(this.exitButton_MouseEnter);
            this.exitButton.MouseLeave += new System.EventHandler(this.exitButton_MouseLeave);
            this.exitButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.exitButton_MouseUp);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.label13.Location = new System.Drawing.Point(90, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 19);
            this.label13.TabIndex = 9;
            this.label13.Text = "Exit";
            // 
            // mainButtonsPanel
            // 
            this.mainButtonsPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainButtonsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainButtonsPanel.Location = new System.Drawing.Point(666, 6);
            this.mainButtonsPanel.Name = "mainButtonsPanel";
            this.mainButtonsPanel.Size = new System.Drawing.Size(350, 650);
            this.mainButtonsPanel.TabIndex = 50;
            // 
            // mainInfoPanel
            // 
            this.mainInfoPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainInfoPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainInfoPanel.Controls.Add(this.panel8);
            this.mainInfoPanel.Controls.Add(this.panel7);
            this.mainInfoPanel.Controls.Add(this.mainLeftPanel);
            this.mainInfoPanel.Location = new System.Drawing.Point(5, 6);
            this.mainInfoPanel.Name = "mainInfoPanel";
            this.mainInfoPanel.Size = new System.Drawing.Size(650, 650);
            this.mainInfoPanel.TabIndex = 51;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.leaderboardsListViewer);
            this.panel8.Controls.Add(this.viewMoreButton);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Location = new System.Drawing.Point(336, 307);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(290, 320);
            this.panel8.TabIndex = 2;
            // 
            // leaderboardsListViewer
            // 
            this.leaderboardsListViewer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.leaderboardsListViewer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.leaderboardsListViewer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderboardsListViewer.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.leaderboardsListViewer.FullRowSelect = true;
            this.leaderboardsListViewer.HideSelection = false;
            this.leaderboardsListViewer.Location = new System.Drawing.Point(7, 25);
            this.leaderboardsListViewer.MultiSelect = false;
            this.leaderboardsListViewer.Name = "leaderboardsListViewer";
            this.leaderboardsListViewer.Size = new System.Drawing.Size(275, 255);
            this.leaderboardsListViewer.TabIndex = 3;
            this.leaderboardsListViewer.UseCompatibleStateImageBehavior = false;
            this.leaderboardsListViewer.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Rank";
            this.columnHeader1.Width = 45;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Rating";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 146;
            // 
            // viewMoreButton
            // 
            this.viewMoreButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.viewMoreButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewMoreButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.viewMoreButton.FlatAppearance.BorderSize = 0;
            this.viewMoreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.viewMoreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.viewMoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.viewMoreButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewMoreButton.ForeColor = System.Drawing.Color.Black;
            this.viewMoreButton.Location = new System.Drawing.Point(182, 286);
            this.viewMoreButton.Name = "viewMoreButton";
            this.viewMoreButton.Size = new System.Drawing.Size(100, 29);
            this.viewMoreButton.TabIndex = 50;
            this.viewMoreButton.Text = "View More...";
            this.viewMoreButton.UseVisualStyleBackColor = false;
            this.viewMoreButton.Click += new System.EventHandler(this.viewMoreButton_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.SkyBlue;
            this.label19.Location = new System.Drawing.Point(3, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 20);
            this.label19.TabIndex = 4;
            this.label19.Text = "Leaderboards";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.motdTextBox);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Location = new System.Drawing.Point(336, 22);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(290, 279);
            this.panel7.TabIndex = 1;
            // 
            // motdTextBox
            // 
            this.motdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.motdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.motdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motdTextBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.motdTextBox.Location = new System.Drawing.Point(7, 34);
            this.motdTextBox.Name = "motdTextBox";
            this.motdTextBox.ReadOnly = true;
            this.motdTextBox.Size = new System.Drawing.Size(275, 232);
            this.motdTextBox.TabIndex = 2;
            this.motdTextBox.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.SkyBlue;
            this.label18.Location = new System.Drawing.Point(3, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(158, 18);
            this.label18.TabIndex = 1;
            this.label18.Text = "Message of the Day";
            // 
            // mainLeftPanel
            // 
            this.mainLeftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.mainLeftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainLeftPanel.Controls.Add(this.gettingStartedLabel);
            this.mainLeftPanel.Controls.Add(this.label1);
            this.mainLeftPanel.Controls.Add(this.tcgLogo);
            this.mainLeftPanel.Location = new System.Drawing.Point(27, 22);
            this.mainLeftPanel.Name = "mainLeftPanel";
            this.mainLeftPanel.Size = new System.Drawing.Size(303, 605);
            this.mainLeftPanel.TabIndex = 0;
            // 
            // gettingStartedLabel
            // 
            this.gettingStartedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gettingStartedLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.gettingStartedLabel.Location = new System.Drawing.Point(1, 155);
            this.gettingStartedLabel.Name = "gettingStartedLabel";
            this.gettingStartedLabel.Size = new System.Drawing.Size(39, 15);
            this.gettingStartedLabel.TabIndex = 1;
            this.gettingStartedLabel.Text = "Text...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(0, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "News and Announcements";
            // 
            // tcgLogo
            // 
            this.tcgLogo.Location = new System.Drawing.Point(4, 4);
            this.tcgLogo.Name = "tcgLogo";
            this.tcgLogo.Size = new System.Drawing.Size(280, 128);
            this.tcgLogo.TabIndex = 0;
            this.tcgLogo.TabStop = false;
            // 
            // Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.mainInfoPanel);
            this.Controls.Add(this.bottomButtonsPanel);
            this.Controls.Add(this.mainButtonsPanel);
            this.DoubleBuffered = true;
            this.Name = "Map";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.Map_Load);
            this.SizeChanged += new System.EventHandler(this.Map_SizeChanged);
            this.bottomButtonsPanel.ResumeLayout(false);
            this.exitButton.ResumeLayout(false);
            this.exitButton.PerformLayout();
            this.mainInfoPanel.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.mainLeftPanel.ResumeLayout(false);
            this.mainLeftPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcgLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel bottomButtonsPanel;
        private System.Windows.Forms.Panel mainButtonsPanel;
        private System.Windows.Forms.Panel mainInfoPanel;
        private System.Windows.Forms.Panel exitButton;
        private System.Windows.Forms.Panel mainLeftPanel;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox motdTextBox;
        private System.Windows.Forms.PictureBox tcgLogo;
        private System.Windows.Forms.ListView leaderboardsListViewer;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button viewMoreButton;
        private System.Windows.Forms.Label gettingStartedLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
    }
}