﻿namespace TradingCardGame.Forms.Screens
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.QuickJoinButton = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.activeGamesPanel = new System.Windows.Forms.Panel();
            this.inputChatBox = new System.Windows.Forms.TextBox();
            this.chatBox = new System.Windows.Forms.ListBox();
            this.matchesLabel = new System.Windows.Forms.Label();
            this.usersLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.playNowButton = new System.Windows.Forms.PictureBox();
            this.playersNameListBox = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playNowButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.QuickJoinButton);
            this.panel1.Controls.Add(this.CreateButton);
            this.panel1.Controls.Add(this.activeGamesPanel);
            this.panel1.Controls.Add(this.inputChatBox);
            this.panel1.Controls.Add(this.chatBox);
            this.panel1.Controls.Add(this.matchesLabel);
            this.panel1.Controls.Add(this.usersLabel);
            this.panel1.Location = new System.Drawing.Point(4, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(698, 757);
            this.panel1.TabIndex = 0;
            // 
            // QuickJoinButton
            // 
            this.QuickJoinButton.BackColor = System.Drawing.Color.Transparent;
            this.QuickJoinButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.QuickJoinButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.QuickJoinButton.FlatAppearance.BorderSize = 0;
            this.QuickJoinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.QuickJoinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.QuickJoinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QuickJoinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuickJoinButton.ForeColor = System.Drawing.Color.Black;
            this.QuickJoinButton.Location = new System.Drawing.Point(127, 16);
            this.QuickJoinButton.Name = "QuickJoinButton";
            this.QuickJoinButton.Size = new System.Drawing.Size(120, 29);
            this.QuickJoinButton.TabIndex = 49;
            this.QuickJoinButton.Text = "Quick Join";
            this.QuickJoinButton.UseVisualStyleBackColor = false;
            // 
            // CreateButton
            // 
            this.CreateButton.BackColor = System.Drawing.Color.Transparent;
            this.CreateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CreateButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.CreateButton.FlatAppearance.BorderSize = 0;
            this.CreateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CreateButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CreateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateButton.ForeColor = System.Drawing.Color.Black;
            this.CreateButton.Location = new System.Drawing.Point(22, 16);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(100, 29);
            this.CreateButton.TabIndex = 48;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = false;
            this.CreateButton.Click += new System.EventHandler(this.CreateMatchButton_Click);
            // 
            // activeGamesPanel
            // 
            this.activeGamesPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.activeGamesPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.activeGamesPanel.Location = new System.Drawing.Point(22, 65);
            this.activeGamesPanel.Name = "activeGamesPanel";
            this.activeGamesPanel.Size = new System.Drawing.Size(660, 420);
            this.activeGamesPanel.TabIndex = 7;
            // 
            // inputChatBox
            // 
            this.inputChatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.inputChatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputChatBox.ForeColor = System.Drawing.Color.White;
            this.inputChatBox.Location = new System.Drawing.Point(22, 724);
            this.inputChatBox.MaxLength = 255;
            this.inputChatBox.Name = "inputChatBox";
            this.inputChatBox.Size = new System.Drawing.Size(660, 22);
            this.inputChatBox.TabIndex = 5;
            this.inputChatBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputChatBox_KeyDown);
            // 
            // chatBox
            // 
            this.chatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.chatBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.chatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatBox.ForeColor = System.Drawing.Color.White;
            this.chatBox.FormattingEnabled = true;
            this.chatBox.HorizontalScrollbar = true;
            this.chatBox.ItemHeight = 16;
            this.chatBox.Location = new System.Drawing.Point(22, 492);
            this.chatBox.Name = "chatBox";
            this.chatBox.Size = new System.Drawing.Size(660, 228);
            this.chatBox.TabIndex = 4;
            this.chatBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.ChatBox_DrawItem);
            this.chatBox.SelectedIndexChanged += new System.EventHandler(this.ChatBox_SelectedIndexChanged);
            // 
            // matchesLabel
            // 
            this.matchesLabel.AutoSize = true;
            this.matchesLabel.BackColor = System.Drawing.Color.Transparent;
            this.matchesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matchesLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.matchesLabel.Location = new System.Drawing.Point(259, 31);
            this.matchesLabel.Name = "matchesLabel";
            this.matchesLabel.Size = new System.Drawing.Size(72, 16);
            this.matchesLabel.TabIndex = 3;
            this.matchesLabel.Text = "Matches: 0";
            // 
            // usersLabel
            // 
            this.usersLabel.AutoSize = true;
            this.usersLabel.BackColor = System.Drawing.Color.Transparent;
            this.usersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.usersLabel.Location = new System.Drawing.Point(259, 15);
            this.usersLabel.Name = "usersLabel";
            this.usersLabel.Size = new System.Drawing.Size(74, 16);
            this.usersLabel.TabIndex = 2;
            this.usersLabel.Text = "Users: 0 / 6";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.playNowButton);
            this.panel2.Location = new System.Drawing.Point(715, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 480);
            this.panel2.TabIndex = 1;
            // 
            // playNowButton
            // 
            this.playNowButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.playNowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.playNowButton.Location = new System.Drawing.Point(23, 12);
            this.playNowButton.Name = "playNowButton";
            this.playNowButton.Size = new System.Drawing.Size(248, 178);
            this.playNowButton.TabIndex = 0;
            this.playNowButton.TabStop = false;
            this.playNowButton.Click += new System.EventHandler(this.PlayNowButton_Click);
            this.playNowButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PlayNowButton_MouseDown);
            this.playNowButton.MouseEnter += new System.EventHandler(this.PlayNowButton_MouseEnter);
            this.playNowButton.MouseLeave += new System.EventHandler(this.PlayNowButton_MouseLeave);
            // 
            // playersNameListBox
            // 
            this.playersNameListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.playersNameListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.playersNameListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playersNameListBox.ForeColor = System.Drawing.Color.White;
            this.playersNameListBox.FullRowSelect = true;
            this.playersNameListBox.HideSelection = false;
            this.playersNameListBox.Location = new System.Drawing.Point(715, 494);
            this.playersNameListBox.MultiSelect = false;
            this.playersNameListBox.Name = "playersNameListBox";
            this.playersNameListBox.Size = new System.Drawing.Size(300, 260);
            this.playersNameListBox.TabIndex = 2;
            this.playersNameListBox.UseCompatibleStateImageBehavior = false;
            this.playersNameListBox.View = System.Windows.Forms.View.Details;
            this.playersNameListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PlayersNameListBox_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Username";
            this.columnHeader1.Width = 288;
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.playersNameListBox);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "Lobby";
            this.Size = new System.Drawing.Size(1024, 768);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playNowButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label matchesLabel;
        private System.Windows.Forms.TextBox inputChatBox;
        private System.Windows.Forms.PictureBox playNowButton;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Button QuickJoinButton;
        private System.Windows.Forms.Label usersLabel;
        private System.Windows.Forms.ListBox chatBox;
        private System.Windows.Forms.ListView playersNameListBox;
        private System.Windows.Forms.Panel activeGamesPanel;
    }
}