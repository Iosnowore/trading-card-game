﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.forms.widgets;
using TradingCardGame.services.packet;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Font;

namespace TradingCardGame.Forms.Screens
{
    public partial class TCG : Form
    {
        private static bool DialogIsOpen;

        private static readonly Navigator NavBar = new Navigator();

        private UserControl ChildWindow;

        private readonly List<SystemMessage> SystemMessages = new List<SystemMessage>();

        private delegate void NoParamsDelegate();
        private delegate void OneParamDelegate(UserControl childWindow);

        public TCG()
        {
            InitializeComponent();
            MinimumSize = new Size(1024, 768);

            CustomFontService.LoadCustomFont();
            DiscordPresenceService.InitializePresence();

            SetChildWindow(new Login());
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void Game_MouseEnter(object sender, EventArgs e)
        {
            //Cursor.Clip = Bounds;
        }

        private void Game_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ChildWindow is Login)
                return;

            PacketService.DisconnectFromServer();
        }

        private void DisposeChildWindow()
        {
            if (ChildWindow != null)
            {
                if (ChildWindow is Lobby || ChildWindow is Src.Forms.Screens.Game)
                {
                    DiscordPresenceService.SetParty(string.Empty, 0, 0);
                }

                if (ChildWindow.InvokeRequired)
                    ChildWindow.Invoke(new NoParamsDelegate(DisposeChildWindow));
                else
                    ChildWindow.Dispose();
            }
        }

        private void AddChildWindow(UserControl ChildWindow)
        {
            if (InvokeRequired)
                Invoke(new OneParamDelegate(SetChildWindow), ChildWindow);
            else
            {
                Controls.Add(ChildWindow);
                this.ChildWindow = ChildWindow;
            }
        }

        public void SetChildWindow(UserControl childWindow)
        {
            DisposeChildWindow();
            AddChildWindow(childWindow);

            foreach (SystemMessage Message in SystemMessages)
            {
                Message.Dispose();
                SystemMessages.Remove(Message);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public UserControl GetChildWindow()
        {
            return ChildWindow;
        }

        public void AddNavigator()
        {
            if (InvokeRequired)
                Invoke(new NoParamsDelegate(AddNavigator));
            else
                Controls.Add(NavBar);
        }
        public bool MouseIsInNavigator()
        {
            return NavBar.ClientRectangle.Contains(PointToClient(MousePosition));
        }

        private void TCG_Layout(object sender, LayoutEventArgs e)
        {
            if (e.AffectedControl is NavigatorAddon || e.AffectedControl is Navigator)
                return;

            NavBar.BringToFront();
        }

        private void TCG_SizeChanged(object sender, EventArgs e)
        {
            NavBar.Location = new Point(NavBar.Location.X, (ClientSize.Height - NavBar.Height) / 2);
        }

        public Navigator GetNavigator()
        {
            return NavBar;
        }

        public static void SetDialogStatus(bool DialogIsOpen)
        {
            TCG.DialogIsOpen = DialogIsOpen;
        }

        public static bool GetDialogStatus()
        {
            return DialogIsOpen;
        }

        public void AddSystemMessage(string Text, string Title)
        {
            // Check to see if there is a similar message, if so, remove the old one
            foreach (SystemMessage Message in SystemMessages)
            {
                if (Message.GetTitle().Equals(Title))
                {
                    Message.Dispose();
                }
            }

            SystemMessage SystemMessage = new SystemMessage(Text, Title);
            SystemMessage.Show();

            SystemMessages.Add(SystemMessage);
        }

        public void RemoveSystemMessage(SystemMessage Message)
        {
            SystemMessages.Remove(Message);
        }
    }
}
