﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using String;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData.Packets.Player;
using TradingCardGame.components;
using TradingCardGame.Components;
using TradingCardGame.forms.dialogs;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Player;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame
{
    public partial class Map : UserControl
    {
        public Map()
        {
            InitializeComponent();
            gettingStartedLabel.Size = new Size(mainLeftPanel.Width, 500);

            tcgLogo.Image = ResourcesLib.GetImageFromMapScreen("logo");
            BackgroundImage = ResourcesLib.GetImageFromBackgrounds("game_bg_02");
            exitButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_exit");
            Dock = DockStyle.Fill;

            for (byte i = 0; i < 4; i++)
                mainButtonsPanel.Controls.Add(new MapButton(i));
            for (byte i = 0; i < 3; i++)
                bottomButtonsPanel.Controls.Add(new MapButtonSmall(i));

            motdTextBox.Text = StringLib.GetString("map:motd");
            gettingStartedLabel.Text = StringLib.GetString("map:gettingstarted");
            leaderboardsListViewer.Items.AddRange(PlayerService.GetLeaderboardItems());

            for (int i = 0; i < leaderboardsListViewer.Items.Count; i++)
                if (i % 2 != 0)
                    leaderboardsListViewer.Items[i].BackColor = Color.DimGray;

            DiscordPresenceService.UpdateDetails("Main Screen");
            DiscordPresenceService.UpdatePartyState(null);

            ButtonAnimation.FormatButton(viewMoreButton);

            mainInfoPanel.BackgroundImage = ResourcesLib.GetImageFromBorders("navframe");
            mainButtonsPanel.BackgroundImage = ResourcesLib.GetImageFromBorders("navframe");
            bottomButtonsPanel.BackgroundImage = ResourcesLib.GetImageFromBorders("navframe");
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            CommandService.ExitGame();
        }

        private void deckBuilderButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            ((TCG)Parent).SetChildWindow(new DeckBuilder());
        }

        private void collectionsButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            ((TCG)Parent).SetChildWindow(new CollectionManager());
        }

        private void exitButton_MouseDown(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_exit_down");
        }

        private void exitButton_MouseLeave(object sender, EventArgs e)
        {
            if (!exitButton.ClientRectangle.Contains(exitButton.PointToClient(MousePosition)))
                exitButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_exit");
        }

        private void exitButton_MouseUp(object sender, MouseEventArgs e)
        {
            exitButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_exit_over");
        }

        private void exitButton_MouseEnter(object sender, EventArgs e)
        {
            exitButton.BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_exit_over");
        }


        private void preferencesButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Controls.Add(new Window(new Preferences(), "Preferences"));
        }

        private void viewMoreButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (leaderboardsListViewer.SelectedItems.Count > 0)
            {
                TCG.SetDialogStatus(true);

                OpenUserInfo(PacketService.GetUserInfoPacket(leaderboardsListViewer.SelectedItems[0].SubItems[2].Text));
            }
        }

        private delegate void OpenUserInfoDelegate(UserInfoPacket UserInfoPacket);

        public void OpenUserInfo(UserInfoPacket UserInfoPacket)
        {
            if (InvokeRequired)
                Invoke(new OpenUserInfoDelegate(OpenUserInfo), UserInfoPacket);
            else
                Controls.Add(new Window(new UserInfo(UserInfoPacket), UserInfoPacket.GetUsername()));
        }

        private void Map_SizeChanged(object sender, EventArgs e)
        {
            int heightOfObjects = bottomButtonsPanel.Location.Y + bottomButtonsPanel.Height - mainInfoPanel.Location.Y;
            bottomButtonsPanel.Location = new Point((ClientSize.Width - bottomButtonsPanel.Width) / 2, ClientSize.Height - (ClientSize.Height - heightOfObjects) / 2 - bottomButtonsPanel.Height);
            mainInfoPanel.Location = new Point(bottomButtonsPanel.Location.X, bottomButtonsPanel.Location.Y - mainInfoPanel.Height - 6);
            mainButtonsPanel.Location = new Point(mainInfoPanel.Location.X + mainInfoPanel.Width + 5, mainInfoPanel.Location.Y);
        }

        private void Map_Load(object sender, EventArgs e)
        {
            MusicService.TransitionMusic("music_lobby_", 2);

            if (!SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "MainScreen"))
                Controls.Add(new Window(new PlayerTips(PlayerTips.Types.MainScreen), "Player Tips"));
            exitButton.BringToFront();
        }
    }
}
