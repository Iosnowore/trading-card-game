﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TradingCardGame.Components;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.guild;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Player;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame
{
    public partial class Login : UserControl
    {
        private Point p = new Point();
        private bool connectingToServer;
        private LoggingInMessage LoggingInMessage;

        private readonly string[] SplashImages = { "tcglogo", "tcgemu_icon", "menubackground_03" };

        private int Index;

        public Login()
        {
            InitializeComponent();
            SettingsService.CreateSettingsXmlIfDoesntExist();

            BackgroundImage = ResourcesLib.GetImageFromBackgrounds(SplashImages[Index]);

            Bitmap b = new Bitmap(BackgroundImage);
            b.MakeTransparent(Color.Black);
            logoPictureBox.Image = b;

            MusicService.PlayMusic("music_intro");
            Dock = DockStyle.Fill;
        }

        public void LoginTimedOut()
        {
            connectingToServer = false;
            mainPanel.Visible = true;
        }

        private void LoginFunction()
        {
            if (!connectingToServer && !string.IsNullOrEmpty(usernameTextBox.Text) && !string.IsNullOrEmpty(passwordTextBox.Text))
            {
                connectingToServer = true;
                mainPanel.Visible = false;
                LoggingInMessage = new LoggingInMessage(this);
                Controls.Add(LoggingInMessage);
                PacketService.ConnectToServer((TCG)Parent, usernameTextBox.Text, passwordTextBox.Text);
            }
        }

        private delegate void _DisposeLoggingInMessage();

        private void DisposeLoggingInMessage()
        {
            if (LoggingInMessage.InvokeRequired)
                LoggingInMessage.Invoke(new _DisposeLoggingInMessage(DisposeLoggingInMessage));
            else
            {
                LoggingInMessage.Dispose();
                LoginTimedOut();
            }
        }

        public void HandleLoginValidationPacket(PacketSender p)
        {
            LoginValidationResponsePacket Packet = (LoginValidationResponsePacket)p.GetPacket();

            switch (Packet.GetResponse())
            {
                case LoginPacketResponses.Responses.WRONG_VERSION:
                    ((TCG)Parent).AddSystemMessage("Your client is outdated. Please update your client.", "Outdated Client");
                    DisposeLoggingInMessage();
                    PacketService.DisconnectFromServer();
                    break;
                case LoginPacketResponses.Responses.BANNED:
                    ((TCG)Parent).AddSystemMessage("You are currently banned.", "Banned");
                    DisposeLoggingInMessage();
                    PacketService.DisconnectFromServer();
                    break;
                case LoginPacketResponses.Responses.FAILURE:
                    ((TCG)Parent).AddSystemMessage("Login information invalid.", "Invalid Login Information");
                    DisposeLoggingInMessage();
                    PacketService.DisconnectFromServer();
                    break;
                case LoginPacketResponses.Responses.CLOSED:
                    ((TCG)Parent).AddSystemMessage("The TCG server is currently only open for staff members only.", "Server Closed");
                    DisposeLoggingInMessage();
                    PacketService.DisconnectFromServer();
                    break;
                case LoginPacketResponses.Responses.SUCCESS:
                    PacketService.SetId(Packet.GetUserId());
                    ResourcesLib.LoadImageList();
                    TCGData.Cards.LoadGameCards();
                    PlayerService.SetLeaderboardItems(Packet.GetLeaderboardInfo());
                    PlayerService.SetUsername(Packet.GetUsername());
                    TCG TCG = (TCG)Parent;
                    TCG.AddNavigator();
                    TCG.SetChildWindow(new Map());

                    if (!string.IsNullOrEmpty(Packet.GetGuildName()))
                        GuildService.SetGuildName(Packet.GetGuildName());

                    if (!string.IsNullOrEmpty(Packet.GetGuildMessage()))
                        HandleGuildMessage(TCG, Packet.GetGuildMessage());
                    break;
            }
        }

        private delegate void _HandleGuildMessage(TCG TCG, string Message);
        private void HandleGuildMessage(TCG TCG, string Message)
        {
            if (TCG.InvokeRequired)
                TCG.Invoke(new _HandleGuildMessage(HandleGuildMessage), TCG, Message);
            else
                TCG.Controls.Add(new Window(new GuildMessage(Message), "Guild Message"));
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape || e.KeyCode == Keys.CapsLock)
                e.SuppressKeyPress = true;
            SoundService.KeyPressSound(e.KeyCode);

            if (e.KeyCode == Keys.Enter)
                LoginFunction();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            LoginFunction();
        }

        private void UsernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape || e.KeyCode == Keys.CapsLock)
            {
                e.SuppressKeyPress = true;

                if (e.KeyCode == Keys.Enter)
                    LoginFunction();
            }
            SoundService.KeyPressSound(e.KeyCode);
        }

        private void SetLoginScreen()
        {
            switchLogoTimer.Enabled = false;
            mainPanel.Location = new Point((Width - mainPanel.Width) / 2, (Height - mainPanel.Height) / 2);
            BackgroundImageLayout = ImageLayout.Stretch;
            BackgroundImage = ResourcesLib.GetImageFromBackgrounds("lobby_background_casual");
            mainPanel.Visible = true;
            DiscordPresenceService.UpdateDetails("Logging In");
        }

        private void SwitchLogoTimer_Tick(object sender, EventArgs e)
        {
            Index++;

            if (Index < SplashImages.Length)
                BackgroundImage = ResourcesLib.GetImageFromBackgrounds(SplashImages[Index]);
            else
                SetLoginScreen();
        }

        private void LoginPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.ExitThread();
        }

        private void MainPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                mainPanel.Location = new Point(e.X - p.X + mainPanel.Location.X, e.Y - p.Y + mainPanel.Location.Y);
        }

        private void MainPanel_MouseDown(object sender, MouseEventArgs e)
        {
            p = new Point(e.X, e.Y);
        }

        private void MainPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (mainPanel.Location.X > Width || mainPanel.Location.X < (0 - mainPanel.Width) || mainPanel.Location.Y > Height || mainPanel.Location.Y < (0 - mainPanel.Height))
                mainPanel.Location = new Point((Width - mainPanel.Width) / 2, (Height - mainPanel.Height) / 2);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
