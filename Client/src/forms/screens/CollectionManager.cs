﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TradingCardGame.Components;
using TradingCardGame.forms.dialogs;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Forms.Widgets;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Collection;
using TradingCardGame.Src.Services.Discord;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame
{
    public partial class CollectionManager : UserControl
    {
        private readonly Card[] currentCards = new Card[10];
        private readonly CollectionCardInfoPanel[] infoPanels = new CollectionCardInfoPanel[10];
        private readonly FilterPanel FilterPanel = new FilterPanel();
        private readonly ViewOption ViewOption;
        private readonly CardTableView TableView;
        private Card previewCard;
        private DropDownMenu dropDownMenu;
        private RedemptionDetail RedemptionDetail;
        private RedemptionConfirmation RedemptionConfirmation;
        private Delivery Delivery;
        private int PageIndex;
        private int MaxPageIndex;
        private LoadType loadType;
        private readonly byte CARDS_PER_PAGE = 10;

        private Button LastClickedButton;
        private readonly bool[] SearchOptions = new bool[] { true, true, true, true, true };

        private CollectionPacket MyCollection;

        private readonly bool ShowOnlyMyCards;

        private bool LoadingCards;

        private int[] ids;

        public enum LoadType
        {
            Packs,
            PromoPacks,
            RedeemableItems,
            RedeemedItems,
            COTF
        }

        public CollectionManager()
        {
            InitializeComponent();

            MyCollection = PacketService.GetCollectionPacket();
            ShowOnlyMyCards = SettingsService.GetBoolFromSettingsXml("ShowOnlyCardsIOwn", "CollectionManager");

            ViewOption = new ViewOption(this);
            ViewOption.Location = new Point(SearchButton.Location.X - ViewOption.Width, ViewOption.Location.Y);
            firstBackgroundPanel.Controls.Add(ViewOption);

            TableView = new CardTableView();
            TableView.Size = new Size(950, 660);
            TableView.Location = new Point(20, 15);
            panel1.Controls.Add(TableView);

            Dock = DockStyle.Fill;

            BackgroundImage = ResourcesLib.GetImageFromBackgrounds("dialog_bg");
            pictureBox1.Image = ResourcesLib.GetImageFromBinder("binder_ul");
            pictureBox2.Image = ResourcesLib.GetImageFromBinder("binder_ur");
            pictureBox3.Image = ResourcesLib.GetImageFromBinder("binder_lr");
            pictureBox4.Image = ResourcesLib.GetImageFromBinder("binder_ll");
            pictureBox5.Image = ResourcesLib.GetImageFromBinder("binder_centercap_t");
            pictureBox10.Image = ResourcesLib.GetImageFromBinder("binder_centercap_b");
            TopBar.BackgroundImage = ResourcesLib.GetImageFromBinder("binder_top");
            RightBar.BackgroundImage = ResourcesLib.GetImageFromBinder("binder_right");
            LeftBar.BackgroundImage = ResourcesLib.GetImageFromBinder("binder_left");
            BottomBar.BackgroundImage = ResourcesLib.GetImageFromBinder("binder_top");
            PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
            NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
            panel1.BackgroundImage = ResourcesLib.GetImageFromBinder("collection_manager_background");
            firstBackgroundPanel.BackgroundImage = ResourcesLib.GetImageFromBorders("navframe_header");

            ButtonAnimation.FormatButton(SearchButton);
            ButtonAnimation.FormatButton(filterButton);
            ButtonAnimation.FormatButton(JumpToButton);

            numberOfCardsLabel.Text = CollectionService.GetNumberOfOwnedCards(MyCollection) + " cards.";
            numberOfCardsLabel.Location = new Point(ViewOption.Location.X - numberOfCardsLabel.Width, numberOfCardsLabel.Location.Y);

            for (int i = 0; i < CARDS_PER_PAGE; i++)
            {
                infoPanels[i] = new CollectionCardInfoPanel(i);
                panel1.Controls.Add(infoPanels[i]);
            }

            FilterPanel.Location = new Point(filterButton.Location.X, filterButton.Location.Y + filterButton.Height);
            firstBackgroundPanel.Controls.Add(FilterPanel);
            FilterPanel.Visible = false;

            DiscordPresenceService.UpdateDetails("Viewing Card Collection");
            DiscordPresenceService.UpdatePartyState(null);
        }

        public void ChangeSearchOptions(int Index)
        {
            dropDownMenu.ChangeCheck(Index);
            SearchOptions[Index] = !SearchOptions[Index];
            LoadCards();
        }

        public int GetCardId(int Index)
        {
            return ids[Index];
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private static readonly int[] X_VALUES = {
            30,
            215,
            400,
            585,
            770
        };

        private static readonly int[] Y_VALUES = {
            20,
            340
        };

        private int[] GetCardIdsForPage(int[] AllCardIds)
        {
            int[] CardIdsForPage = new int[CARDS_PER_PAGE];

            int StartIndex = PageIndex * CARDS_PER_PAGE;

            for (int i = 0; i < CardIdsForPage.Length; i++)
            {
                // Make sure we don't go out of bounds
                if (AllCardIds.Length > StartIndex + i)
                    CardIdsForPage[i] = AllCardIds[StartIndex + i];
                else
                    CardIdsForPage[i] = -1;
            }
            return CardIdsForPage;
        }

        public void LoadCards(int[] ids, LoadType loadType)
        {
            LoadingCards = true;

            this.ids = ids;
            this.loadType = loadType;

            MaxPageIndex = ids.Length / CARDS_PER_PAGE;

            if (ids.Length % CARDS_PER_PAGE == 0)
                MaxPageIndex--;

            if (PageIndex > MaxPageIndex)
                PageIndex = 0;

            string NextPageButtonImage = "binder_nextpage";

            if (PageIndex == MaxPageIndex)
                NextPageButtonImage += "_disabled";

            NextPageButton.Image = ResourcesLib.GetImageFromBinder(NextPageButtonImage);

            CardData CardData = null;
            PackData PackData = null;

            // If graphics view, limit cards per page
            if (ViewOption.IsGraphicView())
            {
                int[] CardIdsToShow = GetCardIdsForPage(ids);

                for (int i = 0; i < CardIdsToShow.Length; i++)
                {
                    if (currentCards[i] == null && CardIdsToShow != null)
                    {
                        switch (loadType)
                        {
                            case LoadType.COTF:
                                CardData = TCGData.Cards.GetCardData(CardIdsToShow[i]);
                                currentCards[i] = new Card(CardData, Card.SIZE.SMALL);
                                break;
                            case LoadType.RedeemableItems:
                                CardData = TCGData.Cards.GetLootCardData(CardIdsToShow[i]);
                                currentCards[i] = new Card(CardData, Card.SIZE.SMALL);
                                break;
                            case LoadType.RedeemedItems:
                                CardData = TCGData.Cards.GetLootCardData(CardIdsToShow[i]);
                                currentCards[i] = new Card(CardData, Card.SIZE.SMALL);
                                break;
                            case LoadType.Packs:
                                PackData = TCGData.Cards.GetPackData(CardIdsToShow[i]);
                                currentCards[i] = new Card(PackData, Card.SIZE.SMALL);
                                break;
                            case LoadType.PromoPacks:
                                PackData = TCGData.Cards.GetPromoPackData(CardIdsToShow[i]);
                                currentCards[i] = new Card(PackData, Card.SIZE.SMALL);
                                break;
                        }
                        currentCards[i].MouseHover += CoverPanel_MouseHover;
                        currentCards[i].MouseLeave += CoverPanel_MouseLeave;
                        currentCards[i].Location = new Point(X_VALUES[i > 4 ? i - 5 : i], Y_VALUES[i > 4 ? 1 : 0]);
                        panel1.Controls.Add(currentCards[i]);
                    }

                    if (CardIdsToShow[i] != -1)
                    {
                        switch (loadType)
                        {
                            case LoadType.COTF:
                                CardData = TCGData.Cards.GetCardData(CardIdsToShow[i]);
                                int CardQuantity = CollectionService.GetNumberOfSpecficCard(CardData.GetId(), MyCollection);
                                int WantAmount = CollectionService.GetNumberOfCardsWanted(CardData.GetId(), MyCollection);

                                currentCards[i].ChangeCard(CardData);
                                infoPanels[i].ChangeCardInfo(CardQuantity, WantAmount);
                                currentCards[i].SetRedeemed(false);
                                break;
                            case LoadType.RedeemableItems:
                                CardData = TCGData.Cards.GetLootCardData(CardIdsToShow[i]);
                                CardQuantity = CollectionService.GetNumberOfSpecficRedeemable(CardData.GetId(), MyCollection);

                                currentCards[i].ChangeCard(CardData);
                                infoPanels[i].ChangeCardInfo(CardQuantity, 0);
                                currentCards[i].SetRedeemed(false);
                                break;
                            case LoadType.RedeemedItems:
                                CardData = TCGData.Cards.GetLootCardData(CardIdsToShow[i]);
                                CardQuantity = CollectionService.GetNumberOfSpecficRedeemed(CardData.GetId(), MyCollection);

                                currentCards[i].ChangeCard(CardData);
                                infoPanels[i].ChangeCardInfo(CardQuantity, 0);
                                currentCards[i].SetRedeemed(true);
                                break;
                            case LoadType.Packs:
                                PackData = TCGData.Cards.GetPackData(CardIdsToShow[i]);
                                CardQuantity = CollectionService.GetNumberOfSpecficPack(PackData.GetId(), MyCollection);

                                currentCards[i].ChangeCard(PackData);
                                infoPanels[i].ChangeCardInfo(CardQuantity, 0);
                                currentCards[i].SetRedeemed(false);
                                break;
                            case LoadType.PromoPacks:
                                PackData = TCGData.Cards.GetPromoPackData(CardIdsToShow[i]);
                                CardQuantity = CollectionService.GetNumberOfSpecficPromoPack(PackData.GetId(), MyCollection);

                                currentCards[i].ChangeCard(PackData);
                                infoPanels[i].ChangeCardInfo(CardQuantity, 0);
                                currentCards[i].SetRedeemed(false);
                                break;
                        }
                        currentCards[i].Visible = true;
                        infoPanels[i].Visible = true;
                    }
                    else
                    {
                        if (currentCards[i] != null)
                            currentCards[i].Visible = false;
                        infoPanels[i].Visible = false;
                    }
                }
            }
            else
            {
                // Remove all old items before adding the new ones
                TableView.Items.Clear();

                foreach (int Id in ids)
                {
                    switch (loadType)
                    {
                        case LoadType.COTF:
                            CardData = TCGData.Cards.GetCardData(Id);
                            int CardQuantity = CollectionService.GetNumberOfSpecficCard(Id, MyCollection);
                            break;
                        case LoadType.RedeemableItems:
                            CardData = TCGData.Cards.GetLootCardData(Id);
                            CardQuantity = CollectionService.GetNumberOfSpecficRedeemable(Id, MyCollection);
                            break;
                        case LoadType.RedeemedItems:
                            CardData = TCGData.Cards.GetLootCardData(Id);
                            CardQuantity = CollectionService.GetNumberOfSpecficRedeemed(Id, MyCollection);
                            break;
                        case LoadType.Packs:
                            PackData = TCGData.Cards.GetPackData(Id);
                            CardQuantity = CollectionService.GetNumberOfSpecficPack(Id, MyCollection);
                            break;
                        case LoadType.PromoPacks:
                            PackData = TCGData.Cards.GetPromoPackData(Id);
                            CardQuantity = CollectionService.GetNumberOfSpecficPromoPack(Id, MyCollection);
                            break;
                    }
                    string[] Row = { CardData.GetTitle(), CardData.GetType(), CardData.GetCost().ToString(), CardData.GetArchetype(), CardData.GetCollectorInfo(), CardData.GetAttack().ToString(), CardData.GetDefense().ToString(), CardData.GetBonus().ToString(), CardData.GetHealthOrLevel().ToString() };
                    ListViewItem ListViewItem = new ListViewItem(Row);
                    TableView.Items.Add(ListViewItem);
                }
                // Resize each column to fit
                foreach (ColumnHeader Header in TableView.Columns)
                    Header.Width = -2;
            }
            LoadingCards = false;
        }

        public void SetDelivery(Delivery Delivery)
        {
            this.Delivery = Delivery;
        }

        private CollectionCardInfoPanel GetInfoPanel(Card ClickedCard)
        {
            for (int i = 0; i < currentCards.Length; i++)
                if (ClickedCard == currentCards[i])
                    return infoPanels[i];
            return null;
        }

        private void CoverPanel_MouseLeave(object sender, EventArgs e)
        {
            if (previewCard != null && !((Card)sender).ClientRectangle.Contains(PointToClient(MousePosition)))
            {
                previewCard.Dispose();
                previewCard = null;
            }
        }

        private void CoverPanel_MouseHover(object sender, EventArgs e)
        {
            CardMouseHover((Card)sender);
        }

        public void CardMouseHover(Card Card)
        {
            if (previewCard == null)
                SetPreviewCard(Card);
        }

        private void NextPageButton_Click(object sender, EventArgs e)
        {
            if (LoadingCards)
                return;

            if (PageIndex < MaxPageIndex)
            {
                PageIndex++;
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
                SoundService.PlaySound("page_flip");
                LoadCards();
            }
        }

        private void NextPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (PageIndex < MaxPageIndex)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_down");
        }

        private void NextPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (PageIndex < MaxPageIndex)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void NextPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (PageIndex < MaxPageIndex)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
        }

        private void NextPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (PageIndex < MaxPageIndex)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void PreviousPageButton_Click(object sender, EventArgs e)
        {
            if (LoadingCards)
                return;

            if (PageIndex > 0)
            {
                if (--PageIndex == 0)
                    PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
                SoundService.PlaySound("page_flip");
                LoadCards();
            }
        }

        private void PreviousPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_down");
        }

        private void PreviousPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void PreviousPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
        }

        private void PreviousPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void NavHomeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            Dispose();
        }

        private void SetPreviewCard(Card card)
        {
            if (card.GetCardData() != null)
            {
                previewCard = new Card(card.GetCardData(), Card.SIZE.LARGE);

                if (card.IsShowingLore())
                    previewCard.ShowLore();
            }
            else if (card.GetPackData() != null)
            {
                previewCard = new Card(card.GetPackData(), Card.SIZE.LARGE);
            }

            if (card.GetRedeemed())
                previewCard.SetRedeemed(true);

            previewCard.Location = new Point(card.Location.X == X_VALUES[3] || card.Location.X == X_VALUES[4] ? card.Location.X - previewCard.Width - 10 : card.Location.X + card.Width + 10);
            panel1.Controls.Add(previewCard);
        }

        private void CollectionManager_SizeChanged(object sender, EventArgs e)
        {
            firstBackgroundPanel.Location = new Point((Parent.ClientSize.Width - firstBackgroundPanel.Width) / 2, (Parent.ClientSize.Height - firstBackgroundPanel.Height) / 2);
        }

        private void SearchButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            LoadCards();
        }

        public void LoadCards()
        {
            switch (loadType)
            {
                case LoadType.COTF:
                    int[] CardIds = new int[MyCollection.GetCards().Count];
                    MyCollection.GetCards().Keys.CopyTo(CardIds, 0);
                    int[] FilteredCards = FilterPanel.FilterCardIds(TCGData.Cards.GetCardIdsThatContainsKeyword(SearchTextBox.Text, ShowOnlyMyCards, CardIds, SearchOptions));
                    LoadCards(FilteredCards, loadType);
                    break;
                case LoadType.RedeemableItems:
                    FilteredCards = TCGData.Cards.GetLootCardIdsThatContainsKeyword(SearchTextBox.Text, ShowOnlyMyCards, MyCollection, false);
                    LoadCards(FilteredCards, loadType);
                    break;
                case LoadType.RedeemedItems:
                    FilteredCards = TCGData.Cards.GetLootCardIdsThatContainsKeyword(SearchTextBox.Text, ShowOnlyMyCards, MyCollection, true);
                    LoadCards(FilteredCards, loadType);
                    break;
                case LoadType.Packs:
                    FilteredCards = ShowOnlyMyCards ? CollectionService.GetOwnedPackIds(MyCollection) : TCGData.Cards.GetPackIds();
                    LoadCards(FilteredCards, loadType);
                    break;
                case LoadType.PromoPacks:
                    FilteredCards = ShowOnlyMyCards ? CollectionService.GetOwnedPromoPackIds(MyCollection) : TCGData.Cards.GetPromoPackIds();
                    LoadCards(FilteredCards, loadType);
                    break;
                default:
                    Console.WriteLine("No LoadType selected");
                    break;
            }

            // Destroy any preview card that may have been showing
            if (previewCard != null)
            {
                previewCard.Dispose();
                previewCard = null;
            }
        }

        private void CollectionManager_Load(object sender, EventArgs e)
        {
            if (ShowOnlyMyCards)
            {
                LoadCards(CollectionService.GetOwnedCardIds(MyCollection), LoadType.COTF);
            }
            else
            {
                LoadCards(TCGData.Cards.GetCardIds(), LoadType.COTF);
            }

            if (!SettingsService.GetBoolFromSettingsXml("HidePlayerTips", "Collection"))
                Controls.Add(new Window(new PlayerTips(PlayerTips.Types.Collection), "Player Tips"));
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (dropDownMenu == null || dropDownMenu.IsDisposed)
            {
                LastClickedButton = SearchButton;
                dropDownMenu = new DropDownMenu(new string[] { "Title", "Keywords", "Game Text", "Artist", "Lore" }, SearchOptions)
                {
                    Location = new Point(SearchButton.Location.X, SearchButton.Location.Y + SearchButton.Height)
                };
                firstBackgroundPanel.Controls.Add(dropDownMenu);
                return;
            }
            dropDownMenu.Dispose();
        }

        private void FilterButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
            LastClickedButton = filterButton;
            FilterPanel.Visible = !FilterPanel.Visible;
        }

        private void JumpToButton_Click(object sender, EventArgs e)
        {
            if (dropDownMenu == null || dropDownMenu.IsDisposed)
            {
                LastClickedButton = JumpToButton;
                dropDownMenu = new DropDownMenu(new string[] { "Packs", "Promo Packs", "Redeemable Items", "Redeemed Items", "Champion Of The Force" })
                {
                    Location = new Point(JumpToButton.Location.X, JumpToButton.Location.Y + JumpToButton.Height)
                };
                firstBackgroundPanel.Controls.Add(dropDownMenu);
                return;
            }
            dropDownMenu.Dispose();
        }

        public void OpenPackOrClaimLoot(Card Card)
        {
            // If user right-clicked on the preview card, don't do anything
            if (Card == previewCard)
                return;

            CollectionCardInfoPanel InfoPanel = GetInfoPanel(Card);

            if (InfoPanel.GetCardQuantity() == 0 || (Delivery != null && !Delivery.IsDisposed) || (Card.GetPackData() == null && !Card.GetCardData().GetArchetype().Equals("loot")))
                return;

            SoundService.PlaySound("button");

            if (Card.GetPackData() != null)
            {
                // TODO: Allow multiple packs to be opened at once

                // Determine if this is a regular Pack or a Promo Pack
                if (loadType == LoadType.Packs)
                {
                    PacketService.SendPacket(Commands.OPEN_PACK, new CommandPacket(new object[] { Card.GetPackData().GetId() }));
                }
                else if (loadType == LoadType.PromoPacks)
                {
                    // This must be a 'Choose a Starter' promo
                    if (Card.GetId() == 0)
                    {
                        if (RedemptionConfirmation == null || RedemptionConfirmation.IsDisposed)
                        {
                            RedemptionConfirmation = new RedemptionConfirmation(Card.GetId(), loadType);
                            Controls.Add(new Window(RedemptionConfirmation, "Redemption"));
                        }
                    }
                    else if (RedemptionDetail == null || RedemptionDetail.IsDisposed)
                    {
                        RedemptionDetail = new RedemptionDetail(Card, 1);
                        Controls.Add(new Window(RedemptionDetail, "Redemption"));
                    }
                }
            }
            else
            {
                // Check if we are redeeming 'Choose a Booster Pack'
                if (Card.GetId() == 0)
                {
                    if (RedemptionConfirmation == null || RedemptionConfirmation.IsDisposed)
                    {
                        RedemptionConfirmation = new RedemptionConfirmation(Card.GetId(), loadType);
                        Controls.Add(new Window(RedemptionConfirmation, "Redemption"));
                    }
                }
                else if (RedemptionDetail == null || RedemptionDetail.IsDisposed)
                {
                    RedemptionDetail = new RedemptionDetail(Card, 1);
                    Controls.Add(new Window(RedemptionDetail, "Redemption"));
                }
            }
        }

        public void UpdateCollection(CollectionPacket MyCollection)
        {
            this.MyCollection = MyCollection;

            numberOfCardsLabel.Text = CollectionService.GetNumberOfOwnedCards(MyCollection) + " cards.";
            numberOfCardsLabel.Location = new Point(ViewOption.Location.X - numberOfCardsLabel.Width, numberOfCardsLabel.Location.Y);
        }

        public void CloseRedemptionDetail()
        {
            RedemptionDetail.Parent.Dispose();
        }

        public void DropDownSelected(int Index)
        {
            if (LastClickedButton == JumpToButton)
            {
                MyCollection = PacketService.GetCollectionPacket();

                switch (Index)
                {
                    case 0:
                        int[] CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedPackIds(MyCollection) : TCGData.Cards.GetPackIds();
                        LoadCards(CardIds, LoadType.Packs);
                        break;
                    case 1:
                        CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedPromoPackIds(MyCollection) : TCGData.Cards.GetPromoPackIds();
                        LoadCards(CardIds, LoadType.PromoPacks);
                        break;
                    case 2:
                        CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedRedeemableIds(MyCollection) : TCGData.Cards.GetLootCardIds();
                        LoadCards(CardIds, LoadType.RedeemableItems);
                        break;
                    case 3:
                        CardIds = CollectionService.GetRedeemedCardIds(MyCollection);
                        LoadCards(CardIds, LoadType.RedeemedItems);
                        break;
                    case 4:
                        CardIds = ShowOnlyMyCards ? CollectionService.GetOwnedCardIds(MyCollection) : TCGData.Cards.GetCardIds();
                        LoadCards(CardIds, LoadType.COTF);
                        break;
                }
            }
            else if (LastClickedButton == SearchButton)
            {
                ChangeSearchOptions(Index);
            }
            else if (LastClickedButton == filterButton)
            {
                FilterPanel.DropDownSelected(Index);
            }
        }

        public void UpdateView()
        {
            if (ViewOption.IsGraphicView())
            {
                NextPageButton.Visible = true;
                PreviousPageButton.Visible = true;
            }
            else
            {
                TableView.BringToFront();

                NextPageButton.Visible = false;
                PreviousPageButton.Visible = false;

                foreach (Card Card in currentCards)
                    Card.Visible = false;

                foreach (CollectionCardInfoPanel InfoPanel in infoPanels)
                    InfoPanel.Visible = false;
            }
            TableView.Visible = !TableView.Visible;
            LoadCards();
        }
    }
}
