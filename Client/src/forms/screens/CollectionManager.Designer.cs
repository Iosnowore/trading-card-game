﻿namespace TradingCardGame
{
    partial class CollectionManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstBackgroundPanel = new System.Windows.Forms.Panel();
            this.SearchButton = new System.Windows.Forms.Button();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.JumpToButton = new System.Windows.Forms.Button();
            this.filterButton = new System.Windows.Forms.Button();
            this.numberOfCardsLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LeftBar = new System.Windows.Forms.Panel();
            this.PreviousPageButton = new System.Windows.Forms.Button();
            this.RightBar = new System.Windows.Forms.Panel();
            this.NextPageButton = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.BottomBar = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.TopBar = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.firstBackgroundPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.LeftBar.SuspendLayout();
            this.RightBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // firstBackgroundPanel
            // 
            this.firstBackgroundPanel.BackColor = System.Drawing.Color.Black;
            this.firstBackgroundPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.firstBackgroundPanel.Controls.Add(this.SearchButton);
            this.firstBackgroundPanel.Controls.Add(this.SearchTextBox);
            this.firstBackgroundPanel.Controls.Add(this.JumpToButton);
            this.firstBackgroundPanel.Controls.Add(this.filterButton);
            this.firstBackgroundPanel.Controls.Add(this.numberOfCardsLabel);
            this.firstBackgroundPanel.Controls.Add(this.panel1);
            this.firstBackgroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstBackgroundPanel.Location = new System.Drawing.Point(0, 0);
            this.firstBackgroundPanel.Name = "firstBackgroundPanel";
            this.firstBackgroundPanel.Size = new System.Drawing.Size(1024, 768);
            this.firstBackgroundPanel.TabIndex = 13;
            // 
            // SearchButton
            // 
            this.SearchButton.BackColor = System.Drawing.Color.Transparent;
            this.SearchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SearchButton.FlatAppearance.BorderSize = 0;
            this.SearchButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.ForeColor = System.Drawing.Color.Black;
            this.SearchButton.Location = new System.Drawing.Point(695, 16);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(111, 29);
            this.SearchButton.TabIndex = 55;
            this.SearchButton.Text = "Search▾";
            this.SearchButton.UseVisualStyleBackColor = false;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            this.SearchButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SearchButton_MouseClick);
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.BackColor = System.Drawing.Color.Black;
            this.SearchTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SearchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTextBox.ForeColor = System.Drawing.SystemColors.Control;
            this.SearchTextBox.Location = new System.Drawing.Point(812, 19);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(160, 22);
            this.SearchTextBox.TabIndex = 54;
            this.SearchTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchTextBox_KeyUp);
            // 
            // JumpToButton
            // 
            this.JumpToButton.BackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.JumpToButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.JumpToButton.FlatAppearance.BorderSize = 0;
            this.JumpToButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.JumpToButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JumpToButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JumpToButton.ForeColor = System.Drawing.Color.Black;
            this.JumpToButton.Location = new System.Drawing.Point(130, 19);
            this.JumpToButton.Name = "JumpToButton";
            this.JumpToButton.Size = new System.Drawing.Size(130, 29);
            this.JumpToButton.TabIndex = 53;
            this.JumpToButton.Text = "Jump to... ▼";
            this.JumpToButton.UseVisualStyleBackColor = false;
            this.JumpToButton.Click += new System.EventHandler(this.JumpToButton_Click);
            // 
            // filterButton
            // 
            this.filterButton.BackColor = System.Drawing.Color.Transparent;
            this.filterButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.filterButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.filterButton.FlatAppearance.BorderSize = 0;
            this.filterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.filterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filterButton.ForeColor = System.Drawing.Color.Black;
            this.filterButton.Location = new System.Drawing.Point(34, 19);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(90, 29);
            this.filterButton.TabIndex = 52;
            this.filterButton.Text = "Filter";
            this.filterButton.UseVisualStyleBackColor = false;
            this.filterButton.Click += new System.EventHandler(this.FilterButton_Click);
            // 
            // numberOfCardsLabel
            // 
            this.numberOfCardsLabel.AutoSize = true;
            this.numberOfCardsLabel.BackColor = System.Drawing.Color.Transparent;
            this.numberOfCardsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfCardsLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.numberOfCardsLabel.Location = new System.Drawing.Point(556, 23);
            this.numberOfCardsLabel.Name = "numberOfCardsLabel";
            this.numberOfCardsLabel.Size = new System.Drawing.Size(73, 20);
            this.numberOfCardsLabel.TabIndex = 18;
            this.numberOfCardsLabel.Text = "0 cards.";
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.LeftBar);
            this.panel1.Controls.Add(this.RightBar);
            this.panel1.Controls.Add(this.pictureBox10);
            this.panel1.Controls.Add(this.BottomBar);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.TopBar);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(20, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(985, 685);
            this.panel1.TabIndex = 13;
            // 
            // LeftBar
            // 
            this.LeftBar.BackColor = System.Drawing.Color.Transparent;
            this.LeftBar.Controls.Add(this.PreviousPageButton);
            this.LeftBar.Location = new System.Drawing.Point(0, 76);
            this.LeftBar.Name = "LeftBar";
            this.LeftBar.Size = new System.Drawing.Size(42, 535);
            this.LeftBar.TabIndex = 23;
            // 
            // PreviousPageButton
            // 
            this.PreviousPageButton.FlatAppearance.BorderSize = 0;
            this.PreviousPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PreviousPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviousPageButton.Location = new System.Drawing.Point(1, 228);
            this.PreviousPageButton.Name = "PreviousPageButton";
            this.PreviousPageButton.Size = new System.Drawing.Size(25, 51);
            this.PreviousPageButton.TabIndex = 18;
            this.PreviousPageButton.UseVisualStyleBackColor = true;
            this.PreviousPageButton.Click += new System.EventHandler(this.PreviousPageButton_Click);
            this.PreviousPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseDown);
            this.PreviousPageButton.MouseEnter += new System.EventHandler(this.PreviousPageButton_MouseEnter);
            this.PreviousPageButton.MouseLeave += new System.EventHandler(this.PreviousPageButton_MouseLeave);
            this.PreviousPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PreviousPageButton_MouseUp);
            // 
            // RightBar
            // 
            this.RightBar.BackColor = System.Drawing.Color.Transparent;
            this.RightBar.Controls.Add(this.NextPageButton);
            this.RightBar.Location = new System.Drawing.Point(918, 76);
            this.RightBar.Name = "RightBar";
            this.RightBar.Size = new System.Drawing.Size(69, 535);
            this.RightBar.TabIndex = 23;
            // 
            // NextPageButton
            // 
            this.NextPageButton.FlatAppearance.BorderSize = 0;
            this.NextPageButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.NextPageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextPageButton.Location = new System.Drawing.Point(32, 228);
            this.NextPageButton.Name = "NextPageButton";
            this.NextPageButton.Size = new System.Drawing.Size(25, 51);
            this.NextPageButton.TabIndex = 17;
            this.NextPageButton.UseVisualStyleBackColor = true;
            this.NextPageButton.Click += new System.EventHandler(this.NextPageButton_Click);
            this.NextPageButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseDown);
            this.NextPageButton.MouseEnter += new System.EventHandler(this.NextPageButton_MouseEnter);
            this.NextPageButton.MouseLeave += new System.EventHandler(this.NextPageButton_MouseLeave);
            this.NextPageButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NextPageButton_MouseUp);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.Location = new System.Drawing.Point(421, 603);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(143, 86);
            this.pictureBox10.TabIndex = 10;
            this.pictureBox10.TabStop = false;
            // 
            // BottomBar
            // 
            this.BottomBar.BackColor = System.Drawing.Color.Transparent;
            this.BottomBar.Location = new System.Drawing.Point(76, 679);
            this.BottomBar.Name = "BottomBar";
            this.BottomBar.Size = new System.Drawing.Size(835, 5);
            this.BottomBar.TabIndex = 22;
            this.BottomBar.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Location = new System.Drawing.Point(421, 1);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(143, 86);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // TopBar
            // 
            this.TopBar.BackColor = System.Drawing.Color.Transparent;
            this.TopBar.Location = new System.Drawing.Point(76, 1);
            this.TopBar.Name = "TopBar";
            this.TopBar.Size = new System.Drawing.Size(835, 5);
            this.TopBar.TabIndex = 18;
            this.TopBar.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Location = new System.Drawing.Point(0, 610);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(76, 76);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Location = new System.Drawing.Point(909, 610);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(76, 76);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Location = new System.Drawing.Point(909, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 76);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // CollectionManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.firstBackgroundPanel);
            this.DoubleBuffered = true;
            this.Name = "CollectionManager";
            this.Size = new System.Drawing.Size(1024, 768);
            this.Load += new System.EventHandler(this.CollectionManager_Load);
            this.SizeChanged += new System.EventHandler(this.CollectionManager_SizeChanged);
            this.firstBackgroundPanel.ResumeLayout(false);
            this.firstBackgroundPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.LeftBar.ResumeLayout(false);
            this.RightBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel firstBackgroundPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox TopBar;
        private System.Windows.Forms.Label numberOfCardsLabel;
        private System.Windows.Forms.Button filterButton;
        private System.Windows.Forms.Button JumpToButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.PictureBox BottomBar;
        private System.Windows.Forms.Panel LeftBar;
        private System.Windows.Forms.Panel RightBar;
        private System.Windows.Forms.Button NextPageButton;
        private System.Windows.Forms.Button PreviousPageButton;
    }
}