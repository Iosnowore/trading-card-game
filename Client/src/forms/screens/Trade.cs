﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TCGData.Packets.Trade;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Chat;
using TradingCardGame.Src.Services.Collection;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.Src.Forms.Screens
{
    public partial class Trade : UserControl
    {
        private readonly bool IsPostedTrade;

        private bool Accepted;
        private bool PlayerOnly;
        private bool GuildOnly;

        private const int CARDS_PER_PAGE = 51;
        private const int POSTEDTRADE_CARD_QUANTITY = 4;

        private int TargetPageIndex;
        private readonly int TheirMaxPage;

        private int PageIndex;
        private readonly int MaxPage;

        private readonly CollectionPacket TargetsCollection;
        private readonly CollectionPacket Collection;

        public Trade(string TargetsUsername, CollectionPacket TargetsCollection, CollectionPacket Collection)
        {
            InitializeComponent();

            TableviewGraphic.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_graphic");
            TableviewTable.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_table");

            PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
            NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
            TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
            TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");

            ButtonAnimation.FormatButton(AcceptButton);
            ButtonAnimation.FormatButton(ClearButton);
            ButtonAnimation.FormatButton(ExitButton);
            ButtonAnimation.FormatButton(YouGetAcceptButton);
            ButtonAnimation.FormatButton(filterButton);
            ButtonAnimation.FormatButton(JumpToButton);
            ButtonAnimation.FormatButton(SearchButton);

            ButtonAnimation.ToggleCheckBox(PlayerCheckBox, false);
            ButtonAnimation.ToggleCheckBox(GuildCheckBox, false);

            PacketService.SetTradeLobby(this);

            this.TargetsCollection = TargetsCollection;
            this.Collection = Collection;

            int CardQuantity = LoadTheirCards();
            TheirMaxPage = CardQuantity / CARDS_PER_PAGE;

            CardQuantity = LoadCards();
            MaxPage = CardQuantity / CARDS_PER_PAGE;

            PlayersListView.SmallImageList = ResourcesLib.PLAYER_LIST;
            PlayersListView.Items.Add(PlayerService.GetUsername(), "bronze");

            if (!string.IsNullOrEmpty(TargetsUsername))
                PlayersListView.Items.Add(TargetsUsername, "bronze");
            else
                IsPostedTrade = true;
        }

        private int LoadCards()
        {
            CollectionPanel.Controls.Clear();

            int[] CardIds = new int[Collection.GetCards().Count];
            Collection.GetCards().Keys.CopyTo(CardIds, 0);

            int CardsShown = CARDS_PER_PAGE * PageIndex;

            int NumberOfCardsToShow = CardIds.Length - CardsShown < CARDS_PER_PAGE ? CardIds.Length - CardsShown : CARDS_PER_PAGE;

            Card Card;

            for (int i = 0; i < NumberOfCardsToShow; i++)
            {
                Card = new Card(TCGData.Cards.GetCardData(CardIds[i + CardsShown]), Card.SIZE.SMALL);
                CollectionPanel.Controls.Add(Card);
                Card.SetQuantityLabel(CollectionService.GetNumberOfSpecficCard(CardIds[i + CardsShown], Collection));
            }
            return CardIds.Length;
        }

        private int LoadTheirCards()
        {
            TheirCollectionPanel.Controls.Clear();

            int[] CardIds;
            Card Card;

            int CardsShown = CARDS_PER_PAGE * TargetPageIndex;

            if (TargetsCollection != null)
            {
                CardIds = new int[TargetsCollection.GetCards().Count];
                TargetsCollection.GetCards().Keys.CopyTo(CardIds, 0);

                int NumberOfCardsToShow = CardIds.Length - CardsShown < CARDS_PER_PAGE ? CardIds.Length - CardsShown : CARDS_PER_PAGE;

                for (int i = 0; i < NumberOfCardsToShow; i++)
                {
                    Card = new Card(TCGData.Cards.GetCardData(CardIds[i + CardsShown]), Card.SIZE.SMALL);
                    TheirCollectionPanel.Controls.Add(Card);
                    Card.SetQuantityLabel(CollectionService.GetNumberOfSpecficCard(CardIds[i + CardsShown], TargetsCollection));
                }
            }
            else
            {
                CardIds = TCGData.Cards.GetCardIds();

                int NumberOfCardsToShow = CardIds.Length - CardsShown < CARDS_PER_PAGE ? CardIds.Length - CardsShown : CARDS_PER_PAGE;

                for (int i = 0; i < NumberOfCardsToShow; i++)
                {
                    Card = new Card(TCGData.Cards.GetCardData(CardIds[i + CardsShown]), Card.SIZE.SMALL);
                    TheirCollectionPanel.Controls.Add(Card);
                    Card.SetQuantityLabel(POSTEDTRADE_CARD_QUANTITY);
                }
            }
            return CardIds.Length;
        }

        private Card GetCardFromPanel(int CardId, FlowLayoutPanel Panel)
        {
            Card Card;

            for (int i = 0; i < Panel.Controls.Count; i++)
            {
                Card = (Card)Panel.Controls[i];
                if (Card.GetId() == CardId)
                    return Card;
            }
            return null;
        }

        private bool PanelContainsCardWithId(int CardId, FlowLayoutPanel Panel)
        {
            for (int i = 0; i < Panel.Controls.Count; i++)
                if (((Card)Panel.Controls[i]).GetId() == CardId)
                    return true;
            return false;
        }

        private void TransferCardToPanel(int CardId, FlowLayoutPanel From, FlowLayoutPanel To)
        {
            Card Card = GetCardFromPanel(CardId, From);

            if (PanelContainsCardWithId(CardId, To))
            {
                Card ExistingCard = GetCardFromPanel(CardId, To);

                if (ExistingCard.GetQuantity() > 0)
                    ExistingCard.SetQuantityLabel(ExistingCard.GetQuantity() + 1);
            }
            else
            {
                Card NewCard = new Card(TCGData.Cards.GetCardData(CardId), Card.SIZE.SMALL);
                NewCard.SetQuantityLabel(NewCard.GetQuantity() + 1);
                To.Controls.Add(NewCard);
            }

            if (Card.GetQuantity() == 0)
                return;

            if (Card.GetQuantity() == 1)
                From.Controls.Remove(Card);
            else
                Card.SetQuantityLabel(Card.GetQuantity() - 1);
        }

        public void ChangeCardLocation(Card Card)
        {
            if (Accepted)
                return;

            if (Card.Parent == CollectionPanel)
            {
                if (!IsPostedTrade) PacketService.SendPacket(Commands.TRADE_ADD, new CommandPacket(new string[] { Card.GetId().ToString() }));
                TransferCardToPanel(Card.GetId(), CollectionPanel, TheyGetPanel);
            }
            else if (Card.Parent == TheyGetPanel)
            {
                if (!IsPostedTrade) PacketService.SendPacket(Commands.TRADE_REMOVE, new CommandPacket(new string[] { Card.GetId().ToString() }));
                TransferCardToPanel(Card.GetId(), TheyGetPanel, CollectionPanel);
            }

            if (!IsPostedTrade)
                return;

            if (Card.Parent == TheirCollectionPanel)
            {
                TransferCardToPanel(Card.GetId(), TheirCollectionPanel, YouGetPanel);
            }
            else if (Card.Parent == YouGetPanel)
            {
                TransferCardToPanel(Card.GetId(), YouGetPanel, TheirCollectionPanel);
            }
        }

        private delegate void AddOrRemoveCardDelegate(int CardId);
        public void AddCardYouGet(int CardId)
        {
            if (YouGetPanel.InvokeRequired)
                YouGetPanel.Invoke(new AddOrRemoveCardDelegate(AddCardYouGet), CardId);
            else
                TransferCardToPanel(CardId, TheirCollectionPanel, YouGetPanel);
        }

        public void RemoveCardYouGet(int CardId)
        {
            if (YouGetPanel.InvokeRequired)
                YouGetPanel.Invoke(new AddOrRemoveCardDelegate(RemoveCardYouGet), CardId);
            else
            {
                for (int i = 0; i < YouGetPanel.Controls.Count; i++)
                {
                    if (((Card)YouGetPanel.Controls[i]).GetId() == CardId)
                    {
                        TransferCardToPanel(CardId, YouGetPanel, TheirCollectionPanel);
                        break;
                    }
                }
            }
        }

        public ListBox GetChatBox()
        {
            return ChatBox;
        }

        private void InputChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChatService.ChatBoxKeyEnter(InputChatBox, ChatBox, e);
            SoundService.KeyPressSound(e.KeyCode);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            if (!Accepted)
            {
                PacketService.SendPacket(Commands.TRADE_CANCEL, null);
                Parent.Dispose();
            }
        }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            if (Accepted)
                return;

            SoundService.PlaySound("button");

            if (IsPostedTrade)
            {
                if (YouGetPanel.Controls.Count == 0 || TheyGetPanel.Controls.Count == 0)
                {
                    return;
                }

                Accepted = !Accepted;

                SortedDictionary<int, int> Offered = new SortedDictionary<int, int>();
                SortedDictionary<int, int> Want = new SortedDictionary<int, int>();

                Card Card;

                for (int i = 0; i < TheyGetPanel.Controls.Count; i++)
                {
                    Card = (Card)TheyGetPanel.Controls[i];
                    Offered.Add(Card.GetId(), Card.GetQuantity());
                }

                for (int i = 0; i < YouGetPanel.Controls.Count; i++)
                {
                    Card = (Card)YouGetPanel.Controls[i];
                    Want.Add(Card.GetId(), Card.GetQuantity());
                }

                Parent.Parent.Controls.Add(new Window(new TradeConfirm(new ConfirmTradePacket(Want, Offered), this, true), "Confirm Posted Trade"));
            }
            else
                PacketService.SendPacket(Commands.TRADE_ACCEPT, null);
        }

        private void YouGetAcceptButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void ResetAllCards()
        {
            Card Card;
            for (int i = 0; i < YouGetPanel.Controls.Count; i++)
            {
                Card = (Card)YouGetPanel.Controls[i];
                TransferCardToPanel(Card.GetId(), YouGetPanel, TheirCollectionPanel);
            }

            for (int i = 0; i < TheyGetPanel.Controls.Count; i++)
            {
                Card = (Card)TheyGetPanel.Controls[i];
                TransferCardToPanel(Card.GetId(), TheyGetPanel, CollectionPanel);
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            if (!Accepted)
            {
                if (!IsPostedTrade) PacketService.SendPacket(Commands.TRADE_CLEAR, null);

                ResetAllCards();

                SoundService.PlaySound("button");
            }
        }

        private delegate void _HandleAcceptPacket(bool Accepted);
        public void HandleAcceptPacket(bool Accepted)
        {
            if (YouGetAcceptButton.InvokeRequired)
            {
                YouGetAcceptButton.Invoke(new _HandleAcceptPacket(HandleAcceptPacket), Accepted);
            }
            else
            {
                if (Accepted)
                {
                    YouGetAcceptButton.Text = "Ready";
                    ButtonAnimation.DisableButton(YouGetAcceptButton);
                }
                else
                {
                    YouGetAcceptButton.Text = "Pending";
                }
            }
        }

        private void FilterButton_Click(object sender, EventArgs e)
        {
            if (!Accepted)
            {
                SoundService.PlaySound("button");
            }
        }

        private void JumpToButton_Click(object sender, EventArgs e)
        {
            if (!Accepted)
            {
                SoundService.PlaySound("button");
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (!Accepted)
            {
                SoundService.PlaySound("button");
            }
        }

        private void PlayerCheckBox_Click(object sender, EventArgs e)
        {
            PlayerOnly = !PlayerOnly;

            ButtonAnimation.ToggleCheckBox(PlayerCheckBox, PlayerOnly);

            SoundService.PlaySound("button");
        }

        private void GuildCheckBox_Click(object sender, EventArgs e)
        {
            GuildOnly = !GuildOnly;

            ButtonAnimation.ToggleCheckBox(GuildCheckBox, GuildOnly);

            SoundService.PlaySound("button");
        }

        private void TheirNextPageButton_Click(object sender, EventArgs e)
        {
            if (TargetPageIndex < TheirMaxPage)
            {
                TargetPageIndex++;
                TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
                SoundService.PlaySound("page_flip");
                LoadTheirCards();

                if (TargetPageIndex == TheirMaxPage)
                    TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_disabled");
            }
        }

        private void TheirNextPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (TargetPageIndex < TheirMaxPage)
                TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_down");
        }

        private void TheirNextPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (TargetPageIndex < TheirMaxPage)
                TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void TheirNextPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (TargetPageIndex < TheirMaxPage)
                TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
        }

        private void TheirNextPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (TargetPageIndex < TheirMaxPage)
                TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void TheirPreviousPageButton_Click(object sender, EventArgs e)
        {
            if (TargetPageIndex > 0)
            {
                if (--TargetPageIndex == 0)
                    TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
                TheirNextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
                SoundService.PlaySound("page_flip");
                LoadTheirCards();
            }
        }

        private void TheirPreviousPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (TargetPageIndex > 0)
                TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_down");
        }

        private void TheirPreviousPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (TargetPageIndex > 0)
                TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void TheirPreviousPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (TargetPageIndex > 0)
                TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
        }

        private void TheirPreviousPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (PageIndex > 0)
                TheirPreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void NextPageButton_Click(object sender, EventArgs e)
        {
            if (PageIndex < MaxPage)
            {
                PageIndex++;
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
                SoundService.PlaySound("page_flip");
                LoadCards();

                if (PageIndex == MaxPage)
                    NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_disabled");
            }
        }

        private void NextPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (PageIndex < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_down");
        }

        private void NextPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (PageIndex < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void NextPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (PageIndex < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
        }

        private void NextPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (PageIndex < MaxPage)
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage_over");
        }

        private void PreviousPageButton_Click(object sender, EventArgs e)
        {
            if (PageIndex > 0)
            {
                if (--PageIndex == 0)
                    PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_disabled");
                NextPageButton.Image = ResourcesLib.GetImageFromBinder("binder_nextpage");
                SoundService.PlaySound("page_flip");
                LoadCards();
            }
        }

        private void PreviousPageButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_down");
        }

        private void PreviousPageButton_MouseEnter(object sender, EventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        private void PreviousPageButton_MouseLeave(object sender, EventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage");
        }

        private void PreviousPageButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (PageIndex > 0)
                PreviousPageButton.Image = ResourcesLib.GetImageFromBinder("binder_lastpage_over");
        }

        public void CancelConfirm()
        {
            Accepted = !Accepted;
            ButtonAnimation.FormatButton(AcceptButton);
        }
    }
}
