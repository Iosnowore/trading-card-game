﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using TradingCardGame.Components;
using TradingCardGame.Forms.Screens;

namespace TradingCardGame.forms.widgets
{
    public partial class NavigatorAddon : UserControl
    {
        private static readonly string[] PLAY_OPTION_TITLES = {
            "Casuals",
            "Tournaments",
            "Tutorials"
        };

        private static readonly string[] CARD_OPTION_TITLES = {
            "Deck Builder",
            "Collections",
            "Trades",
            "Posted Trades"
        };

        private static readonly string[] COMMUNITY_OPTION_TITLES = {
            "Friends",
            "My Guild",
            "Guilds",
            "Leaderboards"
        };

        private static readonly string[] MISC_OPTION_TITLES = {
            "Preferences",
            "Help",
            "EULA"
        };

        private static readonly string[][] TITLES = {
            PLAY_OPTION_TITLES,
            CARD_OPTION_TITLES,
            COMMUNITY_OPTION_TITLES,
            MISC_OPTION_TITLES
        };

        private readonly Navigator NavBar;

        public NavigatorAddon(int option, Navigator NavBar)
        {
            InitializeComponent();

            this.NavBar = NavBar;

            if (option == 0 || option == 3)
                Height = 272;

            for (int i = 0; i < TITLES[option].Length; i++)
                Controls.Add(new NavigatorAddonButton(i, TITLES[option][i], NavBar));
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void NavigatorAddon_MouseLeave(object sender, EventArgs e)
        {
            if (!((TCG)Parent).MouseIsInNavigator())
                NavBar.ToggleNavBar(false);

            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                Dispose();
        }
    }
}
