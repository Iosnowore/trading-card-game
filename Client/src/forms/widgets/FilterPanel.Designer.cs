﻿namespace TradingCardGame.Src.Forms.Widgets
{
    partial class FilterPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SetButton = new System.Windows.Forms.Button();
            this.ArchetypeButton = new System.Windows.Forms.Button();
            this.RestrictionButton = new System.Windows.Forms.Button();
            this.FactionButton = new System.Windows.Forms.Button();
            this.CardTypeButton = new System.Windows.Forms.Button();
            this.TraitButton = new System.Windows.Forms.Button();
            this.RarityButton = new System.Windows.Forms.Button();
            this.KeywordButton = new System.Windows.Forms.Button();
            this.NumbericAttrButton1 = new System.Windows.Forms.Button();
            this.NumbericAttrButton2 = new System.Windows.Forms.Button();
            this.NumbericAttrButton3 = new System.Windows.Forms.Button();
            this.Comparison1Button = new System.Windows.Forms.Button();
            this.Comparison2Button = new System.Windows.Forms.Button();
            this.Comparison3Button = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.Domain1 = new System.Windows.Forms.DomainUpDown();
            this.Domain2 = new System.Windows.Forms.DomainUpDown();
            this.Domain3 = new System.Windows.Forms.DomainUpDown();
            this.CostCheckBox = new System.Windows.Forms.Button();
            this.AttackCheckBox = new System.Windows.Forms.Button();
            this.DmgBonusCheckBox = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SetButton
            // 
            this.SetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SetButton.FlatAppearance.BorderSize = 0;
            this.SetButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.SetButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.SetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SetButton.Location = new System.Drawing.Point(20, 40);
            this.SetButton.Name = "SetButton";
            this.SetButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.SetButton.Size = new System.Drawing.Size(130, 27);
            this.SetButton.TabIndex = 0;
            this.SetButton.Text = "Set                  ▾";
            this.SetButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SetButton.UseVisualStyleBackColor = true;
            this.SetButton.Click += new System.EventHandler(this.SetButton_Click);
            // 
            // ArchetypeButton
            // 
            this.ArchetypeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ArchetypeButton.FlatAppearance.BorderSize = 0;
            this.ArchetypeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ArchetypeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.ArchetypeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ArchetypeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ArchetypeButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ArchetypeButton.Location = new System.Drawing.Point(20, 73);
            this.ArchetypeButton.Name = "ArchetypeButton";
            this.ArchetypeButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.ArchetypeButton.Size = new System.Drawing.Size(130, 27);
            this.ArchetypeButton.TabIndex = 1;
            this.ArchetypeButton.Text = "Archetype      ▾";
            this.ArchetypeButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ArchetypeButton.UseVisualStyleBackColor = true;
            this.ArchetypeButton.Click += new System.EventHandler(this.ArchetypeButton_Click);
            // 
            // RestrictionButton
            // 
            this.RestrictionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RestrictionButton.FlatAppearance.BorderSize = 0;
            this.RestrictionButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RestrictionButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RestrictionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestrictionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestrictionButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RestrictionButton.Location = new System.Drawing.Point(20, 106);
            this.RestrictionButton.Name = "RestrictionButton";
            this.RestrictionButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.RestrictionButton.Size = new System.Drawing.Size(130, 27);
            this.RestrictionButton.TabIndex = 2;
            this.RestrictionButton.Text = "Restriction     ▾";
            this.RestrictionButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RestrictionButton.UseVisualStyleBackColor = true;
            this.RestrictionButton.Click += new System.EventHandler(this.RestrictionButton_Click);
            // 
            // FactionButton
            // 
            this.FactionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FactionButton.FlatAppearance.BorderSize = 0;
            this.FactionButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.FactionButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.FactionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FactionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FactionButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.FactionButton.Location = new System.Drawing.Point(20, 139);
            this.FactionButton.Name = "FactionButton";
            this.FactionButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.FactionButton.Size = new System.Drawing.Size(130, 27);
            this.FactionButton.TabIndex = 3;
            this.FactionButton.Text = "Faction           ▾";
            this.FactionButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FactionButton.UseVisualStyleBackColor = true;
            this.FactionButton.Visible = false;
            this.FactionButton.Click += new System.EventHandler(this.FactionButton_Click);
            // 
            // CardTypeButton
            // 
            this.CardTypeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CardTypeButton.FlatAppearance.BorderSize = 0;
            this.CardTypeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CardTypeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CardTypeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CardTypeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardTypeButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CardTypeButton.Location = new System.Drawing.Point(156, 40);
            this.CardTypeButton.Name = "CardTypeButton";
            this.CardTypeButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.CardTypeButton.Size = new System.Drawing.Size(130, 27);
            this.CardTypeButton.TabIndex = 4;
            this.CardTypeButton.Text = "Card Type     ▾";
            this.CardTypeButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CardTypeButton.UseVisualStyleBackColor = true;
            this.CardTypeButton.Click += new System.EventHandler(this.CardTypeButton_Click);
            // 
            // TraitButton
            // 
            this.TraitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TraitButton.FlatAppearance.BorderSize = 0;
            this.TraitButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.TraitButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.TraitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TraitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TraitButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TraitButton.Location = new System.Drawing.Point(156, 73);
            this.TraitButton.Name = "TraitButton";
            this.TraitButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.TraitButton.Size = new System.Drawing.Size(130, 27);
            this.TraitButton.TabIndex = 5;
            this.TraitButton.Text = "Trait               ▾";
            this.TraitButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TraitButton.UseVisualStyleBackColor = true;
            this.TraitButton.Click += new System.EventHandler(this.TraitButton_Click);
            // 
            // RarityButton
            // 
            this.RarityButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RarityButton.FlatAppearance.BorderSize = 0;
            this.RarityButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.RarityButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.RarityButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RarityButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RarityButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RarityButton.Location = new System.Drawing.Point(156, 106);
            this.RarityButton.Name = "RarityButton";
            this.RarityButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.RarityButton.Size = new System.Drawing.Size(130, 27);
            this.RarityButton.TabIndex = 6;
            this.RarityButton.Text = "Rarity             ▾";
            this.RarityButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RarityButton.UseVisualStyleBackColor = true;
            this.RarityButton.Click += new System.EventHandler(this.RarityButton_Click);
            // 
            // KeywordButton
            // 
            this.KeywordButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.KeywordButton.FlatAppearance.BorderSize = 0;
            this.KeywordButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.KeywordButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.KeywordButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.KeywordButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeywordButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.KeywordButton.Location = new System.Drawing.Point(156, 139);
            this.KeywordButton.Name = "KeywordButton";
            this.KeywordButton.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.KeywordButton.Size = new System.Drawing.Size(130, 27);
            this.KeywordButton.TabIndex = 7;
            this.KeywordButton.Text = "Keyword        ▾";
            this.KeywordButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.KeywordButton.UseVisualStyleBackColor = true;
            this.KeywordButton.Click += new System.EventHandler(this.KeywordButton_Click);
            // 
            // NumbericAttrButton1
            // 
            this.NumbericAttrButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NumbericAttrButton1.FlatAppearance.BorderSize = 0;
            this.NumbericAttrButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumbericAttrButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumbericAttrButton1.Location = new System.Drawing.Point(292, 40);
            this.NumbericAttrButton1.Name = "NumbericAttrButton1";
            this.NumbericAttrButton1.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.NumbericAttrButton1.Size = new System.Drawing.Size(130, 27);
            this.NumbericAttrButton1.TabIndex = 8;
            this.NumbericAttrButton1.Text = "Cost                 ▾";
            this.NumbericAttrButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NumbericAttrButton1.UseVisualStyleBackColor = true;
            this.NumbericAttrButton1.Click += new System.EventHandler(this.CostButton_Click);
            // 
            // NumbericAttrButton2
            // 
            this.NumbericAttrButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NumbericAttrButton2.FlatAppearance.BorderSize = 0;
            this.NumbericAttrButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumbericAttrButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumbericAttrButton2.Location = new System.Drawing.Point(292, 73);
            this.NumbericAttrButton2.Name = "NumbericAttrButton2";
            this.NumbericAttrButton2.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.NumbericAttrButton2.Size = new System.Drawing.Size(130, 27);
            this.NumbericAttrButton2.TabIndex = 9;
            this.NumbericAttrButton2.Text = "Attack              ▾";
            this.NumbericAttrButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NumbericAttrButton2.UseVisualStyleBackColor = true;
            this.NumbericAttrButton2.Click += new System.EventHandler(this.AttackButton_Click);
            // 
            // NumbericAttrButton3
            // 
            this.NumbericAttrButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NumbericAttrButton3.FlatAppearance.BorderSize = 0;
            this.NumbericAttrButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.NumbericAttrButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NumbericAttrButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumbericAttrButton3.Location = new System.Drawing.Point(292, 106);
            this.NumbericAttrButton3.Name = "NumbericAttrButton3";
            this.NumbericAttrButton3.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.NumbericAttrButton3.Size = new System.Drawing.Size(130, 27);
            this.NumbericAttrButton3.TabIndex = 10;
            this.NumbericAttrButton3.Text = "Dmg Bonus     ▾";
            this.NumbericAttrButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NumbericAttrButton3.UseVisualStyleBackColor = true;
            this.NumbericAttrButton3.Click += new System.EventHandler(this.DmgBonusButton_Click);
            // 
            // Comparison1Button
            // 
            this.Comparison1Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Comparison1Button.FlatAppearance.BorderSize = 0;
            this.Comparison1Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.Comparison1Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.Comparison1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Comparison1Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comparison1Button.Location = new System.Drawing.Point(428, 40);
            this.Comparison1Button.Name = "Comparison1Button";
            this.Comparison1Button.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.Comparison1Button.Size = new System.Drawing.Size(90, 27);
            this.Comparison1Button.TabIndex = 11;
            this.Comparison1Button.Text = ">             ▾";
            this.Comparison1Button.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Comparison1Button.UseVisualStyleBackColor = true;
            this.Comparison1Button.Click += new System.EventHandler(this.CostComparisonButton_Click);
            // 
            // Comparison2Button
            // 
            this.Comparison2Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Comparison2Button.FlatAppearance.BorderSize = 0;
            this.Comparison2Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.Comparison2Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.Comparison2Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Comparison2Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comparison2Button.Location = new System.Drawing.Point(428, 73);
            this.Comparison2Button.Name = "Comparison2Button";
            this.Comparison2Button.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.Comparison2Button.Size = new System.Drawing.Size(90, 27);
            this.Comparison2Button.TabIndex = 12;
            this.Comparison2Button.Text = ">             ▾";
            this.Comparison2Button.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Comparison2Button.UseVisualStyleBackColor = true;
            this.Comparison2Button.Click += new System.EventHandler(this.AttackComparisonButton_Click);
            // 
            // Comparison3Button
            // 
            this.Comparison3Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Comparison3Button.FlatAppearance.BorderSize = 0;
            this.Comparison3Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.Comparison3Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.Comparison3Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Comparison3Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comparison3Button.Location = new System.Drawing.Point(428, 106);
            this.Comparison3Button.Name = "Comparison3Button";
            this.Comparison3Button.Padding = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.Comparison3Button.Size = new System.Drawing.Size(90, 27);
            this.Comparison3Button.TabIndex = 13;
            this.Comparison3Button.Text = ">             ▾";
            this.Comparison3Button.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Comparison3Button.UseVisualStyleBackColor = true;
            this.Comparison3Button.Click += new System.EventHandler(this.DmgBonusComparisonButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ResetButton.FlatAppearance.BorderSize = 0;
            this.ResetButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.ResetButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.ResetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.Location = new System.Drawing.Point(428, 139);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(170, 27);
            this.ResetButton.TabIndex = 14;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.Transparent;
            this.CloseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CloseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Location = new System.Drawing.Point(605, 3);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(42, 37);
            this.CloseButton.TabIndex = 15;
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            this.CloseButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CloseButton_MouseDown);
            this.CloseButton.MouseEnter += new System.EventHandler(this.CloseButton_MouseEnter);
            this.CloseButton.MouseLeave += new System.EventHandler(this.CloseButton_MouseLeave);
            // 
            // Domain1
            // 
            this.Domain1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain1.Items.Add("20");
            this.Domain1.Items.Add("19");
            this.Domain1.Items.Add("18");
            this.Domain1.Items.Add("17");
            this.Domain1.Items.Add("16");
            this.Domain1.Items.Add("15");
            this.Domain1.Items.Add("14");
            this.Domain1.Items.Add("13");
            this.Domain1.Items.Add("12");
            this.Domain1.Items.Add("11");
            this.Domain1.Items.Add("10");
            this.Domain1.Items.Add("9");
            this.Domain1.Items.Add("8");
            this.Domain1.Items.Add("7");
            this.Domain1.Items.Add("6");
            this.Domain1.Items.Add("5");
            this.Domain1.Items.Add("4");
            this.Domain1.Items.Add("3");
            this.Domain1.Items.Add("2");
            this.Domain1.Items.Add("1");
            this.Domain1.Items.Add("0");
            this.Domain1.Location = new System.Drawing.Point(524, 40);
            this.Domain1.Name = "Domain1";
            this.Domain1.ReadOnly = true;
            this.Domain1.Size = new System.Drawing.Size(45, 22);
            this.Domain1.TabIndex = 16;
            this.Domain1.Text = "0";
            this.Domain1.SelectedItemChanged += new System.EventHandler(this.Domain1_SelectedItemChanged);
            // 
            // Domain2
            // 
            this.Domain2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain2.Items.Add("20");
            this.Domain2.Items.Add("19");
            this.Domain2.Items.Add("18");
            this.Domain2.Items.Add("17");
            this.Domain2.Items.Add("16");
            this.Domain2.Items.Add("15");
            this.Domain2.Items.Add("14");
            this.Domain2.Items.Add("13");
            this.Domain2.Items.Add("12");
            this.Domain2.Items.Add("11");
            this.Domain2.Items.Add("10");
            this.Domain2.Items.Add("9");
            this.Domain2.Items.Add("8");
            this.Domain2.Items.Add("7");
            this.Domain2.Items.Add("6");
            this.Domain2.Items.Add("5");
            this.Domain2.Items.Add("4");
            this.Domain2.Items.Add("3");
            this.Domain2.Items.Add("2");
            this.Domain2.Items.Add("1");
            this.Domain2.Items.Add("0");
            this.Domain2.Location = new System.Drawing.Point(524, 76);
            this.Domain2.Name = "Domain2";
            this.Domain2.ReadOnly = true;
            this.Domain2.Size = new System.Drawing.Size(45, 22);
            this.Domain2.TabIndex = 17;
            this.Domain2.Text = "0";
            this.Domain2.SelectedItemChanged += new System.EventHandler(this.Domain2_SelectedItemChanged);
            // 
            // Domain3
            // 
            this.Domain3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domain3.Items.Add("20");
            this.Domain3.Items.Add("19");
            this.Domain3.Items.Add("18");
            this.Domain3.Items.Add("17");
            this.Domain3.Items.Add("16");
            this.Domain3.Items.Add("15");
            this.Domain3.Items.Add("14");
            this.Domain3.Items.Add("13");
            this.Domain3.Items.Add("12");
            this.Domain3.Items.Add("11");
            this.Domain3.Items.Add("10");
            this.Domain3.Items.Add("9");
            this.Domain3.Items.Add("8");
            this.Domain3.Items.Add("7");
            this.Domain3.Items.Add("6");
            this.Domain3.Items.Add("5");
            this.Domain3.Items.Add("4");
            this.Domain3.Items.Add("3");
            this.Domain3.Items.Add("2");
            this.Domain3.Items.Add("1");
            this.Domain3.Items.Add("0");
            this.Domain3.Location = new System.Drawing.Point(524, 109);
            this.Domain3.Name = "Domain3";
            this.Domain3.ReadOnly = true;
            this.Domain3.Size = new System.Drawing.Size(45, 22);
            this.Domain3.TabIndex = 18;
            this.Domain3.Text = "0";
            this.Domain3.SelectedItemChanged += new System.EventHandler(this.Domain3_SelectedItemChanged);
            // 
            // CostCheckBox
            // 
            this.CostCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CostCheckBox.FlatAppearance.BorderSize = 0;
            this.CostCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.CostCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.CostCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CostCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostCheckBox.Location = new System.Drawing.Point(575, 40);
            this.CostCheckBox.Name = "CostCheckBox";
            this.CostCheckBox.Size = new System.Drawing.Size(23, 20);
            this.CostCheckBox.TabIndex = 19;
            this.CostCheckBox.UseVisualStyleBackColor = true;
            this.CostCheckBox.Click += new System.EventHandler(this.CostCheckBox_Click);
            // 
            // AttackCheckBox
            // 
            this.AttackCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AttackCheckBox.FlatAppearance.BorderSize = 0;
            this.AttackCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.AttackCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.AttackCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AttackCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackCheckBox.Location = new System.Drawing.Point(575, 76);
            this.AttackCheckBox.Name = "AttackCheckBox";
            this.AttackCheckBox.Size = new System.Drawing.Size(23, 20);
            this.AttackCheckBox.TabIndex = 20;
            this.AttackCheckBox.UseVisualStyleBackColor = true;
            this.AttackCheckBox.Click += new System.EventHandler(this.AttackCheckBox_Click);
            // 
            // DmgBonusCheckBox
            // 
            this.DmgBonusCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DmgBonusCheckBox.FlatAppearance.BorderSize = 0;
            this.DmgBonusCheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.DmgBonusCheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.DmgBonusCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DmgBonusCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DmgBonusCheckBox.Location = new System.Drawing.Point(575, 109);
            this.DmgBonusCheckBox.Name = "DmgBonusCheckBox";
            this.DmgBonusCheckBox.Size = new System.Drawing.Size(23, 20);
            this.DmgBonusCheckBox.TabIndex = 21;
            this.DmgBonusCheckBox.UseVisualStyleBackColor = true;
            this.DmgBonusCheckBox.Click += new System.EventHandler(this.DmgBonusCheckBox_Click);
            // 
            // FilterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.DmgBonusCheckBox);
            this.Controls.Add(this.AttackCheckBox);
            this.Controls.Add(this.CostCheckBox);
            this.Controls.Add(this.Domain3);
            this.Controls.Add(this.Domain2);
            this.Controls.Add(this.Domain1);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.Comparison3Button);
            this.Controls.Add(this.Comparison2Button);
            this.Controls.Add(this.Comparison1Button);
            this.Controls.Add(this.NumbericAttrButton3);
            this.Controls.Add(this.NumbericAttrButton2);
            this.Controls.Add(this.NumbericAttrButton1);
            this.Controls.Add(this.KeywordButton);
            this.Controls.Add(this.RarityButton);
            this.Controls.Add(this.TraitButton);
            this.Controls.Add(this.CardTypeButton);
            this.Controls.Add(this.FactionButton);
            this.Controls.Add(this.RestrictionButton);
            this.Controls.Add(this.ArchetypeButton);
            this.Controls.Add(this.SetButton);
            this.DoubleBuffered = true;
            this.Name = "FilterPanel";
            this.Size = new System.Drawing.Size(650, 200);
            this.Load += new System.EventHandler(this.FilterPanel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SetButton;
        private System.Windows.Forms.Button ArchetypeButton;
        private System.Windows.Forms.Button RestrictionButton;
        private System.Windows.Forms.Button FactionButton;
        private System.Windows.Forms.Button CardTypeButton;
        private System.Windows.Forms.Button TraitButton;
        private System.Windows.Forms.Button RarityButton;
        private System.Windows.Forms.Button KeywordButton;
        private System.Windows.Forms.Button NumbericAttrButton1;
        private System.Windows.Forms.Button NumbericAttrButton2;
        private System.Windows.Forms.Button NumbericAttrButton3;
        private System.Windows.Forms.Button Comparison1Button;
        private System.Windows.Forms.Button Comparison2Button;
        private System.Windows.Forms.Button Comparison3Button;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.DomainUpDown Domain1;
        private System.Windows.Forms.DomainUpDown Domain2;
        private System.Windows.Forms.DomainUpDown Domain3;
        private System.Windows.Forms.Button CostCheckBox;
        private System.Windows.Forms.Button AttackCheckBox;
        private System.Windows.Forms.Button DmgBonusCheckBox;
    }
}
