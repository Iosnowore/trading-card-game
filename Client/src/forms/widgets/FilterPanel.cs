﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TradingCardGame.Components;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Widgets
{
    public partial class FilterPanel : UserControl
    {
        private DropDownMenu DropDownMenu;

        private Button LastButtonClicked;

        private bool DoNotReload;

        // TODO: Save all these inside the string library instead

        public readonly string[] Set =
        {
            "Champions of the Force",
            "Squadrons Over Corellia",
            "Galactic Hunters",
            "Agents of Deception",
            "The Shadow Syndicate",
            "The Nightsister's Revenge",
            "Threat of the Conqueror",
            "The Price of Victory"
        };

        public readonly bool[] SetSettings;

        public readonly string[] Archetype =
        {
            "Imperial",
            "Generic",
            "Jedi",
            "Rebel",
            "Sith"
        };

        public readonly bool[] ArchetypeSettings;

        public readonly string[] Restriction =
        {
            "Chest",
            "Feet",
            "Hands",
            "Head",
            "Leggings",
            "Melee",
            "Neck",
            "Ranged",
            "Wrist"
        };

        public readonly bool[] RestrictionSettings;

        public readonly string[] Faction =
        {
            ""
        };

        public readonly string[] CardType =
        {
            "Ability",
            "Avatar",
            "Item",
            "Quest",
            "Tactic",
            "Unit"
        };

        public readonly bool[] CardTypeSettings;

        public readonly string[] Trait =
        {
            "Armor",
            "Aqualish",
            "Beast",
            "Bothan",
            "Bounty Hunter",
            "Chiss",
            "Droid",
            "Ewok",
            "Gungan",
            "Hutt",
            "Jawa",
            "Nightsister",
            "Singing Mountain",
            "Smuggler",
            "Spy",
            "Starship",
            "Twi'lek",
            "Underworld",
            "Vehicle",
            "Weapon",
            "Wookiee",
            "Zabrak"
        };

        public readonly bool[] TraitSettings;

        public readonly string[] Rarity =
        {
            "Fixed",
            "Common",
            "Uncommon",
            "Rare"
        };

        public readonly bool[] RaritySettings;

        public readonly string[] Keyword =
        {
            ""
        };

        public readonly bool[] KeywordSettings;

        public readonly string[] Config =
        {
            "Attack",
            "Cost",
            "Defense",
            "Dmg Bonus",
            "Health"
        };

        public readonly string[] Comparison =
        {
            ">",
            ">=",
            "=",
            "<=",
            "<",
            "!="
        };

        public readonly bool[] ComparisonCheckBoxesChecked;

        public FilterPanel()
        {
            InitializeComponent();

            BackgroundImage = ResourcesLib.GetImageFromBorders("navframe");
            ButtonAnimation.MouseLeaveClose(CloseButton);
            ButtonAnimation.FormatButton(ResetButton);

            ButtonAnimation.FormatButton(SetButton);
            ButtonAnimation.FormatButton(ArchetypeButton);
            ButtonAnimation.FormatButton(RestrictionButton);
            ButtonAnimation.FormatButton(FactionButton);
            ButtonAnimation.FormatButton(CardTypeButton);
            ButtonAnimation.FormatButton(TraitButton);
            ButtonAnimation.FormatButton(RarityButton);
            ButtonAnimation.FormatButton(KeywordButton);
            ButtonAnimation.FormatButton(NumbericAttrButton1);
            ButtonAnimation.FormatButton(NumbericAttrButton2);
            ButtonAnimation.FormatButton(NumbericAttrButton3);
            ButtonAnimation.FormatButton(Comparison1Button);
            ButtonAnimation.FormatButton(Comparison2Button);
            ButtonAnimation.FormatButton(Comparison3Button);

            ResetCheckBoxes();

            ComparisonCheckBoxesChecked = new bool[3];

            ButtonAnimation.ToggleCheckBox(CostCheckBox, ComparisonCheckBoxesChecked[0]);
            ButtonAnimation.ToggleCheckBox(AttackCheckBox, ComparisonCheckBoxesChecked[1]);
            ButtonAnimation.ToggleCheckBox(DmgBonusCheckBox, ComparisonCheckBoxesChecked[2]);

            SetSettings = new bool[Set.Length];
            ArchetypeSettings = new bool[Archetype.Length];
            CardTypeSettings = new bool[CardType.Length];
            RaritySettings = new bool[Rarity.Length];
            RestrictionSettings = new bool[Restriction.Length];
            TraitSettings = new bool[Trait.Length];
            KeywordSettings = new bool[Keyword.Length];
        }

        private void ResetCheckBoxes()
        {
            ButtonAnimation.ToggleDropDownCheckBox(SetButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(ArchetypeButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(RestrictionButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(FactionButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(CardTypeButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(TraitButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(RarityButton, false);
            ButtonAnimation.ToggleDropDownCheckBox(KeywordButton, false);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Visible = !Visible;
        }

        private void FilterPanel_Load(object sender, EventArgs e)
        {
            BringToFront();
        }

        private void CloseButton_MouseDown(object sender, MouseEventArgs e)
        {
            ButtonAnimation.MouseDownClose(CloseButton);
        }

        private void CloseButton_MouseEnter(object sender, EventArgs e)
        {
            ButtonAnimation.MouseEnterClose(CloseButton);
        }

        private void CloseButton_MouseLeave(object sender, EventArgs e)
        {
            ButtonAnimation.MouseLeaveClose(CloseButton);
        }

        private void SetButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Set, SetSettings, SetButton);
        }

        private void ArchetypeButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Archetype, ArchetypeSettings, ArchetypeButton);
        }

        private void RestrictionButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Restriction, RestrictionSettings, RestrictionButton);
        }

        private void FactionButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Faction, new bool[0], FactionButton);
        }

        private void CardTypeButton_Click(object sender, EventArgs e)
        {
            ButtonClick(CardType, CardTypeSettings, CardTypeButton);
        }

        private void TraitButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Trait, TraitSettings, TraitButton);
        }

        private void RarityButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Rarity, RaritySettings, RarityButton);
        }

        private void KeywordButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Keyword, KeywordSettings, KeywordButton);
        }

        private void CostButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Config, null, NumbericAttrButton1);
        }

        private void AttackButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Config, null, NumbericAttrButton2);
        }

        private void DmgBonusButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Config, null, NumbericAttrButton3);
        }

        private void CostComparisonButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Comparison, null, Comparison1Button);
        }

        private void AttackComparisonButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Comparison, null, Comparison2Button);
        }

        private void DmgBonusComparisonButton_Click(object sender, EventArgs e)
        {
            ButtonClick(Comparison, null, Comparison3Button);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            for (int i = 0; i < SetSettings.Length; i++)
                SetSettings[i] = false;

            for (int i = 0; i < ArchetypeSettings.Length; i++)
                ArchetypeSettings[i] = false;

            for (int i = 0; i < TraitSettings.Length; i++)
                TraitSettings[i] = false;

            for (int i = 0; i < CardTypeSettings.Length; i++)
                CardTypeSettings[i] = false;

            for (int i = 0; i < RaritySettings.Length; i++)
                RaritySettings[i] = false;

            for (int i = 0; i < RestrictionSettings.Length; i++)
                RestrictionSettings[i] = false;

            ResetCheckBoxes();

            NumbericAttrButton1.Text = "Cost                 ▾";
            NumbericAttrButton2.Text = "Attack              ▾";
            NumbericAttrButton3.Text = "Dmg Bonus     ▾";

            DoNotReload = true;

            Domain1.SelectedIndex = Domain1.Items.Count - 1;
            Domain2.SelectedIndex = Domain2.Items.Count - 1; ;
            Domain3.SelectedIndex = Domain3.Items.Count - 1;

            DoNotReload = false;

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.LoadCards();
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.LoadCards();
        }

        private void ButtonClick(string[] Options, bool[] OptionSettings, Button Button)
        {
            SoundService.PlaySound("button");

            if (DropDownMenu != null && !DropDownMenu.IsDisposed)
                DropDownMenu.Dispose();

            if (LastButtonClicked == Button)
            {
                LastButtonClicked = null;
                return;
            }

            LastButtonClicked = Button;

            DropDownMenu = new DropDownMenu(Options, OptionSettings);
            DropDownMenu.Location = new Point(Location.X + Button.Location.X, Location.Y + Button.Location.Y + Button.Height);
            Parent.Controls.Add(DropDownMenu);
        }

        public void DropDownSelected(int Index)
        {
            if (LastButtonClicked == SetButton)
            {
                SetSettings[Index] = !SetSettings[Index];
            }
            else if (LastButtonClicked == ArchetypeButton)
            {
                ArchetypeSettings[Index] = !ArchetypeSettings[Index];
            }
            else if (LastButtonClicked == RestrictionButton)
            {
                RestrictionSettings[Index] = !RestrictionSettings[Index];
            }
            else if (LastButtonClicked == CardTypeButton)
            {
                CardTypeSettings[Index] = !CardTypeSettings[Index];
            }
            else if (LastButtonClicked == TraitButton)
            {
                TraitSettings[Index] = !TraitSettings[Index];
            }
            else if (LastButtonClicked == RarityButton)
            {
                RaritySettings[Index] = !RaritySettings[Index];
            }
            else if (LastButtonClicked == KeywordButton)
            {
                KeywordSettings[Index] = !KeywordSettings[Index];
            }
            else if (LastButtonClicked == NumbericAttrButton1)
            {
                NumbericAttrButton1.Text = GetFormattedDropDown(Config[Index]);
            }
            else if (LastButtonClicked == NumbericAttrButton2)
            {
                NumbericAttrButton2.Text = GetFormattedDropDown(Config[Index]);
            }
            else if (LastButtonClicked == NumbericAttrButton3)
            {
                NumbericAttrButton3.Text = GetFormattedDropDown(Config[Index]);
            }
            else if (LastButtonClicked == Comparison1Button)
            {
                Comparison1Button.Text = GetFormattedDropDown(Comparison[Index]);
            }
            else if (LastButtonClicked == Comparison2Button)
            {
                Comparison2Button.Text = GetFormattedDropDown(Comparison[Index]);
            }
            else if (LastButtonClicked == Comparison3Button)
            {
                Comparison3Button.Text = GetFormattedDropDown(Comparison[Index]);
            }

            if (LastButtonClicked != null && (LastButtonClicked != NumbericAttrButton1 &&
                LastButtonClicked != NumbericAttrButton2 && LastButtonClicked != NumbericAttrButton3 &&
                LastButtonClicked != Comparison1Button) && LastButtonClicked != Comparison2Button &&
                LastButtonClicked != Comparison3Button)
                ButtonAnimation.ToggleDropDownCheckBox(LastButtonClicked, true);

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.LoadCards();
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.LoadCards();
        }

        public string GetFormattedDropDown(string Text)
        {
            switch (Text)
            {
                case "=":
                case ">":
                case "<":
                    return Text + "             ▾";
                case "!=":
                case ">=":
                case "<=":
                    return Text + "           ▾";
                case "Cost":
                    return Text + "                 ▾";
                case "Attack":
                    return Text + "              ▾";
                case "Dmg Bonus":
                    return Text + "     ▾";
                case "Defense":
                    return Text + "           ▾";
                case "Health":
                    return Text + "              ▾";
                default:
                    return Text + " ▾";
            }
        }

        public int[] FilterCardIds(int[] CardIds)
        {
            List<int> NewCardArr = new List<int>();

            CardData CardData;

            for (int i = 0; i < CardIds.Length; i++)
            {
                CardData = TCGData.Cards.GetCardData(CardIds[i]);

                if (MeetsArchetypeRequirement(CardData) && MeetsCardTypeRequirement(CardData) && MeetsTraitRequirement(CardData) && MeetsComparisonRequirements(CardData) && MeetsSetRequirement(CardData) && MeetsRarityRequirement(CardData) && MeetsRestrictionRequirement(CardData))
                {
                    NewCardArr.Add(CardIds[i]);
                }
            }
            return NewCardArr.ToArray();
        }

        private bool MeetsSetRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < SetSettings.Length; i++)
            {
                if (SetSettings[i])
                    break;

                if (i == SetSettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < SetSettings.Length; i++)
            {
                if (SetSettings[i] && CardData.GetCollectorInfo().StartsWith((i + 1).ToString()))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsArchetypeRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < ArchetypeSettings.Length; i++)
            {
                if (ArchetypeSettings[i])
                    break;

                if (i == ArchetypeSettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < ArchetypeSettings.Length; i++)
            {
                if (ArchetypeSettings[i] && CardData.GetArchetype().Equals(Archetype[i].ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsCardTypeRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < CardTypeSettings.Length; i++)
            {
                if (CardTypeSettings[i])
                    break;

                if (i == CardTypeSettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < CardTypeSettings.Length; i++)
            {
                if (CardTypeSettings[i] && CardData.GetType().Equals(CardType[i]))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsTraitRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < TraitSettings.Length; i++)
            {
                if (TraitSettings[i])
                    break;

                if (i == TraitSettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < TraitSettings.Length; i++)
            {
                if (TraitSettings[i] && CardData.GetTraits().Equals(Trait[i]))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsRestrictionRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < RestrictionSettings.Length; i++)
            {
                if (RestrictionSettings[i])
                    break;

                if (i == RestrictionSettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < RestrictionSettings.Length; i++)
            {
                if (RestrictionSettings[i] && CardData.GetRestriction().Equals(Restriction[i]))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsRarityRequirement(CardData CardData)
        {
            // Check if any options were chosen
            for (int i = 0; i < RaritySettings.Length; i++)
            {
                if (RaritySettings[i])
                    break;

                if (i == RaritySettings.Length - 1)
                    return true;
            }

            for (int i = 0; i < RaritySettings.Length; i++)
            {
                if (RaritySettings[i] && CardData.GetCollectorInfo()[1].Equals(Rarity[i][0]))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MeetsComparisonRequirements(CardData CardData)
        {
            string[] Configs = { NumbericAttrButton1.Text, NumbericAttrButton2.Text, NumbericAttrButton3.Text };
            string[] Comparisons = { Comparison1Button.Text, Comparison2Button.Text, Comparison3Button.Text };
            string[] Domains = { Domain1.Text, Domain2.Text, Domain3.Text };

            for (int i = 0; i < Configs.Length; i++)
            {
                if (ComparisonCheckBoxesChecked[i])
                {
                    // Remove space and dropdown arrow for attribute name
                    int SpaceLoc = Configs[i].IndexOf(' ');
                    string Attribute = Configs[i].Substring(0, SpaceLoc);

                    switch (Attribute)
                    {
                        case "Attack":
                            if (!CheckInequality(Comparisons[i], CardData.GetAttack(), int.Parse(Domains[i])))
                                return false;
                            break;
                        case "Cost":
                            if (!CheckInequality(Comparisons[i], CardData.GetCost(), int.Parse(Domains[i])))
                                return false;
                            break;
                        case "Defense":
                            if (!CheckInequality(Comparisons[i], CardData.GetDefense(), int.Parse(Domains[i])))
                                return false;
                            break;
                        case "Dmg Bonus":
                            if (!CheckInequality(Comparisons[i], CardData.GetBonus(), int.Parse(Domains[i])))
                                return false;
                            break;
                        case "Health":
                            if (!CheckInequality(Comparisons[i], CardData.GetHealthOrLevel(), int.Parse(Domains[i])))
                                return false;
                            break;
                    }
                }
            }
            return true;
        }

        private bool CheckInequality(string InequalitySign, int LeftValue, int RightValue)
        {
            // Remove space and dropdown arrow for attribute name
            int SpaceLoc = InequalitySign.IndexOf(' ');
            InequalitySign = InequalitySign.Substring(0, SpaceLoc);

            switch (InequalitySign)
            {
                case ">":
                    return LeftValue > RightValue;
                case ">=":
                    return LeftValue >= RightValue;
                case "=":
                    return LeftValue == RightValue;
                case "<=":
                    return LeftValue <= RightValue;
                case "<":
                    return LeftValue < RightValue;
                case "!=":
                    return LeftValue != RightValue;
                default:
                    Console.WriteLine("No valid Inequality case found!");
                    break;
            }
            return false;
        }

        private void Domain1_SelectedItemChanged(object sender, EventArgs e)
        {
            if (!DoNotReload && ComparisonCheckBoxesChecked[0])
                ((DeckBuilder)Parent).LoadCards();
        }

        private void Domain2_SelectedItemChanged(object sender, EventArgs e)
        {
            if (!DoNotReload && ComparisonCheckBoxesChecked[1])
                ((DeckBuilder)Parent).LoadCards();
        }

        private void Domain3_SelectedItemChanged(object sender, EventArgs e)
        {
            if (!DoNotReload && ComparisonCheckBoxesChecked[2])
                ((DeckBuilder)Parent).LoadCards();
        }

        private void CostCheckBox_Click(object sender, EventArgs e)
        {
            ComparisonCheckBoxesChecked[0] = !ComparisonCheckBoxesChecked[0];

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.LoadCards();
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.LoadCards();

            ButtonAnimation.ToggleCheckBox(CostCheckBox, ComparisonCheckBoxesChecked[0]);

            SoundService.PlaySound("button");
        }

        private void AttackCheckBox_Click(object sender, EventArgs e)
        {
            ComparisonCheckBoxesChecked[1] = !ComparisonCheckBoxesChecked[1];

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.LoadCards();
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.LoadCards();

            ButtonAnimation.ToggleCheckBox(AttackCheckBox, ComparisonCheckBoxesChecked[1]);

            SoundService.PlaySound("button");
        }

        private void DmgBonusCheckBox_Click(object sender, EventArgs e)
        {
            ComparisonCheckBoxesChecked[2] = !ComparisonCheckBoxesChecked[2];

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.LoadCards();
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.LoadCards();

            ButtonAnimation.ToggleCheckBox(DmgBonusCheckBox, ComparisonCheckBoxesChecked[2]);

            SoundService.PlaySound("button");
        }
    }
}
