﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.command;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.forms.widgets
{
    public partial class Navigator : UserControl
    {
        private NavigatorAddon navAdd;
        private UserControl control;
        private int navSideBarAnimation;
        private string pictureName;

        private bool Expand;

        private const int X_COLLAPSED = -80;
        private const int X_EXPANDED = 0;
        private const int X_MOVEMENT = 5;

        public Navigator()
        {
            InitializeComponent();

            BackgroundImage = ResourcesLib.GetImageFromNavigation("nav_dock_glow_01");
            button1.Image = ResourcesLib.GetImageFromNavigation("nav_home");
            button2.Image = ResourcesLib.GetImageFromNavigation("nav_play");
            button3.Image = ResourcesLib.GetImageFromNavigation("nav_cards");
            button4.Image = ResourcesLib.GetImageFromNavigation("nav_community");
            button5.Image = ResourcesLib.GetImageFromNavigation("nav_misc");
            button6.Image = ResourcesLib.GetImageFromNavigation("nav_store_disabled");
            button7.Image = ResourcesLib.GetImageFromNavigation("nav_exit");
        }

        private void Navigator_MouseEnter(object sender, EventArgs e)
        {
            if (Location.X != X_EXPANDED && !Expand)
                ToggleNavBar(true);
        }

        private void Navigator_MouseLeave(object sender, EventArgs e)
        {
            if (ClientRectangle.Contains(PointToClient(MousePosition)))
                return;

            if (control != null)
                control.Dispose();

            ToggleNavBar(false);
        }

        public void ToggleNavBar(bool Expand)
        {
            this.Expand = Expand;

            if (Expand)
                SoundService.PlaySound("sidetab_appear");
            else
                SoundService.PlaySound("sidetab_disappear");

            if (!expandTimer.Enabled)
                expandTimer.Start();
        }

        private void ExpandTimer_Tick(object sender, EventArgs e)
        {
            if (Expand)
            {
                if (Left < X_EXPANDED)
                    Left += X_MOVEMENT;
                else
                    expandTimer.Stop();
            }
            else
            {
                if (Left > X_COLLAPSED)
                    Left -= X_MOVEMENT;
                else
                    expandTimer.Stop();
            }
        }

        private void DealWithButtonClick(Button button)
        {
            TCG TCG = (TCG)Parent;

            SoundService.PlaySound("button");

            if (button == button1)
            {
                if (TCG.GetChildWindow() is Map)
                    return;

                ToggleNavBar(false);
                TCG.SetChildWindow(new Map());
                return;
            }
            else if (button == button6)
            {
                TCG.AddSystemMessage("Sorry, but the store is currently unavailable.", "Store Unavailable");
                ToggleNavBar(false);
                return;
            }
            else if (button == button7)
            {
                CommandService.ExitGame();
                return;
            }

            if (control != null)
                control.Dispose();

            if (button == button2)
                navAdd = new NavigatorAddon(0, this);
            else if (button == button3)
                navAdd = new NavigatorAddon(1, this);
            else if (button == button4)
                navAdd = new NavigatorAddon(2, this);
            else if (button == button5)
                navAdd = new NavigatorAddon(3, this);

            control = navAdd;

            Parent.Controls.Add(navAdd);
            navAdd.Location = new Point(80, Location.Y + button.Location.Y);
            navAdd.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DealWithButtonClick(button7);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.Image = ResourcesLib.GetImageFromNavigation("nav_home_over");
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Image = ResourcesLib.GetImageFromNavigation("nav_home");
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.Image = ResourcesLib.GetImageFromNavigation("nav_play_over");
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.Image = ResourcesLib.GetImageFromNavigation("nav_play");
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.Image = ResourcesLib.GetImageFromNavigation("nav_cards_over");
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.Image = ResourcesLib.GetImageFromNavigation("nav_cards");
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.Image = ResourcesLib.GetImageFromNavigation("nav_community_over");
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.Image = ResourcesLib.GetImageFromNavigation("nav_community");
        }

        private void button5_MouseEnter(object sender, EventArgs e)
        {
            button5.Image = ResourcesLib.GetImageFromNavigation("nav_misc_over");
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            button5.Image = ResourcesLib.GetImageFromNavigation("nav_misc");
        }

        private void button6_MouseEnter(object sender, EventArgs e)
        {
            // store
        }

        private void button6_MouseLeave(object sender, EventArgs e)
        {
            // store
        }

        private void button7_MouseEnter(object sender, EventArgs e)
        {
            button7.Image = ResourcesLib.GetImageFromNavigation("nav_exit_over");
        }

        private void button7_MouseLeave(object sender, EventArgs e)
        {
            button7.Image = ResourcesLib.GetImageFromNavigation("nav_exit");
        }

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            if (navSideBarAnimation > 60)
                navSideBarAnimation = 1;

            if (navSideBarAnimation < 10)
                pictureName = "nav_dock_glow_0";
            else
                pictureName = "nav_dock_glow_";
            BackgroundImage = ResourcesLib.GetImageFromNavigation(pictureName + navSideBarAnimation++);
        }

        private void Navigator_Load(object sender, EventArgs e)
        {
            Location = new Point(X_COLLAPSED, (Parent.ClientSize.Height - Height) / 2);
            animationTimer.Enabled = SettingsService.GetBoolFromSettingsXml("General", "LeftNavigatorPulse");
        }

        public void TogglePulse(bool Pulse)
        {
            animationTimer.Enabled = Pulse;

            if (!Pulse)
                BackgroundImage = ResourcesLib.GetImageFromNavigation("nav_dock_glow_01");
        }
    }
}
