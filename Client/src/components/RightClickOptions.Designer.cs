﻿namespace TradingCardGame.Components
{
    partial class RightClickOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListViewBox = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // ListViewBox
            // 
            this.ListViewBox.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.ListViewBox.BackColor = System.Drawing.Color.Cyan;
            this.ListViewBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListViewBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewBox.Location = new System.Drawing.Point(5, 5);
            this.ListViewBox.Name = "ListViewBox";
            this.ListViewBox.Size = new System.Drawing.Size(190, 110);
            this.ListViewBox.TabIndex = 1;
            this.ListViewBox.UseCompatibleStateImageBehavior = false;
            this.ListViewBox.View = System.Windows.Forms.View.List;
            this.ListViewBox.ItemActivate += new System.EventHandler(this.ListViewBox_ItemActivate);
            // 
            // RightClickOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.ListViewBox);
            this.DoubleBuffered = true;
            this.Name = "RightClickOptions";
            this.Size = new System.Drawing.Size(200, 120);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView ListViewBox;
    }
}
