﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Src.Custom;
using TradingCardGame.Src.Services.Font;

namespace TradingCardGame.Src.Components
{
    public class CardStatPanel : OutlinedTextLabel
    {
        private static readonly Point[] CombatInfoTextPoints = {
            new Point(10, 2),
            new Point(15, 5),
            new Point(20, 8),
            new Point(10, 2),
            new Point(15, 6)
        };

        private static readonly int[] CombatInfoTextSizes = {
            12,
            14,
            20,
            13,
            13
        };

        private static readonly Size[] CombatInfoSizes = {
            new Size(28, 25),
            new Size(40, 35),
            new Size(51, 45),
            new Size(34, 30),
            new Size(40, 36)
        };

        private static readonly Size[] CombatInfoSizesExtended = {
            new Size(43, 25),
            new Size(61, 35),
            new Size(73, 45),
            new Size(43, 25),
            new Size(0, 0)
        };

        public CardStatPanel(int Value, int Index)
        {
            Size = Value > 9 ? CombatInfoSizesExtended[Index] : CombatInfoSizes[Index];

            BackgroundImageLayout = ImageLayout.Zoom;
            BackColor = Color.Transparent;

            Location = CombatInfoTextPoints[Index];
            ForeColor = Color.White;
            Text = Value.ToString();
            CustomFontService.SetCustomFont(this, FontStyle.Bold, CombatInfoTextSizes[Index]);
        }
    }
}
