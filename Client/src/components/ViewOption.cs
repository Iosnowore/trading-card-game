﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.Src.Components
{
    public partial class ViewOption : UserControl
    {
        private readonly UserControl MainParent;

        private bool GraphicView = true;
        private bool NeedsUpdating = false;

        public ViewOption(UserControl MainParent)
        {
            InitializeComponent();

            this.MainParent = MainParent;

            TableViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_table");
            GraphicViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_graphic_down");
        }

        public bool IsGraphicView()
        {
            return GraphicView;
        }

        private void TableViewButton_Click(object sender, System.EventArgs e)
        {
            if (GraphicView)
            {
                TableViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_table_down");
                GraphicViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_graphic");

                ChangedView();
            }
        }

        private void GraphicViewButton_Click(object sender, System.EventArgs e)
        {
            if (!GraphicView)
            {
                TableViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_table");
                GraphicViewButton.BackgroundImage = ResourcesLib.GetImageFromButtons("tableview_graphic_down");

                ChangedView();
            }
        }

        private void ChangedView()
        {
            GraphicView = !GraphicView;
            SoundService.PlaySound("button");
            NeedsUpdating = true;

            if (MainParent is DeckBuilder DeckBuilder)
            {
                DeckBuilder.UpdateView();
            }
            else if (MainParent is CollectionManager CollectionManager)
            {
                CollectionManager.UpdateView();
            }
            NeedsUpdating = false;
        }

        public bool GetNeedsUpdating()
        {
            return NeedsUpdating;
        }
    }
}
