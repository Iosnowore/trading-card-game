﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TCGData.Packets;
using TradingCardGame.forms.dialogs;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;

namespace TradingCardGame.Src.Components
{
    public partial class MatchPanel : UserControl
    {
        private readonly MatchPacket MatchPacket;

        public MatchPanel(MatchPacket MatchPacket)
        {
            InitializeComponent();
            OverlayPanel.BackgroundImage = ResourcesLib.GetImageFromMatch("matchbase_open");

            FriendsOnlyPictureBox.Image = ResourcesLib.GetImageFromMatch("icon_buddy");
            TimedGamePictureBox.Image = ResourcesLib.GetImageFromMatch("icon_timed");
            PasswordRequiredPictureBox.Image = ResourcesLib.GetImageFromMatch("icon_password");
            InfoButton.Image = ResourcesLib.GetImageFromMatch("button_info");
            ObserveButton.Image = ResourcesLib.GetImageFromMatch("button_observe");
            lvldButton.Image = ResourcesLib.GetImageFromMatch("button_lvd");
            JoinButton.Image = ResourcesLib.GetImageFromMatch("button_join");

            this.MatchPacket = MatchPacket;

            string Creator = MatchPacket.GetPlayers()[0];
            string AvatarId = Creator.Split(':')[1];

            if (AvatarId.Length == 1)
                AvatarId = "0" + AvatarId;

            AvatarPictureBox.Image = ResourcesLib.GetImageFromPictures("avatar_" + AvatarId);

            IdLabel.Text = "#" + MatchPacket.GetMatchId();
            FormatLabel.Text = MatchPacket.GetPlayFormat();

            ConfigurePlayerIconSlots();

            PasswordRequiredPictureBox.Visible = !string.IsNullOrEmpty(MatchPacket.GetPassword());
            TimedGamePictureBox.Visible = MatchPacket.GetTimeLimit().Equals("No Time Limit");
            FriendsOnlyPictureBox.Visible = MatchPacket.GetFriendsOnly();
            NumberOfPlayersLabel.Text = MatchPacket.GetPlayers().Length.ToString();
            NameLabel.Text = MatchPacket.GetTitle();
        }

        private Image GetMatchImage(string name)
        {
            return ResourcesLib.GetImageFromMatch(name);
        }

        private void ConfigurePlayerIconSlots()
        {
            string[] Players = MatchPacket.GetPlayers();

            PlayerSlot1.Image = GetMatchImage("playerslot_light");

            PlayerSlot2.Image = Players.Length < 2 ? null : GetMatchImage("playerslot_light");

            if (MatchPacket.GetMaxPlayers() == 4)
            {
                PlayerSlot3.Image = Players.Length < 3 ? null : GetMatchImage("playerslot_light");
                PlayerSlot4.Image = Players.Length < 4 ? null : GetMatchImage("playerslot_light");
            }
            else
            {
                PlayerSlot3.Image = GetMatchImage("playerslot_none");
                PlayerSlot4.Image = GetMatchImage("playerslot_none");
            }
        }

        private bool MatchIsntFull()
        {
            if (MatchPacket.GetMaxPlayers() == 2)
                return PlayerSlot2.Image == null;
            return PlayerSlot3.Image == null || PlayerSlot4.Image == null;
        }

        private void JoinButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (MatchIsntFull())
                Parent.Parent.Parent.Controls.Add(new JoinMatch(MatchPacket.GetMatchId()));
            else
                ((TCG)Parent.Parent.Parent.Parent).AddSystemMessage("The match you tried to join is currently full.", "Match Is Full");
        }

        private void JoinButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void JoinButton_MouseDown(object sender, MouseEventArgs e)
        {
            JoinButton.Image = GetMatchImage("button_join_down");
        }

        private void JoinButton_MouseEnter(object sender, System.EventArgs e)
        {
            JoinButton.Image = GetMatchImage("button_join_over");
        }

        private void JoinButton_MouseLeave(object sender, System.EventArgs e)
        {
            JoinButton.Image = GetMatchImage("button_join");
        }

        private void InfoButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void ObserveButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");
        }

        private void ObserveButton_MouseDown(object sender, MouseEventArgs e)
        {
            ObserveButton.Image = GetMatchImage("button_observe_down");
        }

        private void ObserveButton_MouseEnter(object sender, System.EventArgs e)
        {
            ObserveButton.Image = GetMatchImage("button_observe_over");
        }

        private void ObserveButton_MouseLeave(object sender, System.EventArgs e)
        {
            ObserveButton.Image = GetMatchImage("button_observe");
        }

        private void InfoButton_MouseDown(object sender, MouseEventArgs e)
        {
            InfoButton.Image = GetMatchImage("button_info_down");
        }

        private void InfoButton_MouseEnter(object sender, System.EventArgs e)
        {
            InfoButton.Image = GetMatchImage("button_info_over");
        }

        private void InfoButton_MouseLeave(object sender, System.EventArgs e)
        {
            InfoButton.Image = GetMatchImage("button_info");
        }
    }
}
