﻿namespace TradingCardGame.Components
{
    partial class CollectionCardInfoPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ForTradeLabel = new System.Windows.Forms.Label();
            this.WantLabel = new System.Windows.Forms.Label();
            this.QuantityLabel = new System.Windows.Forms.Label();
            this.ForTradeTitleLabel = new System.Windows.Forms.Label();
            this.WantTitleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.IncreaseButton = new System.Windows.Forms.Button();
            this.DecreaseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ForTradeLabel
            // 
            this.ForTradeLabel.AutoSize = true;
            this.ForTradeLabel.BackColor = System.Drawing.Color.Transparent;
            this.ForTradeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForTradeLabel.ForeColor = System.Drawing.Color.White;
            this.ForTradeLabel.Location = new System.Drawing.Point(128, 31);
            this.ForTradeLabel.Name = "ForTradeLabel";
            this.ForTradeLabel.Size = new System.Drawing.Size(14, 15);
            this.ForTradeLabel.TabIndex = 11;
            this.ForTradeLabel.Text = "0";
            // 
            // WantLabel
            // 
            this.WantLabel.AutoSize = true;
            this.WantLabel.BackColor = System.Drawing.Color.Transparent;
            this.WantLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WantLabel.ForeColor = System.Drawing.Color.White;
            this.WantLabel.Location = new System.Drawing.Point(128, 16);
            this.WantLabel.Name = "WantLabel";
            this.WantLabel.Size = new System.Drawing.Size(14, 15);
            this.WantLabel.TabIndex = 10;
            this.WantLabel.Text = "0";
            // 
            // QuantityLabel
            // 
            this.QuantityLabel.AutoSize = true;
            this.QuantityLabel.BackColor = System.Drawing.Color.Transparent;
            this.QuantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityLabel.ForeColor = System.Drawing.Color.White;
            this.QuantityLabel.Location = new System.Drawing.Point(128, 1);
            this.QuantityLabel.Name = "QuantityLabel";
            this.QuantityLabel.Size = new System.Drawing.Size(14, 15);
            this.QuantityLabel.TabIndex = 9;
            this.QuantityLabel.Text = "0";
            // 
            // ForTradeTitleLabel
            // 
            this.ForTradeTitleLabel.AutoSize = true;
            this.ForTradeTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.ForTradeTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForTradeTitleLabel.ForeColor = System.Drawing.Color.White;
            this.ForTradeTitleLabel.Location = new System.Drawing.Point(4, 31);
            this.ForTradeTitleLabel.Name = "ForTradeTitleLabel";
            this.ForTradeTitleLabel.Size = new System.Drawing.Size(63, 15);
            this.ForTradeTitleLabel.TabIndex = 8;
            this.ForTradeTitleLabel.Text = "For Trade:";
            // 
            // WantTitleLabel
            // 
            this.WantTitleLabel.AutoSize = true;
            this.WantTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.WantTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WantTitleLabel.ForeColor = System.Drawing.Color.White;
            this.WantTitleLabel.Location = new System.Drawing.Point(4, 16);
            this.WantTitleLabel.Name = "WantTitleLabel";
            this.WantTitleLabel.Size = new System.Drawing.Size(38, 15);
            this.WantTitleLabel.TabIndex = 7;
            this.WantTitleLabel.Text = "Want:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Quantity:";
            // 
            // IncreaseButton
            // 
            this.IncreaseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IncreaseButton.FlatAppearance.BorderSize = 0;
            this.IncreaseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.IncreaseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.IncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IncreaseButton.Location = new System.Drawing.Point(88, 17);
            this.IncreaseButton.Name = "IncreaseButton";
            this.IncreaseButton.Size = new System.Drawing.Size(15, 15);
            this.IncreaseButton.TabIndex = 16;
            this.IncreaseButton.UseVisualStyleBackColor = true;
            this.IncreaseButton.Click += new System.EventHandler(this.IncreaseButton_Click);
            this.IncreaseButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IncreaseButton_MouseDown);
            this.IncreaseButton.MouseEnter += new System.EventHandler(this.IncreaseButton_MouseEnter);
            this.IncreaseButton.MouseLeave += new System.EventHandler(this.IncreaseButton_MouseLeave);
            this.IncreaseButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IncreaseButton_MouseUp);
            // 
            // DecreaseButton
            // 
            this.DecreaseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DecreaseButton.FlatAppearance.BorderSize = 0;
            this.DecreaseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.DecreaseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.DecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DecreaseButton.Location = new System.Drawing.Point(105, 17);
            this.DecreaseButton.Name = "DecreaseButton";
            this.DecreaseButton.Size = new System.Drawing.Size(15, 15);
            this.DecreaseButton.TabIndex = 17;
            this.DecreaseButton.UseVisualStyleBackColor = true;
            this.DecreaseButton.Click += new System.EventHandler(this.DecreaseButton_Click);
            this.DecreaseButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DecreaseButton_MouseDown);
            this.DecreaseButton.MouseEnter += new System.EventHandler(this.DecreaseButton_MouseEnter);
            this.DecreaseButton.MouseLeave += new System.EventHandler(this.DecreaseButton_MouseLeave);
            this.DecreaseButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DecreaseButton_MouseUp);
            // 
            // CollectionCardInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.DecreaseButton);
            this.Controls.Add(this.IncreaseButton);
            this.Controls.Add(this.ForTradeLabel);
            this.Controls.Add(this.WantLabel);
            this.Controls.Add(this.QuantityLabel);
            this.Controls.Add(this.ForTradeTitleLabel);
            this.Controls.Add(this.WantTitleLabel);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "CollectionCardInfoPanel";
            this.Size = new System.Drawing.Size(175, 49);
            this.Load += new System.EventHandler(this.CollectionCardInfoPanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ForTradeLabel;
        private System.Windows.Forms.Label WantLabel;
        private System.Windows.Forms.Label QuantityLabel;
        private System.Windows.Forms.Label ForTradeTitleLabel;
        private System.Windows.Forms.Label WantTitleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button IncreaseButton;
        private System.Windows.Forms.Button DecreaseButton;
    }
}
