﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;

namespace TradingCardGame.Components
{
    public partial class CollectionCardInfoPanel : UserControl
    {
        private readonly int Index;

        public CollectionCardInfoPanel(int Index)
        {
            InitializeComponent();
            BackgroundImage = ResourcesLib.GetImageFromBinder("textbox");
            BackgroundImageLayout = ImageLayout.Stretch;
            Location = new Point(X_LOCATIONS[Index > 4 ? Index - 5 : Index], Y_LOCATIONS[Index > 4 ? 1 : 0]);

            this.Index = Index;
        }

        private static readonly int[] Y_LOCATIONS = {
            265,
            585
        };

        private static readonly int[] X_LOCATIONS = {
            30,
            215,
            400,
            585,
            770
        };

        public int GetCardQuantity()
        {
            return int.Parse(QuantityLabel.Text);
        }

        public void ChangeCardInfo(int Quantity, int WantAmount)
        {
            IncreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("increase" + (WantAmount == 10 ? "_disabled" : string.Empty));
            DecreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("decrease" + (WantAmount == 0 ? "_disabled" : string.Empty));

            QuantityLabel.Text = Quantity.ToString();

            WantLabel.Text = WantAmount.ToString();

            if (Quantity < WantAmount)
            {
                ForTradeTitleLabel.Text = "Need:";
                ForTradeLabel.Text = (WantAmount - Quantity).ToString();
            }
            else
            {
                ForTradeTitleLabel.Text = "For Trade:";
                ForTradeLabel.Text = (Quantity - WantAmount).ToString();
            }
        }

        private void CollectionCardInfoPanel_Load(object sender, EventArgs e)
        {
            BringToFront();
        }

        private void IncreaseButton_Click(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("10"))
            {
                SoundService.PlaySound("button");
                PacketService.SendPacket(Commands.CARDWANT_INCREASE, new CommandPacket(new object[] { ((CollectionManager)Parent.Parent.Parent).GetCardId(Index) }));
                ChangeCardInfo(int.Parse(QuantityLabel.Text), int.Parse(WantLabel.Text) + 1);
            }
        }

        private void IncreaseButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (!WantLabel.Text.Equals("10"))
                IncreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("increase_down");
        }

        private void IncreaseButton_MouseEnter(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("10"))
                IncreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("increase_over");
        }

        private void IncreaseButton_MouseLeave(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("10"))
                IncreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("increase");
        }

        private void IncreaseButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (!WantLabel.Text.Equals("10"))
                IncreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("increase");
        }

        private void DecreaseButton_Click(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("0"))
            {
                SoundService.PlaySound("button");
                PacketService.SendPacket(Commands.CARDWANT_DECREASE, new CommandPacket(new object[] { ((CollectionManager)Parent.Parent.Parent).GetCardId(Index) }));
                ChangeCardInfo(int.Parse(QuantityLabel.Text), int.Parse(WantLabel.Text) - 1);
            }
        }

        private void DecreaseButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (!WantLabel.Text.Equals("0"))
                DecreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("decrease_down");
        }

        private void DecreaseButton_MouseEnter(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("0"))
                DecreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("decrease_over");
        }

        private void DecreaseButton_MouseLeave(object sender, EventArgs e)
        {
            if (!WantLabel.Text.Equals("0"))
                DecreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("decrease");
        }

        private void DecreaseButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (!WantLabel.Text.Equals("0"))
                DecreaseButton.BackgroundImage = ResourcesLib.GetImageFromBinder("decrease");
        }
    }
}
