﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TradingCardGame.services.guild;

namespace TradingCardGame.Components
{
    public partial class RightClickOptions : UserControl
    {
        private const string GRANT = "Grant Guild Officer Status";
        private const string REVOKE = "Revoke Guild Officer Status";

        private readonly string SelectedMember;

        public RightClickOptions(string SelectedMember)
        {
            InitializeComponent();
            ListViewBox.Items.Add(GRANT);
            ListViewBox.Items.Add(REVOKE);
            this.SelectedMember = SelectedMember;
        }

        private void ListViewBox_ItemActivate(object sender, System.EventArgs e)
        {
            switch (ListViewBox.SelectedItems[0].Text)
            {
                case GRANT:
                    GuildService.PromoteToOfficer(SelectedMember);
                    break;
                case REVOKE:
                    GuildService.DemoteToMember(SelectedMember);
                    break;
            }
            Dispose();
        }
    }
}
