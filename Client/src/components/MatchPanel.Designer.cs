﻿namespace TradingCardGame.Src.Components
{
    partial class MatchPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OverlayPanel = new System.Windows.Forms.Panel();
            this.PlayerSlot4 = new System.Windows.Forms.PictureBox();
            this.PlayerSlot3 = new System.Windows.Forms.PictureBox();
            this.PlayerSlot2 = new System.Windows.Forms.PictureBox();
            this.PlayerSlot1 = new System.Windows.Forms.PictureBox();
            this.FormatLabel = new System.Windows.Forms.Label();
            this.AvatarPictureBox = new System.Windows.Forms.PictureBox();
            this.IdLabel = new System.Windows.Forms.Label();
            this.FriendsOnlyPictureBox = new System.Windows.Forms.PictureBox();
            this.TimedGamePictureBox = new System.Windows.Forms.PictureBox();
            this.PasswordRequiredPictureBox = new System.Windows.Forms.PictureBox();
            this.NumberOfPlayersLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.InfoButton = new System.Windows.Forms.PictureBox();
            this.ObserveButton = new System.Windows.Forms.PictureBox();
            this.lvldButton = new System.Windows.Forms.PictureBox();
            this.JoinButton = new System.Windows.Forms.PictureBox();
            this.OverlayPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendsOnlyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimedGamePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordRequiredPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObserveButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvldButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JoinButton)).BeginInit();
            this.SuspendLayout();
            // 
            // OverlayPanel
            // 
            this.OverlayPanel.BackColor = System.Drawing.Color.Transparent;
            this.OverlayPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OverlayPanel.Controls.Add(this.PlayerSlot4);
            this.OverlayPanel.Controls.Add(this.PlayerSlot3);
            this.OverlayPanel.Controls.Add(this.PlayerSlot2);
            this.OverlayPanel.Controls.Add(this.PlayerSlot1);
            this.OverlayPanel.Controls.Add(this.FormatLabel);
            this.OverlayPanel.Controls.Add(this.AvatarPictureBox);
            this.OverlayPanel.Controls.Add(this.IdLabel);
            this.OverlayPanel.Controls.Add(this.FriendsOnlyPictureBox);
            this.OverlayPanel.Controls.Add(this.TimedGamePictureBox);
            this.OverlayPanel.Controls.Add(this.PasswordRequiredPictureBox);
            this.OverlayPanel.Controls.Add(this.NumberOfPlayersLabel);
            this.OverlayPanel.Controls.Add(this.NameLabel);
            this.OverlayPanel.Controls.Add(this.InfoButton);
            this.OverlayPanel.Controls.Add(this.ObserveButton);
            this.OverlayPanel.Controls.Add(this.lvldButton);
            this.OverlayPanel.Controls.Add(this.JoinButton);
            this.OverlayPanel.Location = new System.Drawing.Point(0, 0);
            this.OverlayPanel.Name = "OverlayPanel";
            this.OverlayPanel.Size = new System.Drawing.Size(243, 160);
            this.OverlayPanel.TabIndex = 1;
            // 
            // PlayerSlot4
            // 
            this.PlayerSlot4.BackColor = System.Drawing.Color.Transparent;
            this.PlayerSlot4.Location = new System.Drawing.Point(199, 39);
            this.PlayerSlot4.Name = "PlayerSlot4";
            this.PlayerSlot4.Size = new System.Drawing.Size(27, 27);
            this.PlayerSlot4.TabIndex = 6;
            this.PlayerSlot4.TabStop = false;
            // 
            // PlayerSlot3
            // 
            this.PlayerSlot3.BackColor = System.Drawing.Color.Transparent;
            this.PlayerSlot3.Location = new System.Drawing.Point(167, 39);
            this.PlayerSlot3.Name = "PlayerSlot3";
            this.PlayerSlot3.Size = new System.Drawing.Size(27, 27);
            this.PlayerSlot3.TabIndex = 7;
            this.PlayerSlot3.TabStop = false;
            // 
            // PlayerSlot2
            // 
            this.PlayerSlot2.BackColor = System.Drawing.Color.Transparent;
            this.PlayerSlot2.Location = new System.Drawing.Point(134, 39);
            this.PlayerSlot2.Name = "PlayerSlot2";
            this.PlayerSlot2.Size = new System.Drawing.Size(27, 27);
            this.PlayerSlot2.TabIndex = 11;
            this.PlayerSlot2.TabStop = false;
            // 
            // PlayerSlot1
            // 
            this.PlayerSlot1.BackColor = System.Drawing.Color.Transparent;
            this.PlayerSlot1.Location = new System.Drawing.Point(102, 39);
            this.PlayerSlot1.Name = "PlayerSlot1";
            this.PlayerSlot1.Size = new System.Drawing.Size(27, 27);
            this.PlayerSlot1.TabIndex = 12;
            this.PlayerSlot1.TabStop = false;
            // 
            // FormatLabel
            // 
            this.FormatLabel.AutoSize = true;
            this.FormatLabel.BackColor = System.Drawing.Color.Transparent;
            this.FormatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormatLabel.ForeColor = System.Drawing.Color.Black;
            this.FormatLabel.Location = new System.Drawing.Point(99, 17);
            this.FormatLabel.Name = "FormatLabel";
            this.FormatLabel.Size = new System.Drawing.Size(93, 24);
            this.FormatLabel.TabIndex = 8;
            this.FormatLabel.Text = "Standard";
            // 
            // AvatarPictureBox
            // 
            this.AvatarPictureBox.Location = new System.Drawing.Point(16, 16);
            this.AvatarPictureBox.Name = "AvatarPictureBox";
            this.AvatarPictureBox.Size = new System.Drawing.Size(70, 70);
            this.AvatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AvatarPictureBox.TabIndex = 16;
            this.AvatarPictureBox.TabStop = false;
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.BackColor = System.Drawing.Color.Transparent;
            this.IdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.IdLabel.Location = new System.Drawing.Point(97, 4);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(42, 15);
            this.IdLabel.TabIndex = 15;
            this.IdLabel.Text = "#2654";
            // 
            // FriendsOnlyPictureBox
            // 
            this.FriendsOnlyPictureBox.BackColor = System.Drawing.Color.Black;
            this.FriendsOnlyPictureBox.Location = new System.Drawing.Point(156, 70);
            this.FriendsOnlyPictureBox.Name = "FriendsOnlyPictureBox";
            this.FriendsOnlyPictureBox.Size = new System.Drawing.Size(19, 19);
            this.FriendsOnlyPictureBox.TabIndex = 14;
            this.FriendsOnlyPictureBox.TabStop = false;
            // 
            // TimedGamePictureBox
            // 
            this.TimedGamePictureBox.BackColor = System.Drawing.Color.Black;
            this.TimedGamePictureBox.Location = new System.Drawing.Point(130, 70);
            this.TimedGamePictureBox.Name = "TimedGamePictureBox";
            this.TimedGamePictureBox.Size = new System.Drawing.Size(19, 19);
            this.TimedGamePictureBox.TabIndex = 13;
            this.TimedGamePictureBox.TabStop = false;
            // 
            // PasswordRequiredPictureBox
            // 
            this.PasswordRequiredPictureBox.BackColor = System.Drawing.Color.Black;
            this.PasswordRequiredPictureBox.Location = new System.Drawing.Point(104, 70);
            this.PasswordRequiredPictureBox.Name = "PasswordRequiredPictureBox";
            this.PasswordRequiredPictureBox.Size = new System.Drawing.Size(19, 19);
            this.PasswordRequiredPictureBox.TabIndex = 8;
            this.PasswordRequiredPictureBox.TabStop = false;
            // 
            // NumberOfPlayersLabel
            // 
            this.NumberOfPlayersLabel.AutoSize = true;
            this.NumberOfPlayersLabel.BackColor = System.Drawing.Color.Transparent;
            this.NumberOfPlayersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumberOfPlayersLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.NumberOfPlayersLabel.Location = new System.Drawing.Point(188, 69);
            this.NumberOfPlayersLabel.Name = "NumberOfPlayersLabel";
            this.NumberOfPlayersLabel.Size = new System.Drawing.Size(32, 33);
            this.NumberOfPlayersLabel.TabIndex = 9;
            this.NumberOfPlayersLabel.Text = "1";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.BackColor = System.Drawing.Color.Transparent;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.NameLabel.Location = new System.Drawing.Point(76, 138);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(127, 15);
            this.NameLabel.TabIndex = 5;
            this.NameLabel.Text = "Iosnowore\'s Match";
            // 
            // InfoButton
            // 
            this.InfoButton.BackColor = System.Drawing.Color.Black;
            this.InfoButton.Location = new System.Drawing.Point(140, 92);
            this.InfoButton.Name = "InfoButton";
            this.InfoButton.Size = new System.Drawing.Size(32, 32);
            this.InfoButton.TabIndex = 4;
            this.InfoButton.TabStop = false;
            this.InfoButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.InfoButton_MouseClick);
            this.InfoButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.InfoButton_MouseDown);
            this.InfoButton.MouseEnter += new System.EventHandler(this.InfoButton_MouseEnter);
            this.InfoButton.MouseLeave += new System.EventHandler(this.InfoButton_MouseLeave);
            // 
            // ObserveButton
            // 
            this.ObserveButton.BackColor = System.Drawing.Color.Black;
            this.ObserveButton.Location = new System.Drawing.Point(107, 92);
            this.ObserveButton.Name = "ObserveButton";
            this.ObserveButton.Size = new System.Drawing.Size(32, 32);
            this.ObserveButton.TabIndex = 3;
            this.ObserveButton.TabStop = false;
            this.ObserveButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ObserveButton_MouseClick);
            this.ObserveButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ObserveButton_MouseDown);
            this.ObserveButton.MouseEnter += new System.EventHandler(this.ObserveButton_MouseEnter);
            this.ObserveButton.MouseLeave += new System.EventHandler(this.ObserveButton_MouseLeave);
            // 
            // lvldButton
            // 
            this.lvldButton.BackColor = System.Drawing.Color.Black;
            this.lvldButton.Location = new System.Drawing.Point(73, 92);
            this.lvldButton.Name = "lvldButton";
            this.lvldButton.Size = new System.Drawing.Size(32, 32);
            this.lvldButton.TabIndex = 2;
            this.lvldButton.TabStop = false;
            // 
            // JoinButton
            // 
            this.JoinButton.BackColor = System.Drawing.Color.Black;
            this.JoinButton.Location = new System.Drawing.Point(13, 105);
            this.JoinButton.Name = "JoinButton";
            this.JoinButton.Size = new System.Drawing.Size(44, 41);
            this.JoinButton.TabIndex = 1;
            this.JoinButton.TabStop = false;
            this.JoinButton.Click += new System.EventHandler(this.JoinButton_Click);
            this.JoinButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.JoinButton_MouseClick);
            this.JoinButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.JoinButton_MouseDown);
            this.JoinButton.MouseEnter += new System.EventHandler(this.JoinButton_MouseEnter);
            this.JoinButton.MouseLeave += new System.EventHandler(this.JoinButton_MouseLeave);
            // 
            // MatchPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(20)))), ((int)(((byte)(27)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.OverlayPanel);
            this.DoubleBuffered = true;
            this.Name = "MatchPanel";
            this.Size = new System.Drawing.Size(243, 160);
            this.OverlayPanel.ResumeLayout(false);
            this.OverlayPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvatarPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendsOnlyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimedGamePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordRequiredPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObserveButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvldButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JoinButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel OverlayPanel;
        private System.Windows.Forms.PictureBox PlayerSlot4;
        private System.Windows.Forms.PictureBox PlayerSlot3;
        private System.Windows.Forms.PictureBox PlayerSlot2;
        private System.Windows.Forms.PictureBox PlayerSlot1;
        private System.Windows.Forms.Label IdLabel;
        private System.Windows.Forms.Label FormatLabel;
        private System.Windows.Forms.PictureBox FriendsOnlyPictureBox;
        private System.Windows.Forms.PictureBox TimedGamePictureBox;
        private System.Windows.Forms.PictureBox PasswordRequiredPictureBox;
        private System.Windows.Forms.Label NumberOfPlayersLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.PictureBox InfoButton;
        private System.Windows.Forms.PictureBox ObserveButton;
        private System.Windows.Forms.PictureBox lvldButton;
        private System.Windows.Forms.PictureBox JoinButton;
        private System.Windows.Forms.PictureBox AvatarPictureBox;
    }
}
