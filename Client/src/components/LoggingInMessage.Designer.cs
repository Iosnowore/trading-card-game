﻿namespace TradingCardGame.Components
{
    partial class LoggingInMessage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.connectLabel = new System.Windows.Forms.Label();
            this.PingTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // connectLabel
            // 
            this.connectLabel.AutoSize = true;
            this.connectLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.connectLabel.Location = new System.Drawing.Point(42, 59);
            this.connectLabel.Name = "connectLabel";
            this.connectLabel.Size = new System.Drawing.Size(239, 24);
            this.connectLabel.TabIndex = 0;
            this.connectLabel.Text = "Connecting to the server";
            // 
            // PingTimer
            // 
            this.PingTimer.Enabled = true;
            this.PingTimer.Interval = 1000;
            this.PingTimer.Tick += new System.EventHandler(this.PingTimer_Tick);
            // 
            // LoggingInMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(32)))), ((int)(((byte)(42)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.connectLabel);
            this.DoubleBuffered = true;
            this.Name = "LoggingInMessage";
            this.Size = new System.Drawing.Size(323, 143);
            this.Load += new System.EventHandler(this.LoggingInMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label connectLabel;
        private System.Windows.Forms.Timer PingTimer;
    }
}
