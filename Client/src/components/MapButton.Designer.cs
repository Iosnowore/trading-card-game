﻿namespace TradingCardGame.components
{
    partial class MapButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.description = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.BackColor = System.Drawing.Color.Transparent;
            this.description.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(186)))), ((int)(((byte)(229)))));
            this.description.Location = new System.Drawing.Point(126, 36);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(197, 42);
            this.description.TabIndex = 3;
            this.description.Text = "Learn to play and hone\r\nyour skills against the AI.";
            this.description.MouseClick += new System.Windows.Forms.MouseEventHandler(this.description_MouseClick);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.BackColor = System.Drawing.Color.Transparent;
            this.title.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(186)))), ((int)(((byte)(229)))));
            this.title.Location = new System.Drawing.Point(10, 9);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(163, 21);
            this.title.TabIndex = 2;
            this.title.Text = "Tutorials / Scenarios";
            this.title.MouseClick += new System.Windows.Forms.MouseEventHandler(this.title_MouseClick);
            // 
            // MapButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.description);
            this.Controls.Add(this.title);
            this.DoubleBuffered = true;
            this.Name = "MapButton";
            this.Size = new System.Drawing.Size(332, 150);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MapButton_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MapButton_MouseDown);
            this.MouseEnter += new System.EventHandler(this.MapButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.MapButton_MouseLeave);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MapButton_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label title;
    }
}
