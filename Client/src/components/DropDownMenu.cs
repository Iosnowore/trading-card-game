﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs.preferences;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;

namespace TradingCardGame.Components
{
    public partial class DropDownMenu : UserControl
    {
        private readonly bool[] Bool;

        private void Setup(string[] Options)
        {
            HandleLoad(Options);
        }

        public DropDownMenu(string[] Options)
        {
            InitializeComponent();
            Setup(Options);
        }

        public DropDownMenu(string[] Options, bool[] Bool)
        {
            InitializeComponent();
            ListViewBox.SmallImageList = ResourcesLib.BUTTON_IMAGES;

            this.Bool = Bool;
            Setup(Options);
        }

        private void DropDownMenu_Load(object sender, EventArgs e)
        {
            BringToFront();
        }

        private void HandleLoad(string[] Options)
        {
            if (Bool != null)
                for (int i = 0; i < Options.Length; i++)
                    ListViewBox.Items.Add(new ListViewItem(Options[i], Bool[i] ? "dropdown_checkbox_on" : "dropdown_checkbox_off"));
            else
                foreach (string Option in Options)
                    ListViewBox.Items.Add(new ListViewItem(Option));

            int MostCharacters = 0;

            for (int i = 0; i < Options.Length; i++)
                if (Options[i].Length > MostCharacters)
                    MostCharacters = Options[i].Length;

            ListViewBox.Size = new Size(35 + MostCharacters * 8, ListViewBox.Items.Count * 25);
            ColumnHeader.Width = -2;
        }

        private void HandleClick()
        {
            int index = ListViewBox.Items.IndexOf(ListViewBox.SelectedItems[0]);
            ListViewBox.SelectedItems.Clear();

            if (Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.DropDownSelected(index);
            else if (Parent is DeckBuilder DeckBuilder)
                DeckBuilder.DropDownSelected(index);
            else if (Parent is Lobby Lobby)
                Lobby.DropDownSelected(index);
            else if (Parent is UserInterface UserInterface)
                UserInterface.DropDownSelected(index);

            // Update ImageKey if this is a boolean dropdown
            if (Bool != null)
                ListViewBox.Items[index].ImageKey = Bool[index] ? "dropdown_checkbox_on" : "dropdown_checkbox_off";
        }

        private void DropDownMenu_MouseLeave(object sender, EventArgs e)
        {
            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                Dispose();
        }

        private void ListView_MouseMove(object sender, MouseEventArgs e)
        {
            /*
            ListViewItem item = null;

            foreach (ListViewItem viewItem in ListView.Items)
                if (e.Location.Y == viewItem.Position.Y)
                    item = viewItem;

            if (lastItemHovered != null && lastItemHovered != item)
            {
                lastItemHovered.BackColor = Color.Cyan;
                lastItemHovered = item;
                lastItemHovered.BackColor = Color.LightCyan;
            }
            */
        }

        private void ListView_ItemActivate(object sender, EventArgs e)
        {
            HandleClick();
            SoundService.PlaySound("button");

            if (Bool == null)
                Dispose();
        }

        public void ChangeCheck(int Index)
        {
            ListViewBox.Items[Index].ImageKey = ListViewBox.Items[Index].ImageKey.Equals("dropdown_checkbox_off") ? "dropdown_checkbox_on" : "dropdown_checkbox_off";
        }
    }
}
