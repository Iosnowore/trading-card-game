﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Guild;
using TradingCardGame.forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.forms.widgets;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;

namespace TradingCardGame.Components
{
    public partial class NavigatorAddonButton : UserControl
    {
        private readonly Navigator NavBar;

        public NavigatorAddonButton(int index, string title, Navigator NavBar)
        {
            InitializeComponent();

            this.NavBar = NavBar;

            Location = new Point(3, Y_POINTS[index]);
            BackgroundImage = ResourcesLib.GetImageFromNavigation("menu_item");
            ImagePictureBox.Image = ResourcesLib.GetImageFromNavigation(GetImageString(title));
            TitleLabel.Text = title;
        }

        private string GetImageString(string title)
        {
            switch (title)
            {
                case "Casuals":
                    title = "casual";
                    break;
                case "Collections":
                    title = "collection";
                    break;
                case "Trades":
                    title = "trade";
                    break;
                case "Posted Trades":
                    title = "posted_trade";
                    break;
            }
            return "button_image_" + title.ToLower().Replace(" ", string.Empty);
        }

        private static readonly int[] Y_POINTS = {
            2,
            90,
            178,
            266
        };

        private new void Click()
        {
            TCG TCG = (TCG)Parent.Parent;

            switch (TitleLabel.Text)
            {
                case "Casuals":
                    TCG.GetChildWindow().Controls.Add(new Window(new LobbyChooser(), "Choose a Lobby"));
                    break;
                case "Tournaments":
                    TCG.SetChildWindow(new Lobby(LobbyTitles.TOURNAMENT));
                    break;
                case "Tutorials":
                    TCG.GetChildWindow().Controls.Add(new Tutorials());
                    break;
                case "Deck Builder":
                    TCG.SetChildWindow(new DeckBuilder());
                    break;
                case "Collections":
                    TCG.SetChildWindow(new CollectionManager());
                    break;
                case "Trades":
                    // Leave lobby if already in it.
                    if (TCG.GetChildWindow() is Lobby Lobby)
                    {
                        PacketService.SendPacket(Commands.LOBBY_LEAVE, new CommandPacket(new string[] { Lobby.GetLobbyTitle() }));
                        Lobby.SetSentLeaveLobbyPacket();
                    }
                    TCG.SetChildWindow(new Lobby(LobbyTitles.TRADE));
                    break;
                case "Posted Trades":
                    TCG.GetChildWindow().Controls.Add(new Window(new PostedTrades(), "Posted Trades"));
                    break;
                case "Friends":
                    if (TCG.GetDialogStatus())
                        return;
                    TCG.SetDialogStatus(true);
                    TCG.GetChildWindow().Controls.Add(new Window(new Friends(), "Friends"));
                    break;
                case "My Guild":
                    GuildInfoPacket GuildInfo = PacketService.GetGuildInfoPacket();
                    if (string.IsNullOrEmpty(GuildInfo.GetName()))
                        TCG.GetChildWindow().Controls.Add(new Window(new Guild(), "Guilds"));
                    else
                        TCG.GetChildWindow().Controls.Add(new Window(new GuildInfo(GuildInfo), "Guild: " + GuildInfo.GetName()));
                    break;
                case "Guilds":
                    TCG.GetChildWindow().Controls.Add(new Window(new Guild(), "Guilds"));
                    break;
                case "Leaderboards":
                    // TODO
                    break;
                case "Preferences":
                    TCG.GetChildWindow().Controls.Add(new Window(new Preferences(), "Preferences"));
                    break;
                case "Help":
                    Process.Start("https://swgtcg.com/help/index.php");
                    break;
                case "EULA":
                    // TODO
                    break;
            }
            SoundService.PlaySound("button");
            NavBar.ToggleNavBar(false);
            Parent.Dispose();
        }

        private void ImagePictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void TitleLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void NavigatorAddonButton_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void NavigatorAddonButton_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundImage = ResourcesLib.GetImageFromNavigation("menu_item_down");
        }

        private void NavigatorAddonButton_MouseEnter(object sender, EventArgs e)
        {
            BackgroundImage = ResourcesLib.GetImageFromNavigation("menu_item_over");
        }

        private void NavigatorAddonButton_MouseLeave(object sender, EventArgs e)
        {
            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                BackgroundImage = ResourcesLib.GetImageFromNavigation("menu_item");
        }
    }
}
