﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;

namespace TradingCardGame.Components
{
    public partial class LoggingInMessage : UserControl
    {
        private readonly Login login;
        private int seconds = 0;
        private const byte MAX_HANGTIME = 10;

        public LoggingInMessage(Login login)
        {
            InitializeComponent();
            this.login = login;
        }

        private void LoggingInMessage_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
        }

        private void PingTimer_Tick(object sender, EventArgs e)
        {
            if (++seconds > MAX_HANGTIME)
            {
                ((TCG)Parent.Parent).AddSystemMessage("Login connection timed out.", "Login Timed Out");
                PacketService.DisconnectFromServer();
                login.LoginTimedOut();
                Dispose();
            }

            if (!string.IsNullOrEmpty(connectLabel.Text))
                connectLabel.Text = connectLabel.Text.Substring(connectLabel.Text.Length - 3).Equals("...") ? "Connecting to the server" : connectLabel.Text + '.';
            connectLabel.Location = new Point((Width - connectLabel.Width) / 2, connectLabel.Location.Y);
        }
    }
}
