﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TradingCardGame.forms.dialogs;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;

namespace TradingCardGame.components
{
    public partial class MapButton : UserControl
    {
        private readonly byte buttonType;

        public MapButton(byte buttonType)
        {
            InitializeComponent();

            this.buttonType = buttonType;
            LoadButtonData();
            BringToFront();
        }

        private static readonly string[] BACKGROUNDS = {
            "tutorials",
            "casual",
            "trade",
            "tournaments"
        };

        private static readonly string[] TITLES = {
            "Tutorials / Scenarios",
            "Casual Games",
            "Trade",
            "Tournaments"
        };

        private static readonly string[] DESCRIPTIONS = {
            "Learn to play and hone\nyour skills against the AI.",
            "Challenge other players to\nnon-rated games.",
            "Barter with other players\nto gather the cards you're\nlooking for.",
            "Compete in tournaments\nto increase your rating and\nearn rewards."
        };

        private static readonly int[] Y_POINTS = {
            20,
            171,
            322,
            473
        };

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void LoadButtonData()
        {
            BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_" + BACKGROUNDS[buttonType]);
            title.Text = TITLES[buttonType];
            description.Text = DESCRIPTIONS[buttonType];
            Location = new Point(10, Y_POINTS[buttonType]);
        }

        private void MapButton_MouseEnter(object sender, EventArgs e)
        {
            BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_" + BACKGROUNDS[buttonType] + "_over");
        }

        private void MapButton_MouseLeave(object sender, EventArgs e)
        {
            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_" + BACKGROUNDS[buttonType]);
        }

        private void MapButton_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_" + BACKGROUNDS[buttonType] + "_down");
        }

        private void MapButton_MouseUp(object sender, MouseEventArgs e)
        {
            BackgroundImage = ResourcesLib.GetImageFromMapScreen("button_" + BACKGROUNDS[buttonType]);
        }

        private new void Click()
        {
            switch (buttonType)
            {
                case 0:
                    Parent.Parent.Controls.Add(new Window(new SinglePlayerOptions(), "Single Player Options"));
                    break;
                case 1:
                    Parent.Parent.Controls.Add(new Window(new LobbyChooser(), "Select a Lobby"));
                    break;
                case 2:
                    ((TCG)Parent.Parent.Parent).SetChildWindow(new Lobby(LobbyTitles.TRADE));
                    break;
                case 3:
                    ((TCG)Parent.Parent.Parent).SetChildWindow(new Lobby(LobbyTitles.TOURNAMENT));
                    break;
            }
            SoundService.PlaySound("button");
        }

        private void description_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void title_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void MapButton_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }
    }
}
