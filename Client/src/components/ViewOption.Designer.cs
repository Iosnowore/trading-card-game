﻿
namespace TradingCardGame.Src.Components
{
    partial class ViewOption
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.TableViewButton = new System.Windows.Forms.Button();
            this.GraphicViewButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.TitleLabel.ForeColor = System.Drawing.Color.SkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(22, 7);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(30, 13);
            this.TitleLabel.TabIndex = 62;
            this.TitleLabel.Text = "View";
            // 
            // TableViewButton
            // 
            this.TableViewButton.FlatAppearance.BorderSize = 0;
            this.TableViewButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TableViewButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TableViewButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TableViewButton.Location = new System.Drawing.Point(10, 23);
            this.TableViewButton.Name = "TableViewButton";
            this.TableViewButton.Size = new System.Drawing.Size(24, 24);
            this.TableViewButton.TabIndex = 63;
            this.TableViewButton.UseVisualStyleBackColor = true;
            this.TableViewButton.Click += new System.EventHandler(this.TableViewButton_Click);
            // 
            // GraphicViewButton
            // 
            this.GraphicViewButton.FlatAppearance.BorderSize = 0;
            this.GraphicViewButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.GraphicViewButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.GraphicViewButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GraphicViewButton.Location = new System.Drawing.Point(40, 23);
            this.GraphicViewButton.Name = "GraphicViewButton";
            this.GraphicViewButton.Size = new System.Drawing.Size(24, 24);
            this.GraphicViewButton.TabIndex = 64;
            this.GraphicViewButton.UseVisualStyleBackColor = true;
            this.GraphicViewButton.Click += new System.EventHandler(this.GraphicViewButton_Click);
            // 
            // ViewOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.GraphicViewButton);
            this.Controls.Add(this.TableViewButton);
            this.Controls.Add(this.TitleLabel);
            this.Name = "ViewOption";
            this.Size = new System.Drawing.Size(75, 50);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Button TableViewButton;
        private System.Windows.Forms.Button GraphicViewButton;
    }
}
