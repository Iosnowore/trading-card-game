﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;

namespace TradingCardGame.Src.Components.Game
{
    public partial class PhaseMeter : UserControl
    {
        private Mainbase.PHASE Phase;

        private int Turn;

        public PhaseMeter()
        {
            InitializeComponent();

            BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_shelf");

            MenuButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_button_menu");
            PlayerBaseQuestButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_button_quest");

            DrawButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_draw_off");
            QuestButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_quest_off");
            RestoreButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_restore_off");
            MainButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_main_off");
        }

        public void ChangePhase(Mainbase.PHASE Phase)
        {
            this.Phase = Phase;

            if (Phase == Mainbase.PHASE.DRAW)
            {
                // New turn, update Turns counter
                UpdateTurn();

                MainButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_main_off");
                DrawButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_draw_on");
            }
            else if (Phase == Mainbase.PHASE.QUEST)
            {
                DrawButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_draw_off");
                QuestButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_quest_on");
            }
            else if (Phase == Mainbase.PHASE.READY)
            {
                QuestButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_quest_off");
                RestoreButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_restore_on");
            }
            else if (Phase == Mainbase.PHASE.MAIN)
            {
                RestoreButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_restore_off");
                MainButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_main_on");
            }
            else if (Phase == Mainbase.PHASE.OPPONENT_WAIT)
            {
                MainButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_main_off");
            }
        }

        public Mainbase.PHASE GetPhase()
        {
            return Phase;
        }

        private void UpdateTurn()
        {
            Turn++;

            TextLabel.Text = "Turn: " + Turn + " Match: 0";
            TextLabel.Location = new Point((Width - TextLabel.Width) / 2, TextLabel.Location.Y);
        }
    }
}
