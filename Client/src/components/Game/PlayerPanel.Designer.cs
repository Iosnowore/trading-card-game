﻿namespace TradingCardGame.Src.Components.Game
{
    partial class PlayerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ability1 = new System.Windows.Forms.Panel();
            this.Ability2 = new System.Windows.Forms.Panel();
            this.Ability3 = new System.Windows.Forms.Panel();
            this.AvatarCoverPanel = new System.Windows.Forms.Panel();
            this.TopBorder = new System.Windows.Forms.PictureBox();
            this.BonusDamageLabel = new TradingCardGame.Src.Custom.OutlinedTextLabel();
            this.DefenseLabel = new TradingCardGame.Src.Custom.OutlinedTextLabel();
            this.AttackLabel = new TradingCardGame.Src.Custom.OutlinedTextLabel();
            this.AvatarCoverPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopBorder)).BeginInit();
            this.SuspendLayout();
            // 
            // Ability1
            // 
            this.Ability1.BackColor = System.Drawing.Color.Transparent;
            this.Ability1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ability1.Location = new System.Drawing.Point(625, 83);
            this.Ability1.Name = "Ability1";
            this.Ability1.Size = new System.Drawing.Size(130, 111);
            this.Ability1.TabIndex = 53;
            this.Ability1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Ability1_MouseClick);
            // 
            // Ability2
            // 
            this.Ability2.BackColor = System.Drawing.Color.Transparent;
            this.Ability2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ability2.Location = new System.Drawing.Point(760, 83);
            this.Ability2.Name = "Ability2";
            this.Ability2.Size = new System.Drawing.Size(130, 111);
            this.Ability2.TabIndex = 55;
            this.Ability2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Ability2_MouseClick);
            // 
            // Ability3
            // 
            this.Ability3.BackColor = System.Drawing.Color.Transparent;
            this.Ability3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ability3.Location = new System.Drawing.Point(893, 83);
            this.Ability3.Name = "Ability3";
            this.Ability3.Size = new System.Drawing.Size(130, 111);
            this.Ability3.TabIndex = 54;
            this.Ability3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Ability3_MouseClick);
            // 
            // AvatarCoverPanel
            // 
            this.AvatarCoverPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AvatarCoverPanel.Controls.Add(this.BonusDamageLabel);
            this.AvatarCoverPanel.Controls.Add(this.DefenseLabel);
            this.AvatarCoverPanel.Controls.Add(this.AttackLabel);
            this.AvatarCoverPanel.Location = new System.Drawing.Point(348, 61);
            this.AvatarCoverPanel.Name = "AvatarCoverPanel";
            this.AvatarCoverPanel.Size = new System.Drawing.Size(244, 194);
            this.AvatarCoverPanel.TabIndex = 59;
            // 
            // TopBorder
            // 
            this.TopBorder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TopBorder.Location = new System.Drawing.Point(3, 13);
            this.TopBorder.Name = "TopBorder";
            this.TopBorder.Size = new System.Drawing.Size(1021, 27);
            this.TopBorder.TabIndex = 58;
            this.TopBorder.TabStop = false;
            this.TopBorder.Visible = false;
            // 
            // BonusDamageLabel
            // 
            this.BonusDamageLabel.AutoSize = true;
            this.BonusDamageLabel.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BonusDamageLabel.ForeColor = System.Drawing.Color.Moccasin;
            this.BonusDamageLabel.Location = new System.Drawing.Point(60, 93);
            this.BonusDamageLabel.Name = "BonusDamageLabel";
            this.BonusDamageLabel.Size = new System.Drawing.Size(38, 26);
            this.BonusDamageLabel.TabIndex = 24;
            this.BonusDamageLabel.Text = "+0";
            // 
            // DefenseLabel
            // 
            this.DefenseLabel.AutoSize = true;
            this.DefenseLabel.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefenseLabel.ForeColor = System.Drawing.Color.Moccasin;
            this.DefenseLabel.Location = new System.Drawing.Point(70, 58);
            this.DefenseLabel.Name = "DefenseLabel";
            this.DefenseLabel.Size = new System.Drawing.Size(24, 26);
            this.DefenseLabel.TabIndex = 23;
            this.DefenseLabel.Text = "0";
            // 
            // AttackLabel
            // 
            this.AttackLabel.AutoSize = true;
            this.AttackLabel.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackLabel.ForeColor = System.Drawing.Color.Moccasin;
            this.AttackLabel.Location = new System.Drawing.Point(70, 24);
            this.AttackLabel.Name = "AttackLabel";
            this.AttackLabel.Size = new System.Drawing.Size(24, 26);
            this.AttackLabel.TabIndex = 22;
            this.AttackLabel.Text = "0";
            // 
            // PlayerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.AvatarCoverPanel);
            this.Controls.Add(this.Ability1);
            this.Controls.Add(this.Ability2);
            this.Controls.Add(this.Ability3);
            this.Controls.Add(this.TopBorder);
            this.DoubleBuffered = true;
            this.Name = "PlayerPanel";
            this.Size = new System.Drawing.Size(1024, 194);
            this.AvatarCoverPanel.ResumeLayout(false);
            this.AvatarCoverPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopBorder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Ability1;
        private System.Windows.Forms.Panel Ability2;
        private System.Windows.Forms.Panel Ability3;
        private System.Windows.Forms.PictureBox TopBorder;
        private System.Windows.Forms.Panel AvatarCoverPanel;
        private Custom.OutlinedTextLabel AttackLabel;
        private Custom.OutlinedTextLabel DefenseLabel;
        private Custom.OutlinedTextLabel BonusDamageLabel;
    }
}
