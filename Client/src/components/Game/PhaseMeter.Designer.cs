﻿namespace TradingCardGame.Src.Components.Game
{
    partial class PhaseMeter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuButton = new System.Windows.Forms.Button();
            this.DrawButton = new System.Windows.Forms.Button();
            this.QuestButton = new System.Windows.Forms.Button();
            this.RestoreButton = new System.Windows.Forms.Button();
            this.MainButton = new System.Windows.Forms.Button();
            this.TextLabel = new System.Windows.Forms.Label();
            this.PlayerBaseQuestButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MenuButton
            // 
            this.MenuButton.BackColor = System.Drawing.Color.Transparent;
            this.MenuButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MenuButton.FlatAppearance.BorderSize = 0;
            this.MenuButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MenuButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MenuButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MenuButton.Location = new System.Drawing.Point(17, 19);
            this.MenuButton.Name = "MenuButton";
            this.MenuButton.Size = new System.Drawing.Size(40, 40);
            this.MenuButton.TabIndex = 0;
            this.MenuButton.UseVisualStyleBackColor = false;
            // 
            // DrawButton
            // 
            this.DrawButton.BackColor = System.Drawing.Color.Transparent;
            this.DrawButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.DrawButton.FlatAppearance.BorderSize = 0;
            this.DrawButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.DrawButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.DrawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DrawButton.Location = new System.Drawing.Point(55, 3);
            this.DrawButton.Name = "DrawButton";
            this.DrawButton.Size = new System.Drawing.Size(33, 40);
            this.DrawButton.TabIndex = 1;
            this.DrawButton.UseVisualStyleBackColor = false;
            // 
            // QuestButton
            // 
            this.QuestButton.BackColor = System.Drawing.Color.Transparent;
            this.QuestButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.QuestButton.FlatAppearance.BorderSize = 0;
            this.QuestButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.QuestButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.QuestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QuestButton.Location = new System.Drawing.Point(89, 3);
            this.QuestButton.Name = "QuestButton";
            this.QuestButton.Size = new System.Drawing.Size(33, 40);
            this.QuestButton.TabIndex = 2;
            this.QuestButton.UseVisualStyleBackColor = false;
            // 
            // RestoreButton
            // 
            this.RestoreButton.BackColor = System.Drawing.Color.Transparent;
            this.RestoreButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.RestoreButton.FlatAppearance.BorderSize = 0;
            this.RestoreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RestoreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RestoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestoreButton.Location = new System.Drawing.Point(123, 6);
            this.RestoreButton.Name = "RestoreButton";
            this.RestoreButton.Size = new System.Drawing.Size(33, 40);
            this.RestoreButton.TabIndex = 3;
            this.RestoreButton.UseVisualStyleBackColor = false;
            // 
            // MainButton
            // 
            this.MainButton.BackColor = System.Drawing.Color.Transparent;
            this.MainButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MainButton.FlatAppearance.BorderSize = 0;
            this.MainButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MainButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MainButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainButton.Location = new System.Drawing.Point(161, 5);
            this.MainButton.Name = "MainButton";
            this.MainButton.Size = new System.Drawing.Size(33, 40);
            this.MainButton.TabIndex = 4;
            this.MainButton.UseVisualStyleBackColor = false;
            // 
            // TextLabel
            // 
            this.TextLabel.AutoSize = true;
            this.TextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLabel.ForeColor = System.Drawing.Color.White;
            this.TextLabel.Location = new System.Drawing.Point(83, 46);
            this.TextLabel.Name = "TextLabel";
            this.TextLabel.Size = new System.Drawing.Size(86, 13);
            this.TextLabel.TabIndex = 5;
            this.TextLabel.Text = "Turn: 0 Match: 0";
            // 
            // PlayerBaseQuestButton
            // 
            this.PlayerBaseQuestButton.BackColor = System.Drawing.Color.Transparent;
            this.PlayerBaseQuestButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlayerBaseQuestButton.FlatAppearance.BorderSize = 0;
            this.PlayerBaseQuestButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PlayerBaseQuestButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PlayerBaseQuestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayerBaseQuestButton.Location = new System.Drawing.Point(194, 20);
            this.PlayerBaseQuestButton.Name = "PlayerBaseQuestButton";
            this.PlayerBaseQuestButton.Size = new System.Drawing.Size(40, 40);
            this.PlayerBaseQuestButton.TabIndex = 6;
            this.PlayerBaseQuestButton.UseVisualStyleBackColor = false;
            // 
            // PhaseMeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.PlayerBaseQuestButton);
            this.Controls.Add(this.TextLabel);
            this.Controls.Add(this.MainButton);
            this.Controls.Add(this.RestoreButton);
            this.Controls.Add(this.QuestButton);
            this.Controls.Add(this.DrawButton);
            this.Controls.Add(this.MenuButton);
            this.DoubleBuffered = true;
            this.Name = "PhaseMeter";
            this.Size = new System.Drawing.Size(252, 68);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button MenuButton;
        private System.Windows.Forms.Button DrawButton;
        private System.Windows.Forms.Button QuestButton;
        private System.Windows.Forms.Button RestoreButton;
        private System.Windows.Forms.Button MainButton;
        private System.Windows.Forms.Label TextLabel;
        private System.Windows.Forms.Button PlayerBaseQuestButton;
    }
}
