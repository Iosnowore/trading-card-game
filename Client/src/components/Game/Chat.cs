﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Chat;

namespace TradingCardGame.Src.Components.Game
{
    public partial class Chat : UserControl
    {
        public Chat()
        {
            InitializeComponent();

            pictureBox5.Image = ResourcesLib.GetImageFromGamescreen("texttoggle_playerlist");
            pictureBox6.Image = ResourcesLib.GetImageFromGamescreen("texttoggle_both");
        }

        private void ChatInputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChatService.ChatBoxKeyEnter(chatInputTextBox, chatListBox, e);
            SoundService.KeyPressSound(e.KeyCode);
        }

        public void AddText(string Text)
        {
            chatListBox.Items.Add("* " + Text);
        }
    }
}
