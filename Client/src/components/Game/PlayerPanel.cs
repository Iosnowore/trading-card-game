﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Cards;
using Resources;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Custom;

namespace TradingCardGame.Src.Components.Game
{
    public partial class PlayerPanel : UserControl
    {
        private readonly ItemsPanel ItemsPanel;

        private readonly string Position;
        private readonly string Archetype;

        private readonly int AvatarId;

        private readonly Button InfoButton;

        private readonly PictureBox[] AvatarHealthBars;
        private PictureBox[] AvatarPowerBars;

        private readonly SizeableBackgroundPanel AvatarImagePanel;

        private EffectPlayer AvatarExcertEffect;

        // <Card: AbilityCardId, bool: >
        private readonly List<Card> Abilities;

        public PlayerPanel(string Username, int AvatarId, string Archetype, string AvatarImageString, int AvatarHealth, int AvatarAttack, int AvatarDefense, int AvatarBonusDamage, int CardsInHand, int CardsInDeck, string Position)
        {
            InitializeComponent();

            Abilities = new List<Card>();

            this.Archetype = Archetype;

            Ability3.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_ability3");
            Ability2.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_ability2");
            Ability1.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_ability1");
            TopBorder.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_" + Position + "_tile");
            AvatarCoverPanel.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_" + Archetype + "_" + Position);
            SetStyle(ControlStyles.ResizeRedraw, true);

            this.Position = Position;
            this.AvatarId = AvatarId;

            ItemsPanel = new ItemsPanel(Position, CardsInHand, CardsInDeck, Archetype);
            ItemsPanel.Location = new Point(0, Position.Equals("bottom") ? Height - ItemsPanel.Height : 0);
            Controls.Add(ItemsPanel);

            ItemsPanel.SetUsername(Username);

            // Set Avatar Health Bars
            AvatarHealthBars = new PictureBox[AvatarHealth];

            int BarHeight = 100 / AvatarHealth;

            int X = Position.Equals("bottom") ? 28 : 25;
            int Y = Position.Equals("bottom") ? 82 : 22;

            for (int i = 0; i < AvatarHealthBars.Length; i++)
            {
                PictureBox HealthBox = new PictureBox
                {
                    BackColor = Color.Red,
                    BorderStyle = BorderStyle.FixedSingle,
                    Location = new Point(X, Y + (i * BarHeight)),
                    Size = new Size(6, BarHeight)
                };

                // Save the health bar to memory
                AvatarHealthBars[i] = HealthBox;

                // Add the health box to the UI
                AvatarCoverPanel.Controls.Add(HealthBox);
            }

            // Set Avatar Power Bars
            int StartingPower = 3;
            SetPowerBars(StartingPower);

            Image BackgroundImage = CardsLib.GetImageFromCards(AvatarImageString);
            AvatarImagePanel = new SizeableBackgroundPanel(100, Position.Equals("bottom") ? 80 : 20, 100, 100, BackgroundImage)
            {
                Location = new Point(390, Position.Equals("bottom") ? 0 : -10),
                Size = new Size(244, 194)
            };
            Controls.Add(AvatarImagePanel);

            AvatarCoverPanel.Parent = AvatarImagePanel;
            AvatarCoverPanel.Dock = DockStyle.Fill;
            AvatarCoverPanel.MouseClick += AvatarPanel_MouseClick;
            AvatarCoverPanel.MouseDoubleClick += AvatarPanel_MouseDoubleClick;

            if (Position.Equals("top"))
            {
                Ability1.Location = new Point(Ability1.Location.X, 0);
                Ability2.Location = new Point(Ability2.Location.X, 0);
                Ability3.Location = new Point(Ability3.Location.X, 0);

                InfoButton = new Button
                {
                    Location = new Point(198, 34),
                    Size = new Size(26, 83),
                    FlatStyle = FlatStyle.Flat
                };
                InfoButton.FlatAppearance.BorderSize = 0;
                InfoButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
                InfoButton.FlatAppearance.MouseDownBackColor = Color.Transparent;

                InfoButton.BackgroundImageLayout = ImageLayout.Zoom;
                InfoButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("button_conditions");

                InfoButton.MouseEnter += InfoButton_MouseEnter;
                InfoButton.MouseLeave += InfoButton_MouseLeave;
                InfoButton.MouseDown += InfoButton_MouseDown;
                InfoButton.MouseUp += InfoButton_MouseUp;
                InfoButton.MouseClick += InfoButton_MouseClick;

                AvatarCoverPanel.Controls.Add(InfoButton);
            }
            else
            {
                AttackLabel.Location = new Point(AttackLabel.Location.X, AttackLabel.Location.Y + 60);
                DefenseLabel.Location = new Point(DefenseLabel.Location.X, DefenseLabel.Location.Y + 60);
                BonusDamageLabel.Location = new Point(BonusDamageLabel.Location.X, BonusDamageLabel.Location.Y + 58);
            }

            AttackLabel.Text = AvatarAttack.ToString();
            DefenseLabel.Text = AvatarDefense.ToString();
            BonusDamageLabel.Text = "+" + AvatarBonusDamage;
        }

        private void InfoButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("button");

            ((Forms.Screens.Game)Parent).ShowGameInfo();
        }

        private void AvatarPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Forms.Screens.Game Game = (Forms.Screens.Game)Parent;
            Game.DoubleClickAvatar();
        }

        private void AvatarPanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                // Get CardData ready
                CardData CardData = TCGData.Cards.GetCardData(AvatarId);

                // Preview avatar card with the constructed card data
                Forms.Screens.Game Game = (Forms.Screens.Game)Parent;
                Game.PreviewCard(CardData);
            }
        }

        private void InfoButton_MouseUp(object sender, MouseEventArgs e)
        {
            InfoButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("button_conditions_over");
        }

        private void InfoButton_MouseDown(object sender, MouseEventArgs e)
        {
            InfoButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("button_conditions_down");
        }

        private void InfoButton_MouseLeave(object sender, System.EventArgs e)
        {
            InfoButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("button_conditions");
        }

        private void InfoButton_MouseEnter(object sender, System.EventArgs e)
        {
            InfoButton.BackgroundImage = ResourcesLib.GetImageFromGamescreen("button_conditions_over");
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Image BackgroundImageMain = ResourcesLib.GetImageFromGamescreen("playerbase_" + Archetype + "_tile_" + Position);
            base.OnPaintBackground(e);
            Rectangle rc = new Rectangle(0,
                Position.Equals("bottom") ? Height - 111 : 0,
                BackgroundImageMain.Width, 111);
            e.Graphics.DrawImage(BackgroundImageMain, rc);
            BackgroundImageLayout = ImageLayout.None;
        }

        public int GetAvatarHealth()
        {
            int TotalHeath = 0;

            foreach (PictureBox HealthBar in AvatarHealthBars)
                if (HealthBar.BackColor == Color.Red)
                    TotalHeath++;
            return TotalHeath;
        }

        public void UpdateAvatarHealth(int NewHealth)
        {
            int HealthLost = AvatarHealthBars.Length - NewHealth;

            // The lower index is towards the top, change those.
            for (int i = 0; i < HealthLost; i++)
                AvatarHealthBars[i].BackColor = Color.Gray;

            // Setup animation
            EffectPlayer EffectPlayer = new EffectPlayer("cutter_", 34, 2, new Size(100, 100), false, null, true);
            EffectPlayer.Location = new Point(100, Position.Equals("bottom") ? 80 : 20);
            AvatarImagePanel.Controls.Add(EffectPlayer);
            EffectPlayer.BringToFront();

            // Play the animation
            EffectPlayer.StartAnimation(true);

            // Play sound effect
            SoundService.PlaySound("npc_male_damage");
        }

        public void UpdateAvatarPower(int NewPower)
        {
            if (NewPower > AvatarPowerBars.Length)
            {
                // Max power changed
                SetPowerBars(NewPower);
            }
            else
            {
                // Set the power to full
                if (NewPower == AvatarPowerBars.Length)
                {
                    for (int i = 0; i < NewPower; i++)
                        AvatarPowerBars[i].BackColor = Color.SkyBlue;
                }
                else
                {
                    int PowerLost = AvatarPowerBars.Length - NewPower;

                    // The lower index is towards the top, change those.
                    for (int i = 0; i < PowerLost; i++)
                        AvatarPowerBars[i].BackColor = Color.Gray;
                }
            }
        }

        public void PreviewAvatarPower(int CardCost)
        {
            int HealthPreviewed = 0;

            // Make only blue bars yellow (real power left)
            for (int i = 0; i < AvatarPowerBars.Length; i++)
            {
                if (AvatarPowerBars[i].BackColor == Color.SkyBlue)
                {
                    AvatarPowerBars[i].BackColor = Color.Yellow;
                    HealthPreviewed++;
                }

                // Exit the loop if we are done
                if (HealthPreviewed == CardCost)
                    break;
            }
        }

        public void UnpreviewAvatarPower()
        {
            for (int i = 0; i < AvatarPowerBars.Length; i++)
                if (AvatarPowerBars[i].BackColor == Color.Yellow)
                    AvatarPowerBars[i].BackColor = Color.SkyBlue;
        }

        public bool IsPreviewingAvatarPower()
        {
            for (int i = 0; i < AvatarPowerBars.Length; i++)
                if (AvatarPowerBars[i].BackColor == Color.Yellow)
                    return true;
            return false;
        }

        public string GetUsername()
        {
            return ItemsPanel.GetUsername();
        }

        public void UpdateCardStats(int HandQty, int DeckQty, int DiscardedQty)
        {
            ItemsPanel.UpdateCardStats(HandQty, DeckQty, DiscardedQty);
        }

        public int GetAvailablePower()
        {
            int TotalPower = 0;

            foreach (PictureBox PowerBar in AvatarPowerBars)
                if (PowerBar.BackColor == Color.SkyBlue)
                    TotalPower++;
            return TotalPower;
        }

        private void Ability1_MouseClick(object sender, MouseEventArgs e)
        {
            AbilityClick(Ability1);
        }

        private void Ability2_MouseClick(object sender, MouseEventArgs e)
        {
            AbilityClick(Ability2);
        }

        private void Ability3_MouseClick(object sender, MouseEventArgs e)
        {
            AbilityClick(Ability3);
        }

        private void AbilityClick(Panel Panel)
        {
            if (Panel.Controls.Count == 0)
            {
                Forms.Screens.Game Game = (Forms.Screens.Game)Parent;

                Card SelectedCard = Game.GetSelectedCard();

                if (SelectedCard != null && SelectedCard.GetCardData().GetType().Equals("Ability"))
                {
                    // Let server know player wants to place card
                    int CardId = SelectedCard.GetCardData().GetId();

                    // Construct the card placement packet
                    CommandPacket PlacePacket = new CommandPacket(new object[] { CardId });

                    PacketService.SendPacket(Commands.PLACE_CARD, PlacePacket);
                }
            }
        }

        public void RemoveAllAbilityCards()
        {
            if (Ability1.Controls.Count == 1)
                Ability1.Controls[0].Dispose();
            if (Ability2.Controls.Count == 1)
                Ability2.Controls[0].Dispose();
            if (Ability3.Controls.Count == 1)
                Ability3.Controls[0].Dispose();
            Abilities.Clear();
        }

        public void PlaceAbility(PlaymatCard AbilityCard)
        {
            CardData CardData = TCGData.Cards.GetCardData(AbilityCard.GetCardId());
            Card Card = new Card(CardData, AbilityCard, Card.SIZE.TOOLBAR);

            if (Ability1.Controls.Count == 0)
                Ability1.Controls.Add(Card);
            else if (Ability2.Controls.Count == 0)
                Ability2.Controls.Add(Card);
            else if (Ability3.Controls.Count == 0)
                Ability3.Controls.Add(Card);
            Abilities.Add(Card);
        }

        public List<Card> GetAbilityCards()
        {
            return Abilities;
        }

        public int GetAvatarId()
        {
            return AvatarId;
        }

        public void PlaceItem(PlaymatCard PlaymatCard)
        {
            ItemsPanel.PlaceItem(PlaymatCard);
        }

        public List<Card> GetItems()
        {
            return ItemsPanel.GetItems();
        }

        private void SetPowerBars(int Power)
        {
            if (AvatarPowerBars != null)
                for (int i = 0; i < AvatarPowerBars.Length; i++)
                    AvatarPowerBars[i].Dispose();

            AvatarPowerBars = new PictureBox[Power];
            int BarHeight = 100 / AvatarPowerBars.Length;

            int X = Position.Equals("bottom") ? 18 : 15;
            int Y = Position.Equals("bottom") ? 82 : 22;

            for (int i = 0; i < AvatarPowerBars.Length; i++)
            {
                PictureBox PowerBox = new PictureBox
                {
                    BackColor = Color.SkyBlue,
                    BorderStyle = BorderStyle.FixedSingle,
                    Location = new Point(X, Y + (i * BarHeight)),
                    Size = new Size(6, BarHeight)
                };

                // Add PowerBar to memory
                AvatarPowerBars[i] = PowerBox;

                // Add power bar to UI
                AvatarCoverPanel.Controls.Add(PowerBox);
            }
        }

        public void PlayExertEffectOnAvatar(int ExertType)
        {
            string ImageString;

            switch (ExertType)
            {
                case Forms.Screens.Game.EXERT_ATTACK:
                    ImageString = "exhaust_attack_avatar_";
                    SoundService.PlaySound("attack_monster");
                    break;
                case Forms.Screens.Game.EXERT_DAMAGE:
                    ImageString = "exhaust_damage_avatar_";
                    SoundService.PlaySound("attack_monster");
                    break;
                default:
                    ImageString = "exhaust_defend_avatar_";
                    SoundService.PlaySound("defend_monster");
                    break;
            }

            AvatarExcertEffect = new EffectPlayer(ImageString, 40, 2, new Size(100, 100), false, null, false);
            AvatarImagePanel.Controls.Add(AvatarExcertEffect);
            AvatarExcertEffect.Location = new Point(100, Position.Equals("bottom") ? 80 : 20);
            AvatarExcertEffect.BringToFront();
            AvatarExcertEffect.StartAnimation(true);
        }

        public void AlternateBorder()
        {
            if (AvatarCoverPanel.BorderStyle == BorderStyle.Fixed3D)
                AvatarCoverPanel.BorderStyle = BorderStyle.None;
            else
                AvatarCoverPanel.BorderStyle = BorderStyle.Fixed3D;
        }

        public void RemoveAvatarHintAnimation()
        {
            AvatarCoverPanel.BorderStyle = BorderStyle.None;
        }

        public void ReadyAvatar()
        {
            if (AvatarExcertEffect != null)
                AvatarExcertEffect.Dispose();
        }

        public Card GetUnexertedAbilityCard(int CardId)
        {
            foreach (Card Ability in Abilities)
                if (Ability.GetId() == CardId && !Ability.GetPlaymatCard().GetExerted())
                    return Ability;
            return null;
        }

        public Card GetUnexertedItemCard(int CardId)
        {
            foreach (Card Item in ItemsPanel.GetItems())
                if (Item.GetId() == CardId && !Item.GetPlaymatCard().GetExerted())
                    return Item;
            return null;
        }
    }
}
