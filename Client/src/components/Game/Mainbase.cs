﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Effects;
using Resources;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Font;

namespace TradingCardGame.Src.Components.Game
{
    public partial class Mainbase : UserControl
    {
        private const int MAX_PHASE = 29;
        public const int PLAYER = 0;
        public const int ENEMY = 1;

        private readonly PHASE GamePhase;

        private int phase = 0;

        private readonly Button Button1;
        private readonly Button Button2;

        public enum PHASE
        {
            REDRAW,
            OPPONENT_WAIT,
            DRAW,
            QUEST,
            READY,
            MAIN,
            RAID,
            RAID_TAKE_DAMAGE,
            GAME_ENDED
        };

        public Mainbase(PHASE GamePhase)
        {
            InitializeComponent();

            if (GamePhase == PHASE.OPPONENT_WAIT)
            {
                BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_dialogue_top");
                Size = new Size(394, 105);
            }
            else
            {
                BackgroundImage = ResourcesLib.GetImageFromGamescreen("phase_dialogue_bottom");

                // Create Buttons
                if (GamePhase == PHASE.REDRAW || GamePhase == PHASE.MAIN || GamePhase == PHASE.RAID)
                {
                    Button1 = new Button();
                    Button1.Location = new Point(287, 15);
                    Button1.Size = new Size(78, 20);

                    if (GamePhase == PHASE.REDRAW)
                        Button1.Text = "REDRAW";
                    else if (GamePhase == PHASE.MAIN)
                        Button1.Text = "DONE";
                    else if (GamePhase == PHASE.RAID)
                        Button1.Text = "PASS";
                    else if (GamePhase == PHASE.GAME_ENDED)
                        Button1.Text = "Menu";

                    Button1.FlatStyle = FlatStyle.Flat;
                    Button1.FlatAppearance.MouseDownBackColor = Color.Transparent;
                    Button1.BackgroundImage = ResourcesLib.GetImageFromGamescreen("mainbase_button");
                    Button1.BackgroundImageLayout = ImageLayout.Stretch;
                    Button1.Image = EffectsLib.GetImageFromEffects("phase_button_hint_00");
                    CustomFontService.SetCustomFont(Button1, FontStyle.Bold, 8);
                    Button1.Click += Button1_Click;
                    Button1.MouseEnter += Button_MouseEnter;
                    Button1.MouseLeave += Button_MouseLeave;
                    Button1.MouseDown += Button_MouseDown;
                    Controls.Add(Button1);
                }

                if (GamePhase == PHASE.REDRAW || GamePhase == PHASE.GAME_ENDED)
                {
                    Button2 = new Button();
                    Button2.Location = new Point(287, 37);
                    Button2.Size = new Size(78, 20);

                    if (GamePhase == PHASE.REDRAW)
                        Button2.Text = "KEEP";
                    else if (GamePhase == PHASE.GAME_ENDED)
                        Button2.Text = "Next";

                    Button2.FlatStyle = FlatStyle.Flat;
                    Button2.FlatAppearance.MouseDownBackColor = Color.Transparent;
                    Button2.BackgroundImage = ResourcesLib.GetImageFromGamescreen("mainbase_button");
                    Button2.BackgroundImageLayout = ImageLayout.Stretch;
                    Button2.Image = EffectsLib.GetImageFromEffects("phase_button_hint_00");
                    CustomFontService.SetCustomFont(Button2, FontStyle.Bold, 8);
                    Button2.Click += Button2_Click;
                    Button2.MouseEnter += Button_MouseEnter;
                    Button2.MouseLeave += Button_MouseLeave;
                    Button2.MouseDown += Button_MouseDown;
                    Controls.Add(Button2);
                }

                mainbaseLabel.Width = Width - mainbaseLabel.Location.X;
            }
            this.GamePhase = GamePhase;
        }

        private void Button_MouseDown(object sender, MouseEventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromGamescreen("mainbase_button_down");
        }

        private void Button_MouseLeave(object sender, System.EventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromGamescreen("mainbase_button");
        }

        private void Button_MouseEnter(object sender, System.EventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromGamescreen("mainbase_button_over");
        }

        public void SetEnabled(bool Enabled)
        {
            Visible = Enabled;
        }

        public void SetText(string Text)
        {
            mainbaseLabel.Text = Text;
        }

        private void Button1_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            Forms.Screens.Game Game = (Forms.Screens.Game)Parent;

            switch (GamePhase)
            {
                case PHASE.REDRAW:
                    CommandPacket CommandPacket = new CommandPacket(new object[] { true });
                    PacketService.SendPacket(Commands.REDRAW_HAND, CommandPacket);
                    Dispose();
                    break;
                case PHASE.MAIN:
                    // Let server know that we are done with our turn.
                    PacketService.SendPacket(Commands.MAIN_PHASE, null);

                    // Update PhaseMeter
                    Game.ChangePhase(PHASE.OPPONENT_WAIT);

                    // Stop Hand Hint animation
                    Game.StopCardHintAnimation();

                    Dispose();
                    break;
                case PHASE.RAID:
                    // Let server know we're done with the raid turn
                    PacketService.SendPacket(Commands.RAID_END_TURN, null);

                    // Stop the exert hint animation
                    Game.StopCardHintAnimation();

                    Dispose();
                    break;
                case PHASE.GAME_ENDED:
                    // "Menu" -> Return to the selection screen (TODO)
                    TCGCore.GetTCG().SetChildWindow(new Map());
                    break;
            }
            Game.ShowEnemyMainbase(true);
        }

        private void Button2_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            Forms.Screens.Game Game = (Forms.Screens.Game)Parent;

            switch (GamePhase)
            {
                case PHASE.REDRAW:
                    CommandPacket CommandPacket = new CommandPacket(new object[] { false });
                    PacketService.SendPacket(Commands.REDRAW_HAND, CommandPacket);
                    Dispose();
                    break;
                case PHASE.GAME_ENDED:
                    // "Next" -> Next scenario (TODO)
                    TCGCore.GetTCG().SetChildWindow(new Map());
                    break;
            }
            Game.ShowEnemyMainbase(true);
        }

        private void Mainbase_Load(object sender, System.EventArgs e)
        {
            if (GamePhase == PHASE.REDRAW || GamePhase == PHASE.MAIN || GamePhase == PHASE.RAID)
            {
                mainbaseLabel.MaximumSize = new Size(252, 32);

                Timer Timer = new Timer
                {
                    Interval = 30
                };
                Timer.Tick += Timer_Tick;
                Timer.Enabled = true;
                Timer.Start();
            }
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            if (++phase > MAX_PHASE)
            {
                phase = 0;
            }

            string Phase = phase < 10 ? ("0" + phase) : phase.ToString();

            Button1.Image = EffectsLib.GetImageFromEffects("phase_button_hint_" + Phase);

            if (Button2 != null)
                Button2.Image = EffectsLib.GetImageFromEffects("phase_button_hint_" + Phase);
        }

        public PHASE GetPhase()
        {
            return GamePhase;
        }
    }
}
