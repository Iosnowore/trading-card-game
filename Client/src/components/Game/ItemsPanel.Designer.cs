﻿namespace TradingCardGame.Src.Components.Game
{
    partial class ItemsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cardsInDicardPileLabel = new System.Windows.Forms.Label();
            this.cardsInDeckLabel = new System.Windows.Forms.Label();
            this.cardsInHandLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.ItemsPanelRight = new System.Windows.Forms.Button();
            this.ItemsPanelMiddle = new System.Windows.Forms.Button();
            this.ItemsPanelLeft = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cardsInDicardPileLabel
            // 
            this.cardsInDicardPileLabel.AutoSize = true;
            this.cardsInDicardPileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInDicardPileLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInDicardPileLabel.Location = new System.Drawing.Point(37, 135);
            this.cardsInDicardPileLabel.Name = "cardsInDicardPileLabel";
            this.cardsInDicardPileLabel.Size = new System.Drawing.Size(21, 24);
            this.cardsInDicardPileLabel.TabIndex = 20;
            this.cardsInDicardPileLabel.Text = "0";
            // 
            // cardsInDeckLabel
            // 
            this.cardsInDeckLabel.AutoSize = true;
            this.cardsInDeckLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInDeckLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInDeckLabel.Location = new System.Drawing.Point(37, 99);
            this.cardsInDeckLabel.Name = "cardsInDeckLabel";
            this.cardsInDeckLabel.Size = new System.Drawing.Size(21, 24);
            this.cardsInDeckLabel.TabIndex = 19;
            this.cardsInDeckLabel.Text = "0";
            // 
            // cardsInHandLabel
            // 
            this.cardsInHandLabel.AutoSize = true;
            this.cardsInHandLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardsInHandLabel.ForeColor = System.Drawing.Color.White;
            this.cardsInHandLabel.Location = new System.Drawing.Point(37, 63);
            this.cardsInHandLabel.Name = "cardsInHandLabel";
            this.cardsInHandLabel.Size = new System.Drawing.Size(21, 24);
            this.cardsInHandLabel.TabIndex = 18;
            this.cardsInHandLabel.Text = "0";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.BackColor = System.Drawing.Color.Transparent;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.White;
            this.usernameLabel.Location = new System.Drawing.Point(25, 27);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(105, 24);
            this.usernameLabel.TabIndex = 15;
            this.usernameLabel.Text = "Username";
            // 
            // ItemsPanelRight
            // 
            this.ItemsPanelRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ItemsPanelRight.FlatAppearance.BorderSize = 0;
            this.ItemsPanelRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ItemsPanelRight.Location = new System.Drawing.Point(285, 59);
            this.ItemsPanelRight.Name = "ItemsPanelRight";
            this.ItemsPanelRight.Size = new System.Drawing.Size(115, 111);
            this.ItemsPanelRight.TabIndex = 21;
            this.ItemsPanelRight.UseVisualStyleBackColor = true;
            this.ItemsPanelRight.Click += new System.EventHandler(this.ItemsPanelRight_Click);
            // 
            // ItemsPanelMiddle
            // 
            this.ItemsPanelMiddle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ItemsPanelMiddle.FlatAppearance.BorderSize = 0;
            this.ItemsPanelMiddle.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelMiddle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelMiddle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ItemsPanelMiddle.Location = new System.Drawing.Point(98, 59);
            this.ItemsPanelMiddle.Name = "ItemsPanelMiddle";
            this.ItemsPanelMiddle.Size = new System.Drawing.Size(187, 111);
            this.ItemsPanelMiddle.TabIndex = 22;
            this.ItemsPanelMiddle.UseVisualStyleBackColor = true;
            this.ItemsPanelMiddle.Click += new System.EventHandler(this.ItemsPanelMiddle_Click);
            // 
            // ItemsPanelLeft
            // 
            this.ItemsPanelLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ItemsPanelLeft.FlatAppearance.BorderSize = 0;
            this.ItemsPanelLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ItemsPanelLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ItemsPanelLeft.Location = new System.Drawing.Point(66, 59);
            this.ItemsPanelLeft.Name = "ItemsPanelLeft";
            this.ItemsPanelLeft.Size = new System.Drawing.Size(32, 111);
            this.ItemsPanelLeft.TabIndex = 23;
            this.ItemsPanelLeft.UseVisualStyleBackColor = true;
            this.ItemsPanelLeft.Click += new System.EventHandler(this.ItemsPanelLeft_Click);
            // 
            // ItemsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.ItemsPanelLeft);
            this.Controls.Add(this.ItemsPanelMiddle);
            this.Controls.Add(this.ItemsPanelRight);
            this.Controls.Add(this.cardsInDicardPileLabel);
            this.Controls.Add(this.cardsInDeckLabel);
            this.Controls.Add(this.cardsInHandLabel);
            this.Controls.Add(this.usernameLabel);
            this.DoubleBuffered = true;
            this.Name = "ItemsPanel";
            this.Size = new System.Drawing.Size(400, 170);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cardsInDicardPileLabel;
        private System.Windows.Forms.Label cardsInDeckLabel;
        private System.Windows.Forms.Label cardsInHandLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Button ItemsPanelRight;
        private System.Windows.Forms.Button ItemsPanelMiddle;
        private System.Windows.Forms.Button ItemsPanelLeft;
    }
}
