﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;
using TradingCardGame.Src.Services.Font;

namespace TradingCardGame.Src.Components.Game
{
    public partial class ScenarioDialog : UserControl
    {
        private readonly Forms.Screens.Game Game;

        public ScenarioDialog(string Text, Forms.Screens.Game Game)
        {
            InitializeComponent();

            this.Game = Game;

            BackgroundImage = ResourcesLib.GetImageFromBorders("navframe_darker");

            ButtonAnimation.FormatButton(OKButton);

            CustomFontService.SetCustomFont(TextLabel, FontStyle.Regular);

            TextLabel.Text = Text;
        }

        private void OKButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");

            if (Game != null)
                Game.DialogClosed();

            Dispose();
        }
    }
}
