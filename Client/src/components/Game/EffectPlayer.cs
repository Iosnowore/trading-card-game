﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Effects;
using System.Drawing;
using System.Windows.Forms;

namespace TradingCardGame.Src.Components.Game
{
    public class EffectPlayer : PictureBox
    {
        private readonly string EffectRoot;

        private readonly int MaxValue;
        private readonly int NumPlaceValues;
        private readonly bool GameScreen;

        private readonly Timer Timer;
        private readonly UserControl Control;

        private readonly bool DestroyOnFinish;

        private int Index;

        private bool RunOnce;

        public EffectPlayer(string EffectRoot, int MaxValue, int NumPlaceValues, Size Size, bool GameScreen, UserControl Control, bool DestroyOnFinish)
        {
            this.EffectRoot = EffectRoot;
            this.MaxValue = MaxValue;
            this.NumPlaceValues = NumPlaceValues;
            this.Size = Size;
            this.GameScreen = GameScreen;
            this.DestroyOnFinish = DestroyOnFinish;

            this.Control = Control;

            Index = 0;

            BackgroundImageLayout = ImageLayout.Stretch;

            Timer = new Timer();
            Timer.Interval = 50;
            Timer.Tick += Timer_Tick;
        }

        public void StartAnimation(bool RunOnce)
        {
            this.RunOnce = RunOnce;

            Timer.Start();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            string Index = string.Empty;
            int LocalMaxPlaceValues = NumPlaceValues - this.Index.ToString().Length;

            for (int i = 0; i < LocalMaxPlaceValues; i++)
                Index += "0";

            Index += this.Index.ToString();

            System.Console.WriteLine("Playing Effect: " + EffectRoot + Index);
            BackgroundImage = GameScreen ? EffectsLib.GetImageFromGamescreen(EffectRoot + Index) : EffectsLib.GetImageFromEffects(EffectRoot + Index);
            BackColor = Color.Transparent;

            this.Index++;

            if (this.Index > MaxValue)
                if (RunOnce)
                    FinishedTimer();
                else
                    this.Index = 0;
        }

        private void FinishedTimer()
        {
            // Stop the timer
            Timer.Stop();

            if (Control != null)
            {
                // Call the stop event handler for the given control
                if (Control is Forms.Screens.Game Game)
                {
                    Game.HandleEffectCompleted();
                }
                else if (Control is Card Card)
                {
                    Card.Dispose();
                }
            }

            if (DestroyOnFinish)
                Dispose();
        }

        public void Destroy()
        {
            Timer.Dispose();
            Dispose();
        }
    }
}
