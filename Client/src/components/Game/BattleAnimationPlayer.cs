﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Effects;
using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.Src.Custom;
using TradingCardGame.Src.Services.Font;

namespace TradingCardGame.Src.Components.Game
{
    public class BattleAnimationPlayer : Panel
    {
        private readonly string BattleType;

        private int Value;

        private readonly EffectPlayer EffectPlayer;
        private readonly Panel BattleTypePanel;

        private const int LOC_MARGIN = 20;

        private readonly Timer Timer;

        private const int MAX_INDEX = 33;
        private int CurrentIndex = 0;

        private readonly OutlinedTextLabel Label;

        public BattleAnimationPlayer(string Location, string BattleType, int Value, UserControl Game)
        {
            this.BattleType = BattleType;
            this.Value = Value;

            Size = new Size(100, 100);

            // Set the location
            if (Location.Equals("top"))
            {
                this.Location = new Point((Game.Width - Width) / 2, (Game.Height - Height) / 2 - Height - LOC_MARGIN);
            }
            else if (Location.Equals("bottom"))
            {
                this.Location = new Point((Game.Width - Width) / 2, (Game.Height - Height) / 2 + Height + LOC_MARGIN);
            }

            // Set the default background
            BackgroundImage = ResourcesLib.GetImageFromGamescreen("battle_" + Location);
            BackgroundImageLayout = ImageLayout.Zoom;
            BackColor = Color.Transparent;

            // Set battle type - This needs to be a panel since we are having text
            BattleTypePanel = new Panel
            {
                BackgroundImage = EffectsLib.GetImageFromGamescreen("battle_" + BattleType + "_00"),
                BackgroundImageLayout = ImageLayout.Zoom,
                BackColor = Color.Transparent,
                Size = new Size(90, 90)
            };
            BattleTypePanel.Location = new Point((Width - BattleTypePanel.Width) / 2, (Height - BattleTypePanel.Height) / 2);
            Controls.Add(BattleTypePanel);

            // Setup the Value label
            Label = new OutlinedTextLabel
            {
                AutoSize = true,
                ForeColor = Color.Moccasin,
                Text = Value.ToString(),
                Visible = false
            };
            CustomFontService.SetCustomFont(Label, FontStyle.Bold, 24);
            BattleTypePanel.Controls.Add(Label);
            Label.BringToFront();
            Label.Location = new Point((BattleTypePanel.Width - Label.Width) / 2, (BattleTypePanel.Height - Label.Height) / 2);

            // We can use an Effect player for the bonus damage since there is no text on top of it
            EffectPlayer = new EffectPlayer("battle_damage_", 30, 2, new Size(40, 40), true, null, false);

            if (Location.Equals("top"))
            {
                EffectPlayer.Location = new Point((Width - EffectPlayer.Width) / 2, -5);
            }
            else
            {
                EffectPlayer.Location = new Point((Width - EffectPlayer.Width) / 2, Height - EffectPlayer.Height + 5);
            }

            Controls.Add(EffectPlayer);
            EffectPlayer.BringToFront();

            Timer = new Timer();
            Timer.Interval = 50;
            Timer.Tick += Timer_Tick;
        }

        public void RunAnimation()
        {
            Timer.Start();
            EffectPlayer.StartAnimation(true);
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            CurrentIndex++;

            string IndexString = CurrentIndex.ToString();

            if (CurrentIndex < 10)
                IndexString = "0" + IndexString;

            BattleTypePanel.BackgroundImage = EffectsLib.GetImageFromGamescreen("battle_" + BattleType + "_" + IndexString);

            if (CurrentIndex == MAX_INDEX)
            {
                Timer.Stop();

                // Display damage/defense amount
                Label.Visible = true;
            }
        }

        public void SetValue(int Value)
        {
            this.Value = Value;

            // Reset and run animation again
            CurrentIndex = 0;
            Label.Visible = false;
            Label.Text = Value.ToString();
            Timer.Start();
        }

        public int GetValue()
        {
            return Value;
        }

        public void AddDamage(int Value)
        {
            //@TODO: Implement
        }

        public int GetDamage()
        {
            //@TODO: Implement
            return 1;
        }
    }
}
