﻿namespace TradingCardGame.Src.Components.Game
{
    partial class Mainbase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainbaseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainbaseLabel
            // 
            this.mainbaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainbaseLabel.ForeColor = System.Drawing.Color.SandyBrown;
            this.mainbaseLabel.Location = new System.Drawing.Point(15, 15);
            this.mainbaseLabel.Name = "mainbaseLabel";
            this.mainbaseLabel.Size = new System.Drawing.Size(252, 32);
            this.mainbaseLabel.TabIndex = 4;
            this.mainbaseLabel.Text = "Would you like to deal 1 damage to\r\nyour avatar to redraw your hand?";
            // 
            // Mainbase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.mainbaseLabel);
            this.DoubleBuffered = true;
            this.Name = "Mainbase";
            this.Size = new System.Drawing.Size(394, 64);
            this.Load += new System.EventHandler(this.Mainbase_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label mainbaseLabel;
    }
}
