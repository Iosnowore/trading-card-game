﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;

namespace TradingCardGame.Src.Components.Game
{
    public partial class ItemsPanel : UserControl
    {
        private readonly string Position;

        private readonly string Archetype;

        private readonly List<Card> Items;

        public ItemsPanel(string Position, int CardsInHand, int CardsInDeck, string Archetype)
        {
            InitializeComponent();

            this.Position = Position;
            this.Archetype = Archetype;

            Items = new List<Card>();

            ItemsPanelLeft.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_items_left");
            ItemsPanelMiddle.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_items_middle");
            ItemsPanelRight.BackgroundImage = ResourcesLib.GetImageFromGamescreen("playerbase_cavity_" + Position + "_items_right");

            if (Position.Equals("top"))
            {
                ItemsPanelRight.Location = new Point(ItemsPanelRight.Location.X, 0);
                ItemsPanelMiddle.Location = new Point(ItemsPanelMiddle.Location.X, 0);
                ItemsPanelLeft.Location = new Point(ItemsPanelLeft.Location.X, 0);

                usernameLabel.Location = new Point(usernameLabel.Location.X, usernameLabel.Location.Y + 90);

                cardsInDeckLabel.Location = new Point(cardsInDeckLabel.Location.X, cardsInDeckLabel.Location.Y - 55);
                cardsInDicardPileLabel.Location = new Point(cardsInDicardPileLabel.Location.X, cardsInDicardPileLabel.Location.Y - 55);
                cardsInHandLabel.Location = new Point(cardsInHandLabel.Location.X, cardsInHandLabel.Location.Y - 55);
            }

            // Set the card info
            cardsInHandLabel.Text = CardsInHand.ToString();
            cardsInDeckLabel.Text = CardsInDeck.ToString();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Image BackgroundImageMain = ResourcesLib.GetImageFromGamescreen("playerbase_" + Archetype + "_" + (Position.Equals("bottom") ? "l" : "u") + "l");
            base.OnPaintBackground(e);
            Rectangle rc = new Rectangle(0, Position.Equals("bottom") ? -35 : 0, (int)(BackgroundImageMain.Width * .75), (int)(BackgroundImageMain.Height * .75));
            e.Graphics.DrawImage(BackgroundImageMain, rc);
            BackgroundImageLayout = ImageLayout.Zoom;
        }

        public void UpdateCardStats(int HandQty, int DeckQty, int DiscardedQty)
        {
            cardsInHandLabel.Text = HandQty.ToString();
            cardsInDeckLabel.Text = DeckQty.ToString();
            cardsInDicardPileLabel.Text = DiscardedQty.ToString();
        }

        public void PlaceItem(PlaymatCard PlaymatCard)
        {
            CardData CardData = TCGData.Cards.GetCardData(PlaymatCard.GetCardId());
            Card ItemCard = new Card(CardData, PlaymatCard, Card.SIZE.TOOLBAR);
            ItemCard.Location = new Point(ItemsPanelRight.Location.X * (Items.Count + 1), ItemsPanelRight.Location.Y);
            Controls.Add(ItemCard);

            Items.Add(ItemCard);
        }

        public List<Card> GetItems()
        {
            return Items;
        }

        public void SetUsername(string Username)
        {
            usernameLabel.Text = Username;
        }

        public string GetUsername()
        {
            return usernameLabel.Text;
        }

        private void ItemClick()
        {
            Forms.Screens.Game Game = (Forms.Screens.Game)Parent.Parent;

            Card SelectedCard = Game.GetSelectedCard();

            if (SelectedCard != null && SelectedCard.GetCardData().GetType().Equals("Item"))
            {
                // Let server know player wants to place card
                int CardId = SelectedCard.GetCardData().GetId();

                // Construct the card placement packet
                CommandPacket PlacePacket = new CommandPacket(new object[] { CardId });

                PacketService.SendPacket(Commands.PLACE_CARD, PlacePacket);
            }
        }

        private void ItemsPanelRight_Click(object sender, System.EventArgs e)
        {
            ItemClick();
        }

        private void ItemsPanelMiddle_Click(object sender, System.EventArgs e)
        {
            ItemClick();
        }

        private void ItemsPanelLeft_Click(object sender, System.EventArgs e)
        {
            ItemClick();
        }
    }
}
