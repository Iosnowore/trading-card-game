﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms.dialogs;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;

namespace TradingCardGame.Components
{
    public partial class MapButtonSmall : UserControl
    {
        private readonly byte buttonType;

        public MapButtonSmall(byte buttonType)
        {
            InitializeComponent();
            this.buttonType = buttonType;
            LoadButtonData();
        }

        private Image GetMapScreenImage(string image)
        {
            return ResourcesLib.GetImageFromMapScreen(image);
        }

        private void LoadButtonData()
        {
            BackgroundImage = GetMapScreenImage("button_" + BACKGROUNDS[buttonType]);
            TitleLabel.Text = TITLES[buttonType];
            DescriptionLabel.Text = DESCRIPTIONS[buttonType];
            Location = new Point(X_POINTS[buttonType], 8);
        }

        private void MapButtonSmall_Load(object sender, System.EventArgs e)
        {
            BringToFront();
        }

        private static readonly string[] BACKGROUNDS = {
            "preferences",
            "collection",
            "deckbuilder"
        };

        private static readonly string[] TITLES = {
            "Preferences",
            "Collection",
            "Deck Builder"
        };

        private static readonly string[] DESCRIPTIONS = {
            string.Empty,
            "Open packs you've purchased\nand view or sort all the cards\nyou own.",
            "Create new decks and\nimprove your old ones using\nthe cards you've collected."
        };

        private static readonly int[] X_POINTS = {
            35,
            290,
            545
        };

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void MapButtonSmall_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundImage = GetMapScreenImage("button_" + BACKGROUNDS[buttonType] + "_down");
        }

        private void MapButtonSmall_MouseEnter(object sender, System.EventArgs e)
        {
            BackgroundImage = GetMapScreenImage("button_" + BACKGROUNDS[buttonType] + "_over");
        }

        private void MapButtonSmall_MouseLeave(object sender, System.EventArgs e)
        {
            if (!ClientRectangle.Contains(PointToClient(MousePosition)))
                BackgroundImage = GetMapScreenImage("button_" + BACKGROUNDS[buttonType]);
        }

        private new void Click()
        {
            switch (buttonType)
            {
                case 0:
                    Parent.Parent.Controls.Add(new Window(new Preferences(), "Preferences"));
                    break;
                case 1:
                    ((TCG)Parent.Parent.Parent).SetChildWindow(new CollectionManager());
                    break;
                case 2:
                    ((TCG)Parent.Parent.Parent).SetChildWindow(new DeckBuilder());
                    break;
            }
            SoundService.PlaySound("button");
        }

        private void MapButtonSmall_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void TitleLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }

        private void DescriptionLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Click();
        }
    }
}
