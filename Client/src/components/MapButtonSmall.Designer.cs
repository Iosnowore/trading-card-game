﻿namespace TradingCardGame.Components
{
    partial class MapButtonSmall
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.BackColor = System.Drawing.Color.Transparent;
            this.DescriptionLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.DescriptionLabel.Location = new System.Drawing.Point(86, 28);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(168, 45);
            this.DescriptionLabel.TabIndex = 12;
            this.DescriptionLabel.Text = "Open packs you\'ve purchased\r\nand view or sort all the cards\r\nyou own.";
            this.DescriptionLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DescriptionLabel_MouseClick);
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.TitleLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TitleLabel.Location = new System.Drawing.Point(86, 2);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(70, 19);
            this.TitleLabel.TabIndex = 11;
            this.TitleLabel.Text = "Collection";
            this.TitleLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TitleLabel_MouseClick);
            // 
            // MapButtonSmall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.TitleLabel);
            this.DoubleBuffered = true;
            this.Name = "MapButtonSmall";
            this.Size = new System.Drawing.Size(259, 88);
            this.Load += new System.EventHandler(this.MapButtonSmall_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MapButtonSmall_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MapButtonSmall_MouseDown);
            this.MouseEnter += new System.EventHandler(this.MapButtonSmall_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.MapButtonSmall_MouseLeave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Label TitleLabel;
    }
}
