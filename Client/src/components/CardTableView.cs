﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;

namespace TradingCardGame.Src.Components
{
    public class CardTableView : ListView
    {

        private readonly ColumnHeader CardTitle;
        private readonly ColumnHeader CardTrait;
        private readonly ColumnHeader CardCost;
        private readonly ColumnHeader CardArchetype;
        private readonly ColumnHeader CardCollectorInfo;
        private readonly ColumnHeader CardAttack;
        private readonly ColumnHeader CardDefense;
        private readonly ColumnHeader CardBonusDamage;
        private readonly ColumnHeader CardHealth;
        private readonly ColumnHeader CardLevel;

        private readonly ListViewColumnSorter ColumnSorter;

        public CardTableView()
        {
            CardTitle = new ColumnHeader();
            CardTitle.Text = "Title";

            CardTrait = new ColumnHeader();
            CardTrait.Text = "Trait";

            CardCost = new ColumnHeader();
            CardCost.Text = "Cost";

            CardArchetype = new ColumnHeader();
            CardArchetype.Text = "Archetype";

            CardCollectorInfo = new ColumnHeader();
            CardCollectorInfo.Text = "Collector Info";

            CardAttack = new ColumnHeader();
            CardAttack.Text = "Attack";

            CardDefense = new ColumnHeader();
            CardDefense.Text = "Defense";

            CardBonusDamage = new ColumnHeader();
            CardBonusDamage.Text = "Bonus Damage";

            CardHealth = new ColumnHeader();
            CardHealth.Text = "Health";

            CardLevel = new ColumnHeader();
            CardLevel.Text = "Level";

            // Set the sorter object for the listview columns
            ColumnSorter = new ListViewColumnSorter();
            ListViewItemSorter = ColumnSorter;

            AllowColumnReorder = true;
            BackColor = System.Drawing.Color.Black;
            Columns.AddRange(new ColumnHeader[] {
            CardTitle,
            CardTrait,
            CardCost,
            CardArchetype,
            CardCollectorInfo,
            CardAttack,
            CardDefense,
            CardBonusDamage,
            CardHealth,
            CardLevel});
            Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            ForeColor = System.Drawing.Color.LightSkyBlue;
            HideSelection = false;
            Location = new System.Drawing.Point(20, 15);
            Name = "ListView";
            Size = new System.Drawing.Size(950, 660);
            TabIndex = 24;
            UseCompatibleStateImageBehavior = false;
            View = View.Details;
            Visible = false;
            ColumnClick += new ColumnClickEventHandler(CardTableView_ColumnClick);
        }

        private void CardTableView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == ColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (ColumnSorter.Order == SortOrder.Ascending)
                {
                    ColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    ColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                ColumnSorter.SortColumn = e.Column;
                ColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            Sort();
        }
    }
}
