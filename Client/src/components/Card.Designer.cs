﻿namespace TradingCardGame.Src.Components
{
    partial class Card
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoreButton = new System.Windows.Forms.Button();
            this.TraitsLabel = new System.Windows.Forms.Label();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.CostLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.RestrictionsLabel = new System.Windows.Forms.Label();
            this.CollectorInfoLabel = new System.Windows.Forms.Label();
            this.QuantityLabel = new TradingCardGame.Src.Custom.OutlinedTextLabel();
            this.SuspendLayout();
            // 
            // LoreButton
            // 
            this.LoreButton.BackColor = System.Drawing.Color.Transparent;
            this.LoreButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LoreButton.FlatAppearance.BorderSize = 0;
            this.LoreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.LoreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoreButton.ForeColor = System.Drawing.Color.Transparent;
            this.LoreButton.Location = new System.Drawing.Point(122, 196);
            this.LoreButton.Name = "LoreButton";
            this.LoreButton.Size = new System.Drawing.Size(47, 47);
            this.LoreButton.TabIndex = 18;
            this.LoreButton.UseVisualStyleBackColor = false;
            this.LoreButton.Visible = false;
            this.LoreButton.Click += new System.EventHandler(this.LoreButton_Click);
            // 
            // TraitsLabel
            // 
            this.TraitsLabel.AutoSize = true;
            this.TraitsLabel.BackColor = System.Drawing.Color.Transparent;
            this.TraitsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TraitsLabel.Location = new System.Drawing.Point(67, 26);
            this.TraitsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.TraitsLabel.Name = "TraitsLabel";
            this.TraitsLabel.Size = new System.Drawing.Size(61, 12);
            this.TraitsLabel.TabIndex = 17;
            this.TraitsLabel.Text = "Card Traits";
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.Location = new System.Drawing.Point(71, 7);
            this.TitleLabel.Margin = new System.Windows.Forms.Padding(0);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(54, 12);
            this.TitleLabel.TabIndex = 14;
            this.TitleLabel.Text = "Card Title";
            // 
            // CostLabel
            // 
            this.CostLabel.AutoSize = true;
            this.CostLabel.BackColor = System.Drawing.Color.Transparent;
            this.CostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostLabel.Location = new System.Drawing.Point(6, 9);
            this.CostLabel.Name = "CostLabel";
            this.CostLabel.Size = new System.Drawing.Size(69, 15);
            this.CostLabel.TabIndex = 16;
            this.CostLabel.Text = "Card Cost";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.BackColor = System.Drawing.Color.Transparent;
            this.DescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.Location = new System.Drawing.Point(54, 53);
            this.DescriptionLabel.MaximumSize = new System.Drawing.Size(160, 0);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(74, 12);
            this.DescriptionLabel.TabIndex = 15;
            this.DescriptionLabel.Text = "Card Description";
            // 
            // RestrictionsLabel
            // 
            this.RestrictionsLabel.AutoSize = true;
            this.RestrictionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.RestrictionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestrictionsLabel.Location = new System.Drawing.Point(97, 38);
            this.RestrictionsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.RestrictionsLabel.Name = "RestrictionsLabel";
            this.RestrictionsLabel.Size = new System.Drawing.Size(67, 12);
            this.RestrictionsLabel.TabIndex = 20;
            this.RestrictionsLabel.Text = "Restrictions";
            // 
            // CollectorInfoLabel
            // 
            this.CollectorInfoLabel.AutoSize = true;
            this.CollectorInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.CollectorInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CollectorInfoLabel.Location = new System.Drawing.Point(21, 214);
            this.CollectorInfoLabel.Margin = new System.Windows.Forms.Padding(0);
            this.CollectorInfoLabel.Name = "CollectorInfoLabel";
            this.CollectorInfoLabel.Size = new System.Drawing.Size(31, 9);
            this.CollectorInfoLabel.TabIndex = 21;
            this.CollectorInfoLabel.Text = "0A999";
            // 
            // QuantityLabel
            // 
            this.QuantityLabel.AutoSize = true;
            this.QuantityLabel.BackColor = System.Drawing.Color.Transparent;
            this.QuantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityLabel.ForeColor = System.Drawing.Color.White;
            this.QuantityLabel.Location = new System.Drawing.Point(144, 1);
            this.QuantityLabel.Name = "QuantityLabel";
            this.QuantityLabel.Size = new System.Drawing.Size(27, 29);
            this.QuantityLabel.TabIndex = 19;
            this.QuantityLabel.Text = "0";
            this.QuantityLabel.Visible = false;
            // 
            // Card
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.CollectorInfoLabel);
            this.Controls.Add(this.RestrictionsLabel);
            this.Controls.Add(this.QuantityLabel);
            this.Controls.Add(this.LoreButton);
            this.Controls.Add(this.TraitsLabel);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.CostLabel);
            this.Controls.Add(this.DescriptionLabel);
            this.DoubleBuffered = true;
            this.Name = "Card";
            this.Size = new System.Drawing.Size(175, 245);
            this.Load += new System.EventHandler(this.Card_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Card_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Card_MouseDoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Src.Custom.OutlinedTextLabel QuantityLabel;
        private System.Windows.Forms.Button LoreButton;
        private System.Windows.Forms.Label TraitsLabel;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label CostLabel;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Label RestrictionsLabel;
        private System.Windows.Forms.Label CollectorInfoLabel;
    }
}
