﻿namespace TradingCardGame.Components
{
    partial class DropDownMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListViewBox = new System.Windows.Forms.ListView();
            this.ColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // ListViewBox
            // 
            this.ListViewBox.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.ListViewBox.BackColor = System.Drawing.Color.Cyan;
            this.ListViewBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ListViewBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader});
            this.ListViewBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewBox.FullRowSelect = true;
            this.ListViewBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.ListViewBox.LabelWrap = false;
            this.ListViewBox.Location = new System.Drawing.Point(5, 5);
            this.ListViewBox.Name = "ListViewBox";
            this.ListViewBox.Scrollable = false;
            this.ListViewBox.Size = new System.Drawing.Size(150, 100);
            this.ListViewBox.TabIndex = 0;
            this.ListViewBox.UseCompatibleStateImageBehavior = false;
            this.ListViewBox.View = System.Windows.Forms.View.Details;
            this.ListViewBox.ItemActivate += new System.EventHandler(this.ListView_ItemActivate);
            this.ListViewBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ListView_MouseMove);
            // 
            // DropDownMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.ListViewBox);
            this.DoubleBuffered = true;
            this.Name = "DropDownMenu";
            this.Size = new System.Drawing.Size(158, 108);
            this.Load += new System.EventHandler(this.DropDownMenu_Load);
            this.MouseLeave += new System.EventHandler(this.DropDownMenu_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView ListViewBox;
        private System.Windows.Forms.ColumnHeader ColumnHeader;
    }
}
