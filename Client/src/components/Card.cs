﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Cards;
using Resources;
using System;
using System.Drawing;
using System.Windows.Forms;
using TCGData;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Components.Game;
using TradingCardGame.Src.Forms.Screens;
using TradingCardGame.Src.Services.Font;
using TradingCardGame.Src.Services.Image;

namespace TradingCardGame.Src.Components
{
    public partial class Card : UserControl
    {
        private CardData CardData;
        private PackData PackData;

        private PictureBox RedeemedPictureBox;

        private CardStatPanel AttackPanel;
        private CardStatPanel DefensePanel;
        private CardStatPanel BonusDamagePanel;
        private CardStatPanel HealthOrLevelPanel;

        private PictureBox[] HealthBoxes;

        private readonly PlaymatCard PlaymatCard;

        private EffectPlayer ExertEffect;

        private int index;

        private Label KeywordLabel;

        private PictureBox TextIconAttack;
        private PictureBox TextIconDefense;
        private PictureBox TextIconMain;

        public enum SIZE
        {
            SMALL,
            MEDIUM,
            LARGE,
            PLAYMAT,
            TOOLBAR
        }

        public enum TYPE
        {
            ABILITY,
            AVATAR,
            ITEM,
            LOOT,
            PACK,
            TACTIC,
            UNIT,
            QUEST
        }

        private static readonly Size[] CardSizes = {
            new Size(175, 245),
            new Size(225, 315),
            new Size(325, 454),
            new Size(143, 143),
            new Size(130, 111)
        };

        private static readonly Point[] CostLabelPoints = {
            new Point(10, 8),
            new Point(10, 6),
            new Point(18, 12),
            new Point(10, 5),
            new Point(0, 0)
        };

        private static readonly Point[] DescriptionLabelPoints = {
            new Point(14, 182),
            new Point(20, 232),
            new Point(25, 335),
            new Point(14, 182),
            new Point(0, 0)
        };

        private static readonly Point[] CollectorInfoLabelPoints = {
            new Point(22, 233),
            new Point(30, 300),
            new Point(40, 432),
            new Point(14, 182),
            new Point(0, 0)
        };

        private static readonly Size[] MaximumDescriptionSizes = {
            new Size(160, 0),
            new Size(200, 0),
            new Size(290, 0),
            new Size(160, 0),
            new Size(0, 0)
        };

        private static readonly Point[] TitleLabelPoints = {
            new Point(40, 5),
            new Point(52, 5),
            new Point(75, 7),
            new Point(25, 3),
            new Point(23, 3)
        };

        private static readonly Point[] TraitsLabelPoints = {
            new Point(40, 16),
            new Point(52, 19),
            new Point(75, 28),
            new Point(40, 14),
            new Point(0, 0)
        };

        private static readonly Point[] AttackInfoPoints = {
            new Point(15, 58),
            new Point(17, 65),
            new Point(20, 115),
            new Point(15, 25),
            new Point(5, 25)
        };

        private static readonly Size[] LoreSize = {
            new Size(24, 24),
            new Size(36, 36),
            new Size(47, 47),
            new Size(24, 24),
            new Size(0, 0)
        };

        private static readonly Size[] RedeemedSize = {
            new Size(134, 92),
            new Size(134, 92),
            new Size(201, 138),
            new Size(134, 92),
            new Size(0, 0)
        };

        private static readonly int[] DescriptionSizes = {
            8,
            9,
            12,
            8,
            8
        };

        private readonly SIZE size;

        public Card(CardData CardData, PlaymatCard PlaymatCard, SIZE Size)
        {
            InitializeComponent();
            size = Size;
            this.CardData = CardData;
            this.PlaymatCard = PlaymatCard;
            SetComponents(true);
        }

        public Card(CardData CardData, SIZE size, bool GameScene = false)
        {
            InitializeComponent();
            this.size = size;
            this.CardData = CardData;
            SetComponents(GameScene);
        }

        public Card(CardData CardData, SIZE size)
        {
            InitializeComponent();
            this.size = size;
            this.CardData = CardData;
            SetComponents();
        }

        public Card(PackData PackData, SIZE size)
        {
            InitializeComponent();
            this.size = size;
            this.PackData = PackData;
            SetComponents();
        }

        private void SetComponents(bool GameScene = false)
        {
            switch (size)
            {
                case SIZE.SMALL:
                    CustomFontService.SetCustomFont(TitleLabel, FontStyle.Bold, 7);
                    CustomFontService.SetCustomFont(CostLabel, FontStyle.Bold);
                    CustomFontService.SetCustomFont(TraitsLabel, FontStyle.Bold);
                    CustomFontService.SetCustomFont(RestrictionsLabel, FontStyle.Bold);
                    CustomFontService.SetCustomFont(CollectorInfoLabel, FontStyle.Bold);
                    break;
                case SIZE.MEDIUM:
                    index = 1;
                    CustomFontService.SetCustomFont(TitleLabel, FontStyle.Bold, 10);
                    CustomFontService.SetCustomFont(CostLabel, FontStyle.Bold, 16);
                    CustomFontService.SetCustomFont(TraitsLabel, FontStyle.Bold, 10);
                    CustomFontService.SetCustomFont(RestrictionsLabel, FontStyle.Bold, 10);
                    CustomFontService.SetCustomFont(CollectorInfoLabel, FontStyle.Bold, 8);
                    break;
                case SIZE.LARGE:
                    index = 2;
                    CustomFontService.SetCustomFont(TitleLabel, FontStyle.Bold, 13);
                    CustomFontService.SetCustomFont(CostLabel, FontStyle.Bold, 20);
                    CustomFontService.SetCustomFont(TraitsLabel, FontStyle.Bold, 13);
                    CustomFontService.SetCustomFont(RestrictionsLabel, FontStyle.Bold, 13);
                    CustomFontService.SetCustomFont(CollectorInfoLabel, FontStyle.Bold, 11);
                    break;
                case SIZE.PLAYMAT:
                    index = 3;
                    CustomFontService.SetCustomFont(TitleLabel, FontStyle.Regular, 10);
                    break;
                case SIZE.TOOLBAR:
                    index = 4;
                    CustomFontService.SetCustomFont(TitleLabel, FontStyle.Regular, 9);
                    break;
            }

            CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Regular, DescriptionSizes[index]);

            if (CardData != null)
            {
                TitleLabel.Text = CardData.GetTitle();

                string archetype = CardData.GetArchetype();
                LoadAttackInfo(archetype, index);
                LoadDefenseInfo(archetype, index);
                LoadBonusDamageInfo(archetype, index);
                LoadHealthOrLevelInfo(archetype, index);
                base.BackColor = Color.Black;
                DescriptionLabel.MaximumSize = MaximumDescriptionSizes[index];
                Size = CardSizes[index];

                Image Image = CardsLib.GetImageFromCards(CardData.GetImage());

                if (size != SIZE.PLAYMAT && size != SIZE.TOOLBAR)
                {
                    int X = (int)(Width * 0.05f);
                    int SideLength = Width - X;

                    // If null, it must be in Uncompressed
                    if (Image == null)
                        Image = CardsLib.GetImageFromUncompressed(CardData.GetImage());

                    BackgroundImage = ImageService.ResizeImage(Image, X, (int)(Width * 0.15f), SideLength, SideLength);
                }
                else
                {
                    int X = (int)(Width * 0.02f);
                    int SideLength = Width - X;

                    BackgroundImage = ImageService.ResizeImage(Image, X, (int)(Width * 0.12f), SideLength, SideLength);
                    BackColor = Color.Transparent;
                }
                BackgroundImageLayout = ImageLayout.None;

                switch (archetype)
                {
                    case "imperial":
                    case "sith":
                    case "raid":
                        ChangeTextColor(Color.White);
                        break;
                    default:
                        ChangeTextColor(Color.Black);
                        break;
                }

                CostLabel.Text = CardData.GetCost().ToString();
                CostLabel.Visible = CardData.GetCost() != 0;
                DescriptionLabel.Text = CardData.GetDescription();
                TraitsLabel.Text = CardData.GetType();
                RestrictionsLabel.Text = CardData.GetRestriction();
                CollectorInfoLabel.Text = CardData.GetCollectorInfo();

                if (!string.IsNullOrEmpty(CardData.GetTraits()))
                    TraitsLabel.Text += ", " + CardData.GetTraits();

                TitleLabel.Location = TitleLabelPoints[index];
                CostLabel.Location = CostLabelPoints[index];
                TraitsLabel.Location = TraitsLabelPoints[index];
                CollectorInfoLabel.Location = CollectorInfoLabelPoints[index];
                RestrictionsLabel.Location = new Point(Width - RestrictionsLabel.Width - 10, TraitsLabel.Location.Y);

                // Remove any existing keyword label
                if (KeywordLabel != null)
                {
                    KeywordLabel.Dispose();
                    KeywordLabel = null;
                }

                // Setup new keyword label if needed
                if (!string.IsNullOrEmpty(CardData.GetKeyword()))
                {
                    KeywordLabel = new Label();
                    KeywordLabel.AutoSize = true;
                    KeywordLabel.Text = CardData.GetKeyword();
                    KeywordLabel.BackColor = Color.Transparent;
                    KeywordLabel.ForeColor = Color.Blue;
                    CustomFontService.SetCustomFont(KeywordLabel, FontStyle.Underline, DescriptionSizes[index] + 2);
                    Controls.Add(KeywordLabel);
                    KeywordLabel.BringToFront();
                    KeywordLabel.Location = DescriptionLabelPoints[index];

                    DescriptionLabel.Location = new Point(DescriptionLabelPoints[index].X, DescriptionLabelPoints[index].Y + KeywordLabel.Height);
                }
                else
                {
                    DescriptionLabel.Location = DescriptionLabelPoints[index];
                }

                // Remove any old text icons
                if (TextIconAttack != null)
                {
                    TextIconAttack.Dispose();
                    TextIconAttack = null;
                }
                if (TextIconDefense != null)
                {
                    TextIconDefense.Dispose();
                    TextIconDefense = null;
                }
                if (TextIconMain != null)
                {
                    TextIconMain.Dispose();
                    TextIconMain = null;
                }

                // Add any text icons if needed
                if (DescriptionLabel.Text.Contains("[A]"))
                {
                    TextIconAttack = new PictureBox();
                    TextIconAttack.Size = new Size(15, 15);
                    TextIconAttack.Image = CardsLib.GetImageFromCards("text_icon_attack");
                    TextIconAttack.Location = DescriptionLabelPoints[index];
                    TextIconAttack.BackColor = Color.Transparent;
                    Controls.Add(TextIconAttack);
                    TextIconAttack.BringToFront();

                    DescriptionLabel.Text = DescriptionLabel.Text.Replace("[A]", ".\t\t\t\t.");
                }
                if (DescriptionLabel.Text.Contains("[D]"))
                {
                    TextIconDefense = new PictureBox();
                    TextIconDefense.Size = new Size(15, 15);
                    TextIconDefense.Image = CardsLib.GetImageFromCards("text_icon_defense");
                    TextIconDefense.Location = DescriptionLabelPoints[index];
                    TextIconDefense.BackColor = Color.Transparent;
                    Controls.Add(TextIconDefense);
                    TextIconDefense.BringToFront();

                    DescriptionLabel.Text = DescriptionLabel.Text.Replace("[D]", ".\t\t\t\t.");
                }
                if (DescriptionLabel.Text.Contains("[M]"))
                {
                    TextIconMain = new PictureBox();
                    TextIconMain.Size = new Size(15, 15);
                    TextIconMain.Image = CardsLib.GetImageFromCards("text_icon_main_" + (CardData.GetArchetype().Equals("jedi") || CardData.GetArchetype().Equals("rebel") || CardData.GetArchetype().Equals("generic") ? "black" : "white"));
                    TextIconMain.Location = DescriptionLabelPoints[index];
                    TextIconMain.BackColor = Color.Transparent;
                    Controls.Add(TextIconMain);
                    TextIconMain.BringToFront();

                    DescriptionLabel.Text = DescriptionLabel.Text.Replace("[M]", ".\t\t\t\t.");
                }

                // Only create a lore button if there is text to switch between
                if (!string.IsNullOrEmpty(DescriptionLabel.Text))
                {
                    LoreButton.BackgroundImage = CardsLib.GetImageFromCards("lore_button_off");
                    LoreButton.Size = LoreSize[index];
                    LoreButton.Location = new Point(Width - LoreButton.Width, Height - LoreButton.Height);
                }
                else
                {
                    // Show lore if no description available
                    ShowLore();
                }

                if (!GameScene)
                {
                    LoreButton.Visible = !string.IsNullOrEmpty(CardData.GetLore()) && !string.IsNullOrEmpty(CardData.GetDescription());
                }

                SetVisibility(true);

                if (size == SIZE.PLAYMAT || size == SIZE.TOOLBAR)
                {
                    CostLabel.Visible = false;
                    LoreButton.Visible = false;
                    TraitsLabel.Visible = false;
                    CollectorInfoLabel.Visible = false;
                    DescriptionLabel.Visible = false;
                }
                CheckOverflowingText();
            }
            else if (PackData != null)
            {
                // Clear the CardData
                CardData = null;

                BackgroundImage = CardsLib.GetImageFromUncompressed(PackData.GetImage());
                BackgroundImageLayout = ImageLayout.Zoom;
                base.BackColor = Color.Transparent;
                LoadAttackInfo(null, index);
                LoadDefenseInfo(null, index);
                LoadBonusDamageInfo(null, index);
                LoadHealthOrLevelInfo(null, index);
                CostLabel.Visible = false;
                Size = CardSizes[index];
                LoreButton.Visible = false;
                SetVisibility(false);
            }
        }

        private void CheckOverflowingText()
        {
            // Make sure the TitleLabel font size is not too big and fits correctly
            int TitleWidth = TitleLabel.Width + TitleLabel.Location.X + 10;
            while (Width < TitleWidth)
            {
                CustomFontService.SetCustomFont(TitleLabel, TitleLabel.Font.Style, TitleLabel.Font.Size - 1);

                // Since we are reducing the size, make sure to move the title down
                TitleLabel.Location = new Point(TitleLabel.Location.X, TitleLabel.Location.Y + 1);

                // Get the new width
                TitleWidth = TitleLabel.Width + TitleLabel.Location.X;
            }

            if (DescriptionLabel.Visible)
            {
                // Make sure the DescriptionLabel text isn't too long and goes into the collector info label
                int DescriptionEnd = DescriptionLabel.Location.Y + DescriptionLabel.Height;
                while (CollectorInfoLabel.Location.Y < DescriptionEnd)
                {
                    CustomFontService.SetCustomFont(DescriptionLabel, DescriptionLabel.Font.Style, DescriptionLabel.Font.Size - 1);

                    // Get the new end point
                    DescriptionEnd = DescriptionLabel.Location.Y + DescriptionLabel.Height;
                }
            }
        }

        private string GetBackgroundImageNameString(string Archetype, string Type)
        {
            if (size == SIZE.PLAYMAT || size == SIZE.TOOLBAR)
            {
                switch (Type)
                {
                    case "Unit":
                        return "playmatcard_tall_" + Archetype;
                    case "Ability":
                        return "playmatcard_short_" + Archetype;
                    case "Item":
                        return "playmatcard_short_both_" + Archetype;
                    case "Quest":
                        return "playmatcard_quest_" + Archetype;

                }
            }
            return "template_" + Archetype;
        }

        private void LoadAttackInfo(string archetype, int index)
        {
            if (AttackPanel != null)
            {
                AttackPanel.Dispose();
                AttackPanel = null;
            }

            if (CardData != null && !string.IsNullOrEmpty(CardData.GetAttack().ToString()) && !CardData.GetAttack().ToString().Equals("0"))
            {
                string BackgroundImageString = "icon_" + (archetype.Equals("raid") ? "underworld" : archetype) + "_attack";

                int Attack = PlaymatCard == null ? CardData.GetAttack() : PlaymatCard.GetAttack();

                if (Attack > 9)
                    BackgroundImageString += "_longer";

                AttackPanel = new CardStatPanel(Attack, index)
                {
                    BackgroundImage = CardsLib.GetImageFromCards(BackgroundImageString),
                    Location = AttackInfoPoints[index]
                };
                Controls.Add(AttackPanel);
            }
        }

        private void LoadDefenseInfo(string archetype, int index)
        {
            if (DefensePanel != null)
            {
                DefensePanel.Dispose();
                DefensePanel = null;
            }

            if (CardData != null && !string.IsNullOrEmpty(CardData.GetDefense().ToString()) && !CardData.GetDefense().ToString().Equals("0"))
            {
                string BackgroundImageString = "icon_" + (archetype.Equals("raid") ? "underworld" : archetype) + "_defense";

                int Defense = PlaymatCard == null ? CardData.GetDefense() : PlaymatCard.GetDefense();

                if (Defense > 9)
                    BackgroundImageString += "_longer";

                int Index = 1;

                if (size == SIZE.TOOLBAR)
                {
                    Index = AttackPanel == null ? 0 : 1;
                }

                DefensePanel = new CardStatPanel(Defense, index)
                {
                    BackgroundImage = CardsLib.GetImageFromCards(BackgroundImageString)
                };
                DefensePanel.Location = new Point(AttackInfoPoints[index].X, AttackInfoPoints[index].Y + DefensePanel.Height * Index + 2);
                Controls.Add(DefensePanel);
            }
        }

        private void LoadBonusDamageInfo(string archetype, int index)
        {
            if (BonusDamagePanel != null)
            {
                BonusDamagePanel.Dispose();
                BonusDamagePanel = null;
            }

            if (CardData != null && !string.IsNullOrEmpty(CardData.GetBonus().ToString()) && !CardData.GetBonus().ToString().Equals("0"))
            {
                string BackgroundImageString = "icon_" + (archetype.Equals("raid") ? "underworld" : archetype) + "_damage";

                int Damage = PlaymatCard == null ? CardData.GetBonus() : PlaymatCard.GetDamage();

                if (Damage > 9)
                    BackgroundImageString += "_longer";

                int Index = 2;

                if (size == SIZE.TOOLBAR)
                {
                    Index = AttackPanel == null && DefensePanel == null ? 0 : 1;
                }

                BonusDamagePanel = new CardStatPanel(Damage, index)
                {
                    BackgroundImage = CardsLib.GetImageFromCards(BackgroundImageString)
                };
                BonusDamagePanel.Location = new Point(AttackInfoPoints[index].X, AttackInfoPoints[index].Y + BonusDamagePanel.Height * Index + 4);
                Controls.Add(BonusDamagePanel);
            }
        }

        private void LoadHealthOrLevelInfo(string archetype, int index)
        {
            if (HealthOrLevelPanel != null)
            {
                HealthOrLevelPanel.Dispose();
                HealthOrLevelPanel = null;
            }

            // No need to continue if there is no CardData
            if (CardData == null)
                return;

            int Amount = CardData.GetHealthOrLevel();

            // If the amount is not greater than 0, we do not care.
            if (Amount > 0)
            {
                bool UsingHealth = CardData.GetType().Equals("Avatar") || CardData.GetType().Equals("Unit");
                bool UsingLevel = CardData.GetType().Equals("Ability") || CardData.GetType().Equals("Quest");

                if (UsingHealth)
                {
                    if (index == 3)
                    {
                        if (HealthBoxes != null)
                            for (int i = 0; i < HealthBoxes.Length; i++)
                                HealthBoxes[i].Dispose();

                        int Health = PlaymatCard == null ? Amount : PlaymatCard.GetHealth();
                        HealthBoxes = new PictureBox[Health];
                        int HealthBarHeight = 100 / HealthBoxes.Length;

                        for (int i = 0; i < HealthBoxes.Length; i++)
                        {
                            HealthBoxes[i] = new PictureBox
                            {
                                BackColor = Color.Red,
                                BorderStyle = BorderStyle.FixedSingle,
                                Location = new Point(0, 25 + (i * HealthBarHeight)),
                                Size = new Size(15, HealthBarHeight)
                            };
                            Controls.Add(HealthBoxes[i]);
                        }
                    }
                    else
                    {
                        string BackgroundImageString = "icon_" + (archetype.Equals("raid") ? "underworld" : archetype) + "_health";

                        if (Amount > 9)
                            BackgroundImageString += "_longer";

                        HealthOrLevelPanel = new CardStatPanel(Amount, index)
                        {
                            BackgroundImage = CardsLib.GetImageFromCards(BackgroundImageString)
                        };
                    }
                }
                else if (UsingLevel)
                {
                    string BackgroundImageString = "icon_" + (archetype.Equals("raid") ? "underworld" : archetype) + "_level";

                    if (Amount > 9)
                        BackgroundImageString += "_longer";

                    HealthOrLevelPanel = new CardStatPanel(Amount, index);

                    if (index != 3 || !CardData.GetType().Equals("Quest"))
                    {
                        HealthOrLevelPanel.BackgroundImage = CardsLib.GetImageFromCards(BackgroundImageString);
                    }
                }

                if (HealthOrLevelPanel != null)
                {
                    if (index == 3)
                    {
                        HealthOrLevelPanel.Location = new Point(0, 110);
                    }
                    else
                    {
                        int Index = size == SIZE.TOOLBAR ? 1 : 3;

                        HealthOrLevelPanel.Location = new Point(AttackInfoPoints[index].X, AttackInfoPoints[index].Y + HealthOrLevelPanel.Height * Index + 6);
                    }
                    Controls.Add(HealthOrLevelPanel);
                }
            }
        }

        private void SetVisibility(bool visibility)
        {
            DescriptionLabel.Visible = visibility;
            TitleLabel.Visible = visibility;
            TraitsLabel.Visible = visibility;
            CollectorInfoLabel.Visible = visibility;
            RestrictionsLabel.Visible = visibility;
        }

        private void ChangeTextColor(Color color)
        {
            CostLabel.ForeColor = color;
            DescriptionLabel.ForeColor = color;
            TitleLabel.ForeColor = color;
            TraitsLabel.ForeColor = color;
            RestrictionsLabel.ForeColor = color;
        }

        private void Card_Load(object sender, EventArgs e)
        {
            BringToFront();
        }

        public string GetTitle()
        {
            return TitleLabel.Text;
        }

        public void ChangeCard(CardData CardData)
        {
            this.CardData = CardData;
            PackData = null;
            SetComponents();
        }

        public void ChangeCard(PackData PackData)
        {
            this.PackData = PackData;
            CardData = null;
            SetComponents();
        }

        public int GetId()
        {
            return CardData != null ? CardData.GetId() : PackData.GetId();
        }

        public CardData GetCardData()
        {
            return CardData;
        }

        public PackData GetPackData()
        {
            return PackData;
        }

        public void SetQuantityLabel(int Quantity)
        {
            CustomFontService.SetCustomFont(QuantityLabel, FontStyle.Regular);

            QuantityLabel.Text = Quantity.ToString();
            QuantityLabel.Location = new Point(Width - QuantityLabel.Width, 0);
            QuantityLabel.Visible = true;
        }

        public int GetQuantity()
        {
            return int.Parse(QuantityLabel.Text);
        }

        private void LoreButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("button");

            if (!IsShowingLore())
            {
                ShowLore();
            }
            else
            {
                DescriptionLabel.Text = CardData.GetDescription();
                LoreButton.BackgroundImage = CardsLib.GetImageFromCards("lore_button_off");

                switch (size)
                {
                    case SIZE.SMALL:
                        CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Regular, DescriptionSizes[index]);
                        break;
                    case SIZE.MEDIUM:
                        CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Regular, DescriptionSizes[index]);
                        break;
                    case SIZE.LARGE:
                        CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Regular, DescriptionSizes[index]);
                        break;
                }
            }

            // Fix any overflowing text issues
            CheckOverflowingText();
        }

        private void HandleRightClick(object sender, MouseEventArgs e)
        {
            if (Parent.Parent is DeckBuilder DeckBuilderTopPanel)
                DeckBuilderTopPanel.ChangeCardLocation(this);
            else if (Parent.Parent.Parent is DeckBuilder DeckBuilderBottomPanel)
                DeckBuilderBottomPanel.ChangeCardLocation(this);
            else if (Parent.Parent.Parent is Trade Trade)
                Trade.ChangeCardLocation(this);
            else if (Parent.Parent.Parent is CollectionManager CollectionManager)
                CollectionManager.OpenPackOrClaimLoot(this);
            else if (Parent is Forms.Screens.Game Game)
                Game.CardRightClicked(sender, e);
            else if (Parent.Parent.Parent is Forms.Screens.Game Game1)
                Game1.CardRightClicked(sender, e);

        }

        private void HandleDoubleClick(object sender, MouseEventArgs e)
        {
            if (Parent is Forms.Screens.Game Game)
                Game.CardDoubleClicked(sender, e);
            else if (Parent.Parent.Parent is Forms.Screens.Game Game1)
                Game1.CardDoubleClicked(sender, e);
        }

        public bool IsShowingLore()
        {
            return DescriptionLabel.Text.Equals(CardData.GetLore());
        }

        public void ShowLore()
        {
            DescriptionLabel.Text = CardData.GetLore();
            LoreButton.BackgroundImage = CardsLib.GetImageFromCards("lore_button_on");

            switch (size)
            {
                case SIZE.SMALL:
                    CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Italic, DescriptionSizes[index]);
                    break;
                case SIZE.MEDIUM:
                    CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Italic, DescriptionSizes[index]);
                    break;
                case SIZE.LARGE:
                    CustomFontService.SetCustomFont(DescriptionLabel, FontStyle.Italic, DescriptionSizes[index]);
                    break;
            }
        }

        public void SetRedeemed(bool Redeemed)
        {
            if (Redeemed)
            {
                Size Size;

                switch (size)
                {
                    case SIZE.SMALL:
                        Size = RedeemedSize[0];
                        break;
                    case SIZE.MEDIUM:
                        Size = RedeemedSize[1];
                        break;
                    case SIZE.LARGE:
                    default:
                        Size = RedeemedSize[2];
                        break;
                }

                RedeemedPictureBox = new PictureBox
                {
                    Size = Size,
                    Image = ResourcesLib.GetImageFromIcons("loot_redemption_stamp"),
                    Location = new Point((Width - Size.Width) / 2, (Height - Size.Height) / 2),
                    BackColor = Color.Transparent,
                    SizeMode = PictureBoxSizeMode.StretchImage
                };

                RedeemedPictureBox.MouseHover += RedeemedPictureBox_MouseHover;
                Controls.Add(RedeemedPictureBox);
            }
            else if (RedeemedPictureBox != null)
            {
                RedeemedPictureBox.Dispose();
                RedeemedPictureBox = null;
            }
        }

        private void RedeemedPictureBox_MouseHover(object sender, EventArgs e)
        {
            CollectionManager CollectionManager = (CollectionManager)Parent.Parent.Parent;
            CollectionManager.CardMouseHover(this);
        }

        public bool GetRedeemed()
        {
            return RedeemedPictureBox != null;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            if (CardData != null)
            {
                Image Image = CardsLib.GetImageFromCards(GetBackgroundImageNameString(CardData.GetArchetype(), CardData.GetType()));

                Rectangle rc = new Rectangle(0, 0, Width, Height);
                e.Graphics.DrawImage(Image, rc);
            }
        }

        private void Card_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                HandleRightClick(sender, e);
        }

        private void Card_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            HandleDoubleClick(sender, e);
        }

        public void PlayExertAnimation(int ExertType)
        {
            string EffectRoot;

            switch (ExertType)
            {
                case Forms.Screens.Game.EXERT_ATTACK:
                    EffectRoot = "exhaust_attack_";
                    SoundService.PlaySound("attack_monster");
                    break;
                case Forms.Screens.Game.EXERT_DAMAGE:
                    EffectRoot = "exhaust_damage_";
                    SoundService.PlaySound("attack_monster");
                    break;
                default:
                    EffectRoot = "exhaust_defend_";
                    SoundService.PlaySound("defend_monster");
                    break;
            }

            ExertEffect = new EffectPlayer(EffectRoot, 40, 2, new Size(Width, Height), false, null, false);
            Controls.Add(ExertEffect);
            ExertEffect.BringToFront();

            ExertEffect.MouseClick += ExertEffect_MouseClick;
            ExertEffect.MouseDoubleClick += ExertEffect_MouseDoubleClick;

            // Play the animation
            ExertEffect.StartAnimation(true);
        }

        private void ExertEffect_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            HandleDoubleClick(this, e);
        }

        private void ExertEffect_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                HandleRightClick(this, e);
        }

        public void UpdateAttack(int AttackValue)
        {
            AttackPanel.Text = AttackValue.ToString();
        }

        public void UpdateDamage(int DamageValue)
        {
            BonusDamagePanel.Text = DamageValue.ToString();
        }

        public void UpdateDefense(int DefenseValue)
        {
            DefensePanel.Text = DefenseValue.ToString();
        }

        public void UpdateHealth(int HealthValue)
        {
            HealthOrLevelPanel.Text = HealthValue.ToString();

            // Show the health box change
            LoadHealthOrLevelInfo(CardData.GetArchetype(), 3);
        }

        public void DamageCard()
        {
            // Remove 1 health from the card
            for (int i = 0; i < HealthBoxes.Length; i++)
            {
                if (HealthBoxes[i].BackColor == Color.Red)
                {
                    HealthBoxes[i].BackColor = Color.Gray;
                    break;
                }
            }

            // Setup animation
            EffectPlayer EffectPlayer = new EffectPlayer("cutter_", 34, 2, new Size(Width, Height), false, null, true);
            Controls.Add(EffectPlayer);
            EffectPlayer.BringToFront();

            // Play the animation
            EffectPlayer.StartAnimation(true);

            // Play sound effect
            SoundService.PlaySound("npc_male_damage");
        }

        public void DestroyCard()
        {
            // Setup animation
            EffectPlayer EffectPlayer = new EffectPlayer("destroyed_", 34, 2, new Size(Width, Height), false, this, true);
            Controls.Add(EffectPlayer);
            EffectPlayer.BringToFront();

            // Play the animation
            EffectPlayer.StartAnimation(true);

            // Play sound effect
            SoundService.PlaySound("npc_male_death");
        }

        public void Ready()
        {
            if (ExertEffect != null)
                ExertEffect.Dispose();
        }

        public PlaymatCard GetPlaymatCard()
        {
            return PlaymatCard;
        }
    }
}
