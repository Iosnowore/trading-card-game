﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Services.Animation;

namespace TradingCardGame.Src.Forms.Dialogs
{
    public partial class Window : UserControl
    {
        private Point Point;
        private readonly UserControl UserControl;

        public Window(UserControl UserControl, string Title)
        {
            InitializeComponent();

            this.UserControl = UserControl;

            CloseButton.BackgroundImage = ResourcesLib.GetImageFromButtons("closebutton");
            TitlePanel.BackgroundImage = ResourcesLib.GetImageFromDialogs("title_base");
            BackgroundImage = ResourcesLib.GetImageFromBorders("navframe_header");
            BackgroundImageLayout = ImageLayout.Stretch;
            TitleLabel.Text = Title;
            TitlePanel.Size = new Size(TitleLabel.Width + 40, TitlePanel.Height);
            Size = new Size(UserControl.Width + 30, UserControl.Height + 80);
            UserControl.Location = new Point((Width - UserControl.Width) / 2, (TitlePanel.Location.Y + TitlePanel.Height) + 10);
            CloseButton.Location = new Point(Width - CloseButton.Width - 20, CloseButton.Location.Y);
            TitleLabel.Location = new Point((TitlePanel.Width - TitleLabel.Width) / 2, TitleLabel.Location.Y);
            Controls.Add(UserControl);
        }

        private void Window_Load(object sender, System.EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            CloseButton.BringToFront();
            TitlePanel.BringToFront();
            BringToFront();
        }

        private void CloseButton_Click(object sender, System.EventArgs e)
        {
            SoundService.PlaySound("button");
            CloseWindow();
        }

        public void CloseWindow()
        {
            if (UserControl is LaunchMatch LaunchMatch)
            {
                LaunchMatch.LeaveMatch();
            }
            TCG.SetDialogStatus(false);
            Dispose();
        }

        private void CloseButton_MouseDown(object sender, MouseEventArgs e)
        {
            ButtonAnimation.MouseDownClose(CloseButton);
        }

        private void CloseButton_MouseEnter(object sender, System.EventArgs e)
        {
            ButtonAnimation.MouseEnterClose(CloseButton);
        }

        private void CloseButton_MouseLeave(object sender, System.EventArgs e)
        {
            ButtonAnimation.MouseLeaveClose(CloseButton);
        }

        private void CloseButton_MouseUp(object sender, MouseEventArgs e)
        {
            ButtonAnimation.MouseEnterClose(CloseButton);
        }

        private void Window_MouseDown(object sender, MouseEventArgs e)
        {
            Point = new Point(e.X, e.Y);
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Location = new Point(e.X - Point.X + Location.X, e.Y - Point.Y + Location.Y);
        }

        public void SetTitle(string Title)
        {
            TitleLabel.Text = Title;
        }
    }
}
