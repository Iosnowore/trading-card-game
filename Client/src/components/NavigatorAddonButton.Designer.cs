﻿namespace TradingCardGame.Components
{
    partial class NavigatorAddonButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.ImagePictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.White;
            this.TitleLabel.Location = new System.Drawing.Point(89, 2);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(44, 16);
            this.TitleLabel.TabIndex = 6;
            this.TitleLabel.Text = "Title 1";
            this.TitleLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TitleLabel_MouseClick);
            // 
            // ImagePictureBox
            // 
            this.ImagePictureBox.Location = new System.Drawing.Point(7, 6);
            this.ImagePictureBox.Name = "ImagePictureBox";
            this.ImagePictureBox.Size = new System.Drawing.Size(76, 76);
            this.ImagePictureBox.TabIndex = 0;
            this.ImagePictureBox.TabStop = false;
            this.ImagePictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ImagePictureBox_MouseClick);
            // 
            // NavigatorAddonButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.ImagePictureBox);
            this.Controls.Add(this.TitleLabel);
            this.DoubleBuffered = true;
            this.Name = "NavigatorAddonButton";
            this.Size = new System.Drawing.Size(284, 88);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NavigatorAddonButton_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NavigatorAddonButton_MouseDown);
            this.MouseEnter += new System.EventHandler(this.NavigatorAddonButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.NavigatorAddonButton_MouseLeave);
            ((System.ComponentModel.ISupportInitialize)(this.ImagePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.PictureBox ImagePictureBox;
    }
}
