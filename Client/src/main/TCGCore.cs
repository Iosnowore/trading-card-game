﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Diagnostics;
using System.Windows.Forms;
using TradingCardGame.Forms.Screens;

namespace TradingCardGame
{
    public class TCGCore
    {
        private static TCG TCG;

        [STAThread]
        private static void Main(string[] args)
        {
            /*
            if (!Debugger.IsAttached && (args.Length != 1 || !args[0].Equals("ok")))
            {
                MessageBox.Show("Please do not run the TCG game itself. Run the launcher and click play.");
                return;
            }*/

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);

            if (!Debugger.IsAttached && TcgIsActive())
            {
                MessageBox.Show("Sorry, but an instance of Trading Card Game is already running.", "SWG TCG Already Running");
                return;
            }

            TCG = new TCG();
            Application.Run(TCG);
        }

        private static bool TcgIsActive()
        {
            return Process.GetProcessesByName("SWGTCG").Length > 1;
        }

        public static TCG GetTCG()
        {
            return TCG;
        }
    }
}
