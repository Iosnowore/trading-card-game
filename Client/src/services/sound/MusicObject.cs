﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using NAudio.Vorbis;
using NAudio.Wave;
using Sounds;
using System;

namespace TradingCardGame.Src.Services.Sound
{
    public class MusicObject
    {
        private VolumeWaveProvider16 Provider;
        private readonly WaveOut WaveOut;

        private int Index;

        private readonly int MaxIndex;
        private readonly string MusicString;

        private bool FadingOut;
        private bool FadingIn;

        private bool ManuallyStopped;

        public MusicObject(string MusicString, float Volume, int MaxIndex)
        {
            this.MusicString = MusicString;
            this.MaxIndex = MaxIndex;
            Index = 1;

            WaveOut = new WaveOut();

            if (MaxIndex > 0)
            {
                // Setup playback since we are looping
                WaveOut.PlaybackStopped += OnStoppedHandler;
            }

            SetupMusic(Volume);
        }

        private void SetupMusic(float Volume)
        {
            string TmpMusicString = MusicString;

            if (MaxIndex > 0)
            {
                if (Index < 10)
                    TmpMusicString += "0";
                TmpMusicString += Index;
            }

            VorbisWaveReader reader = new VorbisWaveReader(SoundsLib.GetStreamFromMusic(TmpMusicString));
            Provider = new VolumeWaveProvider16(reader.ToWaveProvider16());
            Provider.Volume = Volume;

            WaveOut.Init(Provider);
            WaveOut.Play();
        }

        private void OnStoppedHandler(object sender, EventArgs e)
        {
            if (ManuallyStopped)
                return;

            Index++;

            if (Index > MaxIndex)
                Index = 1;

            SetupMusic(Provider.Volume);
        }

        public void SetVolume(float Volume)
        {
            Provider.Volume = Volume;
        }

        public float GetVolume()
        {
            return Provider.Volume;
        }

        public void Pause()
        {
            WaveOut.Pause();
        }

        public void Stop()
        {
            ManuallyStopped = true;

            WaveOut.Stop();
        }

        public void Resume()
        {
            WaveOut.Resume();
        }

        public string GetMusicString()
        {
            return MusicString;
        }

        public bool GetFadingOut()
        {
            return FadingOut;
        }

        public bool GetFadingIn()
        {
            return FadingIn;
        }

        public void SetFadingOut(bool FadingOut)
        {
            this.FadingOut = FadingOut;
        }

        public void SetFadingIn(bool FadingIn)
        {
            this.FadingIn = FadingIn;
        }
    }
}
