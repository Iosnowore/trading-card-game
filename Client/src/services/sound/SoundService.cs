﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using NAudio.Vorbis;
using NAudio.Wave;
using Sounds;
using System;
using System.Windows.Forms;
using TradingCardGame.Src.Services.Settings;

namespace TradingCardGame.services.sound
{
    public class SoundService
    {
        private static readonly WaveOut waveOut = new WaveOut();
        private static VolumeWaveProvider16 provider;

        public static void PlaySound(string soundFile)
        {
            VorbisWaveReader reader = new VorbisWaveReader(SoundsLib.GetStreamFromSoundEffects(soundFile));
            provider = new VolumeWaveProvider16(reader.ToWaveProvider16());
            SetVolume(float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "SoundVolume")));
            waveOut.Init(provider);
            waveOut.Play();
        }

        private static readonly Keys[] KEYS = {
            Keys.Enter,
            Keys.ShiftKey,
            Keys.CapsLock,
            Keys.ControlKey,
            Keys.Escape,
            Keys.LWin,
            Keys.RWin,
            Keys.NumLock,
            Keys.Scroll,
            Keys.Insert,
            Keys.F1,
            Keys.F2,
            Keys.F3,
            Keys.F4,
            Keys.F5,
            Keys.F6,
            Keys.F7,
            Keys.F8,
            Keys.F9,
            Keys.F10,
            Keys.F11,
            Keys.F12
        };

        public static void SetVolume(float volume)
        {
            if (provider != null)
            {
                provider.Volume = volume;
            }
        }

        public static float GetVolume()
        {
            if (provider != null)
            {
                return provider.Volume;
            }
            return 0f;
        }

        public static void KeyPressSound(Keys keyPressed)
        {
            if (!SettingsService.GetBoolFromSettingsXml("Audio", "Sound") || Array.IndexOf(KEYS, keyPressed) > -1)
                return;

            //SoundPlayer type = new SoundPlayer(Properties.Resources.ui_keyboard_clicking);
            //type.Play();
        }
    }
}
