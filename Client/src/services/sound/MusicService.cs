﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Timers;
using TradingCardGame.Src.Services.Settings;
using TradingCardGame.Src.Services.Sound;

namespace TradingCardGame.services.sound
{
    public class MusicService
    {
        private static readonly List<MusicObject> ActiveMusic = new List<MusicObject>();

        private static readonly Timer FadeTimer = new Timer();

        public static void PlayMusic(string MusicString)
        {
            float Volume = float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume"));
            PlayMusic(MusicString, Volume, 0);
        }

        public static void PlayMusic(string MusicString, int MaxIndex)
        {
            float Volume = float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume"));
            PlayMusic(MusicString, Volume, MaxIndex);
        }

        public static void PlayMusic(string MusicString, float Volume, int MaxIndex)
        {
            MusicObject MusicObject = new MusicObject(MusicString, Volume, MaxIndex);
            ActiveMusic.Add(MusicObject);
        }

        private static MusicObject GetMusicObject(string MusicString)
        {
            foreach (MusicObject MusicObject in ActiveMusic)
                if (MusicObject.GetMusicString().Equals(MusicString))
                    return MusicObject;
            return null;
        }

        public static void TransitionMusic(string MusicString, int MaxIndex)
        {
            // Make sure we don't restart what we are already listening to
            MusicObject MusicObject = GetMusicObject(MusicString);
            if (MusicObject == null)
            {
                // Fade out other music playing
                for (int i = 0; i < ActiveMusic.Count; i++)
                    ActiveMusic[i].SetFadingOut(true);

                // Create new MusicObject
                PlayMusic(MusicString, 0f, MaxIndex);

                // Fade in new music
                MusicObject = GetMusicObject(MusicString);
                MusicObject.SetFadingIn(true);

                FadeTimer.Interval = 50;
                FadeTimer.Elapsed += FadeTimer_Elapsed;
                FadeTimer.Start();
            }
        }

        private static void FadeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            bool StillUpdating = false;

            float FadeVolumeAmount = 0.01f;
            float TargetVolume = float.Parse(SettingsService.GetStringFromSettingsXml("Audio", "MusicVolume"));

            foreach (MusicObject MusicObject in ActiveMusic)
            {
                if (MusicObject.GetFadingIn())
                {
                    float Volume = MusicObject.GetVolume();

                    if (Volume + FadeVolumeAmount > TargetVolume)
                    {
                        MusicObject.SetVolume(TargetVolume);
                        MusicObject.SetFadingIn(false);
                    }
                    else
                    {
                        MusicObject.SetVolume(Volume + FadeVolumeAmount);
                        StillUpdating = true;
                    }
                }
                else if (MusicObject.GetFadingOut())
                {
                    float Volume = MusicObject.GetVolume();

                    if (Volume - FadeVolumeAmount < 0)
                    {
                        MusicObject.SetVolume(0);
                        MusicObject.Stop();
                        MusicObject.SetFadingOut(false);

                        ActiveMusic.Remove(MusicObject);
                    }
                    else
                    {
                        MusicObject.SetVolume(Volume - FadeVolumeAmount);
                        StillUpdating = true;
                    }
                }
            }

            // Check to see if we still have use for this
            if (!StillUpdating)
                FadeTimer.Stop();
        }

        public static bool MusicIsPlaying()
        {
            return ActiveMusic.Count > 0;
        }

        public static void SetVolume(float Volume)
        {
            foreach (MusicObject MusicObject in ActiveMusic)
                MusicObject.SetVolume(Volume);
        }

        public static float GetVolume()
        {
            return ActiveMusic[0].GetVolume();
        }

        public static void StopMusic()
        {
            foreach (MusicObject MusicObject in ActiveMusic)
                MusicObject.Stop();

            ActiveMusic.Clear();
        }
    }
}
