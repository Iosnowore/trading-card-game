﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.packet;
using TradingCardGame.Src.Services.Chat;

namespace TradingCardGame.services.guild
{
    public class GuildService
    {
        private static string GuildName;

        public static void SetGuildName(string GuildName)
        {
            GuildService.GuildName = GuildName;
        }

        public static string GetGuildName()
        {
            return GuildName;
        }

        public static void createGuild(string guildName)
        {
            PacketService.SendPacket(Commands.GUILD_CREATE, new CommandPacket(new string[] { guildName }));
        }

        public void sendGuildInvite(string playerToInvite)
        {
            PacketService.SendPacket(Commands.GUILD_INVITE, new CommandPacket(new string[] { playerToInvite }));
        }

        public static void LeaveGuildCommand(ListBox chatBox)
        {
            PacketService.SendPacket(Commands.GUILD_LEAVE, null);
        }

        public static void GuildKick(string username)
        {
            PacketService.SendPacket(Commands.GUILD_KICK, new CommandPacket(new string[] { username }));
        }

        public static void HandleGuildInvite(TCG TCG, string inviterUsername, string guildName, string guildAbbrev)
        {
            DialogResult dr = MessageBox.Show("You have been invited to the guild " + guildName + " (" + guildAbbrev + ") by player " + inviterUsername + ", would you like to join?", "Guild Invitation To " + '<' + guildAbbrev + '>', MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                PacketService.SendPacket(Commands.GUILD_ACCEPT_INVITE, new CommandPacket(new string[] { guildAbbrev }));
                TCG.AddSystemMessage("You have accepted the guild invite to <" + guildAbbrev + ">.", "Guild Invite Accepted");
            }
            else
                TCG.AddSystemMessage("You have declined the guild invite to <" + guildAbbrev + ">.", "Guild Invite Declined");
        }

        public static void GuildInviteCommand(string playerToInvite, ListBox chatBox)
        {
            if (!string.IsNullOrEmpty(playerToInvite))
                PacketService.SendPacket(Commands.GUILD_INVITE, new CommandPacket(new string[] { playerToInvite }));
        }

        public static void SendGuildChat(string message)
        {
            ChatService.SendGuildChatMessage(message);
        }

        public static void PromoteToOfficer(string Member)
        {
            PacketService.SendPacket(Commands.GUILD_PROMOTE, new CommandPacket(new string[] { Member }));
        }

        public static void DemoteToMember(string Member)
        {
            PacketService.SendPacket(Commands.GUILD_DEMOTE, new CommandPacket(new string[] { Member }));
        }
    }
}
