﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using DiscordRPC;
using DiscordRPC.Logging;
using System;

namespace TradingCardGame.Src.Services.Discord
{
    public class DiscordPresenceService
    {
        private static DiscordRpcClient DiscordRpcClient;

        public static void InitializePresence()
        {
            DiscordRpcClient = new DiscordRpcClient("585581107328843787")
            {

                //Set the logger
                Logger = new ConsoleLogger() { Level = LogLevel.Warning }
            };

            //Subscribe to events
            DiscordRpcClient.OnReady += (sender, e) =>
            {
                Console.WriteLine("Received Ready from user {0}", e.User.Username);
            };

            DiscordRpcClient.OnPresenceUpdate += (sender, e) =>
            {
                Console.WriteLine("Received Update! {0}", e.Presence);
            };

            //Connect to the RPC
            DiscordRpcClient.Initialize();

            //Set the rich presence
            //Call this as many times as you want and anywhere in your code.
            DiscordRpcClient.SetPresence(new RichPresence()
            {
                Details = "Loading Game",
                Assets = new Assets()
                {
                    LargeImageKey = "tcg_logo",
                    LargeImageText = "SWG: TCGEmu",
                    //SmallImageKey = "image_small"
                }
            });

            DiscordRpcClient.UpdateStartTime();
            DiscordRpcClient.RegisterUriScheme();
        }

        public static void UpdateDetails(string Details)
        {
            if (DiscordRpcClient != null)
                DiscordRpcClient.UpdateDetails(Details);
        }

        public static void UpdatePartyState(string State)
        {
            if (DiscordRpcClient != null)
                DiscordRpcClient.UpdateState(State);
        }

        public static void UpdatePartySize(int size)
        {
            if (DiscordRpcClient != null)
                DiscordRpcClient.UpdatePartySize(size);
        }

        public static void SetParty(string Id, int Max, int Size)
        {
            Party party = new Party
            {
                ID = Id,
                Max = Max,
                Size = Size
            };

            if (DiscordRpcClient != null)
                DiscordRpcClient.UpdateParty(party);
        }

        public static void UpdateJoinSecret(string JoinSecret)
        {
            Secrets Secrets = new Secrets
            {
                JoinSecret = JoinSecret
            };

            if (DiscordRpcClient != null)
                DiscordRpcClient.UpdateSecrets(Secrets);
        }
    }
}
