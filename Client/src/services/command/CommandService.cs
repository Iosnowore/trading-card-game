﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using String;
using System;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Services;
using TradingCardGame.forms;
using TradingCardGame.services.guild;
using TradingCardGame.services.packet;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.services.command
{
    public class CommandService
    {
        public static void Command(string command, ListBox listbox)
        {
            string cmd = command;
            string[] args = null;

            if (command.Contains(" "))
            {
                cmd = command.Substring(0, command.IndexOf(' '));
                args = command.Substring(cmd.Length + 1).Split(' ');
            }

            cmd = cmd.ToLower();

            byte cmdByte = Commands.CommandStringToByte(cmd);

            switch (cmdByte)
            {
                case Commands.BROADCAST_MESSAGE:
                    string Message = string.Join(" ", args);
                    if (!string.IsNullOrWhiteSpace(Message))
                        PacketService.SendPacket(Commands.BROADCAST_MESSAGE, new CommandPacket(new string[] { Message }));
                    break;
                case Commands.CLEAR_CHAT:
                    listbox.Items.Clear();
                    listbox.Items.Add("Chat cleared.");
                    break;
                case Commands.FRIEND_ADD:
                    if (args != null)
                    {
                        PlayerService.AddFriendCommand(args[0]);
                    }
                    else
                    {
                        listbox.Items.Add("Please specify a player name to add.");
                    }
                    break;
                case Commands.FRIEND_REMOVE:
                    if (args != null)
                    {
                        PlayerService.RemoveFriendCommand(args[0]);
                    }
                    else
                    {
                        listbox.Items.Add("Please specify a player name to remove.");
                    }
                    break;
                case Commands.GUILD_CHAT:
                    GuildService.SendGuildChat(string.Join(" ", args));
                    break;
                case Commands.GUILD_OPEN_PAGE:
                    listbox.Parent.Parent.Controls.Add(new Guild());
                    break;
                case Commands.GUILD_INVITE:
                    if (args != null)
                        GuildService.GuildInviteCommand(args[0], listbox);
                    else
                        listbox.Items.Add("Please specify a player name to invite.");
                    break;
                case Commands.GUILD_KICK:
                    if (args != null)
                        GuildService.GuildKick(args[0]);
                    else
                        listbox.Items.Add("Please specify a player name to kick.");
                    break;
                case Commands.GUILD_LEAVE:
                    GuildService.LeaveGuildCommand(listbox);
                    break;
                case Commands.IGNORE_PLAYER:
                    if (args != null)
                        PlayerService.IgnorePlayerCommand(args[0]);
                    else
                        listbox.Items.Add("Please specify a player name to ignore.");
                    break;
                case Commands.UNIGNORE_PLAYER:
                    if (args != null)
                        PlayerService.UnignorePlayerCommand(args[0]);
                    else
                        listbox.Items.Add("Please specify a player name to unignore.");
                    break;
                case Commands.KICK_PLAYER:
                    KickPlayerCommand(args, listbox);
                    break;
                case Commands.ME:
                    if (args != null)
                    {
                        Message = string.Join(" ", args);

                        if (!string.IsNullOrWhiteSpace(Message))
                            PacketService.SendPacket(Commands.ME, new CommandPacket(new string[] { Message }));
                    }
                    break;
                case Commands.TIME:
                    DateTime eastern = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time");
                    listbox.Items.Add("Server Time: " + eastern);
                    break;
                case Commands.WHISPER:
                    if (args != null)
                    {
                        string Username = args[0];
                        args[0] = string.Empty;
                        Whisper(Username, string.Join(" ", args), listbox);
                    }
                    break;
                case Commands.POSTEDTRADE:
                    listbox.Parent.Parent.Controls.Add(new Window(new PostedTrades(), "Posted Trades"));
                    break;
                case Commands.HELP:
                    PrintHelpOutput(listbox);
                    break;
                case Commands.SAYTO:
                    if (args != null)
                    {
                        string Target = args[0];
                        args[0] = string.Empty;
                        PacketService.SendPacket(Commands.SAYTO, new CommandPacket(new string[] { Target, string.Join(" ", args) }));
                    }
                    break;
                case Commands.PING_PLAYER:
                    if (args != null)
                    {
                        string Target = args[0];
                        args[0] = string.Empty;
                        SoundService.PlaySound("send_pm");
                        PacketService.SendPacket(Commands.PING_PLAYER, new CommandPacket(new string[] { Target, string.Join(" ", args) }));
                    }
                    break;
                case Commands.SET_CARD_QUANTITY:
                    if (args != null && args.Length == 2)
                        PacketService.SendPacket(Commands.SET_CARD_QUANTITY, new CommandPacket(new object[] { int.Parse(args[0]), int.Parse(args[1]) }));
                    break;
                case Commands.MUTE_PLAYER:
                    if (args != null)
                        PacketService.SendPacket(Commands.MUTE_PLAYER, new CommandPacket(new string[] { args[0] }));
                    break;
                case Commands.JOIN_GAME:
                    if (args != null)
                        PacketService.SendPacket(Commands.JOIN_GAME, new CommandPacket(new string[] { args[0] }));
                    break;
                case Commands.OBSERVE_GAME:
                    if (args != null)
                        PacketService.SendPacket(Commands.OBSERVE_GAME, new CommandPacket(new string[] { args[0] }));
                    break;
                case Commands.TRADE_PLAYER:
                    if (args != null)
                    {
                        if (PlayerService.GetUsername().Equals(args[0]))
                        {
                            listbox.Items.Add("You cannot request a trade with yourself.");
                            return;
                        }
                        PacketService.SendPacket(Commands.TRADE_PLAYER, new CommandPacket(new string[] { args[0] }));
                    }
                    break;
                case Commands.RESUME:
                    // TODO: Implement
                    break;
                default:
                    listbox.Items.Add("Invalid command: " + cmd + ".");
                    break;
            }
            // Move to most recent chat
            listbox.TopIndex = listbox.Items.Count - 1;
        }

        private static void KickPlayerCommand(string[] args, ListBox chatBox)
        {
            string PlayerToKick = args[0];

            if (!string.IsNullOrEmpty(PlayerToKick))
            {
                args[0] = string.Empty;
                PacketService.SendPacket(Commands.KICK_PLAYER, new CommandPacket(new string[] { PlayerToKick, string.Join(" ", args) }));
                return;
            }
            chatBox.Items.Add("Please enter a valid player name.");
        }

        private static void Whisper(string Target, string Message, ListBox chatBox)
        {
            if (!string.IsNullOrEmpty(Message))
                PacketService.SendPacket(Commands.WHISPER, new CommandPacket(new string[] { Target, Message }));
        }

        public static void ExitGame()
        {
            SoundService.PlaySound("button");

            DialogResult dr = MessageBox.Show("Are you sure that you want to exit?", "Exit Confirmation", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
                Application.Exit();
        }

        private static void PrintHelpOutput(ListBox ListBox)
        {
            ListBox.Items.Add("Available commands:");

            string[] Commands = DatatableReader.GetValues("commands", "command");

            string CommandName;
            string CommandDescription;

            foreach (string Command in Commands)
            {
                CommandName = StringLib.GetString("commands:" + Command + "_n");
                CommandDescription = StringLib.GetString("commands:" + Command + "_d");

                ListBox.Items.Add(CommandName + ": " + CommandDescription);
            }
        }
    }
}
