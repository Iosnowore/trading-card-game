﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TradingCardGame.services.packet;

namespace TradingCardGame.services.multiplayer
{
    class MultiplayerService
    {
        private const int CASUAL_CAPACITY_MOS_EISLEY = 6;

        public static string getLobbyPositionValue(int position)
        {
            return "0";
        }

        public static string[] getAllPlayersNamesInLobby()
        {
            List<string> players = new List<string>();

            for (byte i = 0; i < CASUAL_CAPACITY_MOS_EISLEY; i++)
            {
                if (!string.IsNullOrEmpty(getLobbyPositionValue(i)))
                    players.Add(getLobbyPositionValue(i));
            }
            return players.ToArray();
        }

        public static int findEmptyLobbySpot(int playerCapacity)
        {
            for (byte i = 0; i < playerCapacity; i++)
            {
                if (string.IsNullOrEmpty(getLobbyPositionValue(i)))
                    return i;
            }
            return -1;
        }

        public static int getCurrentLobbyPosition()
        {
            for (byte i = 0; i < CASUAL_CAPACITY_MOS_EISLEY; i++)
            {
                if (getLobbyPositionValue(i).Equals(PacketService.GetId()))
                    return i;
            }
            return -1;
        }

        public static string[] getAllMatchIds()
        {
            // TODO: return match ids from server
            return new string[0];
        }

        public void joinGame()
        {

        }

        public int getCurrentGames()
        {
            return 0;
        }

        public int getOnlinePlayers()
        {
            return 1;
        }
    }
}
