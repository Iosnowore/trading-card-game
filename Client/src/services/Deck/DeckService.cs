﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData.Packets.DeckBuilder;

namespace TradingCardGame.Src.Services.Deck
{
    public class DeckService
    {
        private static string SelectedDeckName;

        public static string GetSelectedDeckName()
        {
            return SelectedDeckName;
        }

        public static void SetSelectedDeckName(string SelectedDeckName)
        {
            DeckService.SelectedDeckName = SelectedDeckName;
        }

        public static int GetNumberOfCardsInDeck(DeckPacket DeckPacket)
        {
            int TotalCardQuantity = 0;

            foreach (int Id in DeckPacket.GetCardIds().Keys)
                TotalCardQuantity += DeckPacket.GetCardIds()[Id];

            return TotalCardQuantity;
        }

        public static bool CheckAvatarValidation(int[] CardIds)
        {
            bool AvatarFound = false;

            TCGData.CardData CardData;

            foreach (int CardId in CardIds)
            {
                CardData = TCGData.Cards.GetCardData(CardId);

                if (CardData.GetType().Equals("Avatar"))
                {
                    if (AvatarFound)
                        return false;
                    AvatarFound = true;
                }
            }
            return AvatarFound;
        }

        public static bool CheckQuestsValidation(int[] CardIds)
        {
            List<int> QuestLevels = new List<int>();

            TCGData.CardData CardData;

            foreach (int CardId in CardIds)
            {
                CardData = TCGData.Cards.GetCardData(CardId);

                if (CardData.GetType().Equals("Quest"))
                {
                    if (QuestLevels.Contains(CardData.GetHealthOrLevel()))
                        return false;
                    QuestLevels.Add(CardData.GetHealthOrLevel());
                }
            }
            return true;
        }

        public static bool CheckDeckValidation(int[] CardIds, SortedDictionary<int, int> DeckCards)
        {
            int DeckQuantity = 0;
            int MaxNumberOfSameCard = 0;

            TCGData.CardData CardData;

            foreach (int CardId in CardIds)
            {
                CardData = TCGData.Cards.GetCardData(CardId);

                if (DeckCards[CardId] > MaxNumberOfSameCard)
                    MaxNumberOfSameCard = DeckCards[CardId];

                if (!CardData.GetType().Equals("Avatar") && !CardData.GetType().Equals("Quest"))
                    DeckQuantity += DeckCards[CardId];
            }

            return DeckQuantity >= 50 && MaxNumberOfSameCard <= 4;
        }

        public static bool CheckOwnershipValidation(int[] CardIds)
        {
            // Must have one archetype and generic cards.

            string ArchetypeFound = null;

            TCGData.CardData CardData;

            foreach (int CardId in CardIds)
            {
                CardData = TCGData.Cards.GetCardData(CardId);

                if (CardData.GetArchetype().Equals("generic"))
                    continue;

                if (string.IsNullOrEmpty(ArchetypeFound))
                {
                    ArchetypeFound = CardData.GetArchetype();
                }
                else if (!CardData.GetArchetype().Equals(ArchetypeFound))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool CheckAvatarArchetype(int[] CardIds, string[] RequiredArchetypes)
        {
            if (RequiredArchetypes == null)
                return true;

            TCGData.CardData CardData;

            foreach (int CardId in CardIds)
            {
                CardData = TCGData.Cards.GetCardData(CardId);

                if (CardData.GetType().Equals("Avatar"))
                {
                    string AvatarArcheType = CardData.GetArchetype();

                    foreach (string RequiredArchetype in RequiredArchetypes)
                        if (AvatarArcheType.Equals(RequiredArchetype))
                            return true;
                    return false;
                }
            }
            return false;
        }

        public static bool DeckIsGoodForScenario(SortedDictionary<int, int> DeckCards, string[] RequiredArcheTypes)
        {
            int[] CardIds = new int[DeckCards.Count];
            DeckCards.Keys.CopyTo(CardIds, 0);

            if (!CheckAvatarValidation(CardIds))
                return false;
            if (!CheckQuestsValidation(CardIds))
                return false;
            if (!CheckDeckValidation(CardIds, DeckCards))
                return false;
            if (!CheckOwnershipValidation(CardIds))
                return false;
            if (!CheckAvatarArchetype(CardIds, RequiredArcheTypes))
                return false;
            return true;
        }
    }
}
