﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TCGData.Packets.Game;
using TCGData.Packets.Guild;
using TCGData.Packets.Lobby;
using TCGData.Packets.Player;
using TCGData.Packets.Scenario;
using TCGData.Packets.Trade;
using TradingCardGame.forms;
using TradingCardGame.Forms.Screens;
using TradingCardGame.services.guild;
using TradingCardGame.services.sound;
using TradingCardGame.Src.Forms.Dialogs;
using TradingCardGame.Src.Forms.Screens;
using TradingCardGame.Src.Services.Chat;

namespace TradingCardGame.services.packet
{
    public class PacketService
    {
        private static CollectionPacket CollectionPacket;
        private static RequestGuildsPacket RequestGuildsPacket;
        private static GuildInfoPacket GuildInfoPacket;
        private static MatchPacket MatchPacket;
        private static UserInfoPacket UserInfoPacket;
        private static FriendsIgnoreListPacket FriendsIgnoreListPacket;
        private static DecksPacket DecksPacket;
        private static PostedTradesPacket PostedTradesPacket;
        private static LobbyInfoPacket LobbyInfoPacket;
        private static ScenariosPacket ScenariosPacket;

        private static IPAddress ipAddress;
        private static Thread thread;

        private const string SERVER_ADDRESS = "login.swgtcg.com";
        private const int SERVER_PORT = 4242;

        // Network Protocols
        private const int PROTOCOL = PROTOCOL_UDP;
        private const int PROTOCOL_TCP = 1;
        private const int PROTOCOL_UDP = 2;

        // Sleep after requesting a packet so true loop doesn't go crazy
        // This actually makes it almost half the time to recieve a packet
        // Ex. 5 seconds vs 3 seconds
        private const int SLEEP_TIME = 100;

        private static Lobby lobby;
        private static LaunchMatch matchLobby;
        private static Trade Trade;
        private static TradeConfirm TradeConfirm;
        private static Game Game;

        private static TCG TCG;

        private static EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
        private static AsyncCallback recv = null;

        private static Socket Socket;

        private static int Id;

        public static void SetId(int Id)
        {
            PacketService.Id = Id;
        }

        public static int GetId()
        {
            return Id;
        }

        public static void SetLobby(Lobby lobby)
        {
            PacketService.lobby = lobby;
        }

        public static void SetMatchLobby(LaunchMatch matchLobby)
        {
            PacketService.matchLobby = matchLobby;
        }

        public static LaunchMatch GetMatchLobby()
        {
            return matchLobby;
        }

        public static void SetTradeLobby(Trade Trade)
        {
            PacketService.Trade = Trade;
        }

        public static void DisconnectFromServer()
        {
            SendPacket(Commands.CLIENT_DISCONNECT, null);
            Socket.Close();

            // If we were using TCP, abort the thread we created
            if (thread != null)
                thread.Abort();
        }

        public static void SendPacket(byte command, object data)
        {
            byte[] Packet = new PacketSender(Id, command, data).ToBytes();

            switch (PROTOCOL)
            {
                case PROTOCOL_TCP:
                    Socket.Send(Packet);
                    break;
                case PROTOCOL_UDP:
                    Socket.SendTo(Packet, Socket.RemoteEndPoint);
                    break;
            }
        }

        public static void ConnectToServer(TCG TCG, string Username, string Password)
        {
            try
            {
                if (!IPAddress.TryParse(Dns.GetHostAddresses(SERVER_ADDRESS)[0].ToString(), out ipAddress))
                {
                    MessageBox.Show("IP Address is not valid!");
                    Application.Exit();
                }

                switch (PROTOCOL)
                {
                    case PROTOCOL_TCP:
                        Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        Socket.Connect(new IPEndPoint(ipAddress, SERVER_PORT));
                        thread = new Thread(Data_IN);
                        thread.Start();
                        SendPacket(Commands.LOGIN_VALIDATION, new LoginValidationPacket(Username, Password, Assembly.GetExecutingAssembly().GetName().Version));
                        break;
                    case PROTOCOL_UDP:
                        Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                        Socket.Connect(new IPEndPoint(ipAddress, SERVER_PORT));
                        Receive();

                        SendPacket(Commands.LOGIN_VALIDATION, new LoginValidationPacket(Username, Password, Assembly.GetExecutingAssembly().GetName().Version));
                        break;
                }
                PacketService.TCG = TCG;
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("No connection could be made") || ex.Message.StartsWith("A connection attempt failed"))
                    MessageBox.Show("The TCG game server is currently offline. Please try again later!", "Server Offline");
                else if (ex.Message.Equals("No such host is known"))
                    MessageBox.Show("Unknown login address. The client will now close.", "Unknown Login Address");
                else
                    MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        private static void Receive()
        {
            byte[] Buffer = new byte[Socket.ReceiveBufferSize];

            Socket.BeginReceiveFrom(Buffer, 0, Buffer.Length, SocketFlags.None, ref epFrom, recv = (ar) =>
            {
                byte[] so = (byte[])ar.AsyncState;

                try
                {
                    int bytes = Socket.EndReceiveFrom(ar, ref epFrom);
                    Socket.BeginReceiveFrom(Buffer, 0, Buffer.Length, SocketFlags.None, ref epFrom, recv, so);

                    if (bytes > 0)
                        DataManager(new PacketSender(Buffer));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message);
                }

            }, Buffer);
        }

        private static void Data_IN()
        {
            byte[] buffer;
            int readBytes;

            while (true)
            {
                try
                {
                    buffer = new byte[Socket.ReceiveBufferSize];
                    readBytes = Socket.Receive(buffer);

                    if (readBytes > 0)
                        DataManager(new PacketSender(buffer));
                }
                catch (SocketException e)
                {
                    MessageBox.Show(e.Message, "Connection Lost");
                }
            }
        }

        private static void DataManager(PacketSender p)
        {
            Console.WriteLine("Got packet with command: " + p.GetCommand());

            switch (p.GetCommand())
            {
                case Commands.REQUEST_ALLGUILDINFO:
                case Commands.REQUEST_COLLECTION:
                case Commands.REQUEST_DECK_PACKETS:
                case Commands.REQUEST_GUILDINFO:
                case Commands.REQUEST_MATCH:
                case Commands.REQUEST_FRIENDSIGNORELIST:
                case Commands.REQUEST_USERINFO:
                case Commands.REQUEST_POSTEDTRADES:
                case Commands.REQUEST_LOBBYINFO:
                case Commands.REQUEST_SCENARIOS:
                    UpdatePacket(p.GetPacket());
                    break;
                case Commands.BROADCAST_MESSAGE:
                    CommandPacket CommandPacket = (CommandPacket)p.GetPacket();
                    TCG.AddSystemMessage(CommandPacket.GetContent(1).ToString() + ": " + CommandPacket.GetContent(0).ToString(), "SERVER BROADCAST MESSAGE");
                    break;
                case Commands.LOBBY_CHAT:
                case Commands.MATCH_CHAT:
                case Commands.GUILD_CHAT:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    DealWithChatPacket(CommandPacket.GetContent(0).ToString(), CommandPacket.GetContent(1).ToString(), p.GetCommand());
                    break;
                case Commands.GUILD_INVITE:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    if (CommandPacket.GetNumberOfContents() == 1)
                    {
                        if (lobby != null)
                        {
                            AddChatBoxChat(lobby.GetChatBox(), CommandPacket.GetContent(0).ToString());
                        }
                        TCG.AddSystemMessage(CommandPacket.GetContent(0).ToString(), "Guild Invite");
                    }
                    else
                        GuildService.HandleGuildInvite(TCG, CommandPacket.GetContent(0).ToString(), CommandPacket.GetContent(1).ToString(), CommandPacket.GetContent(2).ToString());
                    break;
                case Commands.LOGIN_VALIDATION:
                    Login Login = (Login)TCG.GetChildWindow();
                    Login.HandleLoginValidationPacket(p);
                    break;
                case Commands.MATCH_JOIN:
                    JoinMatchResponsePacket JoinMatchPacket = (JoinMatchResponsePacket)p.GetPacket();
                    switch (JoinMatchPacket.GetResponse())
                    {
                        case JoinMatchResponses.Responses.FULL:
                            TCG.AddSystemMessage("The match you tried to join is full.", "Match Is Full");
                            break;
                        case JoinMatchResponses.Responses.SUCCESS:
                            MatchPacket MatchPacket = JoinMatchPacket.GetMatchPacket();
                            DisplayLaunchMatchDialog(MatchPacket);
                            break;
                    }
                    break;
                case Commands.KICK_PLAYER:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    HandleGettingKicked(CommandPacket.GetContent(1).ToString());
                    break;
                case Commands.UPDATE_LOBBY_USERNAMES:
                    if (lobby == null)
                        return;

                    UpdateLobbyUsersPacket UpdateUsersPacket = (UpdateLobbyUsersPacket)p.GetPacket();

                    string[] Usernames = new string[UpdateUsersPacket.GetPlayers().Count];
                    UpdateUsersPacket.GetPlayers().Keys.CopyTo(Usernames, 0);

                    lobby.UpdateLobbyUsernames(Usernames);
                    AddChatBoxChat(lobby.GetChatBox(), UpdateUsersPacket.GetMessage());
                    break;
                case Commands.UPDATE_MATCH_USERNAMES:
                    if (matchLobby == null)
                        return;

                    UpdateLobbyUsersPacket UpdateMatchUsersPacket = (UpdateLobbyUsersPacket)p.GetPacket();

                    Usernames = new string[UpdateMatchUsersPacket.GetPlayers().Count];
                    UpdateMatchUsersPacket.GetPlayers().Keys.CopyTo(Usernames, 0);

                    int[] Avatars = new int[Usernames.Length];
                    UpdateMatchUsersPacket.GetPlayers().Values.CopyTo(Avatars, 0);

                    matchLobby.UpdateMatchUsernames(Usernames);

                    matchLobby.ReloadUsers(Usernames, Avatars);
                    AddChatBoxChat(matchLobby.GetChatBox(), UpdateMatchUsersPacket.GetMessage());
                    break;
                case Commands.UPDATE_MATCHES:
                    UpdateMatchListPacket UpdateMatchListPacket = (UpdateMatchListPacket)p.GetPacket();
                    lobby.UpdateMatches(UpdateMatchListPacket.GetMatches());
                    break;
                case Commands.WHISPER:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    HandleWhisper(CommandPacket.GetContent(0).ToString(), CommandPacket.GetContent(1).ToString());
                    break;
                case Commands.PING_PLAYER:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    SoundService.PlaySound("receive_pm");
                    AddChatBoxChat(lobby.GetChatBox(), CommandPacket.GetContent(0) + " pinged you, " + CommandPacket.GetContent(1));
                    break;
                case Commands.TRADE_PLAYER:
                    if (p.GetPacket() is CommandPacket)
                    {
                        CommandPacket = (CommandPacket)p.GetPacket();
                        string RequesterUsername = CommandPacket.GetContent(0).ToString();

                        MangleBox MangleBox = new MangleBox("Player " + RequesterUsername + " is requesting a trade. Do you accept?");
                        AddMangleBox(MangleBox, "Trade Request");

                        string Response = MangleBox.GetResponse();

                        if (Response.Equals("Accept"))
                            AddChatBoxChat(lobby.GetChatBox(), "You have accepted " + RequesterUsername + "'s trade request.");
                        else
                            AddChatBoxChat(lobby.GetChatBox(), "You have declined " + RequesterUsername + "'s trade request.");

                        SendPacket(Commands.TRADE_PLAYER, new CommandPacket(new string[] { RequesterUsername, Response }));
                    }
                    else
                    {
                        InitiateTradePacket TradePacket = (InitiateTradePacket)p.GetPacket();
                        AddTradeWindow(TradePacket);
                    }
                    break;
                case Commands.TRADE_ADD:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    int CardId = (int)CommandPacket.GetContent(0);
                    Trade.AddCardYouGet(CardId);
                    break;
                case Commands.TRADE_REMOVE:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    CardId = (int)CommandPacket.GetContent(0);
                    Trade.RemoveCardYouGet(CardId);
                    break;
                case Commands.TRADE_ACCEPT:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    Trade.HandleAcceptPacket(CommandPacket.GetContent(0).Equals("True"));
                    break;
                case Commands.TRADE_CANCEL:
                    Trade.Dispose();
                    TCG.AddSystemMessage("The trade session you were in was canceled.", "Trade Session Canceled");
                    break;
                case Commands.TRADE_CONFIRM:
                    ConfirmTradePacket ConfirmTradePacket = (ConfirmTradePacket)p.GetPacket();
                    AddTradeConfirmWindow(ConfirmTradePacket);
                    break;
                case Commands.TRADE_CONFIRM_ACCEPTED:
                    DisposeDialog(Trade.Parent);
                    TCG.AddSystemMessage("The trade has been accepted.", "Trade Accepted");
                    break;
                case Commands.REDEEM_LOOTCARD:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    CollectionPacket = (CollectionPacket)CommandPacket.GetContent(2);
                    SortedDictionary<int, int> Cards = (SortedDictionary<int, int>)CommandPacket.GetContent(0);
                    int TransactionId = (int)CommandPacket.GetContent(1);
                    ShowDelivery(Cards, TransactionId, false, CollectionPacket);
                    break;
                case Commands.REDEEM_PACK:
                case Commands.REDEEM_PROMOPACK:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    CollectionPacket = (CollectionPacket)CommandPacket.GetContent(2);
                    Cards = (SortedDictionary<int, int>)CommandPacket.GetContent(0);
                    TransactionId = (int)CommandPacket.GetContent(1);
                    ShowDelivery(Cards, TransactionId, true, CollectionPacket);
                    break;
                case Commands.OPEN_PACK:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    CollectionPacket = (CollectionPacket)CommandPacket.GetContent(2);
                    Cards = (SortedDictionary<int, int>)CommandPacket.GetContent(0);
                    TransactionId = (int)CommandPacket.GetContent(1);
                    ShowDelivery(Cards, TransactionId, false, CollectionPacket);
                    break;
                case Commands.CRASH_PING:
                    SendPacket(Commands.CRASH_PING, null);
                    break;
                case Commands.LOAD_GAME:
                    LoadGamePacket GamePacket = (LoadGamePacket)p.GetPacket();

                    int AvatarId = GamePacket.GetAvatarId();
                    int EnemyAvatarId = GamePacket.GetEnemyAvatarId();

                    string Username = GamePacket.GetUsername();
                    string EnemyUsername = GamePacket.GetEnemyUsername();

                    int[] Hand = GamePacket.GetHand();

                    int QuestId = GamePacket.GetQuestId();
                    int EnemyQuestId = GamePacket.GetEnemyQuestId();

                    int CardsInHand = GamePacket.GetHandQty();
                    int EnemyCardsInHand = GamePacket.GetEnemyHandQty();

                    int CardsInDeck = GamePacket.GetDeckQty();
                    int EnemyCardsInDeck = GamePacket.GetEnemyDeckQty();

                    int ScenarioId = GamePacket.GetScenarioId();

                    // Create the new Game object
                    Game NewGame = new Game(AvatarId, EnemyAvatarId, Username, EnemyUsername, Hand, QuestId, EnemyQuestId, CardsInHand, EnemyCardsInHand, CardsInDeck, EnemyCardsInDeck, ScenarioId);

                    // Update the Game object with the new one
                    Game = NewGame;

                    // Let the server know we loaded the game (safe to send redraw packet)
                    SendPacket(Commands.LOAD_GAME, null);

                    // Show the Game window
                    TCG.SetChildWindow(NewGame);
                    break;
                case Commands.PLACE_CARD:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    string PlayerUsername = (string)CommandPacket.GetContent(0);

                    switch (CommandPacket.GetNumberOfContents())
                    {
                        case 2:
                            if (CommandPacket.GetContent(1) is PlaymatCard PlaymatCard)
                            {
                                // Ability / Item Card

                                if (Game.InvokeRequired)
                                    Game.Invoke(new Game._PlaceCard(Game.PlaceCard), PlayerUsername, PlaymatCard);
                                else
                                    Game.PlaceCard(PlayerUsername, PlaymatCard);
                            }
                            else
                            {
                                // Tactic Card
                                QuestId = (int)CommandPacket.GetContent(1);

                                if (Game.InvokeRequired)
                                    Game.Invoke(new Game._PlayTactic(Game.PlayTactic), PlayerUsername, QuestId);
                                else
                                    Game.PlayTactic(PlayerUsername, QuestId);
                            }
                            break;
                        case 4:
                            // Unit Card
                            QuestId = (int)CommandPacket.GetContent(1);
                            PlaymatCard UnitCard = (PlaymatCard)CommandPacket.GetContent(2);
                            int Key = (int)CommandPacket.GetContent(3);

                            if (Game.InvokeRequired)
                                Game.Invoke(new Game._PlacePlaymatCard(Game.PlacePlaymatCard), PlayerUsername, QuestId, UnitCard, Key);
                            else
                                Game.PlacePlaymatCard(PlayerUsername, QuestId, UnitCard, Key);
                            break;
                    }
                    break;
                case Commands.REDRAW_HAND:
                    if (p.GetPacket() == null)
                    {
                        // We are prompting the user to redraw
                        if (Game.InvokeRequired)
                            Game.Invoke(new Game._HandlePrompRedraw(Game.HandlePromptRedraw));
                        else
                            Game.HandlePromptRedraw();
                    }
                    else
                    {
                        // We are updating the player's hand
                        HandPacket HandPacket = (HandPacket)p.GetPacket();
                        int[] HandIds = HandPacket.GetHandCardIds();

                        if (Game.InvokeRequired)
                            Game.Invoke(new Game._HandleRedraw(Game.HandleRedraw), HandIds);
                        else
                            Game.HandleRedraw(HandIds);
                    }
                    break;
                case Commands.UPDATE_AVATAR_HEALTH:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    // Get the player username and their new avatar health
                    Username = (string)CommandPacket.GetContent(0);
                    int AvatarHealth = (int)CommandPacket.GetContent(1);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleAvatarHealthChange(Game.HandleAvatarHealthChange), Username, AvatarHealth);
                    else
                        Game.HandleAvatarHealthChange(Username, AvatarHealth);
                    break;
                case Commands.UPDATE_AVATAR_POWER:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    // Get the player username and their new avatar health
                    Username = (string)CommandPacket.GetContent(0);
                    int AvatarPower = (int)CommandPacket.GetContent(1);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleAvatarPowerChange(Game.HandleAvatarPowerChange), Username, AvatarPower);
                    else
                        Game.HandleAvatarPowerChange(Username, AvatarPower);
                    break;
                case Commands.UPDATE_CARD_STATS:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    //@TODO: Fix this (NPC is only sending contents of 2, Username and HandQty)
                    if (CommandPacket.GetNumberOfContents() != 4)
                    {
                        MessageBox.Show("Invalid length of contents for UPDATE_CARD_STATS: " + CommandPacket.GetNumberOfContents());
                        return;
                    }

                    Username = (string)CommandPacket.GetContent(0);

                    int HandQty = (int)CommandPacket.GetContent(1);
                    int DeckQty = (int)CommandPacket.GetContent(2);
                    int DiscardedQty = (int)CommandPacket.GetContent(3);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleUpdateCardStats(Game.HandleUpdateCardsStats), Username, HandQty, DeckQty, DiscardedQty);
                    else
                        Game.HandleUpdateCardsStats(Username, HandQty, DeckQty, DiscardedQty);
                    break;
                case Commands.DRAW_PHASE:
                    int[] HandCardIds = null;

                    if (p.GetPacket() != null)
                    {
                        HandPacket HandPacket = (HandPacket)p.GetPacket();
                        HandCardIds = HandPacket.GetHandCardIds();
                    }

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleDrawPhase(Game.HandleDrawPhase), HandCardIds);
                    else
                        Game.HandleDrawPhase(HandCardIds);
                    break;
                case Commands.QUEST_PHASE:
                    bool MyTurn = false;
                    bool CanChooseQuest = false;

                    if (p.GetPacket() != null)
                    {
                        MyTurn = true;

                        CommandPacket = (CommandPacket)p.GetPacket();
                        CanChooseQuest = (bool)CommandPacket.GetContent(0);
                    }

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleQuestPhase(Game.HandleQuestPhase), MyTurn, CanChooseQuest);
                    else
                        Game.HandleQuestPhase(MyTurn, CanChooseQuest);
                    break;
                case Commands.QUEST_ABILITY:
                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleQuestAbility(Game.HandleQuestAbility));
                    else
                        Game.HandleQuestAbility();
                    break;
                case Commands.READY_PHASE:
                    MyTurn = false;

                    if (p.GetPacket() != null)
                    {
                        CommandPacket = (CommandPacket)p.GetPacket();
                        MyTurn = true;
                    }

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleReadyPhase(Game.HandleReadyPhase), MyTurn);
                    else
                        Game.HandleReadyPhase(MyTurn);
                    break;
                case Commands.MAIN_PHASE:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    Username = (string)CommandPacket.GetContent(0);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleMainPhase(Game.HandleMainPhase), Username);
                    else
                        Game.HandleMainPhase(Username);
                    break;
                case Commands.QUEST_COMPLETE:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    QuestId = (int)CommandPacket.GetContent(0);
                    int NewQuestId = (int)CommandPacket.GetContent(1);

                    PlayerUsername = (string)CommandPacket.GetContent(2);

                    int MaxAvatarPower = (int)CommandPacket.GetContent(3);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleQuestCompleted(Game.HandleQuestCompleted), QuestId, NewQuestId, PlayerUsername, MaxAvatarPower);
                    else
                        Game.HandleQuestCompleted(QuestId, NewQuestId, PlayerUsername, MaxAvatarPower);
                    break;
                case Commands.QUEST_UPDATE_COMPLETED_LEVELS:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    QuestId = (int)CommandPacket.GetContent(0);
                    int CompletedLevels = (int)CommandPacket.GetContent(1);
                    PlayerUsername = (string)CommandPacket.GetContent(2);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleQuestUpdateCompletedLevels(Game.HandleQuestUpdateCompletedLevels), QuestId, CompletedLevels, PlayerUsername);
                    else
                        Game.HandleQuestUpdateCompletedLevels(QuestId, CompletedLevels, PlayerUsername);
                    break;
                case Commands.START_RAID:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    string AttackingUsername = (string)CommandPacket.GetContent(0);
                    QuestId = (int)CommandPacket.GetContent(1);

                    int AttackValue = (int)CommandPacket.GetContent(2);
                    int DefenseValue = (int)CommandPacket.GetContent(3);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleStartRaid(Game.HandleStartRaid), AttackingUsername, QuestId, AttackValue, DefenseValue);
                    else
                        Game.HandleStartRaid(AttackingUsername, QuestId, AttackValue, DefenseValue);
                    break;
                case Commands.UPDATE_PLAYED_ABILITIES:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    PlaymatCard[] AbilityCardsPlayed = (PlaymatCard[])CommandPacket.GetContent(0);
                    Username = (string)CommandPacket.GetContent(1);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleUpdatePlayedAbilities(Game.HandleUpdatePlayedAbilities), AbilityCardsPlayed, Username);
                    else
                        Game.HandleUpdatePlayedAbilities(AbilityCardsPlayed, Username);
                    break;
                case Commands.RAID_EXERT_CARD:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    int ExertType = (int)CommandPacket.GetContent(0);

                    // Check if it's a request packet
                    if (CommandPacket.GetNumberOfContents() == 1)
                    {
                        if (Game.InvokeRequired)
                            Game.Invoke(new Game._HandleRequestExertCard(Game.HandleRequestExertCard), ExertType);
                        else
                            Game.HandleRequestExertCard(ExertType);
                    }
                    else
                    {
                        PlayerUsername = (string)CommandPacket.GetContent(1);
                        int ExertedCardKey = (int)CommandPacket.GetContent(2);
                        string CardType = (string)CommandPacket.GetContent(3);

                        if (Game.InvokeRequired)
                            Game.Invoke(new Game._HandleExertCard(Game.HandleExertCard), PlayerUsername, ExertedCardKey, ExertType, CardType);
                        else
                            Game.HandleExertCard(PlayerUsername, ExertedCardKey, ExertType, CardType);
                    }
                    break;
                case Commands.RAID_EXERT_AVATAR:
                    CommandPacket = (CommandPacket)p.GetPacket();

                    PlayerUsername = (string)CommandPacket.GetContent(0);
                    ExertType = (int)CommandPacket.GetContent(1);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleExertAvatar(Game.HandleExertAvatar), PlayerUsername, ExertType);
                    else
                        Game.HandleExertAvatar(PlayerUsername, ExertType);
                    break;
                case Commands.RAID_SHOW_OUTCOME:
                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleShowRaidOutcome(Game.HandleShowRaidOutcome));
                    else
                        Game.HandleShowRaidOutcome();
                    break;
                case Commands.RAID_TAKE_DAMAGE:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    int Damage = (int)CommandPacket.GetContent(0);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleRequestTakeDamage(Game.HandleRequestTakeDamage), Damage);
                    else
                        Game.HandleRequestTakeDamage(Damage);
                    break;
                case Commands.RAID_DAMAGE_CARD:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    int CardKey = (int)CommandPacket.GetContent(0);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleDamageCard(Game.HandleDamageCard), CardKey);
                    else
                        Game.HandleDamageCard(CardKey);
                    break;
                case Commands.RAID_DESTROY_CARD:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    CardKey = (int)CommandPacket.GetContent(0);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleDestroyCard(Game.HandleDestroyCard), CardKey);
                    else
                        Game.HandleDestroyCard(CardKey);
                    break;
                case Commands.RAID_END:
                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleEndRaid(Game.HandleEndRaid));
                    else
                        Game.HandleEndRaid();
                    break;
                case Commands.GAME_END:
                    CommandPacket = (CommandPacket)p.GetPacket();
                    Username = (string)CommandPacket.GetContent(0);

                    if (Game.InvokeRequired)
                        Game.Invoke(new Game._HandleEndGame(Game.HandleEndGame), Username);
                    else
                        Game.HandleEndGame(Username);
                    break;
                default:
                    Console.WriteLine("Got bad packet. Command: " + p.GetCommand());
                    break;
            }
        }

        private delegate void _DisposeDialog(Control Dialog);
        private static void DisposeDialog(Control Dialog)
        {
            if (Dialog.InvokeRequired)
                Dialog.Invoke(new _DisposeDialog(DisposeDialog), Dialog);
            else
                Dialog.Dispose();
        }

        private delegate void _AddTradeConfirmWindow(ConfirmTradePacket Packet);
        private static void AddTradeConfirmWindow(ConfirmTradePacket Packet)
        {
            if (Trade.InvokeRequired)
                Trade.Invoke(new _AddTradeConfirmWindow(AddTradeConfirmWindow), Packet);
            else
            {
                TradeConfirm = new TradeConfirm(Packet, Trade, false);
                Trade.Controls.Add(new Window(TradeConfirm, "Confirm Trade"));
            }
        }

        private delegate void _AddTradeWindow(InitiateTradePacket TradePacket);
        private static void AddTradeWindow(InitiateTradePacket TradePacket)
        {
            if (lobby.InvokeRequired)
                lobby.Invoke(new _AddTradeWindow(AddTradeWindow), TradePacket);
            else
                lobby.Controls.Add(new Window(new Trade(TradePacket.GetTargetsUsername(), TradePacket.GetTargetsCollection(), TradePacket.GetCollection()), "Trade"));
        }

        private delegate void _AddMangleBox(MangleBox MangleBox, string Title);
        private static void AddMangleBox(MangleBox MangleBox, string Title)
        {
            if (lobby.InvokeRequired)
                lobby.Invoke(new _AddMangleBox(AddMangleBox), MangleBox, Title);
            else
                lobby.Controls.Add(new Window(MangleBox, Title));
        }

        private delegate void _ShowDelivery(SortedDictionary<int, int> Cards, int TransactionId, bool IsPack, CollectionPacket UpdatedCollection);
        private static void ShowDelivery(SortedDictionary<int, int> Cards, int TransactionId, bool IsPack, CollectionPacket UpdatedCollection)
        {
            CollectionManager CM = (CollectionManager)TCG.GetChildWindow();

            if (CM.InvokeRequired)
                CM.Invoke(new _ShowDelivery(ShowDelivery), Cards, TransactionId, IsPack, UpdatedCollection);
            else
            {
                // Setup delivery window
                Delivery Delivery = new Delivery(Cards, TransactionId, IsPack);
                CM.SetDelivery(Delivery);
                CM.Controls.Add(new Window(Delivery, "Delivery"));

                // Update the collection manager with the new CollectionPacket and load it
                CM.UpdateCollection(UpdatedCollection);
                CM.LoadCards();
            }
        }

        private delegate void _DisplayLaunchMatchDialog(MatchPacket MatchPacket);
        private static void DisplayLaunchMatchDialog(MatchPacket MatchPacket)
        {
            if (lobby.InvokeRequired)
                lobby.Invoke(new _DisplayLaunchMatchDialog(DisplayLaunchMatchDialog), MatchPacket);
            else
                lobby.Controls.Add(new Window(new LaunchMatch(MatchPacket), "Match #" + MatchPacket.GetMatchId()));
        }

        private delegate void _DealWithChatPacket(string username, string message, byte Command);
        private static void DealWithChatPacket(string username, string message, byte Command)
        {
            if ((lobby == null || lobby.GetChatBox() == null) && (matchLobby == null && matchLobby.GetChatBox() == null) && (Trade == null && Trade.GetChatBox() == null))
                return;

            ListBox chatBox = null;

            switch (Command)
            {
                case Commands.LOBBY_CHAT:
                case Commands.GUILD_CHAT:
                    if (lobby.GetChatBox() != null)
                        chatBox = lobby.GetChatBox();
                    else if (Trade.GetChatBox() != null)
                        chatBox = Trade.GetChatBox();
                    break;
                case Commands.MATCH_CHAT:
                    if (matchLobby.GetChatBox() != null)
                        chatBox = matchLobby.GetChatBox();
                    break;
            }

            if (chatBox.InvokeRequired)
                chatBox.Invoke(new _DealWithChatPacket(DealWithChatPacket), username, message, Command);
            else
            {
                if (!username.Equals(GameData.ServerId))
                {
                    string[] ChatBoxLines = ChatService.GetChatBoxLines(username + ": " + message);

                    foreach (string ChatBoxLine in ChatBoxLines)
                        chatBox.Items.Add(ChatBoxLine);
                }
                else
                    chatBox.Items.Add(message);

                chatBox.TopIndex = chatBox.Items.Count - 1;
            }
        }

        private delegate void _AddChatBoxChat(ListBox data1, string data2);
        public static void AddChatBoxChat(ListBox chatBox, string message)
        {
            if (chatBox.InvokeRequired)
                chatBox.Invoke(new _AddChatBoxChat(AddChatBoxChat), chatBox, message);
            else
                chatBox.Items.Add(message);
        }

        private static void HandleWhisper(string sender, string message)
        {
            AddChatBoxChat(lobby.GetChatBox(), sender + " whispers to you, " + message);
        }

        private static void HandleGettingKicked(string KickReason)
        {
            AddChatBoxChat(lobby.GetChatBox(), "You have been kicked off the game for reason: " + KickReason);
            TCG.AddSystemMessage("You have been kicked from the game.\nReason: " + KickReason, "Kicked From The Game");
            //Environment.Exit(1);
        }

        public static void SendBytes(byte[] bytes)
        {
            Socket.Send(bytes);
        }

        public static void UpdatePacket(object Packet)
        {
            if (Packet is CollectionPacket _CollectionPacket)
                CollectionPacket = _CollectionPacket;
            else if (Packet is RequestGuildsPacket _RequestGuildsPacket)
                RequestGuildsPacket = _RequestGuildsPacket;
            else if (Packet is GuildInfoPacket _GuildInfoPacket)
                GuildInfoPacket = _GuildInfoPacket;
            else if (Packet is MatchPacket _MatchPacket)
                MatchPacket = _MatchPacket;
            else if (Packet is UserInfoPacket _UserInfoPacket)
                UserInfoPacket = _UserInfoPacket;
            else if (Packet is FriendsIgnoreListPacket _FriendsIgnoreListPacket)
                FriendsIgnoreListPacket = _FriendsIgnoreListPacket;
            else if (Packet is DecksPacket _DecksPacket)
                DecksPacket = _DecksPacket;
            else if (Packet is PostedTradesPacket _PostedTradesPacket)
                PostedTradesPacket = _PostedTradesPacket;
            else if (Packet is LobbyInfoPacket _LobbyInfoPacket)
                LobbyInfoPacket = _LobbyInfoPacket;
            else if (Packet is ScenariosPacket _ScenariosPacket)
                ScenariosPacket = _ScenariosPacket;
            else
                Console.WriteLine("Recieved unknown packet: " + Packet.GetType());
        }

        public static CollectionPacket GetCollectionPacket()
        {
            SendPacket(Commands.REQUEST_COLLECTION, null);
            CollectionPacket = null;

            while (true)
            {
                if (CollectionPacket != null)
                    return CollectionPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static RequestGuildsPacket GetRequestGuildsPacket()
        {
            SendPacket(Commands.REQUEST_ALLGUILDINFO, null);
            RequestGuildsPacket = null;

            while (true)
            {
                if (RequestGuildsPacket != null)
                    return RequestGuildsPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static GuildInfoPacket GetGuildInfoPacket()
        {
            SendPacket(Commands.REQUEST_GUILDINFO, null);
            GuildInfoPacket = null;

            while (true)
            {
                if (GuildInfoPacket != null)
                    return GuildInfoPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static MatchPacket GetMatchPacket(string MatchId)
        {
            SendPacket(Commands.REQUEST_MATCH, new CommandPacket(new string[] { MatchId }));
            MatchPacket = null;

            while (true)
            {
                if (MatchPacket != null)
                    return MatchPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static UserInfoPacket GetUserInfoPacket(string Username)
        {
            SendPacket(Commands.REQUEST_USERINFO, new CommandPacket(new string[] { Username }));
            UserInfoPacket = null;

            while (true)
            {
                if (UserInfoPacket != null)
                    return UserInfoPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static FriendsIgnoreListPacket GetFriendsIgnoreListPacket()
        {
            SendPacket(Commands.REQUEST_FRIENDSIGNORELIST, null);
            FriendsIgnoreListPacket = null;

            while (true)
            {
                if (FriendsIgnoreListPacket != null)
                    return FriendsIgnoreListPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static DecksPacket GetDecksPacket()
        {
            SendPacket(Commands.REQUEST_DECK_PACKETS, null);
            DecksPacket = null;

            while (true)
            {
                if (DecksPacket != null)
                    return DecksPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static PostedTradesPacket GetPostedTradesPacket()
        {
            SendPacket(Commands.REQUEST_POSTEDTRADES, null);
            PostedTradesPacket = null;

            while (true)
            {
                if (PostedTradesPacket != null)
                    return PostedTradesPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static LobbyInfoPacket GetLobbyInfoPacket()
        {
            SendPacket(Commands.REQUEST_LOBBYINFO, null);
            LobbyInfoPacket = null;

            while (true)
            {
                if (LobbyInfoPacket != null)
                    return LobbyInfoPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }

        public static ScenariosPacket GetScenariosPacket()
        {
            SendPacket(Commands.REQUEST_SCENARIOS, null);
            ScenariosPacket = null;

            while (true)
            {
                if (ScenariosPacket != null)
                    return ScenariosPacket;
                Thread.Sleep(SLEEP_TIME);
            }
        }
    }
}
