﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Resources;
using System.Drawing;
using System.Windows.Forms;
using TradingCardGame.services.sound;

namespace TradingCardGame.Src.Services.Animation
{
    public class ButtonAnimation
    {
        public static void MouseDownClose(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("closebutton_down");
        }

        public static void MouseEnterClose(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("closebutton_over");
        }

        public static void MouseLeaveClose(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("closebutton");
        }

        public static void MouseDownHelp(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("helpbutton_down");
        }

        public static void MouseEnterHelp(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("helpbutton_over");
        }

        public static void MouseLeaveHelp(Button button)
        {
            button.BackgroundImage = ResourcesLib.GetImageFromButtons("helpbutton");
        }

        public static void ToggleCheckBox(Button CheckBox, bool Toggled)
        {
            CheckBox.BackgroundImage = ResourcesLib.GetImageFromButtons("checkbox_" + (Toggled ? "on" : "off"));
        }

        public static void ToggleDropDownCheckBox(Button DropDown, bool Toggled)
        {
            DropDown.Image = ResourcesLib.GetImageFromButtons("dropdown_checkbox_" + (Toggled ? "on" : "off"));
        }
        public static void ToggleRadioButton(RadioButton RadioButton, bool Toggled)
        {
            RadioButton.ImageIndex = Toggled ? RADIO_ON_INDEX : RADIO_OFF_INDEX;
        }


        public static void FormatRadioButton(RadioButton RadioButton)
        {
            ImageList ImageList = new ImageList();
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("radiobutton_off"));
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("radiobutton_off_disabled"));
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("radiobutton_on"));
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("radiobutton_on_disabled"));

            RadioButton.ImageList = ImageList;
            RadioButton.ImageIndex = 0;

            // Add checked change event handler
            RadioButton.CheckedChanged += RadioButton_CheckedChanged;

            // Set Style
            RadioButton.Appearance = Appearance.Button;

            RadioButton.ForeColor = Color.LightSkyBlue;
            RadioButton.BackColor = Color.Transparent;

            RadioButton.FlatStyle = FlatStyle.Flat;
            RadioButton.FlatAppearance.BorderSize = 0;
            RadioButton.FlatAppearance.CheckedBackColor = Color.Transparent;
            RadioButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            RadioButton.FlatAppearance.MouseOverBackColor = Color.Transparent;

            RadioButton.TextImageRelation = TextImageRelation.ImageBeforeText;
        }

        private const int RADIO_OFF_INDEX = 0;
        private const int RADIO_ON_INDEX = 2;
        private static void RadioButton_CheckedChanged(object sender, System.EventArgs e)
        {
            RadioButton RadioButton = (RadioButton)sender;

            SoundService.PlaySound("button");
            RadioButton.ImageIndex = (RadioButton.Checked ? RADIO_ON_INDEX : RADIO_OFF_INDEX);
        }

        public static void FormatButton(Button Button)
        {
            // Set background image
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg");
            Button.BackgroundImageLayout = ImageLayout.Stretch;

            Button.FlatStyle = FlatStyle.Flat;
            Button.FlatAppearance.BorderSize = 0;
            Button.FlatAppearance.CheckedBackColor = Color.Transparent;
            Button.FlatAppearance.MouseDownBackColor = Color.Transparent;
            Button.FlatAppearance.MouseOverBackColor = Color.Transparent;

            // Set event handlers
            Button.MouseDown += Button_MouseDown;
            Button.MouseEnter += Button_MouseEnter;
            Button.MouseLeave += Button_MouseLeave;

            // MouseUp animation is the same as MouseEnter
            Button.MouseUp += Button_MouseEnter;
        }

        public static void DisableButton(Button Button)
        {
            // Disable the button
            Button.Enabled = false;

            // Set the background image
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg_disabled");
        }

        public static void EnableButton(Button Button)
        {
            // Enable the button
            Button.Enabled = true;

            // Set the background image
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg");
        }

        private static void Button_MouseLeave(object sender, System.EventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg");
        }

        private static void Button_MouseDown(object sender, MouseEventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg_down");
        }

        private static void Button_MouseEnter(object sender, System.EventArgs e)
        {
            Button Button = (Button)sender;
            Button.BackgroundImage = ResourcesLib.GetImageFromButtons("button_lg_over");
        }

        public static void FormatCheckBox(CheckBox CheckBox)
        {
            ImageList ImageList = new ImageList();
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("checkbox_off"));
            ImageList.Images.Add(ResourcesLib.GetImageFromButtons("checkbox_on"));

            CheckBox.ImageList = ImageList;
            CheckBox.ImageIndex = 0;

            // Add checked change event handler
            CheckBox.CheckedChanged += CheckBox_CheckedChanged;

            // Set Style
            CheckBox.Appearance = Appearance.Button;

            CheckBox.ForeColor = Color.LightSkyBlue;
            CheckBox.BackColor = Color.Transparent;

            CheckBox.FlatStyle = FlatStyle.Flat;
            CheckBox.FlatAppearance.BorderSize = 0;
            CheckBox.FlatAppearance.CheckedBackColor = Color.Transparent;
            CheckBox.FlatAppearance.MouseDownBackColor = Color.Transparent;
            CheckBox.FlatAppearance.MouseOverBackColor = Color.Transparent;

            CheckBox.TextImageRelation = TextImageRelation.TextBeforeImage;
        }

        private const int OFF_IMAGE_INDEX = 0;
        private const int ON_IMAGE_INDEX = 1;
        private static void CheckBox_CheckedChanged(object sender, System.EventArgs e)
        {
            CheckBox CheckBox = (CheckBox)sender;

            SoundService.PlaySound("button");
            CheckBox.ImageIndex = (CheckBox.Checked ? ON_IMAGE_INDEX : OFF_IMAGE_INDEX);
        }
    }
}
