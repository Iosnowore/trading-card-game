﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData;
using TCGData.Packets;

namespace TradingCardGame.Src.Services.Collection
{
    public class CollectionService
    {
        public static int[] GetCardIdsFromSearch(string[] cardTitles, string[] cardDescriptions, string searchTerm)
        {
            List<int> cards = new List<int>();
            // TODO: Integrate search options
            for (int i = 0; i < cardTitles.Length; i++)
                if (cardTitles[i].ToLower().Contains(searchTerm.ToLower()) || cardDescriptions[i].ToLower().Contains(searchTerm.ToLower()))
                    cards.Add(i);
            return cards.ToArray();
        }

        public static int GetNumberOfOwnedCards(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> Cards = CollectionPacket.GetCards();

            int TotalQuantity = 0;

            foreach (int Id in Cards.Keys)
                TotalQuantity += Cards[Id];

            return TotalQuantity;
        }

        public static int GetNumberOfSpecficCard(int CardId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> Cards = Collection.GetCards();

            Cards.TryGetValue(CardId, out int Quantity);
            return Quantity;
        }

        public static int GetNumberOfCardsWanted(int CardId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> CardWants = Collection.GetCardWants();

            CardWants.TryGetValue(CardId, out int Wants);
            return Wants;
        }

        public static int GetNumberOfSpecficPack(int PackId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> Packs = Collection.GetPacks();

            Packs.TryGetValue(PackId, out int Quantity);
            return Quantity;
        }

        public static int GetNumberOfSpecficPromoPack(int PackId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> PromoPacks = Collection.GetPromoPacks();

            PromoPacks.TryGetValue(PackId, out int Quantity);
            return Quantity;
        }

        public static int GetNumberOfSpecficRedeemable(int RedeemableId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> Redeemables = Collection.GetRedeemables();

            Redeemables.TryGetValue(RedeemableId, out int Quantity);
            return Quantity;
        }

        public static int GetNumberOfSpecficRedeemed(int RedeemableId, CollectionPacket Collection)
        {
            SortedDictionary<int, int> Redeemed = Collection.GetRedeemed();

            Redeemed.TryGetValue(RedeemableId, out int Quantity);
            return Quantity;
        }

        public static int[] GetOwnedCardIds(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> Cards = CollectionPacket.GetCards();
            int[] CardIds = new int[Cards.Count];
            Cards.Keys.CopyTo(CardIds, 0);
            return CardIds;
        }

        public static int[] GetRedeemedCardIds(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> Cards = CollectionPacket.GetRedeemed();
            int[] CardIds = new int[Cards.Count];
            Cards.Keys.CopyTo(CardIds, 0);
            return CardIds;
        }

        public static int[] GetOwnedRedeemableIds(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> Cards = CollectionPacket.GetRedeemables();
            int[] CardIds = new int[Cards.Count];
            Cards.Keys.CopyTo(CardIds, 0);
            return CardIds;
        }

        public static int[] GetOwnedPackIds(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> Packs = CollectionPacket.GetPacks();
            int[] PackIds = new int[Packs.Count];
            Packs.Keys.CopyTo(PackIds, 0);
            return PackIds;
        }

        public static int[] GetOwnedPromoPackIds(CollectionPacket CollectionPacket)
        {
            SortedDictionary<int, int> PromoPacks = CollectionPacket.GetPromoPacks();
            int[] PromoPackIds = new int[PromoPacks.Count];
            PromoPacks.Keys.CopyTo(PromoPackIds, 0);
            return PromoPackIds;
        }

        public static CardData GetAvatarData(string Title, CollectionPacket Collection)
        {
            for (int i = 0; i < Collection.GetAvatars().Count; i++)
                if (Collection.GetAvatars()[i].GetTitle().Equals(Title))
                    return Collection.GetAvatars()[i];
            return null;
        }
    }
}
