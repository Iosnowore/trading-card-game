﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.forms;
using TradingCardGame.services.command;
using TradingCardGame.services.packet;
using TradingCardGame.Src.Services.Player;

namespace TradingCardGame.Src.Services.Chat
{
    public class ChatService
    {
        private const int MAX_CHAR_PER_LINE = 112;

        public static void SendLobbyChatMessage(string message)
        {
            PacketService.SendBytes(new PacketSender(PacketService.GetId(), Commands.LOBBY_CHAT, new CommandPacket(new string[] { PlayerService.GetUsername(), message })).ToBytes());
        }

        public static void SendMatchChatMessage(string message)
        {
            PacketService.SendBytes(new PacketSender(PacketService.GetId(), Commands.MATCH_CHAT, new CommandPacket(new string[] { PlayerService.GetUsername(), message })).ToBytes());
        }

        public static void SendGuildChatMessage(string message)
        {
            PacketService.SendBytes(new PacketSender(PacketService.GetId(), Commands.GUILD_CHAT, new CommandPacket(new string[] { PlayerService.GetUsername(), message })).ToBytes());
        }

        public static string[] GetChatBoxLines(string String)
        {
            List<string> ChatBoxLines = new List<string>();

            string[] Words = String.Split(' ');

            string Line = string.Empty;
            foreach (string Word in Words)
            {
                if (Line.Length + Word.Length <= MAX_CHAR_PER_LINE)
                {
                    Line += Word;

                    if (!Word.Equals(Words[Words.Length - 1]))
                    {
                        Line += " ";
                    }
                    else
                    {
                        ChatBoxLines.Add(Line);
                    }
                }
                else
                {
                    ChatBoxLines.Add(Line);
                    Line = Word + " ";
                }
            }
            return ChatBoxLines.ToArray();
        }

        public static void ChatBoxKeyEnter(TextBox inputChatBox, ListBox chatBox, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;

            if (!string.IsNullOrWhiteSpace(inputChatBox.Text))
            {
                if (inputChatBox.Text[0].Equals('/'))
                    CommandService.Command(inputChatBox.Text.Substring(1), chatBox);
                else
                {
                    if (inputChatBox.Parent is LaunchMatch)
                    {
                        SendMatchChatMessage(inputChatBox.Text);
                    }
                    else
                    {
                        SendLobbyChatMessage(inputChatBox.Text);
                    }
                }
            }
            inputChatBox.Text = string.Empty;
        }
    }
}
