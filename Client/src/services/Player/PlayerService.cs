﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Windows.Forms;
using TCGData;
using TCGData.Packets.Command;
using TradingCardGame.services.packet;

namespace TradingCardGame.Src.Services.Player
{
    public class PlayerService
    {
        private static ListViewItem[] leaderboardItems;

        private static string Username;

        public static void SetUsername(string Username)
        {
            PlayerService.Username = Username;
        }

        public static string GetUsername()
        {
            return Username;
        }

        public static void SetLeaderboardItems(SortedDictionary<string, int> LeaderboardInfo)
        {
            string[] Usernames = new string[LeaderboardInfo.Count];
            LeaderboardInfo.Keys.CopyTo(Usernames, 0);

            int[] Ratings = new int[LeaderboardInfo.Count];
            LeaderboardInfo.Values.CopyTo(Ratings, 0);

            // Put leaderboard info in order
            int tmpRating;
            string tmpUser;

            for (int i = 0; i < LeaderboardInfo.Count - 1; i++)
            {
                if (Ratings[i] < Ratings[i + 1])
                {
                    tmpRating = Ratings[i];
                    tmpUser = Usernames[i];

                    Ratings[i] = Ratings[i + 1];
                    Usernames[i] = Usernames[i + 1];
                    Ratings[i + 1] = tmpRating;
                    Usernames[i + 1] = tmpUser;
                }
            }

            leaderboardItems = new ListViewItem[LeaderboardInfo.Count];

            for (int i = 0; i < leaderboardItems.Length; i++)
            {
                leaderboardItems[i] = new ListViewItem(new string[] { (i + 1).ToString(), Ratings[i].ToString(), Usernames[i] });
            }
        }

        public static ListViewItem[] GetLeaderboardItems()
        {
            return leaderboardItems;
        }

        public static void AddFriendCommand(string Player)
        {
            PacketService.SendPacket(Commands.FRIEND_ADD, new CommandPacket(new string[] { Player }));
        }

        public static void RemoveFriendCommand(string Player)
        {
            PacketService.SendPacket(Commands.FRIEND_REMOVE, new CommandPacket(new string[] { Player }));
        }

        public static void IgnorePlayerCommand(string Player)
        {
            PacketService.SendPacket(Commands.IGNORE_PLAYER, new CommandPacket(new string[] { Player }));
        }

        public static void UnignorePlayerCommand(string Player)
        {
            PacketService.SendPacket(Commands.UNIGNORE_PLAYER, new CommandPacket(new string[] { Player }));
        }
    }
}
