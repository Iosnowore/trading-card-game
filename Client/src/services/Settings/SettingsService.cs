﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.IO;
using System.Xml;
using TCGData;

namespace TradingCardGame.Src.Services.Settings
{
    public class SettingsService
    {
        private const string SETTINGS_FILENAME = "settings.xml";
        private static readonly XmlDocument XmlDoc = new XmlDocument();

        public static void CreateSettingsXmlIfDoesntExist()
        {
            if (!File.Exists(SETTINGS_FILENAME))
            {
                File.WriteAllText(SETTINGS_FILENAME, GameData.GetSettingsString());
            }
            XmlDoc.Load(SETTINGS_FILENAME);
        }

        public static void SetValueInSettingsXml(string Title, string Setting, string NewValue)
        {
            XmlDoc.SelectSingleNode("Settings/" + Title + "/" + Setting).InnerText = NewValue;
            XmlDoc.Save(SETTINGS_FILENAME);
        }

        public static void SetValueInSettingsXml(string Title, string Setting, bool NewValue)
        {
            XmlDoc.SelectSingleNode("Settings/" + Title + "/" + Setting).InnerText = NewValue.ToString();
            XmlDoc.Save(SETTINGS_FILENAME);
        }

        public static string GetStringFromSettingsXml(string Title, string Setting)
        {
            return XmlDoc.SelectSingleNode("Settings/" + Title + "/" + Setting).InnerText;
        }

        public static bool GetBoolFromSettingsXml(string Title, string Setting)
        {
            return GetStringFromSettingsXml(Title, Setting).Equals("True");
        }

        public static int GetIntFromSettingsXml(string Title, string Setting)
        {
            string StringValue = GetStringFromSettingsXml(Title, Setting);
            return int.Parse(StringValue);
        }
    }
}
