﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace TradingCardGame.Src.Custom
{
    public class SizeableBackgroundPanel : Panel
    {
        private readonly int X;
        private readonly int Y;
        private new readonly int Width;
        private new readonly int Height;

        private new readonly Image BackgroundImage;

        public SizeableBackgroundPanel(int X, int Y, int Width, int Height, Image BackgroundImage)
        {
            this.X = X;
            this.Y = Y;
            this.Width = Width;
            this.Height = Height;

            this.BackgroundImage = BackgroundImage;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (BackgroundImage == null)
            {
                Console.WriteLine("BackgroundImage == null, X: " + Location.X + ", Y: " + Location.Y);
                return;
            }

            base.OnPaintBackground(e);
            Rectangle rc = new Rectangle(X, Y, Width, Height);
            e.Graphics.DrawImage(BackgroundImage, rc);
            BackgroundImageLayout = ImageLayout.None;
        }
    }
}
