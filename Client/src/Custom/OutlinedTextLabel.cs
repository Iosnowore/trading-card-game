﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace TradingCardGame.Src.Custom
{
    public class OutlinedTextLabel : Label
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(BackColor), ClientRectangle);

            using (GraphicsPath gp = new GraphicsPath())
            {
                using (Pen outline = new Pen(Color.Black, 2)
                { LineJoin = LineJoin.Round })

                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;

                    using (Brush foreBrush = new SolidBrush(ForeColor))
                    {
                        // We will want to draw the text in a specific location
                        Rectangle Rec = new Rectangle(ClientRectangle.Location, ClientRectangle.Size);
                        Rec.Height -= (int)(Font.Size / 2);

                        gp.AddString(Text, Font.FontFamily, (int)Font.Style, Font.Size, Rec, sf);
                        e.Graphics.ScaleTransform(1.3f, 1.35f);
                        e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                        e.Graphics.DrawPath(outline, gp);
                        e.Graphics.FillPath(foreBrush, gp);
                    }
                }
            }
        }
    }
}
