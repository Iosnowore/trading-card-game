﻿using System.Collections.Generic;

namespace TradingCardGame.src.resources.control
{
    class Manager : Service
    {
        private readonly List<Service> children;

        public Manager()
        {
            children = new List<Service>();
        }

        protected void AddChildObject(Service s)
        {
            children.Add(s);
        }

        protected void RemoveChildObject(Service s)
        {
            children.Remove(s);
        }
    }
}
