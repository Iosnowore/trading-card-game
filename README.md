# README #

This README contains the steps to download, compile, and run the TCG.

### What does this repository contain? ###

#### Executibles
* TCG Client (C# .NET 4.8)
* TCG Server (C# .NET Core 3.1)

#### Dlls
* Cards.dll (C# .NET 4.8) - Client
#####Contains all game card images.

* Custom.dll (C# .NET 4.8) - Client
##### Contains all custom game images that were not from the live client.

* Effects.dll (C# .NET 4.8) - Client
##### Contains all game effect images.

* Sounds.dll (C# .NET 4.8) - Client
##### Contains all game sound files.

* String.dll (C# .NET 4.8) - Client
##### Contains all game strings.

* TCGData.dll (C# .NET 4.8) - Client & Server
##### Contains game datatables and important game information used by the client and server.

* SWGTCG.dll (C++) - Client
##### Works with the SWG client to load the TCG client

#### Third Party Libraries
###### Client Libraries
* [NAudio and NAudio.Vorbis](https://naudio.codeplex.com) - Used by to control game sound.
* [Discord Rich Presence](https://github.com/Lachee/discord-rpc-csharp) - Used to integrate with Discord.

###### Server Libraries
* MySQL.Data - Used to work with the database (for MySQL), already included in VS NuGet.
* SQLite.Core - Used to work with the database (for SQLite), already included in VS NuGet.

### How do I get set up? ###

## Downloads

#### Required
* [Visual Studio 2019](https://www.visualstudio.com/vs/community)
* [SourceTree](https://www.sourcetreeapp.com)
* [Client Resources](http://www.mediafire.com/file/bf4s10d7b8ftajj/Resources.zip)

#### Recommended
* [Notepad++](https://notepad-plus-plus.org)
* Microsoft Excel

## Getting the Source
* Fork the original TCG repository.
* Open SourceTree and clone the newly forked repository.
* Unzip the Client Resources into the Resources folder under the Client folder.

## Opening the Source
* Run Visual Studio
* Open Trading Card Game.sln

## Compiling the Source
* Select the *Trading Card Game* solution in the dropdown menu at the top of Visual Studio.
* Choose the compile option *debug* and *Any CPU* in the other dropdown menus, if they're not already preset to those.

## Running the Source

### Server Setup

### Database Creation
* Using either MySQL or SQLite, create a database using the schema provided in ROOT/Server/db/tcg.sql

### Server Running
* Run the server to create a config file and close it.
* Fill out the config information.

### Client Running
* Edit the client PacketService to match the IP and port of the server.

### Contribution guidelines ###
* Test your code before you make a PR.
* Don't make any major refactor changes (unless given permission).