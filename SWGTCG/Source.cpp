#include <Windows.h>
#include <string>
#include <Shlwapi.h>
#include <vector>

#include <tchar.h>
#include <windows.h>

#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "user32.lib")

std::wstring GetThisDllDirectory(HMODULE hMod, bool addTrailingSlash = true)
{
	std::wstring sPath = TEXT("");
	wchar_t wszPath[MAX_PATH];

	GetModuleFileNameW(hMod, wszPath, sizeof(wszPath));
	PathRemoveFileSpecW(wszPath);
	sPath = wszPath;
	if (sPath.length() <= 0)
		return std::wstring(TEXT(""));
	if (addTrailingSlash)
		sPath += TEXT("\\");
	return sPath;
}

bool LaunchProcess(const std::wstring& fileName)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	if (CreateProcess(fileName.c_str(), nullptr, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &si, &pi))
		return true;
	return false;
}

void WriteBytes(unsigned int addr, const std::vector<unsigned char>& bytes)
{
	unsigned int dwBack;
	size_t numberOfBytes = bytes.size();
	int offset = 0;
	if (addr != NULL && numberOfBytes > 0) {
		VirtualProtect((void*)addr, numberOfBytes, PAGE_EXECUTE_READWRITE, (PDWORD)&dwBack);
		for each (unsigned char value in bytes) {
			*(unsigned char*)(addr + offset) = value;
			offset++;
		}
		VirtualProtect((void*)addr, numberOfBytes, dwBack, (PDWORD)&dwBack);
	}
}


//#####################################################
// Duuuurty hack for now until we have true emulation.
// disabled the error message that the client throws.
//
// NOTE: we cant unpatch this at DLL_UNLOAD due to
// the client throwing the error post-freelibrary
//#####################################################

static unsigned char origA[3];
static unsigned char origW[3];

void PatchMessageBox(bool apply)
{
	DWORD dwMsgBoxAddress;
	DWORD dwMsgBoxWideAddress;

	__asm mov eax, MessageBoxA;
	__asm mov dwMsgBoxAddress, eax;
	__asm mov eax, MessageBoxW;
	__asm mov dwMsgBoxWideAddress, eax;

	if (dwMsgBoxAddress == 0 || dwMsgBoxWideAddress == 0) {
		return;
	}

	if (apply)
	{
		memcpy(origA, (void*)dwMsgBoxAddress, 3);
		memcpy(origW, (void*)dwMsgBoxWideAddress, 3);

		WriteBytes(dwMsgBoxAddress, { 0xC2, 0x10, 0x00 });
		WriteBytes(dwMsgBoxWideAddress, { 0xC2, 0x10, 0x00 });
	}
	else
	{
		WriteBytes(dwMsgBoxAddress, { origA[0], origA[1], origA[2] });
		WriteBytes(dwMsgBoxWideAddress, { origW[0], origW[1], origW[2] });
	}
}

int __stdcall DllMain(HMODULE moduleHandle, DWORD eventType, LPVOID reserved) {

	if (eventType == DLL_PROCESS_ATTACH)
	{
		DisableThreadLibraryCalls(moduleHandle);

		if (reserved == nullptr)
		{
			PatchMessageBox(true);
			std::wstring clientPath = GetThisDllDirectory(moduleHandle) + std::wstring(TEXT("SWGTCG.exe"));
			if (!LaunchProcess(clientPath)) {
				std::wstring message = TEXT("Unable to launch the TCG client!\n") + clientPath;
				MessageBox(0, message.c_str(), TEXT("Star Wars Galaxies TCG"), 0);
			}
		}
		else
		{
			return 0;
		}
	}
	else if (eventType == DLL_PROCESS_DETACH)
	{
		//PatchMessageBox(false); // DISABLED: it frees us before throwing the messagebox error so this would be useless.
	}

	return 1;
}


/*

//######################################################################
// TODO: Emulate the true functionality of the SWGTCG original library.
//######################################################################

struct CallbackTable
{
void*			navigateProc;
void*			navigateWithPostDataProc;
void*			playSoundProc;
void*			playMusicProc;
void*			setSoundVolumeProc;
void*			setMusicVolumeProc;
void*			stopAllSoundsProc;
void*			setWindowStateProc;
};

extern "C"
{
__declspec (dllexport) void Initialize(int argc, const char *argv[], HWND hwndDesktop, CallbackTable *);
__declspec (dllexport) void RunFrame();
__declspec (dllexport) void Shutdown();
__declspec (dllexport) unsigned GetWindows(unsigned *pWindowsIds, unsigned uNumWindowIds);
__declspec (dllexport) unsigned GetCaptureWindow();
__declspec (dllexport) HCURSOR GetCurrentCursor();
__declspec (dllexport) unsigned GetWindowRepaintRects(unsigned uWindowId, RECT *pRects, unsigned uNumRects);
__declspec (dllexport) unsigned GetWindowSurfaceData();
__declspec (dllexport) void OnMouseEvent(unsigned uWindowId, int eventType, int x, int y, int gx, int gy, int mouseButton, int mouseState, int keyboardState);
__declspec (dllexport) void OnMouseWheelEvent(unsigned uWindowId, int x, int y, int gx, int gy, int delta, int mouseState, int keyboardState);
__declspec (dllexport) unsigned OnKeyEvent(int eventType, int key, int keyboardState, int code, int vkey, int nativeMods);
__declspec (dllexport) void OnFocus(unsigned uWindowId, int focus);
__declspec (dllexport) void OnWindowStateChanged(int);
__declspec (dllexport) void OnMusicCompletion();
}

void Initialize(int argc, const char *argv[], HWND hwndDesktop, CallbackTable *)
{

};

void RunFrame()
{

};

void Shutdown()
{

};

unsigned GetWindows(unsigned *pWindowsIds, unsigned uNumWindowIds)
{
return 0;
};

unsigned GetCaptureWindow()
{
return 0;
};

HCURSOR GetCurrentCursor()
{
return NULL;
};

unsigned GetWindowRepaintRects(unsigned uWindowId, RECT *pRects, unsigned uNumRects)
{
return 0;
};

unsigned GetWindowSurfaceData()
{
return 0;
};

void OnMouseEvent(unsigned uWindowId, int eventType, int x, int y, int gx, int gy, int mouseButton, int mouseState, int keyboardState)
{

};

void OnMouseWheelEvent(unsigned uWindowId, int x, int y, int gx, int gy, int delta, int mouseState, int keyboardState)
{

};

unsigned OnKeyEvent(int eventType, int key, int keyboardState, int code, int vkey, int nativeMods)
{
return 0;
};

void OnFocus(unsigned uWindowId, int focus)
{

};

void OnWindowStateChanged(int)
{

};

void OnMusicCompletion()
{

};
*/