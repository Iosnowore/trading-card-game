﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Net;
using System.Net.Sockets;
using System.Threading;
using TCGData;

namespace Server.src.services.Packet
{
    public class TCPConnection
    {
        private readonly Socket Socket;

        public TCPConnection(string Address, int Port)
        {
            Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket.Bind(new IPEndPoint(IPAddress.Parse(Address), Port));
        }

        public void StartConnection()
        {
            new Thread(ListenThread).Start();
        }

        private void ListenThread()
        {
            while (true)
            {
                Socket.Listen(0);
                Server.playerSrv.AddOnlinePlayer(new ClientData(Socket.Accept(), this));
            }
        }

        public void Data_IN(object cSocket)
        {
            Socket clientSocket = (Socket)cSocket;

            byte[] Buffer;
            int readBytes;

            try
            {
                while (true)
                {
                    Buffer = new byte[clientSocket.SendBufferSize];
                    readBytes = clientSocket.Receive(Buffer);

                    if (readBytes > 0)
                        Server.conSrv.DataManager(new PacketSender(Buffer));
                }
            }
            catch (SocketException e)
            {
                Server.logSrv.LogDebug("TCPConnection", "DATA_IN Exception: " + e.Message);
                //clientSrv.DisconnectClient(conSrv.GetClientByID(new PacketSender(Buffer).GetSenderId()));
            }
        }
    }
}
