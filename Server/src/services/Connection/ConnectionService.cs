﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Packet;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using TCGData;
using TCGData.Packets.Command;

namespace Server.src.services.Connection
{
    public class ConnectionService : Server
    {
        public readonly int SERVER_PORT;
        public readonly int SERVER_PROTOCOL;

        public const int PROTOCOL_TCP = 1;
        public const int PROTOCOL_UDP = 2;

        private readonly TCPConnection TCPConnection;
        private readonly UDPConnection UDPConnection;

        public ConnectionService()
        {
            string Port = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "SERVER_PORT");
            string Protocol = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "SERVER_PROTOCOL");

            if (string.IsNullOrEmpty(Port))
            {
                logSrv.LogError("ConnectionService", "Port is empty");
                return;
            }

            if (string.IsNullOrEmpty(Protocol))
            {
                logSrv.LogError("ConnectionService", "Network protocol is empty");
                return;
            }

            SERVER_PORT = int.Parse(Port);
            SERVER_PROTOCOL = int.Parse(Protocol);

            string Address = GetIP4Address();

            switch (SERVER_PROTOCOL)
            {
                case PROTOCOL_TCP:
                    TCPConnection = new TCPConnection(Address, SERVER_PORT);
                    break;
                case PROTOCOL_UDP:
                    UDPConnection = new UDPConnection(Address, SERVER_PORT);
                    break;
                default:
                    logSrv.LogError("ConnectionService", "SERVER_PROTOCOL is invalid");
                    break;
            }
            logSrv.LogDebug("ConnectionService", "Accepting connections on " + Address + ":" + SERVER_PORT);
            logSrv.Log("ConnectionService", "Loaded");
        }

        public void StartConnection()
        {
            if (TCPConnection != null)
                TCPConnection.StartConnection();
            else if (UDPConnection != null)
                UDPConnection.StartConnection();
            else
                logSrv.LogError("ConnectionService", "No connection found");
        }

        private static string GetIP4Address()
        {
            IPAddress[] ips = Dns.GetHostAddresses(Dns.GetHostName());

            return (from ip in ips
                    where ip.AddressFamily == AddressFamily.InterNetwork
                    select ip)
                   .FirstOrDefault()
                   .ToString();
        }

        public ClientData GetClientByID(int Id)
        {
            foreach (ClientData client in playerSrv.GetOnlinePlayers())
            {
                if (client != null && client.GetId() == Id)
                    return client;
            }
            return null;
        }

        public void SendPacketToClient(PacketSender PacketSender, ClientData Client)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    new BinaryFormatter().Serialize(ms, PacketSender);
                    ms.Flush();
                    byte[] bytes = ms.ToArray();
                    Client.SendBytes(bytes);
                }
                   
                logSrv.LogDebug("ConnectionService", "Sent packet to client - Command: " + PacketSender.GetCommand() + " to Client: " + Client.GetId());
            }
            catch (SocketException e)
            {
                // Keep the try/catch method because we never know what the client user will do
                logSrv.LogWarning("ConnectionService", "SendPacketToClient Exception: " + e.Message);
                clientSrv.DisconnectClient(conSrv.GetClientByID(Client.GetId()));
            }
        }

        public void DataManager(PacketSender PacketSender)
        {
            logSrv.LogDebug("ConnectionService", "Got packet (" + PacketSender.GetCommand() + ") from ClientId " + PacketSender.GetSenderId());

            if (PacketSender.GetPacket() == null)
            {
                packetSrv.HandleEmptyPacket(PacketSender);
            }
            else if (PacketSender.GetPacket() is CommandPacket)
            {
                packetSrv.HandleCommandPacket(PacketSender);
            }
            else
            {
                packetSrv.HandleUnknownPacket(PacketSender);
            }
        }

        public void SendPacketToClients(PacketSender p, ClientData[] Clients)
        {
            foreach (ClientData Client in Clients)
                if (Client != null)
                    SendPacketToClient(p, Client);
        }
    }
}
