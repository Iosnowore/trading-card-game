﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Net;
using System.Net.Sockets;
using TCGData;

namespace Server.src.services.Packet
{
    public class UDPConnection
    {
        private readonly Socket Socket;

        private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
        private AsyncCallback recv = null;

        public UDPConnection(string Address, int Port)
        {
            Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            Socket.Bind(new IPEndPoint(IPAddress.Parse(Address), Port));
        }

        public void StartConnection()
        {
            Receive();
        }

        private void Receive()
        {
            byte[] Buffer = new byte[Socket.ReceiveBufferSize];

            try
            {
                Socket.BeginReceiveFrom(Buffer, 0, Buffer.Length, SocketFlags.None, ref epFrom, recv = (ar) =>
                {
                    Buffer = new byte[Socket.ReceiveBufferSize];
                    byte[] so = (byte[])ar.AsyncState;
                    int bytes = Socket.EndReceiveFrom(ar, ref epFrom);
                    Socket.BeginReceiveFrom(so, 0, so.Length, SocketFlags.None, ref epFrom, recv, so);

                    if (bytes > 0)
                    {
                        PacketSender PacketSender = new PacketSender(so);

                        // Since we are not using TCP, we need to check if this is the first connection, if so, add the client
                        if (PacketSender.GetCommand() == Commands.LOGIN_VALIDATION)
                        {
                            // probably could just use epFrom.ToString().
                            // Optimize everything once we actually get it working
                            IPEndPoint IPEndPoint = (IPEndPoint)epFrom;
                            string IpAddress = IPEndPoint.Address.ToString() + ":" + IPEndPoint.Port;
                            Server.logSrv.LogDebug("UDPConnection", "Player connecting to server from source: " + IpAddress);
                            Server.playerSrv.AddOnlinePlayer(new ClientData(Socket, IPEndPoint));
                        }
                        else
                        {
                            if (!Server.playerSrv.PlayerLoggedIn(PacketSender.GetSenderId()))
                            {
                                Server.logSrv.LogWarning("UDPConnection", "Player (" + PacketSender.GetSenderId() + ") attempted to send a packet but hasn't logged in.");
                                return;
                            }
                            Server.logSrv.LogDebug("UDPConnection", "Player (" + PacketSender.GetSenderId() + ") PacketCommand: " + PacketSender.GetCommand());
                        }
                        Server.conSrv.DataManager(PacketSender);
                    }
                }, Buffer);
            }
            catch (Exception e)
            {
                Server.logSrv.LogWarning("UDPConnection", "DATA_IN Exception: " + e.Message);
            }
        }
    }
}
