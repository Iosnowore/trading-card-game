﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Game;
using System;
using System.Collections.Generic;
using TCGData;

namespace Server.src.services.AI
{
    public class AIService : Server
    {
        public AIService()
        {
            logSrv.Log("AIService", "Loaded");
        }

        public bool RedrawHand(SortedDictionary<int, int> Deck, int[] Hand)
        {
            //@TODO: Implement actual AI thinking
            return false;
        }

        public int GetQuestToRaid(QuestObject QuestOne, QuestObject QuestTwo, CombatPlayerObject PlayerOne)
        {
            // Check if we have units at QuestOne to use
            if (QuestOne.GetNumberOfPlayerTwoCards() > 0)
            {
                int NpcAttack = QuestOne.GetPlayerTwoTotalAttack();
                int PlayerDefense = QuestOne.GetPlayerOneTotalDefense(PlayerOne);

                // Attack if the NPC's attack is greater than the player's defense
                if (NpcAttack > PlayerDefense)
                {
                    return QuestOne.GetQuestId();
                }
            }

            // Check if we have units at QuestTwo to use
            if (QuestTwo.GetNumberOfPlayerTwoCards() > 0)
            {
                int NpcAttack = QuestTwo.GetPlayerTwoTotalAttack();
                int PlayerDefense = QuestTwo.GetPlayerOneTotalDefense(PlayerOne);

                // Attack if the NPC's attack is greater than the player's defense
                if (NpcAttack > PlayerDefense)
                {
                    return QuestTwo.GetQuestId();
                }
            }
            return -1;
        }

        public int GetCardIdToDraw(int[] CardIds, int MaxCost, QuestObject QuestOne, QuestObject QuestTwo, RaidObject RaidObject)
        {
            // Card types
            int[] Units = GetPlayableCardIdsForCardType(CardIds, "Unit", MaxCost);
            int[] Abilities = GetPlayableCardIdsForCardType(CardIds, "Ability", MaxCost);
            int[] Items = GetPlayableCardIdsForCardType(CardIds, "Item", MaxCost);
            int[] Tactics = GetPlayableCardIdsForCardType(CardIds, "Tactic", MaxCost);

            //@TODO: Make this AI smarter

            if (Abilities.Length > 0)
            {
                // Check if we need unit backup instead
                if (QuestOne.GetNumberOfPlayerTwoCards() >= QuestOne.GetNumberOfPlayerOneCards() || QuestTwo.GetNumberOfPlayerTwoCards() >= QuestTwo.GetNumberOfPlayerOneCards())
                {
                    return Abilities[0];
                }
            }

            if (Units.Length > 0)
            {
                return Units[0];
            }

            if (Items.Length > 0)
            {
                return Items[0];
            }

            // Can only draw tactic cards during raids
            if (Tactics.Length > 0 && RaidObject != null)
            {
                return Tactics[0];
            }
            return -1;
        }

        private int[] GetPlayableCardIdsForCardType(int[] Hand, string CardType, int MaxCardCost)
        {
            List<int> PlayableCards = new List<int>();

            CardData CardData;
            foreach (int CardId in Hand)
            {
                if (CardId == -1)
                    continue;

                CardData = Cards.GetCardData(CardId);

                if (CardData.GetType().Equals(CardType) && CardData.GetCost() <= MaxCardCost)
                    PlayableCards.Add(CardId);
            }
            return PlayableCards.ToArray();
        }

        public int GetCardIdForDefenseAction(CombatPlayerObject Npc)
        {
            // @TODO: Look through abilities as well

            return Npc.GetAvatarId();
        }

        public int GetQuestToPlaceUnit(int CardId, QuestObject QuestOne, QuestObject QuestTwo)
        {
            CardData CardData = Cards.GetCardData(CardId);

            // It only matters if we are placing a unit
            if (CardData.GetType().Equals("Unit"))
            {
                int NpcQuestOneCards = QuestOne.GetNumberOfPlayerTwoCards();
                int NpcQuestTwoCards = QuestTwo.GetNumberOfPlayerTwoCards();

                int PlayerQuestOneCards = QuestOne.GetNumberOfPlayerOneCards();
                int PlayerQuestTwoCards = QuestTwo.GetNumberOfPlayerOneCards();

                logSrv.LogDebug("AIService", "NpcQuestOneCards: " + NpcQuestOneCards);
                logSrv.LogDebug("AIService", "NpcQuestTwoCards: " + NpcQuestTwoCards);
                logSrv.LogDebug("AIService", "PlayerQuestOneCards: " + PlayerQuestOneCards);
                logSrv.LogDebug("AIService", "PlayerQuestTwoCards: " + PlayerQuestTwoCards);

                if (PlayerQuestOneCards > NpcQuestOneCards)
                {
                    logSrv.LogDebug("AIService", "PlayerQuestOneCards > NpcQuestOneCards");
                    return QuestOne.GetQuestId();
                }

                if (PlayerQuestTwoCards > NpcQuestTwoCards)
                {
                    logSrv.LogDebug("AIService", "PlayerQuestTwoCards > NpcQuestTwoCards");
                    return QuestTwo.GetQuestId();
                }

            }
            // Generate random side
            logSrv.LogDebug("AIService", "Returning random side");
            bool ChooseQuestOne = new Random().Next(0, 1) == 0;
            return ChooseQuestOne ? QuestOne.GetQuestId() : QuestTwo.GetQuestId();
        }

        public int GetCardToDamage(SortedDictionary<int, PlaymatCard> PlaymatCards)
        {
            int HighestHealth = 0;
            int KeyWithHighestHealth = -1;

            PlaymatCard PlaymatCard;
            foreach (int Key in PlaymatCards.Keys)
            {
                PlaymatCard = PlaymatCards[Key];
                int Health = PlaymatCard.GetHealth();

                if (Health > HighestHealth)
                {
                    HighestHealth = Health;
                    KeyWithHighestHealth = Key;
                }
            }

            return KeyWithHighestHealth;
        }

        public int GetAttackCardToExert(SortedDictionary<int, PlaymatCard> NpcCards, int TotalAttackSoFar, int EnemyTotalExertableDefense)
        {
            //@TODO: Make this AI smarter

            // No need to check if we have 0 cards
            if (NpcCards.Count == 0)
                return -1;

            // No need to exert anymore card cards if the defense is less
            if (TotalAttackSoFar > EnemyTotalExertableDefense)
                return -1;

            int HighestAttackCardKey = -1;
            int HighestAttack = 0;

            PlaymatCard PlaymatCard;
            foreach (int Key in NpcCards.Keys)
            {
                PlaymatCard = NpcCards[Key];

                // No need to check if it's exerted
                if (PlaymatCard.GetExerted())
                    continue;

                int Attack = PlaymatCard.GetAttack();

                if (Attack > HighestAttack)
                {
                    // Update the highest card
                    HighestAttackCardKey = Key;
                    HighestAttack = Attack;
                }
            }
            return HighestAttackCardKey;
        }

        public int GetDamageCardToExert(SortedDictionary<int, PlaymatCard> NpcCards)
        {
            //@TODO: Make this AI smarter

            if (NpcCards.Count == 0)
                return -1;

            int HighestDamageCardKey = -1;
            int HighestDamage = 0;

            PlaymatCard PlaymatCard;
            foreach (int Key in NpcCards.Keys)
            {
                PlaymatCard = NpcCards[Key];

                // No need to check if it's exerted
                if (PlaymatCard.GetExerted())
                    continue;

                int Damage = PlaymatCard.GetDamage();

                if (Damage > HighestDamage)
                {
                    // Update the highest card
                    HighestDamageCardKey = Key;
                    HighestDamage = Damage;
                }
            }
            return HighestDamageCardKey;
        }

        public int GetDefenseCardToExert(SortedDictionary<int, PlaymatCard> NpcCards)
        {
            //@TODO: Make this AI smarter
            int HighestDefenseCardKey = -1;
            int HighestDefense = 0;

            PlaymatCard PlaymatCard;
            foreach (int Key in NpcCards.Keys)
            {
                PlaymatCard = NpcCards[Key];

                // No need to check if it's exerted
                if (PlaymatCard.GetExerted())
                    continue;

                int Defense = PlaymatCard.GetDefense();

                if (Defense > HighestDefense)
                {
                    // Update the highest card
                    HighestDefenseCardKey = Key;
                    HighestDefense = Defense;
                }
            }
            return HighestDefenseCardKey;
        }
    }
}
