﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using System.Collections.Generic;
using System.IO;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Services;

namespace Server.src.services.Staff
{
    public class StaffService : Server
    {
        // <PlayerId, Power>
        private readonly SortedDictionary<int, int> STAFF_USERS;

        // {CommandId:Power}, ex. 0:10
        private readonly string[] StaffCommands;

        public StaffService()
        {
            STAFF_USERS = new SortedDictionary<int, int>();

            string StaffFileLoc = cfgSrv.GetConfigValue("features.cfg", "TABLE_DIR") + @"\staff.sdb";

            if (File.Exists(StaffFileLoc))
            {
                int PlayerId;
                using StreamReader reader = new StreamReader(StaffFileLoc);
                string row = reader.ReadLine();

                while ((row = reader.ReadLine()) != null)
                {
                    string[] splitRow = row.Split('\t');
                    PlayerId = playerSrv.GetId(splitRow[0]);
                    logSrv.Log("StaffService", splitRow[0] + " (" + PlayerId + ") added as a staff member (level: " + splitRow[1] + ")");

                    STAFF_USERS.Add(PlayerId, int.Parse(splitRow[1]));
                }
            }
            else
            {
                using StreamWriter writer = new StreamWriter(StaffFileLoc);
                writer.WriteLine("username\tlevel");
                logSrv.LogDebug("StaffService", "Created staff table");
            }

            // Load staff commands and their power requirement
            string[] CommandIndexes = DatatableReader.GetValues("command_powers", "index");
            string[] CommandPowers = DatatableReader.GetValues("command_powers", "index");

            StaffCommands = new string[CommandIndexes.Length];

            for (int i = 0; i < StaffCommands.Length; i++)
            {
                StaffCommands[i] = CommandIndexes[i] + CollectionService.SECONDARY_DELIMITER + CommandPowers[i];
            }
            logSrv.Log("StaffService", "Loaded");
        }

        public void KickPlayer(int PlayerId, string KickReason)
        {
            if (string.IsNullOrEmpty(KickReason))
            {
                playerSrv.SendMessageToPlayer(PlayerId, "Please specify a reason for kicking the player.", "Could Not Kick Player");
                return;
            }
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.KICK_PLAYER, null);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerId));
            clientSrv.DisconnectClient(conSrv.GetClientByID(PlayerId));
            logSrv.Log("StaffService", playerSrv.GetUsername(PlayerId) + " (" + PlayerId + ") used command 'kickplayer' for reason: " + KickReason);
        }

        public void BroadcastMessage(int PlayerId, string Message)
        {
            string PlayerUsername = playerSrv.GetUsername(PlayerId);
            CommandPacket CommandPacket = new CommandPacket(new string[] { Message, PlayerUsername });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.BROADCAST_MESSAGE, CommandPacket);
            conSrv.SendPacketToClients(PacketSender, playerSrv.GetOnlinePlayers());
            logSrv.Log("StaffService", playerSrv.GetUsername(PlayerId) + " (" + PlayerId + ") used command 'broadcast' with arguments " + Message);
        }

        public void MutePlayer(int PlayerId, string TargetUsername, string Duration)
        {
            int TargetId = playerSrv.GetId(TargetUsername);

            if (!playerSrv.IsOnline(TargetId))
            {
                playerSrv.SendMessageToPlayer(PlayerId, "Target player to mute is offline.", "Mute failed");
                return;
            }

            int ActualDuration = -1;

            if (!string.IsNullOrEmpty(Duration))
            {
                int.TryParse(Duration, out ActualDuration);

                if (ActualDuration <= 0)
                {
                    // todo: send message to staff member letting them know the duration is invalid.
                    return;
                }
            }

            string StaffUsername = playerSrv.GetUsername(PlayerId);

            if (chatSrv.PlayerIsMuted(TargetId))
            {
                playerSrv.SendMessageToPlayer(TargetId, "You have been unmuted by " + StaffUsername + ". You are now able to talk in lobbies again.", "Game Muted");
                playerSrv.SendMessageToPlayer(PlayerId, "You have unmuted " + TargetUsername, "Muted Player");
                chatSrv.UnMutePlayer(TargetId);
            }
            else
            {
                playerSrv.SendMessageToPlayer(TargetId, "You have been muted by " + StaffUsername + ". You will not be able to type in any lobby until you are unmuted", "Game Unmuted");
                playerSrv.SendMessageToPlayer(PlayerId, "You have muted " + TargetUsername, "Muted Player");
                chatSrv.MutePlayer(TargetId);
            }
            logSrv.Log("StaffService", StaffUsername + " (" + PlayerId + ") muted " + TargetUsername + " (" + TargetId + ") for " + ActualDuration + " minutes");
        }

        public void SetCardQuantity(int PlayerId, int CardId, int CardQuantity)
        {
            string StaffUsername = playerSrv.GetUsername(PlayerId);

            // Updated card quantity...
            Collection.Collection Collection = collectionSrv.GetCollection(PlayerId);
            int CurrentCardQuantity = Collection.GetNumberOfSpecficCard(CardId);

            SortedDictionary<int, int> Cards = new SortedDictionary<int, int>();

            if (CardQuantity > CurrentCardQuantity)
            {
                int NumberOfCardsToAdd = CardQuantity - CurrentCardQuantity;
                Cards.Add(CardId, NumberOfCardsToAdd);
                collectionSrv.AddCardsToCollection(PlayerId, Cards);
            }

            // Send system message
            logSrv.Log("StaffService", StaffUsername + " (" + PlayerId + ") used setCardQuantity with params " + CardId + ":" + CardQuantity);
        }

        public bool PlayerIsStaff(int Id)
        {
            return STAFF_USERS.ContainsKey(Id);
        }

        public int GetStaffPowerLevel(int Id)
        {
            STAFF_USERS.TryGetValue(Id, out int level);
            return level;
        }

        public bool PlayerCanUseStaffCommand(int PlayerId, int Command)
        {
            int RequiredStaffPower = GetRequiredStaffPower(Command);
            int PlayerStaffPower = staffSrv.GetStaffPowerLevel(PlayerId);

            return PlayerStaffPower >= RequiredStaffPower;
        }

        private int GetRequiredStaffPower(int CommandIndex)
        {
            string[] CommandDetails;
            foreach (string Command in StaffCommands)
            {
                CommandDetails = Command.Split(CollectionService.SECONDARY_DELIMITER);

                if (int.Parse(CommandDetails[0]) == CommandIndex)
                {
                    return int.Parse(CommandDetails[1]);
                }
            }
            return -1;
        }
    }
}
