﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Text;
using System.Xml;

namespace Server.src.services.Status
{
    public class StatusService : Server
    {
        private readonly string XML_DIR;
        private readonly long StartTime;
        private string Status;

        public StatusService()
        {
            XML_DIR = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "XML_DIR");

            if (string.IsNullOrEmpty(XML_DIR))
            {
                logSrv.LogWarning("StatusService", "XML_DIR is empty");
                return;
            }

            StartTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            SetServerLoading();
            logSrv.Log("StatusService", "Loaded");
        }

        public void SetServerLoading()
        {
            Status = "Loading";
            UpdateStatusXml();
        }

        public void SetServerOffline()
        {
            Status = "Offline";
            UpdateStatusXml();
        }

        public void SetServerOnline()
        {
            Status = "Online";
            UpdateStatusXml();
        }

        public void UpdateStatusXml()
        {
            if (string.IsNullOrEmpty(XML_DIR))
                return;

            bool isOnline = Status.Equals("Online");

            using XmlTextWriter xmlWriter = new XmlTextWriter(XML_DIR, Encoding.UTF8)
            {
                Formatting = Formatting.Indented
            };
            xmlWriter.WriteStartElement("Server");

            xmlWriter.WriteStartElement("Status");
            xmlWriter.WriteString(Status);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("Uptime");
            xmlWriter.WriteString(isOnline ? StartTime.ToString() : "--");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("Population");
            xmlWriter.WriteString(isOnline ? playerSrv.GetOnlinePlayers().Length.ToString() : "--");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("HighestPopulation");
            xmlWriter.WriteString(isOnline ? playerSrv.GetHighestPopulation().ToString() : "--");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
        }
    }
}
