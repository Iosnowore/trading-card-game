﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using Server.src.services.Player;
using System;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Guild;

namespace Server.src.services.Guild
{
    public class GuildService : Server
    {
        // Table
        private const string GUILDS_TABLE = "guilds";

        // Columns
        private const string COL_ID = "id";
        private const string COL_NAME = "name";
        private const string COL_CREATED = "created";
        private const string COL_MEMBERS = "members";
        private const string COL_MESSAGE = "message";
        private const string COL_LASTMODIFIED = "last_modified";

        // Guild ranks
        private const int LEADER_RANK = 3;
        private const int OFFICER_RANK = 2;
        private const int MEMBER_RANK = 1;

        // Guild objects
        private readonly SortedDictionary<int, GuildObject> GuildObjects;

        public GuildService()
        {
            GuildObjects = new SortedDictionary<int, GuildObject>();

            LoadGuilds();

            logSrv.Log("GuildService", "Loaded");
        }

        private void LoadGuilds()
        {
            int[] GuildIds = dbSrv.GetIntTableResults(GUILDS_TABLE, COL_ID);
            string[] GuildNames = dbSrv.GetStringTableResults(GUILDS_TABLE, COL_NAME);
            int[] GuildCreationDates = dbSrv.GetIntTableResults(GUILDS_TABLE, COL_CREATED);
            string[] GuildMessages = dbSrv.GetStringTableResults(GUILDS_TABLE, COL_MESSAGE);
            int[] LastModifieds = dbSrv.GetIntTableResults(GUILDS_TABLE, COL_LASTMODIFIED);

            int GuildLeaderId;
            int[] GuildOfficerIds;
            int[] GuildMemberIds;
            for (int i = 0; i < GuildIds.Length; i++)
            {
                GuildLeaderId = GetMemberIdsOfSpecificRank(GuildIds[i], LEADER_RANK)[0];
                GuildOfficerIds = GetMemberIdsOfSpecificRank(GuildIds[i], OFFICER_RANK);
                GuildMemberIds = GetMemberIdsOfSpecificRank(GuildIds[i], MEMBER_RANK);

                GuildObjects.Add(GuildIds[i], new GuildObject(GuildNames[i], GuildCreationDates[i], GuildLeaderId, GuildOfficerIds, GuildMemberIds, GuildMessages[i], LastModifieds[i]));
            }
            logSrv.LogDebug("GuildService", "Loaded " + GuildObjects.Count + " Guild Objects.");
        }

        private string ConstructMembersString(int GuildId)
        {
            GuildObject Guild = GuildObjects[GuildId];

            string MembersString = Guild.GetLeaderId().ToString() + CollectionService.SECONDARY_DELIMITER + LEADER_RANK;

            // Guild Officers
            int[] PlayerIds = Guild.GetOfficerIds();
            foreach (int OfficerId in PlayerIds)
                MembersString += CollectionService.PRIMARY_DELIMITER.ToString() + OfficerId + CollectionService.SECONDARY_DELIMITER + OFFICER_RANK;

            // Guild Members
            PlayerIds = Guild.GetMemberIds();
            foreach (int MemberId in PlayerIds)
                MembersString += CollectionService.PRIMARY_DELIMITER.ToString() + MemberId + CollectionService.SECONDARY_DELIMITER + MEMBER_RANK;

            // Return final string
            return MembersString;
        }

        private void SyncGuildToDatabase(int GuildId)
        {
            // Check if guild is in the guilds table, could have been deleted
            if (!GuildObjects.ContainsKey(GuildId))
                return;

            // Check and compare data, if different, update it.

            // Data saved on database
            string OldMembers = dbSrv.GetString(GUILDS_TABLE, COL_MEMBERS, COL_ID, GuildId.ToString());
            string OldMessage = dbSrv.GetString(GUILDS_TABLE, COL_MESSAGE, COL_ID, GuildId.ToString());

            // Data saved on game server
            string NewMembers = ConstructMembersString(GuildId);
            string NewMessage = GuildObjects[GuildId].GetMessage();

            // Update members
            if (!OldMembers.Equals(NewMembers))
                dbSrv.ChangeValue(GUILDS_TABLE, COL_NAME, NewMembers, GuildId);

            // Update message
            if (!OldMessage.Equals(NewMessage))
            {
                dbSrv.ChangeValue(GUILDS_TABLE, COL_MESSAGE, NewMessage, GuildId);

                // If the message changed, then the message last modified date changed too
                int NewLastModified = GuildObjects[GuildId].GetLastModified();
                dbSrv.ChangeValue(GUILDS_TABLE, COL_LASTMODIFIED, NewLastModified.ToString(), GuildId);
            }
        }

        public string GetGuildName(int GuildId)
        {
            return GuildObjects[GuildId].GetName();
        }

        public string GetGuildMessage(int GuildId)
        {
            return GuildObjects[GuildId].GetMessage();
        }

        private int GetGuildId(string GuildName)
        {
            foreach (int GuildId in GuildObjects.Keys)
                if (GuildObjects[GuildId].GetName().Equals(GuildName))
                    return GuildId;
            return -1;
        }

        private int[] GetMemberIdsOfSpecificRank(int GuildId, int Rank)
        {
            List<int> Ids = new List<int>();
            string[] MembersArray;

            if (GuildObjects.ContainsKey(GuildId))
            {
                switch (Rank)
                {
                    case LEADER_RANK:
                        return new int[] { GuildObjects[GuildId].GetLeaderId() };
                    case OFFICER_RANK:
                        return GuildObjects[GuildId].GetOfficerIds();
                    case MEMBER_RANK:
                        return GuildObjects[GuildId].GetMemberIds();
                    default: return null;
                }
            }

            // Server hasn't fully loaded yet, so we can't get the info from the data on the server
            string MembersString = dbSrv.GetString(GUILDS_TABLE, COL_MEMBERS, COL_ID, GuildId.ToString());
            MembersArray = MembersString.Split(CollectionService.PRIMARY_DELIMITER);

            string[] MemberDetails;
            for (int i = 0; i < MembersArray.Length; i++)
            {
                MemberDetails = MembersArray[i].Split(CollectionService.SECONDARY_DELIMITER);

                if (int.Parse(MemberDetails[1]) == Rank)
                {
                    Ids.Add(int.Parse(MemberDetails[0]));
                }
            }
            return Ids.ToArray();
        }

        private int[] GetAllMemberIds(int GuildId)
        {
            List<int> AllMemberIds = new List<int>();

            // Add members
            foreach (int MemberId in GuildObjects[GuildId].GetMemberIds())
                AllMemberIds.Add(MemberId);

            // Add officers
            foreach (int OfficerId in GuildObjects[GuildId].GetOfficerIds())
                AllMemberIds.Add(OfficerId);

            // Add leader
            AllMemberIds.Add(GuildObjects[GuildId].GetLeaderId());

            return AllMemberIds.ToArray();
        }

        private void ChangeMemberRank(int GuildId, int PlayerId, int Rank)
        {
            // Remove member from old rank
            if (GuildObjects[GuildId].IsMember(PlayerId))
                GuildObjects[GuildId].RemoveMember(PlayerId);
            else if (GuildObjects[GuildId].IsOfficer(PlayerId))
                GuildObjects[GuildId].RemoveOfficer(PlayerId);

            // Add member to new rank
            switch (Rank)
            {
                case MEMBER_RANK:
                    GuildObjects[GuildId].AddMember(Rank);
                    break;
                case OFFICER_RANK:
                    GuildObjects[GuildId].AddOfficer(Rank);
                    break;
                case LEADER_RANK:
                    GuildObjects[GuildId].SetLeaderId(Rank);
                    break;
            }

            // Sync to database
            SyncGuildToDatabase(GuildId);
        }

        public int GetUsersGuildId(int PlayerId)
        {
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(PlayerId);
            return PlayerObject.GetGuildId();
        }

        private int GetUsersGuildId(string PlayerUsername)
        {
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(PlayerUsername);
            return PlayerObject.GetGuildId();
        }

        private bool UserIsGuildOfficer(int UserId, int GuildId)
        {
            int[] GuildOfficerIds = GetMemberIdsOfSpecificRank(GuildId, OFFICER_RANK);

            foreach (int OfficerId in GuildOfficerIds)
                if (OfficerId == UserId)
                    return true;
            return false;
        }

        private GuildInfoPacket ConstructGuildInfoPacket(int GuildId)
        {
            GuildObject GuildInfo = GuildObjects[GuildId];

            string GuildName = GuildInfo.GetName();
            int CreationDate = GuildInfo.GetCreationDate();

            int LeaderId = GuildInfo.GetLeaderId();
            string Leader = playerSrv.GetUsername(LeaderId);


            string Message = GuildInfo.GetMessage();

            int[] OfficerIds = GuildInfo.GetOfficerIds();
            string[] Officers = new string[OfficerIds.Length];
            for (int i = 0; i < OfficerIds.Length; i++)
                Officers[i] = playerSrv.GetUsername(OfficerIds[i]);

            int[] MemberIds = GuildInfo.GetMemberIds();
            string[] Members = new string[MemberIds.Length];
            for (int i = 0; i < MemberIds.Length; i++)
                Members[i] = playerSrv.GetUsername(MemberIds[i]);


            int[] TotalRatings = GetGuildTotalRatings(GuildId);
            int[] AverageRatings = GetGuildAverageRatings(GuildId, TotalRatings);

            int GuildRank = GetGuildRank(GuildId);

            return new GuildInfoPacket(GuildName, CreationDate, Leader, Message, Officers, Members, TotalRatings, AverageRatings, GuildRank);
        }

        private int[] GetGuildTotalRatings(int GuildId)
        {
            int[] TotalRatings = new int[3];

            int Sum = 0;

            int[] MemberIds = GetAllMemberIds(GuildId);

            string GamesString;

            // Add ranks of each member
            PlayerObject PlayerObject;
            for (int i = 0; i < MemberIds.Length; i++)
            {
                PlayerObject = playerSrv.GetPlayerObject(MemberIds[i]);
                GamesString = PlayerObject.GetGames();
                Sum += playerSrv.GetPlayerRating(GamesString);
            }

            TotalRatings[0] = Sum;
            TotalRatings[1] = Sum;
            TotalRatings[2] = Sum;

            return TotalRatings;
        }

        private int[] GetGuildAverageRatings(int GuildId, int[] GuildTotalRatings)
        {
            int[] AverageRatings = new int[3];

            int[] MemberIds = GetAllMemberIds(GuildId);

            AverageRatings[0] = GuildTotalRatings[0] / MemberIds.Length;
            AverageRatings[1] = GuildTotalRatings[1] / MemberIds.Length;
            AverageRatings[2] = GuildTotalRatings[2] / MemberIds.Length;

            return AverageRatings;
        }

        private void RemoveUserFromGuild(int GettingKickedId, int GuildId)
        {
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(GettingKickedId);

            // Update the player
            PlayerObject.SetGuildId(-1);

            if (GuildObjects[GuildId].GetLeaderId() == GettingKickedId)
            {
                // Remove the actual guild object
                GuildObjects.Remove(GuildId);

                // Remove from the database
                dbSrv.DeleteRow(GUILDS_TABLE, "id", GuildId.ToString());
            }
            else if (GuildObjects[GuildId].IsOfficer(GettingKickedId))
            {
                GuildObjects[GuildId].RemoveOfficer(GettingKickedId);
            }
            else if (GuildObjects[GuildId].IsMember(GettingKickedId))
            {
                GuildObjects[GuildId].RemoveMember(GettingKickedId);
            }

            // Sync to database
            SyncGuildToDatabase(GuildId);

            // We updated the PlayerObject
            playerSrv.SyncPlayerToDatabase(GettingKickedId);
        }

        private int GetGuildRank(int GuildId)
        {
            //@TODO: Implement
            // Get total ranking of each guild
            // If ranking is the same, rank by alphebetical order
            return 1;
        }

        //
        // PacketService Guild Handler Methods
        //

        public void HandleGuildCreate(int CreatorId, string GuildName)
        {
            // Get the create unix timestamp
            string CreateTimeUnix = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();

            // Create the object in the database first so we can get the guild id
            dbSrv.CreateTableRow(GUILDS_TABLE, new string[] { COL_NAME, COL_MEMBERS, COL_CREATED }, new string[] { GuildName, CreatorId.ToString() + CollectionService.SECONDARY_DELIMITER + LEADER_RANK, CreateTimeUnix });

            // Create the guild object
            GuildObject GuildObject = new GuildObject(GuildName, int.Parse(CreateTimeUnix), CreatorId, new int[0], new int[0], string.Empty, int.Parse(CreateTimeUnix));

            // Add the new object to the 
            int GuildId = dbSrv.GetInt(GUILDS_TABLE, "id", "leader", CreatorId.ToString());
            GuildObjects.Add(GuildId, GuildObject);

            // Update the user table
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(CreatorId);
            PlayerObject.SetGuildId(GuildId);

            // Add player to guild chatroom
            chatRoomSrv.CreateChatRoom(GuildName, 100, ChatTypes.ChatType.GUILD);
            chatRoomSrv.AddClientToChatRoom(GuildName, conSrv.GetClientByID(CreatorId));

            // We updated the PlayerObject
            playerSrv.SyncPlayerToDatabase(CreatorId);
        }

        public void HandleGuildDemote(int PlayerId, string TargetUsername)
        {
            int GuildId = GetUsersGuildId(PlayerId);
            int TargetId = playerSrv.GetId(TargetUsername);

            // Demote the officer to member
            ChangeMemberRank(GuildId, TargetId, OFFICER_RANK);
        }

        public void HandleRequestAllGuildInfo(int UserId)
        {
            List<GuildInfoPacket> GuildInfoPackets = new List<GuildInfoPacket>();

            foreach (int GuildId in GuildObjects.Keys)
                GuildInfoPackets.Add(ConstructGuildInfoPacket(GuildId));

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_ALLGUILDINFO, new RequestGuildsPacket(GuildInfoPackets.ToArray())), conSrv.GetClientByID(UserId));
        }

        public void HandleGuildSaveMessage(int PlayerId, string Message)
        {
            string ChangeTimeUnix = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(PlayerId);
            int GuildId = PlayerObject.GetGuildId();

            // Change the message
            GuildObjects[GuildId].SetMessage(Message);

            // Update the last changed date
            GuildObjects[GuildId].SetLastModified(int.Parse(ChangeTimeUnix));

            // Send player system message
            playerSrv.SendMessageToPlayer(PlayerId, "Your new guild message has been saved.", "New Guild Message Saved");

            // Sync to database
            SyncGuildToDatabase(GuildId);
        }

        public void HandleGuildLeave(int PlayerId)
        {
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(PlayerId);
            int GuildId = PlayerObject.GetGuildId();

            if (GuildId == -1)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You are not in a guild to leave.", "Not In a Guild");
                return;
            }

            RemoveUserFromGuild(PlayerId, GuildId);

            string GuildName = GuildObjects[GuildId].GetName();
            playerSrv.SendMessageToPlayer(PlayerId, "You have left " + GuildName + ".", "Left Guild " + GuildName);
        }

        public void HandleAcceptInvite(string GuildNameToJoin, int PlayerId)
        {
            int GuildId = GetGuildId(GuildNameToJoin);

            GuildObjects[GuildId].AddMember(PlayerId);

            string GuildName = GuildObjects[GuildId].GetName();
            chatRoomSrv.AddClientToChatRoom(GuildName, conSrv.GetClientByID(PlayerId));

            // Sync to database
            SyncGuildToDatabase(GuildId);
        }
        public void HandleGuildPromote(int PlayerId, string TargetUsername)
        {
            int GuildId = GetUsersGuildId(PlayerId);
            int TargetId = playerSrv.GetId(TargetUsername);

            // Promote the member to officer
            ChangeMemberRank(GuildId, TargetId, OFFICER_RANK);
        }

        public void HandleRequestGuildInfo(int UserId)
        {
            PlayerObject PlayerObject = playerSrv.GetPlayerObject(UserId);

            int GuildId = PlayerObject.GetGuildId();

            if (GuildId == -1)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_GUILDINFO, new GuildInfoPacket(null, -1, null, null, null, null, null, null, -1)), conSrv.GetClientByID(UserId));
                return;
            }

            GuildInfoPacket GuildInfoPacket = ConstructGuildInfoPacket(GuildId);

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_GUILDINFO, GuildInfoPacket), conSrv.GetClientByID(UserId));
        }

        public void HandleGuildInvite(int InviterId, string UsernameToInvite)
        {
            PacketSender p;
            PlayerObject InviterPlayerObject = playerSrv.GetPlayerObject(InviterId);
            PlayerObject InviteePlayerObject = playerSrv.GetPlayerObject(UsernameToInvite);

            if (InviterId == InviteePlayerObject.GetId())
            {
                // Inviter is the invitee
                p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { "You cannot invite yourself to a guild." }));
                conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviterId));
                return;
            }

            int InviterGuildId = InviterPlayerObject.GetGuildId();

            if (InviterGuildId == -1)
            {
                // Inviter is not in a guild
                p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { "You aren't in a guild to invite anyone." }));
                conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviterId));
                return;
            }

            if (!playerSrv.IsOnline(InviteePlayerObject.GetId()))
            {
                // Client is offline
                p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { "The player you're trying to invite is offline." }));
                conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviterId));
                return;
            }

            if (InviteePlayerObject.GetGuildId() > -1)
            {
                // Client is already in a guild
                p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { "The player you're trying to invite is already in a guild." }));
                conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviterId));
                return;
            }

            p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { "You have invited " + UsernameToInvite + " to join your guild." }));

            conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviterId));

            string GuildName = GuildObjects[InviterGuildId].GetName();
            p = new PacketSender(SERVER_ID, Commands.GUILD_INVITE, new CommandPacket(new string[] { playerSrv.GetUsername(InviterId), dbSrv.GetString(GUILDS_TABLE, COL_NAME, COL_NAME, GuildName), GuildName }));

            conSrv.SendPacketToClient(p, conSrv.GetClientByID(InviteePlayerObject.GetId()));
        }

        public void HandleGuildKick(int KickerId, string GettingKickedUsername)
        {
            int KickersGuildId = GetUsersGuildId(KickerId);

            if (KickersGuildId == -1)
                return;

            if (KickerId == playerSrv.GetId(GettingKickedUsername))
            {
                playerSrv.SendMessageToPlayer(KickerId, "You cannot guild remove yourself.", "");
                return;
            }

            if (GuildObjects[KickersGuildId].GetLeaderId() != KickerId && !UserIsGuildOfficer(KickerId, KickersGuildId))
            {
                playerSrv.SendMessageToPlayer(KickerId, "You must be the leader or officer to remove members from the guild.", "");
                return;
            }

            int GettingKickedGuildId = GetUsersGuildId(GettingKickedUsername);

            if (KickersGuildId != GettingKickedGuildId)
            {
                playerSrv.SendMessageToPlayer(KickerId, "Player " + GettingKickedUsername + " is not in your guild.", "");
                return;
            }

            int GettingKickedId = playerSrv.GetId(GettingKickedUsername);

            if (UserIsGuildOfficer(GettingKickedId, GettingKickedGuildId) && UserIsGuildOfficer(KickerId, KickersGuildId))
            {
                playerSrv.SendMessageToPlayer(KickerId, "Only the guild leader may kick guild officers.", "");
                return;
            }

            if (GuildObjects[KickersGuildId].GetLeaderId() == GettingKickedId)
            {
                playerSrv.SendMessageToPlayer(KickerId, "You cannot kick a guild leader from their own guild", "");
                return;
            }
            RemoveUserFromGuild(GettingKickedId, KickersGuildId);

            if (playerSrv.IsOnline(GettingKickedId))
            {
                playerSrv.SendMessageToPlayer(GettingKickedId, "You have been removed from the guild.", "Removed From Guild " + GettingKickedGuildId);
            }
            playerSrv.SendMessageToPlayer(KickerId, "You have removed " + GettingKickedUsername + " from the guild.", "Removed " + GettingKickedUsername + " From " + GettingKickedGuildId);
        }
    }
}
