﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Server.src.services.Guild
{
    public class GuildObject
    {
        // These values cannot be changed
        private readonly string Name;
        private readonly int CreationDate;

        private int LeaderId;
        private int[] OfficerIds;
        private int[] MemberIds;

        private string Message;
        private int LastModified;

        public GuildObject(string Name, int CreationDate, int LeaderId, int[] OfficerIds, int[] MemberIds, string Message, int LastModified)
        {
            this.Name = Name;
            this.CreationDate = CreationDate;

            this.LeaderId = LeaderId;
            this.OfficerIds = OfficerIds;
            this.MemberIds = MemberIds;

            this.Message = Message;
            this.LastModified = LastModified;
        }

        public void SetLeaderId(int LeaderId)
        {
            this.LeaderId = LeaderId;
        }

        public void SetMessage(string Message)
        {
            this.Message = Message;
        }

        public void AddOfficer(int OfficerId)
        {
            int[] NewOfficersIds = new int[OfficerIds.Length + 1];

            for (int i = 0; i < OfficerIds.Length; i++)
                NewOfficersIds[i] = OfficerIds[i];
            NewOfficersIds[NewOfficersIds.Length - 1] = OfficerId;
            OfficerIds = NewOfficersIds;
        }

        public bool IsOfficer(int PlayerId)
        {
            for (int i = 0; i < OfficerIds.Length; i++)
                if (OfficerIds[i] == PlayerId)
                    return true;
            return false;
        }

        public void RemoveOfficer(int OfficerId)
        {
            List<int> OfficerIdsList = new List<int>();

            for (int i = 0; i < OfficerIds.Length; i++)
                if (OfficerIds[i] != OfficerId)
                    OfficerIdsList.Add(OfficerIds[i]);
            OfficerIds = OfficerIdsList.ToArray();
        }

        public void AddMember(int MemberId)
        {
            int[] NewMemberIds = new int[OfficerIds.Length + 1];

            for (int i = 0; i < OfficerIds.Length; i++)
                NewMemberIds[i] = OfficerIds[i];
            NewMemberIds[NewMemberIds.Length - 1] = MemberId;
            MemberIds = NewMemberIds;
        }

        public void RemoveMember(int MemberId)
        {
            List<int> MemberIdsList = new List<int>();

            for (int i = 0; i < MemberIds.Length; i++)
                if (MemberIds[i] != MemberId)
                    MemberIdsList.Add(MemberIds[i]);
            MemberIds = MemberIdsList.ToArray();
        }

        public bool IsMember(int PlayerId)
        {
            for (int i = 0; i < MemberIds.Length; i++)
                if (MemberIds[i] == PlayerId)
                    return true;
            return false;
        }

        public string GetName()
        {
            return Name;
        }

        public int GetCreationDate()
        {
            return CreationDate;
        }

        public int GetLeaderId()
        {
            return LeaderId;
        }

        public string GetMessage()
        {
            return Message;
        }

        public int[] GetOfficerIds()
        {
            return OfficerIds;
        }

        public int[] GetMemberIds()
        {
            return MemberIds;
        }

        public void SetLastModified(int LastModified)
        {
            this.LastModified = LastModified;
        }

        public int GetLastModified()
        {
            return LastModified;
        }
    }
}
