﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using System;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Trade;

namespace Server.src.services.Trade
{
    public class TradeService : Server
    {
        private readonly List<TradeSession> TradeSessions;
        private readonly List<PostedTradePacket> PostedTrades;

        private const string POSTEDTRADES_TABLE = "postedtrades";

        public TradeService()
        {
            TradeSessions = new List<TradeSession>();
            PostedTrades = new List<PostedTradePacket>();

            LoadPostedTrades();

            logSrv.Log("TradeService", "Loaded");
        }

        public void SendTradeRequest(int RequesterId, string RequesteeUsername)
        {
            if (!playerSrv.UsernameExists(RequesteeUsername))
            {
                playerSrv.SendMessageToPlayer(RequesterId, "Username " + RequesteeUsername + " does not exist.", "Trade Failed");
                return;
            }

            int RequesteeId = playerSrv.GetId(RequesteeUsername);

            if (RequesterId == RequesteeId)
            {
                playerSrv.SendMessageToPlayer(RequesterId, "You cannot trade with yourself.", "Trade Failed");
                return;
            }

            if (!playerSrv.IsOnline(RequesteeId))
            {
                playerSrv.SendMessageToPlayer(RequesterId, "Player " + RequesteeUsername + " is not online.", "Trade Failed");
                return;
            }

            playerSrv.SendMessageToPlayer(RequesterId, "You have invited " + RequesteeUsername + " to a trade.", "Trade Request Sent");
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_PLAYER, new CommandPacket(new string[] { playerSrv.GetUsername(RequesterId) })), conSrv.GetClientByID(RequesteeId));
        }

        public void HandleTradeRequestResponse(int RequesteeId, string RequesterUsername, string Response)
        {
            int RequestersId = playerSrv.GetId(RequesterUsername);

            string RequesteeUsername = playerSrv.GetUsername(RequesteeId);

            if (!Response.Equals("Accept"))
            {
                playerSrv.SendMessageToPlayer(RequestersId, "Player " + RequesteeUsername + " declined your trade request.", "Trade Request Declined");
                return;
            }

            TradeSessions.Add(new TradeSession(RequestersId, RequesterUsername, RequesteeId, RequesteeUsername));
        }

        public TradeSession GetTradeSessionPlayerIsIn(int PlayerId)
        {
            for (int i = 0; i < TradeSessions.Count; i++)
                if (TradeSessions[i].GetRequesterId() == PlayerId || TradeSessions[i].GetRequesteeId() == PlayerId)
                    return TradeSessions[i];
            return null;
        }

        public void RemoveActiveTrade(TradeSession TradeSession)
        {
            TradeSessions.Remove(TradeSession);
        }

        private void LoadPostedTrades()
        {
            PostedTrades.Clear();

            int[] TradeIds = dbSrv.GetIntTableResults(POSTEDTRADES_TABLE, "id");

            for (int i = 0; i < TradeIds.Length; i++)
            {
                string Username = playerSrv.GetUsername(dbSrv.GetInt(POSTEDTRADES_TABLE, "playerid", "id", TradeIds[i].ToString()));
                string Date = dbSrv.GetString(POSTEDTRADES_TABLE, "dateposted", "id", TradeIds[i].ToString());

                string[] Offered = dbSrv.GetString(POSTEDTRADES_TABLE, "offered", "id", TradeIds[i].ToString()).Split(CollectionService.PRIMARY_DELIMITER);
                string[] Want = dbSrv.GetString(POSTEDTRADES_TABLE, "want", "id", TradeIds[i].ToString()).Split(CollectionService.PRIMARY_DELIMITER);

                SortedDictionary<int, int> OfferedDictionary = new SortedDictionary<int, int>();
                SortedDictionary<int, int> WantDictionary = new SortedDictionary<int, int>();

                string[] Card;

                if (string.IsNullOrEmpty(Offered[0]))
                    Offered = new string[0];

                if (string.IsNullOrEmpty(Want[0]))
                    Want = new string[0];

                for (int j = 0; j < Offered.Length; j++)
                {
                    Card = Offered[j].Split(CollectionService.SECONDARY_DELIMITER);
                    OfferedDictionary.Add(int.Parse(Card[0]), int.Parse(Card[1]));
                }

                for (int j = 0; j < Want.Length; j++)
                {
                    Card = Want[j].Split(CollectionService.SECONDARY_DELIMITER);
                    WantDictionary.Add(int.Parse(Card[0]), int.Parse(Card[1]));
                }

                PostedTrades.Add(new PostedTradePacket(TradeIds[i], Username, Date, OfferedDictionary, WantDictionary));
            }
        }

        public void HandleSendPostedTradesPacket(int PlayerId)
        {
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_POSTEDTRADES, new PostedTradesPacket(PostedTrades.ToArray())), conSrv.GetClientByID(PlayerId));
        }

        private PostedTradePacket GetPostedTrade(int TradeId)
        {
            for (int i = 0; i < PostedTrades.Count; i++)
                if (PostedTrades[i].GetId() == TradeId)
                    return PostedTrades[i];
            return null;
        }

        public void HandleAcceptPostedTrade(int PlayerId, string TradeId)
        {
            int OwnerPlayerId = dbSrv.GetInt(POSTEDTRADES_TABLE, "playerid", "id", TradeId);

            if (OwnerPlayerId == -1)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "This posted trade is no longer available.", "Trade Failed");
                return;
            }

            if (PlayerId == OwnerPlayerId)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You cannot accept your own posted trade.", "Trade Failed");
                return;
            }

            string[] OfferedCards = dbSrv.GetString(POSTEDTRADES_TABLE, "offered", "id", TradeId).Split(CollectionService.PRIMARY_DELIMITER);
            string[] WantCards = dbSrv.GetString(POSTEDTRADES_TABLE, "want", "id", TradeId).Split(CollectionService.PRIMARY_DELIMITER);

            SortedDictionary<int, int> OfferedCardsDictionary = new SortedDictionary<int, int>();
            SortedDictionary<int, int> WantCardsDictionary = new SortedDictionary<int, int>();

            string[] Card;

            if (string.IsNullOrEmpty(OfferedCards[0]))
                OfferedCards = new string[0];

            if (string.IsNullOrEmpty(WantCards[0]))
                WantCards = new string[0];

            for (int i = 0; i < OfferedCards.Length; i++)
            {
                Card = OfferedCards[i].Split(CollectionService.SECONDARY_DELIMITER);
                OfferedCardsDictionary.Add(int.Parse(Card[0]), int.Parse(Card[1]));
            }

            for (int i = 0; i < WantCards.Length; i++)
            {
                Card = WantCards[i].Split(CollectionService.SECONDARY_DELIMITER);
                WantCardsDictionary.Add(int.Parse(Card[0]), int.Parse(Card[1]));
            }

            if (!PlayerHasRequiredCardsToTrade(PlayerId, WantCardsDictionary))
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You don't have the required cards to trade.", "Trade Failed");
                return;
            }

            collectionSrv.RemoveCardsFromCollection(OwnerPlayerId, OfferedCardsDictionary);
            collectionSrv.AddCardsToCollection(PlayerId, OfferedCardsDictionary);

            collectionSrv.RemoveCardsFromCollection(PlayerId, WantCardsDictionary);
            collectionSrv.AddCardsToCollection(OwnerPlayerId, WantCardsDictionary);

            dbSrv.DeleteRow(POSTEDTRADES_TABLE, "id", TradeId);
            PostedTrades.Remove(GetPostedTrade(int.Parse(TradeId)));
            playerSrv.SendMessageToPlayer(PlayerId, "You have successfully accepted the trade.", "Trade Accepted");
        }

        private bool PlayerHasRequiredCardsToTrade(int PlayerId, SortedDictionary<int, int> Cards)
        {
            Collection.Collection PlayerCollection = collectionSrv.GetCollection(PlayerId);

            foreach (int CardId in Cards.Keys)
                if (PlayerCollection.GetNumberOfSpecficCard(CardId) < Cards[CardId])
                    return false;
            return true;
        }

        public void HandleCancelPostedTrade(int PlayerId, int PostedTradeId)
        {
            int PosterId = dbSrv.GetInt(POSTEDTRADES_TABLE, "playerid", "id", PostedTradeId.ToString());

            // Ensure the player is the owner
            if (PosterId != PlayerId)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You cannot cancel a posted trade that is not yours.", "Posted Trade Cancellation Failed");
                return;
            }

            // Get the cards to return to the player..
            string GiveCardString = dbSrv.GetString(POSTEDTRADES_TABLE, "offered", "id", PostedTradeId.ToString());
            string[] Cards = GiveCardString.Split(CollectionService.PRIMARY_DELIMITER);

            // Create the list of cards to refund
            SortedDictionary<int, int> CardsToRefund = new SortedDictionary<int, int>();

            string[] CardDetails;
            foreach (string Card in Cards)
            {
                CardDetails = Card.Split(CollectionService.SECONDARY_DELIMITER);
                CardsToRefund.Add(int.Parse(CardDetails[0]), int.Parse(CardDetails[1]));
            }

            // Delete the entry from the database
            dbSrv.DeleteRow(POSTEDTRADES_TABLE, "id", PostedTradeId.ToString());

            // Refund the cards after deletion (to avoid multiple card refund exploits)
            collectionSrv.AddCardsToCollection(PlayerId, CardsToRefund);

            // Send success message to player
            playerSrv.SendMessageToPlayer(PlayerId, "You have successfully canceled your posted trade. The cards have been returned to your collection.", "Posted Trade Cancellation Success");
        }

        public void HandleCreatePostedTrade(int PlayerId, SortedDictionary<int, int> CardsTheyGet, SortedDictionary<int, int> CardsYouGet)
        {
            if (CardsTheyGet.Count == 0)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You must choose at least one card to give.", "Posted Trade Creation Failed");
                return;
            }

            if (CardsYouGet.Count == 0)
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You must choose at least one card to recieve.", "Posted Trade Creation Failed");
                return;
            }

            if (!PlayerHasRequiredCardsToTrade(PlayerId, CardsTheyGet))
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You do not have the required cards to complete this posted trade.", "Posted Trade Creation Failed");
                return;
            }

            collectionSrv.RemoveCardsFromCollection(PlayerId, CardsTheyGet);

            int TradeId = dbSrv.GetNumberOfTableRows(POSTEDTRADES_TABLE);
            string Date = DateTime.Now.ToUniversalTime().ToShortDateString();

            dbSrv.CreateTableRow("postedtrades", new string[] { "id", "playerid", "dateposted", "offered", "want" }, new string[] { TradeId.ToString(), PlayerId.ToString(), Date, GetTradeString(CardsTheyGet), GetTradeString(CardsYouGet) });
            LoadPostedTrades();

            playerSrv.SendMessageToPlayer(PlayerId, "You successfully created the posted trade.", "Posted Trade Creation Success");
        }

        private string GetTradeString(SortedDictionary<int, int> Cards)
        {
            string FormattedString = string.Empty;

            if (Cards.Count == 0)
                return FormattedString;

            foreach (int CardId in Cards.Keys)
                FormattedString += CollectionService.PRIMARY_DELIMITER + CardId.ToString() + CollectionService.SECONDARY_DELIMITER + Cards[CardId];
            return FormattedString.Substring(1);
        }
    }
}
