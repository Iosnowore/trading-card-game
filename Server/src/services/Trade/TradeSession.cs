﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TCGData.Packets.Trade;

namespace Server.src.services.Trade
{
    public class TradeSession : Server
    {
        private readonly int RequesterId;
        private readonly int RequesteeId;

        private readonly string RequesterUsername;
        private readonly string RequesteeUsername;

        private readonly SortedDictionary<int, int> RequesterCardsGiving;
        private readonly SortedDictionary<int, int> RequesteeCardsGiving;

        private bool RequesterAccepted;
        private bool RequesteeAccepted;

        private bool RequesterConfirmed;
        private bool RequesteeConfirmed;

        public TradeSession(int RequesterId, string RequesterUsername, int RequesteeId, string RequesteeUsername)
        {
            this.RequesterId = RequesterId;
            this.RequesteeId = RequesteeId;

            this.RequesterUsername = RequesterUsername;
            this.RequesteeUsername = RequesteeUsername;

            RequesterCardsGiving = new SortedDictionary<int, int>();
            RequesteeCardsGiving = new SortedDictionary<int, int>();

            CollectionPacket RequesteeCollection = collectionSrv.GetCollectionPacket(RequesteeId);
            CollectionPacket RequestersCollection = collectionSrv.GetCollectionPacket(RequesterId);

            ClientData RequesteeClient = conSrv.GetClientByID(RequesteeId);
            ClientData RequesterClient = conSrv.GetClientByID(RequesterId);

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_PLAYER, new InitiateTradePacket(RequesteeUsername, RequesteeCollection, RequestersCollection)), RequesterClient);
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_PLAYER, new InitiateTradePacket(RequesterUsername, RequestersCollection, RequesteeCollection)), RequesteeClient);

            string ChatRoomTitle = "Trade-" + RequesterId;
            chatRoomSrv.CreateChatRoom(ChatRoomTitle, 2, ChatTypes.ChatType.TRADE);

            chatRoomSrv.AddClientToChatRoom(ChatRoomTitle, RequesteeClient);
            chatRoomSrv.AddClientToChatRoom(ChatRoomTitle, RequesterClient);

            chatRoomSrv.SendPacketToChatRoom(ChatRoomTitle, new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, new CommandPacket(new string[] { GameData.ServerId, RequesterUsername + " joined the chat room." })));
            chatRoomSrv.SendPacketToChatRoom(ChatRoomTitle, new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, new CommandPacket(new string[] { GameData.ServerId, RequesteeUsername + " joined the chat room." })));
        }

        public void AddCardToGive(int PlayerId, int CardId)
        {
            if (PlayerId == RequesterId)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_ADD, new CommandPacket(new string[] { CardId.ToString() })), conSrv.GetClientByID(RequesteeId));

                int Index = GetCardIndex(RequesterCardsGiving, CardId);

                if (Index > -1)
                {
                    RequesterCardsGiving.TryGetValue(CardId, out int Quantity);
                    RequesterCardsGiving[CardId] = Quantity + 1;
                }
                else
                {
                    RequesterCardsGiving.Add(CardId, 1);
                }
            }
            else
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_ADD, new CommandPacket(new string[] { CardId.ToString() })), conSrv.GetClientByID(RequesterId));

                int Index = GetCardIndex(RequesteeCardsGiving, CardId);

                if (Index > -1)
                {
                    RequesteeCardsGiving.TryGetValue(CardId, out int Quantity);
                    RequesteeCardsGiving[CardId] = Quantity + 1;
                }
                else
                {
                    RequesteeCardsGiving.Add(CardId, 1);
                }
            }
        }

        public void RemoveCardToGive(int PlayerId, int CardId)
        {
            if (PlayerId == RequesterId)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_REMOVE, new CommandPacket(new string[] { CardId.ToString() })), conSrv.GetClientByID(RequesteeId));

                int Index = GetCardIndex(RequesterCardsGiving, CardId);

                RequesterCardsGiving.TryGetValue(CardId, out int Quantity);

                if (Quantity == 0)
                    RequesterCardsGiving.Remove(CardId);
                else
                    RequesterCardsGiving[CardId] = Quantity - 1;
            }
            else
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_REMOVE, new CommandPacket(new string[] { CardId.ToString() })), conSrv.GetClientByID(RequesterId));

                int Index = GetCardIndex(RequesteeCardsGiving, CardId);

                RequesteeCardsGiving.TryGetValue(CardId, out int Quantity);

                if (Quantity == 0)
                    RequesteeCardsGiving.Remove(CardId);
                else
                    RequesteeCardsGiving[CardId] = Quantity - 1;
            }
        }

        private int GetCardIndex(SortedDictionary<int, int> CardDictionary, int CardId)
        {
            int[] CardIds = new int[CardDictionary.Count];
            CardDictionary.Keys.CopyTo(CardIds, 0);

            for (int i = 0; i < CardIds.Length; i++)
                if (CardIds[i] == CardId)
                    return i;
            return -1;
        }

        public void AcceptTrade(int PlayerId)
        {
            if (PlayerId == RequesterId)
            {
                RequesterAccepted = !RequesterAccepted;
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_ACCEPT, new CommandPacket(new string[] { RequesterAccepted.ToString() })), conSrv.GetClientByID(RequesteeId));
            }
            else
            {
                RequesteeAccepted = !RequesteeAccepted;
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_ACCEPT, new CommandPacket(new string[] { RequesteeAccepted.ToString() })), conSrv.GetClientByID(RequesterId));
            }

            if (RequesterAccepted && RequesteeAccepted)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_CONFIRM, new ConfirmTradePacket(RequesteeCardsGiving, RequesterCardsGiving)), conSrv.GetClientByID(RequesterId));
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_CONFIRM, new ConfirmTradePacket(RequesterCardsGiving, RequesteeCardsGiving)), conSrv.GetClientByID(RequesteeId));
            }
        }

        public void CancelTrade(int PlayerId)
        {
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.TRADE_CANCEL, null), conSrv.GetClientByID(PlayerId == RequesterId ? RequesteeId : RequesterId));

            tradeSrv.RemoveActiveTrade(this);
        }

        public void ConfirmTrade(int PlayerId)
        {
            if (PlayerId == RequesterId)
            {
                RequesterConfirmed = !RequesterConfirmed;
            }
            else
            {
                RequesteeConfirmed = !RequesteeConfirmed;
            }

            if (RequesterConfirmed && RequesteeConfirmed)
            {
                conSrv.SendPacketToClients(new PacketSender(SERVER_ID, Commands.TRADE_CONFIRM_ACCEPTED, null), new ClientData[] { conSrv.GetClientByID(RequesterId), conSrv.GetClientByID(RequesteeId) });

                collectionSrv.RemoveCardsFromCollection(RequesterId, RequesterCardsGiving);
                collectionSrv.RemoveCardsFromCollection(RequesteeId, RequesteeCardsGiving);

                collectionSrv.AddCardsToCollection(RequesterId, RequesteeCardsGiving);
                collectionSrv.AddCardsToCollection(RequesteeId, RequesterCardsGiving);

                tradeSrv.RemoveActiveTrade(this);
            }
        }

        public int GetRequesterId()
        {
            return RequesterId;
        }

        public int GetRequesteeId()
        {
            return RequesteeId;
        }
    }
}
