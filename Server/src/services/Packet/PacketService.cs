﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TCGData.Packets.Trade;

namespace Server.src.services.Packet
{
    public class PacketService : Server
    {
        public PacketService()
        {
            logSrv.Log("PacketService", "Loaded");
        }

        public void HandleUnknownPacket(PacketSender PacketSender)
        {
            switch (PacketSender.GetCommand())
            {
                case Commands.LOGIN_VALIDATION:
                    logSrv.LogDebug("PacketService", "Got login validation packet");
                    LoginValidationPacket LoginValidationPacket = (LoginValidationPacket)PacketSender.GetPacket();
                    loginSrv.ValidateCredentials(playerSrv.GetLastCreatedClient(), LoginValidationPacket);
                    break;
                case Commands.MATCH_CREATE:
                    MatchPacket MatchPacket = (MatchPacket)PacketSender.GetPacket();
                    matchSrv.CreateMatch(PacketSender.GetSenderId(), MatchPacket);
                    break;
                case Commands.CREATE_AVATAR:
                    CardData CardData = (CardData)PacketSender.GetPacket();
                    collectionSrv.CreateAvatarCard(PacketSender.GetSenderId(), CardData);
                    break;
                case Commands.POSTEDTRADE_CREATE:
                    PostedTradePacket TradePacket = (PostedTradePacket)PacketSender.GetPacket();
                    tradeSrv.HandleCreatePostedTrade(PacketSender.GetSenderId(), TradePacket.GetOffered(), TradePacket.GetWant());
                    break;
                case Commands.DECK_SAVE:
                    DeckPacket Packet = (DeckPacket)PacketSender.GetPacket();
                    deckBuildSrv.SaveDeck(PacketSender.GetSenderId(), Packet.GetDeckName(), Packet.GetCardIds());
                    break;
            }
        }

        public void HandleEmptyPacket(PacketSender PacketSender)
        {
            switch (PacketSender.GetCommand())
            {
                case Commands.REQUEST_LOBBYINFO:
                    lobbySrv.HandleRequestLobbyInfo(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_FRIENDSIGNORELIST:
                    playerSrv.SendFriendsIgnoreListPacket(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_POSTEDTRADES:
                    tradeSrv.HandleSendPostedTradesPacket(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_DECK_PACKETS:
                    deckBuildSrv.SendDecks(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_COLLECTION:
                    collectionSrv.HandleRequestCollection(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_GUILDINFO:
                    guildSrv.HandleRequestGuildInfo(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_ALLGUILDINFO:
                    guildSrv.HandleRequestAllGuildInfo(PacketSender.GetSenderId());
                    break;
                case Commands.REQUEST_SCENARIOS:
                    playerSrv.HandleRequestScenarios(PacketSender.GetSenderId());
                    break;
                case Commands.CLIENT_DISCONNECT:
                    clientSrv.DisconnectClient(conSrv.GetClientByID(PacketSender.GetSenderId()));
                    break;
                case Commands.MAIN_PHASE:
                    gameSrv.HandleMainPhaseDone(PacketSender.GetSenderId());
                    break;
                case Commands.LOAD_GAME:
                    gameSrv.HandleLoadedGame(PacketSender.GetSenderId());
                    break;
                case Commands.GUILD_LEAVE:
                    guildSrv.HandleGuildLeave(PacketSender.GetSenderId());
                    break;
                case Commands.MATCH_READY:
                    matchSrv.HandleMatchReady(PacketSender.GetSenderId());
                    break;
                case Commands.TRADE_ACCEPT:
                    tradeSrv.GetTradeSessionPlayerIsIn(PacketSender.GetSenderId()).AcceptTrade(PacketSender.GetSenderId());
                    break;
                case Commands.TRADE_CONFIRM:
                    tradeSrv.GetTradeSessionPlayerIsIn(PacketSender.GetSenderId()).ConfirmTrade(PacketSender.GetSenderId());
                    break;
                case Commands.LEAVE_GAME:
                    gameSrv.HandleLeaveGame(PacketSender.GetSenderId());
                    break;
                case Commands.CRASH_PING:
                    pingSrv.RemovePlayerFromNeedingResponse(PacketSender.GetSenderId());
                    break;
                case Commands.RAID_END_TURN:
                    gameSrv.HandlePassTurn(PacketSender.GetSenderId());
                    break;
                case Commands.RAID_EXERT_AVATAR:
                    gameSrv.HandleExertAvatar(PacketSender.GetSenderId());
                    break;
                case Commands.RAID_DAMAGE_AVATAR:
                    gameSrv.HandleDamageAvatar(PacketSender.GetSenderId());
                    break;
            }
        }

        public void HandleCommandPacket(PacketSender PacketSender)
        {
            CommandPacket CommandPacket = (CommandPacket)PacketSender.GetPacket();

            switch (PacketSender.GetCommand())
            {
                case Commands.LOBBY_CHAT:
                    chatSrv.DealWithLobbyChatPacket(CommandPacket);
                    break;
                case Commands.MATCH_CHAT:
                    chatSrv.DealWithMatchChatPacket(CommandPacket);
                    break;
                case Commands.SAYTO:
                    string message = playerSrv.GetUsername(PacketSender.GetSenderId()) + " (to " + CommandPacket.GetContent(0) + "): " + CommandPacket.GetContent(1);
                    CommandPacket = new CommandPacket(new string[] { GameData.ServerId, message });

                    if (matchSrv.PlayerIsInAMatch(PacketSender.GetSenderId()))
                        matchSrv.GetMatchUserIsIn(PacketSender.GetSenderId()).SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, CommandPacket));
                    else
                        lobbySrv.GetLobbyUserIsIn(PacketSender.GetSenderId()).SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, CommandPacket));
                    break;
                case Commands.LOBBY_JOIN:
                    chatRoomSrv.AddClientToChatRoom(CommandPacket.GetContent(0).ToString(), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    lobbySrv.AddClientToLobby(CommandPacket.GetContent(0).ToString(), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    break;
                case Commands.LOBBY_LEAVE:
                    chatRoomSrv.RemoveClientFromChatRoom(CommandPacket.GetContent(0).ToString(), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    lobbySrv.RemoveClientFromLobby(CommandPacket.GetContent(0).ToString(), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    break;
                case Commands.MATCH_JOIN:
                    Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(PacketSender.GetSenderId());
                    Match.Match Match = matchSrv.GetMatchWithTitle(Lobby, CommandPacket.GetContent(0).ToString());
                    if (Match.GameIsFull())
                    {
                        conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MATCH_JOIN, new JoinMatchResponsePacket(JoinMatchResponses.Responses.FULL, null)), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    }
                    else
                    {
                        conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MATCH_JOIN, matchSrv.GetMatchPacket(PacketSender.GetSenderId(), Match.GetTitle())), conSrv.GetClientByID(PacketSender.GetSenderId()));
                        matchSrv.AddClientToMatch(conSrv.GetClientByID(PacketSender.GetSenderId()), Match.GetTitle());
                    }
                    break;
                case Commands.MATCH_LEAVE:
                    matchSrv.RemoveClientFromMatch(conSrv.GetClientByID(PacketSender.GetSenderId()), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.BROADCAST_MESSAGE:
                    if (staffSrv.PlayerCanUseStaffCommand(PacketSender.GetSenderId(), PacketSender.GetCommand()))
                    {
                        staffSrv.BroadcastMessage(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    }
                    break;
                case Commands.GUILD_ACCEPT_INVITE:
                    guildSrv.HandleAcceptInvite(CommandPacket.GetContent(0).ToString(), PacketSender.GetSenderId());
                    break;
                case Commands.GUILD_CHAT:
                    chatSrv.DealWithGuildChatPacket(CommandPacket);
                    break;
                case Commands.GUILD_CREATE:
                    guildSrv.HandleGuildCreate(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.GUILD_INVITE:
                    guildSrv.HandleGuildInvite(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.GUILD_KICK:
                    guildSrv.HandleGuildKick(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.GUILD_PROMOTE:
                    guildSrv.HandleGuildPromote(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.GUILD_DEMOTE:
                    guildSrv.HandleGuildDemote(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.KICK_PLAYER:
                    if (staffSrv.PlayerCanUseStaffCommand(PacketSender.GetSenderId(), PacketSender.GetCommand()))
                    {
                        string KickReason = CommandPacket.GetContent(1).ToString();
                        string PlayerUsername = CommandPacket.GetContent(0).ToString();
                        int PlayerId = playerSrv.GetId(PlayerUsername);
                        staffSrv.KickPlayer(PlayerId, KickReason);
                    }
                    break;
                case Commands.MUTE_PLAYER:
                    if (staffSrv.PlayerCanUseStaffCommand(PacketSender.GetSenderId(), PacketSender.GetCommand()))
                    {
                        string TargetUsername = CommandPacket.GetContent(0).ToString();
                        string Duration = null;

                        if (CommandPacket.GetNumberOfContents() == 2)
                        {
                            Duration = CommandPacket.GetContent(1).ToString();
                        }
                        staffSrv.MutePlayer(PacketSender.GetSenderId(), TargetUsername, Duration);
                    }
                    break;
                case Commands.SET_CARD_QUANTITY:
                    if (staffSrv.PlayerCanUseStaffCommand(PacketSender.GetSenderId(), PacketSender.GetCommand()))
                    {
                        int[] CardInfo = new int[2];
                        CardInfo[0] = (int)CommandPacket.GetContent(0);
                        CardInfo[1] = (int)CommandPacket.GetContent(1);
                        staffSrv.SetCardQuantity(PacketSender.GetSenderId(), CardInfo[0], CardInfo[1]);
                    }
                    else
                    {
                        playerSrv.SendMessageToPlayer(PacketSender.GetSenderId(), "You do not have the required permissions to use this command.", "No Permission");
                    }
                    break;
                case Commands.REQUEST_MATCH:
                    matchSrv.HandleSendMatchPacket(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.REQUEST_USERINFO:
                    playerSrv.SendUserInfoPacket(CommandPacket.GetContent(0).ToString(), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    break;
                case Commands.ME:
                    if (matchSrv.PlayerIsInAMatch(PacketSender.GetSenderId()))
                        matchSrv.GetMatchUserIsIn(PacketSender.GetSenderId()).SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, new CommandPacket(new string[] { GameData.ServerId, playerSrv.GetUsername(PacketSender.GetSenderId()) + ' ' + CommandPacket.GetContent(0) })));
                    else
                        lobbySrv.GetLobbyUserIsIn(PacketSender.GetSenderId()).SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, new CommandPacket(new string[] { GameData.ServerId, playerSrv.GetUsername(PacketSender.GetSenderId()) + ' ' + CommandPacket.GetContent(0) })));
                    break;
                case Commands.WHISPER:
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.WHISPER, new CommandPacket(new string[] { playerSrv.GetUsername(PacketSender.GetSenderId()), CommandPacket.GetContent(1).ToString() })), conSrv.GetClientByID(playerSrv.GetId(CommandPacket.GetContent(0).ToString())));
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, new CommandPacket(new string[] { GameData.ServerId, "You whisper to " + CommandPacket.GetContent(0) + ", " + CommandPacket.GetContent(1) })), conSrv.GetClientByID(PacketSender.GetSenderId()));
                    break;
                case Commands.FRIEND_ADD:
                    playerSrv.AddFriend(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.FRIEND_REMOVE:
                    playerSrv.RemoveFriend(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.IGNORE_PLAYER:
                    playerSrv.IgnorePlayer(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.UNIGNORE_PLAYER:
                    playerSrv.UnignorePlayer(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.PING_PLAYER:
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.PING_PLAYER, new CommandPacket(new string[] { playerSrv.GetUsername(PacketSender.GetSenderId()), CommandPacket.GetContent(1).ToString() })), conSrv.GetClientByID(playerSrv.GetId(CommandPacket.GetContent(0).ToString())));
                    break;
                case Commands.SAVE_GUILD_MESSAGE:
                    guildSrv.HandleGuildSaveMessage(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.SAVE_AVATAR:
                    playerSrv.HandleSavePlayerAvatar(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.SAVE_ACCOUNTINFO:
                    playerSrv.HandleSaveAccountInfo(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString(), CommandPacket.GetContent(1).ToString(), CommandPacket.GetContent(2).ToString());
                    break;
                case Commands.OPEN_PACK:
                    collectionSrv.HandleOpenPack(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.CARDWANT_DECREASE:
                    collectionSrv.DecreaseCardWant(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.CARDWANT_INCREASE:
                    collectionSrv.IncreaseCardWant(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.DECK_DELETE:
                    deckBuildSrv.DeleteDeck(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.TRADE_PLAYER:
                    if (CommandPacket.GetNumberOfContents() == 1)
                        tradeSrv.SendTradeRequest(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    else
                        tradeSrv.HandleTradeRequestResponse(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString(), CommandPacket.GetContent(1).ToString());
                    break;
                case Commands.TRADE_ADD:
                    tradeSrv.GetTradeSessionPlayerIsIn(PacketSender.GetSenderId()).AddCardToGive(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.TRADE_REMOVE:
                    tradeSrv.GetTradeSessionPlayerIsIn(PacketSender.GetSenderId()).RemoveCardToGive(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.POSTEDTRADE_ACCEPT:
                    tradeSrv.HandleAcceptPostedTrade(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.REDEEM_LOOTCARD:
                    collectionSrv.HandleRedeemLootCard(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0), (int)CommandPacket.GetContent(1));
                    break;
                case Commands.REDEEM_PACK:
                    collectionSrv.HandleRedeemPack(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.REDEEM_PROMOPACK:
                    collectionSrv.HandleRedeemPromoPack(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.POSTEDTRADE_CANCEL:
                    tradeSrv.HandleCancelPostedTrade(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.DECK_OPEN:
                    playerSrv.HandleSelectDeck(PacketSender.GetSenderId(), CommandPacket.GetContent(0).ToString());
                    break;
                case Commands.START_SCENARIO:
                    gameSrv.HandleStartScenario(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0), (string)CommandPacket.GetContent(1));
                    break;
                case Commands.REDRAW_HAND:
                    gameSrv.HandleRedrawHand(PacketSender.GetSenderId(), (bool)CommandPacket.GetContent(0));
                    break;
                case Commands.QUEST_PHASE:
                    gameSrv.HandleSelectedQuest(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.START_RAID:
                    gameSrv.HandleStartRaid(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.PLACE_CARD:
                    Game.Game GamePlayerIsIn = gameSrv.GetGamePlayerIsIn(PacketSender.GetSenderId());

                    if (GamePlayerIsIn == null)
                    {
                        logSrv.LogDebug("PacketService", "GamePlayerIsIn == null");
                        return;
                    }

                    int CardId = (int)CommandPacket.GetContent(0);
                    int QuestId = -1;

                    // If does not have a QuestId, then it's not being placed on playmat (Ability/Item/Tactic card)
                    if (CommandPacket.GetNumberOfContents() == 2)
                        QuestId = (int)CommandPacket.GetContent(1);

                    GamePlayerIsIn.PlaceCard(PacketSender.GetSenderId(), CardId, QuestId);
                    break;
                case Commands.RAID_EXERT_CARD:
                    gameSrv.HandleExertCard(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0), (int)CommandPacket.GetContent(1), (string)CommandPacket.GetContent(2));
                    break;
                case Commands.RAID_TAKE_DAMAGE:
                    gameSrv.HandleTakeDamage(PacketSender.GetSenderId(), (int)CommandPacket.GetContent(0));
                    break;
                case Commands.OBSERVE_GAME:
                    // TODO: Implement
                    break;
                case Commands.JOIN_GAME:
                    // TODO: Implement
                    break;
                default:
                    logSrv.LogWarning("PacketService", "Received unknown packet command: " + PacketSender.GetCommand() + " from userid " + PacketSender.GetSenderId());
                    break;
            }
        }
    }
}
