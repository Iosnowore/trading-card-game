﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace Server.src.services.Database
{
    public class DatabaseService : Server
    {
        private const string SQLITE = "sqlite";
        private const string MYSQL = "mysql";

        private readonly SQLiteConnection SQLITE_CONN;
        private readonly MySqlConnection MYSQL_CONN;

        public DatabaseService()
        {
            string DatabaseType = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_TYPE");

            // Remove case sensitive
            DatabaseType = DatabaseType.ToLower();

            switch (DatabaseType)
            {
                case SQLITE:
                    string DatabaseFile = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_FILE");
                    if (File.Exists(DatabaseFile))
                    {
                        SQLITE_CONN = new SQLiteConnection("Data Source=" + DatabaseFile);
                        SQLITE_CONN.Open();
                    }
                    else
                    {
                        logSrv.LogError("DatabaseService", "DB_FILE is invalid");
                        return;
                    }
                    break;
                case MYSQL:
                    string Address = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_ADDRESS");
                    string Username = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_USERNAME");
                    string Password = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_PASSWORD");
                    string Name = cfgSrv.GetConfigValue(cfgSrv.NETWORK_CFG, "DB_NAME");

                    if (string.IsNullOrEmpty(Address))
                    {
                        logSrv.LogError("DatabaseService", "DB_ADDRESS value is empty");
                        return;
                    }

                    if (string.IsNullOrEmpty(Username))
                    {
                        logSrv.LogError("DatabaseService", "DB_USERNAME value is empty");
                        return;
                    }

                    if (string.IsNullOrEmpty(Password))
                    {
                        logSrv.LogError("DatabaseService", "DB_PASSWORD value is empty");
                        return;
                    }

                    if (string.IsNullOrEmpty(Name))
                    {
                        logSrv.LogError("DatabaseService", "DB_NAME value is empty");
                        return;
                    }

                    string ConnectionString = "Database=" + Name + ";Data Source=" + Address + ";User Id=" + Username + ";Password=" + Password;
                    MYSQL_CONN = new MySqlConnection(ConnectionString);
                    break;
                default:
                    logSrv.LogError("DatabaseService", "DB_TYPE is not valid ('sqlite' or 'mysql')");
                    return;
            }
            logSrv.Log("DatabaseService", "Started");
        }

        private void ExecuteCommand(string command)
        {
            try
            {
                if (SQLITE_CONN != null)
                {
                    using SQLiteCommand cmd = new SQLiteCommand(command, SQLITE_CONN);
                    cmd.ExecuteNonQuery();
                }
                else if (MYSQL_CONN != null)
                {
                    using MySqlCommand cmd = new MySqlCommand(command, MYSQL_CONN);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    logSrv.LogError("DatabaseService", "All connections are null.");
                }
            }
            catch (Exception ex)
            {
                logSrv.LogWarning("DatabaseService", "ExecuteCommand crash prevented: " + ex.Message + "\nCommand: " + command);
            }
        }

        public void ChangeValue(string table, string valueToChangeTitle, string newValue, int Id)
        {
            newValue = newValue.Replace("'", "''");
            ExecuteCommand("UPDATE " + table + " SET " + valueToChangeTitle + " = '" + newValue + "' WHERE id ='" + Id + "'");
        }

        public void ChangeValue(string table, string valueToChangeTitle, string newValue, string Identifier, string IdentifierValue)
        {
            ExecuteCommand("UPDATE " + table + " SET " + valueToChangeTitle + " = '" + newValue + "' WHERE " + Identifier + " ='" + IdentifierValue + "'");
        }

        public string GetString(string table, string valueTitleNeeded, string locateValueTitle, string definiteValue)
        {
            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT " + valueTitleNeeded + " FROM " + table + " WHERE " + locateValueTitle + " = @DEFINITE_VALUE COLLATE NOCASE", SQLITE_CONN);
                cmd.Parameters.AddWithValue("@DEFINITE_VALUE", definiteValue);
                using SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                }
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT " + valueTitleNeeded + " FROM " + table + " WHERE " + locateValueTitle + " = @DEFINITE_VALUE COLLATE NOCASE", MYSQL_CONN);
                cmd.Parameters.AddWithValue("@DEFINITE_VALUE", definiteValue);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                }
            }
            else
            {
                logSrv.LogError("DatabaseService", "All connections are null.");
            }
            return null;
        }

        public int GetInt(string table, string valueTitleNeeded, string locateValueTitle, string definiteValue)
        {
            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT " + valueTitleNeeded + " FROM " + table + " WHERE " + locateValueTitle + " = @DEFINITE_VALUE COLLATE NOCASE", SQLITE_CONN);
                cmd.Parameters.AddWithValue("@DEFINITE_VALUE", definiteValue);
                using SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                }
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT " + valueTitleNeeded + " FROM " + table + " WHERE " + locateValueTitle + " = @DEFINITE_VALUE COLLATE NOCASE", MYSQL_CONN);
                cmd.Parameters.AddWithValue("@DEFINITE_VALUE", definiteValue);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader.IsDBNull(0) ? -1 : reader.GetInt32(0);
                }
            }
            else
            {
                logSrv.LogError("DatabaseService", "All connections are null.");
            }
            return -1;
        }

        public string[] GetStringTableResults(string table, string value)
        {
            List<string> Results = new List<string>();

            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT " + value + " FROM " + table, SQLITE_CONN);
                using SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Results.Add(reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT " + value + " FROM " + table, MYSQL_CONN);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Results.Add(reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
                    }
                }
            }
            else
            {
                logSrv.LogError("DatabaseService", "All connections are null.");
            }
            return Results.ToArray();
        }

        public int[] GetIntTableResults(string table, string value)
        {
            List<int> Results = new List<int>();

            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT " + value + " FROM " + table, SQLITE_CONN);
                using SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Results.Add(reader.GetInt32(i));
                    }
                }
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT " + value + " FROM " + table, MYSQL_CONN);
                using MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Results.Add(reader.GetInt32(i));
                    }
                }
            }
            else
            {
                logSrv.LogError("DatabaseService", "All connections are null.");
            }
            return Results.ToArray();
        }

        public int GetNumberOfTableRows(string table, string valueTitle, string value)
        {
            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT COUNT(*) FROM " + table + " WHERE " + valueTitle + " = " + "@VALUE", SQLITE_CONN);
                cmd.Parameters.AddWithValue("@VALUE", value);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM " + table + " WHERE " + valueTitle + " = " + "@VALUE", MYSQL_CONN);
                cmd.Parameters.AddWithValue("@VALUE", value);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            else
            {
                logSrv.LogError("DatabaseService", "All connections are null.");
                return -1;
            }
        }

        public int GetNumberOfTableRows(string table)
        {
            if (SQLITE_CONN != null)
            {
                using SQLiteCommand cmd = new SQLiteCommand("SELECT COUNT(*) FROM " + table, SQLITE_CONN);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            else if (MYSQL_CONN != null)
            {
                using MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM " + table, MYSQL_CONN);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            else
            {
                logSrv.LogError("DatabaseService", "All database connections are null.");
                return -1;
            }
        }

        public void DeleteRow(string Table, string ValueTitle, string Value)
        {
            ExecuteCommand("DELETE FROM " + Table + " WHERE " + ValueTitle + " = '" + Value + "'");
            logSrv.LogDebug("DatabaseService", "Deleted Table Rows from Table: " + Table);
        }

        public void CreateTableRow(string Table, string[] Columns, string[] Values)
        {
            // Fix return strings if they exist
            for (int i = 0; i < Values.Length; i++)
            {
                if (Values[i].Contains("'"))
                {
                    Values[i] = Values[i].Replace("'", "''");
                }
            }

            ExecuteCommand("INSERT INTO " + Table + "(" + string.Join(",", Columns) + ") VALUES('" + string.Join("','", Values) + "')");
            logSrv.LogDebug("DatabaseService", "Created Table Rows in Table: " + Table);
        }

        public void CreateTableRow(string Table, string Column, string Value)
        {
            CreateTableRow(Table, new string[] { Column }, new string[] { Value });
        }
    }
}
