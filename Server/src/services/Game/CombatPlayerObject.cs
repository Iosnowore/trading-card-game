﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using TCGData;

namespace Server.src.services.Game
{
    public class CombatPlayerObject
    {
        private readonly int PlayerId;
        private readonly int AvatarId;

        private readonly string Username;

        // <int: CardId, int: Quantity>
        private readonly SortedDictionary<int, int> Deck;

        private readonly List<PlaymatCard> PlayedAbilities;
        private readonly List<PlaymatCard> PlayedItems;

        private readonly int[] Hand;

        private int AvatarHealth;
        private int AvatarPower;

        private int CompletedQuests;

        private bool AvatarExerted;

        public CombatPlayerObject(int PlayerId, int AvatarId, string Username, SortedDictionary<int, int> Deck)
        {
            this.PlayerId = PlayerId;
            this.AvatarId = AvatarId;

            this.Username = Username;

            this.Deck = new SortedDictionary<int, int>(Deck);

            AvatarHealth = Cards.GetCardData(AvatarId).GetHealthOrLevel();
            AvatarPower = Game.AVATAR_START_POWER;

            PlayedAbilities = new List<PlaymatCard>();
            PlayedItems = new List<PlaymatCard>();

            Hand = new int[Game.MAX_HAND_CARDS];
        }

        public bool DrawCardIds(int CardsToDraw)
        {
            // Put the deck into ids only so that way the more cards the player has, the more likely he will draw it
            List<int> DeckCardIds = new List<int>();

            foreach (int CardId in Deck.Keys)
                for (int i = 0; i < Deck[CardId]; i++)
                    DeckCardIds.Add(CardId);

            // Create a Random object
            Random Random = new Random();
            int CardsDrawn = 0;

            for (int i = 0; i < Hand.Length; i++)
            {
                if (Hand[i] == Game.INVALID)
                {
                    int PlayableCards = GetNumberOfPlayableCardsInDeck();

                    // If the player has no cards left, he lost.
                    if (PlayableCards == 0)
                        return false;

                    int CardId;
                    int Index;
                    CardData CardData;

                    // Loop until we have a card that isn't a quest type
                    do
                    {
                        // Get a random index of DeckCardIds
                        Index = Random.Next(0, DeckCardIds.Count - 1);
                        CardId = DeckCardIds[Index];
                        CardData = Cards.GetCardData(CardId);
                    } while (CardData.GetType().Equals("Quest") || CardData.GetType().Equals("Avatar"));

                    // Update the hand with the new card id
                    Hand[i] = CardId;

                    // Remove the card from the deck (or decrement the quantity)
                    if (Deck[CardId] == 1)
                        Deck.Remove(CardId);
                    else
                        Deck[CardId] -= 1;

                    // Remove from the list of unpicked card ids
                    DeckCardIds.RemoveAt(Index);

                    // Update CardsDrawn
                    CardsDrawn++;

                    if (CardsDrawn == CardsToDraw)
                        break;
                }
            }
            return true;
        }

        public void ShuffleDeck()
        {
            SortedDictionary<int, int> ShuffledDeck = new SortedDictionary<int, int>();

            // Get player's card ids in deck
            List<int> UnpickedCardIds = new List<int>(Deck.Keys);

            // Create Random object
            Random Random = new Random();

            while (Deck.Count > 0)
            {
                int RandomIndex = Random.Next(0, UnpickedCardIds.Count - 1);

                int CardId = UnpickedCardIds[RandomIndex];
                int CardQuantity = Deck[CardId];

                // Remove from the old deck
                Deck.Remove(CardId);

                // Remove the CardId from the unpicked list
                UnpickedCardIds.Remove(CardId);

                // Add to the shuffled deck
                ShuffledDeck.Add(CardId, CardQuantity);
            }

            // Clear existing deck
            Deck.Clear();

            // Add the shuffled elements
            foreach (int CardId in ShuffledDeck.Keys)
                Deck.Add(CardId, ShuffledDeck[CardId]);
        }

        public int DrawQuest()
        {
            // We always want to draw the lowest level quest

            int LowestLevelQuestId = -1;
            int LowestLevel = int.MaxValue;

            foreach (int CardId in Deck.Keys)
            {
                CardData CardData = Cards.GetCardData(CardId);

                if (CardData.GetType().Equals("Quest") && CardData.GetHealthOrLevel() < LowestLevel)
                {
                    LowestLevelQuestId = CardId;
                    LowestLevel = CardData.GetHealthOrLevel();
                }
            }

            // Remove the quest and return what we used
            Deck.Remove(LowestLevelQuestId);
            return LowestLevelQuestId;
        }

        public void PutHandCardsBackInDeck()
        {
            // Go through each card
            for (int i = 0; i < Hand.Length; i++)
            {
                int CardId = Hand[i];

                // Add the card back to the deck
                if (Deck.ContainsKey(CardId))
                    Deck[CardId] = Deck[CardId] + 1;
                else
                    Deck.Add(CardId, 1);

                // Remove the card from the hand
                Hand[i] = Game.INVALID;
            }
        }

        public int GetNumberOfCardsInHand()
        {
            int Cards = 0;

            foreach (int CardId in Hand)
                if (CardId != Game.INVALID)
                    Cards++;
            return Cards;
        }

        public int GetNumberOfPlayableCardsInDeck()
        {
            int Total = 0;

            CardData CardData;
            foreach (int CardId in Deck.Keys)
            {
                CardData = Cards.GetCardData(CardId);

                switch (CardData.GetType())
                {
                    case "Ability":
                    case "Item":
                    case "Tactic":
                    case "Unit":
                        Total += Deck[CardId];
                        break;
                }
            }

            return Total;
        }

        public SortedDictionary<int, int> GetDeck()
        {
            return Deck;
        }

        public int GetPlayerId()
        {
            return PlayerId;
        }

        public int GetAvatarId()
        {
            return AvatarId;
        }

        public string GetUsername()
        {
            return Username;
        }

        public void SetAvatarHealth(int AvatarHealth)
        {
            this.AvatarHealth = AvatarHealth;
        }

        public int GetAvatarHealth()
        {
            return AvatarHealth;
        }

        public void SetAvatarPower(int AvatarPower)
        {
            this.AvatarPower = AvatarPower;
        }

        public int GetAvatarPower()
        {
            return AvatarPower;
        }

        public void SetCompletedQuests(int CompletedQuests)
        {
            this.CompletedQuests = CompletedQuests;
        }

        public int GetCompletedQuests()
        {
            return CompletedQuests;
        }

        public void SetHandCardId(int Index, int CardId)
        {
            Hand[Index] = CardId;
        }

        public int GetHandCardId(int Index)
        {
            return Hand[Index];
        }

        public int[] GetHandCardIds()
        {
            return Hand;
        }

        public bool UseAbility(int CardId)
        {
            foreach (PlaymatCard PlaymatCard in PlayedAbilities)
                if (PlaymatCard.GetCardId() == CardId)
                {
                    return PlayedAbilities.Remove(PlaymatCard);
                }
            return false;
        }

        public List<PlaymatCard> GetPlayedAbilities()
        {
            return PlayedAbilities;
        }

        public int GetNumberOfPlayedAbilities()
        {
            return PlayedAbilities.Count;
        }

        public bool HasUnexertedAbilities()
        {
            foreach (PlaymatCard Ability in PlayedAbilities)
                if (!Ability.GetExerted())
                    return true;
            return false;
        }

        public int GetNumberOfPlayedItems()
        {
            return PlayedItems.Count;
        }

        public int GetIndexOfCardInHand(int CardId)
        {
            for (int i = 0; i < Hand.Length; i++)
                if (Hand[i] == CardId)
                    return i;
            return Game.INVALID;
        }

        public PlaymatCard AddAbility(int CardId)
        {
            CardData CardData = Cards.GetCardData(CardId);

            int Attack = CardData.GetAttack();
            int Damage = CardData.GetBonus();
            int Defense = CardData.GetDefense();
            int Health = CardData.GetHealthOrLevel();

            PlaymatCard PlaymatCard = new PlaymatCard(CardId, Attack, Damage, Defense, Health);

            PlayedAbilities.Add(PlaymatCard);

            return PlaymatCard;
        }

        public PlaymatCard AddItem(int CardId)
        {
            CardData CardData = Cards.GetCardData(CardId);

            int Attack = CardData.GetAttack();
            int Damage = CardData.GetBonus();
            int Defense = CardData.GetDefense();
            int Health = CardData.GetHealthOrLevel();

            PlaymatCard PlaymatCard = new PlaymatCard(CardId, Attack, Damage, Defense, Health);

            PlayedItems.Add(PlaymatCard);

            return PlaymatCard;
        }

        public int GetUnexertedItemCardId()
        {
            foreach (PlaymatCard PlaymatCard in PlayedItems)
                if (!PlaymatCard.GetExerted())
                    return PlaymatCard.GetCardId();
            return -1;
        }

        public void SetAvatarExerted(bool AvatarExerted)
        {
            this.AvatarExerted = AvatarExerted;
        }

        public bool GetAvatarExerted()
        {
            return AvatarExerted;
        }

        public PlaymatCard GetUnexertedAbility(int CardId)
        {
            foreach (PlaymatCard PlaymatCard in PlayedAbilities)
                if (PlaymatCard.GetCardId() == CardId && !PlaymatCard.GetExerted())
                    return PlaymatCard;
            return null;
        }

        public PlaymatCard GetUnexertedItem(int CardId)
        {
            foreach (PlaymatCard PlaymatCard in PlayedItems)
                if (PlaymatCard.GetCardId() == CardId && !PlaymatCard.GetExerted())
                    return PlaymatCard;
            return null;
        }
    }
}
