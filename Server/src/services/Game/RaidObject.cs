﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Timers;
using TCGData;
using TCGData.Packets.Command;

namespace Server.src.services.Game
{
    public class RaidObject : Server
    {
        private const int BASE_DAMAGE = 1;

        private readonly CombatPlayerObject Attacker;
        private readonly CombatPlayerObject Defender;

        private readonly QuestObject Quest;

        private int Attack;
        private int Damage;
        private int Defense;

        private int CurrentTurnPlayerId;
        private int WinnerPlayerId;

        private bool AttackerEndedAttackTurn;
        private bool DefenderEndedTurn;
        private bool PlayerEndedDamageTurn;

        private readonly Timer TurnTimer;

        private const int TIMER_INTERVAL = 2000;

        private bool AttackerApplyingDamage;
        private bool DefenderApplyingDamage;

        public RaidObject(CombatPlayerObject Attacker, CombatPlayerObject Defender, QuestObject Quest)
        {
            this.Attacker = Attacker;
            this.Defender = Defender;

            this.Quest = Quest;

            CurrentTurnPlayerId = Game.INVALID;
            WinnerPlayerId = Game.INVALID;

            // Set default damage
            Damage = BASE_DAMAGE;

            TurnTimer = new Timer();
            TurnTimer.Interval = TIMER_INTERVAL;
            TurnTimer.Elapsed += TurnTimer_Elapsed;
        }

        private void TurnTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TurnTimer.Stop();

            NextPlayerTurn();
        }

        public void BeginRaid()
        {
            // Get the attacker's username
            string AttackerUsername = Attacker.GetUsername();

            // Construct packet containing attacker's username and the quest
            CommandPacket CommandPacket = new CommandPacket(new object[] { AttackerUsername, Quest.GetQuestId(), Attack, Defense });

            // send the packets to the players
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.START_RAID, CommandPacket);
            SendPacketToPlayers(PacketSender);

            // Since we are going to be calling NextPlayerTurn() set the current turn to the defender
            CurrentTurnPlayerId = Defender.GetPlayerId();

            // Get the attack card to be exerted (use TurnTimer to indirectly call RequestAttackCardToExert method.)
            TurnTimer.Start();
        }

        public void ExertAvatar(int PlayerId)
        {
            CommandPacket CommandPacket = null;

            if (PlayerId == Attacker.GetPlayerId())
            {
                Attacker.SetAvatarExerted(true);

                int AvatarId = Attacker.GetAvatarId();
                string Username = Attacker.GetUsername();

                // Add to the attack
                CardData CardData = Cards.GetCardData(AvatarId);
                Attack += CardData.GetAttack();

                CommandPacket = new CommandPacket(new object[] { Username, Game.EXERT_ATTACK });
            }
            else if (PlayerId == Defender.GetPlayerId())
            {
                Defender.SetAvatarExerted(true);

                int AvatarId = Defender.GetAvatarId();
                string Username = Defender.GetUsername();

                // Add to the defense
                CardData CardData = Cards.GetCardData(AvatarId);
                Defense += CardData.GetDefense();

                CommandPacket = new CommandPacket(new object[] { Username, Game.EXERT_DEFENSE });
            }

            // Tell clients what card was exerted and by who
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_AVATAR, CommandPacket);
            SendPacketToPlayers(PacketSender);

            // Go to the next turn (1 card/avatar exerted per turn)
            logSrv.LogDebug("RaidObject", "Avatar exerted, going to next turn");
            TurnTimer.Start();
        }

        public PlaymatCard GetPlaymatCard(int PlayerId, int KeyOrId, string Type)
        {
            PlaymatCard PlaymatCard = null;

            switch (Type)
            {
                case "Ability":
                    PlaymatCard = GetAbilityCard(PlayerId, KeyOrId);
                    break;
                case "Item":
                    PlaymatCard = GetItemCard(PlayerId, KeyOrId);
                    break;
                case "Unit":
                    PlaymatCard = GetUnitCard(PlayerId, KeyOrId);
                    break;
            }
            return PlaymatCard;
        }

        public void ExertCard(int PlayerId, int KeyOrId, string Type)
        {
            // Check if turn is valid
            if (PlayerId != CurrentTurnPlayerId)
            {
                logSrv.LogWarning("RaidObject", "Player attempted to exert a card when it wasn't their turn: " + PlayerId);
                return;
            }

            PlaymatCard PlaymatCard = GetPlaymatCard(PlayerId, KeyOrId, Type);

            if (PlaymatCard == null)
            {
                logSrv.LogWarning("RaidObject", "Invalid ExertCard, passing turn");
                PassTurn(PlayerId);
                return;
            }

            // Check if already exerted
            if (PlaymatCard.GetExerted())
            {
                logSrv.LogWarning("RaidObject", "Player attempted to exert an already exerted card");
                return;
            }

            // Set the card as exerted
            PlaymatCard.SetExerted(true);

            if (PlayerId == Attacker.GetPlayerId())
            {
                // Get the player's username
                string Username = Attacker.GetUsername();

                // Check if ended exerting attack card turn
                if (!AttackerEndedAttackTurn)
                {
                    // Add to the attack value
                    Attack += PlaymatCard.GetAttack();

                    // Tell clients what card was exerted and by who
                    CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_ATTACK, Username, KeyOrId, Type });
                    PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
                    SendPacketToPlayers(PacketSender);

                    logSrv.LogDebug("RaidObject", "Attacker exerted card (" + PlaymatCard.GetCardId() + ") with (" + PlaymatCard.GetAttack() + ") attack");
                }
                else if (!PlayerEndedDamageTurn)
                {
                    // Set PlayerEndedDamageTurn to true since player can only place 1 damage card
                    PlayerEndedDamageTurn = true;

                    // Exerting bonus damage
                    Damage += PlaymatCard.GetDamage();

                    // Tell clients what card was exerted and by who
                    CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_DAMAGE, Username, KeyOrId, Type });
                    PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
                    SendPacketToPlayers(PacketSender);

                    logSrv.LogDebug("RaidObject", "Attacker exerted card (" + PlaymatCard.GetCardId() + ") with (" + PlaymatCard.GetDamage() + ") bonus damage");
                }
            }
            else if (PlayerId == Defender.GetPlayerId())
            {
                // Get the player's username
                string Username = Defender.GetUsername();

                // Add to the defense
                Defense += PlaymatCard.GetDefense();

                // Tell clients what card was exerted and by who
                CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_DEFENSE, Username, KeyOrId, Type });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
                SendPacketToPlayers(PacketSender);

                logSrv.LogDebug("RaidObject", "Defender exerted card (" + PlaymatCard.GetCardId() + ") with (" + PlaymatCard.GetDefense() + ") defense");
            }

            // Go to the next player (1 card exerted per turn)
            TurnTimer.Start();

        }

        public PlaymatCard GetItemCard(int PlayerId, int CardId)
        {
            CardData CardData = Cards.GetCardData(CardId);

            if (Attacker.GetPlayerId() == PlayerId)
                return Attacker.GetUnexertedItem(CardId);
            if (Defender.GetPlayerId() == PlayerId)
                return Defender.GetUnexertedItem(CardId);
            return null;
        }

        public PlaymatCard GetAbilityCard(int PlayerId, int CardId)
        {
            CardData CardData = Cards.GetCardData(CardId);

            if (Attacker.GetPlayerId() == PlayerId)
                return Attacker.GetUnexertedAbility(CardId);
            if (Defender.GetPlayerId() == PlayerId)
                return Defender.GetUnexertedAbility(CardId);
            return null;
        }

        public PlaymatCard GetUnitCard(int PlayerId, int CardKey)
        {
            // Make sure the CardId is valid, otherwise, pass
            if (CardKey == Game.INVALID)
            {
                logSrv.LogWarning("RaidObject", "Invalid unit CardKey");
                return null;
            }

            return Quest.GetPlaymatCard(CardKey);
        }

        private void SendPacketToPlayers(PacketSender PacketSender)
        {
            if (Attacker.GetPlayerId() != Game.INVALID)
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(Attacker.GetPlayerId()));

            if (Defender.GetPlayerId() != Game.INVALID)
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(Defender.GetPlayerId()));
        }

        private int GetMaxExertableDefense()
        {
            //TODO: Finish this

            // Get the defense of the player's avatar
            CardData AvatarData = Cards.GetCardData(Defender.GetAvatarId());

            return AvatarData.GetDefense();
        }

        private void RequestAttackerCardToExert()
        {
            if (Attacker.GetPlayerId() == Game.INVALID)
            {
                // Check if attacking using units or avatar
                if (Quest.GetNumberOfPlayerTwoCards() > 0)
                {
                    logSrv.LogDebug("RaidObject", "NPC (attacker) is attempting to exert card");

                    // Get the card the NPC wants to use and exert it
                    int MaxPossibleDefense = GetMaxExertableDefense();
                    int AttackCardId = aiSrv.GetAttackCardToExert(Quest.GetPlayerTwoCards(), Attack, MaxPossibleDefense);
                    logSrv.LogDebug("RaidObject", "NPC exerting card: " + AttackCardId);
                    ExertCard(Attacker.GetPlayerId(), AttackCardId, "Unit");
                }
                else
                {
                    if (!Attacker.GetAvatarExerted())
                        ExertAvatar(Attacker.GetPlayerId());
                    else
                        PassTurn(Attacker.GetPlayerId());
                    logSrv.LogDebug("RaidObject", "NPC (attacker) is attempting to exert avatar");
                }
                return;
            }

            // Ask player to choose an attack card
            CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_ATTACK });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(Attacker.GetPlayerId()));
        }

        private void RequestDamageCardToExert(int PlayerId)
        {
            CurrentTurnPlayerId = PlayerId;

            if (PlayerId == Game.INVALID)
            {
                // Get the card the NPC wants to use and exert it
                int DamageCardKey = aiSrv.GetDamageCardToExert(Quest.GetPlayerTwoCards());
                ExertCard(PlayerId, DamageCardKey, "Unit");
                return;
            }

            // Ask player to choose a damage card
            CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_DAMAGE });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerId));
        }

        private void RequestDefenderCardToExert()
        {
            if (Defender.GetPlayerId() == Game.INVALID)
            {
                // Get the card the NPC wants to use and exert it

                // If no unit cards available, exert avatar
                if (Quest.GetPlayerTwoCards().Count == 0)
                {
                    int ItemKeyToExert = Defender.GetUnexertedItemCardId();

                    if (!Defender.GetAvatarExerted())
                        ExertAvatar(Defender.GetPlayerId());
                    else if (ItemKeyToExert != -1)
                        ExertCard(Defender.GetPlayerId(), ItemKeyToExert, "Item");
                    else
                        PassTurn(Defender.GetPlayerId());
                }
                else
                {
                    int DefenseCardKey = aiSrv.GetDefenseCardToExert(Quest.GetPlayerTwoCards());

                    // Make sure the key is valid
                    if (DefenseCardKey > -1)
                    {
                        logSrv.LogDebug("RaidObject", "NPC exerted defense card key: " + DefenseCardKey);
                        ExertCard(Defender.GetPlayerId(), DefenseCardKey, "Unit");
                    }
                    else
                    {
                        PassTurn(Defender.GetPlayerId());
                    }
                }
                return;
            }

            // Ask player to choose a defense card
            CommandPacket CommandPacket = new CommandPacket(new object[] { Game.EXERT_DEFENSE });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_EXERT_CARD, CommandPacket);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(Defender.GetPlayerId()));
        }

        private void TellPlayerToTakeDamage(int PlayerId, int Damage)
        {
            if (PlayerId != Game.INVALID)
            {
                //Player is taking damage
                CommandPacket CommandPacket = new CommandPacket(new object[] { Damage });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_TAKE_DAMAGE, CommandPacket);
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerId));
            }
            else
            {
                if (Quest.GetNumberOfPlayerTwoCards() > 0)
                {
                    // NPC is taking damage
                    for (int i = 0; i < Damage; i++)
                    {
                        int CardToDamage = aiSrv.GetCardToDamage(Quest.GetPlayerTwoCards());
                        DealDamageToCard(PlayerId, CardToDamage);
                    }
                }
                else
                {
                    // We will have to damage the avatar instead
                    for (int i = 0; i < Damage; i++)
                        DealDamageToAvatar(PlayerId);
                }
            }
        }

        public void PlayTactic(int PlayerId, int CardId)
        {
            //@TODO: Implement Tactic functionality
        }

        public void DealDamageToAvatar(int PlayerId)
        {
            logSrv.LogDebug("RaidObject", "Player dealing damage to their avatar (" + PlayerId + ")");

            // Get the CombatPlayerObject we are dealing with
            CombatPlayerObject Player = PlayerId == Attacker.GetPlayerId() ? Attacker : Defender;

            // Get the player's username
            string Username = Player.GetUsername();

            // Deal damage to the avatar
            int AvatarHealth = Player.GetAvatarHealth();

            // Subtract 1 health from the avatar health
            AvatarHealth--;

            // Update the avatar with their new health
            Player.SetAvatarHealth(AvatarHealth);

            // Update the players with health change
            CommandPacket ChangeAvatarHealthPacket = new CommandPacket(new object[] { Username, AvatarHealth });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_HEALTH, ChangeAvatarHealthPacket);
            SendPacketToPlayers(PacketSender);

            // Check if the avatar died
            if (AvatarHealth == 0)
            {
                // Other player won the game
                Game Game = gameSrv.GetGamePlayerIsIn(PlayerId);

                // This will be the opposite of the current PlayerId
                string WinnerUsername = PlayerId == Attacker.GetPlayerId() ? Defender.GetUsername() : Attacker.GetUsername();

                Game.EndTheGame(WinnerUsername);
            }
            else
            {
                // Check if player still has damage to deal
                if (Damage == BASE_DAMAGE)
                {
                    if (PlayerId == Attacker.GetPlayerId())
                        AttackerApplyingDamage = false;
                    else if (PlayerId == Defender.GetPlayerId())
                        DefenderApplyingDamage = false;
                }
                else
                {
                    // Keep going until we go down to 1
                    Damage--;

                    // Tell the player to keep taking damage
                    TellPlayerToTakeDamage(PlayerId, Damage);
                }

                // We are done with the raid, all damage has been applied
                if (!AttackerApplyingDamage && !DefenderApplyingDamage)
                    gameSrv.EndRaid(PlayerId);
            }
        }

        public void DealDamageToCard(int PlayerId, int CardKey)
        {
            // Get the playmat card
            PlaymatCard CardToDamage = Quest.GetPlaymatCard(CardKey);

            int CardHealth = CardToDamage.GetHealth();

            if (CardHealth == 1)
            {
                // Card is dead
                Quest.RemovePlaymatCard(CardKey);

                // Tell clients to destroy the card
                CommandPacket CommandPacket = new CommandPacket(new object[] { CardKey });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_DESTROY_CARD, CommandPacket);
                SendPacketToPlayers(PacketSender);
            }
            else
            {
                // Update the health of the card

                CardHealth--;
                CardToDamage.SetHealth(CardHealth);

                // Tell clients to update the card health
                CommandPacket CommandPacket = new CommandPacket(new object[] { CardKey });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_DAMAGE_CARD, CommandPacket);
                SendPacketToPlayers(PacketSender);
            }

            // Check if player still has damage to deal
            if (Damage == BASE_DAMAGE)
            {
                if (PlayerId == Attacker.GetPlayerId())
                    AttackerApplyingDamage = false;
                else if (PlayerId == Defender.GetPlayerId())
                    DefenderApplyingDamage = false;
            }
            else
            {
                // Keep going until we go down to 1
                Damage--;

                // Tell the player to keep taking damage
                TellPlayerToTakeDamage(PlayerId, Damage);
            }

            // We are done with the raid, all damage has been applied
            if (!AttackerApplyingDamage && !DefenderApplyingDamage)
                gameSrv.EndRaid(PlayerId);
        }

        public void PassTurn(int PlayerId)
        {
            // Make sure they are allowed to pass
            if (CurrentTurnPlayerId != PlayerId)
            {
                logSrv.LogWarning("RaidObject", "Player attempted to skip their turn. It's not their turn.");
                return;
            }

            if (PlayerId == Attacker.GetPlayerId())
            {
                if (!AttackerEndedAttackTurn)
                    AttackerEndedAttackTurn = true;
                else if (Attack > Defense && !PlayerEndedDamageTurn)
                    PlayerEndedDamageTurn = true;
            }
            else if (PlayerId == Defender.GetPlayerId())
            {
                if (!DefenderEndedTurn)
                    DefenderEndedTurn = true;
                else if (Defense > Attack && !PlayerEndedDamageTurn)
                    PlayerEndedDamageTurn = true;
            }

            logSrv.LogDebug("RaidObject", "Player passed turn: " + PlayerId);

            // We updated the pass info, go to next turn
            TurnTimer.Start();
        }

        private void CalculateWinner()
        {
            // Tell players we are done calculating winner
            SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.RAID_SHOW_OUTCOME, null));

            if (Attack > Defense)
            {
                // Attack won --> Ask Attacker to exert Damage card
                RequestDamageCardToExert(Attacker.GetPlayerId());

                // Set the attacker as the winner
                WinnerPlayerId = Attacker.GetPlayerId();

                logSrv.LogDebug("RaidObject", "Attack won raid (Attack: " + Attack + ", Defense: " + Defense + ")");
            }
            else if (Attack < Defense)
            {
                // Defense won --> Ask Defender to exert Damage card
                RequestDamageCardToExert(Defender.GetPlayerId());

                // Set the defender as the winner
                WinnerPlayerId = Defender.GetPlayerId();

                logSrv.LogDebug("RaidObject", "Attack lost raid (Attack: " + Attack + ", Defense: " + Defense + ")");
            }
            else
            {
                // Draw --> Attacker and Defender both take bonus damage (Defender first)
                AttackerApplyingDamage = true;
                DefenderApplyingDamage = true;

                TellPlayerToTakeDamage(Defender.GetPlayerId(), BASE_DAMAGE);
                TellPlayerToTakeDamage(Attacker.GetPlayerId(), BASE_DAMAGE);

                logSrv.LogDebug("RaidObject", "Raid ended in draw (Attack: " + Attack + ", Defense: " + Defense + ")");
            }
        }

        private void NextPlayerTurn()
        {
            logSrv.LogDebug("RaidObject", "Starting next player turn...");
            logSrv.LogDebug("RaidObject", "AttackerEndedAttackTurn=" + AttackerEndedAttackTurn + ", DefenderEndedTurn=" + DefenderEndedTurn);

            // Check if we have to determine winner or deal dmg
            if (AttackerEndedAttackTurn && DefenderEndedTurn)
            {
                // Determine the winner
                if (PlayerEndedDamageTurn)
                {
                    if (Attack > Defense)
                    {
                        // Now with the damage updated, tell defender to take damage
                        logSrv.LogDebug("RaidObject", "Player lost (" + Defender.GetPlayerId() + "), taking damage (" + Damage + ")");

                        DefenderApplyingDamage = true;
                        TellPlayerToTakeDamage(Defender.GetPlayerId(), Damage);
                    }
                    else if (Attack < Defense)
                    {
                        // Now with the damage updated, tell defender to take damage
                        logSrv.LogDebug("RaidObject", "Player lost (" + Attacker.GetPlayerId() + "), taking damage (" + Damage + ")");

                        AttackerApplyingDamage = true;
                        TellPlayerToTakeDamage(Attacker.GetPlayerId(), Damage);
                    }
                }
                else
                {
                    CalculateWinner();
                }
                return;
            }

            // Players are still exerting attack and defense cards
            if (CurrentTurnPlayerId == Attacker.GetPlayerId())
            {
                // Update the current turn player id to the defender
                CurrentTurnPlayerId = Defender.GetPlayerId();

                // Check if defender has another defense card to exert
                if (!DefenderEndedTurn)
                    RequestDefenderCardToExert();
                else
                {
                    // Defender doesn't want to place anything, attacker's turn again
                    CurrentTurnPlayerId = Attacker.GetPlayerId();
                    RequestAttackerCardToExert();
                }
            }
            else if (CurrentTurnPlayerId == Defender.GetPlayerId())
            {
                // Update the current turn player id to the attacker
                CurrentTurnPlayerId = Attacker.GetPlayerId();

                if (!AttackerEndedAttackTurn)
                    RequestAttackerCardToExert();
                else
                {
                    // Attacker doesn't want to place anything, defender's turn again
                    CurrentTurnPlayerId = Defender.GetPlayerId();
                    RequestDefenderCardToExert();
                }
            }
            logSrv.LogDebug("RaidObject", "New player turn: " + CurrentTurnPlayerId);
        }

        public int GetCurrentTurnPlayerId()
        {
            return CurrentTurnPlayerId;
        }

        public QuestObject GetQuest()
        {
            return Quest;
        }

        public CombatPlayerObject GetAttacker()
        {
            return Attacker;
        }

        public int GetWinnerId()
        {
            return WinnerPlayerId;
        }
    }
}
