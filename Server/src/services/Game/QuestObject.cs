﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData;

namespace Server.src.services.Game
{
    public class QuestObject
    {
        private int QuestId;

        private int PlayerOneCompletedLevels;
        private int PlayerTwoCompletedLevels;

        private readonly SortedDictionary<int, PlaymatCard> PlayerOneCards;
        private readonly SortedDictionary<int, PlaymatCard> PlayerTwoCards;

        public QuestObject(int QuestId)
        {
            this.QuestId = QuestId;

            PlayerOneCards = new SortedDictionary<int, PlaymatCard>();
            PlayerTwoCards = new SortedDictionary<int, PlaymatCard>();
        }

        public int GetQuestId()
        {
            return QuestId;
        }

        public void SetQuestId(int QuestId)
        {
            this.QuestId = QuestId;
        }

        public int GetNumberOfPlayerOneCards()
        {
            return PlayerOneCards.Count;
        }

        public int GetNumberOfPlayerTwoCards()
        {
            return PlayerTwoCards.Count;
        }

        public int GetPlayerOneTotalAttack()
        {
            int Attack = 0;

            //@TODO: If no cards, use Avatar value

            foreach (PlaymatCard Card in PlayerOneCards.Values)
            {
                if (!Card.GetExerted())
                    Attack += Card.GetAttack();
            }

            return Attack;
        }

        public int GetPlayerTwoTotalAttack()
        {
            int Attack = 0;

            //@TODO: If no cards, use Avatar value

            foreach (PlaymatCard Card in PlayerTwoCards.Values)
            {
                if (!Card.GetExerted())
                    Attack += Card.GetAttack();
            }

            return Attack;
        }

        public int GetPlayerOneTotalDefense(CombatPlayerObject PlayerOne)
        {
            int Defense = 0;

            if (PlayerOneCards.Count > 0)
            {
                // Get total defense of unit cards

                foreach (PlaymatCard Card in PlayerOneCards.Values)
                {
                    if (!Card.GetExerted())
                        Defense += Card.GetDefense();
                }
            }
            else
            {
                // Add defense of the avatar if not exerted
                if (!PlayerOne.GetAvatarExerted())
                {
                    CardData Avatar = Cards.GetCardData(PlayerOne.GetAvatarId());
                    Defense += Avatar.GetDefense();
                }

                // Get defense of all not exerted unit cards
                foreach (PlaymatCard AbilityCard in PlayerOne.GetPlayedAbilities())
                {
                    if (!AbilityCard.GetExerted())
                        Defense += AbilityCard.GetDefense();
                }
            }
            return Defense;
        }

        public int GetPlayerTwoTotalDefense()
        {
            int Defense = 0;

            //@TODO: If no cards, use Avatar value

            foreach (PlaymatCard Card in PlayerTwoCards.Values)
            {
                if (!Card.GetExerted())
                    Defense += Card.GetDefense();
            }

            return Defense;
        }

        public PlaymatCard AddPlayerOnePlacedCard(int Key, int CardId, int Attack, int Damage, int Defense, int Health)
        {
            PlaymatCard PlaymatCard = new PlaymatCard(CardId, Attack, Damage, Defense, Health);
            PlayerOneCards.Add(Key, PlaymatCard);
            return PlaymatCard;
        }

        public PlaymatCard AddPlayerTwoPlacedCard(int Key, int CardId, int Attack, int Damage, int Defense, int Health)
        {
            PlaymatCard PlaymatCard = new PlaymatCard(CardId, Attack, Damage, Defense, Health);
            PlayerTwoCards.Add(Key, PlaymatCard);
            return PlaymatCard;
        }

        public SortedDictionary<int, PlaymatCard> GetPlayerOneCards()
        {
            return PlayerOneCards;
        }

        public SortedDictionary<int, PlaymatCard> GetPlayerTwoCards()
        {
            return PlayerTwoCards;
        }

        public PlaymatCard GetPlaymatCard(int Key)
        {
            if (PlayerOneCards.ContainsKey(Key))
                return PlayerOneCards[Key];
            if (PlayerTwoCards.ContainsKey(Key))
                return PlayerTwoCards[Key];
            return null;
        }

        public void RemovePlaymatCard(int Key)
        {
            if (PlayerOneCards.ContainsKey(Key))
                PlayerOneCards.Remove(Key);
            else if (PlayerTwoCards.ContainsKey(Key))
                PlayerTwoCards.Remove(Key);
        }

        public void SetPlayerOneCompletedLevels(int PlayerOneCompletedLevels)
        {
            this.PlayerOneCompletedLevels = PlayerOneCompletedLevels;
        }

        public int GetPlayerOneCompletedLevels()
        {
            return PlayerOneCompletedLevels;
        }

        public void SetPlayerTwoCompletedLevels(int PlayerTwoCompletedLevels)
        {
            this.PlayerTwoCompletedLevels = PlayerTwoCompletedLevels;
        }

        public int GetPlayerTwoCompletedLevels()
        {
            return PlayerTwoCompletedLevels;
        }

        public int GenerateNewValidCardKey()
        {
            int IdentifierId = 0;
            while (PlayerOneCards.ContainsKey(IdentifierId) || PlayerTwoCards.ContainsKey(IdentifierId))
                IdentifierId++;
            return IdentifierId;
        }
    }
}
