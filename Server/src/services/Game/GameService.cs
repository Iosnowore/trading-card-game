﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Player;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TCGData.Packets.Game;
using TCGData.Services;

namespace Server.src.services.Game
{
    public class GameService : Server
    {
        private const int NPC_ID = -1;
        private readonly List<Game> GAMES;

        public GameService()
        {
            GAMES = new List<Game>();

            logSrv.Log("GameService", "Loaded");
        }

        public Game GetGamePlayerIsIn(int PlayerId)
        {
            foreach (Game Game in GAMES)
            {
                if (Game.GetPlayerOneId() == PlayerId || Game.GetPlayerTwoId() == PlayerId)
                {
                    return Game;
                }
            }
            return null;
        }

        public void RemovePlayerFromAnyGames(int PlayerId)
        {
            Game PlayersGame = GetGamePlayerIsIn(PlayerId);

            if (PlayersGame != null)
                RemovePlayerFromGame(PlayersGame, PlayerId);
        }

        public void SendRedrawHandPacket(int PlayerId, int[] HandCardIds)
        {
            CommandPacket RedrawHandPacket = new CommandPacket(new object[] { HandCardIds });
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDRAW_HAND, RedrawHandPacket), conSrv.GetClientByID(PlayerId));
        }

        public void HandleRedrawHand(int PlayerId, bool Redraw)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);

            // Let the player know that the NPC is choosing their hand
            if (PlayerId != PlayerGame.GetPlayerOneId())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDRAW_HAND, null), conSrv.GetClientByID(PlayerGame.GetPlayerOneId()));
            }

            if (Redraw)
            {
                // Redraw the player's hand
                PlayerGame.RedrawHand(PlayerId);

                CombatPlayerObject CombatPlayer = PlayerGame.GetCombatPlayerObject(PlayerId);

                int[] NewHand = CombatPlayer.GetHandCardIds();

                // Send the player the new packet
                if (PlayerId == PlayerGame.GetPlayerOneId() || !PlayerGame.GameIsSinglePlayer())
                {
                    HandPacket HandPacket = new HandPacket(NewHand);
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDRAW_HAND, HandPacket), conSrv.GetClientByID(PlayerId));
                }
            }

            // Go to the next player
            if (PlayerId == PlayerGame.GetPlayerOneId())
            {
                int PlayerTwoId = PlayerGame.GetPlayerTwoId();

                // Ask player 2 if they want to redraw
                if (PlayerGame.GameIsSinglePlayer())
                {
                    CombatPlayerObject NpcObject = PlayerGame.GetCombatPlayerObject(PlayerTwoId);

                    // Don't send a packet since player 2 is an AI
                    int[] Hand = NpcObject.GetHandCardIds();
                    SortedDictionary<int, int> Deck = NpcObject.GetDeck();

                    // Get if the AI wants to redraw
                    Redraw = aiSrv.RedrawHand(Deck, Hand);

                    // Recursion for PlayerTwo now
                    HandleRedrawHand(PlayerTwoId, Redraw);
                }
                else
                {
                    // Send the REDRAW_HAND packet to the player
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDRAW_HAND, null), conSrv.GetClientByID(PlayerGame.GetPlayerTwoId()));
                }
            }
            else if (PlayerId == PlayerGame.GetPlayerTwoId())
            {
                // Start the next phase
                PlayerGame.StartDrawPhase();
            }
        }

        public void HandleLeaveGame(int PlayerId)
        {
            // Get the Game object that the player is in.
            Game PlayersGame = GetGamePlayerIsIn(PlayerId);

            // Remove the player
            RemovePlayerFromGame(PlayersGame, PlayerId);
        }

        private void RemovePlayerFromGame(Game PlayersGame, int PlayerId)
        {
            if (PlayersGame.GameIsSinglePlayer())
            {
                // Delete chatroom
                string ChatRoomTitle = PlayersGame.GetChatroomTitle();
                Chat.ChatRoom ChatRoom = chatRoomSrv.GetChatRoomMatch(ChatRoomTitle);
                chatRoomSrv.DeleteChatRoom(ChatRoom);

                // Tell the game that we are stopping it
                PlayersGame.EndTheGame(null);

                // Delete game
                GAMES.Remove(PlayersGame);
            }
        }

        public void CreateNewGame(int[] PlayerIds, string GameType)
        {
            // This is multiplayer, get the Decks of each player
            PlayerObject PlayerOne = playerSrv.GetPlayerObject(PlayerIds[0]);
            DeckPacket PlayerOneDeck = PlayerOne.GetSelectedDeck();

            PlayerObject PlayerTwo = playerSrv.GetPlayerObject(PlayerIds[1]);
            DeckPacket PlayerTwoDeck = PlayerTwo.GetSelectedDeck();

            SortedDictionary<int, int>[] PlayerDecks = { PlayerOneDeck.GetCardIds(), PlayerTwoDeck.GetCardIds() };

            // Create the game give the deck cards
            CreateNewGame(PlayerIds, PlayerDecks, GameType, -1);
        }

        public void CreateNewGame(int[] PlayerIds, SortedDictionary<int, int>[] PlayerDecks, string GameType, int ScenarioId)
        {
            CombatPlayerObject[] Players = new CombatPlayerObject[PlayerIds.Length];

            for (int i = 0; i < PlayerIds.Length; i++)
            {
                int AvatarId = GetAvatarId(PlayerDecks[i]);

                string Username;

                // PlayerTwo is an NPC
                if (PlayerIds[i] == -1)
                {
                    // Set the NPC username to the avatar
                    CardData CardData = Cards.GetCardData(AvatarId);
                    Username = CardData.GetTitle();
                }
                else
                {
                    Username = playerSrv.GetUsername(PlayerIds[i]);
                }

                Players[i] = new CombatPlayerObject(PlayerIds[i], AvatarId, Username, PlayerDecks[i]);
            }

            Game Game = new Game(Players[0], Players[1], GameType, ScenarioId);
            GAMES.Add(Game);
        }

        private int GetAvatarId(SortedDictionary<int, int> Deck)
        {
            int AvatarId = -1;
            CardData CardData;

            foreach (int CardId in Deck.Keys)
            {
                CardData = Cards.GetCardData(CardId);

                if (CardData.GetType().Equals("Avatar"))
                {
                    AvatarId = CardId;
                    break;
                }
            }
            return AvatarId;
        }

        public void HandleLoadedGame(int PlayerId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);

            // Check if the player who is ready is the starting player
            if (PlayerId == PlayerGame.GetCurrentTurnId())
            {
                // Ask starting player if they would like to redraw
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDRAW_HAND, null), conSrv.GetClientByID(PlayerId));
            }
        }

        public void HandleMainPhaseDone(int PlayerId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.ChangePlayerTurn();
        }

        public void HandleSelectedQuest(int PlayerId, int CardId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.AttemptQuestForPlayer(PlayerId, CardId);
        }

        public void HandleStartRaid(int PlayerId, int QuestId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.StartRaid(PlayerId, QuestId);
        }

        public void HandleExertCard(int PlayerId, int CardKey, int ExertType, string CardType)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);

            switch (ExertType)
            {
                case Game.EXERT_ATTACK:
                    PlayerGame.ExertAttackCard(PlayerId, CardKey, CardType);
                    break;
                case Game.EXERT_DAMAGE:
                    PlayerGame.ExertDamageCard(PlayerId, CardKey, CardType);
                    break;
                case Game.EXERT_DEFENSE:
                    PlayerGame.ExertDefenseCard(PlayerId, CardKey, CardType);
                    break;
                default:
                    logSrv.LogWarning("GameService", "Player (" + PlayerId + ") played an invalid ExertType: " + ExertType);
                    break;
            }
        }

        public void HandleExertAvatar(int PlayerId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.ExertAvatar(PlayerId);
        }

        public void HandleDamageAvatar(int PlayerId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.DealDamageToAvatar(PlayerId);
        }

        public void HandleTakeDamage(int PlayerId, int CardKey)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.DealDamagetoCard(PlayerId, CardKey);
        }

        public void HandlePlayTactic(int PlayerId, int CardId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.PlayTactic(PlayerId, CardId);
        }

        public void HandlePassTurn(int PlayerId)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerId);
            PlayerGame.PassTurn(PlayerId);
        }

        public void EndRaid(int PlayerInRaid)
        {
            Game PlayerGame = GetGamePlayerIsIn(PlayerInRaid);
            PlayerGame.EndRaid();
        }

        private SortedDictionary<int, int> GetNpcDeck(int ScenarioId)
        {
            string CardsString = DatatableReader.GetValue("npc_decks", "id", ScenarioId.ToString(), "cards");
            string[] Cards = CardsString.Split("|");

            SortedDictionary<int, int> NpcDeck = new SortedDictionary<int, int>();

            string[] CardArr;
            foreach (string Card in Cards)
            {
                CardArr = Card.Split(":");

                int CardId = int.Parse(CardArr[0]);
                int CardQuantity = int.Parse(CardArr[1]);
                NpcDeck.Add(CardId, CardQuantity);
            }

            return NpcDeck;
        }

        public void HandleStartScenario(int PlayerId, int ScenarioId, string DeckTitle)
        {
            // Get the selected cards for the player
            PlayerObject Player = playerSrv.GetPlayerObject(PlayerId);
            DeckPacket DeckPacket = Player.GetSelectedDeck();
            SortedDictionary<int, int> PlayerDeck = DeckPacket.GetCardIds();
            SortedDictionary<int, int> NpcDeck = GetNpcDeck(ScenarioId);

            // Get the cards that the NPC is going to use
            logSrv.LogDebug("GameService", "Creating scenario with Id: " + ScenarioId);

            // Setup the player ids
            int[] PlayerIds = new int[] { PlayerId, NPC_ID };

            SortedDictionary<int, int>[] Decks = { PlayerDeck, NpcDeck };

            // Create the new match
            CreateNewGame(PlayerIds, Decks, "scenario", ScenarioId);
        }
    }
}
