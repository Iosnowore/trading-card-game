﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.Game;

namespace Server.src.services.Game
{
    public class Game : Server
    {
        private readonly CombatPlayerObject PlayerOne;
        private readonly CombatPlayerObject PlayerTwo;

        private readonly QuestObject QuestOne;
        private readonly QuestObject QuestTwo;

        private RaidObject RaidObject;

        public const int MAX_HAND_CARDS = 6;
        public const int AVATAR_START_POWER = 3;
        public const int INVALID = -1;

        private const int DRAW_PHASE_CARDS = 2;
        private const int TIMER_INTERVAL = 1000;

        public const int EXERT_ATTACK = 0;
        public const int EXERT_DAMAGE = 1;
        public const int EXERT_DEFENSE = 2;

        private readonly string ChatRoomTitle;
        private readonly string GameType;

        private bool RaidOnCoolDown;

        private int CurrentTurnPlayerId = INVALID;

        private readonly System.Timers.Timer PhaseTimer;
        private readonly System.Timers.Timer NpcTimer;

        private bool GameFinished;

        private int PendingAbilityQuestId;

        private PHASES CurrentPhase;

        private enum PHASES
        {
            DRAW_PHASE,
            QUEST_PHASE,
            READY_PHASE,
            MAIN_PHASE
        }

        public Game(CombatPlayerObject PlayerOne, CombatPlayerObject PlayerTwo, string GameType, int ScenarioId)
        {
            this.PlayerOne = PlayerOne;
            this.PlayerTwo = PlayerTwo;

            PhaseTimer = new System.Timers.Timer();
            PhaseTimer.Interval = TIMER_INTERVAL;
            PhaseTimer.Elapsed += PhaseTimer_Elapsed;

            NpcTimer = new System.Timers.Timer();
            NpcTimer.Interval = TIMER_INTERVAL;
            NpcTimer.Elapsed += NpcTimer_Elapsed;

            // Set the game type
            this.GameType = GameType;

            // Set all hand ids to -1 to show that they are empty
            for (int i = 0; i < MAX_HAND_CARDS; i++)
            {
                PlayerOne.SetHandCardId(i, INVALID);
                PlayerTwo.SetHandCardId(i, INVALID);
            }

            // Determine who goes first
            CurrentTurnPlayerId = GetIdOfFirstTurn();

            // Shuffle each player's deck
            PlayerOne.ShuffleDeck();
            PlayerTwo.ShuffleDeck();

            // Play each player's quest (will be level 2's)
            int QuestIdOne = PlayerOne.DrawQuest();
            int QuestIdTwo = PlayerTwo.DrawQuest();

            QuestOne = new QuestObject(QuestIdOne);
            QuestTwo = new QuestObject(QuestIdTwo);

            // Each player draws their hand
            PlayerOne.DrawCardIds(MAX_HAND_CARDS);
            PlayerTwo.DrawCardIds(MAX_HAND_CARDS);

            // Send the player the game info so they can update it on their end
            int PlayerOneDeckQty = PlayerOne.GetNumberOfPlayableCardsInDeck();
            int PlayerTwoDeckQty = PlayerTwo.GetNumberOfPlayableCardsInDeck();

            LoadGamePacket LoadGamePacket = new LoadGamePacket(PlayerOne.GetAvatarId(), PlayerTwo.GetAvatarId(), PlayerOne.GetUsername(), PlayerTwo.GetUsername(), PlayerOne.GetHandCardIds(), QuestIdOne, QuestIdTwo, PlayerOneDeckQty, PlayerTwoDeckQty, PlayerOne.GetNumberOfCardsInHand(), PlayerTwo.GetNumberOfCardsInHand(), ScenarioId);
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.LOAD_GAME, LoadGamePacket), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (PlayerTwo.GetPlayerId() != INVALID)
            {
                // PlayerTwo is a player, send them a packet
                LoadGamePacket = new LoadGamePacket(PlayerTwo.GetAvatarId(), PlayerOne.GetAvatarId(), PlayerTwo.GetUsername(), PlayerOne.GetUsername(), PlayerTwo.GetHandCardIds(), QuestIdTwo, QuestIdOne, PlayerTwoDeckQty, PlayerOneDeckQty, PlayerTwo.GetNumberOfCardsInHand(), PlayerOne.GetNumberOfCardsInHand(), ScenarioId);
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.LOAD_GAME, LoadGamePacket), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            ChatRoomTitle = PlayerOne.GetPlayerId() + "vs" + PlayerTwo.GetPlayerId();
            chatRoomSrv.CreateChatRoom(ChatRoomTitle, 2, ChatTypes.ChatType.MATCH);
            chatRoomSrv.AddClientToChatRoom(ChatRoomTitle, conSrv.GetClientByID(PlayerOne.GetPlayerId()));
            //chatRoomSrv.SendPacketToChatRoom(ChatRoomTitle, new PacketSender(SERVER_ID, Commands.MATCH_CHAT, new CommandPacket(new string[] { GameData.ServerId, "* You draw " })));

            logSrv.LogDebug("Game", "Created new Game");
        }

        private void NpcTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Get the CardId NPC wants to use
            int CardId = aiSrv.GetCardIdToDraw(PlayerTwo.GetHandCardIds(), PlayerTwo.GetAvatarPower(), QuestOne, QuestTwo, RaidObject);
            logSrv.LogDebug("Game", "NPC placing CardId: " + CardId);

            // Make sure the card of valid id
            if (CardId != INVALID)
            {
                int QuestIdToRaid = -1;

                // Check if card is a unit
                CardData CardData = Cards.GetCardData(CardId);

                // Find what quest to place the card
                if (CardData.GetType().Equals("Unit"))
                    QuestIdToRaid = aiSrv.GetQuestToPlaceUnit(CardId, QuestOne, QuestTwo);

                logSrv.LogDebug("Game", "NPC placing card at quest: " + QuestIdToRaid);

                // Place the card
                PlaceCard(INVALID, CardId, QuestIdToRaid);
            }
            else
            {
                if (RaidObject == null && !RaidOnCoolDown)
                {
                    // Check if NPC should raid

                    int QuestId = aiSrv.GetQuestToRaid(QuestOne, QuestTwo, PlayerOne);

                    if (QuestId != -1)
                    {
                        RaidOnCoolDown = true;

                        StartRaid(INVALID, QuestId);

                        // We are pausing the main phase, stop the timer
                        NpcTimer.Stop();
                        return; // Return since we don't want to end the turn
                    }
                }
                // We have no more cards to place and we are not raiding and we have, just change the turn
                NpcTimer.Stop();
                ChangePlayerTurn();
                RaidOnCoolDown = false;
            }
        }

        public void StartDrawPhase()
        {
            CurrentPhase = PHASES.DRAW_PHASE;

            // Update other player of DrawPhase if it's not an NPC
            if (CurrentTurnPlayerId != PlayerOne.GetPlayerId())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.DRAW_PHASE, null), conSrv.GetClientByID(PlayerOne.GetPlayerId()));
            }
            else if (CurrentTurnPlayerId != PlayerTwo.GetPlayerId() && !GameIsSinglePlayer())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.DRAW_PHASE, null), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            // Start the creation of HandPacket
            HandPacket HandPacket = null;

            // Check if the player needs any cards to draw
            if (CurrentTurnPlayerId == PlayerOne.GetPlayerId())
            {
                if (PlayerOne.GetNumberOfCardsInHand() < MAX_HAND_CARDS)
                {
                    // Draw cards
                    bool DrewCards = PlayerOne.DrawCardIds(DRAW_PHASE_CARDS);

                    // Player ran out of cards from deck, therefore lost.
                    if (!DrewCards)
                    {
                        string WinnerUsername = PlayerTwo.GetUsername();
                        EndTheGame(WinnerUsername);
                        return;
                    }
                    SendUpdatedDeckStats(PlayerOne.GetPlayerId());
                }
                HandPacket = new HandPacket(PlayerOne.GetHandCardIds());

                // Replenish avatar power
                int TotalPower = AVATAR_START_POWER + PlayerOne.GetCompletedQuests() + PlayerTwo.GetCompletedQuests();
                PlayerOne.SetAvatarPower(TotalPower);

                // Tell client's to update the avatar power UI bars
                CommandPacket CommandPacket = new CommandPacket(new object[] { PlayerOne.GetUsername(), PlayerOne.GetAvatarPower() });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_POWER, CommandPacket);

                // Send to first player
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                // Send packet to PlayerTwo if not an AI
                if (!GameIsSinglePlayer())
                    conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }
            else if (CurrentTurnPlayerId == PlayerTwo.GetPlayerId())
            {
                if (PlayerTwo.GetNumberOfCardsInHand() < MAX_HAND_CARDS)
                {
                    // Draw cards
                    bool DrewCards = PlayerTwo.DrawCardIds(DRAW_PHASE_CARDS);

                    // Player ran out of cards from deck, therefore lost.
                    if (!DrewCards)
                    {
                        string WinnerUsername = PlayerTwo.GetUsername();
                        EndTheGame(WinnerUsername);
                        return;
                    }
                    SendUpdatedDeckStats(PlayerTwo.GetPlayerId());
                }
                HandPacket = new HandPacket(PlayerTwo.GetHandCardIds());

                // Replenish avatar power
                int TotalPower = AVATAR_START_POWER + PlayerOne.GetCompletedQuests() + PlayerTwo.GetCompletedQuests();
                PlayerTwo.SetAvatarPower(TotalPower);

                // Tell client's to update the avatar power UI bars
                CommandPacket CommandPacket = new CommandPacket(new object[] { PlayerTwo.GetUsername(), PlayerTwo.GetAvatarPower() });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_POWER, CommandPacket);

                // Send to first player
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                // Send packet to PlayerTwo if not an AI
                if (!GameIsSinglePlayer())
                    conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            // Send the HandPacket if it's a player
            if (CurrentTurnPlayerId != INVALID)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.DRAW_PHASE, HandPacket), conSrv.GetClientByID(CurrentTurnPlayerId));
            }

            // Start the next phase (Quest)
            PhaseTimer.Enabled = true;
        }

        private void SendUpdatedDeckStats(int PlayerId)
        {
            string PlayerUsername = null;

            int HandQty = 0;
            int DeckQty = 0;
            int DiscardedQty = 0;

            if (PlayerId == PlayerOne.GetPlayerId())
            {
                PlayerUsername = PlayerOne.GetUsername();
                HandQty = PlayerOne.GetNumberOfCardsInHand();
                DeckQty = PlayerOne.GetNumberOfPlayableCardsInDeck();
            }
            else if (PlayerId == PlayerTwo.GetPlayerId())
            {
                PlayerUsername = PlayerTwo.GetUsername();
                HandQty = PlayerTwo.GetNumberOfCardsInHand();
                DeckQty = PlayerTwo.GetNumberOfPlayableCardsInDeck();
            }

            // Create the stats packet
            CommandPacket Packet = new CommandPacket(new object[] { PlayerUsername, HandQty, DeckQty, DiscardedQty });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_CARD_STATS, Packet);

            // Send the updated card stats to both players
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
        }

        public void ChangePlayerTurn()
        {
            // Change player turn
            CurrentTurnPlayerId = (CurrentTurnPlayerId == PlayerOne.GetPlayerId() ? PlayerTwo.GetPlayerId() : PlayerOne.GetPlayerId());

            // Start the draw phase
            PhaseTimer.Enabled = true;
        }

        private void ApplyAbilityToQuest(int PlayerId, int QuestId, int AbilityId)
        {
            // Get the PlayerObject
            CombatPlayerObject Player = PlayerOne.GetPlayerId() == PlayerId ? PlayerOne : PlayerTwo;

            // Tell the players to update played abilities for the player who played the ability card
            CommandPacket CommandPacket = new CommandPacket(new object[] { Player.GetPlayedAbilities().ToArray(), Player.GetUsername() });

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.UPDATE_PLAYED_ABILITIES, CommandPacket), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.UPDATE_PLAYED_ABILITIES, CommandPacket), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));

            // Get the level of the ability
            CardData AbilityCardData = Cards.GetCardData(AbilityId);
            int AbilityLevel = AbilityCardData.GetHealthOrLevel();

            // Get the level of the quest
            CardData QuestCardData = Cards.GetCardData(QuestId);
            int QuestLevel = QuestCardData.GetHealthOrLevel();

            bool CompletedQuest = false;

            if (QuestId == QuestOne.GetQuestId())
            {
                int TotalCompletedLevels = Player == PlayerOne ? QuestOne.GetPlayerOneCompletedLevels() : QuestOne.GetPlayerTwoCompletedLevels();
                TotalCompletedLevels += AbilityLevel;

                if (TotalCompletedLevels >= QuestLevel)
                {
                    CompletedQuest = true;

                    // Reset points since we're going to replace with a new quest
                    QuestOne.SetPlayerOneCompletedLevels(0);
                    QuestOne.SetPlayerTwoCompletedLevels(0);
                }
                else
                {
                    if (Player == PlayerOne)
                        QuestOne.SetPlayerOneCompletedLevels(TotalCompletedLevels);
                    else if (Player == PlayerTwo)
                        QuestOne.SetPlayerTwoCompletedLevels(TotalCompletedLevels);
                }
            }
            else if (QuestId == QuestTwo.GetQuestId())
            {
                int TotalCompletedLevels = Player == PlayerOne ? QuestTwo.GetPlayerOneCompletedLevels() : QuestTwo.GetPlayerTwoCompletedLevels();
                TotalCompletedLevels += AbilityLevel;

                if (TotalCompletedLevels >= QuestLevel)
                {
                    CompletedQuest = true;

                    // Reset points since we're going to replace with a new quest
                    QuestTwo.SetPlayerOneCompletedLevels(0);
                    QuestTwo.SetPlayerTwoCompletedLevels(0);
                }
                else
                {
                    if (Player == PlayerOne)
                        QuestTwo.SetPlayerOneCompletedLevels(TotalCompletedLevels);
                    else if (Player == PlayerTwo)
                        QuestTwo.SetPlayerTwoCompletedLevels(TotalCompletedLevels);
                }
            }

            if (CompletedQuest)
            {
                // Get a new QuestId from the winning player's deck
                int NewQuestId = Player.DrawQuest();

                // Player ran out of quests, draw from the other player
                if (NewQuestId == INVALID)
                {
                    NewQuestId = PlayerOne == Player ? PlayerTwo.DrawQuest() : PlayerOne.DrawQuest();
                }

                logSrv.LogDebug("Game", "Drawing NewQuestId: " + NewQuestId);

                // Update the new quest with the server
                if (QuestId == QuestOne.GetQuestId())
                    QuestOne.SetQuestId(NewQuestId);
                else if (QuestId == QuestTwo.GetQuestId())
                    QuestTwo.SetQuestId(NewQuestId);

                // Get completed quests so far
                int CompletedQuests = Player.GetCompletedQuests();

                // Increment completed quest
                CompletedQuests++;

                // Set the new completed quests
                Player.SetCompletedQuests(CompletedQuests);

                // Include the new max power in the QUEST_COMPLETE packet
                int NewMaxPower = AVATAR_START_POWER + PlayerOne.GetCompletedQuests() + PlayerTwo.GetCompletedQuests();

                // Update the server-side avatar powers
                PlayerOne.SetAvatarPower(NewMaxPower);
                PlayerTwo.SetAvatarPower(NewMaxPower);

                // Send the packet to players showing that the quest is completed
                CommandPacket = new CommandPacket(new object[] { QuestId, NewQuestId, Player.GetUsername(), NewMaxPower });
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_COMPLETE, CommandPacket), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                if (!GameIsSinglePlayer())
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_COMPLETE, CommandPacket), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }
            else
            {
                // Update the clients with the updated completed levels
                int NewCompletedLevels = 0;

                if (PlayerId == PlayerOne.GetPlayerId())
                {
                    NewCompletedLevels = QuestId == QuestOne.GetQuestId() ? QuestOne.GetPlayerOneCompletedLevels() : QuestTwo.GetPlayerOneCompletedLevels();
                }
                else if (PlayerId == PlayerTwo.GetPlayerId())
                {
                    NewCompletedLevels = QuestId == QuestOne.GetQuestId() ? QuestOne.GetPlayerTwoCompletedLevels() : QuestTwo.GetPlayerTwoCompletedLevels();
                }

                CommandPacket = new CommandPacket(new object[] { QuestId, NewCompletedLevels, Player.GetUsername() });
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_UPDATE_COMPLETED_LEVELS, CommandPacket), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                if (!GameIsSinglePlayer())
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_UPDATE_COMPLETED_LEVELS, CommandPacket), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            // Check if player won by completing quests
            if (Player.GetCompletedQuests() == 4)
            {
                string WinnerUsername = Player.GetUsername();
                EndTheGame(WinnerUsername);
            }
            else
            {
                // Next phase
                PhaseTimer.Enabled = true;
            }
        }

        public void AttemptQuestForPlayer(int PlayerId, int CardId)
        {
            CardData CardData = Cards.GetCardData(CardId);

            switch (CardData.GetType())
            {
                case "Ability":
                    CombatPlayerObject Player = PlayerOne.GetPlayerId() == PlayerId ? PlayerOne : PlayerTwo;

                    // Check that the player has the card - if so, remove it
                    bool PlayerHadAbility = Player.UseAbility(CardId);

                    if (!PlayerHadAbility)
                    {
                        logSrv.LogWarning("Game", "Played attempted to play ability that they do not have in hand.");
                        return;
                    }

                    ApplyAbilityToQuest(PlayerId, PendingAbilityQuestId, CardId);
                    PendingAbilityQuestId = INVALID;
                    break;
                case "Quest":
                    // If there are enemy units here, start a raid
                    QuestObject Quest = GetQuestObject(CardId);
                    PendingAbilityQuestId = CardId;

                    if (PlayerId == PlayerOne.GetPlayerId() && Quest.GetNumberOfPlayerTwoCards() > 0 || PlayerId == PlayerTwo.GetPlayerId() && Quest.GetNumberOfPlayerOneCards() > 0)
                    {
                        StartRaid(PlayerId, CardId);
                    }
                    else
                    {
                        //Request the ability card they want to use
                        if (PlayerId == INVALID)
                            NpcChooseAbilityToPlay();
                        else
                            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_ABILITY, null), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                    }
                    break;
            }
        }

        public void EndTheGame(string WinnerUsername)
        {
            GameFinished = true;

            //@TODO: Implement rewards

            if (!string.IsNullOrEmpty(WinnerUsername))
            {
                CommandPacket CommandPacket = new CommandPacket(new object[] { WinnerUsername });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.GAME_END, CommandPacket);

                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

                if (!GameIsSinglePlayer())
                    conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }
        }

        private void StartQuestPhase()
        {
            CurrentPhase = PHASES.QUEST_PHASE;

            // Update other player of QuestPhase if it's not an NPC
            if (CurrentTurnPlayerId != PlayerOne.GetPlayerId())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_PHASE, null), conSrv.GetClientByID(PlayerOne.GetPlayerId()));
            }
            else if (CurrentTurnPlayerId != PlayerTwo.GetPlayerId() && !GameIsSinglePlayer())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_PHASE, null), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            // Can choose quest if has abilities with levels on the playmat
            bool CanChooseQuest = CurrentTurnPlayerId == PlayerOne.GetPlayerId() && PlayerOne.HasUnexertedAbilities() || CurrentTurnPlayerId == PlayerTwo.GetPlayerId() && PlayerTwo.HasUnexertedAbilities();

            if (CanChooseQuest)
            {
                if (CurrentTurnPlayerId != INVALID)
                {
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_PHASE, new CommandPacket(new object[] { CanChooseQuest })), conSrv.GetClientByID(CurrentTurnPlayerId));
                }
                else
                {
                    logSrv.LogDebug("Game", "NPC choosing quest");

                    // Get the quest we wanna play (the one with the lowest level)
                    CardData CardData = Cards.GetCardData(QuestOne.GetQuestId());
                    int QuestOneLevel = CardData.GetHealthOrLevel();

                    logSrv.LogDebug("Game", "QuestOneLevel: " + QuestOneLevel);

                    CardData = Cards.GetCardData(QuestTwo.GetQuestId());
                    int QuestTwoLevel = CardData.GetHealthOrLevel();

                    logSrv.LogDebug("Game", "QuestTwoLevel: " + QuestTwoLevel);

                    int SelectedQuestId = QuestOneLevel < QuestTwoLevel ? QuestOne.GetQuestId() : QuestTwo.GetQuestId();
                    logSrv.LogDebug("Game", "PlayedAbilities: " + PlayerTwo.GetPlayedAbilities().Count);

                    AttemptQuestForPlayer(INVALID, SelectedQuestId);
                }
            }
            else
            {
                // Check if not an NPC
                if (CurrentTurnPlayerId != INVALID)
                {
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_PHASE, new CommandPacket(new object[] { CanChooseQuest })), conSrv.GetClientByID(CurrentTurnPlayerId));
                }

                // Start next phase timer since player can't choose quest
                PhaseTimer.Enabled = true;
            }
        }

        private void NpcChooseAbilityToPlay()
        {
            int AbilityId = PlayerTwo.GetPlayedAbilities()[0].GetCardId();
            AttemptQuestForPlayer(INVALID, AbilityId);
        }

        public void StartReadyPhase()
        {
            CurrentPhase = PHASES.READY_PHASE;

            // Update other player of ReadyPhase if it's not an NPC
            if (CurrentTurnPlayerId != PlayerOne.GetPlayerId())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.READY_PHASE, null), conSrv.GetClientByID(PlayerOne.GetPlayerId()));
            }
            else if (CurrentTurnPlayerId != PlayerTwo.GetPlayerId() && !GameIsSinglePlayer())
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.READY_PHASE, null), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));
            }

            if (CurrentTurnPlayerId == PlayerOne.GetPlayerId())
            {
                // Ready avatar
                PlayerOne.SetAvatarExerted(false);

                // Ready units
                foreach (PlaymatCard Card in QuestOne.GetPlayerOneCards().Values)
                    Card.SetExerted(false);
                foreach (PlaymatCard Card in QuestTwo.GetPlayerOneCards().Values)
                    Card.SetExerted(false);
            }
            else if (CurrentTurnPlayerId == PlayerTwo.GetPlayerId())
            {
                // Ready avatar
                PlayerTwo.SetAvatarExerted(false);

                // Ready units
                foreach (PlaymatCard Card in QuestOne.GetPlayerTwoCards().Values)
                    Card.SetExerted(false);
                foreach (PlaymatCard Card in QuestTwo.GetPlayerTwoCards().Values)
                    Card.SetExerted(false);
            }

            // check if not a player
            if (CurrentTurnPlayerId != INVALID)
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.READY_PHASE, new CommandPacket(null)), conSrv.GetClientByID(CurrentTurnPlayerId));
            }

            // Start next phase timer
            PhaseTimer.Enabled = true;
        }

        private int[] GetValidCardIdsToPlay()
        {
            List<int> ValidCardIds = new List<int>();

            int[] Hand = CurrentTurnPlayerId == PlayerOne.GetPlayerId() ? PlayerOne.GetHandCardIds() : PlayerTwo.GetHandCardIds();
            int AvatarPower = CurrentTurnPlayerId == PlayerOne.GetPlayerId() ? PlayerOne.GetAvatarPower() : PlayerTwo.GetAvatarPower();

            CardData CardData;
            foreach (int CardId in Hand)
            {
                if (CardId == INVALID)
                    continue;

                CardData = Cards.GetCardData(CardId);

                if ((CardData.GetType().Equals("Unit") || CardData.GetType().Equals("Ability") || CardData.GetType().Equals("Item")) && AvatarPower >= CardData.GetCost())
                {
                    ValidCardIds.Add(CardId);
                }
            }

            return ValidCardIds.ToArray();
        }

        public void StartMainPhase()
        {
            CurrentPhase = PHASES.MAIN_PHASE;

            CombatPlayerObject Player = GetCombatPlayerObject(CurrentTurnPlayerId);

            CommandPacket CommandPacket = new CommandPacket(new object[] { Player.GetUsername() });

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MAIN_PHASE, CommandPacket), conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MAIN_PHASE, CommandPacket), conSrv.GetClientByID(PlayerTwo.GetPlayerId()));

            // Tell NPC to make a move
            if (CurrentTurnPlayerId == INVALID)
            {
                NpcTimer.Start();
            }
        }

        private int GetIdOfFirstTurn()
        {
            // Return PlayerOne if PlayerTwo is an NPC
            if (PlayerTwo.GetPlayerId() == INVALID)
                return PlayerOne.GetPlayerId();

            Random Random = new Random();
            int RandomNumber = Random.Next(0, 1);

            return RandomNumber == 0 ? PlayerOne.GetPlayerId() : PlayerTwo.GetPlayerId();
        }

        private void PhaseTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PhaseTimer.Enabled = false;

            // We are just returning from a raid
            if (RaidObject != null)
            {
                // Remove the RaidObject
                RaidObject = null;

                // Resume the phase where the game left off
                if (CurrentPhase == PHASES.QUEST_PHASE)
                    StartQuestPhase();
                else if (CurrentPhase == PHASES.MAIN_PHASE)
                    StartMainPhase();
            }
            else if (!GameFinished)
            {
                if (CurrentPhase == PHASES.DRAW_PHASE)
                    StartQuestPhase();
                else if (CurrentPhase == PHASES.QUEST_PHASE)
                    StartReadyPhase();
                else if (CurrentPhase == PHASES.READY_PHASE)
                    StartMainPhase();
                else if (CurrentPhase == PHASES.MAIN_PHASE)
                    StartDrawPhase();
            }
        }

        public int GetCurrentTurnId()
        {
            return CurrentTurnPlayerId;
        }

        public bool GameIsSinglePlayer()
        {
            return PlayerTwo.GetPlayerId() == INVALID;
        }

        public void PlaceCard(int PlayerId, int CardId, int QuestId)
        {
            if (CurrentTurnPlayerId != PlayerId && (RaidObject != null && RaidObject.GetCurrentTurnPlayerId() != PlayerId))
            {
                logSrv.LogWarning("Game", "Player (" + PlayerId + ") tried to place card when it wasn't their turn.");
                return;
            }

            CombatPlayerObject Player = GetCombatPlayerObject(PlayerId);
            CardData CardData = Cards.GetCardData(CardId);
            int IndexOfCard = Player.GetIndexOfCardInHand(CardId);

            // Check if the player has the card
            if (IndexOfCard == INVALID)
            {
                logSrv.LogWarning("Game", "Player (" + PlayerId + ") tried placing card (" + CardId + ") without it being in hand");
                return;
            }

            // Check if the player has enough action
            if (CardData.GetCost() > Player.GetAvatarPower())
            {
                logSrv.LogWarning("Game", "Player (" + PlayerId + ") tried placing card with cost (" + CardData.GetCost() + ") with (" + Player.GetAvatarPower() + ") power.");
                return;
            }

            // Remove card from hand
            Player.SetHandCardId(IndexOfCard, INVALID);

            // Update avatar's power
            int NewAvatarPower = Player.GetAvatarPower() - CardData.GetCost();
            Player.SetAvatarPower(NewAvatarPower);

            // Tell client's to update the avatar power UI bars
            CommandPacket CommandPacket = new CommandPacket(new object[] { Player.GetUsername(), Player.GetAvatarPower() });
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_POWER, CommandPacket);

            // Send to first player
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            // Send packet to PlayerTwo if not an AI
            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));

            Thread.Sleep(100);

            // Send updated card statistics
            if (PlayerId != INVALID)
                SendUpdatedDeckStats(PlayerId);

            // Prepare the packet
            CommandPacket Packet = null;
            PlaymatCard Card = null;

            // Add the card to placed
            switch (CardData.GetType())
            {
                case "Ability":
                   Card = Player.AddAbility(CardId);
                    Packet = new CommandPacket(new object[] { Player.GetUsername(), Card });
                    break;
                case "Item":
                    Card = Player.AddItem(CardId);
                    Packet = new CommandPacket(new object[] { Player.GetUsername(), Card });
                    break;
                case "Unit":
                    int Attack = CardData.GetAttack();
                    int Damage = CardData.GetBonus();
                    int Defense = CardData.GetDefense();
                    int Health = CardData.GetHealthOrLevel();
                    int Key = -1;

                    if (QuestId == QuestOne.GetQuestId())
                    {
                        Key = QuestOne.GenerateNewValidCardKey();

                        if (Player == PlayerOne)
                            Card = QuestOne.AddPlayerOnePlacedCard(Key, CardId, Attack, Damage, Defense, Health);
                        else if (Player == PlayerTwo)
                            Card = QuestOne.AddPlayerTwoPlacedCard(Key, CardId, Attack, Damage, Defense, Health);
                    }
                    else if (QuestId == QuestTwo.GetQuestId())
                    {
                        Key = QuestTwo.GenerateNewValidCardKey();

                        if (Player == PlayerOne)
                            Card = QuestTwo.AddPlayerOnePlacedCard(Key, CardId, Attack, Damage, Defense, Health);
                        else if (Player == PlayerTwo)
                            Card = QuestTwo.AddPlayerTwoPlacedCard(Key, CardId, Attack, Damage, Defense, Health);
                    }
                    Packet = new CommandPacket(new object[] { Player.GetUsername(), QuestId, Card, Key });
                    break;
                case "Tactic":
                    RaidObject.PlayTactic(PlayerId, CardId);
                    Packet = new CommandPacket(new object[] { Player.GetUsername(), CardId });
                    break;
                default:
                    logSrv.LogWarning("Game", "Player played invalid card type: " + CardData.GetType());
                    break;
            }

            // Tell real players to update the playmat UI
            PacketSender = new PacketSender(SERVER_ID, Commands.PLACE_CARD, Packet);

            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));

            logSrv.LogDebug("Game", "Player placed card with id: " + CardId);
        }

        public string GetChatroomTitle()
        {
            return ChatRoomTitle;
        }

        public void StartRaid(int AttackerId, int QuestId)
        {
            int DefenderId = AttackerId == PlayerOne.GetPlayerId() ? PlayerTwo.GetPlayerId() : PlayerOne.GetPlayerId();

            CombatPlayerObject AttackerObject = GetCombatPlayerObject(AttackerId);
            CombatPlayerObject DefenderObject = GetCombatPlayerObject(DefenderId);

            QuestObject QuestObject = GetQuestObject(QuestId);

            // Create the raid object
            RaidObject = new RaidObject(AttackerObject, DefenderObject, QuestObject);
            RaidObject.BeginRaid();
        }

        private QuestObject GetQuestObject(int QuestId)
        {
            if (QuestOne.GetQuestId() == QuestId)
                return QuestOne;
            if (QuestTwo.GetQuestId() == QuestId)
                return QuestTwo;
            return null;
        }

        public void EndRaid()
        {
            logSrv.LogDebug("Game", "Ending raid");

            // Let clients know raid ended
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.RAID_END, null);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));

            if (!GameIsSinglePlayer())
                conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerTwo.GetPlayerId()));

            // Check if we need to apply the ability to the quest
            if (PendingAbilityQuestId != INVALID)
            {
                // See if the attacker won
                int AttackerId = RaidObject.GetAttacker().GetPlayerId();

                if (AttackerId == INVALID)
                    NpcChooseAbilityToPlay();
                else
                    conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.QUEST_ABILITY, null), conSrv.GetClientByID(PlayerOne.GetPlayerId()));
            }

            // Return to the phase that was left off on
            PhaseTimer.Start();
        }

        public void ExertAttackCard(int PlayerId, int CardKey, string CardType)
        {
            // Tell the raid object what card was exerted and by who
            RaidObject.ExertCard(PlayerId, CardKey, CardType);
        }

        public void ExertDefenseCard(int PlayerId, int CardKey, string CardType)
        {
            // Tell the raid object what card was exerted and by who
            RaidObject.ExertCard(PlayerId, CardKey, CardType);
        }

        public void ExertDamageCard(int PlayerId, int CardKey, string CardType)
        {
            // Tell the raid object what card was exerted and by who
            RaidObject.ExertCard(PlayerId, CardKey, CardType);
        }

        public void ExertAvatar(int PlayerId)
        {
            RaidObject.ExertAvatar(PlayerId);
        }

        public void PassTurn(int PlayerId)
        {
            // Just tell the RaidObject who passed, it will handle the rest
            RaidObject.PassTurn(PlayerId);
        }

        public void DealDamagetoCard(int PlayerId, int CardKey)
        {
            RaidObject.DealDamageToCard(PlayerId, CardKey);
        }

        public void DealDamageToAvatar(int PlayerId)
        {
            RaidObject.DealDamageToAvatar(PlayerId);
        }

        public void PlayTactic(int PlayerId, int CardId)
        {
            RaidObject.PlayTactic(PlayerId, CardId);
        }

        public int GetPlayerOneId()
        {
            return PlayerOne.GetPlayerId();
        }

        public int GetPlayerTwoId()
        {
            return PlayerTwo.GetPlayerId();
        }

        public CombatPlayerObject GetCombatPlayerObject(int PlayerId)
        {
            if (PlayerOne.GetPlayerId() == PlayerId)
                return PlayerOne;
            if (PlayerTwo.GetPlayerId() == PlayerId)
                return PlayerTwo;
            return null;
        }

        public void RedrawHand(int PlayerId)
        {
            if (PlayerId == PlayerOne.GetPlayerId())
            {
                // Put cards back in deck
                PlayerOne.PutHandCardsBackInDeck();

                // Redraw hand
                PlayerOne.DrawCardIds(MAX_HAND_CARDS);

                // Remove 1 health from avatar
                int NewAvatarHealth = PlayerOne.GetAvatarHealth() - 1;
                PlayerOne.SetAvatarHealth(NewAvatarHealth);

                // Update the players with health change
                CommandPacket CommandPacket = new CommandPacket(new object[] { PlayerOne.GetUsername(), PlayerOne.GetAvatarHealth() });
                PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_HEALTH, CommandPacket);

                if (GameIsSinglePlayer())
                {
                    // Send to the only player playing
                    conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerOne.GetPlayerId()));
                }
                else
                {
                    // Send the both players
                    ClientData[] Clients = new ClientData[] { conSrv.GetClientByID(PlayerOne.GetPlayerId()), conSrv.GetClientByID(PlayerTwo.GetPlayerId()) };
                    conSrv.SendPacketToClients(PacketSender, Clients);
                }
            }
            else if (PlayerId == PlayerTwo.GetPlayerId())
            {
                // Put hand back in deck
                PlayerTwo.PutHandCardsBackInDeck();

                // Redraw hand
                PlayerTwo.DrawCardIds(MAX_HAND_CARDS);

                // Remove 1 health from avatar
                int NewHealth = PlayerTwo.GetAvatarHealth() - 1;
                PlayerTwo.SetAvatarHealth(NewHealth);

                // Update the players with health change
                ClientData[] Clients = new ClientData[] { conSrv.GetClientByID(PlayerOne.GetPlayerId()), conSrv.GetClientByID(PlayerTwo.GetPlayerId()) };
                CommandPacket CommandPacket = new CommandPacket(new object[] { PlayerTwo.GetUsername(), PlayerTwo.GetAvatarHealth() });
                conSrv.SendPacketToClients(new PacketSender(SERVER_ID, Commands.UPDATE_AVATAR_HEALTH, CommandPacket), Clients);
            }
        }
    }
}
