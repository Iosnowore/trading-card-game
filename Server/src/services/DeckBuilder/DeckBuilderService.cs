﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.DeckBuilder;

namespace Server.src.services.DeckBuilder
{
    public class DeckBuilderService : Server
    {
        private const string DECKS_TABLE = "decks";

        // Decks: SortedDictionary<PlayerId, AllDeckObjects>
        private readonly SortedDictionary<int, DeckPacket[]> Decks;

        public DeckBuilderService()
        {
            Decks = new SortedDictionary<int, DeckPacket[]>();

            LoadPlayerDecks();

            logSrv.Log("DeckBuilderService", "Loaded");
        }

        public DeckPacket GetDeck(int PlayerId, string DeckTitle)
        {
            DeckPacket[] PlayersDecks = Decks[PlayerId];

            foreach (DeckPacket DeckPacket in PlayersDecks)
                if (DeckPacket.GetDeckName().Equals(DeckTitle))
                    return DeckPacket;
            return null;
        }

        private void LoadPlayerDecks()
        {
            int[] IdColumns = dbSrv.GetIntTableResults(DECKS_TABLE, "id");

            foreach (int Id in IdColumns)
            {
                LoadPlayerDeck(Id);
            }
            logSrv.LogDebug("DeckBuilderService", "Loaded " + Decks.Count + " player's decks.");
        }

        public void LoadPlayerDeck(int Id)
        {
            string DeckString = dbSrv.GetString(DECKS_TABLE, "decks", "id", Id.ToString());

            // DeckName|CardId:CardQuantity|....   ;    ....CardName|CardId:CardQuantity

            DeckPacket[] DeckPackets;

            // Check if player has any decks
            if (!string.IsNullOrEmpty(DeckString))
            {
                string[] DeckArr = DeckString.Split(CollectionService.TERTIARY_DELIMITER);
                DeckPackets = new DeckPacket[DeckArr.Length];

                string[] DeckSubArr;
                for (int i = 0; i < DeckArr.Length; i++)
                {
                    // DeckName..... |  .....CardId:CardQuanity..... | .....CardId:CardQuanity....
                    DeckSubArr = DeckArr[i].Split(CollectionService.PRIMARY_DELIMITER);

                    string DeckName = DeckSubArr[0];

                    SortedDictionary<int, int> Cards = new SortedDictionary<int, int>();

                    string[] CardDetails;
                    for (int j = 1; j < DeckSubArr.Length; j++)
                    {
                        // ......CardId     :        CardQuanity.... = { CardId, CardQuantity }
                        CardDetails = DeckSubArr[j].Split(CollectionService.SECONDARY_DELIMITER);
                        int CardId = int.Parse(CardDetails[0]);
                        int CardQuantity = int.Parse(CardDetails[1]);
                        Cards.Add(CardId, CardQuantity);

                    }
                    DeckPacket DeckPacket = new DeckPacket(DeckName, Cards);
                    DeckPackets[i] = DeckPacket;
                }
            }
            else
                DeckPackets = new DeckPacket[0];
            Decks.Add(Id, DeckPackets);
        }

        private int GetIndexOfPlayerDeck(DeckPacket[] Decks, string DeckName)
        {
            for (int i = 0; i < Decks.Length; i++)
                if (Decks[i].GetDeckName().Equals(DeckName))
                    return i;
            return -1;
        }

        public void SaveDeck(int PlayerId, string DeckName, SortedDictionary<int, int> CardIds)
        {
            // Form DeckPacket object
            DeckPacket NewDeck = new DeckPacket(DeckName, CardIds);
            DeckPacket[] PlayersDecks = Decks[PlayerId];

            int IndexOfPlayerDeck = GetIndexOfPlayerDeck(PlayersDecks, DeckName);

            // Check if name already exists
            if (IndexOfPlayerDeck == -1)
            {
                // Create a new DeckArray (must expand by 1 for new deck to fit)
                DeckPacket[] NewPlayerDecks = new DeckPacket[PlayersDecks.Length + 1];
                for (int i = 0; i < PlayersDecks.Length; i++)
                    NewPlayerDecks[i] = PlayersDecks[i];

                // The last element will be the new Deck object
                NewPlayerDecks[NewPlayerDecks.Length - 1] = NewDeck;

                // Update the entire Decks for the player since we had to create a new array (due to increase size)
                Decks[PlayerId] = NewPlayerDecks;
            }
            else
            {
                // Just update the existing deck
                Decks[PlayerId][IndexOfPlayerDeck] = NewDeck;
            }

            // Now that the server is in sync, update the database with what the server has.
            SyncPlayerDecksToDatabase(PlayerId);
        }

        private void SyncPlayerDecksToDatabase(int PlayerId)
        {
            // Construct the string value.
            string ConstructedString = string.Empty;

            // Go through each deck and add it
            foreach (DeckPacket Deck in Decks[PlayerId])
            {
                int[] DeckCardIds = new int[Deck.GetCardIds().Count];
                Deck.GetCardIds().Keys.CopyTo(DeckCardIds, 0);

                if (!string.IsNullOrEmpty(ConstructedString))
                    ConstructedString += CollectionService.TERTIARY_DELIMITER;

                ConstructedString += Deck.GetDeckName();

                foreach (int CardId in DeckCardIds)
                    ConstructedString += CollectionService.PRIMARY_DELIMITER.ToString() + CardId + CollectionService.SECONDARY_DELIMITER + Deck.GetCardIds()[CardId];
            }

            dbSrv.ChangeValue(DECKS_TABLE, "decks", ConstructedString, PlayerId);
        }

        public void SendDecks(int PlayerId)
        {
            DeckPacket[] DeckPackets = Decks[PlayerId];
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_DECK_PACKETS, new DecksPacket(DeckPackets)), conSrv.GetClientByID(PlayerId));
        }

        public void DeleteDeck(int PlayerId, string DeckName)
        {
            DeckPacket[] DecksArray = Decks[PlayerId];

            List<DeckPacket> NewDecks = new List<DeckPacket>();

            // Go through each deck, add to a new list if it's not the one we don't want
            foreach (DeckPacket Deck in DecksArray)
                if (!Deck.GetDeckName().Equals(DeckName))
                    NewDecks.Add(Deck);

            // Update the server with the new player's decks we compiled
            Decks[PlayerId] = NewDecks.ToArray();

            // Now that the server is in sync, update the database with what the server has.
            SyncPlayerDecksToDatabase(PlayerId);
        }
    }
}
