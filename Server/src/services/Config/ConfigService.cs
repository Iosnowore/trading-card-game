﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.IO;

namespace Server.src.services.Config
{
    public class ConfigService : Server
    {
        public readonly string NETWORK_CFG = "network.cfg";
        public readonly string FEATURES_CFG = "features.cfg";

        public ConfigService()
        {
            CheckCfgs();
            //logSrv.Log("ConfigService", "Loaded");
        }

        public string GetConfigValue(string file, string cfgValue)
        {
            string value;

            using (StreamReader reader = new StreamReader(@".\cfg\" + file))
            {
                while ((value = reader.ReadLine()) != null)
                {
                    if (value.StartsWith(cfgValue))
                        return value.Replace(cfgValue + "=", string.Empty);
                }
            }
            return value;
        }

        public string GetContentsOfCfg(string file)
        {
            try
            {
                return new StreamReader(@".\cfg\" + file).ReadToEnd();
            }
            catch
            {
                return string.Empty;
            }
        }

        private void CheckCfgs()
        {
            string cfgDir = @".\cfg\";

            if (!Directory.Exists(cfgDir))
                Directory.CreateDirectory(cfgDir);

            if (!File.Exists(cfgDir + NETWORK_CFG))
            {
                using StreamWriter writer = new StreamWriter(cfgDir + NETWORK_CFG);
                writer.WriteLine("# Database Configuration");
                writer.WriteLine("DB_TYPE=");
                writer.WriteLine();
                writer.WriteLine("# SQLite Connection Info");
                writer.WriteLine("DB_FILE=");
                writer.WriteLine();
                writer.WriteLine("# MySQL Connection Info");
                writer.WriteLine("DB_ADDRESS=");
                writer.WriteLine("DB_USERNAME=");
                writer.WriteLine("DB_PASSWORD=");
                writer.WriteLine("DB_NAME");
                writer.WriteLine();
                writer.WriteLine("# Game Configuration");
                writer.WriteLine("SERVER_PORT=");
                writer.WriteLine("SERVER_PROTOCOL=");
            }

            if (!File.Exists(cfgDir + FEATURES_CFG))
            {
                using StreamWriter writer = new StreamWriter(cfgDir + FEATURES_CFG);
                writer.WriteLine("# Forum Data Syncing - mybb or vbulletin");
                writer.WriteLine("FORUM_TYPE=");
                writer.WriteLine();
                writer.WriteLine("# Login Authentication");
                writer.WriteLine("AUTH_URL=");
                writer.WriteLine("RESPONSE_BANNED=");
                writer.WriteLine("RESPONSE_STAFF=");
                writer.WriteLine("RESPONSE_SUCCESS=");
                writer.WriteLine("STAFF_ONLY=");
                writer.WriteLine();
                writer.WriteLine("# Logging");
                writer.WriteLine("EXPORT_LOGS=0");
                writer.WriteLine("CS_LOGGING=0");
                writer.WriteLine("LOG_DIR=");
                writer.WriteLine();
                writer.WriteLine("# Table Directory");
                writer.WriteLine("TABLE_DIR=");
                writer.WriteLine();
                writer.WriteLine("# XML Status");
                writer.WriteLine("XML_DIR=");
            }
        }
    }
}
