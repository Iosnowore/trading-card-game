﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using TCGData;
using TCGData.Services;

namespace Server.src.services.Collection
{
    public class PackService : Server
    {
        private const string RARITY_COMMON = "C";
        private const string RARITY_UNCOMMON = "U";
        private const string RARITY_RARE = "R";
        private const string RARITY_LOOT = "SWG";

        private const int STARTER_PACK_RARES = 3;
        private const int BOOSTER_PACK_AMOUNT = 15;

        // Common: 750/1000 chance
        private const int RARITY_COMMON_MAX = 900;

        // Uncommon: 500/1000 chance
        private const int RARITY_UNCOMMON_MAX = 600;

        // Rare: 250/1000 chance
        private const int RARITY_RARE_MAX = 300;

        private static readonly string[] Rarities = { RARITY_COMMON, RARITY_UNCOMMON, RARITY_RARE };

        public PackService()
        {
            logSrv.Log("PackService", "Loaded");
        }

        private bool RollRarity(string Rarity)
        {
            Random Random = new Random();

            int MaxValue;

            if (Rarity.Equals(RARITY_COMMON))
                MaxValue = RARITY_COMMON_MAX;
            else if (Rarity.Equals(RARITY_UNCOMMON))
                MaxValue = RARITY_UNCOMMON_MAX;
            else
                MaxValue = RARITY_RARE_MAX;

            // Datatable is 1 out of 1000
            // Ex: If C is 700 -> if (Rolled <= 700)
            // Ex: If R is 19 -> if (Rolled <= 19)
            int Rolled = Random.Next(1, 1000);

            return Rolled <= MaxValue;
        }

        private string GetRandomRarity()
        {
            foreach (string Rarity in Rarities)
            {
                if (RollRarity(Rarity))
                    return Rarity;
            }
            // If user did not roll a common, uncommon, or rare, then give them a loot card.
            return RARITY_LOOT;
        }

        public SortedDictionary<int, int> GenerateCardsFromPack(string PackId)
        {
            SortedDictionary<int, int> GeneratedCards = new SortedDictionary<int, int>();

            Random Random = new Random();

            // Check if it's a booster pack
            if (PackId.EndsWith("10"))
            {
                string SetString = DatatableReader.GetPackValue(PackId, 0).Substring(0, 1);

                int Set = int.Parse(SetString);

                int[] SetCardIds = Cards.GetCardIdsFromSet(Set);
                int[] LootCardIds = Cards.GetLootCardIdsFromSpecificSet(Set, false);

                for (int i = 0; i < BOOSTER_PACK_AMOUNT; i++)
                {
                    // Get random rarity to give
                    string Rarity = GetRandomRarity();

                    int[] CardsWithRarity;

                    if (Rarity.Equals("SWG"))
                    {
                        // We do not need to check for rarity since all loot cards are the same
                        CardsWithRarity = LootCardIds;
                    }
                    else
                    {
                        // Get cards with that rarity
                        CardsWithRarity = Cards.GetCardIdsGivenRarity(SetCardIds, Rarity[0]);
                    }

                    // Get a random index of CardsWithRarity
                    int CardIdToGiveIndex = Random.Next(0, CardsWithRarity.Length - 1);

                    // With the index we found, get the CardId
                    int GeneratedCardId = CardsWithRarity[CardIdToGiveIndex];

                    // Add the internal LOOT_CARD int so the server knows this is indeed a loot card
                    if (Rarity.Equals("SWG"))
                        GeneratedCardId += CollectionService.LOOTCARD_INTERVAL;

                    // We already added this card, just increase the quantity
                    if (GeneratedCards.ContainsKey(GeneratedCardId))
                        GeneratedCards[GeneratedCardId] += 1;
                    else
                        GeneratedCards.Add(GeneratedCardId, 1);
                }
            }
            else
            {
                // Must be a starter deck then

                // Get the card string
                string CardString = DatatableReader.GetValue("starterdecks", "id", PackId.ToString(), "cards");
                string[] CardStringArr = CardString.Split('|');

                // Add each card in the string
                string[] CardInfo;
                foreach (string Card in CardStringArr)
                {
                    CardInfo = Card.Split(':');
                    GeneratedCards.Add(int.Parse(CardInfo[0]), int.Parse(CardInfo[1]));
                }

                // Now we add 3 random rare cards (totaling of 58 cards)

                // Get the series of the starter deck
                int Series = int.Parse(PackId[0].ToString());

                // Get the cards in that series
                int[] CardIds = Cards.GetCardIdsFromSet(Series);

                // Get the archetype of the starter deck
                string Archetype = "";

                switch (PackId[2])
                {
                    case '1':
                        Archetype = "imperial";
                        break;
                    case '2':
                        Archetype = "jedi";
                        break;
                    case '3':
                        Archetype = "rebel";
                        break;
                    case '4':
                        Archetype = "sith";
                        break;
                }

                // Filter the archetype
                CardIds = Cards.GetCardIdsGivenArchetype(CardIds, Archetype);

                // Filter the rarity
                CardIds = Cards.GetCardIdsGivenRarity(CardIds, 'R');

                for (int i = 0; i < STARTER_PACK_RARES; i++)
                {
                    // Get a random index of CardsWithRarity
                    int CardIdToGiveIndex = Random.Next(0, CardIds.Length - 1);

                    // With the index we found, get the CardId
                    int GeneratedCardId = CardIds[CardIdToGiveIndex];

                    // Check if we need to increment the quantity or just simply add it
                    if (GeneratedCards.ContainsKey(GeneratedCardId))
                        GeneratedCards[GeneratedCardId] += 1;
                    else
                        GeneratedCards.Add(GeneratedCardId, 1);
                }
            }
            return GeneratedCards;
        }
    }
}
