﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData;

namespace Server.src.services.Collection
{
    public class Collection : Server
    {
        private readonly SortedDictionary<int, int> Cards = new SortedDictionary<int, int>();
        private readonly SortedDictionary<int, int> Wants = new SortedDictionary<int, int>();
        private readonly SortedDictionary<int, int> Redeemables = new SortedDictionary<int, int>();
        private readonly SortedDictionary<int, int> Packs = new SortedDictionary<int, int>();
        private readonly SortedDictionary<int, int> PromoPacks = new SortedDictionary<int, int>();
        private readonly SortedDictionary<int, int> Redeemed = new SortedDictionary<int, int>();
        private readonly List<CardData> Avatars = new List<CardData>();

        private readonly int Id;

        public Collection(int Id)
        {
            this.Id = Id;

            LoadCollectionValue(CollectionService.CARDS);
            LoadCollectionValue(CollectionService.WANTS);
            LoadCollectionValue(CollectionService.REDEEMABLES);
            LoadCollectionValue(CollectionService.PACKS);
            LoadCollectionValue(CollectionService.AVATARS);
            LoadCollectionValue(CollectionService.PROMOPACKS);
            LoadCollectionValue(CollectionService.REDEEMED);
        }

        private void LoadCollectionValue(string valueNeeded)
        {
            string CardsString = dbSrv.GetString(CollectionService.COLLECTIONS, valueNeeded, "id", Id.ToString());

            // If the CardsString is empty, don't load anything.
            if (string.IsNullOrEmpty(CardsString))
                return;

            string[] CardsSplit = CardsString.Split(CollectionService.PRIMARY_DELIMITER);

            string[] cardDetails;

            foreach (string Card in CardsSplit)
            {
                cardDetails = Card.Split(CollectionService.SECONDARY_DELIMITER);

                if (cardDetails.Length < 2)
                {
                    logSrv.LogWarning("Collection", "Player (" + Id + ")  has invalid card (" + Card + "). Not adding this to the collection.");
                    continue;
                }

                switch (valueNeeded)
                {
                    case CollectionService.CARDS:
                        Cards.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                    case CollectionService.REDEEMABLES:
                        Redeemables.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                    case CollectionService.PACKS:
                        Packs.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                    case CollectionService.AVATARS:
                        Avatars.Add(new CardData(-1, cardDetails[0], null, null, cardDetails[1], cardDetails[2], null, null, null, null, cardDetails[3], 0, int.Parse(cardDetails[4]), int.Parse(cardDetails[5]), int.Parse(cardDetails[6]), int.Parse(cardDetails[7])));
                        break;
                    case CollectionService.PROMOPACKS:
                        PromoPacks.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                    case CollectionService.REDEEMED:
                        Redeemed.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                    case CollectionService.WANTS:
                        Wants.Add(int.Parse(cardDetails[0]), int.Parse(cardDetails[1]));
                        break;
                }
            }
        }

        public void ReloadAvatarsInCollection()
        {
            Avatars.Clear();
            LoadCollectionValue(CollectionService.AVATARS);
        }

        public void ReloadCardsInCollection()
        {
            Cards.Clear();
            LoadCollectionValue(CollectionService.CARDS);
        }

        public void ReloadLootCardsInCollection()
        {
            Redeemables.Clear();
            LoadCollectionValue(CollectionService.REDEEMABLES);
        }

        public void ReloadRedeemedCardsInCollection()
        {
            Redeemed.Clear();
            LoadCollectionValue(CollectionService.REDEEMED);
        }

        public void ReloadPacksInCollection()
        {
            Packs.Clear();
            LoadCollectionValue(CollectionService.PACKS);
        }

        public void ReloadWantsInCollection()
        {
            Wants.Clear();
            LoadCollectionValue(CollectionService.WANTS);
        }

        public int GetNumberOfOwnedCards()
        {
            int total = 0;
            for (int i = 0; i < Cards.Count; i++)
            {
                // TODO: Implement
            }
            return total;
        }

        public int GetNumberOfSpecficCard(int Id)
        {
            int Quantity = 0;

            if (Cards.ContainsKey(Id))
            {
                Cards.TryGetValue(Id, out Quantity);
            }
            return Quantity;
        }

        public int GetNumberOfSpecficLootCard(int Id)
        {
            int Quantity = 0;

            if (Redeemables.ContainsKey(Id))
            {
                Redeemables.TryGetValue(Id, out Quantity);
            }
            return Quantity;
        }

        public int GetNumberOfSpecficPack(int Id)
        {
            int Quantity = 0;

            if (Packs.ContainsKey(Id))
            {
                Packs.TryGetValue(Id, out Quantity);
            }
            return Quantity;
        }

        public int GetNumberOfSpecficPromoPack(int Id)
        {
            int Quantity = 0;

            if (PromoPacks.ContainsKey(Id))
            {
                PromoPacks.TryGetValue(Id, out Quantity);
            }
            return Quantity;
        }

        private int[] GetOwnedCardIds()
        {
            int[] ids = new int[Cards.Keys.Count];
            Cards.Keys.CopyTo(ids, 0);
            return ids;
        }

        private int[] GetOwnedLootCardIds()
        {
            int[] ids = new int[Redeemables.Keys.Count];
            Redeemables.Keys.CopyTo(ids, 0);
            return ids;
        }

        public void AddCardToCollection(int CardId, int Quantity)
        {
            if (Cards.ContainsKey(CardId))
                Cards[CardId] += Quantity;
            else
                Cards.Add(CardId, Quantity);
        }

        public void RemoveCardFromCollection(int CardId, int Quantity)
        {
            if (Cards[CardId] == 1)
                Cards.Remove(CardId);
            else
                Cards[CardId] -= Quantity;
        }

        public void AddLootCardToCollection(int CardId, int Quantity)
        {
            if (Redeemables.ContainsKey(CardId))
                Redeemables[CardId] += Quantity;
            else
                Redeemables.Add(CardId, Quantity);
        }

        public void RemoveLootCardFromCollection(int CardId, int Quantity)
        {
            if (Redeemables[CardId] == 1)
                Redeemables.Remove(CardId);
            else
                Redeemables[CardId] -= Quantity;
        }

        public void AddPackToCollection(int PackId, int Quantity)
        {
            if (Packs.ContainsKey(PackId))
                Packs[PackId] += Quantity;
            else
                Packs.Add(PackId, Quantity);
        }

        public void RemovePackFromCollection(int PackId, int Quantity)
        {
            if (Packs[PackId] == 1)
                Packs.Remove(PackId);
            else
                Packs[PackId] -= Quantity;
        }

        public void AddPromoPackToCollection(int PromoPackId, int Quantity)
        {
            if (PromoPacks.ContainsKey(PromoPackId))
                PromoPacks[PromoPackId] += Quantity;
            else
                PromoPacks.Add(PromoPackId, Quantity);
        }

        public void RemovePromoPackFromCollection(int PromoPackId, int Quantity)
        {
            if (PromoPacks[PromoPackId] == 1)
                PromoPacks.Remove(PromoPackId);
            else
                PromoPacks[PromoPackId] -= Quantity;
        }

        public void AddRedeemedToCollection(int CardId, int Quantity)
        {
            if (Redeemed.ContainsKey(CardId))
                Redeemed[CardId] += 1;
            else
                Redeemed.Add(CardId, Quantity);
        }

        public void IncreaseCardWant(int CardId)
        {
            if (Wants.ContainsKey(CardId))
                Wants[CardId] += 1;
            else
                Wants.Add(CardId, 1);
        }

        public void DecreaseCardWant(int CardId)
        {
            if (Wants[CardId] == 1)
                Wants.Remove(CardId);
            else
                Wants[CardId] -= 1;
        }

        public SortedDictionary<int, int> GetCardsDictionary()
        {
            return Cards;
        }

        public SortedDictionary<int, int> GetWantsDictionary()
        {
            return Wants;
        }

        public SortedDictionary<int, int> GetRedeemablesDictionary()
        {
            return Redeemables;
        }

        public SortedDictionary<int, int> GetPacksDictionary()
        {
            return Packs;
        }

        public List<CardData> GetAvatarData()
        {
            return Avatars;
        }

        public SortedDictionary<int, int> GetPromoPacksDictionary()
        {
            return PromoPacks;
        }

        public SortedDictionary<int, int> GetRedeemedDictionary()
        {
            return Redeemed;
        }

        public void SaveToDb()
        {
            string DbCards = dbSrv.GetString("collections", "cards", "id", Id.ToString());
            string DbRedeemables = dbSrv.GetString("collections", "redeemables", "id", Id.ToString());
            string DbPacks = dbSrv.GetString("collections", "packs", "id", Id.ToString());
            string DbPromoPacks = dbSrv.GetString("collections", "promopacks", "id", Id.ToString());
            string DbRedeemed = dbSrv.GetString("collections", "redeemed", "id", Id.ToString());
            string DbWants = dbSrv.GetString("collections", "wants", "id", Id.ToString());

            string CurrentCards = collectionSrv.BuildCollectionString(Cards);
            string CurrentRedeemables = collectionSrv.BuildCollectionString(Redeemables);
            string CurrentPacks = collectionSrv.BuildCollectionString(Packs);
            string CurrentPromoPacks = collectionSrv.BuildCollectionString(PromoPacks);
            string CurrentRedeemed = collectionSrv.BuildCollectionString(Redeemed);
            string CurrentWants = collectionSrv.BuildCollectionString(Wants);

            if (!DbCards.Equals(CurrentCards))
                dbSrv.ChangeValue("collections", "cards", CurrentCards, Id);
            if (!DbRedeemables.Equals(CurrentRedeemables))
                dbSrv.ChangeValue("collections", "redeemables", CurrentRedeemables, Id);
            if (!DbPacks.Equals(CurrentPacks))
                dbSrv.ChangeValue("collections", "packs", CurrentPacks, Id);
            if (!DbPromoPacks.Equals(CurrentPromoPacks))
                dbSrv.ChangeValue("collections", "promopacks", CurrentPromoPacks, Id);
            if (!DbRedeemed.Equals(CurrentRedeemed))
                dbSrv.ChangeValue("collections", "redeemed", CurrentRedeemed, Id);
            if (!DbWants.Equals(CurrentWants))
                dbSrv.ChangeValue("collections", "wants", CurrentWants, Id);
        }
    }
}
