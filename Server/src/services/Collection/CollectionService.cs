﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Command;

namespace Server.src.services.Collection
{
    public class CollectionService : Server
    {
        private readonly SortedDictionary<int, Collection> collections;

        // Custom set ranges
        private const int MIN_TRANSACTIONID = 0;
        private const int MAX_TRANSACTIONID = 1000000000;
        public const int LOOTCARD_INTERVAL = 10000;
        public const int CHOOSE_A_BOOSTER_ID = 0;

        // Seperator strings
        public const char PRIMARY_DELIMITER = '|';
        public const char SECONDARY_DELIMITER = ':';
        public const char TERTIARY_DELIMITER = ';';

        // Table Name
        public const string COLLECTIONS = "collections";

        // Table Columns
        public const string CARDS = "cards";
        public const string WANTS = "wants";
        public const string REDEEMABLES = "redeemables";
        public const string PACKS = "packs";
        public const string AVATARS = "avatars";
        public const string PROMOPACKS = "promopacks";
        public const string REDEEMED = "redeemed";

        public CollectionService()
        {
            collections = new SortedDictionary<int, Collection>();

            Cards.LoadGameCards();
            LoadCollections();
            logSrv.Log("CollectionService", "Loaded");
        }

        private void LoadCollections()
        {
            int[] playerIds = playerSrv.GetAllPlayerIds();
            logSrv.LogDebug("CollectionService", "Found " + playerIds.Length + " collections");

            for (int i = 0; i < playerIds.Length; i++)
            {
                CreateCollectionIfDoesntExist(playerIds[i]);
                AddCollection(playerIds[i]);
            }
        }

        private void CreateCollectionIfDoesntExist(int id)
        {
            if (dbSrv.GetNumberOfTableRows(COLLECTIONS, "id", id.ToString()) == 0)
            {
                dbSrv.CreateTableRow(COLLECTIONS, "id", id.ToString());
            }
        }

        public void AddCollection(int id)
        {
            collections.Add(id, new Collection(id));
        }

        public Collection GetCollection(int id)
        {
            return collections[id];
        }

        public void HandleRequestCollection(int id)
        {
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_COLLECTION, GetCollectionPacket(id)), conSrv.GetClientByID(id));
        }

        public CollectionPacket GetCollectionPacket(int id)
        {
            Collection PlayerCollection = GetCollection(id);

            // Update the packs dictionary in case player claimed a pack in the SWG server
            PlayerCollection.ReloadPacksInCollection();

            return new CollectionPacket(PlayerCollection.GetCardsDictionary(), PlayerCollection.GetWantsDictionary(), PlayerCollection.GetRedeemablesDictionary(), PlayerCollection.GetPacksDictionary(), PlayerCollection.GetPromoPacksDictionary(), PlayerCollection.GetRedeemedDictionary(), PlayerCollection.GetAvatarData());
        }

        private void AddCardToSwgRedeemable(int PlayerId, int CardId)
        {
            // Make sure the user has a row in the table
            bool hasRow = dbSrv.GetNumberOfTableRows("swg_redeemables", "id", PlayerId.ToString()) == 1;

            if (!hasRow)
            {
                dbSrv.CreateTableRow("swg_redeemables", "id", PlayerId.ToString());
            }

            string SwgRedeemablesString = dbSrv.GetString("swg_redeemables", "redeemables", "id", PlayerId.ToString());
            string[] SwgRedeemablesArray = SwgRedeemablesString.Split(PRIMARY_DELIMITER);

            int IndexOfCard = GetIndexOfCardIdInArray(SwgRedeemablesArray, CardId);

            if (IndexOfCard > -1)
            {
                string[] CardDetails = SwgRedeemablesArray[IndexOfCard].Split(SECONDARY_DELIMITER);
                int NewQuantity = int.Parse(CardDetails[1]) + 1;
                SwgRedeemablesArray[IndexOfCard] = CardDetails[0] + PRIMARY_DELIMITER + NewQuantity;

                // Update the string
                SwgRedeemablesString = string.Join(PRIMARY_DELIMITER, SwgRedeemablesArray);
            }
            else
            {
                string ItemString = CardId + SECONDARY_DELIMITER + ":1";

                if (!string.IsNullOrEmpty(SwgRedeemablesString))
                    SwgRedeemablesString += PRIMARY_DELIMITER.ToString();
                SwgRedeemablesString += ItemString;
            }

            // Save the new string
            dbSrv.ChangeValue("swg_redeemables", "redeemables", SwgRedeemablesString, PlayerId);
        }

        public void HandleRedeemLootCard(int PlayerId, int CardId, int CardQuantity)
        {
            if (GetCollection(PlayerId).GetNumberOfSpecficLootCard(CardId) == 0)
            {
                logSrv.LogWarning("CollectionService", "Player (" + PlayerId + ") attempted to redeem loot card they didn't have (" + CardId + ")");
                return;
            }

            AddCardToRedeemed(PlayerId, CardId, CardQuantity);
            AddCardToSwgRedeemable(PlayerId, CardId);

            CardId += LOOTCARD_INTERVAL;
            SortedDictionary<int, int> LootCard = new SortedDictionary<int, int>();
            LootCard.Add(CardId, CardQuantity);
            RemoveCardsFromCollection(PlayerId, LootCard);
            GetCollection(PlayerId).ReloadLootCardsInCollection();
            CollectionPacket CollectionPacket = GetCollectionPacket(PlayerId);
            int TransactionId = new Random().Next(MIN_TRANSACTIONID, MAX_TRANSACTIONID);
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDEEM_LOOTCARD, new CommandPacket(new object[] { LootCard, TransactionId, CollectionPacket })), conSrv.GetClientByID(PlayerId));
            logSrv.WriteCsLog(playerSrv.GetUsername(PlayerId), "REDEEM_LOOTCARD", "Card ID:" + CardId + ", Quantity: " + CardQuantity);
        }

        public void HandleRedeemPack(int PlayerId, int PackId)
        {
            // Get the player collection
            Collection PlayerCollection = GetCollection(PlayerId);

            // Make sure the player has a "Choose a Pack" loot card available
            const int ChooseALootId = 0;

            if (PlayerCollection.GetNumberOfSpecficLootCard(ChooseALootId) == 0)
            {
                logSrv.LogWarning("CollectionService", "Player (" + PlayerId + ") attempted to redeem a pack when they did not have a Choose a Loot lootcard.");
                return;
            }

            // Remove the lootcard from the collection
            SortedDictionary<int, int> LootCards = new SortedDictionary<int, int>();
            LootCards.Add(ChooseALootId + LOOTCARD_INTERVAL, 1);
            RemoveCardsFromCollection(PlayerId, LootCards);

            // Add pack to collection
            AddPackToCollection(PlayerId, PackId);

            // Get the transaction id
            int TransactionId = new Random().Next(MIN_TRANSACTIONID, MAX_TRANSACTIONID);

            // Get the updated collection packet
            CollectionPacket CollectionPacket = GetCollectionPacket(PlayerId);

            // Prepare the client a SortedDictionary
            SortedDictionary<int, int> Packs = new SortedDictionary<int, int>();
            Packs.Add(PackId, 1);

            // Show the delivery window
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDEEM_PACK, new CommandPacket(new object[] { Packs, TransactionId, CollectionPacket })), conSrv.GetClientByID(PlayerId));
        }

        public void HandleRedeemPromoPack(int PlayerId, int PackId)
        {
            // Get the player collection
            Collection PlayerCollection = GetCollection(PlayerId);

            // Make sure the player has a "Choose a Starter Deck" promo pack available
            if (PlayerCollection.GetNumberOfSpecficPromoPack(CHOOSE_A_BOOSTER_ID) == 0)
            {
                logSrv.LogWarning("CollectionService", "Player (" + PlayerId + ") attempted to redeem a pack when they did not have a Choose a Booster promo pack.");
                return;
            }

            // Remove the 'Choose a Booster' promopack from the collection
            RemovePromoPackFromCollection(PlayerId, CHOOSE_A_BOOSTER_ID);

            // Add the new pack to the collection
            AddPackToCollection(PlayerId, PackId);

            // Get the transaction id
            int TransactionId = new Random().Next(MIN_TRANSACTIONID, MAX_TRANSACTIONID);

            // Get the updated collection packet
            CollectionPacket CollectionPacket = GetCollectionPacket(PlayerId);

            // Prepare the client a SortedDictionary
            SortedDictionary<int, int> Packs = new SortedDictionary<int, int>();
            Packs.Add(PackId, 1);

            // Show the delivery window
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REDEEM_PROMOPACK, new CommandPacket(new object[] { Packs, TransactionId, CollectionPacket })), conSrv.GetClientByID(PlayerId));
        }

        private void AddCardToRedeemed(int PlayerId, int CardId, int CardQuantity)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.AddRedeemedToCollection(CardId, CardQuantity);
            Collection.SaveToDb();
        }

        private bool AddPromoPackToCollection(int PlayerId, int PromoPackId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.AddPromoPackToCollection(PromoPackId, 1);
            Collection.SaveToDb();
            return true;
        }

        private bool RemovePromoPackFromCollection(int PlayerId, int PromoPackId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.RemovePromoPackFromCollection(PromoPackId, 1);
            Collection.SaveToDb();
            return true;
        }

        private bool AddPackToCollection(int PlayerId, int PackId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.AddPackToCollection(PackId, 1);
            Collection.SaveToDb();
            return true;
        }

        private bool RemovePackFromCollection(int PlayerId, int PackId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.RemovePackFromCollection(PackId, 1);
            Collection.SaveToDb();
            return true;
        }

        public void HandleOpenPack(int PlayerId, int PackId)
        {
            bool Success = RemovePackFromCollection(PlayerId, PackId);

            if (Success)
            {
                SortedDictionary<int, int> CardsToGive = packSrv.GenerateCardsFromPack(PackId.ToString());

                AddCardsToCollection(PlayerId, CardsToGive);

                int TransactionId = dbSrv.GetNumberOfTableRows("transactions");

                // Let's make sure this TransactionId doesn't exist for some freak reason. If so, keep adding 1 until it isn't.
                int TransactionIdRows = dbSrv.GetNumberOfTableRows("transactions", "id", TransactionId.ToString());

                while (TransactionIdRows > 0)
                {
                    TransactionId++;
                    TransactionIdRows = dbSrv.GetNumberOfTableRows("transactions", "id", TransactionId.ToString());
                }

                CollectionPacket CollectionPacket = GetCollectionPacket(PlayerId);

                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.OPEN_PACK, new CommandPacket(new object[] { CardsToGive, TransactionId, CollectionPacket })), conSrv.GetClientByID(PlayerId));

                // Write logs
                logSrv.WriteTransactionLog(TransactionId, PlayerId, CardsToGive);
                logSrv.WriteCsLog(playerSrv.GetUsername(PlayerId), "OPEN_PACK", "Pack Type: " + PackId);
            }
        }

        public void GivePromoPack(int PlayerId, int PromoPackId)
        {
            bool Added = AddPromoPackToCollection(PlayerId, PromoPackId);

            if (Added)
                logSrv.LogDebug("CollectionService", "Gave player (" + PlayerId + ") promo pack (" + PromoPackId + ").");
            else
                logSrv.LogWarning("CollectionService", "Failed to give player (" + PlayerId + " promo pack (" + PromoPackId + ").");
        }

        public void AddCardsToCollection(int PlayerId, SortedDictionary<int, int> Cards)
        {
            Collection Collection = GetCollection(PlayerId);

            foreach (int CardId in Cards.Keys)
            {
                if (CardId < LOOTCARD_INTERVAL)
                {
                    Collection.AddCardToCollection(CardId, Cards[CardId]);
                }
                else
                {
                    int RealCardId = CardId - LOOTCARD_INTERVAL;
                    Collection.AddLootCardToCollection(RealCardId, Cards[CardId]);
                }
            }
            Collection.SaveToDb();
        }

        public void RemoveCardsFromCollection(int PlayerId, SortedDictionary<int, int> Cards)
        {
            Collection Collection = GetCollection(PlayerId);

            foreach (int CardId in Cards.Keys)
            {
                if (CardId < LOOTCARD_INTERVAL)
                {
                    Collection.RemoveCardFromCollection(CardId, Cards[CardId]);
                }
                else
                {
                    int RealCardId = CardId - LOOTCARD_INTERVAL;
                    Collection.RemoveLootCardFromCollection(RealCardId, Cards[CardId]);
                }
            }
            Collection.SaveToDb();
        }

        private int GetIndexOfCardIdInArray(string[] Cards, int Id)
        {
            string IdString = Id.ToString();
            for (int i = 0; i < Cards.Length; i++)
            {
                if (Cards[i].Split(SECONDARY_DELIMITER)[0].Equals(IdString))
                    return i;
            }
            return -1;
        }

        public void IncreaseCardWant(int PlayerId, int CardId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.IncreaseCardWant(CardId);
            Collection.SaveToDb();
        }

        public void DecreaseCardWant(int PlayerId, int CardId)
        {
            Collection Collection = GetCollection(PlayerId);
            Collection.DecreaseCardWant(CardId);
            Collection.SaveToDb();
        }

        public void CreateAvatarCard(int PlayerId, CardData AvatarData)
        {
            string Avatars = dbSrv.GetString(COLLECTIONS, AVATARS, "id", PlayerId.ToString());
            string CraftedString = AvatarData.GetTitle() + SECONDARY_DELIMITER + AvatarData.GetImage() + SECONDARY_DELIMITER + AvatarData.GetType() + SECONDARY_DELIMITER + AvatarData.GetArchetype() + SECONDARY_DELIMITER + AvatarData.GetAttack() + SECONDARY_DELIMITER + AvatarData.GetDefense() + SECONDARY_DELIMITER + AvatarData.GetBonus() + SECONDARY_DELIMITER + AvatarData.GetHealthOrLevel();

            if (!string.IsNullOrEmpty(Avatars))
                Avatars += PRIMARY_DELIMITER;
            dbSrv.ChangeValue(COLLECTIONS, AVATARS, Avatars + CraftedString, PlayerId);
            collectionSrv.GetCollection(PlayerId).ReloadAvatarsInCollection();
        }

        public string BuildCollectionString(SortedDictionary<int, int> Cards)
        {
            // Create the template arrays
            int[] CardIds = new int[Cards.Count];
            int[] CardQuantities = new int[Cards.Count];

            // Copy the data to the new arrays
            Cards.Keys.CopyTo(CardIds, 0);
            Cards.Values.CopyTo(CardQuantities, 0);

            // Create the string array
            string[] CardArray = new string[CardIds.Length];

            for (int i = 0; i < CardArray.Length; i++)
                CardArray[i] = CardIds[i].ToString() + SECONDARY_DELIMITER + CardQuantities[i];

            // Return the final string
            string FinalString = string.Join(PRIMARY_DELIMITER, CardArray);
            return FinalString;
        }
    }
}
