﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Command;
using TCGData.Packets.DeckBuilder;
using TCGData.Packets.Player;
using TCGData.Packets.Scenario;

namespace Server.src.services.Player
{
    public class PlayerService : Server
    {
        private readonly List<ClientData> ONLINE_PLAYERS;
        private readonly SortedDictionary<string, int> LEADERBOARD_INFO;

        private readonly List<PlayerObject> PLAYERS;

        private const int BASE_RATING = 1500;
        private const int LEADERBOARD_SHOWINGS = 50;

        private const string TBL_USERS = "users";

        private const string COL_ID = "id";
        private const string COL_USERNAME = "username";
        private const string COL_AVATAR = "avatar";
        private const string COL_MESSAGE = "message";
        private const string COL_FRIENDS = "friends";
        private const string COL_IGNORES = "ignores";
        private const string COL_GUILD = "guild";
        private const string COL_IM = "im";
        private const string COL_HOMEPAGE = "homepage";
        private const string COL_GAMES = "games";

        private int highestPop;

        public PlayerService()
        {
            PLAYERS = new List<PlayerObject>();
            ONLINE_PLAYERS = new List<ClientData>();
            LEADERBOARD_INFO = new SortedDictionary<string, int>();

            LoadPlayerObjects();
            LoadLeaderboardInfo();

            logSrv.Log("PlayerService", "Loaded");
        }

        public ClientData[] GetOnlinePlayers()
        {
            return ONLINE_PLAYERS.ToArray();
        }

        public ClientData GetOnlinePlayer(ClientData client)
        {
            return ONLINE_PLAYERS[ONLINE_PLAYERS.IndexOf(client)];
        }

        public void AddOnlinePlayer(ClientData playerToAdd)
        {
            ONLINE_PLAYERS.Add(playerToAdd);

            if (ONLINE_PLAYERS.Count > highestPop)
                highestPop = ONLINE_PLAYERS.Count;

            statusSrv.UpdateStatusXml();
        }

        public void RemoveOnlinePlayer(ClientData playerToRemove)
        {
            ONLINE_PLAYERS.Remove(playerToRemove);
            statusSrv.UpdateStatusXml();
        }

        public void RemoveOnlinePlayer(int playerIdToRemove)
        {
            RemoveOnlinePlayer(conSrv.GetClientByID(playerIdToRemove));
        }

        public int GetHighestPopulation()
        {
            return highestPop;
        }

        public int[] GetAllPlayerIds()
        {
            int[] PlayerIds = new int[PLAYERS.Count];

            for (int i = 0; i < PlayerIds.Length; i++)
                PlayerIds[i] = PLAYERS[i].GetId();
            return PlayerIds;
        }

        public bool PlayerLoggedIn(int PlayerId)
        {
            foreach (PlayerObject PlayerObject in PLAYERS)
                if (PlayerObject.GetId() == PlayerId)
                    return true;
            return false;
        }

        public ClientData GetClient(ClientData client)
        {
            return ONLINE_PLAYERS[ONLINE_PLAYERS.IndexOf(client)];
        }

        public ClientData GetLastCreatedClient()
        {
            return ONLINE_PLAYERS[ONLINE_PLAYERS.Count - 1];
        }

        private void LoadLeaderboardInfo()
        {
            int[] Ratings = new int[PLAYERS.Count];

            for (int i = 0; i < Ratings.Length; i++)
                Ratings[i] = GetPlayerRating(PLAYERS[i].GetGames());

            for (int i = 0; i < PLAYERS.Count; i++)
                LEADERBOARD_INFO.Add(PLAYERS[i].GetUsername(), Ratings[i]);
            logSrv.Log("PlayerService", "Loaded " + LEADERBOARD_INFO.Count + " Leaderboard Info Rows..");
        }

        private void LoadPlayerObjects()
        {
            // Get all player Ids.
            int[] PlayerIds = dbSrv.GetIntTableResults(TBL_USERS, "id");

            foreach (int PlayerId in PlayerIds)
                AddPlayerObject(PlayerId);

            logSrv.Log("PlayerService", "Loaded " + PLAYERS.Count + " Player Objects.");
        }

        private void AddPlayerObject(int PlayerId)
        {
            string Username = dbSrv.GetString(TBL_USERS, "username", "id", PlayerId.ToString());
            int Avatar = dbSrv.GetInt(TBL_USERS, "avatar", "id", PlayerId.ToString());
            string Message = dbSrv.GetString(TBL_USERS, "message", "id", PlayerId.ToString());

            string FriendsString = dbSrv.GetString(TBL_USERS, "friends", "id", PlayerId.ToString());
            string[] Friends = string.IsNullOrEmpty(FriendsString) ? new string[0] : FriendsString.Split(CollectionService.PRIMARY_DELIMITER);

            string IgnoresString = dbSrv.GetString(TBL_USERS, "username", "id", PlayerId.ToString());
            string[] Ignores = string.IsNullOrEmpty(IgnoresString) ? new string[0] : IgnoresString.Split(CollectionService.PRIMARY_DELIMITER);

            int GuildId = dbSrv.GetInt(TBL_USERS, "guild", "id", PlayerId.ToString());
            string IM = dbSrv.GetString(TBL_USERS, "im", "id", PlayerId.ToString());
            string HomePage = dbSrv.GetString(TBL_USERS, "homepage", "id", PlayerId.ToString());
            string Games = dbSrv.GetString(TBL_USERS, "games", "id", PlayerId.ToString());

            string ScenariosString = dbSrv.GetString(TBL_USERS, "scenarios", "id", PlayerId.ToString());
            string[] Scenarios = string.IsNullOrEmpty(ScenariosString) ? new string[0] : ScenariosString.Split(CollectionService.PRIMARY_DELIMITER);

            // Create PlayerObject and add it the the list of players
            PlayerObject PlayerObject = new PlayerObject(PlayerId, Username, Avatar, Message, Friends, Ignores, GuildId, IM, HomePage, Games, Scenarios);
            PLAYERS.Add(PlayerObject);
        }

        public PlayerObject GetPlayerObject(int PlayerId)
        {
            foreach (PlayerObject PlayerObject in PLAYERS)
                if (PlayerObject.GetId() == PlayerId)
                    return PlayerObject;
            return null;
        }

        public PlayerObject GetPlayerObject(string Username)
        {
            foreach (PlayerObject PlayerObject in PLAYERS)
                if (PlayerObject.GetUsername().Equals(Username))
                    return PlayerObject;
            return null;
        }

        public int GetPlayerRating(string wins)
        {
            string[] Parse = wins.Split('-');
            int rating = BASE_RATING;
            rating += int.Parse(Parse[0]) * 100;
            rating -= int.Parse(Parse[1]) * 100;
            return rating;
        }

        public SortedDictionary<string, int> GetLeaderboardInfo()
        {
            // We only need the top 50 players
            SortedDictionary<string, int> FinalList = new SortedDictionary<string, int>();
            string[] Usernames = new string[LEADERBOARD_INFO.Count];
            int[] Scores = new int[LEADERBOARD_INFO.Count];

            LEADERBOARD_INFO.Keys.CopyTo(Usernames, 0);
            LEADERBOARD_INFO.Values.CopyTo(Scores, 0);

            int max = LEADERBOARD_SHOWINGS;

            if (LEADERBOARD_INFO.Count < max)
            {
                max = LEADERBOARD_INFO.Count;
            }

            for (int i = 0; i < max; i++)
            {
                FinalList.Add(Usernames[i], Scores[i]);
            }
            return FinalList;
        }

        public string GetUsername(int PlayerId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);

            if (PlayerObject != null)
                return PlayerObject.GetUsername();
            return null;
        }

        public int GetId(string Username)
        {
            PlayerObject PlayerObject = GetPlayerObject(Username);

            if (PlayerObject != null)
                return PlayerObject.GetId();
            return -1;
        }

        public int GetPlayerAvatar(int PlayerId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            return PlayerObject.GetAvatar();
        }

        public void HandleSavePlayerAvatar(int PlayerId, string AvatarString)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            PlayerObject.SetAvatar(int.Parse(AvatarString));
            SyncPlayerToDatabase(PlayerId);
        }

        public void SendUserInfoPacket(string Username, ClientData Client)
        {
            PlayerObject PlayerObject = GetPlayerObject(Username);

            string Games = PlayerObject.GetGames();
            int Avatar = PlayerObject.GetAvatar();
            string Biography = PlayerObject.GetMessage();


            int GuildId = PlayerObject.GetGuildId();
            string Guild = GuildId != -1 ? guildSrv.GetGuildName(GuildId) : null;
            string HomePage = PlayerObject.GetHomePage();
            string IM = PlayerObject.GetIM();

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_USERINFO, new UserInfoPacket(Username, Games, Avatar, Biography, Guild, HomePage, IM)), Client);
        }

        public void SendFriendsIgnoreListPacket(int PlayerId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);

            string[] FriendIds = PlayerObject.GetFriends();
            string[] IgnoreIds = PlayerObject.GetIgnores();

            string[] FriendUsernames = new string[FriendIds.Length - 1];
            string[] IgnoreUsernames = new string[IgnoreIds.Length - 1];

            for (int i = 0; i < FriendUsernames.Length; i++)
            {
                FriendUsernames[i] = GetUsername(int.Parse(FriendIds[i]));
            }

            for (int i = 0; i < IgnoreUsernames.Length; i++)
            {
                IgnoreUsernames[i] = GetUsername(int.Parse(IgnoreIds[i]));
            }

            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_FRIENDSIGNORELIST, new FriendsIgnoreListPacket(FriendUsernames, IgnoreUsernames)), conSrv.GetClientByID(PlayerId));
        }

        public void HandleRequestScenarios(int PlayerId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.REQUEST_SCENARIOS, new ScenariosPacket(PlayerObject.GetScenarios())), conSrv.GetClientByID(PlayerId));
        }

        public void AddFriend(int PlayerId, string Friend)
        {
            if (!UsernameExists(Friend))
            {
                SendMessageToPlayer(PlayerId, "Username " + Friend + " doesn't exist.", "Failed to Add Friend");
                return;
            }

            int FriendId = GetId(Friend);

            if (PlayerId == FriendId)
            {
                SendMessageToPlayer(PlayerId, "You cannot add yourself to your friends list.", "Friends List Error");
                return;
            }

            if (!IsOnFriendsList(PlayerId, FriendId))
            {
                SendMessageToPlayer(PlayerId, Friend + " is already on your friends list.", "Failed to Add Friend");
                return;
            }

            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] FriendsList = PlayerObject.GetFriends();
            string[] NewFriendsList = new string[FriendsList.Length + 1];

            // Copy over old friends
            for (int i = 0; i < FriendsList.Length; i++)
            {
                NewFriendsList[i] = FriendsList[i];
            }

            // Add new friend
            NewFriendsList[NewFriendsList.Length - 1] = FriendId.ToString();

            // Update server data
            PlayerObject.SetFriends(NewFriendsList);

            // Send system message
            SendMessageToPlayer(PlayerId, "Added player " + Friend + " as a friend.", "Added Friend");

            // Sync DB
            SyncPlayerToDatabase(PlayerId);
        }

        public bool IsOnFriendsList(int PlayerId, int PlayerTargetId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] FriendsList = PlayerObject.GetFriends();

            foreach (string FriendId in FriendsList)
            {
                if (int.Parse(FriendId) == PlayerTargetId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsOnIgnoreList(int PlayerId, int PlayerTargetId)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] IgnoreList = PlayerObject.GetFriends();

            foreach (string IgnoreId in IgnoreList)
            {
                if (int.Parse(IgnoreId) == PlayerTargetId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsOnline(int PlayerId)
        {
            return conSrv.GetClientByID(PlayerId) != null;
        }

        public void RemoveFriend(int PlayerId, string Friend)
        {
            int FriendId = GetId(Friend);

            if (IsOnFriendsList(PlayerId, FriendId))
            {
                SendMessageToPlayer(PlayerId, Friend + " is not on your friends list.", "Failed to Remove Friend");
                return;
            }

            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] Friends = PlayerObject.GetFriends();
            List<string> FriendsList = new List<string>();

            // Add all friends that is not the target of who we want to remove
            for (int i = 0; i < Friends.Length; i++)
            {
                if (int.Parse(Friends[i]) != FriendId)
                {
                    FriendsList.Add(Friends[i]);
                }
            }

            // Send player system message
            SendMessageToPlayer(PlayerId, Friend + " has been removed from your friends list.", "Removed Friend");

            // Update server data
            PlayerObject.SetFriends(FriendsList.ToArray());

            // Sync the database
            SyncPlayerToDatabase(PlayerId);
        }

        public void IgnorePlayer(int PlayerId, string PlayerToIgnore)
        {
            if (!UsernameExists(PlayerToIgnore))
            {
                SendMessageToPlayer(PlayerId, PlayerToIgnore + " is not a valid username.", "Failed to Add Player To Ignore List");
                return;
            }

            int PlayerToIgnoreId = GetId(PlayerToIgnore);

            if (PlayerId == PlayerToIgnoreId)
            {
                SendMessageToPlayer(PlayerId, "You cannot add yourself to your ignore list.", "Ignore List Error");
                return;
            }

            if (IsOnIgnoreList(PlayerId, PlayerToIgnoreId))
            {
                SendMessageToPlayer(PlayerId, PlayerToIgnore + " is already on your ignore list.", "Failed to Add Player To Ignore List");
                return;
            }

            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] IgnoreList = PlayerObject.GetIgnores();
            string[] NewIgnoreList = new string[IgnoreList.Length + 1];

            for (int i = 0; i < IgnoreList.Length; i++)
            {
                NewIgnoreList[i] = IgnoreList[i];
            }

            NewIgnoreList[NewIgnoreList.Length - 1] = PlayerToIgnoreId.ToString();

            // Send system message
            SendMessageToPlayer(PlayerId, PlayerToIgnore + " has been added to your ignore list.", "Added Player to Ignore List");

            // Update server data
            PlayerObject.SetIgnores(NewIgnoreList);

            // Sync the database
            SyncPlayerToDatabase(PlayerId);
        }

        public void UnignorePlayer(int PlayerId, string PlayerToUnignore)
        {
            int IgnoreId = GetId(PlayerToUnignore);

            if (IsOnIgnoreList(PlayerId, IgnoreId))
            {
                SendMessageToPlayer(PlayerId, PlayerToUnignore + " is not on your ignore list.", "Failed to Remove Player From Ignore List");
                return;
            }

            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            string[] IgnoreList = PlayerObject.GetIgnores();
            List<string> NewIgnoreList = new List<string>();

            for (int i = 0; i < IgnoreList.Length; i++)
            {
                if (int.Parse(IgnoreList[i]) != IgnoreId)
                {
                    NewIgnoreList.Add(IgnoreList[i]);
                }
            }

            // Send system message to player
            SendMessageToPlayer(PlayerId, PlayerToUnignore + " has been removed from your ignore list.", "Removed Player From Ignore List");

            // Update server data
            PlayerObject.SetIgnores(NewIgnoreList.ToArray());

            // Sync database
            SyncPlayerToDatabase(PlayerId);
        }

        public bool UsernameExists(string Username)
        {
            return GetPlayerObject(Username) != null;
        }

        public void HandleSelectDeck(int PlayerId, string DeckTitle)
        {
            // Get the DeckPacket object
            DeckPacket Deck = deckBuildSrv.GetDeck(PlayerId, DeckTitle);

            // Get the player object
            PlayerObject Player = GetPlayerObject(PlayerId);

            // Save the DeckPacket object to the PlayerObject
            Player.SetSelectedDeck(Deck);
        }

        public void SendMessageToPlayer(int PlayerId, string Message, string Title)
        {
            conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.BROADCAST_MESSAGE, new CommandPacket(new string[] { Message, Title })), conSrv.GetClientByID(PlayerId));
            // TODO: Send textbox message?
        }

        public void HandleSaveAccountInfo(int PlayerId, string HomePage, string IM, string Message)
        {
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            PlayerObject.SetHomePage(HomePage);
            PlayerObject.SetIM(IM);
            PlayerObject.SetMessage(Message);

            // Sync database
            SyncPlayerToDatabase(PlayerId);
        }

        public void CreatePlayerDatabaseRows(string username)
        {
            logSrv.LogDebug("PlayerService", "Making user row");
            dbSrv.CreateTableRow(TBL_USERS, new string[] { COL_USERNAME }, new string[] { username });
            logSrv.LogDebug("PlayerService", "Made user row");

            // Get the PlayerId (must directly access the DB since the player isn't loaded yet).
            int PlayerId = dbSrv.GetInt(TBL_USERS, COL_ID, COL_USERNAME, username);
            string PlayerIdString = PlayerId.ToString();

            // Add the player object to the server's instance
            playerSrv.AddPlayerObject(PlayerId);

            logSrv.LogDebug("PlayerService", "Making collection row");
            dbSrv.CreateTableRow(CollectionService.COLLECTIONS, COL_ID, PlayerIdString);
            logSrv.LogDebug("PlayerService", "Made collection row");

            // Add the new collection to the server instance
            collectionSrv.AddCollection(PlayerId);

            logSrv.LogDebug("PlayerService", "Making deck row");
            dbSrv.CreateTableRow("decks", COL_ID, PlayerIdString);
            logSrv.LogDebug("PlayerService", "Made deck row");

            // Add the new deck to the server instance
            deckBuildSrv.LoadPlayerDeck(PlayerId);

            // Since this is a new player, give them the 'Choose a Booster' promo
            collectionSrv.GivePromoPack(PlayerId, CollectionService.CHOOSE_A_BOOSTER_ID);
        }

        public void SyncPlayerToDatabase(int PlayerId)
        {
            // Retrieve old db data
            int OldAvatar = dbSrv.GetInt(TBL_USERS, COL_AVATAR, COL_ID, PlayerId.ToString());
            string OldMessage = dbSrv.GetString(TBL_USERS, COL_MESSAGE, COL_ID, PlayerId.ToString());
            string OldFriends = dbSrv.GetString(TBL_USERS, COL_FRIENDS, COL_ID, PlayerId.ToString());
            string OldIgnores = dbSrv.GetString(TBL_USERS, COL_IGNORES, COL_ID, PlayerId.ToString());
            int OldGuildId = dbSrv.GetInt(TBL_USERS, COL_GUILD, COL_ID, PlayerId.ToString());
            string OldIM = dbSrv.GetString(TBL_USERS, COL_IM, COL_ID, PlayerId.ToString());
            string OldHomePage = dbSrv.GetString(TBL_USERS, COL_HOMEPAGE, COL_ID, PlayerId.ToString());
            string OldGames = dbSrv.GetString(TBL_USERS, COL_GAMES, COL_ID, PlayerId.ToString());

            // Latest server data
            PlayerObject PlayerObject = GetPlayerObject(PlayerId);
            int NewAvatar = PlayerObject.GetAvatar();
            string NewMessage = PlayerObject.GetMessage();
            string NewFriends = string.Join(CollectionService.PRIMARY_DELIMITER, PlayerObject.GetFriends());
            string NewIgnores = string.Join(CollectionService.PRIMARY_DELIMITER, PlayerObject.GetIgnores());
            int NewGuildId = PlayerObject.GetGuildId();
            string NewIM = PlayerObject.GetIM();
            string NewHomePage = PlayerObject.GetHomePage();
            string NewGames = PlayerObject.GetGames();

            // Compare the different data
            if (OldAvatar != NewAvatar)
                dbSrv.ChangeValue(TBL_USERS, COL_AVATAR, NewAvatar.ToString(), PlayerId);

            if (!OldMessage.Equals(NewMessage))
                dbSrv.ChangeValue(TBL_USERS, COL_MESSAGE, NewMessage, PlayerId);

            if (!OldFriends.Equals(NewFriends))
                dbSrv.ChangeValue(TBL_USERS, COL_FRIENDS, NewFriends, PlayerId);

            if (!OldIgnores.Equals(NewIgnores))
                dbSrv.ChangeValue(TBL_USERS, COL_IGNORES, NewIgnores, PlayerId);

            if (OldGuildId != NewGuildId)
                dbSrv.ChangeValue(TBL_USERS, COL_GUILD, NewGuildId.ToString(), PlayerId);

            if (!OldIM.Equals(NewIM))
                dbSrv.ChangeValue(TBL_USERS, COL_IM, NewIM, PlayerId);

            if (!OldHomePage.Equals(NewHomePage))
                dbSrv.ChangeValue(TBL_USERS, COL_HOMEPAGE, NewHomePage, PlayerId);

            if (!OldGames.Equals(NewGames))
                dbSrv.ChangeValue(TBL_USERS, COL_GAMES, NewGames, PlayerId);
        }
    }
}
