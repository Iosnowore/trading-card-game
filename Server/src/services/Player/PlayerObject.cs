﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TCGData.Packets.DeckBuilder;

namespace Server.src.services.Player
{
    public class PlayerObject
    {
        // Only variables player cannot change.
        private readonly int Id;
        private readonly string Username;

        private int Avatar;
        private string Message;

        private string[] Friends;
        private string[] Ignores;

        private int GuildId;
        private string IM;
        private string HomePage;
        private string Games;
        private readonly string[] Scenarios;

        private DeckPacket SelectedDeck;

        public PlayerObject(int Id, string Username, int Avatar, string Message, string[] Friends, string[] Ignores, int GuildId, string IM, string HomePage, string Games, string[] Scenarios)
        {
            this.Id = Id;
            this.Username = Username;

            this.Avatar = Avatar;
            this.Message = Message;

            this.Friends = Friends;
            this.Ignores = Ignores;

            this.GuildId = GuildId;
            this.IM = IM;
            this.HomePage = HomePage;
            this.Games = Games;
            this.Scenarios = Scenarios;
        }

        public void SetAvatar(int Avatar)
        {
            this.Avatar = Avatar;
        }

        public void SetMessage(string Message)
        {
            this.Message = Message;
        }

        public void SetFriends(string[] Friends)
        {
            this.Friends = Friends;
        }

        public void SetIgnores(string[] Ignores)
        {
            this.Ignores = Ignores;
        }

        public void SetGuildId(int GuildId)
        {
            this.GuildId = GuildId;
        }

        public void SetIM(string IM)
        {
            this.IM = IM;
        }

        public void SetHomePage(string HomePage)
        {
            this.HomePage = HomePage;
        }

        public void SetGames(string Games)
        {
            this.Games = Games;
        }

        public void SetSelectedDeck(DeckPacket SelectedDeck)
        {
            this.SelectedDeck = SelectedDeck;
        }

        public int GetId()
        {
            return Id;
        }

        public string GetUsername()
        {
            return Username;
        }

        public int GetAvatar()
        {
            return Avatar;
        }

        public string GetMessage()
        {
            return Message;
        }

        public string[] GetFriends()
        {
            return Friends;
        }

        public string[] GetIgnores()
        {
            return Ignores;
        }

        public int GetGuildId()
        {
            return GuildId;
        }

        public string GetIM()
        {
            return IM;
        }

        public string GetHomePage()
        {
            return HomePage;
        }

        public string GetGames()
        {
            return Games;
        }

        public string[] GetScenarios()
        {
            return Scenarios;
        }

        public DeckPacket GetSelectedDeck()
        {
            return SelectedDeck;
        }
    }
}
