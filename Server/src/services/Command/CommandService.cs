﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TCGData;

namespace Server.src.services.Command
{
    public class CommandService : Server
    {
        public CommandService()
        {
            logSrv.Log("CommandService", "Loaded");
        }

        public void HandleCommand(PacketSender p)
        {
            //SendPacketToClients(p, playerSrv.GetOnlinePlayers());
            //consoleSrv.WriteConsoleMessage(p.GetString(0) + " used command " + p.GetString(1));
            // TODO: Add command args to console writeout
        }

        public string DecodeCommand(string data)
        {
            if (!data.Contains(" "))
                return data;

            return data.Split(' ')[0];
        }

        public string DecodeArgs(string data)
        {
            return data.Remove(0, data.IndexOf(' ') + 1);
        }
    }
}
