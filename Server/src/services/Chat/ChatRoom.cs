﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Linq;

using static TCGData.ChatTypes;

namespace Server.src.services.Chat
{
    public class ChatRoom : Server
    {
        private readonly ClientData[] clients;
        private readonly string title;
        private readonly ChatType chatType;

        public ChatRoom(string title, int capacity, ChatType chatType)
        {
            this.title = title;
            this.chatType = chatType;
            clients = new ClientData[capacity];
        }

        private int FindValueInRoom(object value)
        {
            for (int i = 0; i < clients.Length; i++)
                if (clients[i] == value)
                    return i;
            return -1;
        }

        private void ChangeValueInRoom(object oldValue, object newValue)
        {
            int index = FindValueInRoom(oldValue);

            if (index != -1)
                clients[index] = (ClientData)newValue;
        }

        public void AddClientToRoom(ClientData client)
        {
            ChangeValueInRoom(null, client);
        }

        public void RemoveClientFromRoom(ClientData client)
        {
            ChangeValueInRoom(client, null);
        }

        public ClientData[] GetClientsInRoom()
        {
            List<ClientData> actualClients = new List<ClientData>();

            foreach (ClientData client in clients)
                if (client != null)
                    actualClients.Add(client);
            return actualClients.ToArray();
        }

        public bool ContainsClient(ClientData client)
        {
            return clients.Contains(client);
        }

        public string GetTitle()
        {
            return title;
        }

        public ChatType GetChatType()
        {
            return chatType;
        }

        public bool IsFull()
        {
            return FindValueInRoom(null) == -1;
        }
    }
}
