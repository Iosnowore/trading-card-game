﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData;
using static TCGData.ChatTypes;

namespace Server.src.services.Chat
{
    public class ChatRoomService : Server
    {
        private readonly List<ChatRoom> chatRooms;

        public ChatRoomService()
        {
            chatRooms = new List<ChatRoom>();
            GenerateDefaultChatRooms();
            logSrv.Log("ChatRoomService", "Loaded");
        }

        public void CreateChatRoom(string title, int capacity, ChatType chatType)
        {
            chatRooms.Add(new ChatRoom(title, capacity, chatType));
            logSrv.Log("ChatRoomService", "Created Chat Room: " + title);
        }

        public ChatRoom GetChatRoom(string title)
        {
            foreach (ChatRoom room in chatRooms)
                if (room.GetTitle().Equals(title))
                    return room;
            return null;
        }

        public int GetNumberOfRooms()
        {
            return chatRooms.Count;
        }

        public ChatRoom GetRoom(int i)
        {
            return chatRooms[i];
        }

        public ChatRoom GetChatRoomLobby(string title)
        {
            List<ChatRoom> lobbyChatRooms = GetLobbyChatRooms();

            foreach (ChatRoom chatRoom in lobbyChatRooms)
                if (chatRoom.GetTitle().Equals(title))
                    return chatRoom;
            return null;
        }

        public ChatRoom GetChatRoomMatch(string title)
        {
            List<ChatRoom> MatchChatRooms = GetMatchChatRooms();

            foreach (ChatRoom ChatRoom in MatchChatRooms)
                if (ChatRoom.GetTitle().Equals(title))
                    return ChatRoom;
            return null;
        }

        private List<ChatRoom> GetLobbyChatRooms()
        {
            List<ChatRoom> lobbyChatRooms = new List<ChatRoom>();

            foreach (ChatRoom chatRoom in chatRooms)
                if (chatRoom.GetChatType() == ChatType.LOBBY)
                    lobbyChatRooms.Add(chatRoom);
            return lobbyChatRooms;
        }

        private List<ChatRoom> GetMatchChatRooms()
        {
            List<ChatRoom> MatchChatRooms = new List<ChatRoom>();

            foreach (ChatRoom ChatRoom in chatRooms)
                if (ChatRoom.GetChatType() == ChatType.MATCH)
                    MatchChatRooms.Add(ChatRoom);
            return MatchChatRooms;
        }

        public void AddClientToChatRoom(string roomTitle, ClientData client)
        {
            // Get the ChatRoom the user is in
            ChatRoom ChatRoom = GetChatRoom(roomTitle);

            // Add the client to the chatroom
            ChatRoom.AddClientToRoom(client);

            logSrv.LogDebug("ChatRoomService", "Added player (" + client.GetId() + ") to room (" + roomTitle + ")");
        }

        public void RemoveClientFromChatRoom(string roomTitle, ClientData client)
        {
            // Get the ChatRoom the user is in
            ChatRoom ChatRoom = GetChatRoom(roomTitle);

            // Remove the client from the chatroom
            ChatRoom.RemoveClientFromRoom(client);

            logSrv.LogDebug("ChatRoomService", "Removed player (" + client.GetId() + ") from room (" + roomTitle + ")");
        }

        public void RemoveClientFromAnyChatRooms(ClientData client)
        {
            foreach (ChatRoom room in chatRooms)
                if (room.ContainsClient(client))
                    room.RemoveClientFromRoom(client);
        }

        public void SendPacketToChatRoom(string lobbyTitle, PacketSender p)
        {
            conSrv.SendPacketToClients(p, GetChatRoom(lobbyTitle).GetClientsInRoom());
        }

        public void GenerateDefaultChatRooms()
        {
            logSrv.LogDebug("ChatRoomService", "Loading default lobby chat rooms");
            for (int i = 0; i < LobbyTitles.DEFAULT_LOBBY_TITLES.Length; i++)
                CreateChatRoom(LobbyTitles.DEFAULT_LOBBY_TITLES[i], 6, ChatType.LOBBY);
            logSrv.LogDebug("ChatRoomService", "Loaded default lobby chat rooms");

            logSrv.LogDebug("ChatRoomService", "Loading guild chat rooms..");

            string[] guilds = dbSrv.GetStringTableResults("guilds", "name");

            for (int i = 0; i < guilds.Length; i++)
                CreateChatRoom(guilds[i], 10, ChatType.GUILD);
            logSrv.LogDebug("ChatRoomService", "Loaded guild chat rooms..");
        }

        public void DeleteChatRoom(ChatRoom ChatRoom)
        {
            logSrv.LogDebug("ChatRoomService", "Deleting chatroom (" + ChatRoom.GetTitle() + ")");
            chatRooms.Remove(ChatRoom);
        }
    }
}
