﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Command;

namespace Server.src.services.Chat
{
    public class ChatService : Server
    {
        private readonly List<int> MutedPlayers = new List<int>();

        public ChatService()
        {
            logSrv.Log("ChatService", "Loaded");
        }

        public void DealWithGuildChatPacket(CommandPacket Packet)
        {
            string PlayerUsername = Packet.GetContent(0).ToString();
            string GuildName = dbSrv.GetString("users", "guild", "username", PlayerUsername);
            chatRoomSrv.SendPacketToChatRoom(GuildName, new PacketSender(SERVER_ID, Commands.GUILD_CHAT, new CommandPacket(new string[] { GameData.ServerId, GuildName + ": " + Packet.GetContent(0) + ": " + Packet.GetContent(1) })));
            logSrv.Log("ChatService", "[" + GuildName + " GUILDCHAT] " + Packet.GetContent(0) + ": " + Packet.GetContent(1));
        }

        public void DealWithLobbyChatPacket(CommandPacket Packet)
        {
            string PlayerUsername = Packet.GetContent(0).ToString();
            int PlayerId = playerSrv.GetId(PlayerUsername);

            if (!PlayerIsMuted(PlayerId))
            {
                string lobbyTitle = lobbySrv.GetLobbyTitleUserIsIn(PlayerId);
                chatRoomSrv.SendPacketToChatRoom(lobbyTitle, new PacketSender(SERVER_ID, Commands.LOBBY_CHAT, Packet));
                logSrv.Log("ChatService", "[" + lobbyTitle + " LOBBYCHAT] " + Packet.GetContent(0) + ": " + Packet.GetContent(1));
            }
            else
            {
                playerSrv.SendMessageToPlayer(PlayerId, "You are currently muted and cannot talk in any lobbies.", "Message Failed");
            }
        }

        public void DealWithMatchChatPacket(CommandPacket Packet)
        {
            string PlayerUsername = Packet.GetContent(0).ToString();
            int PlayerId = playerSrv.GetId(PlayerUsername);
            string matchTitle = matchSrv.GetMatchTitleUserIsIn(PlayerId);
            chatRoomSrv.SendPacketToChatRoom(matchTitle, new PacketSender(SERVER_ID, Commands.MATCH_CHAT, Packet));
            logSrv.Log("ChatService", "[" + matchTitle + " MATCHCHAT] " + Packet.GetContent(0) + ": " + Packet.GetContent(1));
        }

        public void DealWithWhisperChatPacket(CommandPacket Packet)
        {
            // TODO: implement.. really this is the easiest out of the 4 chat types
        }

        public bool PlayerIsMuted(int PlayerId)
        {
            return MutedPlayers.Contains(PlayerId);
        }

        public void MutePlayer(int PlayerId)
        {
            MutedPlayers.Add(PlayerId);
        }

        public void UnMutePlayer(int PlayerId)
        {
            MutedPlayers.Remove(PlayerId);
        }
    }
}
