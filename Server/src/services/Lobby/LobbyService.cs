﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets.Lobby;

namespace Server.src.services.Lobby
{
    public class LobbyService : Server
    {
        private readonly List<Lobby> lobbies;

        public LobbyService()
        {
            lobbies = new List<Lobby>();
            LoadDefaultLobbies();
            logSrv.Log("LobbyService", "Loaded");
        }

        public void CreateLobby(string lobbyTitle, int PlayerCapacity)
        {
            lobbies.Add(new Lobby(lobbyTitle, PlayerCapacity));
            logSrv.LogDebug("LobbyService", "Created lobby: " + lobbyTitle);
        }

        private void LoadDefaultLobbies()
        {
            logSrv.LogDebug("LobbyService", "Loading default lobbies..");
            for (int i = 0; i < LobbyTitles.DEFAULT_LOBBY_TITLES.Length; i++)
                CreateLobby(LobbyTitles.DEFAULT_LOBBY_TITLES[i], 6);
            logSrv.LogDebug("LobbyService", "Loaded default lobbies");
        }

        public Lobby GetLobby(string lobbyTitle)
        {
            foreach (Lobby lobby in lobbies)
                if (lobby.GetTitle().Equals(lobbyTitle))
                    return lobby;
            return null;
        }

        public Lobby GetLobby(int Index)
        {
            return lobbies[Index];
        }

        public void AddClientToLobby(string lobbyTitle, ClientData client)
        {
            Lobby lobby = GetLobby(lobbyTitle);
            lobby.AddClient(client);

            string[] Usernames = GetLobby(lobbyTitle).GetUsernamesInLobby();

            Dictionary<string, int> Players = new Dictionary<string, int>();

            foreach (string Username in Usernames)
            {
                Players.Add(Username, -1);
            }
            matchSrv.SendUpdatedLobbyMatches(lobby, client);
            lobby.SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.UPDATE_LOBBY_USERNAMES, new UpdateLobbyUsersPacket(playerSrv.GetUsername(client.GetId()) + " joined the lobby.", Players)));
        }

        public void RemoveClientFromLobby(string lobbyTitle, ClientData client)
        {
            GetLobby(lobbyTitle).RemoveClient(client);
        }

        public void RemoveClientFromAnyLobbies(ClientData client)
        {
            foreach (Lobby lobby in lobbies)
                lobby.RemoveClient(client);
        }

        public void RemoveClientFromAnyLobbies(int Id)
        {
            ClientData Client = conSrv.GetClientByID(Id);

            foreach (Lobby lobby in lobbies)
                lobby.RemoveClient(Client);
        }

        public string GetLobbyTitleUserIsIn(int UserId)
        {
            foreach (Lobby lobby in lobbies)
                if (lobby.UserIsInLobby(UserId))
                    return lobby.GetTitle();
            return null;
        }

        public Lobby GetLobbyUserIsIn(int UserId)
        {
            foreach (Lobby lobby in lobbies)
                if (lobby.UserIsInLobby(UserId))
                    return lobby;
            return null;
        }

        public void HandleRequestLobbyInfo(int PlayerId)
        {
            string[] LobbyInfo = new string[LobbyTitles.DEFAULT_LOBBY_TITLES.Length - 2];

            Lobby Lobby;

            for (int i = 0; i < LobbyInfo.Length; i++)
            {
                Lobby = GetLobby(LobbyTitles.DEFAULT_LOBBY_TITLES[i]);
                LobbyInfo[i] = Lobby.GetUsernamesInLobby().Length.ToString() + CollectionService.SECONDARY_DELIMITER + Lobby.GetPlayerCapacity() + CollectionService.SECONDARY_DELIMITER + Lobby.GetNumberOfMatchesInLobby() + CollectionService.SECONDARY_DELIMITER + 0;
            }

            LobbyInfoPacket InfoPacket = new LobbyInfoPacket(LobbyInfo);
            PacketSender Sender = new PacketSender(SERVER_ID, Commands.REQUEST_LOBBYINFO, InfoPacket);

            conSrv.SendPacketToClient(Sender, conSrv.GetClientByID(PlayerId));
        }

        public int GetNumberOfLobbies()
        {
            return lobbies.Count;
        }
    }
}
