﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

using TCGData;
using TCGData.Packets.Lobby;

namespace Server.src.services.Lobby
{
    public class Lobby : Server
    {
        private readonly List<ClientData> clients;
        private readonly List<Match.Match> Matches;
        private readonly string title;
        private readonly int PlayerCapacity;

        public Lobby(string title, int PlayerCapacity)
        {
            clients = new List<ClientData>(PlayerCapacity);
            Matches = new List<Match.Match>();

            this.title = title;
            this.PlayerCapacity = PlayerCapacity;
        }

        public string GetTitle()
        {
            return title;
        }

        public ClientData GetClient(int index)
        {
            return clients[index];
        }

        public void AddClient(ClientData client)
        {
            clients.Add(client);
        }

        public void RemoveClient(ClientData client)
        {
            clients.Remove(client);

            if (clients.Count > 0)
            {
                string[] Usernames = GetUsernamesInLobby();
                Dictionary<string, int> Players = new Dictionary<string, int>();

                foreach (string Username in Usernames)
                {
                    Players.Add(Username, -1);
                }

                SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.UPDATE_LOBBY_USERNAMES, new UpdateLobbyUsersPacket(playerSrv.GetUsername(client.GetId()) + " left the lobby.", Players)));
            }
        }

        public int GetIndexOfClient(ClientData client)
        {
            for (int i = 0; i < clients.Count; i++)
                if (clients[i] == client)
                    return i;
            return -1;
        }

        public int GetPlayerCapacity()
        {
            return PlayerCapacity;
        }

        public void SendPacketToPlayers(PacketSender p)
        {
            conSrv.SendPacketToClients(p, clients.ToArray());
        }

        public bool UserIsInLobby(int id)
        {
            for (int i = 0; i < clients.Count; i++)
                if (clients[i].GetId() == id)
                    return true;
            return false;
        }

        public string[] GetUsernamesInLobby()
        {
            string[] Users = new string[clients.Count];
            for (int i = 0; i < Users.Length; i++)
                Users[i] = playerSrv.GetUsername(clients[i].GetId());
            return Users;

        }

        public void AddMatchToLobby(Match.Match Match)
        {
            Matches.Add(Match);
        }

        public void RemoveMatchFromLobby(Match.Match Match)
        {
            Matches.Remove(Match);
        }

        public int GetNumberOfMatchesInLobby()
        {
            return Matches.Count;
        }

        public Match.Match GetMatchUserIsIn(int Id)
        {
            foreach (Match.Match Match in Matches)
                if (Match.GetCreatorId() == Id)
                    return Match;
            return null;
        }

        public Match.Match GetMatch(int Index)
        {
            return Matches[Index];
        }
    }
}
