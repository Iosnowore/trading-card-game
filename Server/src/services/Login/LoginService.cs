﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Web;
using System.Net;
using System.Reflection;
using TCGData;
using TCGData.Packets;
using static TCGData.LoginPacketResponses;

namespace Server.src.services.Login
{
    public class LoginService : Server
    {
        private readonly string AUTH_URL;
        private readonly string RESPONSE_BANNED;
        private readonly string RESPONSE_STAFF;
        private readonly string RESPONSE_SUCCESS;
        private readonly bool STAFF_ONLY;

        public LoginService()
        {
            AUTH_URL = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "AUTH_URL");
            RESPONSE_BANNED = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "RESPONSE_BANNED");
            RESPONSE_STAFF = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "RESPONSE_STAFF");
            RESPONSE_SUCCESS = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "RESPONSE_SUCCESS");
            STAFF_ONLY = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "STAFF_ONLY").Equals("1");

            logSrv.Log("LoginService", "Loaded");
        }

        private string GetAuthResponse(string un, string pw, string ip)
        {
            using WebClient WebClient = new WebClientWithTimeout
            {
                Proxy = null
            };

            try
            {
                WebClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                return WebClient.UploadString(AUTH_URL, "un=" + un + "&pw=" + pw + "&ip=" + ip);
            }
            catch (WebException ex)
            {
                logSrv.LogWarning("LoginService", "Unable to get auth response: " + ex.Message + " - Trying again");
                return GetAuthResponse(un, pw, ip);
            }
        }

        public void ValidateCredentials(ClientData client, LoginValidationPacket Packet)
        {
            PacketSender p;

            if (Packet.GetClientVersion() == Assembly.GetExecutingAssembly().GetName().Version)
            {
                string un = Packet.GetUsername();
                string pw = Packet.GetPassword();
                string Response = RESPONSE_SUCCESS;

                if (!string.IsNullOrEmpty(AUTH_URL))
                {
                    logSrv.LogDebug("LoginService", "About to check auth result");
                    Response = GetAuthResponse(un, pw, client.GetIpAddress());
                    logSrv.LogDebug("LoginService", "GOT AUTH RESPONSE: " + Response);
                }

                if ((Response.Equals(RESPONSE_SUCCESS) && !STAFF_ONLY) || Response.Equals(RESPONSE_STAFF))
                {
                    string correctedUn = dbSrv.GetString("users", "username", "username", un);

                    if (string.IsNullOrEmpty(correctedUn))
                    {
                        correctedUn = un;
                        playerSrv.CreatePlayerDatabaseRows(un);
                    }

                    int playerId = playerSrv.GetId(correctedUn);
                    client.SetId(playerId);

                    ClientData[] Clients = playerSrv.GetOnlinePlayers();

                    foreach (ClientData Client in Clients)
                    {
                        if (Client.GetId() == playerId && Client != client)
                        {
                            clientSrv.DisconnectClient(Client);
                            break;
                        }
                    }

                    int GuildId = guildSrv.GetUsersGuildId(playerId);

                    string GuildName = null;
                    string GuildMessage = null;

                    if (GuildId != -1)
                    {
                        GuildName = guildSrv.GetGuildName(GuildId);
                        GuildMessage = guildSrv.GetGuildMessage(GuildId);

                        chatRoomSrv.AddClientToChatRoom(GuildName, client);
                    }
                    p = new PacketSender(SERVER_ID, Commands.LOGIN_VALIDATION, new LoginValidationResponsePacket(Responses.SUCCESS, playerId, correctedUn, playerSrv.GetLeaderboardInfo(), GuildName, GuildMessage));
                    logSrv.LogDebug("LoginService", correctedUn + " logged on the server.");
                }
                else if (Response.Equals(RESPONSE_BANNED))
                {
                    p = new PacketSender(SERVER_ID, Commands.LOGIN_VALIDATION, new LoginValidationResponsePacket(Responses.BANNED, -1, null, null, null, null));
                }
                else if (Response.Equals(RESPONSE_SUCCESS))
                {
                    p = new PacketSender(SERVER_ID, Commands.LOGIN_VALIDATION, new LoginValidationResponsePacket(Responses.CLOSED, -1, null, null, null, null));
                }
                else
                {
                    p = new PacketSender(SERVER_ID, Commands.LOGIN_VALIDATION, new LoginValidationResponsePacket(Responses.FAILURE, -1, null, null, null, null));
                }
            }
            else
            {
                logSrv.LogDebug("LoginService", "WRONG CLIENT VERSION.. Server=" + Assembly.GetExecutingAssembly().GetName().Version + ",Client=" + Packet.GetClientVersion());

                p = new PacketSender(SERVER_ID, Commands.LOGIN_VALIDATION, new LoginValidationResponsePacket(Responses.WRONG_VERSION, -1, null, null, null, null));
            }
            conSrv.SendPacketToClient(p, client);
        }
    }
}
