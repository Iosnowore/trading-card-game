﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Timers;
using TCGData;

namespace Server.src.services.Ping
{
    public class PingPlayerService : Server
    {
        private readonly List<int> NeedingResponse;

        private const int PING_COOLDOWN = 5000;

        private readonly Timer Timer;

        public PingPlayerService()
        {
            NeedingResponse = new List<int>();

            PingPlayers();

            Timer = new Timer
            {
                Interval = PING_COOLDOWN
            };
            Timer.Elapsed += Timer_Elapsed;
            Timer.Start();

            logSrv.Log("PingPlayerService", "Loaded");
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            KickUnrespondedPlayers();

            PingPlayers();
        }

        public void PingPlayers()
        {
            NeedingResponse.Clear();

            ClientData[] Clients = playerSrv.GetOnlinePlayers();

            PacketSender Sender = new PacketSender(SERVER_ID, Commands.CRASH_PING, null);

            foreach (ClientData Client in Clients)
            {
                NeedingResponse.Add(Client.GetId());
            }

            conSrv.SendPacketToClients(Sender, Clients);
        }

        public void RemovePlayerFromNeedingResponse(int Id)
        {
            NeedingResponse.Remove(Id);
        }

        public void KickUnrespondedPlayers()
        {
            foreach (int Id in NeedingResponse)
            {
                logSrv.LogWarning("PingPlayerService", "Player (" + Id + ") appears to have crashed. Removing from online list");

                // Remove the player from any lobbies
                lobbySrv.RemoveClientFromAnyLobbies(Id);

                // Remove the player from any ChatRooms
                chatRoomSrv.RemoveClientFromAnyChatRooms(conSrv.GetClientByID(Id));

                // Remove any games that the player is in
                gameSrv.RemovePlayerFromAnyGames(Id);

                // Remove from online list
                playerSrv.RemoveOnlinePlayer(Id);
            }
        }
    }
}
