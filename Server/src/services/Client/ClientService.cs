﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Connection;

namespace Server.src.services.Client
{
    public class ClientService : Server
    {
        public ClientService()
        {
            logSrv.Log("ClientService", "Loaded");
        }

        public void DisconnectClient(ClientData client)
        {
            if (client != null)
            {
                playerSrv.RemoveOnlinePlayer(client);
                logSrv.Log("ClientService", playerSrv.GetUsername(client.GetId()) + " logged off");
                lobbySrv.RemoveClientFromAnyLobbies(client);
                chatRoomSrv.RemoveClientFromAnyChatRooms(client);
                gameSrv.RemovePlayerFromAnyGames(client.GetId());

                // Interrupt the thread if we are running TCP
                if (conSrv.SERVER_PROTOCOL == ConnectionService.PROTOCOL_TCP)
                    client.InterruptThread();
            }
        }
    }
}
