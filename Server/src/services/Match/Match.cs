﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Chat;
using System.Collections.Generic;
using TCGData;
using TCGData.Packets;
using TCGData.Packets.Lobby;
using static TCGData.ChatTypes;

namespace Server.src.services.Match
{
    public class Match : Server
    {
        private readonly int CreatorId;

        private readonly ClientData[] Clients;
        private readonly ChatRoom ChatRoom;

        private MatchPacket MatchInfo;

        private readonly bool[] PlayersReady;

        public Match(int CreatorId, MatchPacket MatchInfo)
        {
            this.CreatorId = CreatorId;
            this.MatchInfo = MatchInfo;

            PlayersReady = new bool[MatchInfo.GetMaxPlayers()];

            Clients = new ClientData[MatchInfo.GetMaxPlayers()];
            chatRoomSrv.CreateChatRoom(MatchInfo.GetTitle(), Clients.Length, ChatType.MATCH);
            ChatRoom = chatRoomSrv.GetChatRoom(MatchInfo.GetTitle());
        }

        public string GetTitle()
        {
            return MatchInfo.GetTitle();
        }

        public int GetCreatorId()
        {
            return CreatorId;
        }

        private void UpdateMatchPacketWithNewUser(string Username, int Avatar)
        {
            ClientData[] Clients = GetClients();
            string[] Players = new string[Clients.Length + 1];

            for (int i = 0; i < Players.Length - 1; i++)
            {
                int Id = Clients[i].GetId();

                string ThisUsername = playerSrv.GetUsername(Id);
                int ThisAvatar = playerSrv.GetPlayerAvatar(Id);

                Players[i] = ThisUsername + ":" + ThisAvatar;
            }

            Players[Players.Length - 1] = Username + ":" + Avatar;

            MatchInfo = new MatchPacket(MatchInfo.GetMatchId(), MatchInfo.GetAllowObservers(), MatchInfo.GetFriendsOnly(), MatchInfo.GetLightVsDark(), MatchInfo.GetMaxPlayers(), MatchInfo.GetGuildOption(), MatchInfo.GetPassword(), MatchInfo.GetPlayFormat(), MatchInfo.GetStructure(), MatchInfo.GetTimeLimit(), MatchInfo.GetTitle(), MatchInfo.GetType(), Players);
        }

        public void AddClientToMatch(ClientData client)
        {
            int emptySpot = IndexOfEmptySpot();

            if (emptySpot != -1)
            {
                // Update the MatchPacket with username and avatar
                string PlayerUsername = playerSrv.GetUsername(client.GetId());
                int PlayerAvatar = playerSrv.GetPlayerAvatar(client.GetId());
                UpdateMatchPacketWithNewUser(PlayerUsername, PlayerAvatar);

                Clients[emptySpot] = client;
                ChatRoom.AddClientToRoom(client);
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MATCH_JOIN, new JoinMatchResponsePacket(JoinMatchResponses.Responses.SUCCESS, MatchInfo)), client);

                string[] Usernames = GetListOfUsernames();
                Dictionary<string, int> Players = new Dictionary<string, int>(Usernames.Length);

                foreach (string Username in Usernames)
                {
                    Players.Add(Username, playerSrv.GetPlayerAvatar(client.GetId()));
                }

                SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.UPDATE_MATCH_USERNAMES, new UpdateLobbyUsersPacket(PlayerUsername + " joined the match lobby.", Players)));
            }
            else
            {
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.MATCH_JOIN, new JoinMatchResponsePacket(JoinMatchResponses.Responses.FULL, null)), client);
            }
        }

        public void SendPacketToPlayers(PacketSender p)
        {
            conSrv.SendPacketToClients(p, Clients);
        }

        public void RemoveClientFromMatch(ClientData Client)
        {
            // Remove client from clients array
            Clients[IndexOfClient(Client)] = null;

            // Remove client from chatroom
            ChatRoom.RemoveClientFromRoom(Client);

            string[] Usernames = GetListOfUsernames();
            Dictionary<string, int> Players = new Dictionary<string, int>(Usernames.Length);

            foreach (string Username in Usernames)
            {
                Players.Add(Username, playerSrv.GetPlayerAvatar(Client.GetId()));
            }

            string PlayerUsername = playerSrv.GetUsername(Client.GetId());
            SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.UPDATE_MATCH_USERNAMES, new UpdateLobbyUsersPacket(PlayerUsername + " left the match lobby.", Players)));
        }

        private int IndexOfEmptySpot()
        {
            for (int i = 0; i < Clients.Length; i++)
                if (Clients[i] == null)
                    return i;
            return -1;
        }

        public bool GameIsFull()
        {
            return IndexOfEmptySpot() == -1;
        }

        public int IndexOfClient(ClientData client)
        {
            for (int i = 0; i < Clients.Length; i++)
                if (Clients[i] != null && Clients[i] == client)
                    return i;
            return -1;
        }

        public bool UserIsInMatch(int UserId)
        {
            foreach (ClientData Client in Clients)
                if (Client.GetId() == UserId)
                    return true;
            return false;
        }

        public MatchPacket GetMatchPacket()
        {
            return MatchInfo;
        }

        private string[] GetListOfUsernames()
        {
            List<string> List = new List<string>();
            foreach (ClientData Client in Clients)
            {
                if (Client != null)
                {
                    List.Add(playerSrv.GetUsername(Client.GetId()));
                }
            }
            return List.ToArray();
        }

        public ClientData[] GetClients()
        {
            List<ClientData> ClientList = new List<ClientData>();

            foreach (ClientData Client in Clients)
                if (Client != null)
                    ClientList.Add(Client);
            return ClientList.ToArray();
        }

        public void TogglePlayerReady(int PlayerId)
        {
            // Update server-side bool
            for (int i = 0; i < Clients.Length; i++)
            {
                if (Clients[i] != null)
                {
                    PlayersReady[i] = !PlayersReady[i];
                }
            }

            // Update the clients on who changed ready
            string ReadyUsername = playerSrv.GetUsername(PlayerId);
            SendPacketToPlayers(new PacketSender(SERVER_ID, Commands.MATCH_READY, new object[] { ReadyUsername }));

            // Check if all players are ready now
            foreach (bool Ready in PlayersReady)
                if (!Ready)
                    return;

            // Start the setup of the game
            int[] PlayerIds = new int[] { Clients[0].GetId(), Clients[1].GetId() };
            gameSrv.CreateNewGame(PlayerIds, MatchInfo.GetType());
        }
    }
}
