﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using TCGData;
using TCGData.Packets;
using TCGData.Packets.Lobby;

namespace Server.src.services.Match
{
    public class MatchService : Server
    {
        public MatchService()
        {
            logSrv.Log("MatchService", "Loaded");
        }

        public void CreateMatch(int CreatorId, MatchPacket MatchPacket)
        {
            // We need to re-create the MatchPacket with MatchId, CreatorAvatar, and CreatorUsername
            int MatchId = CreatorId;

            bool AllowObservers = MatchPacket.GetAllowObservers();
            bool FriendsOnly = MatchPacket.GetFriendsOnly();
            bool LightVsDark = MatchPacket.GetLightVsDark();

            int MaxPlayers = MatchPacket.GetMaxPlayers();

            string GuildOption = MatchPacket.GetGuildOption();
            string Password = MatchPacket.GetPassword();
            string PlayFormat = MatchPacket.GetPlayFormat();
            string Structure = MatchPacket.GetStructure();
            string TimeLimit = MatchPacket.GetTimeLimit();
            string Title = MatchPacket.GetTitle();
            string Type = MatchPacket.GetType();

            int CreatorAvatar = playerSrv.GetPlayerAvatar(CreatorId);
            string CreatorUsername = playerSrv.GetUsername(CreatorId);

            string[] Players = new string[MaxPlayers];
            Players[0] = CreatorAvatar + ":" + CreatorUsername;

            MatchPacket = new MatchPacket(MatchId, AllowObservers, FriendsOnly, LightVsDark, MaxPlayers, GuildOption, Password, PlayFormat, Structure, TimeLimit, Title, Type, Players);

            // Get the lobby to create the match
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(CreatorId);

            // Add the new match to that lobby
            Lobby.AddMatchToLobby(new Match(CreatorId, MatchPacket));

            // Automatically add the creator to that match
            AddClientToMatch(conSrv.GetClientByID(CreatorId), MatchPacket.GetTitle());

            logSrv.LogDebug("MatchService", "User: " + CreatorId + " created match '" + MatchPacket.GetTitle() + "'");

            // Send updated matches to all users in that lobby
            SendUpdatedLobbyMatches(Lobby);
        }

        public Match GetMatchUserIsIn(int CreatorId)
        {
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(CreatorId);

            return Lobby.GetMatchUserIsIn(CreatorId);
        }

        public Match GetMatchWithTitle(Lobby.Lobby Lobby, string Title)
        {
            Match Match;

            for (int i = 0; i < Lobby.GetNumberOfMatchesInLobby(); i++)
            {
                Match = Lobby.GetMatch(i);
                if (Match.GetTitle().Equals(Title))
                {
                    return Match;
                }
            }
            return null;
        }

        public Lobby.Lobby GetLobbyMatchIsIn(int CreatorId)
        {
            Lobby.Lobby Lobby;
            Match Match;

            for (int i = 0; i < lobbySrv.GetNumberOfLobbies(); i++)
            {
                Lobby = lobbySrv.GetLobby(i);

                Match = Lobby.GetMatchUserIsIn(CreatorId);

                if (Match != null)
                {
                    return Lobby;
                }
            }

            return null;
        }

        public void DeleteMatch(Match Match)
        {
            // Get the lobby that the match is in
            Lobby.Lobby Lobby = GetLobbyMatchIsIn(Match.GetCreatorId());

            // Remove the match from the lobby
            Lobby.RemoveMatchFromLobby(Match);

            // Remove any existing players from the match
            ClientData[] Clients = Match.GetClients();

            foreach (ClientData Client in Clients)
                Match.RemoveClientFromMatch(Client);

            // Update lobby matches to the specified lobby
            SendUpdatedLobbyMatches(Lobby);

            logSrv.LogDebug("MatchService", "User: " + Match.GetCreatorId() + " deleted match");
        }

        public string GetMatchTitleUserIsIn(int UserId)
        {
            Match Match = GetMatchUserIsIn(UserId);
            return Match.GetTitle();
        }

        private void SendUpdatedLobbyMatches(Lobby.Lobby Lobby)
        {
            // Get all matches
            MatchPacket[] Matches = GetMatchesInLobby(Lobby);

            logSrv.LogDebug("MatchService", "Sending " + Matches.Length + " matches to " + chatRoomSrv.GetChatRoomLobby(Lobby.GetTitle()).GetClientsInRoom().Length + " clients");

            // Send matches to all users in that lobby
            conSrv.SendPacketToClients(new PacketSender(SERVER_ID, Commands.UPDATE_MATCHES, new UpdateMatchListPacket(Matches)), chatRoomSrv.GetChatRoomLobby(Lobby.GetTitle()).GetClientsInRoom());
        }

        public void SendUpdatedLobbyMatches(Lobby.Lobby Lobby, ClientData Client)
        {
            MatchPacket[] Matches = GetMatchesInLobby(Lobby);

            // If a user joins the lobby and there are no matches, we do not need to send them a packet
            if (Matches.Length > 0)
            {
                logSrv.LogDebug("MatchService", "Sending " + Matches.Length + " matches to " + chatRoomSrv.GetChatRoomLobby(Lobby.GetTitle()).GetClientsInRoom().Length + " clients");
                conSrv.SendPacketToClient(new PacketSender(SERVER_ID, Commands.UPDATE_MATCHES, new UpdateMatchListPacket(Matches)), Client);
            }
        }

        private MatchPacket[] GetMatchesInLobby(Lobby.Lobby Lobby)
        {
            MatchPacket[] matches = new MatchPacket[Lobby.GetNumberOfMatchesInLobby()];

            for (int i = 0; i < matches.Length; i++)
            {
                matches[i] = Lobby.GetMatch(i).GetMatchPacket();
            }

            return matches;
        }

        public void AddClientToMatch(ClientData client, string Title)
        {
            // Get the lobby that the user is in
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(client.GetId());

            // Get the match object using the title
            Match Match = GetMatchWithTitle(Lobby, Title);

            // Add the client to the match
            Match.AddClientToMatch(client);
        }

        public void RemoveClientFromMatch(ClientData client, string Title)
        {
            // Get the lobby that the user is in
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(client.GetId());

            // Get the match object using the title
            Match Match = GetMatchWithTitle(Lobby, Title);

            if (client.GetId() == Match.GetCreatorId())
            {
                // Just delete the entire match
                DeleteMatch(Match);
            }
            else
            {
                // Remove the client from the match
                Match.RemoveClientFromMatch(client);
            }
        }

        public void HandleMatchReady(int PlayerId)
        {
            Match Match = GetMatchUserIsIn(PlayerId);
            Match.TogglePlayerReady(PlayerId);
        }

        public bool PlayerIsInAMatch(int UserId)
        {
            Match Match = GetMatchUserIsIn(UserId);
            return Match != null;
        }

        public void HandleSendMatchPacket(int PlayerId, string MatchTitle)
        {
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(PlayerId);
            Match Match = GetMatchWithTitle(Lobby, MatchTitle);
            MatchPacket Packet = Match.GetMatchPacket();
            PacketSender PacketSender = new PacketSender(SERVER_ID, Commands.REQUEST_MATCH, Packet);
            conSrv.SendPacketToClient(PacketSender, conSrv.GetClientByID(PlayerId));
        }

        public MatchPacket GetMatchPacket(int PlayerId, string MatchTitle)
        {
            Lobby.Lobby Lobby = lobbySrv.GetLobbyUserIsIn(PlayerId);
            Match Match = GetMatchWithTitle(Lobby, MatchTitle);
            MatchPacket Packet = Match.GetMatchPacket();
            return Packet;
        }
    }
}
