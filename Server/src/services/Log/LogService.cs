﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.Collection;
using Server.src.services.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Timers;

namespace Server.src.services.Log
{
    public class LogService : Server
    {
        private static StreamWriter writer;
        private static Timer timer;
        private readonly string logsDir;
        private readonly string CS_LOG_SCRIPT_URL;
        private readonly bool DEBUG = true;

        private const int LOG_INTERVAL = 100;

        public LogService()
        {
            bool Export = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "EXPORT_LOGS").Equals("1");
            if (Export)
            {
                logsDir = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "LOG_DIR");

                if (!Directory.Exists(logsDir))
                    Directory.CreateDirectory(logsDir);

                string FileName = DateTime.Now.ToString("yyyy-MM-dd-HH-mm");

                writer = new StreamWriter(logsDir + @"\" + FileName + ".log");
                timer = new Timer
                {
                    Interval = LOG_INTERVAL
                };
                timer.Elapsed += Timer_Elapsed;
                timer.Enabled = true;
            }

            bool CSLogging = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "CS_LOGGING").Equals("1");
            if (CSLogging)
            {
                CS_LOG_SCRIPT_URL = cfgSrv.GetConfigValue(cfgSrv.FEATURES_CFG, "CS_LOG_SCRIPT_URL");
                LogDebug("LogService", "Setting up log script URL: " + CS_LOG_SCRIPT_URL);
            }

            LogDebug("LogService", "Loaded");
        }

        public void Log(string Title, string Message)
        {
            string MessageWithDate = "[" + DateTime.Now + "] " + Title + ": " + Message;
            Console.WriteLine(MessageWithDate);

            if (writer != null)
                writer.WriteLine(MessageWithDate);
        }

        public void Log(string Title, string Message, string PlayerName)
        {
            string MessageWithDate = "[" + DateTime.Now + "] " + Title + ": " + Message.Replace("%20", PlayerName);
            Console.WriteLine(MessageWithDate);

            if (writer != null)
                writer.WriteLine(MessageWithDate);
        }

        public void LogDebug(string Title, string Message)
        {
            if (DEBUG && writer != null)
            {
                string MessageWithDate = "[" + DateTime.Now + "] DEBUG" + Title + ": " + Message;
                writer.WriteLine(MessageWithDate);
            } 
        }

        public void LogError(string Title, string Message)
        {
            Log(Title + " ERROR", Message);
        }

        public void LogWarning(string Title, string Message)
        {
            Log(Title + " WARNING", Message);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            writer.Flush();
        }

        public void WriteTransactionLog(int Id, int UserId, SortedDictionary<int, int> Cards)
        {
            long UnixTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

            string CardsString = string.Empty;

            foreach (int CardId in Cards.Keys)
            {
                if (!string.IsNullOrEmpty(CardsString))
                    CardsString += CollectionService.PRIMARY_DELIMITER;

                CardsString += CardId + CollectionService.SECONDARY_DELIMITER + Cards[CardId];
            }

            string[] Columns = { "id", "date", "userid", "cards" };
            string[] Values = { Id.ToString(), UnixTime.ToString(), UserId.ToString(), CardsString };
            dbSrv.CreateTableRow("transactions", Columns, Values);
        }

        public void WriteCsLog(string username, string category, string action)
        {
            if (!string.IsNullOrEmpty(CS_LOG_SCRIPT_URL))
            {
                using WebClient WebClient = new WebClientWithTimeout
                {
                    Proxy = null
                };

                try
                {
                    WebClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    WebClient.UploadString(CS_LOG_SCRIPT_URL, "un=" + username + "&ca=" + category + "&ev=" + action);
                }
                catch (WebException ex)
                {
                    LogError("LogService", "Unable to send log: " + ex.Message + " - Trying again");
                    WriteCsLog(username, category, action);
                }
            }
        }
    }
}
