﻿/*
 *   Star Wars Galaxies TCGEmu
 *   Copyright (C) 2016 Iosnowore
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Server.src.services.AI;
using Server.src.services.Chat;
using Server.src.services.Client;
using Server.src.services.Collection;
using Server.src.services.Command;
using Server.src.services.Config;
using Server.src.services.Connection;
using Server.src.services.Database;
using Server.src.services.DeckBuilder;
using Server.src.services.Game;
using Server.src.services.Guild;
using Server.src.services.Lobby;
using Server.src.services.Log;
using Server.src.services.Login;
using Server.src.services.Match;
using Server.src.services.Packet;
using Server.src.services.Ping;
using Server.src.services.Player;
using Server.src.services.Staff;
using Server.src.services.Status;
using Server.src.services.Trade;
using System.Runtime.InteropServices;

namespace Server
{
    public class Server
    {
        public static AIService aiSrv;
        public static ChatRoomService chatRoomSrv;
        public static ChatService chatSrv;
        public static ClientService clientSrv;
        public static CollectionService collectionSrv;
        public static CommandService cmdSrv;
        public static ConfigService cfgSrv;
        public static ConnectionService conSrv;
        public static DatabaseService dbSrv;
        public static DeckBuilderService deckBuildSrv;
        public static GameService gameSrv;
        public static GuildService guildSrv;
        public static LoginService loginSrv;
        public static LobbyService lobbySrv;
        public static LogService logSrv;
        public static MatchService matchSrv;
        public static PacketService packetSrv;
        public static PackService packSrv;
        public static PingPlayerService pingSrv;
        public static PlayerService playerSrv;
        public static StaffService staffSrv;
        public static StatusService statusSrv;
        public static TradeService tradeSrv;

        public const int SERVER_ID = -1;

        private static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            // These are required to load first
            cfgSrv = new ConfigService();
            logSrv = new LogService();
            statusSrv = new StatusService();
            dbSrv = new DatabaseService();

            aiSrv = new AIService();
            chatSrv = new ChatService();
            cmdSrv = new CommandService();
            clientSrv = new ClientService();
            playerSrv = new PlayerService();
            collectionSrv = new CollectionService();
            conSrv = new ConnectionService();
            chatRoomSrv = new ChatRoomService();
            deckBuildSrv = new DeckBuilderService();
            gameSrv = new GameService();
            guildSrv = new GuildService();
            loginSrv = new LoginService();
            lobbySrv = new LobbyService();
            matchSrv = new MatchService();
            packetSrv = new PacketService();
            packSrv = new PackService();
            staffSrv = new StaffService();
            tradeSrv = new TradeService();
            pingSrv = new PingPlayerService();

            // Open the server for players
            conSrv.StartConnection();

            logSrv.Log("TCG Server", "Loaded");
            statusSrv.SetServerOnline();
            System.Console.ReadKey();
        }

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    statusSrv.SetServerOffline();
                    return false;
                default:
                    return false;
            }
        }
    }
}
