<?php
	//
	//	@author: Iosnowore (c) 2019 TCGEmu
	//
	
	function getConn($galaxy) {
		$table;
		
		if ($galaxy === 'Vengeance') {
			$table = 'db/tcg.db';
		} else {
			$table = 'db/tc_tcg.db';
		}
		
		$conn = new SQLite3($table);
		return $conn;
	}
?>