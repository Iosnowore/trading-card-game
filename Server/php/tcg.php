<?php
	//
	//	@author: Iosnowore (c) 2019 TCGEmu
	//
	
	include 'tcg_utils.php';

	$realKey = 1234;
	$realServerIp = '';
	
	$username = $_GET['un'];
	$lootId = $_GET['id'];
	$key = $_GET['key'];
	$type = $_GET['type'];
	$galaxy = $_GET['g'];
	
	// TODO: Switch to POST for increased security
	/*if (empty($username)) {
		$username = $_POST['un'];
		$lootId = $_POST['id'];
		$key = $_POST['key'];
		$type = $_POST['type'];
		$galaxy = $_POST['g'];
	}*/

	$ip;

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	if (!empty($realServerIp) && $ip != $realServerIp) {
		logFunc($logFile, 'Invalid IP: ' . $ip);
		echo 0;
		return;
	}

	if (empty($username) || empty($key) || !is_numeric($type) || empty($galaxy) || ($type == 1 && empty($lootId))) {
		logFunc('One or more args are missing.');
		echo 0;
		return;
	}

	if ($key != $realKey) {
		logFunc('Invalid Key: ' . $key);
		echo 0;
		return;
	}

	// Claim pack
	switch ($type) {
		case 0: // Request claimable loot items
			echo requestClaimableLoot($username, $galaxy);
			break;
		case 1: // Claim loot item
			echo claimLootItem($username, $lootId, $galaxy);
			break;
		case 2: // Claim pack
			echo claimPack($username, $lootId, $galaxy);
			break;
		default:
			echo 0;
			logFunc('Invalid type: ' . $type);
			break;
	}

	function logFunc($txt) {
		$logFile = fopen('tcg.log', 'a');
		fwrite($logFile, $txt . PHP_EOL);
		fclose($logFile);
	}
?>