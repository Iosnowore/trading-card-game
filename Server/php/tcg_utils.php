<?php
	//
	//	@author: Iosnowore (c) 2019 TCGEmu
	//

	include 'mysqlite_conn.php';

	function claimPack($un, $packId, $galaxy) {
		// Create DB connection
		$conn = getConn($galaxy);
		
		// Get the player's userid, using their username
		$query = "SELECT id FROM users WHERE username = '$un' collate NOCASE";
		$userId = $conn->querySingle($query);
		
		if (empty($userId)) {
			return 0;
		}
		
		// Check and see what packs the user already has
		$query = "SELECT packs FROM collections WHERE id='$userId'";
		$packString = $conn->querySingle($query);
		
		// Split pack string into an array to look at individual packs
		$packStringArr = explode('|', $packString);
		
		// Go through each pack and see if there is an already existing pack with the same ID
		// If so, add one to the quantity and return 1 (success)
		// If not, add it to the pack string
		$packSubstring;
		for ($i = 0; $i < sizeof($packStringArr); $i++) {
			$packSubstring = explode(':', $packStringArr[$i]);
			
			if ($packSubstring[0] === $packId) {
				$packString = str_replace($packStringArr[$i], $packId . ':' . ($packSubstring[1] + 1), $packString);
				$query = "UPDATE collections SET packs = '$packString' where id = '$userId'";
				$conn->exec($query);
				return 1;
			}
		}
		
		// If we are here, the user does not have an existing pack of the same ID
		if (!empty($packString)) {
			// There are existing packs and therefore we must add a concatenate char (|)
			$packString .= '|';
		}
		
		// Let's now add the pack to the string
		$packString .= $packId . ':1';
		$query = "UPDATE collections SET packs = '$packString' where id = '$userId'";
		$conn->exec($query);
		return 1;
	}
	
	function requestClaimableLoot($un, $galaxy) {
		// Create DB connection
		$conn = getConn($galaxy);
		
		// Get the player's userid, using their username
		$query = "SELECT id FROM users WHERE username = '$un' collate NOCASE";
		$userId = $conn->querySingle($query);
		// Get all game redeemable items
		$query = "SELECT redeemables FROM swg_redeemables WHERE id='$userId'";
		$packString = $conn->querySingle($query);
		return $packString;
	}
	
	function claimLootItem($un, $id, $galaxy) {
		// Create DB connection
		$conn = getConn($galaxy);
		
		// Get the player's userid, using their username
		$query = "SELECT id FROM users WHERE username = '$un' collate NOCASE";
		$userId = $conn->querySingle($query);
		
		// Get all game redeemable items
		$query = "SELECT redeemables FROM swg_redeemables WHERE id='$userId'";
		$redeemableString = $conn->querySingle($query);
		// Split the redeemable string into redeemable items
		$redeemableSubstring = explode('|', $redeemableString);
		
		// Go through each redeemable item and check the id.
		// If the id matches, reduce the quantity by 1.
		// If the quantity is already 1, just remove the item string entirely
		$redeemable;
		for ($i = 0; $i < sizeof($redeemableSubstring); $i++) {
			$redeemable = explode(':', $redeemableSubstring[$i]);
			if ($redeemable[0] === $id) {
				// Let's see what we are replacing the old value with
				// If there's only 1 left, remove the whole thing
				// If there's more than 1 left, decrement it by 1
				$replacement = $redeemable[1] == 1 ? "" : "$id:" . ($redeemable[1] - 1);
				// @TODO: We will need to consider that the below will cause massive issues
				// Ex. if we do str_replace('0:1', '') ..... it will also replace elements like '100:1' -> '10'
				// Perhaps we shall replace by element like so...
				$redeemableSubstring[$i] = str_replace("$id:" . $redeemable[1], $replacement, $redeemableSubstring[$i]);
				
				// Filter out any possible empty array values
				$redeemableSubstring = array_filter($redeemableSubstring);
				
				// Now let's reconstruct our redeemablesString
				$redeemableString = implode('|', $redeemableSubstring);
				
				$query = "UPDATE swg_redeemables SET redeemables = '$redeemableString' WHERE id = '$userId'";
				$conn->exec($query);
				return 1;
			}
		}
		// If we reach here, the user did not have the loot item.
		return 0;
	}
?>