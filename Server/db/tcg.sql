--
-- File generated with SQLiteStudio v3.4.4 on Sat Jul 1 14:05:12 2023
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: collections
CREATE TABLE IF NOT EXISTS "collections" (
	`id`	INTEGER NOT NULL UNIQUE,
	`cards`	TEXT,
	`redeemables`	TEXT,
	`packs`	TEXT,
	`avatars`	TEXT,
	`promopacks`	TEXT,
	`redeemed`	TEXT,
	`wants`	TEXT,
	PRIMARY KEY(`id`)
);

-- Table: decks
CREATE TABLE IF NOT EXISTS "decks" (
	`id`	INTEGER NOT NULL UNIQUE,
	`decks`	TEXT,
	PRIMARY KEY(`id`)
);

-- Table: guilds
CREATE TABLE IF NOT EXISTS "guilds" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT NOT NULL UNIQUE,
	"created"	INTEGER NOT NULL,
	"members"	TEXT NOT NULL,
	"message"	TEXT,
	"last_modified"	INTEGER
);

-- Table: postedtrades
CREATE TABLE IF NOT EXISTS `postedtrades` (
	`id`	INTEGER NOT NULL UNIQUE,
	`playerid`	INTEGER NOT NULL,
	`dateposted`	TEXT NOT NULL,
	`offered`	TEXT,
	`want`	INTEGER,
	PRIMARY KEY(`id`)
);

-- Table: swg_redeemables
CREATE TABLE IF NOT EXISTS "swg_redeemables" (
	"id"	INTEGER NOT NULL UNIQUE,
	"redeemables"	TEXT,
	PRIMARY KEY("id")
);

-- Table: transactions
CREATE TABLE IF NOT EXISTS "transactions" (
	"id"	INTEGER NOT NULL UNIQUE,
	"date"	INTEGER NOT NULL,
	"userid"	INTEGER NOT NULL,
	"cards"	TEXT NOT NULL,
	PRIMARY KEY("id")
);

-- Table: users
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"username"	TEXT NOT NULL UNIQUE,
	"avatar"	INTEGER NOT NULL DEFAULT 1,
	"message"	TEXT,
	"friends"	TEXT,
	"ignores"	TEXT,
	"guild"	INTEGER,
	"im"	TEXT,
	"homepage"	TEXT,
	"games"	TEXT NOT NULL DEFAULT '0-0',
	"scenarios"	TEXT
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
